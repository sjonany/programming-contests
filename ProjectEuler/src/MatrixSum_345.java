import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//euler 345
//actually this is just bipartite matching. but this turns out to be fast enough
public class MatrixSum_345 {
	static int N;
	static int[][] m;
	static int max;
	static int[] bestComb;
	static boolean[] seen;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(in.readLine());
		m = new int[N][N];
		for(int i = 0; i < N; i++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			for(int j = 0; j < N; j++){
				m[i][j] = Integer.parseInt(tk.nextToken());
			}
		}
		
		bestComb = new int[N+1];
		max = 0;
		seen = new boolean[N+1];
		
		dfs(1, new boolean[N+1], new int[N+1], 0);
		for(int i = 1 ; i<= N; i++){
			System.out.print(m[i-1][bestComb[i]-1]);
			if(i != N)System.out.print(" + ");
		}
		System.out.print(" = " + max);
	}
	
	static void dfs(int toSet, boolean[] used, int[] order, int curSum){
		if(toSet == 2 && !seen[order[1]]){
			System.out.println(order[1]);
			seen[order[1]] = true;
		}
		
		if(toSet == N+1){
			if(curSum > max){
				max = curSum;
				bestComb = order.clone();
			}
			return;
		}
		
		if(curSum + 1000 * (N+1-toSet) < max){
			return;
		}
		
		for(int i = 1 ; i <= N; i++){
			if(!used[i]){
				used[i] = true;
				order[toSet] = i;
				
				dfs(toSet+1, used, order, curSum + m[toSet-1][i-1]);
				used[i] = false;
			}
		}
	}
}
