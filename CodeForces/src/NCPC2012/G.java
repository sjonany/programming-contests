package NCPC2012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class G {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		tk = tk(in.readLine());
		int W = Integer.parseInt(tk.nextToken());
		int N = Integer.parseInt(tk.nextToken());
		
		Set<Line> lines = new HashSet<Line>();
		Line first = null;
		for(int i = 0 ; i < N; i++) {
			tk = tk(in.readLine());
			int x1 = Integer.parseInt(tk.nextToken());
			int y1 = Integer.parseInt(tk.nextToken());
			int x2 = Integer.parseInt(tk.nextToken());
			int y2 = Integer.parseInt(tk.nextToken());
			Line l = new Line(x1,y1,x2,y2);
			if(first == null) first = l;
			lines.add(l);
		}
		
		boolean isAllPar = true;
		for(Line l : lines) {
			if(l.a != first.a || l.b != first.b) {isAllPar = false; break;}
		}
		
		int numDistinct = lines.size();
		int ans = -1;
		if(isAllPar) {
			int numReg = numDistinct + 1;
			if(W <= numReg) {
				ans = 0;
			} else {
				numReg *= 2;
				int cost = 1;
				if(W <= numReg) {
					ans = cost;
				} else {
					int need = W - numReg;
					cost += need / 2 + need % 2;
					ans = cost;
				}
			}
		} else {
			int numReg = numDistinct * 2;
			if(W <= numReg) {
				ans = 0;
			} else {
				int need = W - numReg;
				ans = need / 2 + need % 2;
			}
		}
		
		out.println(ans);
		out.close();
	}
	
	static class Line {
		public long a;
		public long b;
		public long c;
		
		public Line(long x1, long y1, long x2, long y2){
			long A = y2 - y1;
			long B = x1 - x2;
			long C = -A * x1 - B * y1;
			if(A < 0) {A = -A; B = -B; C = -C;};
			if(A==0 && B < 0) {B= -B; C = -C;}
			this.a = A; this.b = B; this.c = C;
			long g = gcd(a, gcd(b,c));
			a/=g; b/=g; c/=g;
		}
		
		long gcd(long a, long b){ 
			a = Math.abs(a);
			b = Math.abs(b);
			if(a == 0 || b == 0) return a + b;
			while(b > 0) {
				long c = b;
				b = a % b;
				a = c;
			}
			return a;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (a ^ (a >>> 32));
			result = prime * result + (int) (b ^ (b >>> 32));
			result = prime * result + (int) (c ^ (c >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Line other = (Line) obj;
			if (a != other.a)
				return false;
			if (b != other.b)
				return false;
			if (c != other.c)
				return false;
			return true;
		}
	}

	static StringTokenizer tk (String s){
		return new StringTokenizer(s);
	}
}
