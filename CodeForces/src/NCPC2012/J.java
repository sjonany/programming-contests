package NCPC2012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class J {
	static int N;
	static int[] p;
	static int[] r;
	static int[] c;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		N = Integer.parseInt(in.readLine());
		p = new int[N+1];
		r = new int[N+1];
		c = new int[N+1];
		
		p[0] = -1;
		Item[] items = new Item[N];
		for(int i = 1; i <= N; i++) {
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int pi = Integer.parseInt(tk.nextToken());
			int ri = Integer.parseInt(tk.nextToken());
			int ci = Integer.parseInt(tk.nextToken());
			p[i] = pi;
			r[i] = ri;
			c[i] = ci;
			items[i-1] = new Item(i, ri);
		}
		
		Arrays.sort(items);
		int count = 0;
		for(Item it : items) {
			if(push(it.id, it.weight)) {
				count++;
			}
		}
		
		out.println(count);
		out.close();
	}
	
	static boolean push(int cur, int val) {
		if(cur == 0) {
			return true;
		} else {
			if(c[cur] >= val ){
				if(push(p[cur], val)) {
					c[cur] -= val;
					return true;
				} else {
					return false;
				}
			}else{
				return false;
			}
		}
	}
	
	static class Item implements Comparable<Item>{
		public Item(int id, int weight) {
			this.id = id;
			this.weight = weight;
		}
		public int id;
		int weight;
		@Override
		public int compareTo(Item o) {
			return this.weight - o.weight;
		}
	}
}
