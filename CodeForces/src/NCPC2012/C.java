package NCPC2012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class C {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		List<Integer> input = new ArrayList<Integer>();
		SortedSet<Integer> set = new TreeSet<Integer>();
		while(true){
			String s = in.readLine();
			if(s == null || s.trim().length() ==0){
				break;
			}
			
			if(s.charAt(0) == '#'){
				input.add(-1);
			}else{
				int it = Integer.parseInt(s);
				input.add(it);
				set.add(it);
			}
		}
		
		Map<Integer, Integer> indexer = new HashMap<Integer, Integer>();
		int[] back = new int[set.size()+1];
		int id = 1;
		for(int it : set){
			back[id] = it;
			indexer.put(it, id);
			id++;
		}
		
		Fenwick fen = new Fenwick(id);
		int count = 0;
		for(int q : input){
			if(q == -1){
				int index = count/2;
				index++;
				
				int it = bin(fen, index)+1;
				fen.increment(it, -1);
				out.println(back[it]);
				count--;
				
			}else{
				fen.increment(indexer.get(q), 1);
				count++;
			}
		}
		
		out.close();
	}
	
	//get first int >= x, x >= 1
	static int bin(Fenwick fen, int x) {
		int lo = 1;
		int hi = fen.tree.length-1;
		
		if(fen.getTotalFreq(lo) >= x){
			lo = 0;
		}else{
			while(lo < hi) {
				int mid = lo + (hi-lo+1)/2;
				if(fen.getTotalFreq(mid) >= x){
					hi = mid-1;
				}else{
					lo = mid;
				}
			}
		}
		return lo;
	}
	
	static class Fenwick{
		int[] tree;
		public Fenwick(int n){
			tree = new int[n+1];
		}
		
		public void increment(int i, int val) {
			while(i < tree.length){
				tree[i] += val;
				i += (i & -i);
			}
		}
		
		public int getTotalFreq(int i){
			int sum = 0;
			while(i > 0){
				sum += tree[i];
				i -= (i & -i);
			}
			return sum;
		}
	}
	
	static StringTokenizer tk (String s){
		return new StringTokenizer(s);
	}
}
