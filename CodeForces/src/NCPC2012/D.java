package NCPC2012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class D {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int X = Integer.parseInt(in.readLine());
		char[] arr = in.readLine().toCharArray();

		int curStat = 0;
		char curHold = ' ';
		int count = 0;
		for(int i = 0; i < arr.length; i++) {
			int newStat = 0;
			if(arr[i] == 'W') {
				newStat = curStat - 1;
			} else {
				newStat = curStat + 1;
			}

			if(Math.abs(newStat) > X) {
				if(curHold == ' '){
					curHold = arr[i];
				} else {
					if(curHold == arr[i]) {
						break;
					} else {
						if(curHold == 'W'){
							curStat--;
						}else{
							curStat++;
						}
						curHold = ' ';
						count++;
						i--;
					}
				}
			}else{
				curStat = newStat;
				count++;
			}
		}
		if(curHold != ' ') {
			int newStat = 0;
			if(curHold == 'W') {
				newStat = curStat - 1;
			} else {
				newStat = curStat + 1;
			}
			if(Math.abs(newStat) <= X) {
				count++;
			}
		}
		
		out.println(count);
		out.close();
	}
}
