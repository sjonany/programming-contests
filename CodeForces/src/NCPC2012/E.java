package NCPC2012;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class E {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	static int J;
	static int R;
	static int D;
	static int SOURCE;
	static int DEST;
	
	static Point2D[] points;
	static int edges[][];
	static double[][] dist;
	static boolean found = false;
	static double[][][] angles;
	static Line2D[][] lines;
	public static void main(String[] args) throws Exception{
		out = new PrintWriter(System.out);
		
		in = new BufferedReader(new InputStreamReader(System.in));
		tk = tk (in.readLine());
		J = Integer.parseInt(tk.nextToken());
		R = Integer.parseInt(tk.nextToken());
		D = Integer.parseInt(tk.nextToken());
		
		points = new Point2D[J];
		for(int i = 0; i < J; i++){
			tk = tk (in.readLine());
			int x = Integer.parseInt(tk.nextToken());
			int y = Integer.parseInt(tk.nextToken());
			points[i] = new Point2D.Double(x,y);
		}
		lines = new Line2D[J][J];
		for(int i = 0; i < J; i++){
			for(int j = 0; j < J; j++){
				lines[i][j] = new Line2D.Double(points[i], points[j]);
			}	
		}
		
		List<Integer>[] oldEdges = (List<Integer>[]) Array.newInstance(List.class, J);
		for(int i = 0; i < J; i++){
			oldEdges[i] = new ArrayList<Integer>();
		}
		
		for(int i = 0; i < R; i++){
			tk = tk (in.readLine());
			int from = Integer.parseInt(tk.nextToken()) - 1;
			int to = Integer.parseInt(tk.nextToken()) - 1;
			oldEdges[from].add(to);
		}
		
		angles = new double[J][J][J];
		for(int i = 0; i < J; i++){
			for(int j = 0; j < J; j++){
				for(int k = 0; k < J; k++){	
					double aa = getAngle(lines[i][j], lines[j][k]);
					double a = Math.toDegrees(Math.min(aa, 2*Math.PI - aa));
					angles[i][j][k] = a;
					if(i==j || j == k){
						angles[i][j][k] = 0;
					}
				}
			}
		}
		
		edges = new int[J][];
		for(int i = 0; i < J; i++){
			edges[i] = new int[oldEdges[i].size()];
			for(int j = 0; j < edges[i].length; j++){
				edges[i][j] = oldEdges[i].get(j);
			}
		}
		
		SOURCE = 0;
		DEST= J -1;
		
		dist = new double[J][J];
		for(int i = 0; i < J; i++){
			for(int j = 0; j < J; j++){
				dist[i][j] = points[i].distance(points[j]);
			}	
		}

		ans = new double[J][J];
		visit = new boolean[J][J];
		
		if(!check(1000)){
			out.println("Impossible");
			out.close();
			return;
		}
		
		double lo = 0;
		double hi = 360;
		while( hi - lo > 0.0000001){
			double mid = (lo+hi)/2;
			if(check(mid)){
				hi = mid;
			}else{
				lo = mid;
			}
		}
		
		out.println((lo+hi)/2);
		out.close();
	}

	static double[][] ans;
	static boolean[][] visit;
	static boolean check(double angle) {
		//prev cur
		for(int i = 0 ;i < J; i++){
			for(int j = 0 ;j < J; j++){
				ans[i][j] = Integer.MAX_VALUE;
				visit[i][j] = false;
			}
		}
		PriorityQueue<PQNode> pq = new PriorityQueue<PQNode>();
		pq.add(new PQNode(new Node(0,SOURCE), 0));
		
		double answer = Integer.MAX_VALUE;
		while(!pq.isEmpty()){
			PQNode pqnode = pq.poll();
			Node node = pqnode.node;
			double oldDist = pqnode.weight;
			if(node.to == DEST) {
				answer = Math.min(answer, oldDist);
			}
			if(oldDist > ans[node.from][node.to]){
				continue;
			}
			visit[node.from][node.to] = true;
			for(int out : edges[node.to]){
				int cur = node.to;
				int prev = node.from;
				double a = angles[prev][cur][out];
				if(a > angle){
					continue;
				}
				double newdist = pqnode.weight + dist[cur][out];
				if(!visit[cur][out] && newdist < ans[cur][out]){
					ans[cur][out] = newdist;
					pq.add(new PQNode(new Node(cur, out), newdist));
				}
			}
		}
		return answer <= D + 0.00001;
	}
	
	static String str(Line2D line){
		return line.getP1() + " " + line.getP2();
	}
	
	static double getAngle(Line2D v1, Line2D v2){
		double dx1 = v1.getX2() - v1.getX1();
		double dy1 = v1.getY2() - v1.getY1();
		
		double dx2 = v2.getX2() - v2.getX1();
		double dy2 = v2.getY2() - v2.getY1();
		
		
		double result = Math.atan2(dy2, dx2) - Math.atan2(dy1, dx1);
		if(result < 0) return result + 2.0 * Math.PI;
		return result;
	}
	
	static class Node{
		public int from;
		public int to;
		public Node(int from, int to) {
			this.from = from;
			this.to = to;
		}
	}
	
	static class PQNode implements Comparable<PQNode>{
		public Node node;
		public double weight;
		public PQNode(Node node, double weight) {
			this.node = node;
			this.weight = weight;
		}
		@Override
		public int compareTo(PQNode arg0) {
			return Double.compare(weight, arg0.weight);
		}
	}
	
	static StringTokenizer tk (String s){
		return new StringTokenizer(s);
	}
}
