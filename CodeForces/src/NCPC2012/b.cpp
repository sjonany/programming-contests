#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include <vector>
#include <queue>

using namespace std;

int n;
int ind[100005];
int in[100005];

typedef long long LL;

LL fenwick[100005];

int update(int at, int value){
    for(int i = at; i <= n; i += (i & (-i)))
	fenwick[i] += value;
}

LL get(int at){
    LL sum = 0;
    for(int i = at; i; i -= (i & (-i)))
	sum += fenwick[i];
    return sum;
}



int main(){
    scanf("%d", &n);
    int a;
    for(int i = 1; i <= n; i++ ){
	scanf("%d", &a);
	ind[a] = i;
    }
    LL cnt = 0;
    for(int i = 1; i <= n; i++ ){
	int a;
	scanf("%d", &a);
	cnt += get(n) - get(ind[a]);
	update(ind[a], 1);
    }
    if(cnt % 2) printf("Impossible\n");
    else printf("Possible\n");

    return 0;
}
