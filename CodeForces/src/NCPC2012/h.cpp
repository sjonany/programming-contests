#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include <vector>
#include <queue>

using namespace std;

#define MAXN 1005
typedef pair<int, int> PII;
int N, H, L;
bool visited[MAXN];
vector<PII> adj[MAXN]; // v, c
int dist[MAXN];

int main(){
    priority_queue<PII, vector<PII>, greater<PII> > Q;
    scanf("%d %d %d", &N, &H, &L);
    int a;
    for(int i = 0; i < N; i++ )
	dist[i] = 100000000;
    for(int i = 0; i < H; i++ ){
	scanf("%d", &a);
	Q.push(PII(0, a));
	dist[a] = 0;
    }
    int u, v, c;
    for(int i = 0; i < L; i++ ){
	scanf("%d %d", &u, &v);
	adj[u].push_back(PII(v, 1));
	adj[v].push_back(PII(u, 1));
    }

    while(!Q.empty()){
	u = Q.top().second;
	Q.pop();
	if(visited[u]) continue;
	visited[u] = true;
	int sz = adj[u].size();
	for(int i = 0; i < sz; i++ ){
	    v = adj[u][i].first;
	    c = adj[u][i].second;
	    if(!visited[v] && dist[v] > dist[u] + c){
		dist[v] = dist[u] + c;
		Q.push(PII(dist[v], v));
	    }
	}
    }

    int maxx = 0;
    int ind = 0;
    for(int i = 0; i < N; i++ ){
	if(dist[i] > maxx){
	    maxx = dist[i];
	    ind = i;
	}
    }

    printf("%d\n", ind);
    return 0;
}
