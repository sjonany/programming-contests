#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>

using namespace std;

char in[105];

int main(){
    int n;
    scanf("%d", &n);
    scanf("%s", in);
    int maxx = 0;
    int len = strlen(in);
    int w = 0, m = 0;
    char skip = '!';
    for(int j = 0; j < len; j++ ){
	if(skip == 'W'){
	    if(fabs(w + 1 - m) <= n){
		skip = '!';
		maxx++;
		w++;
	    }
	} else if(skip == 'M'){
	    if(fabs(m + 1 - w) <= n){
		skip = '!';
		maxx++;
		m++;
	    }
	}
	int tw = w;
	int tm = m;
	if(in[j] == 'W'){
	    tw++;
	} else {
	    tm++;
	}
	if(fabs(tw - tm) <= n){
	    maxx++;
	    w = tw;
	    m = tm;
	} else {
	    if(skip == '!'){
		skip = in[j];
	    } else {
		break;
	    }
	}
    }

    printf("%d\n", maxx);
    return 0;
}
