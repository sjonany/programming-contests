package MemSQLStartCup1;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.PriorityQueue;
import java.util.Set;

public class StadiumAndGames {
	static BigInteger curDen;
	static BigInteger MAX = BigInteger.valueOf(Long.MAX_VALUE).multiply(BigInteger.valueOf(3));
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		Set<BigInteger> ans = new HashSet<BigInteger>();
		long n = in.nextLong();
		BigInteger two = BigInteger.valueOf(2);
		BigInteger three = BigInteger.valueOf(3);
		BigInteger four = BigInteger.valueOf(4);
		BigInteger negOne = BigInteger.valueOf(-1);
		curDen = BigInteger.ONE;
		for(; curDen.compareTo(MAX) < 0 ; curDen = curDen.multiply(two)){
			BigInteger twoDenSq = curDen.multiply(curDen).multiply(two);
			BigInteger b = twoDenSq.subtract(curDen.multiply(three));
			BigInteger c = twoDenSq.multiply(BigInteger.valueOf(-n));
			BigInteger root = getRoot(b.multiply(b).subtract(c.multiply(four)));
			if(root == null){
				continue;
			}
			BigInteger negB = b.multiply(negOne);
			BigInteger firstNum = negB.add(root);
			if(firstNum.compareTo(BigInteger.ZERO) >= 0 && firstNum.mod(two).equals(BigInteger.ZERO)){
				BigInteger newAns = firstNum.divide(two);
				if(newAns.mod(curDen).equals(BigInteger.ZERO) && !newAns.divide(curDen).mod(two).equals(BigInteger.ZERO)) {
					ans.add(newAns);
				}
			}
			
			BigInteger secondNum = negB.subtract(root);
			if(secondNum.compareTo(BigInteger.ZERO) >= 0 && secondNum.mod(two).equals(BigInteger.ZERO)){
				BigInteger newAns = secondNum.divide(two);
				if(newAns.mod(curDen).equals(BigInteger.ZERO) && !newAns.divide(curDen).mod(two).equals(BigInteger.ZERO)) {
					ans.add(newAns);
				}
			}
		}
		if(ans.size() == 0){
			out.println(-1);
		}else{
			PriorityQueue<BigInteger> pq = new PriorityQueue<BigInteger>();
			for(BigInteger b : ans){
				pq.add(b);
			}
			while(!pq.isEmpty()){
				out.println(pq.poll());
			}
		}
		out.close();
		}
	
	public static BigInteger getRoot(BigInteger x){
	    if (x.compareTo(BigInteger.ZERO) < 0) {
	    	return null;
	    }
	    
	    if (x == BigInteger.ZERO || x == BigInteger.ONE) {
	        return x;
	    }
	    
	    BigInteger two = BigInteger.valueOf(2L);
	    BigInteger y;
	    
	    for (y = x.divide(two);
	            y.compareTo(x.divide(y)) > 0;
	            y = ((x.divide(y)).add(y)).divide(two));
	    if(y.multiply(y).equals(x))return y;
	    return null;
	} 
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
