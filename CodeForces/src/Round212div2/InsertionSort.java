package Round212div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class InsertionSort {

	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int N = Integer.parseInt(in.readLine());
		int[] arr = new int[N];
		StringTokenizer tk = new StringTokenizer(in.readLine());
		for(int i = 0; i < N; i ++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}
		
		int inv = 0;
		for(int l = 0; l < N; l++) {
			for(int r = l+1; r < N; r++) {
				if(arr[r] < arr[l]) {
					inv++;
				}
			}
		}	
		
		// consider a[i], if go from i->left, how many >< a[i]?
		short[][] countGreater = new short[N][N];
		short[][] countLess = new short[N][N];
		for(int r = 1; r < N; r++) {
			for(int l = r-1; l >= 0; l--) {
				countGreater[r][l] = countGreater[r][l+1];
				countLess[r][l] = countLess[r][l+1];
				if(arr[l] > arr[r]) {
					countGreater[r][l]++;
				} else {
					countLess[r][l]++;
				}
			}
		}
		
		int min = Integer.MAX_VALUE;
		int count = 0;
		for(int l = 0; l < N; l++) {
			// from l->r, how many < a[l] or > a[l]
			int numLess = 0;
			int numGreater = 0;
			for(int r = l+1; r < N; r++) {
				if(arr[r] < arr[l]) {
					int change = numGreater - numLess + countLess[r][l+1] - countGreater[r][l+1] - 1;
					if(change < min) {
						min = change;
						count = 1;
					} else if(change == min) {
						count++;
					}
					numLess++;
				} else {
					numGreater++;
				}
			}
		}
		
		out.println((inv+min) + " " + count);
		out.close();
	}
}
