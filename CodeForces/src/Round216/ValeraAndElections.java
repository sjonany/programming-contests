package Round216;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class ValeraAndElections {
	static InputReader in;
	static PrintWriter out;
	static List<Integer>[] edges;
	static boolean[] prob;
	static int count;
	static List<Integer> ans;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		ans = new ArrayList<Integer>();
		edges = new List[n];
		for(int i=0;i<n;i++)edges[i]= new ArrayList();
		prob = new boolean[n];
		for(int i=0;i<n-1;i++) {
			int a = in.nextInt()-1;
			int b = in.nextInt()-1;
			int t = in.nextInt();
			edges[a].add(b);
			edges[b].add(a);
			if(t==2){
				prob[a] = true;
				prob[b] = true;
			}
		}
		
		dfs(0,-1);
		out.println(ans.size());
		for(int i:ans){
			out.print((i+1) + " ");
		}
		out.close();
	}
	
	static boolean dfs(int cur, int par) {
		boolean hasProbChild = false;
		for(int vo : edges[cur]) {
			if(vo==par)continue;
			hasProbChild |= dfs(vo, cur);
		}
		if(prob[cur] && !hasProbChild)ans.add(cur);
		
		return prob[cur] || hasProbChild;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
