package Round216;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Queue;

public class ValeraAndFools {
	static InputReader in;
	static PrintWriter out;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int k = in.nextInt();
		int[] parr = new int[n];
		
		for(int i=0;i<n;i++)parr[i]=in.nextInt();
		//from the back
		int[][] countProb = new int[n][101];
		int[] counter = new int[101];
		counter[parr[n-1]]++;
		countProb[n-1] = counter.clone();
		for(int i = n-2; i>=0;i--) {
			counter[parr[i]]++;
			countProb[i] = counter.clone();
		}
		
		int[][] dist = new int[n+1][n+1];
		for(int[] a:dist) Arrays.fill(a, -1);
		
		if(n==1){
			out.println(1);
			out.close();
			return;
		}
		dist[0][1] = 0;
		Queue<Node> q = new LinkedList();
		q.add(new Node(0,1));
		while(!q.isEmpty()) {
			Node node = q.poll();
			int victim = node.victim;
			int prevdist = dist[node.victim][node.l];
			int l = node.l;
			if(victim >= n || l >= n){
				continue;
			}
			if(parr[victim] != 0) {
				//if victim shoots & is killed
				if(countProb[l][0] != n-l) {
					int newVic = l+1;
					int newL = newVic+1;
					if(newVic == n) newL = n;
					if(newL <= n) {
						if(dist[newVic][newL] == -1) {
							q.add(new Node(newVic, newL));
							dist[newVic][newL] = prevdist + 1;
						}
					}
				}
							
				//if victim shoots & not killed
				if(countProb[l][100] == 0) {
					int newL = l+1;
					if(newL <= n) {
						if(dist[victim][newL] == -1) {
							q.add(new Node(victim, newL));
							dist[victim][newL] = prevdist + 1;
						}
					}
				}
			}
			
			if(parr[victim] != 100){
				//if victim doesn't shoot, and is killed
				if(countProb[l][0] != n-l) {
					int newVic = l;
					int newL = l+1;
					if(newVic == n) newL = n;
					if(newL <= n) {
						if(dist[newVic][newL] == -1) {
							q.add(new Node(newVic, newL));
							dist[newVic][newL] = prevdist + 1;
						}
					}
				}
				
				//if victim doesn't shoot, and is not killed ->nothing changes
			}
		}
		
		int count = 0;
		for(int i = 0; i <= n; i++) {
			for(int j = 0; j <= n; j++) {
				if(dist[i][j] != -1 && dist[i][j] <= k) {
					count++;
				}
			}
		}
		out.println(count);
		out.close();
	}
	
	static class Node {
		public int victim;
		public int l;
		public Node(int victim, int l) {
			this.victim = victim;
			this.l = l;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
