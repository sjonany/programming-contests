package Round216;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;

/**
in general, when wanting to find segments intersection/subsume/etc.
convert it to a problem that has the form "count those whose left < x1 & other condition"
The condition left < x1 can be imposed by iterating from left to right, and the other condition
can be imposed by using fenwick tree

two sets: source and query
f(x1, x2) = number of segments in reserve whose left >= x1 & left <= x2 and right >= x2
given this, how to answer queries?

if points = x1,x2,..,xk
f(-inf, x1) + 
f(x1+1, x2) + 
...         +
f(xk-1+1,xk)


Count all the f(,) queries

Now... to calculate f(l,r)'s
sort r's in descending order for both 
1. query set
2. source set

if i am processing r = R
fenwick[i] = number of source segments with left <= i, and right >= R
for the segment l = L, 
f(L,R) = fenwick[R] - fenwick[L-1]

then to update, 
while(next updated R >= query R)
	put it in fenwick

 * @author sjonany
 *
 */
public class ValeraAndQueries {
	static InputReader in;
	static PrintWriter out;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
	
		int n = in.nextInt();
		int m = in.nextInt();
		
		Segment[] sourceSegments = new Segment[n];
		for(int i=0;i<n;i++) {
			sourceSegments[i] = new Segment(in.nextInt(), in.nextInt());
		}
		
		List<Integer>[] queries = new List[m];
		for(int i=0;i<m;i++){
			int sz = in.nextInt();
			queries[i] = new ArrayList();
			for(int j=0;j<sz;j++) {
				queries[i].add(in.nextInt());
			}
		}
		
		List<Segment> qs = new ArrayList();
		for(int i = 0; i < m; i++) {
			int l = 0;
			for(int p : queries[i]) {
				Segment s = new Segment(l, p);
				s.id = i;
				l = p + 1;
				qs.add(s);
			}
		}
		
		Collections.sort(qs);
		Arrays.sort(sourceSegments);
		
		int[] ans = new int[m];
		
		// at the start of iteration, when processing q=querySegment[i]  
		// bit[i] = number of sourceSegments with r >= q.r & l = i
		Fenwick1 bit = new Fenwick1(1000005);
		int si = 0;
		for(Segment s : qs) {
			int qr = s.r;
			while(si < sourceSegments.length) {
				Segment source = sourceSegments[si];
				if(source.r >= qr) {
					bit.increment(source.l, 1);
					si++;
				}else{
					break;
				}
			}
			
			ans[s.id] += bit.querySum(s.r) - bit.querySum(s.l-1);
		}
		
		for(int i : ans) {
			out.println(i);
		}
		
		out.close();
	}

	static class Fenwick1 {
		long[] tree;
		public Fenwick1(int n) {
			tree = new long[n+1];
		}
		
		public void increment(int i, int v) {
			while(i < tree.length) {
				tree[i] += v;
				i += (i & -i);
			}
		}
		
		public int querySum(int i) {
			int sum = 0;
			while(i > 0) {
				sum += tree[i];
				i -= (i & -i);
			}
			return sum;
		}
	}
	
	static class Segment implements Comparable<Segment>{
		public int l;
		public int r;
		public int id;
		
		public Segment(int l, int r) {
			this.l = l;
			this.r = r;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + l;
			result = prime * result + r;
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			Segment other = (Segment) obj;
			if (l != other.l)
				return false;
			if (r != other.r)
				return false;
			return true;
		}

		@Override
		public int compareTo(Segment o) {
			return o.r - this.r;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
