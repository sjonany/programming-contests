package Round194;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Chips {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int m = in.nextInt();
		
		boolean[] badR = new boolean[n];
		boolean[] badC = new boolean[n];
		
		badR[0] = true;
		badR[n-1] =true;	
		badC[0] = true;
		badC[n-1] =true;
		for(int i = 0; i < m; i++){
			int r = in.nextInt()-1;
			int c = in.nextInt()-1;
			badR[r] = true;
			badC[c] = true;
		}
		
		Map<Integer, Integer> tally = new HashMap<Integer, Integer>();
		int count = 0;
		for(int i = 0 ; i < n ;i++){
			if(i==n-1-i){
				//middle, can't swap
				if(!badR[i] || !badC[i])
					count++;
				continue;
			}
			int k = Math.min(i, n-1-i);
			Integer prev = tally.get(k);
			if(prev == null)
				prev = 0;
			
			if(!badR[i]){
				prev++;
			}
			
			if(!badC[i]){
				prev++;
			}
			
			tally.put(k, prev);
		}
		
		for(Entry<Integer, Integer> i  : tally.entrySet()){
			int v = i.getValue();
			if(v==1){
				count++;
			}else if(v==2){
				count+=2;
			}else if(v==3){
				count+=3;
			}else if(v==4){
				count+=4;
			}
		}
		out.println(count);
		
		out.close();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
