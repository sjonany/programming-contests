package Round194;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Set;

public class LuckyTicket {
	static Set<String> ans;
	
	static int m;
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int k = in.nextInt();
		m = in.nextInt();
		
		String[] arr = new String[]{"","0","00","000","0000","00000","000000","0000000","00000000"};
		
		ans = new HashSet<String>();
		
		for(int r = 0; r <= 9999; r++){
			//get all numbers that can be formed by playing around with r's digits
			String rStr = "" + r;
			rStr += arr[4-rStr.length()];
			int[] digs = new int[4];
			for(int i =0; i < digs.length;i++)
				digs[i] = rStr.charAt(i)-'0';
			//stick, operation
			Set<Integer> vals = new HashSet<Integer>();
			stick(0, digs, new ArrayList<Integer>(), vals);
			//for each such number n, do k-n. Since r -> n, then k-n and n = k
			for(int n : vals){
				int other = Math.abs(k-n);
				String otherStr = "" + other;
				if(otherStr.length() + rStr.length() > 8) continue;
				String rem = arr[8-otherStr.length()-rStr.length()];
				ans.add(otherStr + rStr + rem);
				ans.add(otherStr + rem + rStr);
				ans.add(rStr + otherStr + rem);
				ans.add(rStr + rem + otherStr);
				ans.add(rem + otherStr + rStr);
				ans.add(rem + rStr + otherStr);
				if(ans.size() > m)break;
			}
			if(ans.size() > m)break;
		}
		
		int i = 0;
		for(String s : ans){
			out.println(s);
			i++;
			if(i == m)break;
		}
		out.close();
	}
	
	static void stick(int toSet, int[] ordered, List<Integer> sticked, Set<Integer> vals){
		if(toSet == ordered.length){
			operation(0, 0, sticked, vals);
		}else{
			//stick to prev
			if(toSet != 0){
				int prevVal = sticked.get(sticked.size()-1);
				int newSticked = prevVal*10 + ordered[toSet];
				sticked.set(sticked.size()-1, newSticked);
				stick(toSet+1, ordered, sticked, vals);
				sticked.set(sticked.size()-1, prevVal);
			}
			
			//don't stick to prev
			sticked.add(ordered[toSet]);
			stick(toSet+1, ordered, sticked, vals);
			sticked.remove(sticked.size()-1);
		}
	}
	
	static void operation(int toSet, int curVal, List<Integer> sticked, Set<Integer> vals){
		if(toSet == sticked.size()){
			vals.add(Math.abs(curVal));
		}else{
			if(toSet == 0){
				operation(toSet+1, sticked.get(toSet), sticked, vals);
			}else{
				//sub
				operation(toSet+1, curVal - sticked.get(toSet), sticked, vals);
				//add
				operation(toSet+1, curVal + sticked.get(toSet), sticked, vals);
				//mul
				operation(toSet+1, curVal * sticked.get(toSet), sticked, vals);				
			}
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
