package Round194;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class CharacteristicsOfRectangles {
	static int[][] mat;
	
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int m = in.nextInt();
		
		mat = new int[n][m];
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for(int r = 0 ; r < n; r ++){
			for(int c = 0 ;c < m; c++){
				mat[r][c] = in.nextInt();
				min = Math.min(min, mat[r][c]);
				max = Math.max(max, mat[r][c]);
			}
		}
		
		int lo=min;
		int hi=max;
		
		while(lo < hi){
			int mid = lo + (hi-lo+1)/2;
			if(!check(mid)){
				hi = mid-1;
			}else{
				lo = mid;
			}
		}
		
		out.println(lo);
		out.close();
	}
	
	//the q is now just given TF mat, any rectangle corner?
	static boolean check(int x){
		//have I seen col-i, col-j pairs for any row?
		boolean[][] seen = new boolean[mat[0].length][mat[0].length];
		//each item will either add a new pair, or we have found our answer. 
		//there are only n^2 such pairs
		
		int[] validCols = new int[mat[0].length];
		
		for(int r = 0; r < mat.length; r++){
			int validColSize = 0;
			for(int c = 0; c < mat[0].length; c++){
				if(mat[r][c] >= x){
					validCols[validColSize++] = c;
					for(int i = 0; i < validColSize-1; i++){
						if(!seen[validCols[i]][c]){
							seen[validCols[i]][c] = true;
						}else{
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
