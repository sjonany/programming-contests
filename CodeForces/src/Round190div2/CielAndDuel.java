package Round190div2;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;

public class CielAndDuel {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int m = in.nextInt();
		
		List<Integer> atk = new ArrayList<Integer>();
		List<Integer> def = new ArrayList<Integer>();
		
		for(int i = 0 ; i < n; i++){
			String type = in.nextToken();
			if(type.equals("ATK")){
				atk.add(in.nextInt());
			}else{
				def.add(in.nextInt());
			}
		}
		
		int[] ciel = new int[m];
		for(int i = 0; i < ciel.length; i++){
			ciel[i] = in.nextInt();
		}
		
		Collections.sort(atk);
		Collections.sort(def);
		Arrays.sort(ciel);
		
		//if don't want to destroy all
		int dmg1 = 0;
		int cielPos = ciel.length-1;
		int enPos = 0;
		while(cielPos >= 0 && enPos < atk.size() && atk.get(enPos) < ciel[cielPos]){
			dmg1 += ciel[cielPos] - atk.get(enPos);
			enPos++;
			cielPos--;
		}
		
		//if destroy all
		int dmg2 = 0;
		boolean[] hasAttacked = new boolean[ciel.length];
		int defIn = 0;
		int cielIn = 0;
		while(defIn != def.size()){
			while(cielIn != ciel.length && ciel[cielIn] <= def.get(defIn)){
				cielIn++;
			}
			if(cielIn == ciel.length){
				break;
			}else{
				hasAttacked[cielIn] = true;
				cielIn++;
			}
			defIn++;
		}
		
		if(defIn == def.size()){
			List<Integer> remCiel = new ArrayList<Integer>();
			for(int i = 0; i < hasAttacked.length; i++){
				if(!hasAttacked[i])
					remCiel.add(ciel[i]);
			}
			boolean[] hasAttacked2 = new boolean[remCiel.size()];
			int atkIn = 0;
			cielIn = 0;
			while(atkIn != atk.size()){
				while(cielIn != remCiel.size() && remCiel.get(cielIn) < atk.get(atkIn)){
					cielIn++;
				}
				if(cielIn == remCiel.size()){
					break;
				}else{
					dmg2 += remCiel.get(cielIn) - atk.get(atkIn);
					hasAttacked2[cielIn] = true;
					cielIn++;
				}
				atkIn++;
			}
			
			if(atkIn == atk.size()){
				for(int i = 0; i < hasAttacked2.length; i++){
					if(!hasAttacked2[i]){
						dmg2 += remCiel.get(i);
					}
				}
			}
		}
		
		out.println(Math.max(dmg1, dmg2));
		out.close();
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}


}
