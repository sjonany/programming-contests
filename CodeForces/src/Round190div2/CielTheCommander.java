package Round190div2;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

public class CielTheCommander {
	static List<Integer>[] slowEdges;
	static int[][] effEdges;
	static char[] label;
	static int[] subtreeSize;
	static int count = 0;
	static int n;
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		n = in.nextInt();
		subtreeSize = new int[n];
		slowEdges = (List<Integer>[]) Array.newInstance(List.class, n);
		label = new char[n];
		
		for(int i = 0; i < n; i++){
			slowEdges[i] = new ArrayList<Integer>();
		}
		for(int i = 0; i < n-1; i++){
			int a = in.nextInt()-1;
			int b = in.nextInt()-1;
			slowEdges[a].add(b);
			slowEdges[b].add(a);
		}
		
		effEdges = new int[n][];
		for(int i = 0; i < n ; i++ ){
			effEdges[i] = new int[slowEdges[i].size()];
			for(int j = 0; j < slowEdges[i].size(); j++ ){
				effEdges[i][j] = slowEdges[i].get(j);
			}	
		}
		
		label(0, 'A');
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0 ; i < n; i++){
			sb.append(label[i] + " ");
		}
		out.println(sb);
		
		out.close();
	}
	
	static void label(int v, char curLabel){
		centroid = -1;
		getSubtreeSize(v, -1);
		findCentroid(v, -1, subtreeSize[v]);
		label[centroid] = curLabel;
		for(int child : effEdges[centroid]){
			if(label[child] == 0){
				label(child, (char)(curLabel+1));
			}
		}
	}
	
	static void getSubtreeSize(int v, int parent){
		subtreeSize[v] = 1;
		for(int child : effEdges[v]){
			if(child != parent && label[child] == 0){
				getSubtreeSize(child, v);
				subtreeSize[v] += subtreeSize[child];
			}
		}
	}
	
	//http://www.stringology.org/event/1999/p5.html
	//proof that centroid is the vertex with N(v) <= N/2
	static int centroid;
	static void findCentroid(int v, int parent, int totalNodes){
		if(centroid != -1){
			return;
		}
		for(int child : effEdges[v]){
			if(child != parent && label[child] == 0){
				findCentroid(child, v, totalNodes);
			}
		}
		if(subtreeSize[v] >= totalNodes/2 && centroid == -1){
			centroid = v;
		}
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}


}