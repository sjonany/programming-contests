package Round190div2;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class CielAndRobot {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int tx = in.nextInt();
		int ty = in.nextInt();
		
		String command = in.nextToken();
		
		int X = 0;
		int Y = 1;
		//after running the ith' command, what's my position
		int[][] coords = new int[command.length()+1][2];
		int curX = 0;
		int curY = 0;
		for(int i = 0; i < command.length(); i++){
			char c = command.charAt(i);
			if(c == 'U'){
				curY++;
			}else if(c == 'R'){
				curX++;
			}else if(c == 'D'){
				curY--;
			}else if(c == 'L'){
				curX--;
			}
			
			coords[i][X] = curX;
			coords[i][Y] = curY;
		}
		
		for(int i = 0; i < command.length()+1; i++){
			int mulX = tx - coords[i][X];
			int mulY = ty - coords[i][Y];
			int vecX = coords[command.length()-1][X];
			int vecY = coords[command.length()-1][Y];
			if((long)mulX * vecX < 0 || (long)mulY * vecY < 0){
				continue;
			}
			if(mulX == 0){
				if(vecX != 0){
					//mult = 0
					if(mulY == 0){
						out.println("Yes");
						out.close();
						return;
					}else{
						continue;
					}
				}else{
					//multiplier = anything
					if(mulY == 0){
						out.println("Yes");
						out.close();
						return;
					}else{
						if(vecY == 0){
							continue;
						}else{
							if(mulY % vecY == 0){
								out.println("Yes");
								out.close();
								return;
							}else{
								continue;
							}
						}
					}
				}
			}else{
				if(vecX == 0){
					continue;
				}else{
					if(mulX % vecX == 0){
						int mul = mulX / vecX;
						if((long)mul * vecY == mulY){
							out.println("Yes");
							out.close();
							return;
						}else{
							continue;
						}
					}else{
						continue;
					}
				}
			}
		}
		out.println("No");
		out.close();
	}
	
	static class Point{
		public int x;
		public int y;	
		
		public Point(int x, int y){
			this.x = x;
			this.y = y;
		}
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}


}
