package Round176div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class IQTest {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int[][] a = new int[4][4];
		for(int i=0;i<4;i++){
			String s = sc.nextToken();
			for(int j=0;j<4;j++){
				a[i][j] = (int)s.charAt(j);
			}
		}
		
		int[] k = new int[1000];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				k[a[i][j]]++;
				k[a[i][j+1]]++;
				k[a[i+1][j]]++;
				k[a[i+1][j+1]]++;
				
				if(Math.max(k[(int)'#'], k[(int)'.']) >= 3){
					System.out.println("YES");
					return;
				}
				k[(int)'#'] = 0;
				k[(int)'.'] = 0;
			}
		}
		
		System.out.println("NO");
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
