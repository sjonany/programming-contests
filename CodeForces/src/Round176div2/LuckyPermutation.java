package Round176div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class LuckyPermutation {
	/*
	 index , val
	 i, f(i)
	 f(i),n-i+1
	 n-i+1,n-f(i)+1
	 n-f(i)+1,i
	 
	 if n-i+1 = i -> i=(n+1)/2, cycle length = 1
	 if n-i+1 != i -> f(i)!=n-f(i)+1 to be a permutation too, cycle length = 4, must
	 
	 total n mod 4 = 1 or 0, then
	 choose f(i) = i+1 because it's nice to see how the array will be filled up. 
	 Maybe it's possible to choose a different f(i) too?
	 */
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		if(n%4 <= 1){
			int[] a = new int[n+1];
			
			for(int i=1;i<=n/2;i+=2){
				a[i] = i+1;
				a[i+1] = n-i+1;
				a[n-i+1] = n-i;
				a[n-i] = i;
			}
			
			if(n%4 == 1){
				a[(n+1)/2] = (n+1)/2;
			}
			
			for(int i=1;i<=n;i++){
				System.out.print(a[i] + " ");
			}
		}else{
			System.out.println(-1);
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
