package Round168div2;

import java.util.Scanner;


//timeout at test 20
public class Convex {
	static char[][] a;
	static int m;
	static int n;
	static int numDestroy = 0;
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		m = s.nextInt();
		n = s.nextInt();
		
		a= new char[m][n];
		for(int i=0; i< m;i ++){
			String line = s.next();
			for(int j=0; j<n; j++){
				a[i][j] = line.charAt(j);
			}
		}

		int blackCount = 0;
		for(int i=0; i< m;i ++){
			for(int j=0; j<n; j++){
				if(a[i][j] == 'B'){
					blackCount++;
				}
			}
		}


		char[][] ori = copy(a);
		for(int i=0; i< m;i ++){
			for(int j=0; j<n; j++){
				if(ori[i][j] == 'B'){
					for(int dir=0; dir<=3; dir++)
						destroy(copy(ori), i,j,1,dir);
					if(blackCount != numDestroy){
						
						//System.out.println("no at " + i+ " " + j + " count = " + numDestroy);
						System.out.println("NO");
						return;
					}
					numDestroy = 0;
					a = copy(ori);
				}
			}
		}
		
		System.out.println("YES");
		
		/*
		for(int i=0; i< m;i ++){
			for(int j=0; j<n; j++){
				if(a[i][j] == 'B'){
					if(isSolve(a, i,j, blackCount))
						System.out.println("YES");
					else{
						System.out.println("NO");
					}
					return;
				}
			}
		}*/
	}
	
	private static void destroy(char[][] mat, int r, int c, int cTurn, int dir){
		//System.out.println(r + " " + c + " " + cTurn);
		//pm(mat);
		if(r >= 0 && r <= m-1 && c >= 0 && c<=n-1 && cTurn >= 0){
			if(a[r][c] == 'B'){
				numDestroy++;
				a[r][c] = 'W';
				//System.out.println("Destroy " + r +" " + c);
			}
			if(mat[r][c] == 'W'){
				return;
			}else{
				mat[r][c] = 'W';
			}
		}else{
			return;
		}
		
		char[][] mC;
		if(dir == 0){	
			mC = copy(mat);
			destroy(mC,r-1,c,cTurn, 0);
		}else{
			mC = copy(mat);
			destroy(mC,r-1,c,cTurn-1, 0);
		}
		
		if(dir == 1){	
			mC = copy(mat);
			destroy(mC,r,c+1,cTurn, 1);
		}else{
			mC = copy(mat);
			destroy(mC,r,c+1,cTurn-1, 1);
		}
		
		
		if(dir == 2){	
			mC = copy(mat);
			destroy(mC,r+1,c,cTurn, 2);
		}else{
			mC = copy(mat);
			destroy(mC,r+1,c,cTurn-1, 2);
		}
		
		
		if(dir == 3){	
			mC = copy(mat);
			destroy(mC,r,c-1,cTurn, 3);
		}else{
			mC = copy(mat);
			destroy(mC,r,c-1,cTurn-1, 3);
		}
	}
	
	private static void pm(char[][] matrix){
		for(char i=0;i<m; i++){
			for(char j=0; j<n; j++){
				System.out.print(matrix[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	//can solve if start at r and c?
	private static boolean isSolve(char[][] mat, int r, int c, int blackCount){
		if(mat[r][c] == 'W'){
			return blackCount == 0;
		}
		
		if(mat[r][c] == 'B' && blackCount == 1){
			return true;
		}

		if(r-1 >=0 && mat[r-1][c] == 'B'){
			char[][] copy = copy(mat);
			copy[r][c] = 'W';
			if(isSolve(copy, r-1, c, blackCount-1))
				return true;
		}

		if(r+1 <= m-1 && mat[r+1][c] == 'B'){
			char[][] copy = copy(mat);
			copy[r][c] = 'W';
			if(isSolve(copy, r+1, c, blackCount-1))
				return true;
		}

		if(c+1 <= n-1 && mat[r][c+1] == 'B'){
			char[][] copy = copy(mat);
			copy[r][c] = 'W';
			if(isSolve(copy, r, c+1, blackCount-1))
				return true;
		}

		if(c-1 >= 0 && mat[r][c-1] == 'B'){
			char[][] copy = copy(mat);
			copy[r][c] = 'W';
			if(isSolve(copy, r, c-1, blackCount-1))
				return true;
		}

		return false;
	}

	private static char[][] copy(char[][] mat){
		char[][]copy = new char[m][n];
		for(char i=0;i<m; i++){
			for(char j=0; j<n; j++){
				copy[i][j]  = mat[i][j];
			}
		}
		return copy;
	}
}
