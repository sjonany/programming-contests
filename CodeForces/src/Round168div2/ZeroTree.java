package Round168div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

//tried to understand ping's solution
public class ZeroTree {
	static int[] val;
	static int n;
	//key1 = min of( 0 =plus or 1=minus) moves to make subtree rooted there 0
	//key2 = which vertex? 
	static long[][] dp;
	static HashMap<Integer, List<Integer>> edges;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		edges = new HashMap<Integer, List<Integer>>();
		
		for(int i=1; i<=n-1; i++){
			int v1 = sc.nextInt();
			int v2 = sc.nextInt();
			
			List<Integer> out1 = edges.get(v1);
			if(out1 == null){
				out1 = new ArrayList<Integer>();
				edges.put(v1, out1);
			}
			out1.add(v2);
			
			List<Integer> out2 = edges.get(v2);
			if(out2 == null){
				out2 = new ArrayList<Integer>();
				edges.put(v2, out2);
			}
			out2.add(v1);
		}
		
		val = new int[n+1];
		
		for(int i=1; i<n+1;i++){
			val[i] = sc.nextInt();
		}
		
		dp = new long[2][n+1];
		dp[0] = new long[n+1];
		dp[1] = new long[n+1];
		for(int i=0; i<n+1 ; i++){
			dp[0][i] = -1;
			dp[1][i] = -1;
		}
		
		fillDp(0, 1);
		System.out.println(dp[0][1] + dp[1][1]);
	}
	
	private static void fillDp(int parent, int vi){
		List<Integer> out = edges.get(vi);
		long maxPlus = 0;
		long maxMinus = 0;
		for(int vout: out){
			if(vout != parent){
				fillDp(vi, vout);
				long curPlus = dp[0][vout];
				long curMin = dp[1][vout];
				
				if(curPlus > maxPlus){
					maxPlus = curPlus;
				}
				
				if(curMin > maxMinus){
					maxMinus = curMin;
				}
			}
		}
		
		long remain = maxPlus - maxMinus + val[vi];

		dp[0][vi] = maxPlus;
		dp[1][vi] = maxMinus;
		if(remain < 0){
			dp[0][vi] += -remain;
		}else if(remain > 0){
			dp[1][vi] += remain;
		}
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}