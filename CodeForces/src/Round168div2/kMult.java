package Round168div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//obviously time out haha
//soln: sort ascending, keep adding element, 
//check if element%k !=0 && element/k is in set, then don't add element
public class kMult {
	static int n;
	static int k;
	public static void main(String[] args){
		FastScanner s = new FastScanner();
		n = s.nextInt();
		k = s.nextInt();
		
		int[] a = new int[n];
		for(int i=0; i<n; i++){
			a[i] = s.nextInt();
		}
		
		boolean []b = new boolean[n];
		for(int i=0; i<n; i++)b[i] = true;
		System.out.println(maxSub(a, b));
	}
	
	private static int maxSub(int[]a, boolean[] b){
		boolean hasMove = false;
		for(int i=0; i<b.length;i++){
			//still pickable
			if(b[i]){
				hasMove = true;
				break;
			}
		}
		if(!hasMove){
			return 0;
		}
		
		int maxSize = -1;
		for(int i=0; i<a.length; i++){
			if(b[i]){
				boolean[] copyArr = copy(b);
				for(int j=0; j< a.length; j++){
					if(copyArr[j] && j != i && (a[i] * k == a[j] || a[j] * k == a[i])){
						copyArr[j] = false;
					}
				}
				copyArr[i] = false;
				//System.out.println("If pick " + i + "," + a[i] + " " + Arrays.toString(copyArr));
				//pick this element
				int val = maxSub(a, copyArr) + 1;
				if(maxSize == -1 || maxSize < val){
					maxSize = val;
				} 
			}
		}
		return maxSize;
	}
	
	private static boolean[] copy(boolean [] a){
		boolean[] copy = new boolean[a.length];
		for(int i=0; i<a.length; i++){
			copy[i] = a[i];
		}
		return copy;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
