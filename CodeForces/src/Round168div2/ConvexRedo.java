package Round168div2;

import java.util.Scanner;

public class ConvexRedo {
	static char[][] a;
	static int m;
	static int n;
	static int numDestroy = 0;
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		m = s.nextInt();
		n = s.nextInt();
		
		a= new char[m][n];
		for(int i=0; i< m;i ++){
			String line = s.next();
			for(int j=0; j<n; j++){
				a[i][j] = line.charAt(j);
			}
		}
		
		for(int r=0; r<m; r++){
			for(int c=0; c<n; c++){
				if(a[r][c] == 'B'){
					//incomplete line
					for(int cc = c+1; cc<n; cc++){
						if(a[r][cc] == 'B' && !isConnected(r,c,r,cc)){
							System.out.println("NO");//+ "-  " + r + "," + c + " is not connected to " + r + "," + cc);
							return;
						}
					}
					
					//complete block
					for(int rr = r+1; rr < m; rr++){
						for(int cc =0; cc<n; cc++){
							if(a[rr][cc] == 'B' && !isConnected(r,c, rr, cc)){
								System.out.println("NO");// + "-  " + r + "," + c + " is not connected to " + rr + "," + cc);
								return;
							}
						}
					}
				}
			}
		}
		
		System.out.println("YES");
	}
	
	private static boolean isConnected(int r1, int c1, int r2, int c2){
		//single line case - must be done without turning
		//because if turn, has to be >=3
		if(r1 == r2){
			int start = Math.min(c1, c2);
			int end = Math.max(c1, c2);
			for(int c = start; c<=end; c++){
				if(a[r1][c] != 'B')
					return false;
			}
			
			return true;
		}
		
		if(c1 == c2){
			int start = Math.min(r1, r2);
			int end = Math.max(r1, r2);
			for(int r = start; r<=end; r++){
				if(a[r][c1] != 'B')
					return false;
			}
			
			return true;
		}
		
		//L-turn case, two possible cases
		int meetc1 = c1;
		int meetr1 = r2;
		
		if(isConnected(r1,c1,meetr1,meetc1) && isConnected(r2,c2,meetr1, meetc1)){
			return true;
		}
		
		int meetc2 = c2;
		int meetr2 = r1;

		if(isConnected(r1,c1,meetr2,meetc2) && isConnected(r2,c2,meetr2, meetc2)){
			return true;
		}
		
		return false;
	}
}
