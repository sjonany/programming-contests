package Round168div2;

import java.util.Scanner;

public class LightsOut {
	static int[][] m = new int[3][3];
	static int[][] a = new int[3][3];
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		for(int i=0; i< 3;i ++){
			m[i] = new int[3];
			a[i] = new int[3];
			for(int j=0; j<3; j++){
				m[i][j] = s.nextInt();
				a[i][j] = 1;
			}
		}
		
		for(int i=0; i< 3;i ++){
			for(int j=0; j<3; j++){
				a[i][j] += m[i][j];
				if(i-1>=0){
					a[i-1][j] += m[i][j];
				}
				if(i+1<=2){
					a[i+1][j] += m[i][j];
				}
				
				if(j-1>=0){
					a[i][j-1] += m[i][j];
				}
				
				if(j+1<=2){
					a[i][j+1] += m[i][j];
				}
			}
		}
		
		for(int i=0; i< 3;i ++){
			for(int j=0; j<3; j++){
				System.out.print(a[i][j]%2);
			}
			System.out.println();
		}
	}
}
