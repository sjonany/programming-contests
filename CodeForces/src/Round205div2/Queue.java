package Round205div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Queue {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		List<Integer> pos = new ArrayList<Integer>();
		int index = 0;
		while(true) {
			int c = in.read();
			if(c == -1){
				break;
			}
			
			if(c == 'F'){
				pos.add(index);
			}
			index++;
		}
		
		if(pos.size() == 0){
			System.out.println(0);
			return;
		}
		
		int[] wait = new int[pos.size()];
		wait[0] = 0;
		int max = pos.get(0);
		
		for(int i = 1; i < pos.size(); i++) {
			int distToTarget = pos.get(i) - i;
			wait[i] = Math.max(0, (pos.get(i-1) + wait[i-1]) - pos.get(i) + 2);
			if(distToTarget == 0) {
				wait[i] = 0;
			}
			max = Math.max(max, distToTarget + wait[i]);
		}
		System.out.println(max);
	}
}
