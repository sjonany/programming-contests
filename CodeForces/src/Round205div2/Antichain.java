package Round205div2;

import java.util.Scanner;

public class Antichain {
	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		int count = 0;
		boolean[] used = new boolean[s.length()];
		int[] arr = new int[s.length()];
		int start = 0;
		for(int i = 0; i < s.length()-1; i++) {
			if(s.charAt(i) != s.charAt(i+1)){
				start = i +1;
				break;
			}
		}
		for(int i = 0; i < s.length(); i++){
			arr[i] = s.charAt((start + i) % s.length());
		}

		boolean existLong = false;
		for(int i = 0; i < s.length(); i++){
			if(used[i]){
				continue;
			}

			int end = i;
			while(true){
				if(end+1 < arr.length && arr[end+1] == arr[i])
					end++;
				else
					break;
			}

			if(end - i + 1 >= 2) {
				existLong = true;
				count++;
				for(int k = i; k <= end + 1; k++) {
					used[k % arr.length] = true;
				}
			}
		}

		if(!existLong) {
			System.out.println(arr.length / 2);	
			return;
		}	

		// exist a used[i]
		// used[i], then !used[i+1]
		int afterUsed = -1;
		for(int i = 0; i < arr.length; i++){
			if(used[i] && !used[(i+1) % arr.length]){
				afterUsed = (i+1) % arr.length;
				break;
			}
		}
		
		if(afterUsed != -1) {
			//alternating chains
			for(int i = 0; i < s.length(); i++) {
				if(!used[(afterUsed+i) % arr.length]){
					int len = 1;
					while(!used[(afterUsed+i+len - 1) % arr.length]) {
						used[(afterUsed+i+len-1) % arr.length] = true;
						len++;
					}
					len--;
					count += len / 2 + len % 2;	
				}
			}
		}

		System.out.println(count);
	}
}
