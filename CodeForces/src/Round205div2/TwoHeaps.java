package Round205div2;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Set;

public class TwoHeaps {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt()*2;
		int[] count = new int[100];
		int[] arr = new int[n];
		for(int i = 0; i < n; i++){
			arr[i] = in.nextInt();
			count[arr[i]]++;
		}
		
		Set<Integer> singles = new HashSet<Integer>();
		Set<Integer> dups = new HashSet<Integer>();
		for(int i = 0; i< 100; i++) {
			if(count[i] == 0) continue;
			if(count[i] == 1) singles.add(i);
			else dups.add(i);
		}
		
		int[] assignments = new int[n];
		boolean[] hasDuped = new boolean[100];
		int numSingleForOne = singles.size() / 2;
		int numSingleForTwo = singles.size() - numSingleForOne;
		int numSingleLeft = numSingleForOne;
		
		int countOne = 0;
		for(int i = 0; i < n; i++) {
			int it = arr[i];
			//dups
			if(dups.contains(it) && !hasDuped[it]){
				hasDuped[it] = true;
				assignments[i] = 1;
				countOne++;
				count[it]--;
			}
			
			if(singles.contains(it) && numSingleLeft > 0){
				numSingleLeft--;
				assignments[i] = 1;
				countOne++;
			}
		}
		
		for(int i = 0; i < n; i++){
			if(countOne >= n/2){
				break;
			}
			
			if(assignments[i] != 1){
				if(dups.contains(arr[i]) && count[arr[i]] > 1){
					assignments[i] = 1;
					count[arr[i]]--;
					countOne++;
				}
			}
		}
		
		long ans = (dups.size() +  numSingleForOne) * (dups.size() +  numSingleForTwo);
		out.println(ans);
		for(int i = 0; i < n; i++){
			out.print((assignments[i] == 1 ? 1 : 2) + " ");
		}
		
		out.close();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
