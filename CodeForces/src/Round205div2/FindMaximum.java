package Round205div2;

import java.io.IOException;
import java.io.InputStream;
import java.util.InputMismatchException;

public class FindMaximum {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		int n = in.nextInt();
		int[] arr = new int[n];
		for(int i = 0; i < n; i++){
			arr[i] = in.nextInt();
		}
		
		int[] m = new int[n];
		String s = in.nextToken();
		for(int i = 0; i < n; i++){
			m[i] = s.charAt(i) -'0';
		}
		
		//sum if all one's at the prefix
		long[] prefSum = new long[n];
		
		prefSum[0] = arr[0];
		for(int i = 1; i < n; i++){
			prefSum[i] = prefSum[i-1] + arr[i];
		}
		
		//suffix sum based on m[i] = 1
		long[] sufSumBit = new long[n+1];
		sufSumBit[n-1] = m[n-1] == 1 ? arr[n-1] : 0;
		for(int i = n-2; i >= 0; i--){
			sufSumBit[i] = sufSumBit[i+1];
			if(m[i] == 1){
				sufSumBit[i] += arr[i];
			}
		}
	
		//which bit is the first lower?, greedily make the rest 1's
		long max = sufSumBit[0];
		for(int lobit = 0; lobit < n; lobit++){
			if(m[lobit] == 0){
				continue;
			}
			
			if(lobit == 0)
				max = Math.max(max, sufSumBit[lobit+1]);
			else
				max = Math.max(max, prefSum[lobit-1] + sufSumBit[lobit+1]);
		}
		System.out.println(max);
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
