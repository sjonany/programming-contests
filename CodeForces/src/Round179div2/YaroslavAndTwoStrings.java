package Round179div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class YaroslavAndTwoStrings {
	static long MOD =  1000000007;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		String a = sc.nextToken();
		String b = sc.nextToken();
		
		int ADOM = 0;
		int BDOM =1;
		int EQ = 2;
		int INC = 3;
		//how many ways to make incomparable if end state at j
		long dp[][] = new long[n][4];
		
		char a0 = a.charAt(0);
		char b0 = b.charAt(0);
		
		if(a0 == '?' && b0 == '?'){
			dp[0][ADOM] = 45;
			dp[0][BDOM] = 45;
			dp[0][EQ] = 10;
		}
		else if(a0 == '?' && b0 != '?'){
			int bVal = b0-'0';
			dp[0][ADOM] = 9-bVal;
			dp[0][BDOM] = bVal;
			dp[0][EQ] = 1;
		}else if(b0 == '?' && a0 != '?'){
			int aVal = a0-'0';
			dp[0][ADOM] = aVal;
			dp[0][BDOM] = 9-aVal;
			dp[0][EQ] = 1;
		}
		else{
			//both numbers
			int aVal = a0-'0';
			int bVal = b0-'0';
			if(aVal > bVal){
				dp[0][ADOM] = 1;
			}else if(aVal < bVal){
				dp[0][BDOM] = 1;
			}else{
				dp[0][EQ] = 1;
			}
		}
		
		for(int i=1;i<n;i++){
			char ai = a.charAt(i);
			char bi = b.charAt(i);

			//ways to make b>a, a>b or a==b
			long ba;
			long ab;
			long aeqb;
			
			if(ai == '?' && bi == '?'){
				ba = 45;
				ab = 45;
				aeqb = 10;
			}
			else if(ai == '?' && bi != '?'){
				int bVal = bi-'0';
				ba = bVal;
				ab = 9-bVal;
				aeqb = 1;
			}else if(ai != '?' && bi == '?'){
				int aVal = ai-'0';
				ba = 9-aVal;
				ab = aVal;
				aeqb = 1;
			}
			else{
				//both numbers
				int aVal = a.charAt(i)-'0';
				int bVal = b.charAt(i)-'0';
				if(aVal > bVal){
					ab = 1;
					ba = 0;
					aeqb = 0;
				}else if(aVal < bVal){
					ab =0;
					ba = 1;
					aeqb = 0;
				}else{
					ab = 0;
					ba = 0;
					aeqb = 1;
				}
			}
			
			dp[i][ADOM] = sum(new long[]{(dp[i-1][ADOM] * (ab+aeqb)) % MOD, 
					(dp[i-1][EQ] * ab) % MOD});
			dp[i][BDOM] = sum(new long[]{(dp[i-1][BDOM] * (ba+aeqb)) % MOD, 
					(dp[i-1][EQ] * ba) % MOD});
			dp[i][EQ] = (dp[i-1][EQ] * aeqb) % MOD;
			dp[i][INC] = sum(new long[]{
					(dp[i-1][ADOM] * ba) %MOD,
					(dp[i-1][BDOM] * ab) %MOD,
					(dp[i-1][INC] * (ab+ba+aeqb))%MOD
			});
		}
		/*
		for(int i=0;i<n;i++){

			System.out.println("i=" + i);
			System.out.println("ADOM=" + dp[i][ADOM]);
			System.out.println("BDOM=" +  dp[i][BDOM]);
			System.out.println("EQ=" +  dp[i][EQ]);
			System.out.println("INC=" +  dp[i][INC]);
			System.out.println();
		}*/
		System.out.println(dp[n-1][INC]);
	}
	
	public static long sum(long[] arr){
		long res = 0;
		for(long a : arr){
			res = (res + a) % MOD;
		}
		return res;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



