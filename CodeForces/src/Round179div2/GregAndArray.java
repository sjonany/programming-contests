package Round179div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class GregAndArray {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int m = sc.nextInt();
		int k = sc.nextInt();
		
		int[] oriArray = new int[n];
		for(int i=0;i<n;i++){
			oriArray[i] = sc.nextInt();
		}
	
		//l r d
		int[][] ops = new int[m][3];
		for(int i=0;i<m;i++){
			ops[i][0] = sc.nextInt()-1;
			ops[i][1] = sc.nextInt()-1;
			ops[i][2] = sc.nextInt();
		}
		
		int[][] queries = new int[k][2];
		for(int i=0;i<k;i++){
			queries[i][0] = sc.nextInt()-1;
			queries[i][1] = sc.nextInt()-1;
		}

		//how many times query k is executed
		int[] opsCount = new int[m];
		for(int qi = 0; qi<k;qi++){
			opsCount[queries[qi][0]]++;
			if(queries[qi][1]+1 < m)
				opsCount[queries[qi][1]+1]--;
		}
		int[] opsFCount = new int[m];
		int curFCount = 0;
		for(int qi =0;qi<m;qi++){
			curFCount += opsCount[qi];
			opsFCount[qi] = curFCount;
		};
		
		long[] arrCount = new long[n];
		for(int i = 0; i<m;i++){
			arrCount[ops[i][0]] += (long)opsFCount[i] * ops[i][2];
			if(ops[i][1]+1 < n)
				arrCount[ops[i][1]+1] -= (long)opsFCount[i] * ops[i][2];
		}
		
		long curCount = 0;
		for(int i=0; i<n;i++){
			curCount += arrCount[i];
			System.out.print((curCount+oriArray[i]) + " ");
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



