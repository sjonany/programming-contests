package MemSQLStartCup2;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class Banana {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		String s = in.nextToken();
		int n = in.nextInt();
		int[] count = new int[26];
		
		boolean[] appear = new boolean[26];
		for(int i = 0 ; i < s.length(); i++){
			count[s.charAt(i) - 'a']++;
			appear[s.charAt(i) - 'a'] = true;
		}
		int countApp = 0;
		for(int i = 0; i < appear.length;i++){
			if(appear[i]) 
				countApp++;
		}
		
		if(countApp > n){
			out.println(-1);
		}else{
			for(int numSheet = 1; numSheet <= 1000; numSheet++){
				int req = 0;
				int[] counter = new int[26];
				for(int ch = 0 ; ch < 26; ch++){
					int eachSheet = count[ch] / numSheet;
					if(eachSheet * numSheet < count[ch])
						eachSheet++;
					counter[ch] = eachSheet;
					req += eachSheet;
				}
				if(req <= n){
					out.println(numSheet);
					StringBuilder sb = new StringBuilder();
					for(int ch = 0 ; ch< 26; ch++){
						for(int i=  0; i < counter[ch]; i++){
							sb.append((char)('a'+ch));
						}
					}
					while(sb.length() < n)
					{
						sb.append('a');
					}
					
					out.println(sb.toString());
					out.close();
					return;
				}
			}
		}
		
		out.close();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
