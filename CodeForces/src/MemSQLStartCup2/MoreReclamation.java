package MemSQLStartCup2;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MoreReclamation {
	static Map<State, Integer> sg;

	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		int r = in.nextInt();
		int n = in.nextInt();

		List<State> states = new ArrayList<State>();
		boolean[][] reclaimed = new boolean[r+1][2];
		for(int i = 0; i < n; i++){
			int ro = in.nextInt()-1;
			int co = in.nextInt()-1;
			reclaimed[ro][co] = true;
			for(int rr = ro-1; rr <= ro+1; rr++){
				if(rr < 0 || rr >= r) continue;
				reclaimed[rr][1-co] = true;
			}
		}

		reclaimed[r][0] = true;
		reclaimed[r][1] = true;
		
		boolean isState = true;
		int prevRow = 0;

		if(reclaimed[0][0] && reclaimed[0][1]){
			isState = false;
		}
		
		for(int rr = 0; rr < r+1 ; rr++){
			if(reclaimed[rr][0] && reclaimed[rr][1]){
				//time to cut
				if(isState){
					//found a new state
					states.add(new State(rr-prevRow, reclaimed[rr-1][0], reclaimed[rr-1][1],reclaimed[prevRow][0], reclaimed[prevRow][1]));
					isState = false;
				}else{
					//have been cutting
				}
			}else{
				//keep building state
				if(isState){

				}else{
					//start of new state
					prevRow = rr;
					isState = true;
				}
			}
		}
		
		/*
		System.out.println("reclaimed = ");
		p(reclaimed);
		
		for(int i = 0; i < states.size(); i++){
			System.out.println(i);
			p(states.get(i));
		}*/
		
		sg = new HashMap<State, Integer>();
		sg.put(new State(1, true, true, true, true), 0);
		int mySg = 0;
		for(State state : states){
			mySg ^= getSg(state);
		}
		
		if(mySg != 0){
			out.println("WIN");
		}else{
			out.println("LOSE");
		}
		out.close();
	}
	
	static void p(State state){
		boolean[][] arr = new boolean[state.length][2];
		arr[0][0] = state.topLeftMark;
		arr[0][1] = state.topRightMark;
		arr[arr.length-1][0] = state.botLeftMark;
		arr[arr.length-1][1] = state.botRightMark;
		p(arr);
	}
	
	static void p(boolean[][] arr){
		for(int r = 0 ; r < arr.length; r++){
			for(int c = 0 ; c< arr[0].length; c++){
				System.out.print(arr[r][c] ? 'T' : 'F');
			}
			System.out.println();
		}
		System.out.println();
	}
	
	static int getSg(State state){
		if(sg.containsKey(state)){
			return sg.get(state);
		}else{
			Set<Integer> childSg = new HashSet<Integer>();
			boolean[][] reclaimed = new boolean[state.length][2];
			reclaimed[reclaimed.length-1][0] = state.botLeftMark;
			reclaimed[reclaimed.length-1][1] = state.botRightMark;
			reclaimed[0][0] = state.topLeftMark;
			reclaimed[0][1] = state.topRightMark;
			
			boolean[][] newReclaimed = new boolean[state.length][2];
			for(int r = 0; r <reclaimed.length ;r++){
				for(int c = 0; c < 2; c++){
					newReclaimed[r][c] = reclaimed[r][c];
				}
			}
			
			//try all possible moves
			for(int r = 0 ; r < reclaimed.length; r++){
				for(int c = 0; c < 2; c++){
					if(!reclaimed[r][c]){
						newReclaimed[r][c] = true;
						for(int rr = r-1; rr <= r+1; rr++){
							if(rr < 0 || rr >= reclaimed.length) continue;
							newReclaimed[rr][1-c] = true;
						}
						
						List<State> states = new ArrayList<State>();
						if(r > 1 && r < reclaimed.length -2){
							//split into two for sure
							//top
							states.add(new State(r, newReclaimed[r-1][0], newReclaimed[r-1][1], newReclaimed[0][0], newReclaimed[0][1]));
							//bottom
							states.add(new State(reclaimed.length- r -1, newReclaimed[reclaimed.length-1][0], 
									newReclaimed[reclaimed.length-1][1], newReclaimed[r+1][0], newReclaimed[r+1][1] ));
						}else{
							//edge cases
							boolean isState = true;
							int prevRow = 0;

							if(newReclaimed[0][0] && newReclaimed[0][1]){
								isState = false;
							}
							
							for(int rr = 0; rr < newReclaimed.length+1 ; rr++){
								if(rr == newReclaimed.length || (newReclaimed[rr][0] && newReclaimed[rr][1])){
									//time to cut
									if(isState){
										//found a new state
										states.add(new State(rr-prevRow, newReclaimed[rr-1][0], newReclaimed[rr-1][1],newReclaimed[prevRow][0], newReclaimed[prevRow][1]));
										isState = false;
									}else{
										//have been cutting
									}
								}else{
									//keep building state
									if(isState){

									}else{
										//start of new state
										prevRow = rr;
										isState = true;
									}
								}
							}
						}
						
						if(states.size() == 0){
							childSg.add(0);
						}else if(states.size() == 1){
							childSg.add(getSg(states.get(0)));
						}else{
							childSg.add(getSg(states.get(0)) ^ getSg(states.get(1)));
						}
						
						//restore
						newReclaimed[r][c] = reclaimed[r][c];
						for(int rr = r-1; rr <= r+1; rr++){
							if(rr < 0 || rr >= reclaimed.length) continue;
							newReclaimed[rr][1-c] = reclaimed[rr][1-c];
						}
					}
				}
			}
			
			int mex =0;
			while(childSg.contains(mex)){
				mex++;	
			}
			sg.put(state, mex);
			return mex;
		}
		
	}

	static class State{
		public int length;
		public boolean botLeftMark;
		public boolean botRightMark;
		public boolean topLeftMark;
		public boolean topRightMark;

		public State(int length, boolean botLeftMark, boolean botRightMark,
				boolean topLeftMark, boolean topRightMark) {
			this.length = length;
			this.botLeftMark = botLeftMark;
			this.botRightMark = botRightMark;
			this.topLeftMark = topLeftMark;
			this.topRightMark = topRightMark;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (botLeftMark ? 1231 : 1237);
			result = prime * result + (botRightMark ? 1231 : 1237);
			result = prime * result + length;
			result = prime * result + (topLeftMark ? 1231 : 1237);
			result = prime * result + (topRightMark ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			State other = (State) obj;
			if (botLeftMark != other.botLeftMark)
				return false;
			if (botRightMark != other.botRightMark)
				return false;
			if (length != other.length)
				return false;
			if (topLeftMark != other.topLeftMark)
				return false;
			if (topRightMark != other.topRightMark)
				return false;
			return true;
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}

		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
