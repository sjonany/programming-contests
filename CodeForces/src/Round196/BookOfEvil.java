package Round196;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;


public class BookOfEvil {
	static int n;
	static int m;
	static int d;
	static boolean[] evil;

	//max distance to evil
	static int[] up;
	static int[] down;

	static List<Integer>[] edges;
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		n = in.nextInt();
		m = in.nextInt();
		d = in.nextInt();

		up = new int[n+1];
		down = new int[n+1];
		edges = (List<Integer>[]) Array.newInstance(List.class, n+1);
		evil = new boolean[n+1];

		for(int i = 1; i <= n; i++){
			edges[i] = new ArrayList<Integer>();
		}

		for(int i= 0 ; i < m; i++){
			evil[in.nextInt()] = true;
		}

		for(int i = 1; i <= n-1; i++){
			int from = in.nextInt();
			int to = in.nextInt();
			edges[from].add(to);
			edges[to].add(from);
		}

		dfs(1, -1);
		up[1] = -1;
		dfs2(1, -1, -1);

		int count = 0;
		for(int i = 1 ; i<= n; i++){
			if(up[i] < 0){
				if(down[i] < 0){
					continue;
				}
				if(down[i] <= d) count++;
			}else{
				if(down[i] < 0){
					if(up[i]<=d)count++;
				}else{
					if(Math.max(up[i], down[i]) <= d)count++;
				}
			}
		}
		System.out.println(count);
	}

	static void dfs(int cur, int par){
		int ans = -1;
		if(evil[cur]) ans = 0; 

		for(int child : edges[cur]){
			if(child == par) continue;
			dfs(child, cur);
			if(down[child] != -1){
				ans = Math.max(ans, 1+down[child]);
			}
		}

		down[cur] = ans;
	}

	//pre:dp cur is set, pos:dp children set
	static void dfs2(int cur, int par, int ppar){
		int numChildren = edges[cur].size();
		if(cur != 1) numChildren--;
		
		if(numChildren == 0) return;
		int[] children = new int[numChildren];
		int[] maxLeft = new int[numChildren];
		int[] maxRight = new int[numChildren];
		
		int childIndex = 0;
		for(int child : edges[cur]){
			if(child == par) continue;
			children[childIndex++] = child;
		}

		maxLeft[0] = down[children[0]];
		for(int i = 1; i < maxLeft.length; i++){
			maxLeft[i] = Math.max(maxLeft[i-1], down[children[i]]);
		}
		
		maxRight[numChildren-1] = down[children[numChildren-1]];
		for(int i = numChildren-2; i >= 0; i--){
			maxRight[i] = Math.max(maxRight[i+1], down[children[i]]);
		}
		
		for(int i = 0; i < children.length; i++){
			int id = children[i];
			up[id] = -1;
			if(up[cur] != -1) up[id] = 1 + up[cur];
			if(evil[cur]){
				up[id] = Math.max(1, up[id]);
			}
			
			int neighbor = -1;
			if(i+1 < numChildren){
				neighbor = Math.max(neighbor, maxRight[i+1]);
			}
			
			if(i-1 >= 0){
				neighbor = Math.max(neighbor, maxLeft[i-1]);
			}
			
			if(neighbor != -1){
				up[id] = Math.max(up[id], 2 + neighbor);
			}
		}
		
		for(int child : edges[cur]){
			if(child == par) continue;
			dfs2(child, cur, par);
		}

	}


	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
