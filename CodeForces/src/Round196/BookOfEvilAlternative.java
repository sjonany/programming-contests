package Round196;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

//too slow, n^2
public class BookOfEvilAlternative {
	static List<Edge>[] outs;
	static Edge[] edges;
	static int n;
	static int m;
	static boolean[] evil;
	static int ASC = 0;
	static int DES = 1;
	// dp[ASC/DES][i] = edge id go up, max distance to evil
	static int[][] dp;
	static int UNSET = -2;
	static int NOEVIL = -1;

	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int d = in.nextInt();

		outs = (List<Edge>[]) Array.newInstance(List.class, n + 1);
		evil = new boolean[n + 1];
		for (int i = 1; i <= n; i++) {
			outs[i] = new ArrayList<Edge>();
		}

		for (int i = 1; i <= m; i++) {
			evil[in.nextInt()] = true;
		}

		edges = new Edge[n - 1];
		for (int i = 0; i < edges.length; i++) {
			int from = in.nextInt();
			int to = in.nextInt();
			edges[i] = new Edge(i, from, to);
			outs[from].add(edges[i]);
			outs[to].add(new Edge(i, to, from));
		}

		dp = new int[2][n - 1];
		Arrays.fill(dp[ASC], UNSET);
		Arrays.fill(dp[DES], UNSET);

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < dp[i].length; j++) {
				get(i, j);
			}
		}

		int count = 0;
		for (int i = 1; i <= n; i++) {
			Edge e = outs[i].get(0);
			int d1 = dp[e.getDirection()][e.id];
			if (d1 != NOEVIL) {
				d1++;
			}
			
			int d2 = dp[e.getDirection() ^ 1][e.id];

			if (Math.max(d1, d2) <= d) {
				count++;
			}
		}
		System.out.println(count);
	}

	// call to dp[i][j], i = asc/desc, j = edge id
	static int get(int i, int j) {
		if (dp[i][j] == UNSET) {
			int curNode = edges[j].getDest(i);
			if (evil[curNode]) {
				dp[i][j] = 0;
			} else {
				dp[i][j] = NOEVIL;
			}

			for (Edge out : outs[curNode]) {
				if (out.id == j)
					continue;
				int ans = get(out.getDirection(), out.id);
				if (ans != -1) {
					dp[i][j] = Math.max(dp[i][j], 1 + ans);
				}
			}
		}
		return dp[i][j];
	}

	static class Edge {
		public int id;
		public int from;
		public int to;

		public Edge(int id, int from, int to) {
			this.id = id;
			this.from = from;
			this.to = to;
		}

		public int getDest(int direction) {
			if (direction == ASC) {
				return Math.max(from, to);
			} else {
				return Math.min(from, to);
			}
		}

		public int getDirection() {
			if (to > from)
				return ASC;
			return DES;
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}

		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}