package Round196;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;

public class DivisorTree {
	static long[] arr;
	static int n;
	static boolean[][] divides;	
	static int minNum;
	static int TOPPEST;
	static int[] config;
	static List<Integer>[] children;
	static int[] primeDivs;
	
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		minNum = Integer.MAX_VALUE;
		n = in.nextInt();
		arr = new long[n];
		for(int i = 0; i < arr.length; i++){
			arr[i] = in.nextLong();
		}
		
		TOPPEST = n;
		Arrays.sort(arr);
		divides = new boolean[n][n];
		for(int i = 0; i < arr.length; i++){
			for(int j = 0; j < arr.length;j++){
				divides[i][j] = arr[i] % arr[j] == 0;
			}
		}
		config = new int[n];
		children = (List<Integer>[]) Array.newInstance(List.class, n+1);
		for(int i = 0; i < children.length; i++){
			children[i] = new ArrayList<Integer>();
		}
		primeDivs = new int[n];
		for(int i = 0; i < primeDivs.length; i++){
			primeDivs[i] = countFactor(arr[i]);
		}
		search(0);
		out.println(minNum);
		out.close();
	}
	
	static void search(int toSet) {
		if(toSet == arr.length){
			if(children[TOPPEST].size() == 1){
				int count = count(children[TOPPEST].get(0));
				if(count < 0)return;
				minNum = Math.min(minNum, count);
			}else{
				int sum = 1;
				for(int child : children[TOPPEST]){
					int count = count(child);
					if(count < 0)return;
					sum += count;
				}
				minNum = Math.min(minNum, sum);
			}
		}else{
			for(int i = toSet + 1; i < n; i++){
				if(divides[i][toSet]){
					children[i].add(toSet);
					config[toSet] = i;
					search(toSet+1);
					children[i].remove(children[i].size()-1);
				}
			}
			children[TOPPEST].add(toSet);
			config[toSet] = TOPPEST;
			search(toSet+1);
			children[TOPPEST].remove(children[TOPPEST].size()-1);
		}
	}
	
	static int count(int rootId){
		if(primeDivs[rootId] == 1){
			//if prime, also means no children btw
			return 1;
		}
		
		long rem = arr[rootId];
		int ans = 1;
	
		boolean good = true;
		int facLeft = primeDivs[rootId];
		for(int childId : children[rootId]){
			if(rem % arr[childId] == 0){
				int count = count(childId);
				if(count < 0){
					return -1;
				}
				ans += count(childId);
				rem /= arr[childId];
				facLeft -= primeDivs[childId];
			}else{
				good = false;
				break;
			}
		}
		
		if(!good){
			return -1;
		}
		
		ans += facLeft;
		
		return ans;
	}
	
	static int countFactor(long x){
		int num = 0;
		long div = 2;
		long sq = (long) Math.sqrt(x);
		while(x > 1){
			while(x%div == 0){
				x/=div;
				num++;
			}
			div++;
			if(div > sq){
				if(x>1){
					num++;
				}
				break;
			}
		}
		return num;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
