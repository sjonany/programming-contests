package Round181div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Array {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int[] arr=  new int[n];
		
		List<Integer> pos = new ArrayList<Integer>();
		List<Integer> negs = new ArrayList<Integer>();
		List<Integer> zeros = new ArrayList<Integer>();
		for(int i=0;i<n;i++){
			arr[i] = sc.nextInt();
			if(arr[i] == 0){
				zeros.add(arr[i]);
			}else if(arr[i] < 0){
				negs.add(arr[i]);
			}else{
				pos.add(arr[i]);
			}
		}
		
		if(pos.size() == 0){
			pos.add(negs.get(0));
			negs.remove(0);
			pos.add(negs.get(0));
			negs.remove(0);
		}
		
		if(negs.size() %2 == 0){
			zeros.add(negs.get(0));
			negs.remove(0);
		}
		
		System.out.print(negs.size() + " ");
		for(int i: negs){
			System.out.print(i + " ");
		}
		
		System.out.println();
		
		System.out.print(pos.size() + " ");
		for(int i: pos){
			System.out.print(i + " ");
		}
		
		System.out.println();
		
		System.out.print(zeros.size() + " ");
		for(int i: zeros){
			System.out.print(i + " ");
		}
		
		System.out.println();
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



