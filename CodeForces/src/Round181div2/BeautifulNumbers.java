package Round181div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BeautifulNumbers {
	static long MOD =  1000000007;
	static long[] fac;
	
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int a = sc.nextInt();
		int b = sc.nextInt();
		int n = sc.nextInt();
		fac = new long[n+1];
		fac[0] = 1;
		for(int i=1;i<n+1;i++){
			fac[i] = (fac[i-1] * i) % MOD;
		}
		
		long result = 0;
		for(int aC = 0; aC<=n; aC++){
			int bC = n-aC;
			long sum = aC*a + bC*b;
			boolean isGood = true;
			while(sum > 0){
				if(sum%10 != a && sum %10!=b){
					isGood = false;
					break;
				}
				sum/=10;
			}
			
			if(isGood){
				result = (result + Comb(n,Math.min(aC, bC)))%MOD;
			}
		}
		System.out.println(result);
	}
	
	static long Comb (int n, int k) {	
		long num = fac[n];
		long denom = (fac[k] * fac[n-k]) % MOD;
		return (num * modInverse(denom, MOD))%MOD;
	}
	
	/* This function calculates (a^b)%MOD */
	static long pow(long a, long b, long MOD) {
	long x = 1, y = a;
	    while(b > 0) {
	        if(b%2 == 1) {
	            x=(x*y);
	            if(x>MOD) x%=MOD;
	        }
	        y = (y*y);
	        if(y>MOD) y%=MOD;
	        b /= 2;
	    }
	    return x;
	}

	//1/v in mod t. works if a and m are coprime
	public static long modInverse(long v, long t){
		long a = v;
		long b = t;
	    long x = 1, y = 0;
	    long xLast = 0, yLast = 1;
	    long q, r, m, n;
	    while(a != 0) {
	        q = b / a;
	        r = b % a;
	        m = xLast - q * x;
	        n = yLast - q * y;
	        xLast = x; 
	        yLast = y;
	        x = m;
	        y = n;
	        b = a; 
	        a = r;
	    }
	    return (xLast+ t) % t;
	}
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



