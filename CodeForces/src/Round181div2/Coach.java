package Round181div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

public class Coach {
	static long MOD =  1000000007;

	static Map<Integer, List<Integer>> adjMap;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int m = sc.nextInt();
		
		adjMap = new HashMap<Integer, List<Integer>>();
		for(int i=0;i<n;i++){
			adjMap.put(i+1, new ArrayList<Integer>());
		}
		for(int i=0;i<m;i++){
			int from = sc.nextInt();
			int to = sc.nextInt();
			List<Integer> outs = adjMap.get(from);
			if(outs == null){
				outs = new ArrayList<Integer>();
				adjMap.put(from, outs);
			}
			
			outs.add(to);
			
			List<Integer> ins = adjMap.get(to);
			if(ins == null){
				ins = new ArrayList<Integer>();
				adjMap.put(to, ins);
			}
			
			ins.add(from);
		}
		int groupNum = 0;
		boolean[] visited = new boolean[n+1];
		
		int[][] groups = new int[n/3][3];
		Queue<Integer> lonely = new LinkedList<Integer>();
		for(Integer node : adjMap.keySet()){
			List<Integer> children = adjMap.get(node);
			int length = children.size();
			if(length == 2){
				int c1 = children.get(0);
				int c2 = children.get(1);
				if(!visited[c1] && !visited[c2] && !visited[node]){
					visited[node] = true;
					visited[c1] = true;
					visited[c2] = true;
					groups[groupNum][0]=node;
					groups[groupNum][1]=c1;
					groups[groupNum][2]=c2;
					groupNum++;
				}
			}else if(length > 2){
				System.out.println(-1);
				return;
			}else if(length == 0){
				lonely.add(node);
			}
		}
		
		for(Integer node : adjMap.keySet()){
			List<Integer> children = adjMap.get(node);
			int length = children.size();
			if(length == 1){
				int c1 = children.get(0);
				if(!visited[c1] && !visited[node]){
					visited[node] = true;
					visited[c1] = true;
					if(lonely.isEmpty()){
						System.out.println(-1);
						return;
					}else{
						int newpartner = lonely.remove();
						visited[newpartner] = true;
						groups[groupNum][0]=newpartner;
						groups[groupNum][1]=c1;
						groups[groupNum][2]=node;
						groupNum++;
					}
				}
			}
		}
		
		if(lonely.size() %3 != 0){
			System.out.println(-1);
			return;
		}else{
			int t = lonely.size()/3;
			for(int i=0;i<t;i++){
				groups[groupNum][0]=lonely.remove();
				groups[groupNum][1]=lonely.remove();
				groups[groupNum][2]=lonely.remove();
				groupNum++;
			}
		}
		
		if(groupNum != n/3){
			System.out.println(-1);
			return;
		}else{
			for(int i=0;i<n/3;i++){
				System.out.println(groups[i][0] + " " + groups[i][1] + " " + groups[i][2] + " ");
			}
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



