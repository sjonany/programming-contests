package Round223;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class SerejaAndTree {
	
	static PrintWriter out;
	static InputReader in;
	static long[] pow = new long[7001];
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int N = in.nextInt();
		int M = in.nextInt();
		
		List<Segment>[] segments = new List[N+1];
		for(int i=0; i < segments.length; i++) {
			segments[i] = new ArrayList();
		}
		
		int[] set = new int[1000005];
		for(int qi=0; qi<M;qi++) {
			if(in.nextInt() == 1) {
				int level = in.nextInt();
				long l = in.nextLong();
				long r = in.nextLong();
				int v = in.nextInt();
				segments[level].add(new Segment(l,r,v));
			} else {
				int level = in.nextInt();
				long pos = in.nextLong();
				long l = pos;
				long r = pos;
				
				int count = 0;
				while(level <= N) {
					for(Segment s : segments[level]) {
						if(s.right >= l && s.left <= r) {
							if(set[s.v] != qi) {
								set[s.v] = qi;
								count++;
							}
						}
					}
					level++;
					l = nextLeft(l);
					r = nextRight(r);
				}
				out.println(count);
			}
		}
		out.close();
	}
	
	
	static long nextLeft(long pos) {
		long hi = Long.highestOneBit(pos);
		if(hi == pos) {
			return pos + Long.numberOfTrailingZeros(hi);
		} else {
			return pos + Long.numberOfTrailingZeros(hi) + 1;
		}
	}
	static long nextRight(long pos) {
		return pos + Long.numberOfTrailingZeros(Long.highestOneBit(pos)) + 1;
	}
	
	static class Segment {
		public long left;
		public long right;
		public int v;
		public Segment(long left, long right, int v) {
			this.left = left;
			this.right = right;
			this.v = v;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
