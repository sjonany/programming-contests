package Round223;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Stack;
import java.util.StringTokenizer;

public class SerejaAndBracketsOffline {
	
	static PrintWriter out;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		String s = in.readLine();
		int M = Integer.parseInt(in.readLine());
		Segment[] queries = new Segment[M];
		for(int i=0;i<M;i++) {
			tk = tk (in.readLine());
			queries[i] = new Segment(Integer.parseInt(tk.nextToken()), Integer.parseInt(tk.nextToken()), i);
		}
		
		Arrays.sort(queries, new Comparator<Segment>(){
			@Override
			public int compare(Segment o1, Segment o2) {
				return o1.r - o2.r;
			}
		});
		
		int[] closest = new int[s.length()+1];
		Stack<Item> stack = new Stack();
		for(int i = 1; i <= s.length(); i++) {
			Item it = new Item(s.charAt(i-1) == '(' ? 1 : -1, i);
			if(it.side == -1) {
				if(!stack.isEmpty() && stack.peek().side == 1) {
					closest[it.pos] = stack.pop().pos;
				} 
			} else {
				stack.push(it);
			}
			
		}
		
		// fen[i] = best answer for l -> i
		Fenwick2 fen = new Fenwick2(1000005);
		int r = 1;
		int[] ans = new int[M];
		for(Segment q : queries) {
			while(r < q.r) {
				r++;
				if(closest[r] > 0) {
					fen.update(1, closest[r], 1);
				}
			}
			ans[q.id] = (int)(2  * fen.query(q.l));
		}
		
		for(int a : ans) out.println(a);
		out.close();
	}
	
	static class Item {
		public int side;
		public int pos;
		public Item(int side, int pos) {
			this.side = side;
			this.pos = pos;
		}
	}
	
	static class Fenwick2{ 
		public Fenwick1 tree;
		
		public Fenwick2(int n){
			tree = new Fenwick1(n);
		}
		
		public int query(int i) {
			return tree.getTotalFreq(i);
		}
		
		public void update(int i, int j, int v){
			tree.increment(i, v);
			tree.increment(j+1, -v);
		}
	}
	
	static class Fenwick1{
		int[] tree;
		public Fenwick1(int n){
			tree = new int[n+1];
		}

		public void increment(int i, int val){
			while(i < tree.length){
				tree[i] += val;
				i += (i & -i);
			}
		}

		public int getTotalFreq(int i){
			int sum = 0;
			while(i > 0){
				sum += tree[i];
				i -= (i & -i);
			}
			return sum;
		}
	}
	
	static class Segment {
		public int l;
		public int r;
		public int id;
		public Segment(int l, int r, int id) {
			this.l = l;
			this.r = r;
			this.id = id;
		}
	}
	
	/*
	 exist opt sol where all right most brackets are used. -> think of -1 pushed to the right.
	 given this, then to match, right is matched with closest unmatched left
	 the introduction of a new ) will increase solns for those <= closest left
	 */
	
	static StringTokenizer tk(String s) {return new StringTokenizer(s);}
}
