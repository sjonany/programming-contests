package Round223;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class SerejaAndBracketsOnline {
	static PrintWriter out;
	static InputReader in;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		String s = in.nextToken();
		SegmentTree tree = new SegmentTree(s);
		
		int m = in.nextInt();
		for(int i=0;i<m;i++) {
			int l = in.nextInt();
			int r = in.nextInt();
			out.println(tree.query(l-1,r-1).matched);
		}
		
		out.close();
	}
	
	static class Item {
		public int left;
		public int right;
		public int matched;
		
		public Item(int left, int right, int matched) {
			this.left = left;
			this.right = right;
			this.matched = matched;
		}
	}
	
	static class SegmentTree {
		private Item merge(Item val1, Item val2){
			// exists a solution where right brackets all pushed to the right
			// so it never hurts to optimize right half first
			// then, just optimize right. then left can do at most previous best + extra right bracket matches
			int extra = Math.min(val1.left, val2.right);
			int newMatched = val1.matched + val2.matched + 2 * extra;
			return new Item(val1.left - extra + val2.left, val1.right + val2.right - extra, newMatched);
		}

		//1-based
		Item[] tree;

		//num els in the original array
		int n;
		public SegmentTree(int n){
			this.n = n;
			//num nodes in tree = 2*n-1 <= 2*2^ceil(log_2 (n)) - 1 
			int numNodes = 2 * (int)(Math.pow(2,Math.ceil(Math.log(n)/Math.log(2))))-1;
			tree = new Item[numNodes+1];
		}

		public SegmentTree(String arr){
			this(arr.length());
			buildTree(1, 0, arr.length()-1, arr);
		}

		private void buildTree(int node, int a, int b, String arr){
			if(a > b) return;

			//leaf node
			if(a == b){
				tree[node] = new Item(0,0,0);
				if(arr.charAt(a) == '(') {
					tree[node].left++;
				} else {
					tree[node].right++;
				}
				return;
			}
			int mid = (a+b)/2;
			buildTree(2*node, a, mid, arr);
			buildTree(2*node+1, mid+1, b, arr);

			tree[node] = merge(tree[2*node], tree[2*node+1]);
		}
		
		public Item query(int i, int j){
			return query(1,0,n-1,i,j);
		}
		
		/**
		 * Query tree to get sum of values within range [i, j]
		 */
		private Item query(int node, int a, int b, int i, int j) {
			if(a == i && b == j){
				return tree[node];
			}
			else {
				//TODO: CHANGE IF WANT MAX -> -INF
				Item res = new Item(0,0,0);
				int mid = (a+b)/2;

				if(i <= mid) res = merge(res, query(node * 2, a, mid, i, Math.min(j, mid)));
				if(j > mid) res = merge(res, query(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j));
				
				return res;
			}
		}
	}

	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
