package Round223;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class SerejaAndPrefixes {
	
	static PrintWriter out;
	static InputReader in;

	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int M = in.nextInt();
		Query[] queries = new Query[M];
		for(int i=0;i<M; i++) {
			queries[i] = new Query();
			int type = in.nextInt();
			queries[i].type = type;
			if(type == 1) {
				queries[i].newnum = in.nextInt();
			} else {
				queries[i].preflength = in.nextInt();
				queries[i].rep = in.nextInt();
			}
		}
		
		int N = in.nextInt();
		long[] indices = new long[N];
		for(int i=0;i<N;i++) {
			indices[i] = in.nextLong();
		}
		
		long[] arr = new long[100005];
		long curlen = 0;
		int nextop = 0;
		int nextq = 0;
		
		while(true) {
			if(nextq == indices.length) break;
			Query op = queries[nextop];
			long newlen = curlen + 1;
			if(op.type == 2) {
				newlen = curlen + op.preflength * op.rep;
			}
			
			long queriedindex = indices[nextq];
			boolean solved = false;
			if(queriedindex >= curlen && queriedindex <= newlen) {
				// answer
				if(op.type == 1) {
					out.print(op.newnum + " ");
				} else {
					long offset = queriedindex - curlen;
					offset--;
					offset %= op.preflength;
					offset++;
					out.print(arr[(int)offset] + " ");
				}
				nextq++;
				solved = true;
			}
			
			if(!solved) {
				if(op.type == 1) {
					if(curlen + 1 < arr.length) {
						arr[(int)(curlen + 1)] = op.newnum;
					}
				} else {
					if(curlen + 1 < arr.length) {
						int nexttoupdate = (int) (curlen+1);
						boolean good = true;
						for(int r=0;r<op.rep;r++){
							for(int i=1;i<=op.preflength;i++) {
								if(nexttoupdate >= arr.length) {
									good = false;
									break;
								}
								arr[nexttoupdate] = arr[i];
								nexttoupdate++;
							}
							if(!good)break;
						}
					}
				}
				curlen = newlen;
				nextop++;
			}
		}
		
		out.close();
	}
	
	static class Query {
		public int type;
		public long newnum;
		public long preflength;
		public long rep;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
