package Round187;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class SerejaAndSubsequences {
	static long MOD = 1000000007;
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int[] arr = new int[n];
		for(int i = 0 ; i < n ; i++){
			arr[i] = in.nextInt();
		}
		
		//num ans for subs that end exactly with val = i
		BIT bit = new BIT(1000000);
		
		for(int i = 0; i < n; i++){
			int val = arr[i];
			// no extension
			long ans = 1;
			// extend
			ans += bit.getTotalFreq(val);
			ans %= MOD;
			ans *= val;
			ans %= MOD;
			bit.increment(val, ans - bit.getFreq(val));
		}
		out.println(bit.getTotalFreq(1000000));
		out.close();
	}
	
	static class BIT{
		long[] tree;
		public BIT(int n){
			tree = new long[n+1];
		}
		
		public void increment(int idx, long val){
			while(idx < tree.length){
				tree[idx] += val;
				tree[idx] %= MOD;
				idx += (idx & -idx);
			}
		}

		public long getFreq(int idx){
			return idx==0? getTotalFreq(0) : getTotalFreq(idx)-getTotalFreq(idx-1);
		}
		
		public long getTotalFreq(int idx){
			long sum = 0;
			while(idx > 0){
				sum += tree[idx];
				sum %= MOD;
				idx -= (idx & -idx);
			}
			return sum;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
