package Round187;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.InputMismatchException;

public class SerejaAndStraightLines {
	static double[] minXFromBottom;
	static double[] maxXFromBottom;
	static double[] minXToTop;
	static double[] maxXToTop;
	static Point2D.Double[] points;
	static double[] yVals;
	static int n;
	
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		n = in.nextInt();
		points = new Point2D.Double[n];
		for(int i = 0; i < n; i++){
			int x = in.nextInt();
			int y = in.nextInt();
			points[i] = (Double) AffineTransform.getRotateInstance(Math.toRadians(-45)).transform(new Point2D.Double(x,y), null);
		}
		
		Arrays.sort(points, new Comparator<Point2D.Double>(){
			@Override
			public int compare(Double o1, Double o2) {
				return java.lang.Double.compare(o1.y, o2.y);
			}
		});
		
		//for points[0->i], what's min X?
		minXFromBottom = new double[n];
		maxXFromBottom = new double[n];
		
		//for points[i->n-1], what's min X?
		minXToTop = new double[n];
		maxXToTop = new double[n];
		
		double curMinX = Integer.MAX_VALUE;
		double curMaxX = Integer.MIN_VALUE;
		
		for(int i = 0; i < n; i++){
			curMinX = Math.min(curMinX, points[i].x);
			curMaxX = Math.max(curMaxX, points[i].x);
			minXFromBottom[i] = curMinX;
			maxXFromBottom[i] = curMaxX;
		}
		
		curMinX = Integer.MAX_VALUE;
		curMaxX = Integer.MIN_VALUE;
		for(int i = n-1; i >= 0; i--){
			curMinX = Math.min(curMinX, points[i].x);
			curMaxX = Math.max(curMaxX, points[i].x);
			minXToTop[i] = curMinX;
			maxXToTop[i] = curMaxX;
		}
		
		yVals = new double[n];
		for(int i = 0; i < n; i++){
			yVals[i] = points[i].y;
		}
		
		double left = 0;
		double right = Integer.MAX_VALUE;
		while(right - left > 0.001){
			double mid = (left+right)/2.0;
			if(good(mid)){
				right = mid;
			}else{
				left = mid;
			}
		}
		Integer trunc = (int)left; 
		if(left-trunc > 0.75 || left-trunc < 0.25){
			out.println(Math.round(left));
		}else{
			out.println(Math.floor(left) + 0.5);
		}
		out.close();
	}
	
	static boolean good(double w){
		w = w * Math.sin(Math.PI/4.0) * 2.0;
		for(int i = 0; i < n; i++){
			int top = Arrays.binarySearch(yVals, yVals[i] + w);
			if(top < 0){
				top = -1-top;
				top--;
			}
			
			int bottom = Arrays.binarySearch(yVals, yVals[i] - 0.00001);
			if(bottom < 0){
				bottom = -1-bottom;
			}
			double bottomMinX = Integer.MAX_VALUE;
			double bottomMaxX = Integer.MIN_VALUE;
			double topMinX = Integer.MAX_VALUE;
			double topMaxX = Integer.MIN_VALUE;
			
			if(bottom != 0){
				bottomMinX = minXFromBottom[bottom-1];
				bottomMaxX = maxXFromBottom[bottom-1];
			}
			
			if(top != n-1){
				topMinX = minXToTop[top+1];
				topMaxX = maxXToTop[top+1];
			}
			
			if(Math.max(topMaxX, bottomMaxX) - Math.min(bottomMinX, topMinX) < w){
				return true;
			}
		}
		
		return false;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
