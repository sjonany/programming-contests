package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;


public class A {
	static Map<String, Integer> nameMap;
	static List<Edge>[] edges;
	static int id;
	static int dest;
	static int min;
	static int numChoice;
	static List<Edge> sortedEdges;
	
	static class Edge{
		public int from;
		public int to;
		public int weight;

		public Edge(int from, int to, int weight) {
			this.from = from;
			this.to = to;
			this.weight = weight;
		}
	}

	public static void main(String[] args) throws Exception{
		
		edges = (List<Edge>[]) Array.newInstance(List.class, 21);
		for(int i = 0; i < edges.length; i++){
			edges[i] = new ArrayList<Edge>();
		}
		
		id = 0;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		nameMap = new HashMap<String, Integer>();
		sortedEdges = new ArrayList<Edge>();
		int m = Integer.parseInt(in.readLine());
		for(int i = 0 ;i < m; i++){
			tk = tk(in.readLine());
			String from = tk.nextToken();
			String to = tk.nextToken();
			int weight = Integer.parseInt(tk.nextToken());
			if(!nameMap.containsKey(from)){
				nameMap.put(from, id++);
			}

			if(!nameMap.containsKey(to)){
				nameMap.put(to, id++);
			}

			int f = nameMap.get(from);
			int t = nameMap.get(to);
			sortedEdges.add(new Edge(f, t, weight));
			edges[f].add(new Edge(f,t, weight));
			edges[t].add(new Edge(t,f, weight));
		}
		
		Collections.sort(sortedEdges, new Comparator<Edge>(){
			@Override
			public int compare(Edge o1, Edge o2) {
				return o1.weight - o2.weight;
			}
		});
		
		numChoice = Integer.parseInt(in.readLine());
		
		min = Integer.MAX_VALUE;	
		
		dest = nameMap.get("Park");
		choose(0, 0, new ArrayList<Integer>());
		out.println("Total miles driven: " + min);
		out.close();
	}
	
	static void choose(int toSet, int runningSum, List<Integer> chosen){
		if(toSet == edges[dest].size()){
			if(chosen.size() > numChoice || chosen.isEmpty()) return;
			DisjointSet dj = new DisjointSet(id);
			for(int i = 0; i < chosen.size()-1; i++){
				dj.union(chosen.get(i), chosen.get(i+1));
			}

			//park + chosen 
			int numCovered = chosen.size() + 1;
			int totalWeight = runningSum;
			for(Edge e : sortedEdges){
				if(e.from == dest || e.to == dest) continue;
				boolean disjoint = dj.union(e.from, e.to);
				if(disjoint) {
					numCovered++;
					totalWeight += e.weight;
				}
				if(numCovered >= id)
					break;
			}
			if(numCovered == id)
				min = Math.min(totalWeight, min);
		}else{
			if(chosen.size() < numChoice){
				chosen.add(edges[dest].get(toSet).to);
				choose(toSet+1, runningSum + edges[dest].get(toSet).weight, chosen);
				chosen.remove(chosen.size()-1);
			}
			choose(toSet+1, runningSum, chosen);
		}
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
	
	static class DisjointSet{
		int[] parent;
		int[] size;
		
		public DisjointSet(int n) {
			parent = new int[n];
			size = new int[n];
			for(int i = 0; i < n; i++){
				parent[i] = i;
				size[i] = 1;
			}
		}
		
		public int getLeader(int x){
			if(parent[x] != x){
				parent[x] = getLeader(parent[x]);
				return parent[x];
			}else{
				return x;
			}
		}
		
		public boolean union(int x, int y){
			x = getLeader(x);
			y = getLeader(y);
			
			if(x==y){
				return false;
			}
			
			int larger = x;
			int smaller = y;
			if(size[x] < size[y]){
				larger = y;
				smaller = x;
			}
			
			parent[smaller] = larger;
			size[larger] += size[smaller];
			return true;
		}
	}
}
