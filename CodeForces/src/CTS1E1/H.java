package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class H {
    
    public static void main(String[] args) throws Exception{
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out = new PrintWriter(System.out);
        StringTokenizer tk;
        int TEST = Integer.parseInt(in.readLine());
        for(int test = 1; test <= TEST; test++){
            tk = tk(in.readLine());
            int n = Integer.parseInt(tk.nextToken());
            StringBuilder sb = new StringBuilder();
            int count = 0;
            int[] charTally = new int[26];
            while(true){
                tk = tk(in.readLine());
                while(tk.hasMoreTokens()){
                    String tok = tk.nextToken();
                    sb.append(tok);
                    count += tok.length();
                    for(int i = 0 ;i  <tok.length();i++){
                        charTally[(int)(tok.charAt(i)-'A')]++;
                    }
                }
                if(count >= n)break;
            }
            
            String crib = in.readLine();
            int foundS = -1;
            int foundM = -1;
            
            for(int s = 1; s < 26; s++){
                StringBuilder sb2 = new StringBuilder();
                for(int i = 0; i < crib.length(); i++){
                    sb2.append((char)((((int)(crib.charAt(i) - 'A') + s) % 26 + 'A')));
                }
                int[] charTally2 = new int[26];
                for(int i = 0;  i< sb2.length(); i++){
                    charTally2[(int)(sb2.charAt(i)-'A')]++;
                }
                
                boolean good1 = true;
                for(int i = 0; i < 26;i++){
                    if(charTally2[i] > charTally[i]){
                        good1= false;
                        break;
                    }
                }
                if(!good1)continue;
                
                for(int m = 5; m <= Math.min(20, sb.length()); m++){
                    for(int start = 0; start <= sb.length() - crib.length(); start++){
                        boolean good = true;
                        for(int i = 0; i < sb2.length(); i++){
                            int ind = i + start;
                            int group = ind / m;
                            int groupOffset = ind % m;
                            int groupSize = m;
                            //lastgroup
                            if((group+1) * m >= sb.length()){
                                groupSize = sb.length() % m;
                                if(groupSize==0)groupSize=m;
                            }
                            char sb1Char = sb.charAt(group * m + (groupSize-1-groupOffset));
                            if(sb1Char != sb2.charAt(i)){
                                good = false;
                                break;
                            }
                        }
                        if(good){
                            foundS = s;
                            foundM = m;
                        }
                        if(foundS >= 0)break;
                    }
                    if(foundS >= 0)break;
                }
                
                if(foundS >= 0)break;
            }
            
            if(foundS != -1){
                out.println(foundS + " " + foundM);
            }else{
                out.println("Crib is not encrypted.");
            }
        }
        
        out.close();
    }
    
    static StringTokenizer tk(String s){
        return new StringTokenizer(s);
    }
}