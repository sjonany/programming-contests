package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class J {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		String line = in.readLine();
		while(line!=null && line.trim().length() != 0){
			tk = tk(line);
			
			int n = Integer.parseInt(tk.nextToken());
			int k = Integer.parseInt(tk.nextToken());
			out.println(count(n, 0, k));
			line = in.readLine();
		}
		
		out.close();
	}
	
	//pre: can't create anymore new cigars
	static int count(int good, int bad, int k){
		if(good+bad < k) return good;
		return good + count((good+bad)/k, (good+bad)%k, k);
	}
	

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
