package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class K {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		int m = Integer.parseInt(in.readLine());
		int[] arr = new int[m];
		tk = tk(in.readLine());
		for(int i = 0 ; i < m; i++){
			arr[i] = Integer.parseInt(tk.nextToken());
		}
		Arrays.sort(arr);
		
		if(arr.length == 1 && arr[0] == 0){
			out.println(1);
			out.println(0);
			out.close();
			return;
		}
		
		boolean[][] mat = solve(arr);

		out.println(arr[arr.length-1]+1 + (arr[0] == 0 ? 1 : 0));
		print(mat, out);
		out.close();
	}
	
	static boolean[][] solve(int[] arr) {
		int l = 0;
		int r = arr.length-1;
		int offset = 0;
		int sz = arr[r]+1;
		if(arr[l] == 0) sz++;
		boolean[][] mat = new boolean[sz][sz];
		
		if(arr[l] == 0){
			offset++;
			l++;
		}
		
		int deduct = 0;
		while(l <= r){
			for(int rr = 0; rr < arr[l] - deduct; rr++){
				int count = 0;
				int cc = 0;
				while(true){
					int ro = rr + offset;
					int co = cc + offset;
					cc++;
					if(ro == co) {
						continue;
					}
					mat[ro][co] = true;
					mat[co][ro] = true;
					count++;
					if(count >= arr[r] - deduct) break;
				}
			}
			offset += arr[l]-deduct+1;
			deduct += arr[l]-deduct;
			r--;
			l++;
		}
		return mat;
	}
	
	static void print(boolean[][] mat, PrintWriter out){
		for(int i = 0; i < mat.length; i++){
			for(int j = 0; j < mat[0].length; j++){
				out.print(mat[i][j] ? 1 : 0);
				if(j!=mat[0].length-1)
					out.print(" ");
			}
			out.println();
		}
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
	
	/*
	 induction on f(k) = given k+1 vertices, can solve problem where max degree = k, and no zero degrees
	 base case - f(1), 2 vertices
	 i.h assume f(d), d<k
	 i.s show f(k)
	 let v = min requirement
	 pick v vertices, connect to one another, (v-1) saturation each, then to the other (k+1-v) vertices.
	 now each of the v vertices have v-1 +  k+1-v = k degree, and the remaining k+1-v have v degree
	 leave the v vertices alone, and take 1 vertex with v degree. This leaves k-v vertices, and we have satisfied v and k.
	 remove k and v from requirement list, and repeat with the rest - v
	 
	 max(the rest - v) <= k-v-1 (note that the rest != v, so none have zero degree)
	 By i.h, f(k-v-1) say with k-v vertices, it's doable , and we happen to have k-v vertices.
	 if max(the rest - v) is less than k-v-1, it's fine to leave blanks too, because they have
	 v saturation already, so we will only have duplicate v's
	 */
}
