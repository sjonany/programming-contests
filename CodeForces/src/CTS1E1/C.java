package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class C {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		StringTokenizer tk;
		
		boolean first = true;
		while(true){
			tk = tk(in.readLine());
			int m = Integer.parseInt(tk.nextToken());
			int n = Integer.parseInt(tk.nextToken());
			if(m == 0 && n == 0)break;
			if(!first)out.println();
			first = false;
			
			int[] f = new int[m+n];
			for(int i = 0 ; i < f.length; i++){
				f[i] = Integer.parseInt(tk.nextToken());
			}
			
			//x^m -> x^m+n-1
			Fraction[][] mat = new Fraction[n][n+1];
			for(int r = 0; r < mat.length-1; r++){
				mat[r][n] = new Fraction(0,1);
			}
			mat[mat.length-1][n] = new Fraction(1,1);
			
			for(int deg = m; deg <= m+n-1; deg++){
				int qdeg = 0;
				for(; qdeg < n; qdeg++){
					int fdeg = deg-qdeg;
					if(fdeg < 0 || qdeg >= n) break;
					mat[deg-m][qdeg] = new Fraction(f[fdeg], 1);		
				}
				for(int c = qdeg; c < n; c++){
					mat[deg-m][c] = new Fraction(0,1);
				}
			}
			
			Fraction[] q = solve(mat);
			Fraction[] p = new Fraction[m];
			
			for(int pdeg = 0; pdeg < m; pdeg++){
				Fraction sum = new Fraction(0,1);
				for(int qdeg = 0; qdeg < n; qdeg++){
					int fdeg = pdeg - qdeg;
					if(fdeg < 0) break;
					sum = sum.add(q[qdeg].mul(new Fraction(f[fdeg],1)));
				}
				p[pdeg] = sum;
			}
			
			print(p, out);
			print(q, out);
		}
		
		out.close();
	}
	
	static void print(Fraction[] arr, PrintWriter out){
		boolean isZero = true;
		for(Fraction f : arr ){
			if(f.num != 0){
				isZero = false;
				break;
			}
		}
		
		if(isZero){
			out.println("(0,0)");
		}else{
			List<String> ans = new ArrayList<String>();
			for(int i = 0 ; i < arr.length; i++){
				Fraction f = arr[i];
				if(f.num == 0) continue;
				ans.add(String.format("(%s,%d)", f.toString(), i));
			}
			
			for(int i = 0 ;i  <ans.size() ;i++){
				out.print(ans.get(i));
				if(i != ans.size()-1){
					out.print(" ");
				}
			}
			out.println();
		}
	}
	
	static Fraction[] solve(Fraction[][] mat){
		int numRow = mat.length;
		int numCol = mat[0].length;
		int maxRank = Math.min(numRow, numCol);
		for(int pCol = 0; pCol < maxRank; pCol++) {
			int startRow = pCol;
			int maxPivotRowIndex = startRow;
			for(int row = startRow+1; row < numRow; row++) {
				if(mat[row][pCol].absgreater(mat[maxPivotRowIndex][pCol])) {
					maxPivotRowIndex = row;
				}
			}
			if(mat[maxPivotRowIndex][pCol].num == 0){
				continue;
			}

			Fraction[] temp = mat[startRow];
			mat[startRow] = mat[maxPivotRowIndex];
			mat[maxPivotRowIndex] = temp;
			for(int r = startRow + 1; r < numRow; r++) {
				if(mat[r][pCol].num == 0) continue;
				Fraction mul = mat[r][pCol].div(mat[startRow][pCol]);
				for(int c = pCol; c < numCol; c++) {
					mat[r][c] = mat[r][c].sub(mat[startRow][c].mul(mul));
				}
			}
		}
		Fraction[] x = new Fraction[numCol-1];

		for(int r = numRow-1; r >= 0; r--){
			Fraction sum = new Fraction(0,1);
			for(int c = numCol-2; c > r; c--) {
				sum = sum.add(mat[r][c].mul(x[c]));
			}
			x[r] = (mat[r][numCol-1].sub(sum)).div(mat[r][r]);
		}
		return x;
	}
	
	static class Fraction {
		int num;
		int den;
		
		public Fraction(int n, int d) {
			this.num = n;
			this.den = d;
			reduce();
		}
		
		public void reduce() {
			if(den < 0) {
				num*=-1;
				den*=-1;
			}
			if(num == 0){
				den = 1;
				return;
			}
			int g = gcd(Math.abs(num),den);
			num/=g;
			den/=g;
		}
		
		public Fraction mul (Fraction f){
			return new Fraction(f.num * num, f.den * den);
		}
		
		public Fraction add (Fraction f) {
			return new Fraction(f.num * den + num * f.den, den * f.den);
		}
		
		public Fraction sub (Fraction f) {
			return new Fraction(-f.num * den + num * f.den, den * f.den);
		}
		
		public Fraction div(Fraction f){
			return new Fraction(num * f.den, den * f.num);
		}
		
		public int gcd(int a, int b) {
			while(b > 0) {
				int c = a % b;
				a = b;
				b = c;
			}
			return a;
		}
		
		public boolean absgreater(Fraction f){
			return Math.abs(num * f.den) > Math.abs(f.num * den);
		}
		
		@Override
		public String toString(){
			if(num == 0) return "0";
			if(den == 1) return ""+num;
			return num + "/" + den;
		}
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
