package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class B {
	static int n;
	static int[] gons;
	static int s;
	static int MAX_I = 1000;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		boolean first=true;
		while(true){
			if(!first)out.println();
			first=false;
			n = Integer.parseInt(in.readLine());
			if(n==0)break;
			gons = new int[n];
			tk = tk(in.readLine());
			for(int i = 0 ; i < n; i++){
				gons[i] = Integer.parseInt(tk.nextToken());
			}
			s = Integer.parseInt(in.readLine());
			solve(out);
		}

		out.close();
		
	}
	
	static void solve(PrintWriter out){
		int[] indices = new int[gons.length];
		int count = 0;
		
		for(int i = 0; i < indices.length; i++){
			indices[i] = binSearch(gons[i], s);
		}
		
		while(true){
			int min = Integer.MAX_VALUE;
			for(int i = 0; i < indices.length; i++){
				int ii = indices[i];
				min = Math.min(min, (ii+1) + (gons[i]-2)* (ii%2==0 ? ii/2*(ii+1) : (ii+1)/2*ii));
			}
			
			List<Integer> moved = new ArrayList<Integer>();
			for(int i = 0; i < indices.length; i++){
				int ii = indices[i];
				int v = (ii+1) + (gons[i]-2)* (ii%2==0 ? ii/2*(ii+1) : (ii+1)/2*ii);
				
				if(v == min) {
					moved.add(i);
					indices[i]++;
				}
			}
			
			
			if(moved.size() > 1){
				out.print(min + ":");
				for(int i = 0 ; i < moved.size(); i++){
					out.print(gons[moved.get(i)]);
					if(i != moved.size()-1){
						out.print(" ");
					}
				}
				out.println();
				count++;
			}
			
			if(count == 5)break;
		}
	}
	
	//search arr_k for first guy >= s
	static int binSearch(int k, int s) {
		if(s==1)return 0;
		int left = 0;
		int right = MAX_I;
		
		while(left < right - 1) {
			int i = (left + right) / 2;
			long val = (i+1) + (k-2)* (i%2==0 ? i/2*(i+1) : (i+1)/2*i);
			if(val >= s){
				right = i;
			}else{
				left = i;
			}
		}
		
		return left+1;
	} 
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}