package CTS1E1;
import java.awt.geom.Line2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class L {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		String line = in.readLine();
		while(line != null && line.trim().length()!=0) {
			tk = tk(line);
			double h1 = Double.parseDouble(tk.nextToken());
			double h2 = Double.parseDouble(tk.nextToken());
			double c = Double.parseDouble(tk.nextToken());
		
			double l = 0;
			double r = Math.max(h1, h2);
			while(Math.abs(r-l) >= 0.00001) {
				double w = (r+l)/2.0;
				
				//left line
				double c1 = Math.sqrt(h1*h1-w*w);
				double m1 = -c1 / w;
				
				double m2 = Math.sqrt(h2*h2-w*w)/w;
				double curY = m2 * c1 / (m2-m1);
				if(curY > c) {
					l = w;
				}else{
					r = w;
				}
			}
			out.println(String.format("%.3f", l));
			line = in.readLine();
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
