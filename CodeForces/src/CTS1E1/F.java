package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;


public class F {
	static int LOSE_TURN = Integer.MAX_VALUE;
	static int[] board;
	static int START = 0;
	static int END;
	//dp[i][j] = Pr(x= i, turn =j) / MAX
	static long[][] dp;
	static int T;
	
	static long ONE = (long) Math.pow(2, 50);

	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		int TEST = Integer.parseInt(in.readLine());
		for(int test = 1; test <= TEST; test++){
			tk = tk(in.readLine());
			int m = Integer.parseInt(tk.nextToken());
			T = Integer.parseInt(tk.nextToken());
			board = new int[m+2];
			END = board.length-1;

			tk = tk(in.readLine());
			for(int i = 1; i<=m; i++){
				String s = tk.nextToken();
				if(s.equals("L")){
					board[i] = LOSE_TURN;
				}else{
					board[i] = Integer.parseInt(s);
				}
			}

			END = board.length-1;
			dp = new long[T+1][board.length];
			long prob = solve();
			if(prob == ONE/2){
				out.println("Push. 0.5000");
			}else{
				double p = 1.0 * prob / ONE;
				if(p < 0.5){
					out.println(String.format("Bet against. %.4f",p));
				}else{
					out.println(String.format("Bet for. %.4f", p));
				}
			}
		}

		out.close();
	}

	static long solve() {
		dp[START][0] = ONE;
		for(int  t =0; t<T; t++) {
			for(int px = 0; px < board.length; px++){
				if(px == END)continue;
				//where to next?
				//move right 1
				int nextX = Math.min(END,px+1);

				double prob = dp[t][px] / 2;
				int instr = board[nextX];
				if (instr == LOSE_TURN) {
					if (t + 2 < T) {
						dp[t + 2][nextX] += prob;
					}
				} else {
					dp[t + 1][nextX + instr] += prob;
				}


				//move right 2
				nextX = Math.min(END, px+2);
				instr = board[nextX];
				if(instr == LOSE_TURN) {
					if(t+2 < T){
						dp[t+2][nextX] += prob;
					}
				}else {
					dp[t+1][nextX + instr] += prob;
				}
			}
		}
		long total = 0;
		for(int t = 1; t <= T; t++){
			total += dp[t][END];
		}
		return total;
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
