package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;


public class G {

	static int X = 0;
	static int Z = 1;
	static int FIRST = 0;
	static int SECOND = 1;
	static int[][] points;
	static int[][][] cuts;
	static int n;

	static int[][] curCut;
	static StringBuilder ans;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		n = Integer.parseInt(in.readLine());
		points = new int[n][2];
		for(int i = 0; i < n; i++){
			tk = tk(in.readLine());
			Integer.parseInt(tk.nextToken());
			points[i][X] = Integer.parseInt(tk.nextToken());
			points[i][Z] = Integer.parseInt(tk.nextToken());
			//ignore the rest
		}
		
		int p = Integer.parseInt(in.readLine());
		cuts = new int[p][2][2];
		for(int i = 0; i < p; i++){
			tk = tk(in.readLine());
			cuts[i][FIRST][X] = Integer.parseInt(tk.nextToken());
			cuts[i][FIRST][Z] = Integer.parseInt(tk.nextToken());
			cuts[i][SECOND][X] = Integer.parseInt(tk.nextToken());
			cuts[i][SECOND][Z] = Integer.parseInt(tk.nextToken());
		}
		
		Node root = new Node();
		for(int i = 0; i < n; i++){
			root.objs.add(i);
		}
		
		for(int i = 0; i < p; i++){
			curCut = cuts[i];
			cut(root);
		}
		ans = new StringBuilder();
		inorder(root);
		out.println(ans);
		out.close();
	}
	
	static void inorder(Node node){
		if(node.left == null && node.right == null){
			for(int objId : node.objs){
				ans.append((char)('A'+objId));
			}
		}else{
			inorder(node.left);
			inorder(node.right);
		}
	}

	static void cut(Node node) {
		if(node.left != null){
			cut(node.left);
		}
		
		if(node.right != null){
			cut(node.right);
		}
		
		if(node.left == null && node.right == null){
			if(node.objs.size() == 1)return;
			List<Integer> userSide = new ArrayList<Integer>();
			List<Integer> otherSide = new ArrayList<Integer>();
			int us = getSide(new int[]{0,Integer.MAX_VALUE/2}, curCut);
			for(int objId : node.objs){
				int side = getSide(points[objId], curCut);
				if(side == us){
					userSide.add(objId);
				}else{
					otherSide.add(objId);
				}
			}
			
			if(userSide.size() != 0 && otherSide.size() != 0){
				node.objs = null;
				node.left = new Node();
				node.left.objs = userSide;
				node.right = new Node();
				node.right.objs = otherSide;
			}
		}
	}
	
	static int getSide(int[] pt, int[][] cut){
		long c = ((long)cut[SECOND][X] - cut[FIRST][X]) * ((long)pt[Z]-cut[FIRST][Z]) -
				((long)cut[SECOND][Z] - cut[FIRST][Z]) * ((long)pt[X]-cut[FIRST][X]);
		return (int)(c/Math.abs(c));
	}
	
	static class Node{
		public int id;
		public Node left;
		public Node right;
		public List<Integer> objs;
		public Node() {
			this.left = null;
			this.right = null;
			this.objs = new ArrayList<Integer>();
		}
		
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
