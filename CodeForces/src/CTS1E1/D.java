package CTS1E1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class D {
	static int n;
	static boolean[][][] board;
	static int MAXX;
	static int MAXY;
	static int MAXZ;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		boolean first = true;
		while(true){
			if(!first)out.println();
			first=false;
			n = Integer.parseInt(in.readLine());
			
			board = new boolean[21][21][21];
			MAXX=0;
			MAXY=0;
			MAXZ=0;
			for(int y = 0; y < n; y++){
				int x = 0;
				tk = tk(in.readLine());
				while(tk.hasMoreTokens()){
					int z = Integer.parseInt(tk.nextToken())-1;
					if(z==-1)break;
					for(int i = 0 ; i <= z; i++){
						board[x][y][i] = true;
					}
					MAXX=Math.max(MAXX,x);
					MAXY=Math.max(MAXY,y);
					MAXZ=Math.max(MAXZ,z);
					x++;
				}
			}
			for(int z = 0; z <= MAXZ; z++){
				boolean print = false;
				List<Integer> l = new ArrayList<Integer>();
				for(int y = 0; y <= MAXY; y++){
					int xCount = 0;
					while(xCount <= MAXX && board[xCount][y][z]){
						xCount++;
					}
					xCount--;
					if(xCount < 0){
						break;
					}else{
						l.add(xCount);
					}
				}
				for(int i = 0 ; i < l.size(); i++){
					out.print(l.get(i)+1);
					print=true;
					if(i!=l.size()-1){
						out.print(" ");
					}
				}
				out.println();
				if(!print)break;
			}
			
			out.println();
			
			for(int x = 0; x <= MAXX; x++){
				boolean print = false;
				List<Integer> l = new ArrayList<Integer>();
				for(int z = 0; z <= MAXZ; z++){
					int yCount = 0;
					while(yCount <= MAXY && board[x][yCount][z]){
						yCount++;
					}
					yCount--;
					if(yCount < 0){
						break;
					}else{
						l.add(yCount);
					}
				}
				for(int i = 0 ; i < l.size(); i++){
					out.print(l.get(i)+1);
					print=true;
					if(i!=l.size()-1){
						out.print(" ");
					}
				}
				out.println();
				if(!print)break;
			}
			
			out.println();
			
			if(n==0)break;
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
