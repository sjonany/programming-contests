package Round167div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

public class DimaAndSequence {
	static int n;
	public static void main(String[] args){
		
		
		HashMap<Integer, Long> count = new HashMap<Integer, Long>();
		long total = 0;
		//HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();
		
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		for(int i=0;i<n;i++){
			int bCount = Integer.bitCount(sc.nextInt());
			Long prevCount = count.get(bCount);
			if(prevCount == null){
				prevCount = (long) 1;
			}else{
				total += prevCount;
				prevCount ++;
			}
			
			count.put(bCount, prevCount);
		}
		
		System.out.println(total);
	}
	

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
