package Round167div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

public class DimaAndTwoSequences {
	public static int m;
	public static int n;
	public static Hashtable<Integer, Long> a = new Hashtable<Integer, Long>();
	public static Hashtable<Integer, Integer> count = new Hashtable<Integer, Integer>();
	public static int owedDiv = 0;
	
	public static int[] aSeq;
	public static int[] bSeq;
	
	public static void main(String[] args){
		FastScanner reader = new FastScanner();
		n= reader.nextInt();
		
		aSeq = new int[n+1];
		
		for(int i=1; i<=n ;i++){
			aSeq[i] = reader.nextInt();
		}
		
		bSeq = new int[n+1];
		
		for(int i=1; i<=n ;i++){
			bSeq[i] = reader.nextInt();
		}
		
		m = reader.nextInt();	
		
		//count numRep first
		for(int i=1; i<=n; i++){
			if(aSeq[i] == bSeq[i]){
				owedDiv++;
			}
		}
		
		for(int i=1; i<=2*n ;i++){
			int k = i>n ?  bSeq[i-n] : aSeq[i];
			Integer countK = count.get(k);
			Long fact = a.get(k);
			if(countK == null){
				a.put(k, (long) 1);
				count.put(k, 1);
			}else{
				//factorial on the fly
				int newK = countK+1;
				
				while(newK % 2 == 0 && owedDiv >0){
					newK /= 2;
					owedDiv--;
				}	
				newK %= m;
				long newFact = (fact * newK) %m;
				
				a.put(k, newFact);
				count.put(k, countK+1);
			}
		}	
		
		long ans = 1;
		
		Set<Entry<Integer,Long>> aTable = a.entrySet();
		for(Entry<Integer, Long> aPair : aTable){
			ans *= aPair.getValue();
			ans %= m;
		}
		
		System.out.println(ans);
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
