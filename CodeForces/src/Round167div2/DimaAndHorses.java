package Round167div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class DimaAndHorses {
	static int[] locHorse;
	static List<List<Integer>> enemies;
	public static void main(String[] args){
		FastScanner sc= new FastScanner();
		
		int n = sc.nextInt();
		int m = sc.nextInt();
		
		//enemies[i] = list of enemies of horse i
		enemies = new ArrayList<List<Integer>>();
		for(int horse=0;horse<=n;horse++){
			enemies.add(new ArrayList<Integer>());
		}
		
		for(int i=1;i<=m;i++){
			int h1 = sc.nextInt();
			int h2 = sc.nextInt();
			enemies.get(h1).add(h2);
			enemies.get(h2).add(h1);
		}
		
		//all horse in group 0 initially
		locHorse = new int[n+1];
		//each jump reduces a bad edge by >=1
		for(int horse=1;horse<=n;horse++){
			jump(horse);
		}
		for(int horse=1;horse<=n;horse++){
			System.out.print(locHorse[horse]);
		}
	}
	
	public static void jump(int horse){
		int countEnemy = 0;
		for(Integer enemy : enemies.get(horse)){
			if(locHorse[enemy] == locHorse[horse]){
				countEnemy++;
			}
		}
		if(countEnemy >= 2){
			locHorse[horse] = locHorse[horse] == 0 ? 1 : 0;
			for(Integer enemy : enemies.get(horse)){
				if(locHorse[enemy] == locHorse[horse]){
					jump(enemy);
				}
			}
		}
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
