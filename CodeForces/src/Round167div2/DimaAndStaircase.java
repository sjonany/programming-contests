package Round167div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//trick: the highest is either the block at pos 0, or the block to be added
//because the shape of the stair is U 
public class DimaAndStaircase {
	static int n;
	static int m;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		long[] h = new long[n];
		for(int i=0; i<n;i++){
			h[i] = sc.nextLong();
		}	

		long curLeftH = h[0];
		
		m = sc.nextInt();
		for(int i=1; i<=m; i++){
			int wi = sc.nextInt();
			int hi = sc.nextInt();
			
			long maxCurH = Math.max(curLeftH, h[wi-1]);
			curLeftH = maxCurH + hi;
			System.out.println(maxCurH);
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}