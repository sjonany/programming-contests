package Round183div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class D {
	static boolean good = false;
	static boolean pushed = false;
	static int n;
	static int m;
	static int x;
	static int y;
	static int a;
	static int b;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		m = sc.nextInt();
		x = sc.nextInt();
		y = sc.nextInt();
		a = sc.nextInt();
		b = sc.nextInt();
		int gcd = gcd(a,b);
		a/=gcd;
		b/=gcd;
		
		int lx,ly, rx,ry;
		//TODO: try all possible centers if not good
		lx = x - a/2;
		ly = y - a/2;
		rx = lx + a;
		ry = ly + b;
		
		//L, R, U, D
		if(lx < 0){
			pushed = true;
			int dx = -lx;
			lx += dx;
			rx += dx;
		}
		
		if(rx > n){
			pushed = true;
			int dx = rx-n;
			rx -= dx;
			lx -= dx;
		}
		
		if(ly <0){
			pushed = true;
			int dy = -ly;
			ly += dy;
			ry += dy;
		}
		
		if(ry > n){
			pushed = true;
			int dy = ry-n;
			ry -= dy;
			ly -= dy;
		}	
		
		if(lx >= 0 && lx <=n && ly >= 0 && ly <= n && 
				rx >= 0 && rx <=n && ry >= 0 && ry <= n ){
			if(pushed){
				System.out.println(lx + " " +  ly + " " + rx + " " +  ry);
			}else{
			}	
		}
		
	}

	//start with smallest dim, with fixed midpoint
	static void solve(double mx, double my){

		boolean isHalf = false;
		if(Math.abs((int)mx - mx) > 0.3  || Math.abs((int)my - my) > 0.3 ){
			isHalf = true;
		}
		
		boolean isKHalf =false;
		if(a%2 == 1 || b%2 == 1 ){
			isKHalf = true;
		}
		
		if(!isHalf && !isKHalf){
			int k = binSearch1((int)Math.round(mx), (int)Math.round(my));
			int lx = (int)Math.round(mx - k * a/2);
			int rx = (int)Math.round(mx + k * a/2);
			int ly = (int)Math.round(my - k * b/2);
			int ry = (int)Math.round(my + k * b/2);
		}else if(isHalf && !isKHalf){
			
		}
		
	}
	
	public static int binSearch1(int mx, int my){
		
		int lo=1;
		int hi=n;
		
		//in this case B = val[i] >= s, A = val[i]<s
		//inv: lo...hi contains 'A' -> AAAABB
		while(lo < hi){
			int mid = lo + (hi-lo+1)/2;
			int lx = (int)Math.round(mx - mid * a/2);
			int rx = (int)Math.round(mx + mid * a/2);
			int ly = (int)Math.round(my - mid * b/2);
			int ry = (int)Math.round(my + mid * b/2);
			
			if(lx >= 0 && lx <=n && ly >= 0 && ly <= n && 
					rx >= 0 && rx <=n && ry >= 0 && ry <= n ){
				lo = mid;
			}else{
				hi = mid-1;
			}
		}
	
		//lo points to highest index where A is true
		//EXCEPT for when everything is BBBBBB, then output still 0.
		//need to check case when arr[0] is B, if so, lo = -1
		return lo;
	}
	
	public static int gcd(int a, int b) //valid for positive integers.
	{
		while(b > 0)
		{
			int c = a % b;
			a = b;
			b = c;
		}
		return a;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



