package Round183div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class B {
	static int[] NUM_DAYS = new int[]{0,31,28,31,30,31,30,31,31,30,31,30,31};
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		String start = sc.nextToken();
		String end = sc.nextToken();
		
		String[] sToks = start.split(":");
		String[] eToks = end.split(":");
		
		int sy = Integer.parseInt(sToks[0]);
		int sm = Integer.parseInt(sToks[1]);
		int sd = Integer.parseInt(sToks[2]);
		
		int ey = Integer.parseInt(eToks[0]);
		int em = Integer.parseInt(eToks[1]);
		int ed = Integer.parseInt(eToks[2]);
		
		int count = 0;
		
		if(!(sy < ey || (sy==ey && sm<em) || (sy==ey && sm==em && sd<ed))){
			ey = Integer.parseInt(sToks[0]);
			em = Integer.parseInt(sToks[1]);
			ed = Integer.parseInt(sToks[2]);
			
			sy = Integer.parseInt(eToks[0]);
			sm = Integer.parseInt(eToks[1]);
			sd = Integer.parseInt(eToks[2]);
			
		}
		
		while(sy < ey || (sy==ey && sm<em) || (sy==ey && sm==em && sd<ed)){
			int numDay = NUM_DAYS[sm];

			if(sm == 2 && sy % 4 == 0 && !(sy % 100 == 0 && sy % 400 != 0)){
				numDay = 29;
			}
			
			sd++;
			count++;
			if(sd > numDay){
				sm++;
				sd = 1;
				if(sm == 13){
					sy++;	
					sm = 1;
				}
			}
		}
		System.out.println(count);
	}
	
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



