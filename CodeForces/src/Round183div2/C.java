package Round183div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class C {
	static int[] a;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		for(int n=1;n<=1001;n+=2){
		
		if(n % 2 == 0){
			System.out.println(-1);
			return;
		}
		/*
		int[] c = new int[n];
		StringBuffer out = new StringBuffer();
		for(int i=0;i<n;i++){
			if(i==n-1){
				out.append(i);
			}else{
				out.append(i + " ");
			}
			c[i] = (i*2)%n;
		}
		System.out.println(out);
		System.out.println(out);
		
		out = new StringBuffer();
		for(int i=0;i<n;i++){
			if(i==n-1){
				out.append(c[i]);
			}else{
				out.append(c[i] + " ");
			}
		}*/
		Set<Integer> cs = new HashSet<Integer>();
		boolean good = true;
		
		int[] b=  new int[n];
		for(int i=0;i<n/2;i++){
			b[i] = n-(i+1)*2;
		}
		for(int i=n/2;i<n;i++){
			b[i]= (n-1-i)*2;
		}
		for(int i=0;i<n;i++){
			int s = (i+b[i])%n;
			if(cs.contains(s)){
				good = false;
				break;
			}
			cs.add(s);
		}
		if(!good)
		System.out.println(n);
		}
		System.out.println("done");
	}
	
	public static void p(int toSet, int[] b, boolean[] used){
		if(toSet==b.length){
			Set<Integer> c = new HashSet<Integer>();
			boolean good = true;
			for(int i=0;i<b.length;i++){
				int s = (a[i]+b[i])%a.length;
				if(c.contains(s)){
					good = false;
					break;
				}
				c.add(s);
			}
			
			if(good){
				System.out.println(Arrays.toString(b));
			}
		}else{
			for(int i=0;i<b.length;i++){
				if(!used[i]){
					b[toSet] = i;
					used[i] = true;
					p(toSet+1, b, used);
					used[i] = false;
				}
			}
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



