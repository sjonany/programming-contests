package Round175div2;

import java.util.Scanner;

public class SlightlyDecreasingPermutation{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int k = sc.nextInt();
		
		int a[] = new int[n+1];
		for(int i=1; i<=n; i++){
			a[i] = n-i+1;
		}

		for(int i=1; i<=k; i++){
			System.out.print((n-i+1) + " ");
		}
		for(int i=1; i<=n-k; i++){
			System.out.print(i + " ");
		}
		sc.close();
	}
}
