package Round175div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/*

 15
his = 150347555
mine = 399832298

13
his = 695720788
mine =280560796
 */
public class PermutationSum{
	static int M = 1000000007;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int[]b = new int[n];


		for(int i=0;i<n;i++){
			b[i] = i+1;
		}

		long fac = fac(n);
		long sum = 0;
		boolean isPermutable = true;
		while(isPermutable){
			Set<Integer> c = new HashSet<Integer>();
			boolean isPerm = true;
			for(int j=0;j<n;j++){
				int ci = (int)((j + b[j]-1)%n+1);
				if(c.contains(ci)){
					isPerm = false;
					break;
				}else{
					c.add(ci);
				}
			}
			if(isPerm){
				sum++;
			}
			isPermutable = nextPerm(b);
		}
		
		System.out.println((sum*fac) % M);
	}

	public static long count(long n){
		n=n-3;
		long ans= 1;
		for(int i=1;i<=n;i++){
			ans = (ans * (2*i+1)) %M;
		}
		return ans;
	}

	public static long fac(long n){
		long i =1;
		for(int j=1;j<=n;j++){
			i = (i*j)%M;
		}

		return i;
	}

	public static boolean nextPerm(int[] p) {          
		for (int a = p.length - 2; a >= 0; --a)
			if (p[a] < p[a + 1])
				for (int b = p.length - 1;; --b)
					if (p[b] > p[a]) {
						int t = p[a];
						p[a] = p[b];
						p[b] = t;
						for (++a, b = p.length - 1; a < b; ++a, --b) {
							t = p[a];
							p[a] = p[b];
							p[b] = t;
						}
						return true;
					}
		return false;

	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
