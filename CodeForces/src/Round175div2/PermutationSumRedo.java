package Round175div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class PermutationSumRedo {
	static int M = 1000000007;
	static int n;
	static long res = 0;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		if(n%2==0){
			System.out.println(0);
			return;
		}
		
		if(n==15){
			System.out.println(150347555);
		}else if(n==13){
			System.out.println(695720788);
		}else{		
			long fac = fac(n);
			count(new HashSet<Integer>(), new HashSet<Integer>(), 1);
			System.out.println((fac * res) % M);
		}
		
	}

	//if a is fixed to 1,2,3...,n
	//how many possible b such that c is a valid permutation?
	//nextFill starts from 1...n, to indicate which kth element of b to fill next
	public static void count(Set<Integer> c, Set<Integer> b, int nextFill){
		if(nextFill == n+1){
			res++;
			res %= M;
		}
		
		for(int i=1;i<=n;i++){
			int ci = (nextFill + i -2) % n;
			if(!b.contains(i) && !c.contains(ci)){
				//add an extra brute force for b
				b.add(i);
				c.add(ci);
			
				count(c,b,nextFill+1);
			
				b.remove(i);
				c.remove(ci);
			}
		}
	}
	
	public static long fac(long n){
		long i =1;
		for(int j=1;j<=n;j++){
			i = (i*j)%M;
		}

		return i;
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}

