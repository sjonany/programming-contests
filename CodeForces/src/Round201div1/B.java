package Round201div1;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

public class B {

	static int[][][] dp;
	static Pointer[][][] pointers;
	static String virus;
	static List<Integer>[][] prevs;

	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		String s1 = in.nextToken();
		String s2 = in.nextToken();
		virus = in.nextToken();

		int[] F = buildPartialMatch(virus);
		//prevs[char][i] = previous state that when take char, will lead to state i
		List<Integer>[][] prevs = (List<Integer>[][]) Array.newInstance(List[].class, 26);
		for(int c = 0; c < 26; c++){
			prevs[c] = (List<Integer>[]) Array.newInstance(List.class, F.length);
			for(int i = 0; i < F.length; i++){
				prevs[c][i] = new ArrayList<Integer>();
			}
		}

		int[][] auto = buildAutomaton(virus, F);
		for(int i = 0; i <= virus.length(); i++){
			for(int c = 0; c < 26; c++){
				prevs[c][auto[i][c]].add(i);
			}
		}
		
		int s1l = s1.length();
		int s2l = s2.length();

		//LCS considering s1[..i], s2[..j], such that suffix matches k-length prefix of virus
		dp = new int[s1l][s2l][virus.length()];
		pointers = new Pointer[s1l][s2l][virus.length()];

		for(int i = 0; i < s1l; i++){
			for(int j = 0; j < s2l; j++){
				for(int k = 0; k < virus.length(); k++){
					dp[i][j][k] = -1;
					char c1 = s1.charAt(i);
					char c2 = s2.charAt(j);
					if(c1 == c2){
						for(int prev : prevs[c1-'A'][k]){
							if(prev == virus.length()) continue;
							update(i,j,k,i-1,j-1,prev,1);
						}
					}
					update(i,j,k,i-1,j,k,0);
					update(i,j,k,i,j-1,k,0);
				}	
			}	
		}

		int bestK = -1;
		for(int k = 0; k < virus.length(); k++){
			if(bestK == -1 || dp[s1l-1][s2l-1][bestK] < dp[s1l-1][s2l-1][k]){
				bestK = k;
			}
		}

		StringBuilder sb = new StringBuilder();
		int i = s1l-1;
		int j = s2l-1;
		int k = bestK;
		while(true){
			if(i < 0 || j < 0 ) break;
			Pointer p = pointers[i][j][k];
			if(p == null) break;
			if(p.i == i-1 && p.j == j-1){
				sb.append(s1.charAt(i));
			}

			i = p.i;
			j = p.j;
			k = p.k;
		}

		StringBuilder revsb = new StringBuilder();
		for(int ii = sb.length()-1; ii >= 0; ii--){
			revsb.append(sb.charAt(ii));
		}

		if(revsb.length() == 0) out.println(0);
		else out.println(revsb);
		out.close();
	}

	//which state will I be if at state i, and encounter char c, assume UPPERCASE
	static int[][] buildAutomaton(String s, int[] F) {
		int[][] auto = new int[F.length][26];
		auto[0][s.charAt(0)-'A'] = 1;
		for(int i = 1; i < F.length; i++){
			System.arraycopy(auto[F[i]], 0, auto[i], 0, 26);
			if(i != s.length())
				auto[i][s.charAt(i)-'A'] = i+1;
		}
		return auto;
	}

	static void update(int i, int j, int k, int ii, int jj, int kk, int offset){
		int newval = -1;
		if(ii < 0 || jj < 0){
			if(kk != 0) return;
			newval = offset;
		}else{
			if(dp[ii][jj][kk] == -1) return;
			newval = dp[ii][jj][kk] + offset;
		}

		if(dp[i][j][k] < newval){
			dp[i][j][k] = newval;
			pointers[i][j][k] = new Pointer(ii,jj,kk);
		}
	}

	// O(n), F[m] = i, consider first m chars of s, the length of longest PROPER suffix-prefix is i 
	static int[] buildPartialMatch(String s){
		int[] F = new int[s.length()+1];
		F[0] = F[1] = 0;
		for(int i = 2; i <= s.length(); i++){
			int j = F[i-1];
			char extension = s.charAt(i-1);
			while(true) {
				if(s.charAt(j) == extension) {
					F[i] = j+1;
					break;
				}

				if(j == 0) {
					F[i] = 0;
					break;
				}
				j = F[j];
			}
		}
		return F;
	}

	static class Pointer{
		public int i;
		public int j;
		public int k;
		public Pointer(int i, int j, int k) {
			this.i = i;
			this.j = j;
			this.k = k;
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
