package Round201div1;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Set;
import java.util.TreeSet;

public class CFaster {
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		int n = in.nextInt();
		Set<Integer> xset = new TreeSet<Integer>();
		for(int i = 0; i < n; i++){
			xset.add(in.nextInt());
		}

		int a = in.nextInt();
		int b = in.nextInt();

		int cur = a;

		int[] toremove = new int[xset.size()+1];
		int rid = 0;
		int step = 0;
		while(cur > b){
			int bestnextcur = cur-1;
			for(int x : xset){
				int nextcur = cur - (cur % x);
				if(nextcur < b){
					toremove[rid++] = x;
				}else{
					bestnextcur = Math.min(nextcur, bestnextcur);
				}
			}
			cur = bestnextcur;
			step++;
			if(rid != 0){
				for(int ri = 0; ri < rid; ri++){
					xset.remove(toremove[ri]);
				}
				rid = 0;
			}
		}

		out.println(step);
		out.close();
	}

	/**
	 if dp[i] = min #steps to transform i -> a
	 dp[i] is monotone increasing, and hence it never hurts to decrease the most at each step
	 proof: if we are at num = k right now. if dp[k] = dp[k-1] + 1, easy case
	 but if dp[k] = dp[m]  +1, where k - (k%x_i) = m, two cases
	 if k%x_i = 0, impossible, since we are not doing anything
	 so, k%x_i >=1, which means, (k-1) - ((k-1)%x_i) = k - (k %x_i), 
	 so, dp[k-1] will also have considered dp[m]+1. Thus, dp[k] >= dp[k-1] in any case

	 next, we can discard x_i where  i-(i%xi) < b, because we will keep decreasing i, 
	 i-1 - (i-1) %xi , if i%x >= 1, will stay the same, and so <b
	 but if i%x ==0, then i-1 will be an even greater deduction to i-1.. so < b still

	 consider the relevant x_i's
	 i - i%x_i will cause i to be divisible by x_i
	 then, after 2 more steps, since I have to decrease as much as possible, and I can 
	 choose to -1, then -%x_i again, I end up in the lower multiple of x_i.
	 Thus, after 2-3 steps, I will always at least decrease by max{relevant x_i}

	 10^6 * 3 *|x|/max{x}, but max{relevant x} >= |relevant x| at each step. So O(n)..
	 but idk hashset keeps tle :( so i just use treeset
	 */

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
