package Round201div1;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Set;

public class C {
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		int n = in.nextInt();
		Set<Integer> xset = new HashSet<Integer>();
		for(int i = 0; i < n; i++){
			xset.add(in.nextInt());
		}
		
		int a = in.nextInt();
		int b = in.nextInt();
		
		//div[i] = maximum xi that is a factor of a+i
		int[] div = new int[a-b+1];
		Arrays.fill(div, -1);
		
		//n log n, because worst case is xset = [1..n], then (b-a)* (1/1 + 1/2 + 1/3 +...)  
		for(int x : xset){
			int base = (b/x) * x;
			while(base < b) base += x;
			for(int mul = base; mul <= a; mul += x){
				div[mul-b] = Math.max(div[mul-b], x);
			}
		}
		
		Fenwick bit = new Fenwick(a-b+1);
		//dp[i] = min number of steps to convert a -> b+i
		int[] dp = new int[a-b+1];
		Arrays.fill(dp, 1000001);
		dp[a-b] = 0;
		bit.update(a-b+1, 0);
		for(int i = a-b-1; i >= 0; i--){
			dp[i] = Math.min(dp[i], 1+dp[i+1]);
			int maxXi= div[i];
			if(maxXi != -1){
				int rightEnd = Math.min(a-b,i+maxXi-1);
				int min = bit.getMin(rightEnd+1);
				dp[i] = Math.min(dp[i], 1+min);
			}
			bit.update(i+1, dp[i]);
		}
		
		out.println(dp[0]);
		out.close();
	}
	
	//prefix-min, only works when update is never increasing
	static class Fenwick{
		int[] tree;
		public Fenwick(int n){
			tree = new int[n+1];
			Arrays.fill(tree, 1000001);
		}

		public void update(int i, int val){
			while(i < tree.length){
				tree[i] = Math.min(tree[i], val);
				i += (i & -i);
			}
		}

		public int getMin(int i){
			int min = Integer.MAX_VALUE;
			while(i > 0){
				min = Math.min(min, tree[i]);
				i -= (i & -i);
			}
			return min;
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
