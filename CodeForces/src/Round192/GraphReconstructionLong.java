package Round192;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class GraphReconstructionLong {
	static StringBuilder sb;
	static List<Integer>[] edges;
	static int m;
	static int n;
	
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		n = in.nextInt();
		m = in.nextInt();
		
		edges = (List<Integer>[])Array.newInstance(List.class, n+1);
		for(int i = 1 ; i <edges.length; i++){
			edges[i] = new ArrayList<Integer>();
		}
		
		for(int i = 0; i < m;  i++){
			int a = in.nextInt();
			int b = in.nextInt();
			edges[a].add(b);
			edges[b].add(a);
		}
		
		if(n <= 6){
			//brute force
			boolean[][] bad = new boolean[n+1][n+1];
			for(int i = 1 ; i<= n; i++){
				for(int j = 1 ; j<= n; j++){
					bad[i][j] = edges[i].contains(j);
				}
			}
			
			List<List<Integer>> allPerms = new ArrayList<List<Integer>>();
			List<List<Integer>> allConfigs = new ArrayList<List<Integer>>();
			getAllPerms(new ArrayList<Integer>(), new boolean[n+1], allPerms);
			getAllConfigs(new ArrayList<Integer>(), n, allConfigs);
			
			//every configuration can be expressed as a permutation of nodes, and 3 states:
			final int CONTINUE = 0;
			final int CYCLE = 1;
			final int END = 2;
			
			StringBuilder ans = null;
			boolean good = false;
			for(List<Integer> perm : allPerms){
				for(List<Integer> config : allConfigs){
					ans = new StringBuilder();
					int numEdges = 0;
					//last start points to index right after last end / cycle
					int lastStart = perm.get(0);
					int lastStartIndex = 0;
					boolean isConfigGood = true;
					for(int i = 0; i < perm.size() && isConfigGood; i++){
						int curNode = perm.get(i);
						switch (config.get(i)){ 
							case CONTINUE:
								if(i == perm.size()-1){
									isConfigGood = false;
								}else{
									int nextNode = perm.get(i+1);
									if(bad[curNode][nextNode]){
										isConfigGood = false;
									}else{
										ans.append(curNode + " " + nextNode +"\n");
										numEdges++;
									}
								}
								break;
							case CYCLE:
								if(lastStartIndex < 0 || lastStartIndex+1 >= i){
									isConfigGood = false;
								}else{
									if(bad[curNode][lastStart]){
										isConfigGood = false;
									}else{
										ans.append(curNode + " " + lastStart + "\n");
										numEdges++;
										lastStartIndex = i+1;
										if(i < perm.size()-1)
											lastStart = perm.get(i+1);
									}
								}
								break;
							case END:
								lastStartIndex = i+1;
								if(i < perm.size()-1)
									lastStart = perm.get(i+1);
								break;
						}
								
						if(numEdges == m){
							good = true;
							break;
						}
					}
					if(good)
						break;
				}
				if(good)
					break;
			}
			
			if(good){
				out.println(ans);
			}else{
				out.println(-1);
			}
			out.close();
			return;
		}else{
			//n > 6
			//somebody's pro solution :<
			//we note that if bfs, i and i+3 will never be adjacent
			//if node i is distance d from root, then the worst possible case
			//,since each node has at most 2 outgoing edges, is that
			//node i+1 also has distance d, then i+2 must have distance d+1, 
			//but touches i, due to bfs nature, and finally i+3 also has distance d+1
			//but touches node i+1, not i
			//if the condition is more relaxed, then i+3 distance >= i distance + 2, 
			//which means it's trivially non touching
			//the other case is when i and i+3%n can be cyclic in the same component?
			//since n>6, we can eyeball that this will never happen
			
			boolean[] visit = new boolean[n+1];
			int[] order = new int[n];
			int index = 0;
			for(int i = 1 ; i <= n; i ++){
				if(!visit[i]){
					visit[i] = true;
					Queue<Integer> q = new LinkedList<Integer>();
					q.add(i);
					while(!q.isEmpty()){
						int j = q.poll();
						order[index] = j;
						index++;
						for(int next : edges[j]){
							if(!visit[next]){
								visit[next] = true;
								q.add(next);
							}
						}
					}
				}
			}
			for(int i = 0; i < m ;i++){
				out.println(order[i]  + " " + order[(i+3)%n]);
			}
		}
		
		out.close();
	}
	
	static void getAllPerms(List<Integer> current, boolean[] used, List<List<Integer>> compilation){
		if(current.size() == used.length-1){
			compilation.add(new ArrayList<Integer>(current));
		}else{
			for(int i = 1 ; i < used.length; i++){
				if(!used[i]){
					current.add(i);
					used[i] = true;
					getAllPerms(current, used, compilation);
					used[i] = false;
					current.remove(current.size()-1);
				}
			}
		}
	}
	
	static void getAllConfigs(List<Integer> current, int n, List<List<Integer>> compilation){
		if(current.size() == n){
			compilation.add(new ArrayList<Integer>(current));
		}else{
			for(int i = 0; i< 3; i++){
				current.add(i);
				getAllConfigs(current, n, compilation);
				current.remove(current.size()-1);
			}
		}
	}
	
	public static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
