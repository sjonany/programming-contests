package Round192;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Queue;

public class BiridianForest {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int m = in.nextInt();
		int n = in.nextInt();
		int[][] board = new int[m][n];
		int startR = -1234;
		int startC = -1243;
		int endR = -1234;
		int endC = -1234;
		
		for(int r = 0 ;r < m; r++){
			String s = in.nextToken();
			for(int c = 0 ;c < n; c++){
				if(Character.isDigit(s.charAt(c))){
					board[r][c] = s.charAt(c)-'0';
				}else if(s.charAt(c) == 'T'){
					board[r][c] = -1;
				}else if(s.charAt(c) == 'E'){
					endR = r;
					endC = c;
					board[r][c] = 0;
				}else{
					startR = r;
					startC = c;
					board[r][c] = 0;
				}
			}
		}
		
		boolean[][] visit = new boolean[m][n];
		int[][] distance = new int[m][n];
		for(int r = 0; r < m; r++){
			Arrays.fill(distance[r], Integer.MAX_VALUE);
		}
		
		Queue<Point> q = new LinkedList<Point>();
		q.add(new Point(endR, endC));
		
		int[][] dirs = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
		visit[endR][endC] = true;
		distance[endR][endC] = 0;
		
		while(!q.isEmpty()){
			Point p = q.poll();
			for(int[] dir : dirs){
				int nr = p.r + dir[0];
				int nc = p.c + dir[1];
				if(nr>=0 && nr < m && nc>=0 && nc < n && !visit[nr][nc] && board[nr][nc] != -1){
					visit[nr][nc] = true;
					distance[nr][nc] = distance[p.r][p.c] + 1;
					q.add(new Point(nr,nc));
				}
			}
		}
		
		int myDist = distance[startR][startC];
		int count = 0;
		for(int r = 0; r < m; r++){
			for(int c= 0 ;c <n;c++){
				if(board[r][c] > 0 && myDist >= distance[r][c]){
					count += board[r][c];
				}
			}
		}
		
		out.println(count);
		
		out.close();
	}
	
	static class Point{
		public int r;
		public int c;
		
		public Point(int r, int c) {
			this.r = r;
			this.c = c;
		}
		
	}
	
	public static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
