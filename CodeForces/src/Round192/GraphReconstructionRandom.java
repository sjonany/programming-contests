package Round192;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class GraphReconstructionRandom {
	static StringBuilder sb;
	static List<Integer>[] edges;
	static int m;
	static int n;
	
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		n = in.nextInt();
		m = in.nextInt();
		
		edges = (List<Integer>[])Array.newInstance(List.class, n+1);
		for(int i = 1 ; i <edges.length; i++){
			edges[i] = new ArrayList<Integer>();
		}
		
		for(int i = 0; i < m;  i++){
			int a = in.nextInt();
			int b = in.nextInt();
			edges[a].add(b);
			edges[b].add(a);
		}
		
		List<Integer> perm = new ArrayList<Integer>();
		for(int i = 1; i <= n; i++){
			perm.add(i);
		}
		for(int i=1; i<=300; i++){
			Collections.shuffle(perm);
			StringBuilder ans = new StringBuilder();
			
			boolean good = true;
			for(int j = 0 ;j < m ;j++){
				if(edges[perm.get(j)].contains(perm.get((j+1)%n))){
					good=false;
					break;
				}else{
					ans.append(perm.get(j) + " " + perm.get((j+1)%n)+"\n");
				}
			}
			if(good){
				out.println(ans);
				out.close();
				return;
			}
		}
		
		out.println(-1);
		out.close();
	}
	
	public static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
