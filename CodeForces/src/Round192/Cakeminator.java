package Round192;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class Cakeminator {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		int m = in.nextInt();
		int n = in.nextInt();
		boolean[][] board = new boolean[m][n];
		boolean[][] eaten = new boolean[m][n];
		for(int i = 0; i < m; i++){
			String s = in.nextToken();
			for(int j = 0; j < n; j++){
				board[i][j] = (s.charAt(j) == '.');
			}
		}
		
		for(int r = 0; r < m; r++){
			boolean good = true;
			for(int c = 0; c < n; c++){
				if(!board[r][c]){
					good = false;
					break;
				}
			}
			if(good){
				for(int c = 0; c < n; c++){
					eaten[r][c] = true;
				}
			}
		}
		
		for(int c = 0; c < n; c++){
			boolean good = true;
			for(int r = 0; r < m; r++){
				if(!board[r][c]){
					good = false;
					break;
				}
			}
			if(good){
				for(int r = 0; r < m; r++){
					eaten[r][c] = true;
				}
			}
		}
		
		int count = 0;
		for(int r = 0; r < m; r++){
			for(int c = 0; c < n; c++){
				if(eaten[r][c])count++;
			}
		}
		out.println(count);
		out.close();
	}
	
	public static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
