package Round192;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class Purification {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		boolean[][] evil = new boolean[n][n];
		
		for(int r = 0 ; r < n; r ++){
			String s = in.nextToken();
			for(int c = 0 ; c < n; c ++){
				evil[r][c] = s.charAt(c) == 'E';
			}
		}
		
		boolean rowTraversal = true;
		List<Integer> ans = new ArrayList<Integer>();
		for(int r = 0; r < n; r++){
			boolean exist = false;
			for(int c = 0; c < n; c++){
				if(!evil[r][c]){
					ans.add(c);
					exist = true;
					break;
				}
			}
			if(!exist){
				rowTraversal = false;
				break;
			}
		}
		
		if(rowTraversal){
			for(int r = 0 ;r <n;r++){
				out.println((r+1) + " " + (ans.get(r)+1));
			}
		}else{
			boolean colTraversal = true;
			ans = new ArrayList<Integer>();
			for(int c = 0; c < n; c++){
				boolean exist = false;
				for(int r = 0; r < n; r++){
					if(!evil[r][c]){
						ans.add(r);
						exist = true;
						break;
					}
				}
				if(!exist){
					colTraversal = false;
					break;
				}
			}
			
			if(colTraversal){
				for(int c = 0 ;c <n;c++){
					out.println((ans.get(c)+1) + " " + (c+1));
				}
			}else{
				out.println(-1);
			}
		}
		
		out.close();
	}
	
	public static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
