package Round191div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class FlippingGame {
	public static void main(String[] args) throws Exception{
		FastScanner in = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int[] arr = new int[n];
		for(int i = 0; i < n; i++){
			arr[i] = in.nextInt();
		}
		int max = 0;
		
		for(int i = 0; i < n; i++){
			for(int j = i; j < n; j++){
				int count = 0;
				for(int s = 0; s < i; s++){
					if(arr[s] == 1)count++;
				}
				for(int s = j+1; s < n; s++){
					if(arr[s] == 1)count++;
				}
				for(int s = i; s <= j; s++){
					if(arr[s] == 0)count++;
				}
				max = Math.max(count, max);
			}
		}
		
		out.println(max);
		
		out.close();
	}
	
	static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
