package Round191div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class MagicFive {
	static long MOD = 1000000007;
	public static void main(String[] args) throws Exception{
		FastScanner in = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		long ans = 0;
		String a = in.nextToken();
		int k = in.nextInt();
		for(int i = 0; i < a.length(); i++){
			char c = a.charAt(i);
			if(c == '0' || c =='5'){
				ans += exp(2, i, MOD);
				ans %= MOD;
			}
		}
		long cur = exp(2,a.length(), MOD);
		ans *= (exp(cur,k,MOD)-1) * (modInv(cur-1, MOD)) % MOD;
		out.println(ans % MOD);
		out.close();
	}
	
	static long modInv(long a, long mod){
		return BigInteger.valueOf(a).modInverse(BigInteger.valueOf(mod)).longValue();
	}
	
	public static long exp(long a, long n, long mod){
		if(n==0){
			return 1;
		}

		if(n % 2 == 0){
			long r = exp(a, n/2, mod);
			return (r*r) % mod;
		}else{
			return (a * exp(a,n-1, mod)) % mod;
		}
	}
	
	static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
