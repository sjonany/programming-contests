package Round191div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class FlippingGameFaster {
	public static void main(String[] args) throws Exception{
		FastScanner in = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int[] arr = new int[n];
		int[] b = new int[n];
		int s = 0;
		for(int i = 0; i < n; i++){
			arr[i] = in.nextInt();
			if(arr[i] == 1){
				s++;
				b[i] = -1;
			}else{
				b[i] = 1;
			}
		}
		
		//max sum contiguous sequence
		//end exactly at i
		int[] dp = new int[n];
		dp[0] = b[0];
		int max = dp[0];
		for(int i = 1; i < n; i++){
			dp[i] = Math.max(b[i] + dp[i-1], b[i]);
			max = Math.max(dp[i], max);
		}
		max += s;
		out.println(max);
		
		out.close();
	}
	
	static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
