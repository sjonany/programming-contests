package Round191div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class BlockTower {
	static char[][] board;
	static int R;
	static int C;
	static StringBuffer log;
	static int moveCount = 0;
	public static void main(String[] args) throws Exception{
		FastScanner in = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		log = new StringBuffer();
		R = in.nextInt();
		C = in.nextInt();
		board = new char[R][C];
		for(int r = 0; r < R; r++){
			String s = in.nextToken();
			for(int c = 0; c < C; c++){
				board[r][c] = s.charAt(c);
			}
		}
		
		for(int r = 0; r < R; r++){
			for(int c = 0; c < C; c++){
				if(board[r][c] == '.'){
					dfs(r,c, true);
				}
			}
		}
		out.println(moveCount);
		out.println(log);
		out.close();
	}
	
	static int[][] DIRS = new int[][]{{1,0},{-1,0},{0,1},{0,-1}};
	static void dfs(int r, int c, boolean isBlue){
		board[r][c] = '#';
		log.append("B " + (r+1) + " " + (c+1) + "\n");
		moveCount++;
		for(int[] dir : DIRS){
			int nextR = r + dir[0];
			int nextC = c + dir[1];
			if(nextR >= 0 && nextR < R && nextC >=0 && nextC< C && board[nextR][nextC] == '.'){
				dfs(nextR, nextC, false);
			}
		}
		if(!isBlue){
			log.append("D " + (r+1) + " " + (c+1) + "\n");
			log.append("R " + (r+1) + " " + (c+1) + "\n");
			moveCount += 2;
		}
	}
	
	static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
