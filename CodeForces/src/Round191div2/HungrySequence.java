package Round191div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class HungrySequence {
	public static void main(String[] args) throws Exception{
		FastScanner in = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		boolean[] sieve = new boolean[10000001];
		int[] arr = new int[n];
		int arrI = 0;
		for(int i = 2; i < sieve.length; i++){
			if(sieve[i]){
				continue;
			}
			arr[arrI] = i;
		    arrI++;
			for (int j = 1; i*j < sieve.length; j++) {
				sieve[i*j] = true;
			}
			
			if(arrI == n){
				break;
			}
		}
		
		for(int i = 0; i < arr.length; i++){
			out.print(arr[i]);
			if(i != arr.length-1){
				out.print(" ");
			}
		}
		
		out.close();
	}
	
	static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
