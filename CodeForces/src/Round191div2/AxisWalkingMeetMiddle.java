package Round191div2;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Map.Entry;

public class AxisWalkingMeetMiddle {
	static int MOD = 1000000007;
	static long[] fac;
	static int[] arr1;
	static int[] arr2;
	static Map<Key, Integer> left;
	static Map<Key, Integer> right;
	static int n;

	public static void main(String[] args) throws Exception {
		fac = new long[25];
		fac[0] = 1;
		for (int i = 1; i < fac.length; i++) {
			fac[i] = (fac[i - 1] * i) % MOD;
		}

		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		n = in.nextInt();
		arr1 = new int[Math.round(n / 2f)];
		arr2 = new int[n - arr1.length];
		for (int i = 0; i < arr1.length; i++) {
			arr1[i] = in.nextInt();
		}
		for (int i = 0; i < arr2.length; i++) {
			arr2[i] = in.nextInt();
		}

		int k = in.nextInt();
		int b1 = -1;
		int b2 = -1;
		if (k >= 1)
			b1 = in.nextInt();
		if (k == 2)
			b2 = in.nextInt();
		
		if(k != 0){
			// key = <sum,count> , val = # subsets with that sum,count
			left = new HashMap<Key, Integer>();
			right = new HashMap<Key, Integer>();

			Key[] leftKeys = new Key[1 << arr1.length];
			leftKeys[0] = new Key(0, 0);
			left.put(leftKeys[0], 1);
			for (int i = 0, mask = 1; i < arr1.length; i++, mask <<= 1) {
				leftKeys[mask] = new Key(1, arr1[i]);
			}
			for (int i = 1; i < (1 << arr1.length); i++) {
				int last = i & -i;
				int prev = i - last;
				leftKeys[i] = new Key(1 + leftKeys[prev].numChosen,
						leftKeys[prev].sum + leftKeys[last].sum);
				Integer prevCount = left.get(leftKeys[i]);
				if (prevCount == null) {
					prevCount = 0;
				}
				left.put(leftKeys[i], prevCount + 1);
			}

			Key[] rightKeys = new Key[1 << arr2.length];
			rightKeys[0] = new Key(0, 0);
			right.put(rightKeys[0], 1);
			for (int i = 0, mask = 1; i < arr2.length; i++, mask <<= 1) {
				rightKeys[mask] = new Key(1, arr2[i]);
			}
			rightKeys[0] = new Key(0, 0);
			for (int i = 1; i < (1 << arr2.length); i++) {
				int last = i & -i;
				int prev = i - last;
				rightKeys[i] = new Key(1 + rightKeys[prev].numChosen,
						rightKeys[prev].sum + rightKeys[last].sum);
				Integer prevCount = right.get(rightKeys[i]);
				if (prevCount == null) {
					prevCount = 0;
				}
				right.put(rightKeys[i], prevCount + 1);
			}
		}
		
		if (k == 0) {
			out.println(fac[n]);
		} else if (k == 1) {
			long ans = fac[n] - getNumPathTouch(b1);
			if (ans < 0)
				ans += MOD;
			out.println(ans);
		} else {
			long ans = fac[n];
			ans -= getNumPathTouch(b1);
			if (ans < 0)
				ans += MOD;
			ans -= getNumPathTouch(b2);
			if (ans < 0)
				ans += MOD;
			ans += getNumPathTouch(b1, b2);
			out.println(ans % MOD);
		}

		out.println();
		out.close();
	}

	// num paths touch b
	static long getNumPathTouch(int b) {
		long totalAns = 0;

		for (Entry<Key, Integer> leftEntry : left.entrySet()) {
			for (int rightCount = 0; rightCount <= arr2.length; rightCount++) {
				Integer numRightMatch = right.get(new Key(rightCount, b
						- leftEntry.getKey().sum));
				if (numRightMatch != null) {
					int totalBeforeTouch = leftEntry.getKey().numChosen
							+ rightCount;
					long facCount = fac[totalBeforeTouch]
							* fac[n - totalBeforeTouch] % MOD;
					long subsetPairCount = (leftEntry.getValue() * numRightMatch)
							% MOD;
					totalAns = totalAns + (facCount * subsetPairCount) % MOD;
					totalAns %= MOD;
				}
			}
		}
		return totalAns;
	}

	// return num path touches b1 and b2
	static long getNumPathTouch(int b1, int b2) {
		if (b1 > b2) {
			int temp = b1;
			b1 = b2;
			b2 = temp;
		}

		Map<Key, Map<Key, Integer>> leftTally = new HashMap<Key, Map<Key, Integer>>(10000000);
		Map<Key, Map<Key, Integer>> rightTally = new HashMap<Key, Map<Key, Integer>>(10000000);
		choose(0, new Key2(0,0,0,0), arr1, leftTally);
		choose(0, new Key2(0,0,0,0), arr2, rightTally);
		
		long ans = 0;
		
		for (Entry<Key, Map<Key, Integer>> firstLeftEntry : leftTally.entrySet()) {
			long needLeft = b1 - firstLeftEntry.getKey().sum;
			int countFirstLeft = firstLeftEntry.getKey().numChosen;
			if(needLeft < 0) continue;
			for (Entry<Key, Integer> firstMidEntry : firstLeftEntry.getValue().entrySet()) {
				long needMid = b2 - b1 - firstMidEntry.getKey().sum;
				int countFirstMid = firstMidEntry.getKey().numChosen;
				if(needMid < 0) continue;
				
				//now look at the other n/2 partition
				for (int leftCount = 0; leftCount <= arr2.length; leftCount++) {
					Map<Key, Integer> secondRightTally = rightTally.get(new Key(leftCount, needLeft));
					if(secondRightTally != null){
						for (int midCount = 0; midCount <= arr2.length-leftCount; midCount++) {
							Integer numRightMatch = secondRightTally.get(new Key(midCount, needMid));
							if(numRightMatch != null){
								int numLeft = countFirstLeft + leftCount;
								int numMid = countFirstMid + midCount;
								int numRight = n - numLeft - numMid;
								long perm = ((fac[numLeft] * fac[numMid])%MOD * fac[numRight])%MOD;
								long subsetPairCount = (firstMidEntry.getValue() * numRightMatch) % MOD;
								ans = ans + (perm * subsetPairCount) % MOD;
								ans %= MOD;
							}
						}
					}
				}
			}
		}
		return ans;
	}
	
	static void choose(int toSet, Key2 curKey, int[] arr, Map<Key, Map<Key, Integer>> tally){
		if(toSet == arr.length){
			Key first = new Key(curKey.numLeftChosen, curKey.leftSum);
			Key second = new Key(curKey.numMidChosen, curKey.midSum);
			Map<Key, Integer> midTally = tally.get(first);
			if(midTally == null){
				midTally = new HashMap<Key, Integer>();
				tally.put(first, midTally);
			}
			Integer prevCount = midTally.get(second);
			if(prevCount == null){
				prevCount = 0;
			}
			
			midTally.put(second, prevCount+1);
		}else{
			//either left, mid, or right
			//left
			curKey.leftSum += arr[toSet];
			curKey.numLeftChosen++;
			choose(toSet+1, curKey, arr, tally);
			curKey.leftSum -= arr[toSet];
			curKey.numLeftChosen--;
			
			//mid
			curKey.midSum += arr[toSet];
			curKey.numMidChosen++;
			choose(toSet+1, curKey, arr, tally);
			curKey.midSum -= arr[toSet];
			curKey.numMidChosen--;
			
			//right
			choose(toSet+1, curKey, arr, tally);
		}
	}
	
	static class Key2 {
		public Key2 copy(){
			return new Key2(leftSum, midSum, numLeftChosen, numMidChosen);
		}

		public long leftSum;
		public long midSum;
		public int numLeftChosen;
		public int numMidChosen;

		public Key2(long leftSum, long midSum, int numLeftChosen,
				int numMidChosen) {
			this.leftSum = leftSum;
			this.midSum = midSum;
			this.numLeftChosen = numLeftChosen;
			this.numMidChosen = numMidChosen;
		}
	}

	static class Key {
		public int numChosen;
		public long sum;

		public Key(int numChosen, long sum) {
			this.numChosen = numChosen;
			this.sum = sum;
		}

		@Override
		public boolean equals(Object other) {
			Key o = (Key) other;
			return this.numChosen == o.numChosen && this.sum == o.sum;
		}

		@Override
		public int hashCode() {
			return 31 * numChosen + (int) sum;
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}

		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
