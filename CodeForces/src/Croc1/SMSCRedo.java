package Croc1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class SMSCRedo {
	static int n;
	
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		
		long size = 0;
		//beginning of the iteration -> size of q before adding ith task
		
		long maxSize = 0;
		long arrivalTime = 0;
		long prevTime = 0;
		long length;
		for(int i=0; i<n;i++){
			arrivalTime = sc.nextInt();
			length = sc.nextInt();
			size = size - Math.min(size, arrivalTime - prevTime) + length;
			maxSize = Math.max(size, maxSize);
			prevTime = arrivalTime;
		}
		
		System.out.println((arrivalTime + size) + " " + maxSize);
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
