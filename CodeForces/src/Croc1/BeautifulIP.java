package Croc1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class BeautifulIP {
	static int n;
	static int[] nums;
	static int need;
	static List<int[]> ips = new ArrayList<int[]>();
	
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		if(n>6){
			System.out.println(0);
			return;
		}
		nums = new int[n];
		
		for(int i=0;i<n;i++){
			nums[i] = sc.nextInt();
			need |= (1<<nums[i]);
		}
		
		for(int length=4;length<=12;length++){
			int[] built = new int[length];
			genPal(0,built,length);
		}

		System.out.println(ips.size());
		StringBuffer res = new StringBuffer();
		for(int[] item : ips){
			res.append(String.format("%d.%d.%d.%d\n",item[0],item[1],item[2],item[3]));
		}
		if(ips.size() !=0)
			System.out.println(res);
	}
	
	public static void genPal(int toSet, int[] built, int length){
		if(toSet == (length+1)/2){
			int used = 0;
			for(int b: built){
				used |= (1<<b);
			}
			
			if((used & need) == need){
				divide(0, -1, new int[4],built);
			}
		}else{
			for(int num:nums){
				built[toSet] = built[length-toSet-1] = num;
				genPal(toSet+1, built, length);
			}
		}
	}
	
	//given pal consists of valid numbers
	public static void divide(int toSet, int prevEnd, int[] built, int[] pal){
		if(toSet == 3){
			int elLeft = pal.length-1-prevEnd;
			if(elLeft > 3 || elLeft <1){
				return;
			}else{
				int newTok = pal[prevEnd+1];
				if(pal[prevEnd+1] == 0){
					if(prevEnd+1 != pal.length-1){
						return;
					}
				}else{
					for(int i=prevEnd+2; i<pal.length; i++){
						newTok*=10;
						newTok += pal[i];
					}
				}
				if(newTok <= 255){
					built[3] = newTok;
					ips.add(new int[]{built[0],built[1],built[2],built[3]});
				} 
			}
		}else{
			int start = prevEnd+1;
			if(start >= pal.length-1){
				return;
			}
			if(pal[start] == 0){
				built[toSet] = 0;
				divide(toSet+1, prevEnd+1, built, pal);
			}else{
				for(int end=prevEnd+1; end<Math.min(prevEnd+4, pal.length); end++){
					int newTok = pal[start];
					for(int i=start+1; i<=end; i++){
						newTok*=10;
						newTok += pal[i];
					}
					if(newTok <= 255){
						built[toSet] = newTok;
						divide(toSet+1, end, built, pal);
					}
				}
			}
		}
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}



