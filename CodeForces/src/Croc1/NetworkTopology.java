package Croc1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class NetworkTopology {
	static int n;
	static int m;
	static Map<Integer, List<Integer>> adjMap;
	
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		m = sc.nextInt();
		adjMap = new HashMap<Integer, List<Integer>>();
		
		for(int i=0;i<m;i++){
			int from = sc.nextInt();
			int to = sc.nextInt();
			List<Integer> outs = adjMap.get(from);
			if(outs == null){
				outs = new ArrayList<Integer>();
				adjMap.put(from, outs);
			}
			
			outs.add(to);
			
			List<Integer> ins = adjMap.get(to);
			if(ins == null){
				ins = new ArrayList<Integer>();
				adjMap.put(to, ins);
			}
			
			ins.add(from);
		}
		
		List<Integer> ones = new ArrayList<Integer>();
		List<Integer> twos = new ArrayList<Integer>();
		List<Integer> others = new ArrayList<Integer>();
		for(Integer node : adjMap.keySet()){
			int length = adjMap.get(node).size();
			if(length == 1){
				ones.add(node);
			}else if(length == 2){
				twos.add(node);
			}else{
				others.add(node);
			}
		}
		
		//check bus
		if(ones.size() == 2 && others.size() == 0){
			Integer end = ones.get(0);
			int cur = adjMap.get(end).get(0);
			int prev = end;
			for(int visit = 1; visit < n; visit++){
				List<Integer> children = adjMap.get(cur);
				if(visit == n-1 && children.size() == 1){
					System.out.println("bus topology");
					return;
				}
				if(children.size() != 2){
					break;
				}
				
				int temp = cur;
				if(children.get(0) == prev){
					cur = children.get(1);
				}else{
					cur = children.get(0);
				}

				prev = temp;
			}
		}
		
		//check ring
		if(ones.size() == 0 && others.size() == 0 && twos.size() == n){
			System.out.println("ring topology");
			return;
		}
		
		//check star
		if(others.size() == 1 && adjMap.get(others.get(0)).size() == n-1 && ones.size() == n-1){
			System.out.println("star topology");
			return;
		}
		
		System.out.println("unknown topology");
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
