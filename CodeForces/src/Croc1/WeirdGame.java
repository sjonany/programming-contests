package Croc1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class WeirdGame {
	public static void main(String[] args){
		FastScanner sc=  new FastScanner();
		int n = sc.nextInt();
		String s = sc.nextToken();
		String t = sc.nextToken();
		int countSame= 0;
		int countS= 0;
		int countT= 0;
		int sOnes = 0;
		int tOnes = 0;
		for(int i=0;i<s.length();i++){
			char si = s.charAt(i);
			char ti = t.charAt(i);
			if(si == '1' && ti== '1'){
				countSame++;
			}else if(si == '0' && ti == '1'){
				countT++;
			}else if(si == '1' && ti == '0'){
				countS++;
			}
		}

		sOnes += (countSame+1)/2;
		tOnes += countSame/2;
		if(countSame % 2 != 0){
			//t can sneak in extra move
			if(countS >0){
				countS--;
			}else{
				if(countT>0){
					countT--;
					tOnes++;
				}
			}
		}
		//System.out.println(sOnes + " " + tOnes + " " + countS + " " + countT);
		if(countS == countT){
			if(sOnes == tOnes){
				System.out.println("Draw");
			}else{
				System.out.println("First");
			}
			return;
		}else if(countS < countT){
			int diff = countT-countS;
			if(diff %2 == 0){
				tOnes += diff/2;
			}else{
				tOnes += (diff-1)/2;
			}
		}else{
			int diff = countS-countT;
			if(diff %2 == 0){
				sOnes += diff/2;
			}else{
				sOnes += (diff+1)/2;
			}
		}
		if(sOnes == tOnes){
			System.out.println("Draw");
		}else if(sOnes >tOnes ){
			System.out.println("First");
		}else{
			System.out.println("Second");
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
