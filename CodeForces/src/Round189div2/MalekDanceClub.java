package Round189div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class MalekDanceClub {
	static long MOD = 1000000007;
	//dp[i] = number of pairs if the question is for the first (i+1) low bits of x, and strings of length i+1
	static long[] dp;
	public static void main(String[] args) throws Exception{
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		String s = sc.nextToken();
		dp = new long[s.length()];
		if(s.charAt(s.length()-1) == '1') dp[0] = 1;
		for(int i = 1; i < s.length(); i++){
			dp[i] = 2 * dp[i-1];
			if(s.charAt(s.length()-i-1) == '1'){
				dp[i] += pow(4,i);
			}
			dp[i] %= MOD;
		}
		
		out.println(dp[s.length()-1]);
		out.close();
	}
	
	/*
	 if s[i] = '0' or '1'
	 pairs extended by adding 0 0 or 1 1 to the front will still hold. that is. a<c and a^x > c^x are still valid for
	 the previous pairs. This gives 2*dp[i-1]
	 1 0 doesn't hold because a<c fails
	 0 1 doesn't hold if s[i]  = '0' because a^x < c^x
	 however, for s[i] = '1', 0 1 will hold for all pairs of length i-1 because a^x > c^x (flip bits by s[i] = 1)
	 */
	
	static long pow(long a, long e){
		if(e == 0){
			return 1;
		}
		if(e % 2 == 0){
			long ans = pow(a, e/2);
			return (ans * ans) % MOD;
		}else{
			return (a * pow(a, e-1)) % MOD;
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
