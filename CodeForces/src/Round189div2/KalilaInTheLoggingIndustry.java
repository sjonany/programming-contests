package Round189div2;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class KalilaInTheLoggingIndustry {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		long[] a = new long[n];
		long[] b = new long[n];
		for(int i = 0; i < n; i++){
			a[i] = in.nextLong();
		}
		for(int i = 0; i < n; i++){
			b[i] = in.nextLong();
		}
		
		//min cost to completely cut the ith tree, considering 1->i, excluding the last recharge
		long[] dp = new long[n];
		dp[0] = 0;
	
		//the visible lines
		Line[] stack = new Line[n];
		int top = 0;
		int lastJ = 0;
		stack[top] = new Line(b[0],dp[0],0);
		
		for(int i = 1; i < n; i++){
			//want to find min f(x), but restriction of the problem makes x ascending
			while(lastJ != top && f(a[i], stack[lastJ]) > f(a[i], stack[lastJ+1])){
				lastJ++;
			}
			dp[i] = dp[stack[lastJ].id] + (long)b[stack[lastJ].id] * a[i];
			
			
			//possible in this order because b_i, the "slope", is already sorted
			Line newLine = new Line(b[i],dp[i],i);
			while(top >= 1 && getXIntersect(newLine, stack[top-1]) >= getXIntersect(newLine, stack[top])){
				stack[top] = null;
				top--;
			}
			top++;
			stack[top] = newLine;
			lastJ = Math.min(lastJ, top);
		}
		out.println(dp[n-1]);
		out.close();
	}
	
	static long f(long x, Line l){
		return l.m*x+l.c;
	}
	
	static double getXIntersect(Line l1, Line l2){
		return 1.0 * (l2.c-l1.c) / (l1.m - l2.m);
	}
	
	static class Line{
		public Line(long m, long c, int id) {
			this.id = id;
			this.m = m;
			this.c = c;
		}
		
		@Override
		public String toString(){
			return String.format("%d : y = %dx + %d",id, m,c);
		}
		
		public int id;
		public long m;
		public long c;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
