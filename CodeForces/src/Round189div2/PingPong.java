package Round189div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class PingPong {
	static int top;
	static Interval[] items = new Interval[101];
	
	public static void main(String[] args) throws Exception{
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = sc.nextInt();
		top = 1;
		for(int i = 0; i < n; i++){
			int t = sc.nextInt();
			Interval q = new Interval(sc.nextInt(),sc.nextInt());
			if(t == 1){
				items[top] = q;
				top++;
			}else{
				if(dfs(q.a, q.b, new boolean[top])){
					out.println("YES");
				}else{
					out.println("NO");
				}
			}
		}
		
		out.close();
	}
	
	static boolean dfs(int cur, int to, boolean[] visit){
		if(visit[cur]){
			return false;
		}
		if(cur == to){
			return true;
		}
		
		visit[cur] = true;
		for(int i = 1; i < visit.length; i++){
			if(!visit[i] && items[cur].canMove(items[i])){
				if(dfs(i, to, visit)){
					return true;
				}
			}
		}
		return false;
	}
	
	static class Interval{
		public int a;
		public int b;
		public Interval(int a, int b){
			this.a = a;
			this.b = b;
		}
		
		public boolean canMove(Interval other){
			return (a < other.b && a > other.a) ||
					(b < other.b && b > other.a);
		}
		
		@Override
		public boolean equals(Object o){
			Interval other = (Interval)o;
			return other.a == this.a && other.b == this.b;
		}
		
		@Override
		public int hashCode(){
			return a + 31 * b;
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
