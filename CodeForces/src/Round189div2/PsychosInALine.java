package Round189div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class PsychosInALine {
	public static void main(String[] args) throws Exception{
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n  = sc.nextInt();
		int[] arr = new int[n];
		
		//doubly linked list
		int[] next = new int[n];
		int[] prev = new int[n];

		//indices of people who last killed
		//these are the only people who could possibly kill again
		//because it can't be that the big guy next to him gets deleted and he doesn't. So he remains blocked
		List<Integer> lastKilled = new ArrayList<Integer>();
		
		
		for(int i = 0; i < arr.length; i++){
			arr[i] = sc.nextInt();
			next[i] = i+1;
			prev[i] = i-1;
			lastKilled.add(i);
		}
		
		prev[0] = -1;
		next[arr.length-1] = -1;
		
		int turn  = 0;
		while(lastKilled.size() != 0){
			List<Integer> newLastKilled = new ArrayList<Integer>();
			
			for(int killer : lastKilled){
				int victim = next[killer];
				if(victim == -1)
					continue;
			
				if(arr[killer] > arr[victim]){
					//it could be that a dead person killed. but we shouldn't count them.
					if(prev[victim] == killer)
						newLastKilled.add(killer);
					
					if(next[victim] != -1){
						prev[next[victim]] = prev[victim];
					}

					//need to modify based on prev because a dead person can kill too, and we need
					//to update the next of the previous alive person
					//so.. next[killer] = next[victim] is wrong!
					next[prev[victim]] = next[victim];
				}
				//if less than, no need to kill, because that next guy is going to kill for you anyways
				//we still have to go through all previous killers, even those who died.
				//we note that their right link is still maintained, and they can kill and die simultaneously in 1 turn.
			}
			
			turn++;
			lastKilled = newLastKilled;
		}
		
		out.println(turn-1);
		out.close();
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
