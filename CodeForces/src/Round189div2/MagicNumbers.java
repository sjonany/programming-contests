package Round189div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class MagicNumbers {
	public static void main(String[] args) throws Exception{
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		String s = sc.nextToken();
		if(isMagic(s)){
			out.println("YES");
		}else{
			out.println("NO");
		}
		
		out.close();
	}
	
	public static boolean isMagic(String s){
		if(s.length() == 0){
			return true;
		}
		
		if(s.startsWith("1") && isMagic(s.substring(1))){
			return true;
		}
		
		if(s.startsWith("14") && isMagic(s.substring(2))){
			return true;
		}
		
		if(s.startsWith("144") && isMagic(s.substring(3))){
			return true;
		}
		return false;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
