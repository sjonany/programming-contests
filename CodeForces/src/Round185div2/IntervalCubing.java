package Round185div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class IntervalCubing {
	public static final int MOD = 95542721;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int[] arr = new int[n];
		for(int i=0;i<n;i++){
			arr[i] = sc.nextInt();
		}
		PrintWriter out = new PrintWriter(System.out);
		LazySegmentTree tree = new LazySegmentTree(arr);
		int m = sc.nextInt();
		for(int i=0;i<m;i++){
			int type = sc.nextInt();
			int l = sc.nextInt();
			int r = sc.nextInt();
			if(type == 2){
				tree.update(l-1, r-1);
			}else{
				out.println(tree.query(l-1, r-1));
			}
		}
		out.close();
	}
	
	public static class LazySegmentTree {
		public static final int MOD = 95542721;
		
		//1-based
		int[][] tree;
		int[] lazy;

		//num els in the original array
		int n;
		public LazySegmentTree(int[] arr){
			this.n = arr.length;
			//num nodes in tree = 2*n-1 <= 2*2^ceil(log_2 (n)) - 1 
			int numNodes = 2 * (int)(Math.pow(2,Math.ceil(Math.log(n)/Math.log(2))))-1;
			tree = new int[48][numNodes+1];
			lazy = new int[numNodes+1];
			buildTree(1, 0, arr.length-1, arr);
		}

		//a and b are 0-based indices of the original array
		private void buildTree(int node, int a, int b, int[] arr){
			if(a > b) return;

			//leaf node
			if(a == b){
				tree[0][node] = arr[a];
				for(int i=1;i<48;i++){
					long cube = tree[i-1][node];
					cube = (cube * tree[i-1][node]) % MOD;
					cube = (cube * tree[i-1][node]) % MOD;
					tree[i][node] = (int) cube;
				}
				return;
			}
			int mid = (a+b)/2;
			buildTree(2*node, a, mid, arr);
			buildTree(2*node+1, mid+1, b, arr);

			for(int i=0;i<48;i++){
				tree[i][node] = (tree[i][2*node] + tree[i][2*node+1]) % MOD;
			}
		}

		/**
		 * perform operation on i->j based on original array index
		 */
		public void update(int i, int j){
			update(1, 0, n-1, i, j);
		}

		public long query(int i, int j){
			return query(1,0,n-1,i,j, 0);
		}
		
		//guarantee: i,j is between a,b. a,b, is current node's range
		/**
		 * Increment elements within range [i, j] with value value
		 */
		private void update(int node, int a, int b, int i, int j) {
			if(a == i && b == j){
				lazy[node] += 1;
				if(lazy[node] == 48){
					lazy[node] = 0;
				}
				return;
			}else{
				int mid = (a+b)/2;
				if(i <= mid) update(node * 2, a, mid, i, Math.min(j, mid));
				if(j > mid) update(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j);
				
				int left = 2*node;
				int right = 2*node+1;
				int leftI = lazy[left];
				int rightI = lazy[right];
				for(int k=0;k<48;k++){
					tree[k][node] = (tree[leftI][left] + tree[rightI][right]) % MOD;
					leftI++;
					rightI++;
					if(leftI == 48){
						leftI = 0;
					}
					
					if(rightI == 48){
						rightI = 0;
					}
				}
			}
		}

		/**
		 * Query tree to get max element value within range [i, j]
		 */
		private long query(int node, int a, int b, int i, int j, int sumLazy) {
			sumLazy += lazy[node];
			if(a == i && b == j){
				return tree[sumLazy%48][node];
			}
			else {
				long res = 0;
				int mid = (a+b)/2;

				if(i <= mid) res += query(node * 2, a, mid, i, Math.min(j, mid), sumLazy);
				if(j > mid) res += query(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j, sumLazy);
				
				return res % MOD;
			}
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
