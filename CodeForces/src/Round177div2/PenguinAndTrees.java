package Round177div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class PenguinAndTrees {
	static Map<Integer, List<Integer>> adjMap;
	//size of subtree rooted at v
	static long[] treeSize;
	static long n;
	//number of pairs of paths that intersect
	static long totalIntersectPair = 0;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		if(n == 1){
			System.out.println(0);
			return;
		}
		adjMap = new HashMap<Integer, List<Integer>>();
		treeSize = new long[(int)n+1];
		for(int i=0;i<n-1;i++){
			int v1 = sc.nextInt();
			int v2 = sc.nextInt();
			List<Integer> out1 = adjMap.get(v1);
			if(out1 == null){
				out1 = new ArrayList<Integer>();
				adjMap.put(v1, out1);
			}
			
			out1.add(v2);

			List<Integer> out2 = adjMap.get(v2);
			if(out2 == null){
				out2 = new ArrayList<Integer>();
				adjMap.put(v2, out2);
			}
			
			out2.add(v1);
		}
		
		long totalPath = n*(n-1)/2;
		long totalPair = totalPath * totalPath;
		getNumIntersect(0,1);
		System.out.println(totalPair - totalIntersectPair);
	}
	
	//for every intersecting pair, we can uniquely identify it by its toppest intersected node
	static void getNumIntersect(int parent, int cur){
		treeSize[cur] = 1;
		long numPathNotTouchRoot = 0;
		for(int child : adjMap.get(cur)){
			if(child != parent){
				getNumIntersect(cur, child);
				treeSize[cur] += treeSize[child];
				numPathNotTouchRoot += treeSize[child]*(treeSize[child]-1)/2;
			}
		}
		long totPaths = treeSize[cur] * (treeSize[cur]-1)/2;
		//path that touches root, and belongs in this subtree only
		long pathTouchesRoot = totPaths - numPathNotTouchRoot;
		//path that touches root, and goes up to the big top tree, uniquely identified by a 
		//node in this subtree, and another in the upper big tree
		long remNode = n-treeSize[cur];
		long pathFromBottomToTop = remNode * treeSize[cur];
		//either exactly one pair goes up, or both of them stay in the subtree
		//can't have all go up -> intersected node exists higher up -> contradiction
		totalIntersectPair += pathTouchesRoot*pathTouchesRoot + 2 * pathTouchesRoot * pathFromBottomToTop;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
