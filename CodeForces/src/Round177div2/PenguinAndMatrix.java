package Round177div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class PenguinAndMatrix {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int m = sc.nextInt();
		int d = sc.nextInt();
		
		Integer[] arr = new Integer[n*m];
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.nextInt();
		}
	
		Arrays.sort(arr);
		//how many increments to fix up to 0..i
		int[] inc = new int[n*m];
		int[] dec = new int[n*m];
		
		for(int i=1;i<arr.length;i++){
			if(arr[i] != arr[i-1]){
				int diff = arr[i]-arr[i-1];
				if(diff % d != 0){
					System.out.println(-1);
					return;
				}else{
					inc[i] = inc[i-1] + diff/d * i;
				}
			}else{
				inc[i] = inc[i-1];
			}
		}
		
		for(int i=arr.length-2;i>=0;i--){
			if(arr[i] != arr[i+1]){
				int diff = arr[i+1]-arr[i];
				if(diff % d != 0){
					System.out.println(-1);
					return;
				}else{
					dec[i] = dec[i+1] + diff/d * (arr.length-i-1);
				}
			}else{
				dec[i] = dec[i+1];
			}
		}

		int min = Integer.MAX_VALUE;
		for(int i=0;i<arr.length;i++){
			min = Math.min(min, inc[i] + dec[i]);
		}
		
		System.out.println(min);
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
