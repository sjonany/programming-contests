package Round177div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PenguinAndHouses {
	static long MOD = 1000000007;
	static long firstGroup = 0;
	static long secondGroup = 0;
	static int n;
	static int k;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		n = sc.nextInt();
		k = sc.nextInt();
		
		//house 1 to k
		int[] firstHs = new int[k+1];
		for(int i=1;i<=k;i++){
			firstHs[1] =i;
			search(firstHs, 2);
		}
		
		//second group
		secondGroup = exp(n-k, n-k);
		//System.out.println(secondGroup);
		System.out.println((firstGroup * secondGroup) % MOD);
	}
	
	public static long exp(long a, long n){
		if(n==0){
			return 1;
		}

		if(n % 2 == 0){
			long r = exp(a, n/2);
			return (r*r) % MOD;
		}else{
			return (a * exp(a,n-1)) % MOD;
		}
	}
	
	public static void search(int[] firstHs, int toSet){
		if(toSet == k+1){
			boolean isValid=  true;
			for(int i=1;i<=k;i++){
				boolean canReachOne = false;
				int curHouse = i;
				curHouse = firstHs[curHouse];
				for(int move = 0; move <k; move++){
					if(curHouse == 1){
						canReachOne = true;
						break;
					}else{
						curHouse = firstHs[curHouse];
					}
				}
				if(!canReachOne){
					isValid = false;
					break;
				}
			}
			if(isValid){
				firstGroup = (firstGroup + 1) % MOD;
			}
		}else{
			for(int i=1;i<=k;i++){
				if(i!= toSet){
					firstHs[toSet] = i;
					search(firstHs, toSet+1);
				}
			}
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
