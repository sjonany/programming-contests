package Round177div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PenguinAndStrings {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int k = sc.nextInt();
		if(n < k){
			System.out.println(-1);
			return;
		}
		
		if(k==1){
			if(n == 1){
				System.out.println("a");
			}else{
				System.out.println(-1);
			}
		}else{
			int i =0;
			while(i<n){
				System.out.print("a");
				i++;
				if(n-i == k-2){
					for(char c ='c'; c<= 'a' + (k-1); c++){
						System.out.print(c);
					}
					return;
				}
				System.out.print("b");
				i++;
				if(n-i == k-2){
					for(char c ='c'; c<= 'a' + (k-1); c++){
						System.out.print(c);
					}
					return;
				}
			}
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
