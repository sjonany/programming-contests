package Round93div1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//BUG
public class PasswordKmp {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		String s = sc.nextToken();

		int[] F = buildPrefixFcn(s);
		int maxInternal = 0;
		for(int i = 2; i < s.length(); i++){
			maxInternal = Math.max(maxInternal, F[i]);
		}
		int end = F[s.length()];
		int ans = Math.min(end, maxInternal);
		if(ans == 0){
			System.out.println("Just a legend");
		}else{
			System.out.println(s.substring(0, ans));
		}
	}
	
	static int[] buildPrefixFcn(String s){
		int[] F = new int[s.length()+1];
		F[0] = F[1] = 0;
		for(int i = 2; i < F.length; i++){
			int j = F[i-1];
			while(true){
				if(s.charAt(j) == s.charAt(i-1)){
					F[i] = j+1;
					break;
				}
				if(j == 0) {
					F[i] = 0;
					break;
				}
				
				j = F[j];
			}
		}
		return F;
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
