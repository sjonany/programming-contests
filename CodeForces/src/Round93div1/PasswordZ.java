package Round93div1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PasswordZ {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		String s = sc.nextToken();

		int[] z = new int[s.length()];
		int n = s.length();
		int L = 0, R = 0;
		//note that maxZ does not care about z[0] = n
		//so it keeps track only of length of longest infix
		int maxZ = 0;
		for (int i = 1; i < n; i++) {
			if (i > R) {
				//bf
				L = R = i;
				while (R < n && s.charAt(R-L) == s.charAt(R)) R++;
				z[i] = R-L; R--;
			} else {
				int k = i-L;
				//beta inside alpha, and closed by a1 != a2
				if (z[k] < R-i+1) z[i] = z[k];
				else {
					//bf
					L = i;
					while (R < n && s.charAt(R-L) == s.charAt(R)) R++;
					z[i] = R-L; R--;
				}
			}
			if(z[i] == n-i && maxZ >= n-i){
				System.out.println(s.substring(0,z[i]));
				return;
			}
			maxZ = Math.max(z[i],maxZ);
		}
		
		System.out.println("Just a legend");
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
