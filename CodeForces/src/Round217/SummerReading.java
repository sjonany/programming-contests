package Round217;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;

/**
10
1 0 0 0 3 0 0 4 0 0
 */
public class SummerReading {

	//need to handle same book cases 1..1..1..1 shouldn't be possible.just greedy stream them, then divide into subcases

	// at state[i], if i am reading a[i] to kth day, looking at the previous marked day
	// which book state was I in
	static int[][] prevbookday;
	// look back, and store how many days total to finish prev book
	static int[][] prevbookfinday;
	static List<State> states;
	static InputReader in;
	static PrintWriter out;
	
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		int N = in.nextInt();
		int[] a = new int[N];
		for(int i=0;i<N;i++) {
			a[i] = in.nextInt();
		}
		
		if(a[0] >= 2) {
			out.println(-1);
			out.close();
			return;
		}

		states = new ArrayList<State>();
		states.add(new State(0, 1));
		a[0] = 1;
		for(int day=1; day < N; day++) {
			if(a[day] != 0){
				states.add(new State(day, a[day]));
			}
		}

		for(int i = 0; i < states.size()-1; i++) {
			if(states.get(i).book > states.get(i+1).book) {
				out.println(-1);
				out.close();
				return;
			}
		}
		
		boolean isspeciallast = false;
		if(states.get(states.size()-1).day != N-1) {
			states.add(new State(N-1, -1));
			isspeciallast = true;
		}
		prevbookday = new int[states.size()][6];
		prevbookday[0][1] = 1;
		prevbookfinday = new int[states.size()][6];

		for(int i=1; i < states.size(); i++) {
			if(isspeciallast && i == states.size()-1) { 
				State orilaststate = states.get(states.size()-2);
				int dayleft = N - orilaststate.day - 1;
				int maxbook = orilaststate.book + dayleft/2;
				int minbook = orilaststate.book + (dayleft-1)/2;
				states.get(states.size()-1).book = maxbook;
				solve(i);
				boolean good = false;
				for(int day=2;day<=5;day++){
					if(prevbookday[i][day] != 0) {
						good = true;
						break;
					}		
				}
				if(good)break;
				states.get(states.size()-1).book = minbook;
				solve(i);
			} else {
				solve(i);
			}
		}

		int[] record = new int[N];
		int lastbookprogress = 0;
		for(int i=5; i>=2;i--) {
			if(prevbookday[states.size()-1][i] != 0) {
				lastbookprogress = i;
				break;
			}
		}

		if(lastbookprogress==0){
			out.println(-1);
			out.close();
			return;
		}

		// recover prevsteps
		for(int si = states.size()-1; si > 0; si--) {
			if(states.get(si).book == states.get(si-1).book) {
				for(int i=states.get(si-1).day; i <= states.get(si).day; i++){
					record[i] = states.get(si).book;
				}
				lastbookprogress = prevbookday[si][lastbookprogress];
				continue;
			} 
			int endday = states.get(si).day;
			int endbook = states.get(si).book;
			for(int i = 0; i < lastbookprogress; i++) {
				record[endday] = endbook;
				endday--;
			}
			endbook--;
			int prevbookfind = prevbookfinday[si][lastbookprogress];
			int prevbookprogress = prevbookday[si][lastbookprogress];
			int startbook = states.get(si-1).book;
			int startday = states.get(si-1).day;
			int dayleft = prevbookfind - prevbookprogress;
			for(int i = 0; i <= dayleft; i++) {
				record[startday] = startbook;
				startday++;
			}
			startbook++;

			int[] count = new int[6];
			int blockstofill = endday - startday + 1;
			int numblocks = endbook-startbook+1;
			count[2] = numblocks;
			int blockneeded = blockstofill - 2 * numblocks;
			count[5] += blockneeded / 3;
			count[2] -= count[5];
			blockneeded -= count[5] * 3;
			if(blockneeded != 0) {
				count[2]--;
				count[2+blockneeded]++;
			}

			int curday = startday;
			int curbook = startbook;
			int blockcount = 1;
			while(curday <= endday) {
				while(count[blockcount] == 0){
					blockcount++;
				}
				for(int i=0;i<blockcount;i++){
					record[curday] = curbook;
					curday++;
				}
				count[blockcount]--;
				curbook++;
			}
			lastbookprogress = prevbookprogress;
		}

		int lastbook = record[N-1];
		out.println(lastbook);
		for(int i=0;i<N;i++){
			out.print(record[i] + " ");
		}
		out.close();
	}
	
	static void solve(int i) {
		for(int curbookprogress = 1; curbookprogress <= 5; curbookprogress++) {
			if(states.get(i).book == states.get(i-1).book) {
				int prevprogress = curbookprogress - (states.get(i).day - states.get(i-1).day);
				if(prevprogress >= 1 && prevprogress <= 5 && prevbookday[i-1][prevprogress] != 0) {
					prevbookday[i][curbookprogress] = prevprogress;
				}
				continue;
			}
			for(int prevbookprogress = 1; prevbookprogress <= 5; prevbookprogress++) {
				if(prevbookday[i-1][prevbookprogress] == 0) {
					continue;
				} else {
					boolean ispossible = false;

					for(int fintime = Math.max(2, prevbookprogress); fintime <= 5; fintime++) {
						int startfillday = fintime - prevbookprogress + states.get(i-1).day + 1;
						int endfillday = states.get(i).day - curbookprogress;
						int blockstofill = endfillday - startfillday + 1;
						int numblocks = states.get(i).book - states.get(i-1).book - 1;

						if(blockstofill < 0) {
							continue;
						}

						if(2 * numblocks <= blockstofill && blockstofill <= 5 * numblocks) {
							prevbookfinday[i][curbookprogress] = fintime;
							prevbookday[i][curbookprogress] = prevbookprogress;
							ispossible = true;
							break;
						}
					}
					if(ispossible) {
						break;
					}
				}
			}
		}
	}
	
	static class State {
		//from zero
		public int day;
		//from one
		public int book;
		public State(int day, int book) {
			this.day = day;
			this.book = book;
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
