package Round217;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;


public class BrokenMonitor {
	static char DOT = '.';
	static char W = 'w';
	static char PLUS = '+';
	static int[][] count;
	static int ROW;
	static int COL;
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		ROW = in.nextInt();
		COL = in.nextInt();
		
		char[][] board = new char[ROW][COL];
		for(int i=0;i<ROW;i++){
			String tok = in.nextToken();
			for(int j=0;j<COL;j++){
				board[i][j] = tok.charAt(j);
			}
		}
		
		//num w's from top left to i,j
		count = new int[ROW][COL];
		for(int r=0;r<ROW;r++) {
			int curCountRow = 0;
			for(int c=0;c<COL;c++) {
				boolean isW = board[r][c] == W;
				if(isW) {
					curCountRow++;
				}
				count[r][c] = curCountRow;
				if(r != 0){
					count[r][c] += count[r-1][c];
				}
			}	
		}
		
		int left = 0;
		int right = COL-1;
		int bottom = ROW-1;
		int top = 0;
		while(countCol(left) == 0){
			left++;
		}
		
		while(countCol(right) == 0){
			right--;
		}
		
		while(countRow(top) == 0) {
			top++;
		}
		
		while(countRow(bottom) == 0){
			bottom --;
		}
		
		int totW = count[ROW-1][COL-1];
		
		int ansl=-1;
		int ansr=-1;
		int ansb=-1;
		int anst=-1;
		int minSz = Integer.MAX_VALUE;
		for(int exl = 0; exl <= left; exl++) {
			for(int exr = right; exr < COL; exr++) {
				int sz = exr-exl;
				if(minSz <= sz) continue;
				//if bottom expand
				int newbot = top + sz;
				if(newbot < ROW  && newbot >= bottom) {
					if(countFrame(top, newbot, exl, exr) == totW) {
						if(minSz > sz){
							minSz = sz;
							ansl = exl;
							ansr = exr;
							anst = top;
							ansb = newbot;
						}
					}
				}
				
				//if top expand
				int newtop = bottom - sz;
				if(newtop >= 0 && newtop <= top) {
					if(countFrame(newtop, bottom, exl, exr) == totW) {
						if(minSz > sz){
							minSz = sz;
							ansl = exl;
							ansr = exr;
							anst = newtop;
							ansb = bottom;
						}
					}
				}
			}
		}
		
		for(int ext = 0; ext <= top; ext++) {
			for(int exb = bottom; exb < ROW; exb++) {
				int sz = exb-ext;
				if(minSz <= sz) continue;
				//if left expand
				int newleft = right - sz;
				if(newleft >=0 && newleft <= left) {
					if(countFrame(ext, exb, newleft, right) == totW) {
						if(minSz > sz){
							minSz = sz;
							ansl = newleft;
							ansr = right;
							anst = ext;
							ansb = exb;
						}
					}
				}
				
				//if right expand
				int newright = left + sz;
				if(newright < COL && newright >= right) {
					if(countFrame(ext, exb, left, newright) == totW) {
						if(minSz > sz){
							minSz = sz;
							ansl = left;
							ansr = newright;
							anst = ext;
							ansb = exb;
						}
					}
				}
			}
		}
		
		if(minSz == Integer.MAX_VALUE) {
			out.println(-1);
		}else{
			for(int c=ansl;c <=ansr;c++) {
				if(board[anst][c] == DOT) {
					board[anst][c] = PLUS;
				}
				if(board[ansb][c] == DOT) {
					board[ansb][c] = PLUS;
				}
			}
			for(int r=anst;r <=ansb;r++) {
				if(board[r][ansl] == DOT) {
					board[r][ansl] = PLUS;
				}
				if(board[r][ansr] == DOT) {
					board[r][ansr] = PLUS;
				}
			}
			for(int r=0;r<ROW;r++) {
				for(int c=0;c<COL;c++) {
					out.print(board[r][c]);
				}
				out.println();
			}
		}
		out.close();
	}
	
	static int countRow(int r) {
		if(r==0) return count[0][COL-1];
		return count[r][COL-1] - count[r-1][COL-1];
	}
	
	static int countCol(int c) {
		if(c==0) return count[ROW-1][0];
		return count[ROW-1][c] - count[ROW-1][c-1];
	}
	
	static int countFrame(int top, int bot, int left, int right) {
		int exterior = countSq(top, bot, left, right);
		if(bot != top && left != right) {
			return exterior - countSq(top+1, bot-1, left+1, right-1);
		} else {
			return exterior;
		}
	}
	
	static int countSq(int top, int bot, int left, int right) {
		int ans = count[bot][right];
		if(left > 0) {
			ans -= count[bot][left-1];
		}
		if(top > 0) {
			ans -= count[top-1][right];
			if(left > 0){
				ans += count[top-1][left-1];
			}
		}
		return ans;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
