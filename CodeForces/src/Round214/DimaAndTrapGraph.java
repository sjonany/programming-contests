package Round214;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class DimaAndTrapGraph {
	static List<Edge>[] edges;
	static int N;
	static int M;
	static int[] lefts;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk = tk(in.readLine());
		
		N = Integer.parseInt(tk.nextToken());
		M = Integer.parseInt(tk.nextToken());
		
		edges = new List[N];
		for(int i=0;i<N;i++)edges[i] = new ArrayList();
		
		lefts = new int[M];
		for(int i=0;i<M;i++) {
			tk=tk(in.readLine());
			int a = Integer.parseInt(tk.nextToken())-1;
			int b = Integer.parseInt(tk.nextToken())-1;
			int l = Integer.parseInt(tk.nextToken());
			int r = Integer.parseInt(tk.nextToken());
			lefts[i] = l;
			edges[a].add(new Edge(a,b,l,r));
			edges[b].add(new Edge(b,a,l,r));
		}
		
		int lo = 1;
		int hi = Integer.MAX_VALUE/3;
		
		visit = new int[N];
		if(!check(1)){
			out.println("Nice work, Dima!");
			out.close();
			return;
		}
		
		while(lo<hi){
			int mid = (lo + hi + 1) /2;
			if(check(mid)) {
				lo = mid;
			}else{
				hi = mid-1;
			}
		}
		out.println(lo);
		out.close();
	}
	
	static int id = 0;
	static int[] visit;
	static int sl;
	static int sr;
	static boolean check(int x) {
		for(int l : lefts) {
			sl = l;
			sr = sl + x - 1;
			id++;
			if(dfs(0)){
				return true;
			}
		}
		return false;
	}
	
	static boolean dfs(int cur) {
		if(cur == N-1) return true;
		visit[cur] = id;
		for(Edge e : edges[cur]) {
			if(visit[e.to] != id && sl >= e.l && sr <= e.r) {
				if(dfs(e.to)){
					return true;
				}
			}
		}
		return false;
	}
	
	static class Edge {
		public int from;
		public int to;
		public int l;
		public int r;
		
		public Edge(int from, int to, int l, int r) {
			this.from = from;
			this.to = to;
			this.l = l;
			this.r = r;
		}
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
