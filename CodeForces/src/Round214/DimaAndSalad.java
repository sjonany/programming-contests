package Round214;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class DimaAndSalad {
	static int OFFSET = 1000 * 100;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		StringTokenizer tk = tk(in.readLine());
		int n = Integer.parseInt(tk.nextToken());
		int k = Integer.parseInt(tk.nextToken());
		
		int[] a = new int[n];
		int[] b = new int[n];
		
		tk = tk(in.readLine());
		for(int i=0;i<n;i++) {
			a[i] = Integer.parseInt(tk.nextToken());
		}
		
		tk = tk(in.readLine());
		for(int i=0;i<n;i++) {
			b[i] = Integer.parseInt(tk.nextToken());
		}
		
		int[] v = new int[n];
		for(int i=0;i<n;i++) {
			v[i] = a[i] - b[i] * k;
		}
		
		//dp[i][j] = max numerator if possible to make sum v[] = j, or -1 
		int[][] dp = new int[n][100*100 + OFFSET + 5];
		Arrays.fill(dp[0], -1);
		dp[0][0 + OFFSET] = 0;
		dp[0][v[0] + OFFSET] = a[0];
		for(int i = 1; i < n; i++) {
			for(int j = 0; j < dp[0].length; j++) {
				int val = j - OFFSET;
				//if i is in pot
				int prevIndex = val-v[i]+OFFSET;
				int ifinpot = prevIndex >= 0  && prevIndex < dp[0].length && dp[i-1][prevIndex] != -1 ? dp[i-1][prevIndex] + a[i] : -1;
				int ifnotinpot = dp[i-1][j];
				dp[i][j] = Math.max(ifinpot, ifnotinpot);
			}
		}
		if(dp[n-1][0+OFFSET] <= 0) out.println(-1);
		else out.println(dp[n-1][0+OFFSET]);
		out.close();
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
