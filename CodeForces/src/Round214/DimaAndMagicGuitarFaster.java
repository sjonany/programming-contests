package Round214;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;

public class DimaAndMagicGuitarFaster {
	
	//in this col, what is the min/max row for i?
	static short[][] minRow;
	static short[][] maxRow;
	
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		int R = in.nextInt();
		int C = in.nextInt();
		int K = in.nextInt();
		int S = in.nextInt();
		minRow = new short[10][C];
		maxRow = new short[10][C];
		for(int i=1;i<=9;i++){
			for(int c=0;c<C;c++){
				minRow[i][c] = -1;
				maxRow[i][c] = -1;
			}
		}
		
		short[][] board = new short[R][C];
		for(int r=0;r<R;r++) {
			for(int c=0;c<C;c++){
				board[r][c] = (short)in.nextInt();
			}
		}
		
		boolean[][] eval = new boolean[10][10];
		int[] qs = new int[S];
		for(int i=0;i<S;i++) {
			qs[i] = in.nextInt();
		}
		for(int i=0;i<S-1;i++){
			int lo = qs[i];
			int hi = qs[i+1];
			if(lo>hi){
				int temp=lo;
				lo=hi;
				hi=temp;
			}
			eval[lo][hi] = true;
		}
		
		//max of x+y, x-y, -x+y, -x-y
		int[] xpy = new int[10];
		int[] xmy = new int[10];
		int[] ymx = new int[10];
		int[] mxmy = new int[10];
		Arrays.fill(xpy, Integer.MIN_VALUE);
		Arrays.fill(xmy, Integer.MIN_VALUE);
		Arrays.fill(ymx, Integer.MIN_VALUE);
		Arrays.fill(mxmy, Integer.MIN_VALUE);
		
		for(int r=0;r<R;r++) {
			for(int c=0;c<C;c++) {
				xpy[board[r][c]] = Math.max(xpy[board[r][c]], r+c);
				xmy[board[r][c]] = Math.max(xmy[board[r][c]], c-r);
				ymx[board[r][c]] = Math.max(ymx[board[r][c]], r-c);
				mxmy[board[r][c]] = Math.max(mxmy[board[r][c]], -r-c);
			}	
		}
		
		int ans = 0;
		for(int lo=0;lo<10;lo++) {
			for(int hi=lo;hi<10;hi++) {
				if(eval[lo][hi]) {
					ans = Math.max(ans, xpy[lo] + mxmy[hi]);
					ans = Math.max(ans, xmy[lo] + ymx[hi]);
					ans = Math.max(ans, ymx[lo] + xmy[hi]);
					ans = Math.max(ans, mxmy[lo] + xpy[hi]);
				}
			}	
		}
		out.println(ans);
		out.close();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
