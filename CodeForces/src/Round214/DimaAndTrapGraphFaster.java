package Round214;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class DimaAndTrapGraphFaster {
	static List<Edge>[] edges;
	static int N;
	static int M;
	static long[] lefts;
	static long[] rights;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk = tk(in.readLine());
		
		N = Integer.parseInt(tk.nextToken());
		M = Integer.parseInt(tk.nextToken());
		
		edges = new List[N];
		for(int i=0;i<N;i++)edges[i] = new ArrayList();
		
		lefts = new long[M];
		rights = new long[M];
		for(int i=0;i<M;i++) {
			tk=tk(in.readLine());
			int a = Integer.parseInt(tk.nextToken())-1;
			int b = Integer.parseInt(tk.nextToken())-1;
			int l = Integer.parseInt(tk.nextToken());
			int r = Integer.parseInt(tk.nextToken());
			lefts[i] = l;
			rights[i] = r;
			edges[a].add(new Edge(a,b,l,r));
			edges[b].add(new Edge(b,a,l,r));
		}
		
		int maxLength = -1;
		int li = 0;
		int ri = 0;
		
		visit = new int[N];
		Arrays.sort(lefts);
		Arrays.sort(rights);
		while(true) {
			if(li>=M || ri>=M)break;
			int l = (int)lefts[li];
			int r = (int)rights[ri];
			if(check(l,r)) {
				maxLength = Math.max(maxLength, r-l+1);
				ri++;
			} else {
				li++;
			} 
		}
		
		if(maxLength <= 0) {
			out.println("Nice work, Dima!");
		} else {
			out.println(maxLength);
		}
		out.close();
	}
	
	static int id = 0;
	static int[] visit;
	static int sl;
	static int sr;
	static boolean check(int l, int r) {
		if(r<l)return true;
		sl = l;
		sr = r;
		id++;
		return dfs(0);
	}
	
	static boolean dfs(int cur) {
		if(cur == N-1) return true;
		visit[cur] = id;
		for(Edge e : edges[cur]) {
			if(visit[e.to] != id && sl >= e.l && sr <= e.r) {
				if(dfs(e.to)){
					return true;
				}
			}
		}
		return false;
	}
	
	static class Edge {
		public int from;
		public int to;
		public int l;
		public int r;
		
		public Edge(int from, int to, int l, int r) {
			this.from = from;
			this.to = to;
			this.l = l;
			this.r = r;
		}
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
