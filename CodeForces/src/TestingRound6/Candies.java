package TestingRound6;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Candies {
	public static void main(String[] args){
		FastScanner sc= new FastScanner();
		int n = sc.nextInt();
		int m = sc.nextInt();
		int small = n/m;
		for(int numSmall=0;numSmall<=m;numSmall++){
			int numBig = m-numSmall;
			if(numBig *(small+1) + numSmall*small == n){
				StringBuffer out = new StringBuffer();
				for(int i=0;i<m;i++){
					out.append((i<numSmall? small : small+1) + (i!=m-1 ? " " : ""));
				}
				System.out.println(out);
			}
		}
	}
	
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
