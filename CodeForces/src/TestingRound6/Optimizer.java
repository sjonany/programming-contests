package TestingRound6;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class Optimizer {
	public static void main(String[] args){
		FastScanner sc= new FastScanner();
		int n = sc.nextInt();
		int m = sc.nextInt();
		List<Range> ranges = new ArrayList<Range>();
		List<Integer> unnecessaryId = new ArrayList<Integer>();
		for(int i=1;i<=m;i++){
			int from = sc.nextInt();
			int to = from + sc.nextInt()-1;
			ranges.add(new Range(from,to,i));
		}
		
		Collections.sort(ranges, new Comparator<Range>() {
			@Override
			public int compare(Range o1, Range o2) {
				if(o1.from != o2.from){
					return o1.from - o2.from;
				}
				
				return o2.to - o1.to;
			}
		});
		
		List<Range> curRanges = new ArrayList<Range>();
		
		for(int i=0;i<m;i++){
			Range r = ranges.get(i);
			int lastEnd = curRanges.size() == 0 ? -1 : curRanges.get(curRanges.size()-1).to;
			if(r.from > lastEnd){
				curRanges.add(r);
			}else{
				if(r.to > lastEnd){
					if(curRanges.size() >= 2 && curRanges.get(curRanges.size()-2).to + 1 >= r.from){
						unnecessaryId.add(curRanges.get(curRanges.size()-1).id);
						curRanges.remove(curRanges.size()-1);
					}
				
					curRanges.add(r);
				}else{
					unnecessaryId.add(r.id);
				}
			}
		}
		System.out.println(unnecessaryId.size());
		Collections.sort(unnecessaryId);
		StringBuffer out = new StringBuffer();
		for(int id : unnecessaryId){
			out.append(id + " ");
		}
		System.out.println(out);
	}
	static class Range{
		public int from;
		public int to;
		public int id;
		public Range(int from , int to, int id){
			this.from =from;
			this.to = to;
			this.id = id;
		}
		
		@Override
		public String toString(){
			return id + ","  + from + "->" + to ;
		}
		
		
	}
	
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}

}
