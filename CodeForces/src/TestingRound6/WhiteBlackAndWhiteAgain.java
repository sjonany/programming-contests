package TestingRound6;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class WhiteBlackAndWhiteAgain {
	static long MOD = 1000000009;
	static long[] fac;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int w = sc.nextInt();
		int b = sc.nextInt();
		
		fac = new long[w+b+1];
		fac[0] = 1;
		for(int i=1;i<fac.length;i++){
			fac[i] = (i* fac[i-1])%MOD;
		}
		long total = 0;
		for(int goodDays=2; goodDays<=n-1; goodDays++){
			int badDays = n-goodDays; 
			if(goodDays > w || badDays >b){
				continue;
			}
			long badDaySplit = comb(b-1, badDays-1);
			long goodDaySplit = (comb(w-1, goodDays-1) * (goodDays-1))%MOD;
			total = (total + goodDaySplit*badDaySplit) %MOD;
		}
		total = (total*fac[w])%MOD;
		total = (total*fac[b])%MOD;
		System.out.println(total);
	}
	
	static long comb(int n, int k){
		BigInteger denom = BigInteger.valueOf((fac[n-k] *fac[k])%MOD);
		long inv = denom.modInverse(BigInteger.valueOf(MOD)).longValue();
		return (fac[n] * inv) % MOD;
	}


	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
