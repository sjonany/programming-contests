package Round171div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Ladder {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int m = sc.nextInt();
		int[] arr  = new int[n];
		
		List<Integer> compressed = new ArrayList<Integer>();
		int prev = 0;
		for(int i=0; i<n; i++){
			int cur = sc.nextInt();
			
			if(i == 0 || cur != prev){
				compressed.add(cur);
			}
			arr[i] = compressed.size()-1;
			
			prev = cur;
		}
		
		n = compressed.size();
		boolean[] isPeak = new boolean[n];
		boolean[] isTrough = new boolean[n];
		for(int i=1; i<n-1; i++){
			if(compressed.get(i) > compressed.get(i-1) && compressed.get(i) > compressed.get(i+1)){
				isPeak[i] = true;
			}

			if(compressed.get(i) < compressed.get(i-1) && compressed.get(i) < compressed.get(i+1)){
				isTrough[i] = true;
			}
		}
		
		//bumps seen from 0... i-1
		int[] peakCount = new int[n];
		int[] troughCount = new int[n];
		
		for(int i=1; i<n; i++){
			if(isPeak[i-1]){
				peakCount[i] = peakCount[i-1]+1;
			}else{
				peakCount[i] = peakCount[i-1];
			}
			
			if(isTrough[i-1]){
				troughCount[i] = troughCount[i-1]+1;
			}else{
				troughCount[i] = troughCount[i-1];
			}
		}
		
		for(int i=0; i<m; i++){
			int l = arr[sc.nextInt()-1];
			int r = arr[sc.nextInt()-1];
			
			int lTrough = l+1 >= n ? troughCount[l] : troughCount[l+1];
			int lPeak = l+1 >= n ? peakCount[l] : peakCount[l+1];
			if(r==l || troughCount[r] == lTrough && peakCount[r]-lPeak<=1){
				System.out.println("Yes");
			}else{
				System.out.println("No");
			}
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
