package Round171div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BeautifulDecompositionRedo {
	public static void main(String[] args){
		int POS = 0;
		int NEG = 1;
		FastScanner sc = new FastScanner();
		String s = sc.nextToken();
		
		int[][] dp = new int[2][s.length()];
		
		dp[POS][0] = s.charAt(s.length()-1) == '0' ? 0:1;
		dp[NEG][0] = 1;
		
		for(int i=1; i<s.length(); i++){
			char cur = s.charAt(s.length()-i-1);
			if(cur == '0'){
				dp[POS][i] = dp[POS][i-1];
				dp[NEG][i] = 1 + Math.min(dp[POS][i-1], dp[NEG][i-1]);
			}else{
				dp[NEG][i] = dp[NEG][i-1];
				dp[POS][i] = 1 + Math.min(dp[POS][i-1], dp[NEG][i-1]);
			}
		}
		
		System.out.println(dp[POS][s.length()-1]);
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
