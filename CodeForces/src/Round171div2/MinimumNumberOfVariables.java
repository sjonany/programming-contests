package Round171div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/*
inputs:
5
1 2 3 6 8

2

3
3 6 5

-1

6
2 4 8 6 10 18

3
 */

public class MinimumNumberOfVariables {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int[] m = new int[n];
		for(int i=0;i<n;i++){
			m[i] = sc.nextInt();
		}
		
		//index[a][b] = c -> m[b] + m[c] = m[a], b&c<a
		int[][] index = new int[n][n];
		for(int i=0; i<n; i++){
			for(int j=0; j<n; j++){
				index[i][j] = -1;
			}
		}
		
		for(int a=0; a<n; a++){
			for(int b=0; b<a; b++){
				for(int c=0; c<a; c++){
					if(m[b] + m[c] == m[a]){
						index[a][b] = c;
					}
				}
			}
		}
		
		//isPossible[i][Set] = true if u can finish the ith step (read m[0...i])
		//with exactly els = Set
		boolean[] isPossible = new boolean[1<<n];
		boolean[] isNextPossible = new boolean[1<<n];
		
		isPossible[1] = true;
		
		for(int i=1; i<n; i++){
			int shiftedI = 1<<i;
			//System.out.println("i = " + i);
			//check all the prev states
			for(int p=0; p< shiftedI; p++){
				if(isPossible[p]){
					int curBitStr = p;
					boolean isSumExist = false;
					//check if there exists a sum
					for(int prevStateI = 0; prevStateI < i && !isSumExist; prevStateI++){
						int bitAt = curBitStr & 1;
						if(bitAt == 1){
							int compBitI = index[i][prevStateI];
							if(compBitI != -1 && ((p >> compBitI) & 1) == 1){
								isSumExist = true;
								break;
							}
						}
						curBitStr >>= 1;
					}
					
					if(isSumExist){
						//if make new var
						int completeP = p | shiftedI;
						isNextPossible[completeP] = true;
						//if replace one of the vars
						for(int toReplace = 0; toReplace < i; toReplace++){
							isNextPossible[completeP & (~(1<<toReplace))] = true;
						}
					}
				}
			}
			isPossible = isNextPossible;
			isNextPossible = new boolean[1<<n];
		}
		
		int min = Integer.MAX_VALUE;
		for(int p=0; p< (1<<n); p++){
			if(isPossible[p])
				min=Math.min(min, Integer.bitCount(p));
		}
		
		if(min == Integer.MAX_VALUE){
			System.out.println(-1);
		}else{
			System.out.println(min);
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
