package Round171div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PointOnSpiral {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int x = sc.nextInt();
		int y = sc.nextInt();
		
		if(x==0 && y==0){
			System.out.println(0);
			return;
		}
		
		//push to corner
		//top right
		int step = 0;
		if(Math.abs(x) < y && y > 0){
			x = y;
			step++;
		}else if(y<=0 && x > y && x < -y+1){
			//bottom left
			x = y;
			step++;
		}else if(x < 0 && Math.abs(y) < (-x)){
			//top left
			y = -x;
			step++;
		}else if(x > 0 &&  y < x && y > (-x+1) ){
			y = -x+1;
			step++;
			//bottom right
		}
		
		
		if(x==y && x>=0){
			System.out.println(step + 4*(x-1) + 1);
			return;
		}else if(x==y && x < 0){
			System.out.println(step + 4*(-x-1) + 3);
			return;
		}else if(x < 0 && x == -y){
			System.out.println(step + 4*(-x-1) + 2);
			return;
		}else{
			System.out.println(step + 4*(x-1));
			return;
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
