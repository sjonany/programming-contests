package Round171div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//practice making spirals!
public class PointOnSpiralEmulator {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int x = sc.nextInt();
		int y = sc.nextInt();
		
		if(x==0 && y==0){
			System.out.println(0);
			return;
		}
		
		int[][]dir = new int[][]{
				{1,0},
				{0,1},
				{-1,0},
				{0,-1}};
		int d = 0;
		int step = 0;
		int mx=0;
		int my=0;
		int dirCount = 0;
		while(true){
			if(d%2 == 0){
				step++;
			}
			for(int i=1; i<=step; i++){
				mx += dir[d][0];
				my += dir[d][1];
				if(mx == x && my == y){
					System.out.println(dirCount);
					return;
				}
			}
			
			d = (d+1)%4;
			dirCount++;
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
