package Round215;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class SerejaAndSets {
	public static void main(String[] args) throws Exception {
		PrintWriter out = new PrintWriter(System.out);
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer tk = tk(in.readLine());
		int N = Integer.parseInt(tk.nextToken());
		int M = Integer.parseInt(tk.nextToken());
		int D = Integer.parseInt(tk.nextToken());
		
		int[] arr = new int[N];
		for(int i = 0; i < M; i++) {
			tk = tk(in.readLine());
			int sz = Integer.parseInt(tk.nextToken());
			for(int j=0;j<sz;j++){
				arr[Integer.parseInt(tk.nextToken())-1] = i;
			}
		}
		int[] targets = new int[N-D+1];
		
		// from the region of size D, how many hits
		int[] countContain = new int[M];
		for(int i = 0; i < D; i++) {
			countContain[arr[i]]++;
		}
		
		targets[0] = conv(countContain);
		for(int i=1; i<targets.length; i++) {
			countContain[arr[i-1]]--;
			countContain[arr[i+D-1]]++;
			targets[i] = conv(countContain);
		}
		out.println(minHitSet(targets, M));
		out.close();
	}
	
	static int conv(int[] arr) {
		int ans = 0;
		for(int i=0;i<arr.length;i++) {
			ans<<=1;
			if(arr[i] > 0) ans+=1;
		}
		return ans;
	}
	
	//min hitting set if targets[i] = bitstring, is one if it's one of the targets
	static int minHitSet(int[] targets, int numhitter) {
		//it's not a hitting set iff the exclude set supersets one of the targets
		int max = 1 << numhitter;
		boolean[] invalid = new boolean[max];
		for(int t:targets) {invalid[t] = true;}
		for(int i = 0; i < max; i++) {
			if(invalid[i]) {
				for(int j = 0; j < numhitter; j++) {
					invalid[i | (1<<j)] = true;
				}
			}
		}
		
		int maxexclude = 0;
		for(int i = 0; i < max; i++) {
			if(!invalid[i]){
				maxexclude = Math.max(maxexclude, Integer.bitCount(i));
			}
		}
		
		return numhitter - maxexclude;
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
