package Round215;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class SerejaAndAnagrams{
	static InputReader in;	
	static PrintWriter out;

	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);	

		int n = in.nextInt();
		int m = in.nextInt();
		int p = in.nextInt();

		int[] a = new int[n];
		int[] b = new int[m];
		for(int i=0;i<n;i++){a[i] = in.nextInt();}
		for(int i=0;i<m;i++){b[i] = in.nextInt();}

		SortedMap<Integer, Integer> bcounter = new TreeMap();
		for(int bb : b) {
			add(bcounter, bb, 1);
		}

		SortedSet<Integer> ans = new TreeSet();
		boolean[] done = new boolean[n];
		for(int qi = 0; qi < n; qi++){
			if(done[qi]){
				break;
			}
			done[qi] = true;

			if((long)qi + (long)(m-1)*p >= n){
				break;
			}

			SortedMap<Integer, Integer> acounter = new TreeMap();	
			int qq = qi;
			for(int i = 0; i <= m-1 ; i++) {
				add(acounter, a[qq], 1);
				qq += p;
			}			

			SortedSet<Integer> mismatches = new TreeSet();
			
			for(int akey : acounter.keySet()) {
				if(get(acounter, akey) != get(bcounter, akey)) {
					mismatches.add(akey);
				}
			}
			
			for(int bkey : bcounter.keySet()) {
				if(get(bcounter, bkey) != get(acounter, bkey)) {
					mismatches.add(bkey);
				}
			}
			
			if(mismatches.size() == 0) {
				ans.add(qi);
			}
			
			int ql = qi;
			ql += p;
			while(true) {
				long qr = (long)ql + (long)(m-1) * p;
				if(qr < n){
					done[ql] = true;
					int added = a[(int)qr];
					int rem = a[ql - p];
					
					add(acounter, added, 1);
					add(acounter, rem, -1);
					
					if(get(acounter, added) == get(bcounter, added)) {
						mismatches.remove(added);
					} else {
						mismatches.add(added);
					}
					
					if(get(acounter, rem) == get(bcounter, rem)) {
						mismatches.remove(rem);
					} else {
						mismatches.add(rem);
					}
					
					if(mismatches.size() == 0) {
						ans.add(ql);
					}
				} else {
					break;
				}
				
				ql += p;
			}
		}
		out.println(ans.size());
		for(int i  : ans) {
			out.print((i+1) + " ");
		}
		out.println();
		out.close();
	}
	
	static void add(Map<Integer, Integer> map, int key, int val) {
		Integer prev = map.get(key);
		if(prev == null) prev = 0;
		map.put(key, prev + val);
	}
	
	static int get(Map<Integer, Integer> map, int key) {
		Integer prev = map.get(key);
		if(prev == null) prev = 0;
		return prev;
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
