package Round215;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;

public class SerejaAndAlgorithm {
	static InputReader in;
	static PrintWriter out;

	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);

		String s = in.nextToken();

		int[][] count = new int[s.length()][26];

		int[] counter = new int[26];
		for(int i = 0; i < s.length(); i++){
			counter[s.charAt(i)-'a']++;
			for(int j = 0; j < 26; j++) {
				count[i][j] = counter[j];;
			}
		}

		int m = in.nextInt();

		int[][][] mycount = new int[3][s.length()][26];

		char[] chr = new char[]{'x','y','z'};
		for(int j = 0; j < 3; j++){
			int[] mycounter = new int[26];
			for(int i = 0; i < s.length(); i++){
				mycounter[chr[(j+i)%3]-'a']++;
				for(int k = 0; k < 26; k++) {
					mycount[j][i][k] = mycounter[k];
				}
			}
		}

		//System.out.println(Arrays.deepToString(count));
		//System.out.println(Arrays.deepToString(mycount));
		
		for(int i = 0 ; i < m; i++) {
			int l = in.nextInt()-1;
			int r = in.nextInt()-1;
			boolean terminate = false;
			if(r-l+1 >= 3) {
				for(int t = 0; t < 3; t++){
					boolean eq = true;
					for(int j = 24; j < 26; j++){
						int prevCount = 0;
						int prevMyCount = 0;
						if(l != 0){
							prevCount = count[l-1][j];
							prevMyCount = mycount[t][l-1][j];
						}
						/*
						System.out.println("sub = " + s.substring(l,r+1));
						System.out.println(count[r][j] +  " " + prevCount + " " + (count[r][j]-prevCount));
						System.out.println(mycount[t][r][j] +  " " + prevMyCount + " " + (mycount[t][r][j]-prevMyCount));
						System.out.println();*/
						if(count[r][j]-prevCount != mycount[t][r][j] - prevMyCount){
							eq = false;
							break;
						}
					}
					if(eq){
						terminate = true;
						break;
					}
				}
			} else {
				terminate = true;
			}
			if(terminate){
				out.println("YES");
			}else {
				out.println("NO");
			}
		}

		out.close();
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}

		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}