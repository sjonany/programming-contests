package Round169div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Maxxor {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		long l = sc.nextLong();
		long r = sc.nextLong();
		
		if(l == r){
			System.out.println(0);
			return;
		}
		long lLength = countBinLength(l);
		long rLength = countBinLength(r);
		
		if(rLength == lLength){
			long xor = l ^ r;
			long xorL = countBinLength(xor);
			rLength = xorL;
			//long common = ((r >> xorL) << xorL);
			//l ^= common;
			//r ^= common;
		}

		//guaranteed l&r different lengths
		
		long val = exp(2,rLength)-1;
		System.out.println(val);
		
	}

	public static long exp(long a, long n){
		if(n==0){
			return 1;
		}

		if(n % 2 == 0){
			long r = exp(a, n/2);
			return (r*r);
		}else{
			return (a * exp(a,n-1));
		}
	}
	
	public static long countBinLength(long value) {
	    return Long.SIZE-Long.numberOfLeadingZeros(value);
	}
	
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
