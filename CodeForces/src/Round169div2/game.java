package Round169div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

public class game {
	public static void main(String[] args){
		HashMap<Character, Integer> counts = new HashMap<Character, Integer>();
		FastScanner sc = new FastScanner();
		String line = sc.nextToken();
		for(int i=0; i< line.length(); i++){
			Character key = new Character(line.charAt(i));
			Integer count = counts.get(key);
			if(count == null){
				counts.put(key, 1);
			}else{
				counts.put(key, count+1);
			}
		}
		
		int oddCount = 0;
		for(Entry<Character,Integer> entry : counts.entrySet()){
			if(entry.getValue() %2 == 1){
				oddCount++;
			}
		}
		
		if(oddCount == 0 || oddCount % 2 == 1){
			System.out.println("First");
		}else{
			System.out.println("Second");
		}
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
