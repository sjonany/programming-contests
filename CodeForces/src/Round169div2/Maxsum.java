package Round169div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Maxsum {
	//redo: peeked at ping's soln. Darnit, I was stupid. I thought it's a segment tree, so I gave up
	public static void main(String[] args){
		BigInteger sum = BigInteger.ZERO;
		FastScanner sc  = new FastScanner();
		int n = sc.nextInt();
		int q = sc.nextInt();
		
		int[] els = new int[n];
		for(int i=0; i< n; i++){
			els[i] = sc.nextInt();
		}
		
		Arrays.sort(els);
		
		int[] ascent = new int[n+1];
		for(int i=1; i<= q; i++){
			int l = sc.nextInt()-1;
			int r = sc.nextInt()-1;
			
			ascent[l] ++;
			ascent[r+1] --;
		}
		
		int[] touchCount = new int[n];
		int alt = 0;
		for(int i=0; i<n; i++){
			alt += ascent[i];
			touchCount[i] = alt;
		}
		
		Arrays.sort(touchCount);
		
		for(int i= n-1; i>=0 ;i--){
			BigInteger val = BigInteger.valueOf(((long)touchCount[i]) * els[i]);
			sum = sum.add(val);
		}
		
		System.out.println(sum);
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
