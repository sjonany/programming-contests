package CTS1E7;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class H {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);

	static int N;
	public static void main(String[] args) throws Exception{
		int TEST = in.nextInt();
		for(int test= 0; test<TEST;test++){
			int costDent = in.nextInt();
			int costFace = in.nextInt();
			N = in.nextInt();
			Point2D[] points = new Point2D[N];
			for(int i= 0; i < N; i++){
				int x = in.nextInt();
				int y = in.nextInt();
				points[i] = new Point2D.Double(x,y);
			}
			
			int countDent = 0;
			int countGoodFace = N;
			
			for(int i = 0; i < N; i++) {
				for(int j = 2; j < N-1; j++){
					int it = (i + j) % N;
					boolean found = true;
					for(int k = 0; k < N; k++) {
						int p = (i+k)%N;
						if(p==i || p==it){
							continue;
						}
						
						if(!isCCW(new double[]{points[it].getX()- points[i].getX(), points[it].getY() - points[i].getY()},
								new double[]{points[p].getX()- points[i].getX(), points[p].getY() - points[i].getY()})) {
							found = false;
							break;
						}
					}
					if(found){ 
						countDent++;
						countGoodFace -= (N-j);
					}
				}
			}
			
			out.println(Math.max(0,-countDent * costDent + countGoodFace * costFace));
		}
		
		out.close();
	}
	
	public static boolean isCCW(double[] v1, double[] v2){
		return v1[0]*v2[1] - v1[1]*v2[0] > 0;
	}
	
	//0 if on line, -1 or 1 if on some side
	public static int getSideOfLine(Line2D divide, Point2D p1){
		double[] vDiv = new double[]{divide.getX2()-divide.getX1(),
				divide.getY2()-divide.getY1(),0};
		double[] v1 = new double[]{p1.getX()-divide.getX1(),
				p1.getY()-divide.getY1(),0};
		
		double c = vDiv[0] * v1[1] - vDiv[1] * v1[0];
		if(Math.abs(c) < 0.00001){
			return 0;
		}else if(c>0){
			return 1;
		}else{
			return -1;
		}
	}
	
	public static Area makePoly(List<double[]> points){
		Path2D.Double poly = new Path2D.Double();
		poly.moveTo(points.get(0)[0], points.get(0)[1]);
		for(int i=1;i<points.size();i++){
			poly.lineTo(points.get(i)[0], points.get(i)[1]);
		}
		poly.closePath();
		
		return new Area(poly);
	}

	
	static class Interval{
		public int from;
		public int to;
		public Interval(int from, int to) {
			this.from = from;
			this.to = to;
		}
		
		public boolean isWithin(Interval other){
			int myfrom = from;
			int myto = to;
			if(myto < myfrom){
				myto += N;
			}
			int otherFrom = other.from;
			int otherTo = other.to;
			if(otherTo < otherFrom){
				otherTo += N;
			}
			
			return myfrom >= other.from && myto <= other.to;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
