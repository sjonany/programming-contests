package CTS1E7;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Set;

public class E {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);

	public static void main(String[] args) throws Exception{
		int TEST = in.nextInt();
		for(int test = 0; test < TEST ; test++){
			int N = in.nextInt();

			List<Integer>[] dirs = (List<Integer>[]) Array.newInstance(List.class, N);
			int[] strength = new int[N];
			for(int i = 0 ;i  < N; i++){
				int id = in.nextInt();
				dirs[id] = new ArrayList<Integer>();
				strength[id] = in.nextInt();
				int numChild =  in.nextInt();
				for(int j = 0 ;j  < numChild ;j ++){
					dirs[id].add(in.nextInt());
				}
			}
			
			Set<Edge> edgeSet = new HashSet<Edge>();
			int tot = 0;
			for(int i = 0; i <N; i++){
				for(int child : dirs[i]){
					int weight = strength[i] + strength[child];
					Edge e = new Edge(i, child, -weight);
					if(!edgeSet.contains(e)){
						tot+=weight;
						edgeSet.add(new Edge(i, child, -weight));
					}
				}
			}
			List<Edge> allEdges = new ArrayList<Edge>();
			allEdges.addAll(edgeSet);
			out.println(tot + mst(allEdges, N));
		}
		out.close();
	}
	
	static int mst(List<Edge> g, int N){
		Collections.sort(g, new Comparator<Edge>(){
			@Override
			public int compare(Edge o1, Edge o2) {
				return o1.weight - o2.weight;
			}
		});
		
		DisjointSet dj = new DisjointSet(N);
		int total = 0;
		for(Edge e : g){
			if(!dj.union(e.from, e.to)){
				total += e.weight;
			}
		}
		
		return total;
	}
	
	static class Edge{
		public int weight;
		public int from;
		public int to;
		
		public Edge(int from, int to, int weight){
			if(from > to){
				int t = from;
				from = to;
				to = t;
			}
			this.from = from;
			this.to = to;
			this.weight=  weight;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + from;
			result = prime * result + to;
			result = prime * result + weight;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			Edge other = (Edge) obj;
			if (from != other.from)
				return false;
			if (to != other.to)
				return false;
			if (weight != other.weight)
				return false;
			return true;
		}
		
	}
	
	static class DisjointSet{
		int[] parent;
		int[] size;
		
		public DisjointSet(int s){
			parent = new int[s];
			size = new int[s];
			for(int i = 0; i < s; i++){
				parent[i] = i;
				size[i] = 1;
			}
		}
		
		public int getLeader(int x){
			if(parent[x] == x){
				return x;
			}
			parent[x] = getLeader(parent[x]);
			return parent[x];
		}
		
		public int getSize(int x){
			return size[getLeader(x)];
		}
		
		public boolean union(int x, int y){
			x = getLeader(x);
			y = getLeader(y);
			if(x == y){
				return true;
			}
			//y as root
			if(size[x] < size[y]){
				int temp = x;
				x = y;
				y = temp;
			}
			
			parent[x] = y;
			size[y] += size[x];
			return false;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
