package CTS1E7;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.StringTokenizer;

public class B {
	static FastScanner in = new FastScanner();
	static PrintWriter out = new PrintWriter(System.out);

	public static void main(String[] args) throws Exception{
		int TEST = in.nextInt();
		for(int test = 0; test < TEST; test++){
			int a = in.nextInt();
			Map<Character, Integer> charToId = new HashMap<Character, Integer>();
			char[] chars = new char[a];
			for(int i = 0 ;i < a; i++){
				char c = in.nextToken().charAt(0);
				charToId.put(c, i);
				chars[i] = c;
			}
			double[][] errorMat = new double[a][a];
			for(int i = 0; i < a; i++){
				for(int j = 0; j < a; j++){
					errorMat[i][j] = Math.log(in.nextDouble());
				}	
			}

			double[][] markov = new double[a][a];
			for(int i = 0; i < a; i++){
				for(int j = 0; j < a; j++){
					markov[i][j] = Math.log(in.nextDouble());
				}	
			}
			
			int w = in.nextInt();
			for(int ww = 0; ww < w; ww ++){
				String ms = in.nextToken();
				int[] arr = new int[ms.length()];
				for(int i = 0 ;i  < ms.length(); i++){
					arr[i] = charToId.get(ms.charAt(i));
				}

				//max likelihood if consider 0-i, ends with j
				double[][] dp = new double[arr.length][a];
				int[][] back = new int[arr.length][a];

				for(int i = 0; i < arr.length; i++){
					for(int j = 0; j < a; j++){
						dp[i][j] = 0.0;
						boolean set = false;
						if(i==0){
							dp[i][j] = errorMat[j][arr[i]];
						}else{
							for(int prev = 0; prev < a; prev++){
								double error = errorMat[j][arr[i]];
								double mark = markov[prev][j];
								double prevDp = dp[i-1][prev];
								double newDp = prevDp + mark + error;
								if(!set || newDp > dp[i][j]){
									dp[i][j] = newDp;
									back[i][j] = prev;
									set= true;
								}
							}
						}
					}
				}

				int bestEnding = 0;
				for(int i = 0; i < a; i++){
					if(dp[arr.length-1][bestEnding] < dp[arr.length-1][i]){
						bestEnding = i;
					}
				}
				//System.out.println(Arrays.deepToString(dp));

				StringBuilder sb = new StringBuilder();
				sb.append(chars[bestEnding]);
				for(int i = arr.length-1; i>=1; i--){
					sb.append(chars[back[i][bestEnding]]);
					bestEnding = back[i][bestEnding];
				}
				StringBuilder sbRev = new StringBuilder();
				for(int i = sb.length()-1; i>=0;i--){
					sbRev.append(sb.charAt(i));
				}
				out.println( sbRev);
			}
		}
		out.close();
	}

	static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
