package CTS1E7;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class K {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);
	static int N;
	static List<Integer>[] occ;
	public static void main(String[] args) throws Exception{
		occ = (List<Integer>[]) Array.newInstance(List.class, 10);
		for(int i = 0; i < 10; i++){
			occ[i] = new ArrayList<Integer>();
		}
		N = in.nextInt();
		int K = in.nextInt();
		int REQ = N-K;
		String s = in.nextToken();
		int[] arr = new int[s.length()];
		
		for(int i = 0; i < s.length(); i++){
			arr[i] = s.charAt(i) - '0';
			occ[arr[i]].add(i);
		}
		pointer = new int[10];
		for(int i = 0; i  < 10; i++){
			if(occ[i].size() == 0){
				pointer[i] = -1;
			}
		}
		StringBuilder ans = new StringBuilder();
		solve(arr,0, REQ, ans);
		out.println(ans.toString());
		out.close();
	}
	
	static int[] pointer;
	//solve if want REQ
	static void solve(int[] arr, int start, int REQ, StringBuilder sb){ 
		if(REQ == 0) return;
		int newStart = 0;
		for(int dig = 9; dig >= 0; dig--){
			if(pointer[dig] != -1 && (N - occ[dig].get(pointer[dig]) - 1) >= (REQ -1)){
				sb.append(dig);
				newStart = occ[dig].get(pointer[dig])+1;
				break;
			}
		}
		
		for(int dig = 0; dig <= 9; dig++){ 
			while(pointer[dig] != -1 && occ[dig].get(pointer[dig]) < newStart){
				if(pointer[dig] == occ[dig].size()-1){
					pointer[dig] = -1;
					break;
				}else{
					pointer[dig]++;
				}
			}
		}
		solve(arr, newStart, REQ-1, sb);
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
