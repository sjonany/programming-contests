package CTS1E7;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;

public class L {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);

	public static void main(String[] args) throws Exception{
		int R = in.nextInt();
		int C = in.nextInt();
		State start = new State(1,4,3,2,5,6);
		//map[state] = k means from starting point to the first of state, it takes k values, EXCLUDING the last face
		//also, the state is first seen in the ith row
		Map<State, Result> map = new HashMap<State, Result>();
		int curRow = 0;
		long res = 0;
		State curState = start;
		boolean hasRepped = false;
		while(true){
			map.put(curState, new Result(res, curRow));
			res += curState.top;
			for(int c = 0; c < C-1; c++){
				curState = curState.rollRight();
				res+=curState.top;
			}
			curState = curState.rollDown();
			curRow++;
			if(curRow == R){
				out.println(res);
				out.close();
				return;
			}
			res+=curState.top;
			for(int c = 0; c < C-1; c++){
				curState = curState.rollLeft();
				res+=curState.top;
			}
			curState = curState.rollDown();
			curRow++;
			if(curRow == R){
				out.println(res);
				out.close();
				return;
			}
			if(!hasRepped && map.containsKey(curState)){
				Result result = map.get(curState);
				long deltaCost = res - result.cost;
				int deltaRow = curRow - result.row;
				while(curRow + deltaRow < R - 5){
					curRow += deltaRow;
					res += deltaCost;
				}
				hasRepped = true;
			}
		}
	}
	
	static class Result{
		public long cost;
		//0 indexed
		public int row;
		public Result(long cost, int row) {
			this.cost = cost;
			this.row = row;
		}
	}
	
	static class State{
		public int top;
		public int left;
		public int right;
		public int front;
		public int back;
		public int bottom;
		
		public State(){}
		public State(int top, int left, int right, int front, int back,
				int bottom) {
			this.top = top;
			this.left = left;
			this.right = right;
			this.front = front;
			this.back = back;
			this.bottom = bottom;
		}
		
		public State rollRight(){
			State s = new State();
			s.top = left;
			s.left = bottom;
			s.bottom = right;
			s.right = top;
			s.front = front;
			s.back = back;
			return s;
		}
		
		public State rollLeft(){
			State s = new State();
			s.top = right;
			s.right = bottom;
			s.bottom = left;
			s.left = top;
			s.front = front;
			s.back = back;
			return s;
		}

		public State rollDown() {
			State s = new State();
			s.top = back;
			s.back = bottom;
			s.bottom = front;
			s.front = top;
			s.left = left;
			s.right = right;
			return s;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + back;
			result = prime * result + bottom;
			result = prime * result + front;
			result = prime * result + left;
			result = prime * result + right;
			result = prime * result + top;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			State other = (State) obj;
			if (back != other.back)
				return false;
			if (bottom != other.bottom)
				return false;
			if (front != other.front)
				return false;
			if (left != other.left)
				return false;
			if (right != other.right)
				return false;
			if (top != other.top)
				return false;
			return true;
		}
		
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
