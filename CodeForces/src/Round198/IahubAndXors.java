package Round198;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class IahubAndXors {
	
	static int M;
	static int N;
	static Fenwick[][] trees;
	
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		N = in.nextInt();
		M = in.nextInt();
		
		//Represents 4 parts to N[][]
		trees = new Fenwick[2][2];
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 2; j++){
				trees[i][j] = new Fenwick(N/2+5);
			}
		}
		
		for(int i = 0 ; i < M; i++) {
			if(in.nextInt() == 1){
				int x1 = in.nextInt();
				int y1 = in.nextInt();
				int x2 = in.nextInt();
				int y2 = in.nextInt();
				out.println(queryM(x2,y2) ^ queryM(x2, y1-1) ^ queryM(x1-1,y2) ^ queryM(x1-1,y1-1));
			}else{
				int x1 = in.nextInt();
				int y1 = in.nextInt();
				int x2 = in.nextInt();
				int y2 = in.nextInt();
				long v = in.nextLong();

				updateN(x1,y1,v);
				updateN(x2+1,y1,v);
				updateN(x1,y2+1,v);
				updateN(x2+1,y2+1,v);
			}
		}
		
		out.close();
	}
	
	//M.Q(1,1,X,Y) = N(1,1,x,y), but only x,y of same parity
	static long queryM(int x, int y) {
		return trees[x%2][y%2].query((int)Math.ceil(x/2.0), (int)Math.ceil(y/2.0));
	}
	
	//M.U(x1,y1,x2,y2)
	static void updateN(int x, int y, long v) {
		trees[x%2][y%2].update((int)Math.ceil(x/2.0), (int)Math.ceil(y/2.0), v);
	}
	
	//original matrix = M, dual matrix = N, such that N(1,1,x,y) = M[x][y]
	//M.Q(x,y) = N(1,1,x,y)
	//M.Q(1,1,X,Y) = N(1,1,1,1) ^ N(1,1,1,2) ^... = {N[1][1]} ^ {N[1][1] ^ N[1][2]} ^...
	//N[x][y] will get xored (X-x+1) (Y-y+1) times, and so will only appear if (X-x+1) and (Y-y+1) are both odd
	//That is, N[x][y] will be relevant iff x shares the same parity as X, and y with Y.
	//So, M.Q(1,1,X,Y) = ^ N[x][y], all x,y < X and Y, and share same parity
	
	//M.U(x1,y1,x2,y2,v) -> just put v in 4 corners in N. Classic trick if N(1,1,x,y) = M[x][y] 
	
	//point update, range query
	static class Fenwick {
		public long[][] tree;
		public int N;
		
		public Fenwick(int N){
			this.N = N;
			tree = new long[N+1][N+1];
		}
		
		public long query(int x, int y){
			if(x== 0 || y ==0) return 0;
			long sum = 0;
			for(int i = x; i > 0; i -= (i & -i)){
				for(int j = y; j > 0; j -= (j & -j)){
					sum ^= tree[i][j];
				}
			}
			return sum;
		}

		public void update(int x, int y, long val){
			if(x == 0 || y == 0) return;
			for (int i = x; i <= N; i += (i & -i)) {
				for (int j = y; j <= N; j += (j & -j)) {
					tree[i][j] ^= val;
				}
			}
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
