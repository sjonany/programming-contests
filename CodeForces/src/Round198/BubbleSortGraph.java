package Round198;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class BubbleSortGraph {
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int[] arr = new int[n];
		for(int i = 0 ;i < n; i++){
			arr[i] = in.nextInt();
		}
		
		out.println(getLongestIncreasingSubsequence(arr));
		out.close();
	}
	
	static int getLongestIncreasingSubsequence(int[] arr){
		if(arr.length <= 1) return arr.length;
		
		int[] dp = new int[arr.length];
		int mSize = 2;
		int[] m = new int[arr.length+1];
		
		dp[0] = 1;
		m[1] = 0;
		int maxLength = -1;
		for(int i = 1; i < dp.length; i++){
			int j = binSearch(arr, arr[i], mSize-1, m);
			dp[i] = 1 + j;
			
			if(dp[i] >= mSize){
				m[dp[i]] = i;
				mSize++;
			}else{
				if(arr[m[dp[i]]] > arr[i]){
					m[dp[i]] = i;
				}
			}
			maxLength = Math.max(dp[i], maxLength);
		}
		
		return maxLength;
	}
	
	//get largest index in arr > s, given arr is descending
	public static int binSearch(int[] arr, int s, int hi, int[] m){
		int lo=1;
		
		if(arr[m[1]] > s){
			return 0;
		}else{
			while(lo < hi){
				int mid = lo + (hi-lo+1)/2;
				if(arr[m[mid]] > s){
					hi = mid-1;
				}else{
					lo = mid;
				}
			}
		}
		return lo;
	}
	
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
