package Round198;
import java.io.IOException;
import java.io.InputStream;
import java.util.InputMismatchException;

public class MaximumAreaQuadrilateral {

	static int X = 0;
	static int Y = 1;
	static int[][] points;
	public static void main(String[] args){
		InputReader in = new InputReader(System.in);
		int n = in.nextInt();
		points = new int[n][2];
		for(int i= 0 ; i < n; i++){
			points[i][X] = in.nextInt();
			points[i][Y] = in.nextInt();
		}

		double maxArea = 0;
		for(int i = 0; i < n; i++){
			for(int j = i+1; j < n; j++){
				double maxThisArea = 0;
				double maxThatArea = 0;
				for(int k = 0; k < n; k++){
					if(k == i || k == j)
						continue;
					
					double area = calcArea(i,j,k);
					if(area < 0){
						maxThatArea = Math.max(maxThatArea, -area);
					}else{
						maxThisArea = Math.max(maxThisArea, area);
					}
				}
				
				//important! it might be the case that triangle is bigger than quad if quad is non convex
				if(maxThisArea == 0 || maxThatArea == 0) continue;
				
				maxArea = Math.max(maxArea, maxThisArea + maxThatArea);
			}
		}
		System.out.println(maxArea);
	}
	
	public static double calcArea(int i, int j, int k){
		return (points[i][0]*points[j][1] - points[j][0]*points[i][1] + 
			points[j][0]*points[k][1] - points[k][0]*points[j][1] + 
			points[k][0]*points[i][1] - points[i][0]*points[k][1])/2.0 ;
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
