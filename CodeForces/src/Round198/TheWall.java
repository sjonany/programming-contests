package Round198;
import java.util.Scanner;

public class TheWall {
	public static void main(String[] args) throws Exception{
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		int y = in.nextInt();
		int lo = in.nextInt();
		int hi = in.nextInt();
		int lcm = x * y / gcd(x,y);
		
		int minTime = lo / lcm;
		if(minTime * lcm < lo){
			minTime++;
		}
		
		int maxTime = hi / lcm;
		if(maxTime * lcm > hi){
			maxTime--;
		}
		
		System.out.println(maxTime-minTime+1);
		
	}
	
	static int gcd(int a, int b){
		while(b > 0){
			int c = b;
			b = a % b;
			a = c;
		}
		return a;
	}
}
