package Round198;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Set;

public class IahubAndPermutations {
	static long[] fac;
	static long MOD = 1000000007;
	static BigInteger MODB = BigInteger.valueOf(MOD);
	
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		init();
		int n = Integer.parseInt(in.nextToken());
		int[] arr = new int[n];
		int numNegs = 0;
		Set<Integer> usedIndices = new HashSet<Integer>();
		Set<Integer> usedNums = new HashSet<Integer>();
		
		for(int i = 0; i < arr.length; i++){
			arr[i] = in.nextInt();
			if(arr[i] != -1){
				usedIndices.add(i+1);
				usedNums.add(arr[i]);
			}else{
				numNegs++;
			}
		}
		
		Set<Integer> unused = new HashSet<Integer>();
		for(int i = 1; i <=n ;i++){
			if(!usedIndices.contains(i) && !usedNums.contains(i)){
				unused.add(i);
			}
		}
		
		long curVal = 0;
		int multiplier = 1;
		int free = unused.size();
		for(int i = 1 ; i <= unused.size(); i++){
			curVal = (curVal + multiplier * choose(free,i) * fac[numNegs-i])  % MOD;
			multiplier *= -1;
		}
		
		curVal = fac[numNegs] - curVal;
		curVal %= MOD;
		while(curVal < 0)curVal +=MOD;
		out.println(curVal);
		
		out.close();
	}
	
	static void init(){
		fac = new long[2001];
		fac[0]=1;
		fac[1] = 1;
		for(int i =2; i< fac.length; i++){
			fac[i]  = (fac[i-1] * i) %MOD;
		}
	}
	
	static long choose(int n, int k){
		return BigInteger.valueOf(fac[n]).multiply(BigInteger.valueOf((fac[k]*fac[n-k])%MOD).modInverse(MODB)).mod(MODB).longValue();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
