package Round225;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class PropagatingTree {
	
	static PrintWriter out;
	static InputReader in;
	
	static int N;
	static int M;
	static int[] arr;
	static int OTHER_ROOT;
	static int dfsid = 1;
	
	static List<Integer>[] oriedges;
	static List<Integer>[] edges;
	// from original node id to dfs order, which can be duplicated because there are 2 trees
	static int[] dfsToDfsRight1;
	static int[] nodeToDfs1;
	static int[] dfsToDfsRight2;
	static int[] nodeToDfs2;
	
	static int[] dfsToDfsRightAll;
	static int[] nodeToDfsAll;
	static int[] dfsToNodeAll;
	static int[] whichTree;
	
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		N = in.nextInt();
		M = in.nextInt();
		
		dfsToDfsRight1= new int[N+5];
		nodeToDfs1= new int[N+5];
		dfsToDfsRight2= new int[N+5];
		nodeToDfs2= new int[N+5];
		dfsToDfsRightAll= new int[N+5];
		nodeToDfsAll= new int[N+5];
		dfsToNodeAll= new int[N+5];
		whichTree= new int[N+5];
		arr = new int[N];
		for(int i = 0; i  < N;i++) {
			arr[i] = in.nextInt();
		}
		
		oriedges = new List[N+5];
		edges = new List[N+5];
		
		for(int i=0;i<edges.length;i++){
			oriedges[i] = new ArrayList();
			edges[i] = new ArrayList();
		}
		
		for(int i=0;i<N-1;i++) {
			int from = in.nextInt();
			int to = in.nextInt();
			oriedges[from].add(to);
			oriedges[to].add(from);
		}
		
		// a new parent node that supers all root's children. now we have two rooted trees!
		OTHER_ROOT = N+1;
		buildTree(1,-1,-1, 0);
		
		for(int child : oriedges[1]) {
			edges[child].add(OTHER_ROOT);
			edges[OTHER_ROOT].add(child);
		}
		
		whichTree[OTHER_ROOT] = 2;
		dfsid = 1;
		dfsAll(1, -1);
		dfsid = 1;
		dfs(1, -1);
		dfsid = 1;
		dfs(OTHER_ROOT, -1);
		
		Fenwick2 fen1 = new Fenwick2(N+5);
		Fenwick2 fen2 = new Fenwick2(N+5);
		Fenwick2 allfen = new Fenwick2(N+5);
		
		for(int i=0;i<M;i++) {
			int type = in.nextInt();
			if(type==1){
				int node = in.nextInt();
				int val = in.nextInt();
				int left = -1;
				int right = -1;
				Fenwick2 fen = null;
				
				if(whichTree[node] == 1) {
					left = nodeToDfs1[node];
					right = dfsToDfsRight1[left];
					fen = fen1;
				} else {
					left = nodeToDfs2[node];
					right = dfsToDfsRight2[left];
					fen = fen2;
				}
				fen.update(left, right, (long)2l*val);
				
				int allleft = nodeToDfsAll[node];
				int allright=  dfsToDfsRightAll[allleft];
				allfen.update(allleft, allright, -val);
			}else{
				int node = in.nextInt();
				Fenwick2 fen = whichTree[node] == 1 ? fen1 : fen2;
				int dfs = whichTree[node] == 1 ? nodeToDfs1[node] : nodeToDfs2[node];
				long ans = fen.query(dfs) + allfen.query(nodeToDfsAll[node]);
				out.println(ans + arr[node-1]);
			}
		}
		
		out.close();
	}
	
	static void dfsAll(int cur, int par) {
		dfsid++;
		nodeToDfsAll[cur] = dfsid;
		dfsToNodeAll[dfsid] = cur;
		dfsToDfsRightAll[dfsid] = dfsid;
		for(int child : oriedges[cur]) {
			if(child == par || child == OTHER_ROOT) continue;
			dfsAll(child, cur);
			dfsToDfsRightAll[nodeToDfsAll[cur]] = dfsToDfsRightAll[nodeToDfsAll[child]];
		}
	}
	
	static void dfs(int cur, int par) {
		dfsid++;
		int[] nodeToDfs = whichTree[cur] == 1 ? nodeToDfs1 : nodeToDfs2;
		int[] dfsToDfsRight = whichTree[cur] == 1 ? dfsToDfsRight1 : dfsToDfsRight2;
		
		nodeToDfs[cur] = dfsid;
		dfsToDfsRight[dfsid] = dfsid;
		
		for(int child : edges[cur]) {
			if(child == par) continue;
			dfs(child, cur);
			dfsToDfsRight[nodeToDfs[cur]] = dfsToDfsRight[nodeToDfs[child]];
		}
	}
	
	static void buildTree(int cur, int par, int par2, int level) {
		if(level % 2 == 0){
			whichTree[cur] = 1;
		}else {
			whichTree[cur] = 2;
		}
		
		if(par2 > 0) {
			edges[par2].add(cur);
			edges[cur].add(par2);
		}
		for(int next : oriedges[cur]) {
			if(next != par) {
				buildTree(next, cur, par, level+1);
			}
		}
	}

	static class Fenwick1{
		long[] tree;
		public Fenwick1(int n){
			tree = new long[n+1];
		}

		public void increment(int i, long val){
			while(i < tree.length){
				tree[i] += val;
				i += (i & -i);
			}
		}

		public long getTotalFreq(int i){
			long sum = 0;
			while(i > 0){
				sum += tree[i];
				i -= (i & -i);
			}
			return sum;
		}
	}

	//range update polong query
	static class Fenwick2{ 
		public Fenwick1 tree;
		
		public Fenwick2(int n){
			tree = new Fenwick1(n);
		}
		
		public long query(int i) {
			return tree.getTotalFreq(i);
		}
		
		public void update(int i, int j, long v){
			tree.increment(i, v);
			tree.increment(j+1, -v);
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
