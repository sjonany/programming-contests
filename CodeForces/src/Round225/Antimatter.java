package Round225;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class Antimatter {
	
	static PrintWriter out;
	static InputReader in;
	static long MOD = 1000000007;
	static int MAXV = 10000;
	static int SZ = 2*MAXV+5;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int N = in.nextInt();
		int[] arr = new int[N];
		for(int i=0;i<N;i++) {
			arr[i] = in.nextInt();
		}
		
		// num = sum exactly ending in i
		long[] dp = new long[SZ];
		long[] prevdp = new long[SZ];
		long ans = 0;
		
		prevdp[conv(arr[0])]++;
		prevdp[conv(-arr[0])]++;
		ans += prevdp[conv(0)];
		for(int i=1; i<N;i++) {
			dp[conv(arr[i])]++;
			dp[conv(-arr[i])]++;
			for(int j = -MAXV; j <= MAXV;j++) {
				int cj = conv(j);
				dp[cj] += prevdp[conv(j - arr[i])];
				dp[cj] %= MOD;
				dp[cj] += prevdp[conv(j + arr[i])];
				dp[cj] %= MOD;
			}

			ans += dp[conv(0)];
			ans %= MOD;
			prevdp = dp;
			dp = new long[SZ];
		}
	
		out.println(ans);
		out.close();
	}
	
	// convert value to index
	static int conv(int val) {
		return Math.max(0, Math.min(val + MAXV, SZ-1));
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
