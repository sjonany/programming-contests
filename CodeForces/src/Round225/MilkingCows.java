package Round225;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class MilkingCows {
	
	static PrintWriter out;
	static InputReader in;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int N = in.nextInt();
		boolean[] arr = new boolean[N];
		int cOne = 0;
		int cZero = 0;
		for(int i=0;i<N;i++) {
			arr[i] = in.nextInt() == 1;
			if(arr[i]){
				cOne++;
			}
		}
		
		cZero = N - cOne;
		// how many to my left are ones
		int[] leftOne = new int[N];
		int[] rightZero = new int[N];
		
		for(int i=1;i<N;i++){
			leftOne[i] = leftOne[i-1] + (arr[i-1] ? 1 : 0);
		}
		
		for(int i=N-2; i >= 0; i--){
			rightZero[i] = rightZero[i+1] + (!arr[i+1] ? 1 : 0);
		}
		
		long ans1 = 0;
		long ans2 = 0;
		
		for(int i=0;i  < N; i++) {
			if(!arr[i]){
				ans1 += leftOne[i];
			}else{
				ans2 += rightZero[i];
			}
		}
		
		
		out.println(Math.min(ans1, ans2));
		
		out.close();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
