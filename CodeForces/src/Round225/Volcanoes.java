package Round225;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Volcanoes {
	
	static PrintWriter out;
	static InputReader in;
	static int N;
	static int M;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		N = in.nextInt();
		M = in.nextInt();
		
		Map<Integer, List<Integer>> blocks = new TreeMap<Integer, List<Integer>>();
		for(int i=0;i<M;i++) {
			int r = in.nextInt();
			int c = in.nextInt();
			put(blocks, r, c);
		}
		
		int prevLayer = -1000;
		Set<Integer> layers = new TreeSet<Integer>();
		layers.addAll(blocks.keySet());
		// add N+1 separators for easier code
		for(int layer : layers) {
			put(blocks, layer, N+1);
			if(prevLayer > 0) {
				// only keep track of 1 empty row for consecutive ones
				if(prevLayer + 1 != layer) {
					put(blocks, prevLayer+1, N+1);
				}
			}
			prevLayer = layer;
		}
		
		if(!blocks.containsKey(1)) {
			put(blocks, 1, N+1);
		}
		if(!blocks.containsKey(N)) {
			put(blocks, N, N+1);
		}
		
		boolean good = true;
		TreeSet<Interval> intervals = new TreeSet<Interval>(new Comparator<Interval>() {
			@Override
			public int compare(Interval o1, Interval o2) {
				int dl = o1.l - o2.l;
				if(dl != 0) {
					return dl;
				}
				return o1.r - o2.r;
			}
		});
		intervals.add(new Interval(1,  1));
		Interval[] toRemove = new Interval[2*M+5];
		int sz = 0;
		for(int row : blocks.keySet()) {
			List<Integer> layer = blocks.get(row);
			Collections.sort(layer);
			int prevBlock = 0;
			for(int block : layer) {
				Interval left = intervals.ceiling(new Interval(prevBlock+1, prevBlock+1));
				if(left == null) {
					break;
				}
				if(left.l > block) {
					continue;
				}
				int leftmost = left.l;
				sz = 0;
				Interval toAdd = null;
				for(Interval interval : intervals.tailSet(left)) {
					if(interval.l <= block)
						toRemove[sz++] = interval;
					if(interval.r >= block) {
						if(interval.l <= block) {
							if(block+1 <= interval.r)
								toAdd = new Interval(block+1, interval.r);
						}
						break;
					}
				}
				for(int i=0;i<sz;i++){
					intervals.remove(toRemove[i]);
				}
				if(toAdd != null)
					intervals.add(toAdd);
				
				if(block-1 >= leftmost) {
					intervals.add(new Interval(leftmost, block-1));
				}
				prevBlock = block;
			}
			if(intervals.size() == 0) {
				good = false;
				break;
			}
		}
		if(good)
			good = intervals.last().r == N;
		if(good) {
			out.println(2*N-2);
		}else{
			out.println(-1);
		}
		
		out.close();
	}
	
	static void put(Map<Integer, List<Integer>> map, int key, int val) {
		if(!map.containsKey(key)){
			map.put(key, new ArrayList());
		}
		map.get(key).add(val);
	}
	
	static class Interval {
		public int l;
		public int r;
		public Interval(int l, int r) {
			this.l = l;
			this.r = r;
		}
		@Override
		public String toString() {
			return "[l=" + l + ", r=" + r + "]";
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
