package Round210;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class LevkoAndArray {
	static int N;
	static int K;
	static int[] arr;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		StringTokenizer tk = new StringTokenizer(in.readLine());
		N = Integer.parseInt(tk.nextToken());
		K = Integer.parseInt(tk.nextToken());

		arr = new int[N];
		tk = new StringTokenizer(in.readLine());
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}

		dp = new int[2][N];
		if (N == 1) {
			out.println(0);
		} else {
			int lo = 0;
			int hi = Integer.MAX_VALUE;
			// find first true - FFTT
			while (lo < hi) {
				int mid = (int)((((long)lo + hi)) / 2);
				if(check(mid)) {
					hi = mid;
				} else {
					lo = mid + 1;
				}
			}
			
			out.println(lo);
		}
		out.close();
	}
	
	static int CH = 0;
	static int NOTCH = 1;
	static int[][] dp;
	static boolean check(int mid){
		//min trans to make 0..i consistent, and a[i] is changed or not changed
		dp[CH][0] = 1;
		
		for(int i = 1; i < N; i++) {
			dp[CH][i] = 1 + Math.min(dp[CH][i-1], dp[NOTCH][i-1]);
			//when everything is changed
			dp[NOTCH][i] = i;
			
			//how far does the changed extends from i to the left?
			for(int j = 1; j <= i; j++) {
				if(Math.abs(arr[i] - arr[j-1]) <= (long) mid * (i-j+1)) {
					dp[NOTCH][i] = Math.min(dp[NOTCH][i], (i-j) + dp[NOTCH][j-1]);
				}
			}
		}
		
		return Math.min(dp[CH][N-1], dp[NOTCH][N-1]) <= K;
	}
}
