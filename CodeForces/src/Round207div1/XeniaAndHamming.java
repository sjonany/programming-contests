package Round207div1;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;

public class XeniaAndHamming {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);
	
	public static void main(String[] args) throws Exception {
		long n = in.nextLong();
		long m = in.nextLong();
		String s1 = in.nextToken();
		String s2 = in.nextToken();
		
		if(s1.length() > s2.length()){
			String s3 = s2;
			s2 = s1;
			s1 = s3;
			long temp = n;
			n = m;
			m = temp;
		}
		
		long lcm = lcm(s1.length(), s2.length());
		int[] groupIndex = new int[s2.length()];
		Arrays.fill(groupIndex, -1);
		List<int[]> groupCounts = new ArrayList<int[]>();
		
		int groupCount = 0;
		for(int i = 0; i < s1.length(); i++) {
			long cur = i;
			if(groupIndex[(int)cur] != -1) continue;
			int[] count = new int[26];
			while(true) {
				count[s2.charAt((int)cur) - 'a']++;
				groupIndex[(int)cur] = groupCount;
				cur = (cur + s1.length()) % s2.length();
				if(groupIndex[(int)cur] != -1){
					break;
				}
			}
			groupCounts.add(count);
			groupCount++;
		}
		
		long match = 0;
		for(int i = 0; i < s1.length(); i++) {
			int ch = s1.charAt(i) - 'a';
			int[] count = groupCounts.get(groupIndex[i]);
			match += count[ch];
		}
		
		out.println(BigInteger.valueOf(lcm - match)
				.multiply(BigInteger.valueOf(n))
				.multiply(BigInteger.valueOf(s1.length()))
						.divide(BigInteger.valueOf(lcm)));
		out.close();
	}
	
	static long lcm(long a, long b) { 
		return a * b / gcd(a,b);
	}
	
	static long gcd(long a, long b) {
		while(b > 0 ){
			long c = b;
			b = a % b;
			a = c;
		}
		return a;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
