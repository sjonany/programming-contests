package Round207div1;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.SortedSet;
import java.util.TreeSet;

public class KnightTournament {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);

	public static void main(String[] args) throws Exception {
		int N = in.nextInt();
		int M = in.nextInt();
		int[] ans = new int[N+1];
		Arrays.fill(ans, -1);
		TreeSet<Integer> unfilled = new TreeSet<Integer>();
		for(int i = 0; i < N; i++){
			unfilled.add(i+1);
		}
		
		int[] toremove = new int[N+1];
		int sz = 0;
		for(int i = 0; i < M; i++) {
			int l = in.nextInt();
			int r = in.nextInt();
			int win = in.nextInt();
			SortedSet<Integer> set = unfilled.subSet(l, r+1);
			sz = 0;
			for(int j : set){
				if(j != -1){
					if(j != win) {
						ans[j] = win;
						toremove[sz++] = j;
					}
				}
			}
			for(int j = 0; j < sz; j++) {
				unfilled.remove(toremove[j]);
			}
		}
		
		for(int i= 1; i <= N; i++){
			if(ans[i] == -1){
				ans[i] = 0;
			}
			out.print(ans[i] + " ");
		}

		out.close();
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
