package Round207div1;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class Compartments {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);

	public static void main(String[] args) throws Exception {
		int m = in.nextInt();
		int[] count = new int[5];
		int n = 0;
		for(int i= 0 ; i < m; i++){
			int x = in.nextInt();
			count[x]++;
			n += x;
		}

		int min = Integer.MAX_VALUE;
		for(int w4 = 0; w4 <= n/4; w4++){
			if((n - 4 * w4) % 3 == 0){
				int ans = 0;
				int w3 = (n - 4*w4) / 3;
				int[] want = {0,0,0,w3,w4};
				int[] ori = count.clone();
				want[0] = 1000000;
				ori[0] = 1000000;
				while(true) {
					int wantIndex = -1;
					int oriIndex = -1;
					for(int i = 4; i >= 0; i--) { 
						if(want[i] != 0 && wantIndex == -1) {
							wantIndex = i;
						}
						if(ori[i] != 0 && oriIndex == -1) {
							oriIndex = i;
						}
					}
					
					if(wantIndex == 0 && oriIndex == 0) {
						break;
					}
					int consumed = Math.min(want[wantIndex], ori[oriIndex]);
					want[wantIndex] -= consumed;
					ori[oriIndex] -= consumed;
					ans += Math.abs(wantIndex - oriIndex) * consumed;
				}
				min = Math.min(min, ans);
			}
		}

		if(min == Integer.MAX_VALUE){
			out.println(-1);
		}else{
			out.println(min/2);
		}

		out.close();
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
