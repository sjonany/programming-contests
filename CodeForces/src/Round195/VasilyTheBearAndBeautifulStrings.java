package Round195;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Scanner;

public class VasilyTheBearAndBeautifulStrings {
	static long M = 1000000007;
	static BigInteger MOD = BigInteger.valueOf(M);
	static long[] fac = new long[200005];
	public static void main(String[] args) throws Exception{
		Scanner in = new Scanner(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int m = in.nextInt();
		int g = in.nextInt();
		
		fac[0] = 1;
		for(int i = 1 ; i <= m+n; i++){
			fac[i] = (fac[i-1] * i) % M;
		}
		
		// dp[i] = # one-convertibles , i zeroes and m ones
		
		long[] dp = new long[n+1];

		Long ans = null;
		// no zeroes
		// switch #ones
		if(m == 0){
			// all zeroes, only 1 way to make a 1
			if(n == 1){
				ans = 0l;
			}else{
				ans = (long)((n+1)%2);
			}
		}else if(m == 1){
			dp[0] = 1;
		}else{
			dp[0] = 0;
		}
		
		if(ans == null){
			for(int i = 1; i <= n; i++){
				dp[i] = c(m-1 + i, m) - dp[i-1];
				if(dp[i]<0)dp[i]+= M;
				dp[i] %= M;
			}
			ans = dp[n];
		}
		
		if(g == 0){
			ans = c(m+n,n)-ans;
			if(ans<0)ans+= M;
			ans %= M;
		}
		
		out.println(ans);
		out.close();
	}
	
	static long c(int n, int k){
		return BigInteger.valueOf(fac[n]).multiply(BigInteger.valueOf(fac[k] * fac[n-k]).modInverse(MOD)).mod(MOD).longValue();
	}
}
