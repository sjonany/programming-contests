package Round195;
import java.io.PrintWriter;
import java.util.Scanner;


public class VasilyTheBearAndFly {
	public static void main(String[] args) throws Exception{
		Scanner in = new Scanner(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int m = in.nextInt();
		int R = in.nextInt();
		
		double diag = Math.sqrt(2*R*R);
		double bend = 2*R + diag;
		double longBend = 2*R + 2*diag;
		double leftAns = 0;
		double straightAns = 0;
		for(int start = 1; start <= m; start++){
			straightAns += 2*R;
			leftAns += start >= 2 ? bend : 0;
			if(start >= 3){
				leftAns += longBend * (start-2);
				leftAns += 1.0 * R * (start-2) * (start-3);
			}
		}
		
		out.println(1.0*(2*leftAns + straightAns)/m/m);
		out.close();
	}
}