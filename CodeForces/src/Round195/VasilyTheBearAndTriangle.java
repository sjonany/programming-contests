package Round195;
import java.awt.Point;
import java.io.PrintWriter;
import java.util.Scanner;


public class VasilyTheBearAndTriangle {
	public static void main(String[] args) throws Exception{
		Scanner in = new Scanner(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int x = in.nextInt();
		int y = in.nextInt();
		
		Point p1 = new Point();
		Point p2 = new Point();
	
		p1.x = 0;
		p1.y = (Math.abs(x) + Math.abs(y)) * (y / Math.abs(y));
		p2.y = 0;
		p2.x = (Math.abs(x) + Math.abs(y)) * (x / Math.abs(x));
		
		if(p1.x > p2.x){
			Point p = p1;
			p1 = p2;
			p2 = p;
		}
		out.println(p1.x + " " + p1.y + " " + p2.x +  " " + p2.y);
		out.close();
	}
}
