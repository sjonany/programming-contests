package Round219;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class CountingRectanglesIsFun {
	
	static PrintWriter out;
	static InputReader in;
	static int ROW;
	static int COL;
	static int[][] count;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
	
		ROW = in.nextInt();
		COL = in.nextInt();
		int q = in.nextInt();
		
		char[][] board = new char[ROW][COL];
		for(int i=0;i<ROW;i++){
			String tok = in.nextToken();
			for(int j=0;j<COL;j++){
				board[i][j] = tok.charAt(j);
			}
		}
		
		count = new int[ROW][COL];
		for(int r=0;r<ROW;r++) {
			int curCountRow = 0;
			for(int c=0;c<COL;c++) {
				boolean isW = board[r][c] == '1';
				if(isW) {
					curCountRow++;
				}
				count[r][c] = curCountRow;
				if(r != 0){
					count[r][c] += count[r-1][c];
				}
			}	
		}
		
		// num of all rects whose top left is exactly source, and bot right<=
		int[][][][] countCorner = new int[ROW][COL][ROW][COL];
		for(int ur = 0; ur < ROW; ur++){
			for(int lc = 0; lc < COL; lc++) {
				for(int br = ur; br < ROW; br++) {
					for(int rc = lc; rc < COL; rc++) {
						int pushleft=0;
						int pushup=0;
						int pushleftup=0;
						
						if(rc > lc) {
							pushleft = countCorner[ur][lc][br][rc-1];
						}
						
						if(br > ur) {
							pushup = countCorner[ur][lc][br-1][rc];
							if(rc>lc){
								pushleftup = countCorner[ur][lc][br-1][rc-1];
							}
						}
						
						countCorner[ur][lc][br][rc] = pushleft+pushup-pushleftup;
						if(countSq(ur,br,lc,rc) == 0) {
							countCorner[ur][lc][br][rc]++;
						}
					}	
				}
			}
		}
		
		int[][][][] sum = new int[ROW][COL][ROW][COL];
		for(int w=1;w<=COL;w++){
			for(int h=1;h<=ROW;h++){
				for(int ur = 0; ur < ROW; ur++) {
					int br = ur + h - 1;
					if(br >= ROW) break;
					for(int lc =0;lc<COL;lc++) {
						int rc = lc+w-1;
						if(rc>=COL)break;
						int pushbot = 0;
						int pushright = 0;
						int pushbr = 0;
						
						if(h>1){
							pushbot = sum[ur+1][lc][br][rc];
						}
						
						if(w>1){
							pushright = sum[ur][lc+1][br][rc];
							if(h>1) {
								pushbr = sum[ur+1][lc+1][br][rc];
							}
						}
						
						sum[ur][lc][br][rc] = pushbot+pushright-pushbr+countCorner[ur][lc][br][rc];
					}
				}
			}	
		}
		
		for(int i=0;i<q; i++){
			int ur = in.nextInt()-1;
			int lc = in.nextInt()-1;
			int br = in.nextInt()-1;
			int rc = in.nextInt()-1;
			out.println(sum[ur][lc][br][rc]);
		}
		
		out.close();
	}
	
	static int countSq(int top, int bot, int left, int right) {
		int ans = count[bot][right];
		if(left > 0) {
			ans -= count[bot][left-1];
		}
		if(top > 0) {
			ans -= count[top-1][right];
			if(left > 0){
				ans += count[top-1][left-1];
			}
		}
		return ans;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
