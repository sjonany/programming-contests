package Round219;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class ChoosingSubtreeIsFun {
	
	static int[][] edges;
	static int numedges;
	static TreeSet<Node> set;
	
	public static void main(String[] args) throws Exception {
		set = new TreeSet<Node>(new Comparator<Node>() {
			@Override
			public int compare(Node o1, Node o2) {
				return o1.dfsorder - o2.dfsorder;
			}
		});
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk = new StringTokenizer(in.readLine());
		int N = Integer.parseInt(tk.nextToken());
		int K = Integer.parseInt(tk.nextToken());
		
		List<Integer>[] slo = new List[N];
		for(int i=0;i<N;i++)slo[i] = new ArrayList();
		for(int i=0;i<N-1;i++) {
			tk = new StringTokenizer(in.readLine());
			int a = Integer.parseInt(tk.nextToken())-1;
			int b = Integer.parseInt(tk.nextToken())-1;
			slo[a].add(b);
			slo[b].add(a);
		}
		edges = new int[N][];
		for(int i=0;i<N;i++){
			edges[i] = new int[slo[i].size()];
			for(int j=0;j<edges[i].length;j++)edges[i][j]=slo[i].get(j);
		}
		
		numedges = 0;
		initLCA(N, 0);
		
		int l=0;
		int r=0;
		set.add(new Node(0, tin[0]));
		int max = 0;
		while(true){ 
			if(l>=N || r>=N)break;
			if(numedges+1 <= K) {
				max = Math.max(max,r-l+1);
				r++;
				if(r<N)
					add(r);
			} else {
				remove(l);
				l++;
				if(l>r && l< N){
					r=l;
					numedges=0;
					set.add(new Node(l, tin[l]));
				}
			}
		}
		out.println(max);
		out.close();
	}
	
	static int dist(int i, int j){
		return depth[i] + depth[j] - 2 * depth[lca(i,j)];
	}
	
	static void add(int i){
		set.add(new Node(i, tin[i]));
		numedges+=delta(i);
	}
	
	static void remove(int i){
		numedges-=delta(i);
		set.remove(new Node(i, tin[i]));
	}
	
	//change in number of edges
	static int delta(int i) {
		Node n = new Node(i, tin[i]);
		Node prev = set.lower(n);
		Node next = set.higher(n);
		if(prev == null) {
			prev = set.last();
		}
		if(next == null) {
			next = set.first();
		}
		return (dist(prev.id, n.id) + dist(n.id, next.id) - dist(prev.id, next.id)) / 2;
	}
	
	static class Node {
		public int id;
		public int dfsorder;
		public Node(int id, int dfsorder) {
			this.id = id;
			this.dfsorder = dfsorder;
		}
	}
	
	static int[] tin, tout;
	static int[][] up;
	static int timer;
	static int l;
	static int[] depth;

	static void dfs(int v, int p){
		depth[v] = depth[p] + 1;
		tin[v] = ++timer;
		up[v][0] = p;
	    for(int i = 1; i <= l; i++ )
			up[v][i] = up[up[v][i-1]][i-1];
	    
		for(int to : edges[v]){
			if (to != p)
				dfs(to, v);
		}
		tout[v] = ++timer;
	}

	static boolean upper(int a, int b){
		return tin[a] <= tin[b] && tout[a] >= tout[b];
	}

	static int lca(int a, int b){
		if(upper(a, b)) return a;
		if(upper(b, a)) return b;
		for (int i = l; i >= 0; i--)
			if(!upper(up[a][i], b))
				a = up[a][i];
		return up[a][0];
	}

	static void initLCA(int n, int root){
		tin = new int[n];
		tout = new int[n];
		up = new int[n][];
		depth = new int[n];
		
		l = 1;
		while((1 << l) <= n) ++l;
		for (int i = 0; i <n; i++) 
	        up[i] = new int[l + 1];
		depth[root] = -1;
		dfs(root, root);
	}
}
