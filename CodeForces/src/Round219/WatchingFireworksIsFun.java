package Round219;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.StringTokenizer;

public class WatchingFireworksIsFun {
	static PrintWriter out;
	static InputReader in;
	static StringTokenizer tk;

	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);

		int N = in.nextInt();
		int M = in.nextInt();
		int D = in.nextInt();

		long offset = 0;
		int[] pos = new int[M];
		int[] time = new int[M];

		for(int i=0;i<M;i++) {
			int a = in.nextInt();
			int b = in.nextInt();
			int t = in.nextInt();
			time[i] = t;
			offset += b;
			pos[i] = a-1;
		}

		// best reward if at the end of ith fireword, and at pos j
		int[] dpPrev = new int[N];
		int[] dpCur = new int[N];

		for(int i=0;i<N;i++){
			dpPrev[i] = -Math.abs(pos[0] - i);
		}

		int DMAX = 150005;
		Deque posdeq = new Deque(DMAX);
		Deque valdeq = new Deque(DMAX);
		for(int i = 1; i < M; i++) {
			posdeq.reset();
			valdeq.reset();
			// window width =  2 * sz + 1;
			int sz = (int)Math.min(N,((long)(time[i] - time[i-1]) * D));
			int maxRight = Math.min(N-1, sz);
			for(int p=0; p<=maxRight; p++) {
				int newit = dpPrev[p];
				while(posdeq.dsize > 0 && valdeq.deq[valdeq.back] <= newit) {
					valdeq.removeBack();
					posdeq.removeBack();
				}
				posdeq.addBack(p);
				valdeq.addBack(newit);
			}
			int leftpos = -sz;
			int rightpos = sz;
			for(int j = 0; j < N; j++) {
				while(posdeq.dsize > 0 && posdeq.deq[posdeq.front] < leftpos) {
					posdeq.removeFront();
					valdeq.removeFront();
				}
				if(rightpos < N) {
					int newit = dpPrev[rightpos];
					while(posdeq.dsize > 0 && valdeq.deq[valdeq.back] <= newit) {
						valdeq.removeBack();
						posdeq.removeBack();
					}
					posdeq.addBack(rightpos);
					valdeq.addBack(newit);
				}
				dpCur[j] = valdeq.deq[valdeq.front] - Math.abs(pos[i] - j);
				leftpos++;
				rightpos++;
			}

			int[] temp = dpCur;
			dpCur = dpPrev;
			dpPrev = temp;
		}

		int max = Integer.MIN_VALUE;
		for(int i=0;i<N;i++) {
			max = Math.max(max, dpPrev[i]);
		}

		out.println(max+offset);
		out.close();
	}

	// non circular deque, optimized becuase don't have to handle circular
	static class Deque{
		public int[] deq;
		public int front;
		public int back;
		public int cap;
		public int dsize;
		
		public Deque(int cap) {
			this.deq = new int[cap];
			this.cap = cap;
			reset();
		}
		
		public void reset() {
			front = 0;
			back = -1;
			dsize = 0;
		}

		public void addBack(int i) {
			dsize++;
			back++;
			//if(back==cap)back = 0;
			deq[back] = i;
		}

		public void addFront(int i) {
			dsize++;
			front--;
			//if(front<0)front=cap-1;
			deq[front] = i;
		}

		public int removeFront() {
			dsize--;
			int it = deq[front];
			front++;
			//if(front==cap)front=0;
			return it;
		}

		public int removeBack() {
			dsize--;
			int it = deq[back];
			back--;
			//if(back<0)back=cap-1;
			return it;
		}
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
