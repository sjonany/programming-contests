package Round219;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Scanner;


public class MakingSequencesIsFun {
	static long w;
	static long m;
	static long k;
	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		w = sc.nextLong();
		m = sc.nextLong();
		k = sc.nextLong();
		
		long curnum = m;
		long remCredit = w;
		
		while(true) {
			long next = max(countDig(curnum));
			BigInteger curCost = cost(curnum, next);
			if(curCost.compareTo(BigInteger.valueOf(remCredit)) <= 0) {
				remCredit-=curCost.longValue();
				curnum = next+1;
			} else {
				break;
			}
		}
		
		long nums = remCredit / (countDig(curnum) * k);
		out.println(nums - m + curnum);
		out.close();
	}
	
	// must have same digs
	static BigInteger cost(long start, long end) {
		return BigInteger.valueOf(end - start + 1).multiply(BigInteger.valueOf(countDig(start))).multiply(BigInteger.valueOf(k));
	}
	
	static long max(long countDig) {
		return (long) (Math.pow(10, countDig) - 1);
	}
	
	static long countDig(long x){
		long dig = 0;
		while(x>0){
			dig++;
			x/=10;
		}
		return dig;
	}
}
