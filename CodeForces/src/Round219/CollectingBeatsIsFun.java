package Round219;
import java.io.PrintWriter;
import java.util.Scanner;


public class CollectingBeatsIsFun {
	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int k = sc.nextInt();
		int[] count = new int[10];
		for(int i=0;i<4;i++){
			String s = sc.next();
			for(int j=0;j<s.length();j++){
				char c = s.charAt(j);
				if(c!='.'){
					count[c-'0']++;
				}
			}
		}
		boolean good = true;
		for(int i=1;i<=9;i++){
			if(count[i] > 2*k){
				good= false;
				break;
			}
		}
		out.println(good?"YES":"NO");
		out.close();
	}
}
