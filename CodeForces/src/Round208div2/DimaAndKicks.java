package Round208div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class DimaAndKicks {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	static int R;
	static int C;
	static int boardOneCount = 0;
	static boolean[][] board;
	static int maxLen;

	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		tk = tk(in.readLine());
		R = Integer.parseInt(tk.nextToken());
		C = Integer.parseInt(tk.nextToken());
		board = new boolean[R][C];

		for (int r = 0; r < R; r++) {
			tk = tk(in.readLine());
			for (int c = 0; c < C; c++) {
				board[r][c] = tk.nextToken().charAt(0) == '1';
				if(board[r][c]){
					boardOneCount++;
				}
			}
		}
		
		maxLen = 0;
		initMaxLen();
		maxLen--;
		
		edges = new int[R*C][4];
		List<Integer> ans = new ArrayList<Integer>();
		for(int len = 2; len <= maxLen; len++) {
			if(maxLen % len == 0){
				if(check(len)){
					ans.add(len);
				}
			}
		}
		if(ans.size() == 0){
			out.println(-1);
		}else{
			for(int i : ans) out.print(i + " ");
		}
		
		out.close();
	}

	static int[][] edges;
	static int[] edgeSize;
	static Map<Node, Integer> nodeMap;
	static int nodeCount;
	static int repOneCount;
	static Node[] nodes;
	
	static boolean check(int len){
		repOneCount = 0;
		nodeCount = 0;
		nodeMap = new HashMap<Node, Integer>();
		nodes = new Node[R*C];
		edgeSize = new int[R*C];
		
		//find corner node
		Node first = null;
		for(int r=0;r<R;r++){
			for(int c=0;c<C;c++){
				if(board[r][c]){
					first = new Node(r,c);
					break;
				}
			}
			if(first!=null)break;
		}
		
		repOneCount++;
		nodes[nodeCount] = first;
		nodeMap.put(first, nodeCount++);
		
		//find all the other nodes
		for(int r=0;r<R;r++){
			for(int c=0;c<C;c++){
				int dr = Math.abs(first.r - r);
				int dc = Math.abs(first.c - c);
				if(r==first.r && c == first.c) continue;
				if(board[r][c] && dr % len == 0 && dc % len == 0){
					repOneCount++;
					nodes[nodeCount] = new Node(r,c);
					nodeMap.put(nodes[nodeCount], nodeCount++);
				}
			}	
		}
		
		for(int i=0; i < nodeCount; i++){
			int r = nodes[i].r;
			int c = nodes[i].c;
			
			//to the right
			boolean isEdge = true;
			for(int step = 0; step < len; step++){
				c++;
				if(c >= C || !board[r][c]){
					isEdge = false;
					break;
				}
			}
			if(isEdge) {
				repOneCount += len - 1;
				int otherId = nodeMap.get(new Node(r, c));
				edges[i][edgeSize[i]++] = otherId;
				edges[otherId][edgeSize[otherId]++] = i;
			}
			
			r = nodes[i].r;
			c = nodes[i].c;
			
			//to bottom
			isEdge = true;
			for(int step = 0; step < len; step++){
				r++;
				if(r >= R || !board[r][c]){
					isEdge = false;
					break;
				}
			}
			if(isEdge) {
				repOneCount += len - 1;
				int otherId = nodeMap.get(new Node(r,c));
				edges[i][edgeSize[i]++] = otherId;
				edges[otherId][edgeSize[otherId]++] = i;
			}
		}

		if(repOneCount != boardOneCount){
			return false;
		}
		
		return isEulerPath();
	}
	
	static boolean isEulerPath(){
		visit = new boolean[nodeCount];
		numVisit = 0;
		dfs(0);
		
		if(numVisit != nodeCount){
			return false;
		}
		
		int numOdd = 0;
		for(int i = 0; i < nodeCount; i++){
			if(edgeSize[i] % 2 == 1){
				numOdd++;
			}
		}
		return numOdd == 0 || numOdd == 2;
	}

	static boolean[] visit;
	static int numVisit;
	public static void dfs(int vi){
		visit[vi] = true;
		numVisit++;
		for(int i = 0; i < edgeSize[vi]; i++){
			int vout = edges[vi][i];
			if(!visit[vout]){
				dfs(vout);
			}
		}
	}
	
	static class Node{
		public int r;
		public int c;
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + c;
			result = prime * result + r;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			Node other = (Node) obj;
			if (c != other.c)
				return false;
			if (r != other.r)
				return false;
			return true;
		}
		public Node(int r, int c) {
			this.r = r;
			this.c = c;
		}
		@Override
		public String toString() {
			return "Node [r=" + r + ", c=" + c + "]";
		}
		
		
	}
	
	static void initMaxLen() {
		for (int r = 0; r < R; r++) {
			boolean isIn = false;
			int count = 0;
			for (int c = 0; c < C; c++) {
				if(isIn){
					maxLen = Math.max(maxLen, count);
					if(board[r][c]){
						count++;
					}else{
						count=0;
						isIn = false;
					}
					maxLen = Math.max(maxLen, count);
				}else{
					if(board[r][c]){
						isIn = true;
						count++;
					}
				}
			}
		}
		
		for (int c = 0; c < C; c++) {
			boolean isIn = false;
			int count = 0;
			for (int r = 0; r < R; r++) {
				if(isIn){
					maxLen = Math.max(maxLen, count);
					if(board[r][c]){
						count++;
					}else{
						isIn = false;
						count=0;
					}
					maxLen = Math.max(maxLen, count);
				}else{
					if(board[r][c]){
						isIn = true;
						count++;
					}
				}
			}
		}
	}
	
	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
