package Round208div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class DimaAndHares {
    static BufferedReader in;
    static PrintWriter out;
    static StringTokenizer tk;
    
    public static void main(String[] args) throws Exception{
        in = new BufferedReader(new InputStreamReader(System.in));
        out = new PrintWriter(System.out);
        
        int n = Integer.parseInt(in.readLine());
        int[][] p = new int[3][n];
        for(int i = 0 ; i < 3 ; i++) {
            tk = tk(in.readLine());
            for(int j = 0 ; j < n; j++) {
                p[i][j] = Integer.parseInt(tk.nextToken());
            }
        }
        
        int[][] dp = new int[2][n];
        dp[0][0] = p[0][0];
        dp[1][0] = -Integer.MAX_VALUE/2;
        for(int i = 1 ; i < n; i++) {
            dp[0][i] = p[0][i] + Math.max(dp[0][i-1] - p[0][i-1] + p[1][i-1], dp[1][i-1] - p[1][i-1] + p[2][i-1]);
            dp[1][i] = p[1][i] + Math.max(dp[0][i-1], dp[1][i-1]);
        }
        out.println(Math.max(dp[1][n-1], dp[0][n-1]));
        out.close();
    }
    
    static StringTokenizer tk(String s){
        return new StringTokenizer(s);
    }
}