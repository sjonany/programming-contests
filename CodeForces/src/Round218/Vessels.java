package Round218;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Vessels {
	
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	static int[] cap;
	static long[] amount;
	static int[] next;
	static int N;
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		N = Integer.parseInt(in.readLine());
		cap = new int[N];
		next = new int[N];
		for(int i=0;i<N;i++){
			next[i]=i+1;
		}
		tk = tk(in.readLine());
		for(int i=0;i<N;i++){
			cap[i]=Integer.parseInt(tk.nextToken());
		}
		
		int m = Integer.parseInt(in.readLine());
		amount = new long[N];
		for(int i=0;i<m;i++){
			tk = tk(in.readLine());
			if(Integer.parseInt(tk.nextToken()) == 1){
				int target = Integer.parseInt(tk.nextToken())-1;
				int amot = Integer.parseInt(tk.nextToken());
				fill(target, amot);
			}else{
				out.println(amount[Integer.parseInt(tk.nextToken())-1]);
			}
		}
		
		out.close();
	}
	
	static int fill(int target, int amot) {
		amount[target] += amot;
		int leftover = 0;
		if(amount[target] > cap[target]) {
			leftover = (int) (amount[target] - cap[target]);
			amount[target] = cap[target];
		}
		if(target != N-1 && leftover > 0) {
			next[target] = fill(next[target], leftover);
			return next[target];
		} else {
			return target;
		}
	}
	
	
	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
