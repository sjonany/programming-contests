package Round218;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Hamburgers {
	
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		String s = in.readLine();
		int[] count = new int[26];
		for(int i=0;i<s.length();i++) {
			count[s.charAt(i)-'A']++;
		}
		
		tk = tk(in.readLine());
		int nb = Integer.parseInt(tk.nextToken());
		int ns = Integer.parseInt(tk.nextToken());
		int nc = Integer.parseInt(tk.nextToken());

		tk = tk(in.readLine());
		int pb = Integer.parseInt(tk.nextToken());
		int ps = Integer.parseInt(tk.nextToken());
		int pc = Integer.parseInt(tk.nextToken());
		
		long r = Long.parseLong(in.readLine());
		long lo = 0;
		long hi = r+nb+ns+nc;
		
		while(hi>lo) {
			long mid = (lo+hi+1)/2;
			boolean good = true;
			
			long needb = count['B'-'A'] * mid;
			long needs = count['S'-'A'] * mid;
			long needc = count['C'-'A'] * mid;
			
			long buyb = Math.max(0, needb - nb);
			long buys = Math.max(0, needs - ns);
			long buyc = Math.max(0, needc - nc);
			
			if(buyb*pb+buys*ps+buyc*pc > r){
				good = false;
			}
			
			if(good) {
				lo = mid;
			}else {
				hi = mid-1;
			}
		}
		
		out.println(lo);
		out.close();
	}
	
	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
