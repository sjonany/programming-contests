package Round218;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class SubwayInnovation {
	
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	static long[] pref;
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		int N = Integer.parseInt(in.readLine());
		tk = tk(in.readLine());
		Station[] arr = new Station[N];
		for(int i=0;i<N;i++)arr[i] = new Station(i+1, Long.parseLong(tk.nextToken()));
		Arrays.sort(arr, new Comparator<Station>() {
			@Override
			public int compare(Station o1, Station o2) {
				return Long.compare(o1.pos, o2.pos);
			}
		});
		
		int K = Integer.parseInt(in.readLine());
		
		long[] dists = new long[N-1];
		for(int i=0;i<N-1;i++) {
			dists[i] = arr[i+1].pos-arr[i].pos;
		}
		
		pref = new long[dists.length];
		pref[0] = dists[0];
		for(int i=1;i<pref.length;i++){
			pref[i] = pref[i-1]+dists[i];
		}
		
		int NUM_SEG = K-1;
		// K*a[i] + K-1 * a[i+1] + ... + 1 * a[i+K-1], a[] = dist
		long[] fronttoback = new long[dists.length];
		for(int i=0;i<NUM_SEG;i++){
			fronttoback[0] += (NUM_SEG-i)*dists[i];
		}
		for(int i=1;i<fronttoback.length;i++) {
			int newel = i + NUM_SEG - 1;
			if(newel >= dists.length) break;
			fronttoback[i] = fronttoback[i-1] - NUM_SEG * dists[i-1] + getSumDist(i, newel);
		}
		
		// 1*a[i] + 2*a[i+1] + ... + K * a[i+K-1]
		long[] backtofront = new long[dists.length];
		for(int i=0;i<NUM_SEG;i++){
			backtofront[0] += (i+1)*dists[i];
		}
		for(int i=1;i<backtofront.length;i++) {
			int newel = i + NUM_SEG - 1;
			if(newel >= dists.length) break;
			backtofront[i] = backtofront[i-1] + NUM_SEG * dists[i+NUM_SEG-1] - getSumDist(i-1, newel-1);
		}
		
		long cost = 0;
		for(int i=0;i<K-1;i++) {
			cost += (i+1)*(NUM_SEG-i)*dists[i];
		}
		
		long mincost = cost;
		int besti = 0;
		for(int i = 1; i < N-1; i++) {	
			int newel = i + NUM_SEG - 1;
			if(newel >= dists.length) break;
			cost = cost - fronttoback[i-1] + backtofront[i];
			if(mincost > cost) {
				mincost = cost;
				besti = i;
			}
		}
		
		for(int i=besti;i<=besti+NUM_SEG;i++) {
			out.print(arr[i].id + " ");
		}
		
		out.close();
	}
	
	static long getSumDist(int i, int j) {
		long p = 0;
		if(i!=0){
			p = pref[i-1];
		}
		return pref[j] - p;
	}
	
	static class Station {
		public int id;
		public long pos;
		public Station(int id, long pos) {
			this.id = id;
			this.pos = pos;
		}
		
		
	}
	
	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
