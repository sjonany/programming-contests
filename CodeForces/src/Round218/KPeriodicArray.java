package Round218;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class KPeriodicArray {
	
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		tk = tk(in.readLine());
		int N = Integer.parseInt(tk.nextToken());
		int K = Integer.parseInt(tk.nextToken());
		int[] arr = new int[N];
		tk = tk(in.readLine());
		for(int i=0;i<N;i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}
		
		int cost = 0;
		
		for(int i=0;i<K;i++) {
			int cur = i;
			int countOne = 0;
			while(cur<N){
				if(arr[cur] == 1)countOne++;
				cur+=K;
			}
			cost += Math.min(countOne, N/K - countOne);
		}
		
		
		out.println(cost);
		out.close();
	}
	
	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
