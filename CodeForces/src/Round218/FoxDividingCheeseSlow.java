package Round218;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

public class FoxDividingCheeseSlow {
	
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	// log^3 soln
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		tk = tk(in.readLine());
		int a = Integer.parseInt(tk.nextToken());
		int b = Integer.parseInt(tk.nextToken());
		
		Map<Integer, Integer> aans = solve(a);
		Map<Integer, Integer> bans = solve(b);
		
		int min = Integer.MAX_VALUE;
		for(int target : aans.keySet()) {
			if(bans.containsKey(target)) {
				min = Math.min(min, aans.get(target) + bans.get(target));
			}
		}
		if(min == Integer.MAX_VALUE) {
			out.println(-1);
		}else{
			out.println(min);
		}
		out.close();
	}
	
	static Map<Integer, Integer> solve(int a) {
		Map<Integer, Integer> numToCost = new HashMap<Integer, Integer>();
		
		Queue<Integer> q = new LinkedList<Integer>();
		numToCost.put(a, 0);
		
		int[] cans = new int[3];
		int cansize = 0;

		q.add(a);
		while(!q.isEmpty()){
			int cur = q.poll();
			cansize=0;
			if(cur%2==0){
				cans[cansize++] = cur/2;
			}if(cur%3==0){
				cans[cansize++] = cur/3;
			}if(cur%5==0){
				cans[cansize++] = cur/5;
			}
			int newdist = numToCost.get(cur)+1;
			for(int i=0;i<cansize;i++) {
				int child = cans[i];
				if(!numToCost.containsKey(child)){
					numToCost.put(child, newdist);
					q.add(child);
				}
			}
		}
		
		return numToCost;
	}
	
	
	
	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
