package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class I {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int x1 = Integer.parseInt(tk.nextToken());
			int y1 = Integer.parseInt(tk.nextToken());
			int x2 = Integer.parseInt(tk.nextToken());
			int y2 = Integer.parseInt(tk.nextToken());
			if(x1 == 0 && y1 == 0 && x2 == 0 && y2 == 0)break;
			tk = new StringTokenizer(reader.readLine());
			int wx1 = Integer.parseInt(tk.nextToken());
			int wy1 = Integer.parseInt(tk.nextToken());
			int wx2 = Integer.parseInt(tk.nextToken());
			int wy2 = Integer.parseInt(tk.nextToken());
			
			if(x1 > x2){
				int temp = x1;
				x1 = x2;
				x2 = temp;
				temp = y1;
				y1 = y2;
				y2 = temp;
			}
			
			if(wx1 > wx2){
				int temp = wx1;
				wx1 = wx2;
				wx2 = temp;
				temp = wy1;
				wy1 = wy2;
				wy2 = temp;
			}
			
			double d1 = Math.min(distance(x1,y1,wx1,wy1) + distance(x2,y2, wx1,wy1), 
					distance(x1,y1,wx2,wy2) + distance(x2,y2, wx2,wy2));
			
			double directD = distance(x1,y1,x2,y2);
			double xInt = Double.MAX_VALUE;
			double yInt = Double.MAX_VALUE;

			double m1 = getGradient(x1,y1,x2,y2);
			if(m1 == Double.MAX_VALUE){
				double m2 = getGradient(wx1, wy1, wx2, wy2);
				if(m2 == Double.MAX_VALUE){
				}else{
					double c2 = getIntercept(wx1, wy1, m2);
					yInt = m2 * x1 + c2;
					xInt = x1;
				}
			}else{
				double c1 = getIntercept(x1, y1, m1);
				double m2 = getGradient(wx1, wy1, wx2, wy2);
				if(m2 == Double.MAX_VALUE){
					yInt = m1 * wx1 + c1;
					xInt = wx1;
				}else{
					double c2 = getIntercept(wx1, wy1 , m2);
					xInt = (c2-c1)/(m1-m2);
					yInt = m2 * xInt + c2;
				}
			}
			
			if(xInt != Double.MAX_VALUE 
					&& xInt <= wx2 && xInt >= wx1 && yInt <= Math.max(wy2, wy1) && yInt >= Math.min(wy1, wy2)
					&& xInt <= x2 && xInt >= x1 && yInt <= Math.max(y2, y1) && yInt >= Math.min(y1, y2)){
			}else{
				d1 = Math.min(d1, directD);
			}
			
			out.printf("Case " + t + ": %.3f\n", d1/2.0);
			t++;
		}
		out.close();
	}
	
	static double getGradient(int x1, int y1, int x2, int y2){
		if(x1 == x2){
			return Double.MAX_VALUE;
		}
		return 1.0*(y2-y1)/(x2-x1);
	}
	
	static double getIntercept(int x1, int y1, double grad){
		return y1 - grad * x1;
	}
	
	static double distance(int x1, int y1, int x2, int y2){
		return Math.sqrt(Math.pow(y2-y1,2) + Math.pow(x2-x1,2));
	}
}
