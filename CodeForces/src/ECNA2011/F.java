package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

//oops misunderstood. didn't know u can force non-existing edges
public class F {
	static int N;
	static int[][] adjMap;
	static int[][] costMap;
	static int[] path;
	//dp[i][j] = min dist to travel from path[0] to path[i], with j forced paths, and forced paths range from 0->i
	static int[][] dp;
	static int M;
	
	static int MAX_N = 100;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		adjMap = new int[MAX_N][MAX_N];
		costMap = new int[MAX_N][MAX_N];
		dp = new int[MAX_N][MAX_N];
		path = new int[MAX_N];
		int t = 1;
		while(true){
			N = Integer.parseInt(reader.readLine());
			if(N == 0) break;
			for(int i = 0; i < N; i++){
				StringTokenizer tk = new StringTokenizer(reader.readLine());
				for(int j = 0; j < N; j++){
					adjMap[i][j] = Integer.parseInt(tk.nextToken());
				}
			}
			
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			M = Integer.parseInt(tk.nextToken());
			
			int oriPathCost = 0;
			for(int i = 0; i < M; i++){
				path[i] = Integer.parseInt(tk.nextToken());
				if(i >= 1){
					oriPathCost += adjMap[path[i-1]][path[i]];
				}
			}
			
			floydWarshall();		
			//i = path[0] -> path[i]
			for(int i = 0; i < M; i++){
				dp[i][0] = costMap[path[0]][path[i]];
				//j = num forced path
				for(int j = 1; j < i+1; j++){
					dp[i][j] = Integer.MAX_VALUE;
					//k is the starting point of last forced path
					for(int k = 0; k < i; k++){
						int forceCost = adjMap[path[k]][path[k+1]];
						if(forceCost == 0){
							continue;
						}
						int remCost = costMap[path[k+1]][path[i]];
						if(k == i-1){
							remCost = 0;
						}
						if(remCost == Integer.MAX_VALUE){
							continue;
						}
						
						int prevCost = dp[k][j-1];
						if(k == 0){
							prevCost = 0;
						}
						if(prevCost == Integer.MAX_VALUE){
							continue;
						}
						
						dp[i][j] = Math.min(dp[i][j], prevCost + forceCost + remCost);
					}
				}
			}
			
			for(int k = 0; k < M; k++){
				if(dp[M-1][k] == oriPathCost){
					out.println("Case " + t + ": " + k);
					break;
				}
			}
			
			t++;
		}
		
		out.close();
	}
	
	static void p(int[][] m){
		for(int i = 0; i < m.length; i++){
			for(int j = 0; j < m[0].length; j++){
				System.out.print(m[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	static void floydWarshall(){
		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++){
				costMap[i][j] = Integer.MAX_VALUE;
			}
		}
		
		for(int i = 0; i < N; i++){
			costMap[i][i] = 0;
		}
		
		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++){
				if(adjMap[i][j] != 0){
					costMap[i][j] = adjMap[i][j];
				}
			}
		}
		
		for(int k = 0; k < N; k++){
			for(int i = 0; i < N; i++){
				for(int j = 0; j < N; j++){
					if(costMap[i][k] != Integer.MAX_VALUE && costMap[k][j] != Integer.MAX_VALUE){
						if(costMap[i][k] + costMap[k][j] < costMap[i][j]){
							costMap[i][j] = costMap[i][k] + costMap[k][j];
						}
					}
				}
			}
		}
	}
}
