package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class B {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int s = Integer.parseInt(tk.nextToken());
			int c = Integer.parseInt(tk.nextToken());
			int p = Integer.parseInt(tk.nextToken());
			int l = Integer.parseInt(tk.nextToken());
			if(s == 0 && c == 0 && p == 0 && l == 0)break;
			
			boolean[][] visit = new boolean[s][c];
			visit[p][l] = true;
			boolean found = false;
			for(int k = 1; k <= s*c; k++){
				p++;
				l++;
				if(p == s) p = 0;
				if(l == c) l = 0;
				if(visit[p][l]){
					break;
				}
				if(p == l && l == 0){
					out.println("Case " + t + ": " + (k/s) + " " + k%s + "/" + s);
					found = true;
					break;
				}
			}
			if(!found){
				out.println("Case " + t + ": Never");
			}
			
			t++;
		}
		
		out.close();
	}
}
