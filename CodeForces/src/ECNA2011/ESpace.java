package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class ESpace {
	//using ping's trick pre^=1. cool swap
	//dp[i][j] = number of ways if person 't' is there, and total value = j, using person of types 0->i	
	/* 
(a) if i == that guy's type
	if that guy + k other of same type
	dp[i-(k+1)*val][j-1] * n-1Ck
(b) if i != that guy's type
	k = num people used. 
	sum over all k
	dp[i-k*val][j-1] * nCr
	 */
	
	static long dp[][];
	//weight
	static int w[];
	//limit
	static int l[];
	static int V = 60;
	static long[][] c;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		dp = new long[2][V+1];
		w = new int[10];
		l = new int[10];
		
		c = new long[V+1][V+1];
		
		for(int i = 0; i < c.length; i++){
			c[i][0] = 1;
		}
		for(int i = 1; i < c.length; i++){
			for(int j = 1; j <= i; j++){
				c[i][j] = c[i-1][j] + c[i-1][j-1];
			}
		}
		
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int n = Integer.parseInt(tk.nextToken());
			int q = Integer.parseInt(tk.nextToken());
			if(n == 0 && q == 0){
				break;
			}
			tk = new StringTokenizer(reader.readLine());
			
			for(int i = 0; i < n; i++){
				w[i] = Integer.parseInt(tk.nextToken());
				l[i] = Integer.parseInt(tk.nextToken());
			}
			
			out.print("Case " + t + ": ");
			for(int fixed = 0; fixed < n; fixed++){
				int pre = 0;
				int next = 1;
				for(int i = 0; i < dp.length; i++){
					for(int j = 0; j < dp[0].length; j++){
						dp[i][j] = 0;
					}
				}
				
				if(fixed == 0){
					for(int k = 0; k < l[0]; k++){
						dp[0][(k+1)*w[0]] = c[l[0]-1][k]; 
					}
				}else{
					for(int k = 0; k <= l[0]; k++){
						dp[0][k*w[0]] = c[l[0]][k];
					}
				}
				for(int i = 1; i < n; i++){
					for(int j = 0; j <= V; j++){
						dp[next][j] = 0;
						if(i == fixed){
							int k = 0;
							while(true){
								if(j - (k+1)*w[i] < 0 || (k+1) > l[i])break;
								dp[next][j] += dp[pre][j-(k+1)*w[i]] * c[l[i]-1][k];
								k++;
							}
						}else{
							int k = 0;
							while(true){
								if(j - k*w[i] < 0 || k > l[i])break;
								dp[next][j] += dp[pre][j-k*w[i]] * c[l[i]][k];
								k++;
							}
						}
					}
					pre ^= 1;
					next ^= 1;
				}
				
				long ans = 0;
				for(int v = q; v < Math.min(q + w[fixed], V+1); v++){
					ans += dp[pre][v];
				}
				out.print(ans);
				if(fixed != n-1)out.print(" ");
			}
			out.println();
			t++;
		}
		
		out.close();
	}
}
