package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class C {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int b = Integer.parseInt(tk.nextToken());
			int c = Integer.parseInt(tk.nextToken());
			if(b==0 && c ==0 ) break;
			int[][] rank = new int[b][c];
			for(int i = 0; i< b; i++){
				tk = new StringTokenizer(reader.readLine());
				for(int j = 0; j < c; j++){
					int id = Integer.parseInt(tk.nextToken());
					rank[i][id] = j;
				}
			}
			
			LinkedList<Integer> candidates = new LinkedList<Integer>();
			for(int i = 0 ; i < c; i++){
				candidates.add(i);
			}
			
			while(candidates.size() > 1){
				int id1 = candidates.getLast();
				int id2 = candidates.getFirst();
				int countA = 0;
				int countB = 0;
				for(int round = 0; round < b;  round++){
					if(rank[round][id1] < rank[round][id2]){
						countA++;
					}else{
						countB++;
					}
				}
				
				if(countA > countB){
					candidates.removeFirst();
				}else if(countA < countB){
					candidates.removeLast();
				}else{
					candidates.removeFirst();
					candidates.removeLast();
				}
			}
			
			if(candidates.size() == 0){
				out.println("Case " + t + ": No Condorcet winner");
			}else{
				int id = candidates.getFirst();
				boolean good = true;
				for(int enemy = 0; enemy < c && good; enemy++){
					if(enemy == id)continue;
					int countA = 0;
					int countB = 0;
					for(int round = 0; round < b;  round++){
						if(rank[round][id] < rank[round][enemy]){
							countA++;
						}else{
							countB++;
						}
					}
					if(countA <= countB){
						good = false;
					}
				}
				if(good){
					out.println("Case " + t + ": " + id);
				}else{
					out.println("Case " + t + ": No Condorcet winner");
				}
			}
			t++;
		}
		
		
		out.close();
	}
}
