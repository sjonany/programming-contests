package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

public class A {
	static int N;
	static String S;
	static String E;
	static Cost[] tax;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		tax = new Cost[1000];
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			N = Integer.parseInt(tk.nextToken());
			if(N == 0) break;
			S = tk.nextToken();
			E = tk.nextToken();
			
			tk = new StringTokenizer(reader.readLine());
			for(int i = 0; i < N; i++){
				tax[i] = new Cost(i,Integer.parseInt(tk.nextToken()));
			}
			
			List<Cost> toOn = new ArrayList<Cost>();
			List<Cost> toOff = new ArrayList<Cost>();
			List<Cost> rightOnes = new ArrayList<Cost>();
			
			for(int i = 0; i < S.length(); i++){
				char c1 = S.charAt(i);
				char c2 = E.charAt(i);
				if(c1 == '1'){
					if(c2 == '1'){
						rightOnes.add(tax[i]);
					}else{
						toOff.add(tax[i]);
					}
				}else{
					//c1 = 0
					if(c2 == '1'){
						toOn.add(tax[i]);
					}else{
						
					}
				}
			}
			
			Collections.sort(toOn);
			Collections.sort(toOff);
			Collections.sort(rightOnes);
			
			long minCost = Long.MAX_VALUE;
			long oriCost = 0;
			
			for(int i = 0; i < N; i++){
				if(S.charAt(i) == '1'){
					oriCost += tax[i].cost;
				}
			}
			
			for(int k = 0; k <= rightOnes.size(); k++){
				long curCost = oriCost;
				long totalCost = 0;
				
				//all off
				PriorityQueue<Cost> toOffOrdered = new PriorityQueue<Cost>(1000, new Comparator<Cost>(){
					@Override
					public int compare(Cost o1, Cost o2) {
						return (int)(-o1.cost + o2.cost);
					}
					
				});
				
				for(Cost c : toOff){
					toOffOrdered.add(c);
				}
				
				for(int i = 0; i < k; i++){
					toOffOrdered.add(rightOnes.get(rightOnes.size()-i-1));
				}
				
				//all on
				PriorityQueue<Cost> toOnOrdered = new PriorityQueue<Cost>();
				for(Cost c : toOn){
					toOnOrdered.add(c);
				}
				
				for(int i = 0; i < k; i++){
					toOnOrdered.add(rightOnes.get(rightOnes.size()-i-1));
				}
				
				while(!toOffOrdered.isEmpty()){
					Cost c = toOffOrdered.remove();
					curCost -= c.cost;
					totalCost += curCost;
				}
				
				while(!toOnOrdered.isEmpty()){
					Cost c = toOnOrdered.remove();
					curCost += c.cost;
					totalCost += curCost;
				}
				
				if(minCost > totalCost){
					minCost = totalCost;
				}
			}
			out.println("Case " + t + ": " + minCost);
			t++;
		}
		
		out.close();
	}
	
	static class Cost implements Comparable<Cost>{
		public int index;
		public long cost;
		
		public Cost(int index, long cost){
			this.index = index;
			this.cost = cost;
		}

		@Override
		public int compareTo(Cost o) {
			return (int)(this.cost - o.cost);
		}
		
		@Override
		public String toString(){
			return index + ", cost = " + cost;
		}
	}
}
