package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class H {
	static int N;
	static int[] parentArm;
	static int[] parentWeight;
	static Node[] arms;
	static Node[] weights;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int t = 1;

		while(true){
			N = Integer.parseInt(reader.readLine());
			if(N == 0)break;
			parentArm = new int[N+1];
			parentWeight = new int[N+1];
			arms = new Node[N+1];
			weights = new Node[2*N+1];

			for(int i = 1; i <= N; i++){
				StringTokenizer tk = new StringTokenizer(reader.readLine());
				int dl = Integer.parseInt(tk.nextToken());
				int dr = Integer.parseInt(tk.nextToken());
				String t1 = tk.nextToken();
				String t2 = tk.nextToken();
				int id1 = Integer.parseInt(tk.nextToken());
				int id2 = Integer.parseInt(tk.nextToken());
				Node n = new Node(i);
				n.dl = dl;
				n.dr = dr;
				n.left = new Node(id1);
				n.right = new Node(id2);
				if(t1.equals("W")){
					weights[id1] = n.left;
					n.leftWeight = true;
				}

				if(t2.equals("W")){
					weights[id2] = n.right;
					n.rightWeight = true;
				}

				arms[i] = n;
			}

			for(int i = 1; i <= N; i++){
				Node n = arms[i];
				if(n.leftWeight){
					n.left = weights[n.left.id];
				}else{
					n.left = arms[n.left.id];
				}      
				if(n.rightWeight){
					n.right = weights[n.right.id];
				}else{
					n.right = arms[n.right.id];
				}
				n.left.parent = n;
				n.right.parent = n;
			}

			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int resId = Integer.parseInt(tk.nextToken());
			int resMinW = Integer.parseInt(tk.nextToken());

			weights[resId].num = 1;
			weights[resId].den = 1;
			spread(weights[resId]);
			out.println("Case " + t + ": " + total(resId, resMinW));
			t++;
		}

		out.close();
	}

	static void spread(Node n){
		if(n.set) return;
		n.set = true;
		//par
		if(n.parent != null){
			if(n.parent.left == n){
				n.parent.right.num = n.num * n.parent.dl;
				n.parent.right.den = n.den * n.parent.dr;
				n.parent.right.normalize();
				
				spread(n.parent.right);

				n.parent.num = n.num * n.parent.right.den + n.den * n.parent.right.num;
				n.parent.den = n.den * n.parent.right.den;
				n.parent.normalize();
				spread(n.parent);
			}
			else if(n.parent.right == n){
				n.parent.left.num = n.num * n.parent.dr;
				n.parent.left.den = n.den * n.parent.dl;
				n.parent.left.normalize();
				
				spread(n.parent.left);

				n.parent.num = n.num * n.parent.left.den + n.den * n.parent.left.num;
				n.parent.den = n.den * n.parent.left.den;
				n.parent.normalize();
				spread(n.parent);
			}
		}

		if(n.left != null){
			n.right.num = n.num * n.dl;
			n.right.den = n.den * (n.dl + n.dr);
			n.right.normalize();
			
			spread(n.right);
			
			n.left.num = n.num * n.dr;
			n.left.den = n.den * (n.dl + n.dr);
			n.left.normalize();
			
			spread(n.left);
		}
	}

	static int total(int resId, int resVal){
		double sum = 0;
		//TODO: LCM
		long lcm = lcm();
		for(int i = 1; i<= 2*N; i++){
			if(weights[i] == null)break;
			weights[i].num = weights[i].num * (lcm / weights[i].den);
			weights[i].den = 1;
			sum += weights[i].num;
		}
		if(resVal > weights[resId].num){
			long mul = resVal/weights[resId].num; 
			if(mul * weights[resId].num < resVal){
				mul++;
			}
			sum *= mul;
		}
		return (int)sum;
	}
	
	static long lcm(){
		long cur = 1;
		for(int i = 1; i<= 2*N; i++){
			if(weights[i] == null)break;
			cur = lcm(cur, weights[i].den);
		}
		return cur;
	}
	
	public static long lcm(long a, long b){
		return (a * b) / gcd(a,b);
	}
	

	static long gcd(long a, long b){
		while(b > 0){
			long c = a % b;
			a = b;
			b = c;
		}
		return a;
	}
	
	static class Node{
		public boolean set;
		public boolean leftWeight;
		public boolean rightWeight;
		public int id;
		public int dl;
		public int dr;
		public long num;
		public long den;
		public Node left;
		public Node right;
		public Node parent;

		public Node(int id){
			this.id = id;
		}

		@Override
		public String toString(){
			return id + (isArm() ? "A" : "W") + ", left " + dl + " = [" + left + "], right "
					+ dr + " = [" + right + "]";
		}

		public boolean isArm(){
			return left != null;
		}
		
		public void normalize(){
			long div = gcd(num, den);
			num /= div;
			den /= div;
		}
	}
}