package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class D {
	static long MAX = 1000000000;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		List<Integer> a = new ArrayList<Integer>();
		List<Integer> b = new ArrayList<Integer>();

		for(long i = 1;; i++){
			long av = i * (i+1)/2;
			long bv = i*i-1;
			if(av <= MAX){
				a.add((int)av);
			}else{
				break;
			}
			if(bv <= MAX){
				b.add((int)bv);
			}
		}
		
		List<Integer> inter = new ArrayList<Integer>();
		inter.add(0);
		int ind1 = 0;
		int ind2 = 0;
		while(true){
			if(ind1 >= a.size() || ind2 >= b.size()){
				break;
			}

			if(a.get(ind1) < b.get(ind2)){
				ind1++;
			}else if(a.get(ind1) > b.get(ind2)){
				ind2++;
			}else{
				inter.add(a.get(ind1));
				ind1++;
				ind2++;
			}
		}

		int t = 1;
		
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int lo = Integer.parseInt(tk.nextToken());
			int hi = Integer.parseInt(tk.nextToken());
			if(lo == 0 && hi == 0)break;
			hi-=2;
			int i1 = Collections.binarySearch(inter, lo);
			int i2 = Collections.binarySearch(inter, hi);

			if(i1 < 0){
				i1 = -1-i1;
			}

			if(i2 < 0){
				i2 = -1-i2-1;
			}

			out.println("Case " + t + ": " + (i2-i1+1));
			t++;
		}


		out.close();
	}
}