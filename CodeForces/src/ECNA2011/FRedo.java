package ECNA2011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class FRedo {
	static int N;
	static int[][] adjMap;
	static int[][] costMap;
	static int[] path;
	//dp[i][j] = min force so that path[i,i+1,...,j] is taken by gps to go from path[i] -> path[j]
	static int[][] dp;
	static int M;
	
	static int MAX_N = 100;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		adjMap = new int[MAX_N][MAX_N];
		costMap = new int[MAX_N][MAX_N];
		dp = new int[MAX_N][MAX_N];
		path = new int[MAX_N];
		int t = 1;
		while(true){
			N = Integer.parseInt(reader.readLine());
			if(N == 0) break;
			for(int i = 0; i < N; i++){
				StringTokenizer tk = new StringTokenizer(reader.readLine());
				for(int j = 0; j < N; j++){
					adjMap[i][j] = Integer.parseInt(tk.nextToken());
				}
			}
			
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			M = Integer.parseInt(tk.nextToken());
			
			for(int i = 0; i < M; i++){
				path[i] = Integer.parseInt(tk.nextToken());
			}
			
			floydWarshall();	
			//k = path length	
			//i = start
			for(int k = 1; k < M; k++){
				for(int i = 0; i+k < M; i++){
					int j = i+k;
					dp[i][j] = N;
					//either there is no need to force
					int costWithoutForce = 0;
					for(int l = i+1; l <= j; l++){
						if(adjMap[path[l-1]][path[l]] == 0){
							costWithoutForce = Integer.MAX_VALUE;
							break;
						}
						costWithoutForce += adjMap[path[l-1]][path[l]];
					}
					
					if(costWithoutForce == costMap[path[i]][path[j]]){
						dp[i][j] = 0;
						continue;
					}
					
					//or exist force l -> l+1
					for(int l = i; l < j; l++){
						dp[i][j] = Math.min(dp[i][j], dp[i][l] + 1 + dp[l+1][j]);
					}
				}
			}
			
			out.println("Case " + t + ": " + dp[0][M-1]);
			t++;
		}
		
		out.close();
	}
	
	static void p(int[][] m){
		for(int i = 0; i < m.length; i++){
			for(int j = 0; j < m[0].length; j++){
				System.out.print(m[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	static void floydWarshall(){
		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++){
				costMap[i][j] = Integer.MAX_VALUE;
			}
		}
		
		for(int i = 0; i < N; i++){
			costMap[i][i] = 0;
		}
		
		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++){
				if(adjMap[i][j] != 0){
					costMap[i][j] = adjMap[i][j];
				}
			}
		}
		
		for(int k = 0; k < N; k++){
			for(int i = 0; i < N; i++){
				for(int j = 0; j < N; j++){
					if(costMap[i][k] != Integer.MAX_VALUE && costMap[k][j] != Integer.MAX_VALUE){
						if(costMap[i][k] + costMap[k][j] < costMap[i][j]){
							costMap[i][j] = costMap[i][k] + costMap[k][j];
						}
					}
				}
			}
		}
	}
}
