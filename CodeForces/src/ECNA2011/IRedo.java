package ECNA2011;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class IRedo {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int x1 = Integer.parseInt(tk.nextToken());
			int y1 = Integer.parseInt(tk.nextToken());
			int x2 = Integer.parseInt(tk.nextToken());
			int y2 = Integer.parseInt(tk.nextToken());
			if(x1 == 0 && y1 == 0 && x2 == 0 && y2 == 0)break;
			tk = new StringTokenizer(reader.readLine());
			int wx1 = Integer.parseInt(tk.nextToken());
			int wy1 = Integer.parseInt(tk.nextToken());
			int wx2 = Integer.parseInt(tk.nextToken());
			int wy2 = Integer.parseInt(tk.nextToken());
			
			
			double d1 = Math.min(distance(x1,y1,wx1,wy1) + distance(x2,y2, wx1,wy1), 
					distance(x1,y1,wx2,wy2) + distance(x2,y2, wx2,wy2));
			double d2 = distance(x1,y1,x2,y2);
			
			if(isSegmentIntersect(x1,y1,x2,y2,wx1,wy1,wx2,wy2)){
				out.printf("Case " + t + ": %.3f\n", d1/2.0);
			}else{
				out.printf("Case " + t + ": %.3f\n", d2/2.0);
			}
			
			t++;
		}
		out.close();
	}
	
	//0 if on line, -1 or 1 if on some side
	public static int getSideOfLine(Line2D divide, Point2D p1){
		double[] vDiv = new double[]{divide.getX2()-divide.getX1(),
				divide.getY2()-divide.getY1(),0};
		double[] v1 = new double[]{p1.getX()-divide.getX1(),
				p1.getY()-divide.getY1(),0};
		
		double c = vDiv[0] * v1[1] - vDiv[1] * v1[0];
		if(Math.abs(c) < 0.00001){
			return 0;
		}else if(c>0){
			return 1;
		}else{
			return -1;
		}
	}
	
	static boolean isSegmentIntersect(double x1, double y1, double x2, double y2, 
			double wx1, double wy1, double wx2, double wy2){
		Point2D p1 = new Point2D.Double(x1,y1);
		Point2D p2 = new Point2D.Double(x2,y2);
		Point2D wp1 = new Point2D.Double(wx1, wy1);
		Point2D wp2 = new Point2D.Double(wx2, wy2);
		Line2D l = new Line2D.Double(p1, p2);
		Line2D wl = new Line2D.Double(wp1, wp2);
		
		return getSideOfLine(l, wp1) != getSideOfLine(l, wp2) &&
				getSideOfLine(wl, p1) != getSideOfLine(wl, p2);
	}
	
	static double distance(int x1, int y1, int x2, int y2){
		return Math.sqrt(Math.pow(y2-y1,2) + Math.pow(x2-x1,2));
	}
}
