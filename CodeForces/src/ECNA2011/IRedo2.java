package ECNA2011;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class IRedo2 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int x1 = Integer.parseInt(tk.nextToken());
			int y1 = Integer.parseInt(tk.nextToken());
			int x2 = Integer.parseInt(tk.nextToken());
			int y2 = Integer.parseInt(tk.nextToken());
			if(x1 == 0 && y1 == 0 && x2 == 0 && y2 == 0)break;
			tk = new StringTokenizer(reader.readLine());
			int wx1 = Integer.parseInt(tk.nextToken());
			int wy1 = Integer.parseInt(tk.nextToken());
			int wx2 = Integer.parseInt(tk.nextToken());
			int wy2 = Integer.parseInt(tk.nextToken());
			
			
			double d1 = Math.min(distance(x1,y1,wx1,wy1) + distance(x2,y2, wx1,wy1), 
					distance(x1,y1,wx2,wy2) + distance(x2,y2, wx2,wy2));
			double d2 = distance(x1,y1,x2,y2);
			
			if(isSegmentIntersect(x1,y1,x2,y2,wx1,wy1,wx2,wy2)){
				out.printf("Case " + t + ": %.3f\n", d1/2.0);
			}else{
				out.printf("Case " + t + ": %.3f\n", d2/2.0);
			}
			
			t++;
		}
		out.close();
	}
	
	static boolean isSegmentIntersect(double x1, double y1, double x2, double y2, 
			double wx1, double wy1, double wx2, double wy2){
		return	new Line2D.Double(wx1, wy1, wx2, wy2).intersectsLine(
				new Line2D.Double(x1,y1,x2,y2));
	}
	static double distance(int x1, int y1, int x2, int y2){
		return Math.sqrt(Math.pow(y2-y1,2) + Math.pow(x2-x1,2));
	}
}
