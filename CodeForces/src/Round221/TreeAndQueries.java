package Round221;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class TreeAndQueries {
	static List<Integer>[] edges;
	static int N;
	static int[] nodeToDfs;
	static int[] dfsToNode;
	static int[] color;
	// dfs ids of the leftmost child and rightmost child of this dfs node
	// note that these form intervals [x,y], and so we can use Mo
	static int[] dfsToDfsLeft;
	static int[] dfsToDfsRight;
	static int dfsid = -1;
	
	// number of nodes with color i
	static int[] freq;
	// ff[i] = number of colors whose freq >= i
	static int[] ff;
	
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		tk = tk(in.readLine());
		
		N = Integer.parseInt(tk.nextToken());
		int Q = Integer.parseInt(tk.nextToken());
		
		nodeToDfs = new int[N];
		dfsToNode = new int[N];
		color = new int[N];
		edges = new List[N];
		dfsToDfsLeft = new int[N];
		dfsToDfsRight = new int[N];
		ff = new int[N+1];
		for(int i=0;i<N;i++) {
			edges[i] = new ArrayList();
		}
		
		tk = tk(in.readLine());
		for(int i=0; i<N; i++) {
			color[i] = Integer.parseInt(tk.nextToken());
		}
		
		for(int i=0; i<N-1; i++) {
			tk = tk (in.readLine());
			int a = Integer.parseInt(tk.nextToken())-1;
			int b = Integer.parseInt(tk.nextToken())-1;
			edges[a].add(b);
			edges[b].add(a);
		}
		
		dfs(0, -1);
		int[] ans = new int[Q];
		Query[] queries = new Query[Q];
		for(int i=0; i<Q; i++) {
			tk = tk(in.readLine());
			int node = Integer.parseInt(tk.nextToken())-1;
			int k = Integer.parseInt(tk.nextToken());
			queries[i] = new Query(dfsToDfsLeft[nodeToDfs[node]], dfsToDfsRight[nodeToDfs[node]], k, i);
		}
		
		final int BUCKET = (int) Math.sqrt(dfsid);
		Arrays.sort(queries, new Comparator<Query>(){
			@Override
			public int compare(Query o1, Query o2) {
				int dif = o1.l / BUCKET -  o2.l / BUCKET;
				if(dif != 0) return dif;
				return o1.r - o2.r;
			}
		});
		
		int l = 0;
		int r = 0;
		int MAX = 100005;
		
		freq = new int[MAX];
		
		freq[color[dfsToNode[l]]]++;
		ff[1] = 1;
		for(Query q : queries) {
			while(l > q.l) {
				l--;
				add(l);
			}
			
			while(r < q.r) {
				r++;
				add(r);
			}
			
			while(l < q.l) {
				remove(l);
				l++;
			}
			
			while(r > q.r) {
				remove(r);
				r--;
			}
			if(q.k > N) {
				ans[q.id] = 0;
			} else 
				ans[q.id] = ff[q.k];
		}
		
		for(int qid=0; qid < Q; qid++) {
			out.println(ans[qid]);
		}
		
		out.close();
	}
	
	static void add(int dfsid) {
		int c = color[dfsToNode[dfsid]];
		// < freq[c] stays the same, > freq[c]+1 stays the same too
		ff[freq[c]+1]++;
		freq[c]++;
	}
	
	static void remove(int dfsid){ 
		int c = color[dfsToNode[dfsid]];
		// > freq[c] stays the same, < freq[c] stays the same
		ff[freq[c]]--;
		freq[c]--;
	}
	
	static void dfs(int cur, int par) {
		dfsid++;
		nodeToDfs[cur] = dfsid;
		dfsToNode[dfsid] = cur;
		
		dfsToDfsLeft[dfsid] = dfsid;
		dfsToDfsRight[dfsid] = dfsid;
		for(int child : edges[cur]) {
			if(child == par) continue;
			dfs(child, cur);
			dfsToDfsRight[nodeToDfs[cur]] = dfsToDfsRight[nodeToDfs[child]];
		}
	}
	
	static class Query {
		public int l;
		public int r;
		public int k;
		public int id;
		
		public Query(int l, int r, int k, int id) {
			this.l = l;
			this.r = r;
			this.k = k;
			this.id = id;
		}
	}
	
	static StringTokenizer tk(String s) {return new StringTokenizer(s);}
}
