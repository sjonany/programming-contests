package Round173div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PaintingEggs{
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int aSum = 0;
		for(int i=0;i<n;i++){
			aSum += sc.nextInt();
			sc.nextInt();
		}
		
		int sizeA2Min = (int)Math.ceil((aSum-500.0)/1000.0);
		int sizeA2Max = (int)Math.floor((aSum+500.0)/1000.0);
		if(sizeA2Min <= sizeA2Max && sizeA2Min >=0 && sizeA2Max <= n){
			for(int i=1;i<=sizeA2Min;i++){
				System.out.print("G");
			}
			for(int i=1;i<=n-sizeA2Min;i++){
				System.out.print("A");
			}
			return;
		}
		
		System.out.println(-1);
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
