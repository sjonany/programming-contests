package Round173div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

//fail O(n^2) TLE
public class SausageMaximization{
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		long[] a = new long[n];
		for(int i=0; i<n; i++){
			a[i] = sc.nextLong();
		}
		
		long[] delLeft = new long[n];
		delLeft[0] = a[0];
		long curXor = a[0];
		for(int i=1; i<n;i++){
			curXor ^= a[i];
			delLeft[i] = curXor;
		}
		
		//System.out.println("delLeft = " + Arrays.toString(delLeft));
		long[] delRight = new long[n];
		delRight[n-1] = a[n-1];
		curXor = a[n-1];
		for(int i=n-2; i>=0;i--){
			curXor ^= a[i];
			delRight[i] = curXor;
		}
		//System.out.println("delRight = " + Arrays.toString(delRight));
		//idk... im pretty sure this o(n^2) wouldnt work :(
		//the entire thing
		long maxXor = delLeft[n-1];
		//if left is empty
		for(int right=1; right<n; right++){
			maxXor = Math.max(delRight[right], maxXor);
		}
		//if right is empty
		for(int left=0; left<n-1; left++){
			maxXor = Math.max(delLeft[left], maxXor);
		}
		for(int left=0; left<n; left++){
			for(int right=left+2;right<n;right++){
				maxXor = Math.max(delLeft[left] ^ delRight[right], maxXor);
			}
		}
		System.out.println(maxXor);
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
