package Round173div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class SausageMaxRedo {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		long[] a = new long[n];
		for(int i=0; i<n; i++){
			a[i] = sc.nextLong();
		}
		
		//i is one-based. if 0, 0.
		long[] delLeft = new long[n+1];
		delLeft[0] = 0;
		delLeft[1] = a[0];
		long curXor = a[0];
		for(int i=1; i<n;i++){
			curXor ^= a[i];
			delLeft[i+1] = curXor;
		}
		
		Long[] delRight = new Long[n+1];
		delRight[0] = (long) 0;
		delRight[1] = a[n-1];
		curXor = a[n-1];
		for(int i=1; i<n;i++){
			curXor ^= a[n-i-1];
			delRight[i+1] = curXor;
		}

		Arrays.sort(delRight);
		
		//for each left, find right that max {leftVal ^ rightVal}
		//it's ok if right overlaps with left, because xor will cancel them out, and we
		//are taking the max including cases where left does not overlap with right
		long max = 0;
		for(int left=0;left<=n;left++){
			//2^40 >= (2^10)^4 > 1000^4 = 10^12 
			long rightVal = 0;
			//inv: arr[lo] <= rightVal <= arr[hi] and arr[lo] matches arr[hi] for the left 40..40-i digits
			int lo = 0;
			int hi = n;
			//fill bits for rightVal one by one starting from highest bit
			for(long i=40; i>=0; i--){
				long sep = rightVal + (((long)1)<<i);
				int ind = binSearch(delRight, lo, hi, sep);
				if(ind == -1){
					//everybody has bit = 1, so no choice but to make this bit =1 too
					rightVal = sep;
				}else if(ind == hi){
					//everybody has bit = 0, so we have to choose 0 and leave rightVal untouched
				}else{
					//some have 0, some have 1
					//ind+1 points to the first guy who has 1
					int leftBit = (int) ((delLeft[left] >> i) & 1);
					if(leftBit == 0){
						//to max xor, gotta add 1 to this bit
						rightVal = sep;
						lo = ind+1;
					}else{
						//leftBit =1, make this bit = 0
						hi = ind;
					}
				}
			}
			max = Math.max(max, delLeft[left] ^ rightVal);
		}
		
		System.out.println(max);
	}
	
	//@return highest index where arr[lo]<s
	public static int binSearch(Long[] arr, int lo, int hi, long s){
		if(arr[lo]>=s){
			lo = -1;
		}else{
			while(lo < hi){
				int mid = lo + (hi-lo+1)/2;
				if(arr[mid] >= s){
					hi = mid-1;
				}else{
					lo = mid;
				}
			}
		}
		return lo;
	}
	

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
