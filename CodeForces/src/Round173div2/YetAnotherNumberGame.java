package Round173div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class YetAnotherNumberGame{
	public static void main(String[] args){
		String FIRST = "BitLGM";
		String SECOND = "BitAryo";
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int[] a = new int[3];
		for(int i=0;i<n;i++){
			a[i] = sc.nextInt();
		}
		
		if(n == 1){
			if(a[0] == 0){
				System.out.println(SECOND);
			}else{
				System.out.println(FIRST);
			}
		}else if(n == 2){
			//dp[i][j] = true iff current player to move wins
			boolean[][] dp = new boolean[a[0]+1][a[1]+1];
			dp[0][0] = false;
			for(int i=0;i<= a[0];i++){
				for(int j=0;j<= a[1]; j++){
					if(i==j && i==0) continue;
					for(int k=1;k<=i;k++){
						dp[i][j] |= !dp[i-k][j];
					}
					for(int k=1;k<=j;k++){
						dp[i][j] |= !dp[i][j-k];
					}
					for(int k=1;k<=Math.min(i, j);k++){
						dp[i][j] |= !dp[i-k][j-k];
					}
				}
			}
			if(dp[a[0]][a[1]]){
				System.out.println(FIRST);
			}else{
				System.out.println(SECOND);
			}
		}else{	
			if((a[0]^a[1]^a[2]) == 0){
				System.out.println(SECOND);
			}else{
				System.out.println(FIRST);
			}
		}
		
	}
	
	//proof that there is no move to change nim-ness
			/**
	induction on bit length (actually need this to be [a,b,c] to use the IH :/ )
	base case n=1-> all moves same as nim except when 1 1 1 -> player 1 wins, still same
	ih: no change to nim-ness if length k-1
	is: no change to nimness if length k
	if exist a 0 in a,b,c -> is nim already, because can't use special move, easy
	if not, then without loss of gen, let c be min a,b,c,
	we have to show that using the special move won't change nim-ness
	if a^b^c != 0-> take nim move, ih, win
	so only have to prove that if a^b^c == 0 -> (a-c) xor (b-c) != 0
	since c is not zero (see above 5 line), it has to contain a bit =1.
	Pick such a bit that is smallest for c. since a^b^c =0 -> let's say a contains bit =1, b contains b=0 
	at the same location.
	Then b-c -> 1 at that location (no carry from left, since no deduction from c side cos we take smallest 1 from c)
	a-c = 0 at that location. thus non-zero.
			 */
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
