package Round209div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class Neatness {
	static int N;
	static int sr;
	static int sc;

	static int[][] edges;
	static int[] edgeSize;
	static boolean[][] board;
	
	static int R = 0;
	static int C = 1;
	static int NUM_INIT_ON = 0;
	static PrintWriter out;
	
	// indices of ons
	static TreeSet<Integer>[] rowOns;
	static TreeSet<Integer>[] colOns;
	
	static int UNTOUCHED = 0;
	static int EXPLORING = 1;
	static int EXPLORED = 2;
	static int[][] visit;
	static StringBuilder sb = new StringBuilder();
	
	// how many on-lights are reached in turn on phase
	static int countReached = 0;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		StringTokenizer tk = new StringTokenizer(in.readLine());
		N = Integer.parseInt(tk.nextToken());
		sr = Integer.parseInt(tk.nextToken()) - 1;
		sc = Integer.parseInt(tk.nextToken()) - 1;
		
		board = new boolean[N][N];
		edgeSize = new int[N*N];
		edges = new int[N*N][4];
		rowOns = new TreeSet[N];
		colOns = new TreeSet[N];
		visit = new int[N][N];
		for(int i=0;i<N;i++){
			rowOns[i] = new TreeSet();
			colOns[i] = new TreeSet();
		}
		
		for(int r=0;r<N;r++) {
			tk = new StringTokenizer(in.readLine());
			for(int c=0;c<N;c++) {
				board[r][c] = Integer.parseInt(tk.nextToken()) == 1;
				if(board[r][c]){
					NUM_INIT_ON++;
					rowOns[r].add(c);
					colOns[c].add(r);
				}
			}	
		}
		
		turnOn(sr,sc);
		if(countReached != NUM_INIT_ON) {
			System.out.println("NO");
			return;
		}
		
		out.println("YES");
		out.print(sb);
		sb = new StringBuilder();
		turnOff(sr,sc);
		
		out.println(sb);
		out.close();
	}
	
	static int VISITED = 4;
	static void turnOff(int r, int c){
		visit[r][c] = VISITED;
		for(int i = 0; i < edgeSize[id(r,c)]; i++){
			int vout = edges[id(r,c)][i];
			int nc = vout % N;
			int nr = vout / N;
			int dr = nr - r;
			int dc = nc - c;
			int dirId = -1;
			if(dr == 0){
				if(dc>0){
					dirId = 0;
				}else{
					dirId = 3;
				}
			}else{
				if(dr > 0){
					dirId = 1;
				}else{
					dirId = 2;
				}
			}
			
			if(visit[nr][nc] != VISITED){
				sb.append(DIR_NAMES[dirId]);
				turnOff(nr,nc);
				sb.append(DIR_REV_NAMES[dirId]);
			}
		}
		sb.append(OFF);
	}
	
	
	static char ON = '1';
	static char OFF = '2';
	static int[][] DIRS = {{0,1},{1,0},{-1,0},{0,-1}};
	static char[] DIR_NAMES = {'R','D','U','L'};
	static char[] DIR_REV_NAMES = {'L','U','D','R'};
	// pre: unexplored, post = explored + return back here
	static void turnOn(int r, int c) {
		visit[r][c] = EXPLORING;
		if(board[r][c]) {
			countReached++;
		} else {
			sb.append(ON);
			board[r][c] = true;
			rowOns[r].add(c);
			colOns[c].add(r);
		}
		
		for(int dirId = 0; dirId < 4; dirId++) {
			int[] dir = DIRS[dirId];
			int nr = r + dir[R];
			int nc = c + dir[C];
			if(nr >= 0 && nr < N && nc >= 0 && nc < N && visit[nr][nc] == UNTOUCHED){
				boolean seeLight = false;
				if(dir[R] != 0){
					if(dir[R] < 0){
						seeLight = colOns[c].floor(nr) != null;
					}else{
						seeLight = colOns[c].ceiling(nr) != null;
					}
				}else{
					if(dir[C] < 0){
						seeLight = rowOns[r].floor(nc) != null;
					}else{
						seeLight = rowOns[r].ceiling(nc) != null;
					}
				}
				if(seeLight) {
					sb.append(DIR_NAMES[dirId]);
					int prevNode = id(r,c);
					int newNode = id(nr,nc);
					edges[prevNode][edgeSize[prevNode]++] = newNode; 
					edges[newNode][edgeSize[newNode]++] = prevNode; 
					turnOn(nr,nc);
					sb.append(DIR_REV_NAMES[dirId]);
				}
			}
		}
		
		visit[r][c] = EXPLORED;
	}
	
	static int id(int r, int c){
		return r*N + c;
	}
}
