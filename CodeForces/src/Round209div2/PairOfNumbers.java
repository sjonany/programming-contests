package Round209div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class PairOfNumbers {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int N = Integer.parseInt(in.readLine());
		StringTokenizer tk = new StringTokenizer(in.readLine());
		int[] arr = new int[N];
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}

		// dp[i] = j -> j = left most such that j->i are div by i
		int[] dpLeft = new int[N];
		dpLeft[0] = 0;
		for (int i = 1; i < N; i++) {
			dpLeft[i] = i;
			while (dpLeft[i] - 1 >= 0 && arr[dpLeft[i] - 1] % arr[i] == 0)
				dpLeft[i] = dpLeft[dpLeft[i] - 1];
		}

		int[] dpRight = new int[N];
		dpRight[N - 1] = N - 1;
		for (int i = N - 2; i >= 0; i--) {
			dpRight[i] = i;
			while (dpRight[i] + 1 < N && arr[dpRight[i] + 1] % arr[i] == 0)
				dpRight[i] = dpRight[dpRight[i] + 1];
		}
		
		int max = 0;
		for (int i = 0; i < N; i++) {
			int val = dpRight[i] - dpLeft[i];
			if (val > max)
				max = val;
		}

		Set<Integer> pairs = new TreeSet<Integer>();
		for (int i = 0; i < N; i++) {
			int val = dpRight[i] - dpLeft[i];
			if(max == val){
				pairs.add(dpLeft[i]+1);
			}
		}

		out.println(pairs.size() + " " + max);
		for(int i : pairs){
			out.print(i + " ");
		}
		out.close();
	}
}
