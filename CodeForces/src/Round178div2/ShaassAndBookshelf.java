package Round178div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class ShaassAndBookshelf {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n= sc.nextInt();
		List<Integer> ones = new ArrayList<Integer>();
		List<Integer> twos = new ArrayList<Integer>();
		
		for(int i=0;i<n;i++){
			int t = sc.nextInt();
			int w = sc.nextInt();
			
			if(t==1){
				ones.add(w);
			}else{
				twos.add(w);
			}
		}
		
		Collections.sort(ones);
		Collections.sort(twos);

		int[] sumOnes = new int[ones.size()+1];
		sumOnes[0] = 0;
		for(int i=1;i<=ones.size();i++){
			sumOnes[i] = sumOnes[i-1] + ones.get(i-1);
		}
		
		int[] sumTwos = new int[twos.size()+1];
		sumTwos[0] = 0;
		for(int i=1;i<=twos.size();i++){
			sumTwos[i] = sumTwos[i-1] + twos.get(i-1);
		}
		
		int totalThickness = ones.size() + twos.size()*2;
		int minThickness = Integer.MAX_VALUE;
		for(int c1 = 0; c1<=ones.size(); c1++){
			for(int c2=0; c2<=twos.size(); c2++){
				//if c1 1's and c2 2's are picked as outsider 
				int width = sumOnes[c1] + sumTwos[c2];
				int thickness = totalThickness - c1 - c2*2;
				if(thickness<minThickness && width <= thickness){
					minThickness = thickness;
				}
			}
		}
		System.out.println(minThickness);
	}
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
