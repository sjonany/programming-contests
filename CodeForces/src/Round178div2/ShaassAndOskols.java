package Round178div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class ShaassAndOskols {
	public static void main(String[] args){
		FastScanner sc= new FastScanner();
		int n = sc.nextInt();
		int[] wires = new int[n+1];
		
		for(int i=1;i<=n;i++){
			wires[i]=sc.nextInt();
		}
		
		int m = sc.nextInt();
		for(int i=1;i<=m;i++){
			int wireI = sc.nextInt();
			int leftI = sc.nextInt();
			int left = leftI-1;
			int right = wires[wireI] - left - 1;
			if(wireI+1 <=n){
				wires[wireI+1] += right;
			}	
			if(wireI-1 >=1){
				wires[wireI-1] += left;
			}
			wires[wireI] = 0;
		}
		
		for(int i=1;i<=n;i++){
			System.out.println(wires[i]);
		}
	}

public static class FastScanner {
	BufferedReader br;
	StringTokenizer st;

	public FastScanner(String s) {
		try {
			br = new BufferedReader(new FileReader(s));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public FastScanner() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}

	String nextToken() {
		while (st == null || !st.hasMoreElements()) {
			try {
				st = new StringTokenizer(br.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return st.nextToken();
	}

	int nextInt() {
		return Integer.parseInt(nextToken());
	}

	long nextLong() {
		return Long.parseLong(nextToken());
	}

	double nextDouble() {
		return Double.parseDouble(nextToken());
	}
}
}

