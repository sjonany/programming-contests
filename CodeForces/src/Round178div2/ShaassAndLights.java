package Round178div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class ShaassAndLights {
	static long MOD = 1000000007;
	static long[] fact = new long[1001];
	static long[] exp = new long[1001];
	static long[][] comb = new long[1001][1001];
	
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int m = sc.nextInt();
		
		comb[1][1] = 1;
		for(int i=2;i<comb.length;i++){
			for(int j=1;j<=i;j++){
				if(j==1){
					comb[i][j] = i;
				}else{
					comb[i][j] = (comb[i-1][j] + comb[i-1][j-1]) % MOD;
				}
			}
		}
		
		fact[0] = 1;
		for(int i=1;i<=n;i++){
			fact[i] = (fact[i-1]*i) % MOD;
		}
		
		exp[0] = 1;
		for(int i=1;i<=n;i++){
			exp[i] = (exp[i-1]*2) %MOD;
		}
	
		
		int[] ons = new int[m];
		for(int i=0;i<m;i++){
			ons[i] = sc.nextInt();
		}
		
		Arrays.sort(ons);

		//if no lights at the end, only one way to extend each time
		boolean isLeftClumpFixed = ons[0] != 1;
		boolean isRightClumpFixed = ons[ons.length-1] != n;
		
		List<Integer> clumps = new ArrayList<Integer>();
		int totalClumpSize = n-m;
		for(int i=-1;i<ons.length;i++){
			int countBetween; 
			if(i == -1){
				countBetween = ons[i+1]-1;
			}
			else if(i == ons.length-1){
				countBetween = n+1 - ons[i] -1;
			}else{
				countBetween = ons[i+1]-ons[i]-1;
			}
			if(countBetween > 0){
				clumps.add(countBetween);
			}
		}
		
		//ways to organize movement orders
		//for each movement order, -> group k, i times. depends on whether edge clump or not. if not edge clump,
		//2^(k-1) times, else just 1
		
		long movOrderCount = multnom(totalClumpSize, clumps);
		for(int i=0;i<clumps.size();i++){
			if(i==0 && isLeftClumpFixed)continue;
			if(i==clumps.size()-1 && isRightClumpFixed)continue;
			movOrderCount = (movOrderCount * exp[clumps.get(i)-1]) % MOD;
		}
		
		System.out.println(movOrderCount);
	}

	//multinomial high! / small1!/small2!etc.
	public static long multnom(int high, List<Integer> smalls){
		long res = 1;
		int curHigh = high;
		for(int i=0;i<smalls.size();i++){
			res  = (res * comb[curHigh][smalls.get(i)]) % MOD;
			curHigh -= smalls.get(i);
		}
		return res;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
