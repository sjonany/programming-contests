package Round170div1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class Game {
	static int N;
	static int M;
	static int K;
	static Map<Integer,List<Segment>> badRowSegments; 
	static Map<Integer,List<Segment>> badColSegments; 
	static TreeSet<Integer> badRows;
	static TreeSet<Integer> badCols;

	static Map<Integer,List<Segment>> remRowSegments; 
	static Map<Integer,List<Segment>> remColSegments; 

	static Map<Integer,Integer> remRowSegmentsSize; 
	static Map<Integer,Integer> remColSegmentsSize; 
	
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		M = sc.nextInt();
		N = sc.nextInt();
		K = sc.nextInt();
		remRowSegments = new TreeMap<Integer,List<Segment>>();
		remColSegments = new TreeMap<Integer,List<Segment>>();
		badRowSegments = new TreeMap<Integer,List<Segment>>();
		badColSegments = new TreeMap<Integer,List<Segment>>();
		remRowSegmentsSize = new TreeMap<Integer,Integer>();
		remColSegmentsSize = new TreeMap<Integer,Integer>();
		badRows = new TreeSet<Integer>();
		badCols = new TreeSet<Integer>();
		
		for(int i=0;i<K;i++){
			int xbi = sc.nextInt();
			int ybi = sc.nextInt();
			int xei = sc.nextInt();
			int yei = sc.nextInt();
			
			if(xbi == xei){
				List<Segment> cutSegments = badColSegments.get(xbi);
				if(cutSegments == null){
					cutSegments = new ArrayList<Segment>();
					badColSegments.put(xbi, cutSegments);
					badCols.add(xbi);
				}
				cutSegments.add(new Segment(ybi,yei));
			}else{
				List<Segment> cutSegments = badRowSegments.get(ybi);
				if(cutSegments == null){
					cutSegments = new ArrayList<Segment>();
					badRowSegments.put(ybi, cutSegments);
					badRows.add(ybi);
				}
				cutSegments.add(new Segment(xbi,xei));
			}
		}
		
		int nimSum = 0;
		for(int r : badRows){
			List<Segment> goods = getUntouchedSegments(badRowSegments.get(r), 0, M);
			int totalGood = 0;
			for(Segment good : goods){
				totalGood += good.getLength();
			}
			remRowSegmentsSize.put(r, totalGood);
			remRowSegments.put(r, goods);
			nimSum ^= totalGood;
		}
		
		for(int c : badCols){
			List<Segment> goods = getUntouchedSegments(badColSegments.get(c), 0, N);
			int totalGood = 0;
			for(Segment good : goods){
				totalGood += good.getLength();
			}
			remColSegmentsSize.put(c, totalGood);
			remColSegments.put(c, goods);
			nimSum ^= totalGood;
		}
		
		if((N+1 -2 -badRows.size()) %2 == 1){
			nimSum ^= M;
		}
		
		if((M+1 -2 -badCols.size()) %2 == 1){
			nimSum ^= N;
		}
		
		if(nimSum == 0){
			System.out.println("SECOND");
		}else{
			if(badRows.size() < N+1 -2){
				int goodR = 1;
				for(;goodR < N; goodR++){
					if(!badRows.contains(goodR)){
						break;
					}
				}
				List<Segment> s = new ArrayList<Segment>();
				s.add(new Segment(0,M));
				remRowSegments.put(goodR, s);
				remRowSegmentsSize.put(goodR, s.get(0).getLength());
			}
			
			if(badCols.size() < M+1 -2){
				int goodC = 1;
				for(;goodC < M+1; goodC++){
					if(!badCols.contains(goodC)){
						break;
					}
				}
				List<Segment> s = new ArrayList<Segment>();
				s.add(new Segment(0,N));
				remColSegments.put(goodC, s);
				remColSegmentsSize.put(goodC, s.get(0).getLength());
			}
			
			//by nim, must exist a move to make next nim val = 0
			//CUR = totNim
			//CUR ^ totNim = 0
			//(seg1^totNim) ^ the rest = 0
			//since nim allows 1 move per turn, must exist a segment that can be reduced to seg^totNim
			
			for(int r : remRowSegmentsSize.keySet()){
				int size = remRowSegmentsSize.get(r);
				int targetSize = size - (nimSum ^ size);
				
				if(targetSize <= size && targetSize >= 0){
					int curSize = 0;
					List<Segment> goodSegments = remRowSegments.get(r);
					Segment end = null;
					for(Segment s : goodSegments){
						if(curSize + s.getLength() > targetSize){
							end = s;
							break;
						}else{
							curSize += s.getLength();
						}
					}
					int finalEnd = end == null ? M :end.start + (targetSize-curSize);
				
					System.out.println("FIRST");
					System.out.println(0 + " " + r + " " + finalEnd + " " + r);
					
					return;
				}
			}
			
			for(int c : remColSegmentsSize.keySet()){
				int size = remColSegmentsSize.get(c);
				int targetSize = size - (nimSum ^ size);
				
				if(targetSize <= size && targetSize >=0){
					int curSize = 0;
					List<Segment> goodSegments = remColSegments.get(c);
					Segment end = null;
					for(Segment s : goodSegments){
						if(curSize + s.getLength() > targetSize){
							end = s;
							break;
						}else{
							curSize += s.getLength();
						}
					}
					int finalEnd = end == null ? N :end.start + (targetSize-curSize);
			
					System.out.println("FIRST");
					System.out.println(c + " " + 0 + " " + c + " " + finalEnd);
					
					return;
				}
			}	
		}
	}
	
	public static void debug(){
		System.out.println();
		System.out.println("row -> size -> good row segments");
		for(int r : remRowSegments.keySet()){
			System.out.println(r + " " + remRowSegmentsSize.get(r) + " " + remRowSegments.get(r));
		}
		
		System.out.println();
		System.out.println("col -> size -> good col segments");
		for(int c : remColSegments.keySet()){
			System.out.println(c + " " + remColSegmentsSize.get(c) + " " + remColSegments.get(c));
		}
	}
	
	//get good segments untouched by bad segments
	public static List<Segment> getUntouchedSegments(List<Segment> badSegments, int start, int end){
		Collections.sort(badSegments);
		badSegments.add(new Segment(end, end+1));
		int curEnd = 0;
		List<Segment> good = new ArrayList<Segment>();
		for(Segment bad : badSegments){
			if(bad.start > curEnd){
				good.add(new Segment(curEnd, bad.start));
				curEnd = bad.end;
			}else{
				curEnd = Math.max(bad.end, curEnd);
			}
		}
		return good;
	}
	
	public static class Segment implements Comparable<Segment>{
		public int start;
		public int end;
		
		public Segment(int p1, int p2){
			start = Math.min(p1, p2);
			end = Math.max(p1,  p2);
		}

		public int getLength(){
			return end - start;
		}
		
		@Override
		public int compareTo(Segment o) {
			return this.start - o.start;
		}
		
		@Override
		public String toString(){
			return start + "->" + end;
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
