package Round220;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class InnaAndPinkPony {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	static int dir;
	static int[] mrs = new int[]{1,-1};
	static int[] mcs = new int[]{1, -1};
	static int ROW;
	static int COL;
	static int dr;
	static int dc;
	static int sr;
	static int sc;
	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		tk = tk(in.readLine());
		ROW = Integer.parseInt(tk.nextToken());
		COL = Integer.parseInt(tk.nextToken());
		sr = Integer.parseInt(tk.nextToken());
		sc = Integer.parseInt(tk.nextToken());
		dr = Integer.parseInt(tk.nextToken());
		dc = Integer.parseInt(tk.nextToken());
		
		List<Integer> ans = new ArrayList();
		ans.add(ok(ROW, COL));
		ans.add(ok(1 , 1));
		ans.add(ok(1, COL));
		ans.add(ok(ROW, 1));
		int min = Integer.MAX_VALUE;
		for(int i:ans){
			min = Math.min(i, min);
		}
		if(min == Integer.MAX_VALUE) {
			System.out.println("Poor Inna and pony!");
		}else{
			System.out.println(min);
		}
	}
	
	static int ok(int tr, int tc) {
		if(Math.abs(tr-sr) % dr != 0 || Math.abs(tc -sc) % dc != 0) return Integer.MAX_VALUE;
		int stepr = Math.abs(tr-sr)/dr;
		int stepc = Math.abs(tc-sc)/dc;
		if((stepr + stepc) % 2 != 0) {
			return Integer.MAX_VALUE;
		}
		
		if(sr == tr && sc == tc) return 0;
		boolean canmove = false;
		for(int mr : mrs){
			for(int mc: mcs) {
				int nc = sc + mc * dc;
				int nr = sr + mr * dr;
				canmove |= (nr <= ROW && nr >= 1 && nc <= COL && nc >= 1);
			}
		}
		if(!canmove) return Integer.MAX_VALUE;
		return Math.max(stepr, stepc);
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
