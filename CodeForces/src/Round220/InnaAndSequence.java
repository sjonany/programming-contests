package Round220;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class InnaAndSequence {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	static int SZ = 1000005;
	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);

		int numbad;
		int numquery;

		tk = tk(in.readLine());
		numquery = Integer.parseInt(tk.nextToken());
		numbad = Integer.parseInt(tk.nextToken());
		
		tk = tk(in.readLine());
		boolean[] isbad = new boolean[SZ];
		for(int i=0;i<numbad;i++) {
			isbad[Integer.parseInt(tk.nextToken())] = true;
		}

		//how many thumps til i die if i am original at spot i
		int[] life = new int[SZ];
		int[] culbad = new int[SZ];
		
		for(int i=1;i<SZ; i++) {
			culbad[i] = culbad[i-1];
			if(isbad[i]){
				culbad[i]++;
				life[i] = 1;
			}else{
				if(culbad[i] == 0) {
					life[i] = Integer.MAX_VALUE/3;
				}else 
					life[i] = life[i-culbad[i]] + 1;
			}
		}
		
		//how many times will i get thumped if im the ith query
		int[] thumps = new int[numquery];
		int[] qs = new int[numquery];

		for(int i=0;i<numquery;i++) {
			qs[i] = Integer.parseInt(in.readLine());
		}
		
		for(int i = numquery-1; i>=0;i--) {
			thumps[i] = qs[i] < 0 ? 1 : 0;
			if(i!=numquery-1)
				thumps[i] += thumps[i+1];
		}
		
		int len = 0;
		for(int i=0;i<numquery;i++) {;
			if(qs[i] < 0) {
				len -= culbad[len];
			}else{
				len++;
				if(life[len] > thumps[i]) {
					out.print(qs[i]);
				}
			}
		}
		if(len == 0){
			out.println("Poor stack!");
		}
		out.close();
	}
	
	/*
an element is alive iff
at the position he is added, his life > number affected thumps
number affected thumps = number of thumps that happen after he is added
life = 1 if he is located at bad position, or else, 1 + life of next position after he got thumped = i - culbad[i]
	 */
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
