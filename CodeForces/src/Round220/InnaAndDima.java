package Round220;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class InnaAndDima {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	static int ROW;
	static int COL;

	static int[][] visit;
	static boolean loop = false;
	static short[][] board;
	// longest path starting from r,c
	static int[][] dp;

	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		tk = tk(in.readLine());
		ROW = Integer.parseInt(tk.nextToken());
		COL = Integer.parseInt(tk.nextToken());
		board = new short[ROW][COL];
		visit = new int[ROW][COL];
		for(int r=0;r<ROW;r++){
			String s = in.readLine();
			for(int c=0;c<COL;c++){
				switch(s.charAt(c)){
				case 'D':
					board[r][c] = 0;
					break;
				case 'I':
					board[r][c] = 1;
					break;
				case 'M':
					board[r][c] = 2;
					break;
				default:
					board[r][c] = 3;
					break;
				}
			}
		}

		for(int r=0;r<ROW;r++) {
			for(int c=0;c<COL;c++) {
				if(board[r][c] == 0 && visit[r][c] == UNVISITED) {
					dfs(r,c);
				}
				if(loop)break;
			}	
			if(loop)break;
		}

		if(loop) {
			out.println("Poor Inna!");
			out.close();
			return;
		}

		dp = new int[ROW][COL];
		int maxlen = 0;
		for(int r=0;r<ROW;r++) {
			for(int c=0;c<COL;c++) {
				if(dp[r][c] == 0){
					dp[r][c] = get(r,c);
				}
				if(board[r][c] == 0){
					maxlen = Math.max(maxlen, dp[r][c]);
				}
			}	
		}
		
		int ans = maxlen / 4;
		if(ans == 0) {
			out.println("Poor Dima!");
		} else {
			out.println(ans);
		}
		out.close();
	}

	static int get(int r, int c) {
		if(dp[r][c] != 0) {
			return dp[r][c];
		}
		
		for(int[] mv : mvs) {
			int nr = r + mv[0];
			int nc = c + mv[1];
			if(nr >= 0 && nr < ROW && nc >= 0 && nc < COL && (board[r][c] + 1) % 4 == board[nr][nc]) {
				dp[r][c] = Math.max(dp[r][c], get(nr, nc));
			}
		}
		dp[r][c]++;
		return dp[r][c];
	}
	
	static int UNVISITED = 0;
	static int EXPLORING = 1;
	static int DONE = 2;
	static int[][] mvs = {{0,1},{1,0},{-1,0},{0,-1}};
	static void dfs(int r, int c) {
		if(visit[r][c] != 0) return;
		visit[r][c] = EXPLORING;
		for(int[] mv : mvs) {
			int nr = r + mv[0];
			int nc = c + mv[1];
			if(nr >= 0 && nr < ROW && nc >= 0 && nc < COL && (board[r][c] + 1) % 4 == board[nr][nc]) {
				if(visit[nr][nc] == EXPLORING) {
					loop = true;
					return;
				}
				if(visit[nr][nc] == UNVISITED)
					dfs(nr, nc);
			}
		}
		visit[r][c] = DONE;
	}

	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
