package Round220;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class InnaAndNine {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		String s = in.readLine();
		
		int count = 0;
		boolean isnine=false;
		long ans = 1;
		for(int i=0;i < s.length();i++) {
			int c1 = s.charAt(i)-'0';
			int c2 = i+1 == s.length() ? 100 : s.charAt(i+1)-'0';
			if(isnine) {
				if(c1+c2 == 9){
					count++;
				}else{
					if(count % 2 == 1){
						ans *= (count +  1) /  2;
					}
					count = 0;
					isnine = false;
				}
			}else{
				if(c1+c2 == 9) {
					isnine = true;
					count+=2;
				}
			}
		}
		
		System.out.println(ans);
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
