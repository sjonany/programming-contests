package Round220;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class InnaAndBabies {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	static int HORI;
	static int VERT;
	static Map<Integer, List<Line>> horbucket;
	static Map<Integer, List<Line>> verbucket;
	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		tk = tk (in.readLine());
		HORI = Integer.parseInt(tk.nextToken());
		VERT = Integer.parseInt(tk.nextToken());
		
		horbucket = new HashMap();
		verbucket = new HashMap();
		
		for(int i=0;i<HORI;i++) {
			tk = tk(in.readLine());
			int x = Integer.parseInt(tk.nextToken());
			int y = Integer.parseInt(tk.nextToken());
			int horx = x - y;
			int hory = x + y;
			Line hor = new Line(horx, horx, hory);
			add(horbucket, hor, hory);
		}
		
		for(int i=0;i<VERT;i++) {
			tk = tk(in.readLine());
			int x = Integer.parseInt(tk.nextToken());
			int y = Integer.parseInt(tk.nextToken());
			int verx = x - y;
			int very = x + y;
			Line ver = new Line(very, very, verx);
			add(verbucket, ver, verx);
		}
		
		for(int key : horbucket.keySet()) {
			Collections.sort(horbucket.get(key), new Comparator<Line>() {
				@Override
				public int compare(Line o1, Line o2) {
					return o1.t1 - o2.t1;
				}
			});
		}
		
		for(int key : verbucket.keySet()) {
			Collections.sort(verbucket.get(key), new Comparator<Line>() {
				@Override
				public int compare(Line o1, Line o2) {
					return o1.t1 - o2.t1;
				}
			});
		}
		
		edges = new short[HORI + VERT + 1][Math.max(HORI, VERT)];
		// key = hors, hors, value = ver, such that hor -> ver -> hor
		map = new short[edges.length][edges.length];
		hors = new LineList();
		vers = new LineList();
		hors.arr = new Line[HORI];
		vers.arr = new Line[VERT];
		edgeSize = new short[edges.length];
		for(int i=0;i<HORI;i++){
			hors.arr[i] = new Line(0,0,0);
		}
		for(int i=0;i<VERT;i++){
			vers.arr[i] = new Line(0,0,0);
		}
		
		int lo = 0;
		int MAX = Integer.MAX_VALUE / 3;
		int hi = MAX;
		
		while(lo < hi) {
			int mid = (lo + hi + 1) / 2;
			if(check(mid)) {
				hi = mid - 1;
			} else {
				lo = mid;
			}
		}
		if(lo == MAX) {
			System.out.println("Poor Sereja!");
		} else {
			System.out.println(lo+1);
		}
	}
	
	static void add(Map<Integer, List<Line>> map, Line it, int key) {
		if(!map.containsKey(key)) {
			map.put(key, new ArrayList());
		}
		map.get(key).add(it);
	}
	
	static LineList hors;
	static LineList vers;
	static short[] edgeSize;
	static short[][] edges;
	static short[][] map;
	static boolean check(int t) {
		hors.size = 0;
		vers.size = 0;
		
		clump(hors, horbucket, t);
		clump(vers, verbucket, t);

		for(int i=0;i<hors.arr.length;i++)hors.arr[i].id = (short) (i + 1);
		for(int i=0;i<vers.arr.length;i++)vers.arr[i].id = (short) (i + hors.size + 1);
		Arrays.fill(edgeSize, (short)0);
		
		for(short[] arr : map) {
			Arrays.fill(arr, (short)0);
		}
		
		for(int horii = 0; horii < hors.size; horii++) {
			Line hor = hors.arr[horii];
			for(int verii = 0; verii < vers.size; verii++) {
				Line ver = vers.arr[verii];
				if(ver.p <= hor.t2 && ver.p >= hor.t1 && hor.p >= ver.t1 && hor.p <= ver.t2) {
					edges[hor.id][edgeSize[hor.id]++] = ver.id;
					edges[ver.id][edgeSize[ver.id]++] = hor.id;
				}
			}
		}
		
		for(int i = 0; i < hors.size; i++) {
			short v1 = hors.arr[i].id;
			for(int v2i = 0; v2i < edgeSize[v1]; v2i++) {
				short v2 = edges[v1][v2i];
				for(int v3i = 0; v3i < edgeSize[v2]; v3i++) {
					short v3 = edges[v2][v3i];
					if(v3 == v1) continue;
					if(map[v1][v3] == 0)
						map[v1][v3] = v2;
					else {
						if(map[v1][v3] == v2) {
							continue;
						} else {
							// found cycle of 4
							return true;
						}
					} 
						
				}
			}
		}
		return false;
	}
	
	static void clump(LineList lines, Map<Integer, List<Line>> bucket, int t) {
		for(List<Line> linear : bucket.values()) {
			Line cur = linear.get(0);
			int ct1 = cur.t1 - 2 * t;
			int ct2 = cur.t2 + 2 * t;
			int cp = cur.p;
			int index = 0;
			while(true) {
				while(index + 1 < linear.size()) {
					Line next = linear.get(index+1);
					int nt1 = next.t1 - 2 * t;
					int nt2 = next.t2 + 2 * t;
					if(nt1 <= ct2) {
						ct2 = Math.max(ct2, nt2);
						index++;
					} else {
						break;
					}
				}
				
				Line l = lines.arr[lines.size++];
				l.p = cp;
				l.t1 = ct1;
				l.t2 = ct2;
				
				index++;
				if(index >= linear.size()) {
					break;
				}
				
				cur = linear.get(index);
				ct1 = cur.t1 - 2 * t;
			    ct2 = cur.t2 + 2 * t;
				cp = cur.p;
			}
		}
	}
	
	static class LineList {
		public Line[] arr;
		public int size;
	}
	
	static class Line { 
		public int t1;
		public int t2; 
		public int p;
		public short id;
		
		public Line(Line l) {
			this(l.t1, l.t2, l.p);
		}
		
		public Line(int t1, int t2, int p) {
			this.t1 = t1;
			this.t2 = t2;
			this.p = p;
		}
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
