package Round220;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class InnaAndSequenceSlower {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);

		int numbad;
		int numquery;

		tk = tk(in.readLine());
		numquery = Integer.parseInt(tk.nextToken());
		numbad = Integer.parseInt(tk.nextToken());
		
		tk = tk(in.readLine());
		int[] bads = new int[numbad];
		
		for(int i=0;i<numbad;i++) {
			bads[i] = Integer.parseInt(tk.nextToken());
		}
		Arrays.sort(bads);
		
		int SZ = numquery+5;
		
		// arr[i] = number of elements with index <= i that are good, i >= 1
		Fenwick2 arr = new Fenwick2(SZ);
		int[] els = new int[SZ];
		
		int len = 0;
		for(int i=0;i<numquery;i++) {
			int q = Integer.parseInt(in.readLine());
			if(q < 0) {
				int badi = 0;
				while(badi < bads.length) {
					int target = bads[badi]-badi;
					int arri = search(target, arr, SZ);
					if(arri < 0) 
						break;
					arr.update(arri, SZ, -1);
					els[arri] = -1;
					badi++;
				}
			} else {
				len++;
				els[len] = q;
				arr.update(len, SZ, 1);
			}
		}
		
		int count = 0;
		
		for(int i = 1; ; i++) {
			int ind = search(i, arr, SZ);
			if(ind < 0){
				break;
			}
			out.print(els[ind]);
			count++;
		}
		
		if(count == 0){
			out.println("Poor stack!");
		}
		out.close();
	}
	
	static int search(int v, Fenwick2 arr, int SZ) {
		int lo = 1;
		int hi = SZ;
		if(arr.query(SZ) < v) return -1;
		if(arr.query(lo) >= v) return lo;
		while(lo < hi) {
			int mid = (lo + hi + 1) / 2;
			if(arr.query(mid) < v) {
				lo = mid;
			} else {
				hi = mid-1;
			}
		}
		return lo+1;
	}
	
	static class Fenwick1{
		long[] tree;
		public Fenwick1(int n){
			tree = new long[n+1];
		}

		public void increment(int i, long val){
			while(i < tree.length){
				tree[i] += val;
				i += (i & -i);
			}
		}

		public long getTotalFreq(int i){
			long sum = 0;
			while(i > 0){
				sum += tree[i];
				i -= (i & -i);
			}
			return sum;
		}

		// get largest value with cumulative sum less than or equal to x;
		// for smallest, pass x-1 and add 1 to result
		public int getind(int x) {
			int N = tree.length-1;
			int idx = 0, mask = N;
			while(mask>0 && idx < N) {
				int t = idx + mask;
				if(x >= tree[t]) {
					idx = t;
					x -= tree[t];
				}
				mask >>= 1;
			}
			return idx;
		}
	}

	//range update point query
	static class Fenwick2{ 
		public Fenwick1 tree;
		
		public Fenwick2(int n){
			tree = new Fenwick1(n);
		}
		
		public long query(int i) {
			return tree.getTotalFreq(i);
		}
		
		public void update(int i, int j, long v){
			tree.increment(i, v);
			tree.increment(j+1, -v);
		}
	}
	
	static StringTokenizer tk(String s){return new StringTokenizer(s);}
}
