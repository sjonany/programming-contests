package Round186div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;


public class IlyaAndQueries {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		String tok = sc.nextToken();
		int[] dp = new int[tok.length()];
		
		for(int i=0;i<dp.length-1;i++){
			int prev = i == 0 ? 0 : dp[i-1];
			if(tok.charAt(i) == tok.charAt(i+1)){
				dp[i] =  prev+1;
			}else{
				dp[i] = prev;
			}
		}
		if(dp.length>1)
			dp[dp.length-1] = dp[dp.length-2];
		
		int m = sc.nextInt();
		for(int i=0;i<m;i++){
			int l = sc.nextInt()-1;
			int r = sc.nextInt()-2;
			if(l == 0){
				out.println(dp[r]);
			}else{
				out.println(dp[r] - dp[l-1]);
			}
		}
		
		out.close();
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
