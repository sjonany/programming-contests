package Round186div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.StringTokenizer;


public class IlyaAndMatrix {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int size = sc.nextInt();
		int pow = 0;
		int cur = 1;
		while(cur < size){
			cur *= 4;
			pow++;
		}

		
		long[] arr = new long[size];
		for(int i=0;i<size;i++){
			arr[i] = sc.nextInt();
		}
		/*
		pow = 2;
		
		size = 16;
		arr = new long[size];
		for(int i=0;i<size;i++){
			arr[i] = i+1;
		}*/
		Arrays.sort(arr);
		BigInteger sum = BigInteger.ZERO;
		BigInteger ans = BigInteger.ZERO;
		
		int count = 0;
		int[] indices = new int[pow+1];
		int k=1;
		for(int i=0;i<=pow;i++){
			indices[i] = k;
			k*=4;
		}
		
		int index = 0;
		for(int i = size-1;i>=0;i--){
			sum = sum.add(BigInteger.valueOf(arr[i]));
			count++;
			if(count == indices[index]){
				ans = ans.add(sum.multiply(BigInteger.valueOf(pow+1)));
				sum = BigInteger.ZERO;
				index++;
				pow--;
			}
		}
		
		out.println(ans);
		out.close();
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
