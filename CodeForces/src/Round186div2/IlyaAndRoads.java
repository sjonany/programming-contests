package Round186div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class IlyaAndRoads {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		int n = sc.nextInt();
		int m = sc.nextInt();
		int k = sc.nextInt();
		
		//fix i->j exact min cost
		long[][] dp = new long[n][n];
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++)
				dp[i][j] = Long.MAX_VALUE;
		}
		
		for(int i=0;i<m; i++){
			int l = sc.nextInt()-1;
			int r = sc.nextInt()-1;
			int c = sc.nextInt();

			dp[l][r] = Math.min(dp[l][r], c);
		}
		
		for(int length=n-1;length>=1;length--){
			for(int start = 0; start < n-length+1; start++){
				int end = start + length - 1;
				//either length=k, exactly, or wider guy
				if(end + 1 < n){
					dp[start][end] = Math.min(dp[start][end], dp[start][end+1]);
				}
				
				if(start > 0){
					dp[start][end] = Math.min(dp[start][end], dp[start-1][end]);
				}
			}
		}
		
		//for(int i=0;i<n;i++){for(int j=0;j<n;j++){if(dp[i][j]!=Long.MAX_VALUE)System.out.println((i+1) +"->"+(j+1)+ " = " +dp[i][j] + " ");}}
		
		//first i cols, exactly j holes
		long[][] dp2 = new long[n][n+1];
		for(int i=0;i<n;i++){
			for(int j=1;j<n+1;j++){
				dp2[i][j] = Long.MAX_VALUE;
				if(i != 0)
					dp2[i][j] = Math.min(dp2[i][j], dp2[i-1][j]);
				if(i==j-1)
					dp2[i][j] = Math.min(dp2[i][j], dp[0][i]);
				for(int part = i; part>0;part--){
					int rem = j - (i-part+1);
					if(rem >= 0 && dp[part][i] != Long.MAX_VALUE && dp2[part-1][rem] != Long.MAX_VALUE)
						dp2[i][j] = Math.min(dp2[i][j], dp[part][i] + dp2[part-1][rem]);
				}
			}
		}
		
		long bestCost = Long.MAX_VALUE;
		for(int holes = k; holes <= n; holes++){
			bestCost = Math.min(bestCost, dp2[n-1][holes]);
		}
		
		if(bestCost == Long.MAX_VALUE){
			out.println(-1);
		}else{
			out.println(bestCost);
		}
		out.close();
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
