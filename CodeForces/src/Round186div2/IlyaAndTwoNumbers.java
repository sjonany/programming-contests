package Round186div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Stack;
import java.util.StringTokenizer;

public class IlyaAndTwoNumbers {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = sc.nextInt();
		int m = sc.nextInt();
		int[] a = new int[m];
		int[] b = new int[m];
		for(int i = 0; i < n; i++){
			a[sc.nextInt()]++;
		}
		
		for(int i = 0; i < n; i++){
			b[sc.nextInt()]++;
		}
		
		int[] c = new int[m];
		Stack<Count> st = new Stack<Count>();
		
		/*
		 first consume pairs where total < m 
		 
		 know for sure that there is no harm in immediately consuming if total = m-1, since addition
		 each item can be used one anyways.
		 from how the loop goes in increasing order, if j = m-i-1 has finished consuming stack's top, 
		 then it's ok to consume items < i, because there is no way they can generate m-1. If they had,
		 they would no longer be on the stack.
		 */
		for(int i = 0; i < m; i++){
			if(a[i] != 0)
				st.add(new Count(i, a[i]));
			int j = m-i-1;
			while(!st.isEmpty() && b[j] > 0){
				Count top = st.peek();
				int numUsed = Math.min(top.count, b[j]);
				c[j + top.val] += numUsed;
				b[j] -= numUsed;
				top.count -= numUsed;
				if(top.count == 0){
					st.pop();
				}
			}
		}
		
		/*
		 does there exist a case where a pair should not have been totaled, but be moduloed?
		 is it possible to have taken pair that is better if done in modulo?
		 now stack is left with unmatched i's, where we have to overshoot
		 if we have to overshoot, need to max sum
		 */
		for(int i = m-1; i >= 0;i--){
			while(b[i] > 0){
				Count top = st.peek();
				int numUsed = Math.min(top.count, b[i]);
				c[i + top.val- m] += numUsed;
				b[i] -= numUsed;
				top.count -= numUsed;
				if(top.count == 0){
					st.pop();
				}
			}
		}
		
		for(int i = m-1; i >= 0; i--){
			for(int j = 0; j < c[i]; j++){
				out.print(i + " ");
			}
		}
		
		out.close();
	}
	
	static class Count{
		public int val;
		public int count;
		public Count(int v, int c){
			this.val = v;
			this.count = c;
		}
	}

	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
