package Round224;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
// n^2 log n solution with recursion in Java MLE, just for fun :D
public class D {
	
	static PrintWriter out;
	static InputReader in;
	static char[][] board;
	
	static short UNVISITED = 0;
	static short VISITING = 1;
	static short EXPLORED = 2;
	static boolean cycle = false;
	static short[][] visited;
	static int ROW;
	static int COL;
	static int[][] dp;
	static int ROOT;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		ROW = in.nextInt();
		COL = in.nextInt();
		board = new char[ROW][COL];
		
		for(int i=0;i<ROW;i++) {
			board[i] = in.nextToken().toCharArray();
		}
		visited = new short[ROW][COL];
		
		for(int r=0;r<ROW;r++) {
			for(int c=0;c<COL;c++) {
				if(visited[r][c] == UNVISITED)
					dfs(r,c);
				if(cycle)break;
			}	
			if(cycle)break;
		}
		
		if(cycle){
			out.println(-1);
			out.close();
			return;
		}
		
		dp = new int[ROW][COL];
		for(int r = 0; r <ROW; r++) {
			Arrays.fill(dp[r], -1);;
		}
		
		for(int r=0;r<ROW;r++) {
			for(int c=0;c<COL;c++) {
				dp[r][c] = solve(r,c);
			}	
		}
		
		int max = Integer.MIN_VALUE;
		
		for(int r=0;r<ROW;r++) {
			for(int c=0;c<COL;c++) {
				max = Math.max(max, dp[r][c]);
			}	
		}
		
		if(max == 0){
			out.println(0);
			out.close();
			return;
		}
		
		List<Point> sols = new ArrayList<Point>();
		for(int r=0;r<ROW;r++) {
			for(int c=0;c<COL;c++) {
				if(max == dp[r][c]){
					sols.add(new Point(r,c));
				}
			}	
		}
		
		// LCA
		ROOT = ROW * COL;
		initLCA(ROOT+1, ROOT);
		
		// if exist two points that won't collide -> 2*max
		boolean existNonCollidePair = false;
		
		if(sols.size() >= 2) {
			for(int i=0; i < sols.size()-1; i++) {
				if(!collide(sols.get(i), sols.get(i+1))) {
					existNonCollidePair = true;
				}
			}
		}
		
		if(existNonCollidePair)
			out.println(2*max);
		else 
			out.println(2*max - 1);
		
		out.close();
	}
	
	static boolean collide(Point p1, Point p2) {
		int v1 = p1.r * COL + p1.c;
		int v2 = p2.r * COL + p2.c;
		int par = lca(v1,v2);
		int r = par / COL;
		int c = par % COL;
		if(par == ROOT || board[r][c] == '#') {
			return false;
		} else {
			return depth[v1] == depth[v2];
		}
	}
	
	static int[] tin, tout;
	static int[][] up;
	static int timer;
	static int l;
	static int[] depth;
/**
first detect cycle
if cycle -> -1
get dp[i][j] = points if single guy goes
then u can only get blocked once

Build trees -> root = last guy before "#"
find tree tips and their depths

see tree tips that get = max1
if these tips are in the same tree, they will collide
	- 2*max-1
if there are max tips that are not in the same tree, 
	we have 2 * max
So just get, for each tree, find their maxes, 
max(2*max-1, treemax1 + treemax2)


 */
	static int[][] dirs = {{1,0},{0,1},{-1,0},{0,-1}};
	static char[] symb = {'^','<','v','>'};
	static void dfsl(int v, int p){
		depth[v] = depth[p] + 1;
		tin[v] = ++timer;
		up[v][0] = p;
	    for(int i = 1; i <= l; i++ )
			up[v][i] = up[up[v][i-1]][i-1];
	    
	    if(v == ROOT) {
	    	for(int r=0;r<ROW;r++) {
	    		for(int c=0;c<COL;c++) {
		    		if(board[r][c] == '#'){
		    			dfsl(r*COL + c, v);
		    		}
		    	}	
	    	}
	    } else {
	    	int r = v / COL;
	    	int c = v % COL;
	    	if(board[r][c] == '#') {
	    		for(int i =0;i<4;i++){
	    			int nr = r + dirs[i][0];
	    			int nc = c + dirs[i][1];
					if(nr>=0 && nr<ROW && nc>=0 && nc < COL && board[nr][nc] == symb[i]) {
						int target = nr * COL + nc;
						if(p != target){
							dfsl(target, v);
						}
					}
	    		}
	    	}else{
	    		for(int i =0;i<4;i++){
	    			int nr = r + dirs[i][0];
	    			int nc = c + dirs[i][1];
					if(nr>=0 && nr<ROW && nc>=0 && nc < COL && board[nr][nc] == symb[i]) {
						int target = nr * COL + nc;
						if(p != target){
							dfsl(target, v);
						}
					}
	    		}
	    	}
	    }
		tout[v] = ++timer;
	}

	static boolean upper(int a, int b){
		return tin[a] <= tin[b] && tout[a] >= tout[b];
	}

	static int lca(int a, int b){
		if(upper(a, b)) return a;
		if(upper(b, a)) return b;
		for (int i = l; i >= 0; i--)
			if(!upper(up[a][i], b))
				a = up[a][i];
		return up[a][0];
	}

	static void initLCA(int n, int root){
		tin = new int[n];
		tout = new int[n];
		up = new int[n][];
		depth = new int[n];
		
		l = 1;
		while((1 << l) <= n) ++l;
		for (int i = 0; i <n; i++) 
	        up[i] = new int[l + 1];
		depth[root] = -1;
		dfsl(root, root);
	}
	
	
	static class Point {
		public int r;
		public int c;
		public Point(int r, int c) {
			this.r = r;
			this.c = c;
		}
		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + "]";
		}
	}
	
	static int solve(int r, int c) {
		if(dp[r][c] >= 0){
			return dp[r][c];
		} else {
			if(board[r][c] == '#'){
				dp[r][c] = 0;
			}else{
				if(board[r][c] == '^') {
					dir[0] = -1;
					dir[1] = 0;
				} else if(board[r][c] == '>') {
					dir[0] = 0;
					dir[1] = 1;
				}else if(board[r][c] == '<') {
					dir[0] = 0;
					dir[1] = -1;
				}else {
					dir[0] = 1;
					dir[1] = 0;
				}

				int nr = r + dir[0];
				int nc = c + dir[1];
				dp[r][c] = 1;
				if(nr>=0 && nr<ROW && nc>=0 && nc < COL) {
					dp[r][c] += solve(nr, nc);
				}
			}
		}
		return dp[r][c];
	}
	
	static int[] dir = new int[2];
	static void dfs(int r, int c) {
		visited[r][c] = VISITING;
		if(board[r][c] == '#') {
			
		} else {
			if(board[r][c] == '^') {
				dir[0] = -1;
				dir[1] = 0;
			} else if(board[r][c] == '>') {
				dir[0] = 0;
				dir[1] = 1;
			}else if(board[r][c] == '<') {
				dir[0] = 0;
				dir[1] = -1;
			}else {
				dir[0] = 1;
				dir[1] = 0;
			}
			
			int nr = r + dir[0];
			int nc = c + dir[1];
			if(nr>=0 && nr<ROW && nc>=0 && nc < COL) {
				if(visited[nr][nc] == VISITING) {
					cycle = true;
				} else if(visited[nr][nc] == UNVISITED){
					dfs(nr,nc);
				} else {
					//already explored, no cycle
				}
			}
		}
		visited[r][c] = EXPLORED;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
