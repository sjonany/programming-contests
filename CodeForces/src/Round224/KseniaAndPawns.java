package Round224;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
public class KseniaAndPawns{
	static PrintWriter out;
	static InputReader in;
	static char[][] board;
	
	static int ROW;
	static int COL;
	static int[][] dp;
	static int ROOT;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		ROW = in.nextInt();
		COL = in.nextInt();
		board = new char[ROW][COL];
		
		boolean existPoint = false;
		for(int i=0;i<ROW;i++) {
			board[i] = in.nextToken().toCharArray();
			for(int j=0;j<COL;j++) {
				if(board[i][j] != '#')
					existPoint = true;
			}
		}
		
		if(!existPoint) {
			out.println(0);
			out.close();
			return;
		}
		
		if(isCycle()) {
			out.println(-1);
			out.close();
			return;
		}
		
		List<Integer> maxes = new ArrayList<Integer>();
		int[][] dist = new int[ROW][COL];
		for(int r = 0; r < ROW; r++) {
			for(int c = 0; c < COL; c++) {
				if(board[r][c] != '#') {
					int[] move = move(r,c);
					if(board[r+move[0]][c+move[1]] == '#') {
						// found a root!
						Queue<Integer> q = new LinkedList();
						int max = 1;
						dist[r][c] = 1;
						q.add(r*COL+c);
						while(!q.isEmpty()) {
							int node = q.poll();
							int cr = node / COL;
							int cc = node % COL;
							for(int[] dir : moves) {
								int nr = cr + dir[0];
								int nc = cc + dir[1];
								if(board[nr][nc] == '#') continue;
								int[] cmove = move(nr,nc);
								if(nr + cmove[0] == cr && nc + cmove[1] == cc) {
									dist[nr][nc] = dist[cr][cc]+1;
									max = Math.max(dist[nr][nc], max);
									q.add(nr*COL + nc);
								}
							}
						}
						maxes.add(max);
					}
				}
			}	
		}
		Collections.sort(maxes);
		int ans = 2 * maxes.get(maxes.size()-1)- 1;
		if(maxes.size() > 1) {
			ans = Math.max(ans, maxes.get(maxes.size()-1) + maxes.get(maxes.size()-2));
		}
		out.println(ans);
		out.close();
	}
	
	static boolean isCycle() {
		int[][] visited = new int[ROW][COL];
		int time = 1;
		for(int r = 0; r < ROW; r++) {
			for(int c = 0; c < COL; c++) {
				if(visited[r][c] != 0 || board[r][c] == '#') {
					continue;
				}
				int cr = r;
				int cc = c;
				
				while(true) {
					visited[cr][cc] = time;
					int[] move = move(cr,cc);
					cr += move[0];
					cc += move[1];
					if(visited[cr][cc] == time) {
						return true;
					}
					if(board[cr][cc] == '#')
						break;
				}
				time++;
			}	
		}
		return false;
	}
	
	static int[][] moves = {{1,0},{0,1},{-1,0},{0,-1}};
	static int[] move(int r, int c) {
		char ch = board[r][c];
		if(ch == '>')
			return moves[1];
		else if(ch == '<') 
			return moves[3];
		else if(ch == '^')
			return moves[2];
		else if(ch == 'v')
			return moves[0];
		System.out.println("wtf");
		return null;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
