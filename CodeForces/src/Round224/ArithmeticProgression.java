package Round224;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.TreeSet;

public class ArithmeticProgression {
	
	static PrintWriter out;
	static InputReader in;
	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int N = in.nextInt();
		Long[] els = new Long[N];
		for(int i=0;i<N;i++){
			els[i] = in.nextLong();
		}
		
		Arrays.sort(els);
		if(N == 1){
			out.println(-1);
		} else {
			int period = Integer.MAX_VALUE;
			for(int i=0;i<N-1;i++) {
				period = (int)Math.min(period, els[i+1]-els[i]);
			}

			boolean isarith = true;
			List<Integer> badindices = new ArrayList();
			for(int i=0;i<N-1;i++) {
				if((int)(els[i+1]-els[i]) != period) {
					isarith = false;
					badindices.add(i);
				}
			}
			
			if(isarith) {
				TreeSet<Integer> set = new TreeSet();
				if(N == 2 && ((els[1] - els[0]) % 2 == 0)) {
					set.add((int) (els[0] - period));
					set.add((int) ((els[0] + els[1]) / 2));
					set.add((int) (els[N-1] + period));
					out.println(set.size());
					for(int i : set){
						out.print(i + " ");
					}
				} else {
					set.add((int) (els[0] - period));
					set.add((int) (els[N-1] + period));
					out.println(set.size());
					for(int i : set){
						out.print(i + " ");
					}
				}
			} else {
				if(badindices.size() == 1){
					int bad = badindices.get(0);
					if(els[bad+1] - els[bad] == period * 2) {
						out.println(1);
						out.println(els[bad] + period);
					} else {
						out.println(0);
					}
				}else {
					out.println(0);
				}
			}
		}
		
		out.close();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
