package Round188div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.StringTokenizer;

public class Balance {
	static int n;
	static int v;
	static int e;
	static int k;
	static int[] amount;
	static int[] desired;
	static Set<Integer>[] edges;
	static List<List<Integer>> comps;
	static boolean [] isComp;

	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);

		n = sc.nextInt();
		v = sc.nextInt();
		e = sc.nextInt();
		k = 0;
		isComp = new boolean[n+1];
		amount = new int[n+1];
		desired = new int[n+1];
		comps = new ArrayList<List<Integer>>();
		edges = (Set<Integer>[]) Array.newInstance(Set.class, n+1);
		for(int i = 1; i <= n; i++){
			edges[i] = new HashSet<Integer>();
		}

		for(int i = 1; i <= n; i++){
			amount[i] = sc.nextInt();
		}

		for(int i = 1; i <= n; i++){
			desired[i] = sc.nextInt();
		}

		for(int i = 0; i < e; i++){
			int x = sc.nextInt();
			int y = sc.nextInt();
			edges[x].add(y);
			edges[y].add(x);
		}

		findConnectedComponents();
		//check sum comp desired = amount
		for(List<Integer> comp : comps){
			long sum = 0;
			for(int v : comp){
				sum += amount[v] - desired[v];
			}
			if(sum != 0){
				out.println("NO");
				out.close();
				return;
			}
		}

		StringBuffer log = new StringBuffer();
		//for each component fix
		for(List<Integer> comp : comps){
			Queue<Integer> exceed = new LinkedList<Integer>();
			Queue<Integer> less = new LinkedList<Integer>();
			for(int v : comp){
				if(amount[v] > desired[v]){
					exceed.add(v);
				}else if(amount[v] < desired[v]){
					less.add(v);
				}
			}
			
			while(!exceed.isEmpty()){
				int big = exceed.remove();
				int small = less.remove();
				
				int val = Math.min(amount[big] - desired[big], 
						desired[small] - amount[small]);
				
				List<Integer> path = new ArrayList<Integer>();
				int[] parent = new int[n+1];
				for(int i=0; i<parent.length; i++){
					parent[i] = -1;
				}
				parent[big] = 0;
				dfsTree(parent, big);

				int curNode = small;
				while(curNode != 0){
					path.add(curNode);
					curNode = parent[curNode];
				}
				
				Collections.reverse(path);
				
				transfer(path, path.size()-1, val, log);
				
				//at least one will be fixed
				if(amount[big] != desired[big]){
					exceed.add(big);
				}
				if(amount[small] != desired[small]){
					less.add(small);
				}
				
			}
		}
		out.println(k);
		out.println(log);
		out.close();
	}
	
	static void dfsTree(int[] parent, int node){
		for(int child : edges[node]){
			if(parent[child] == -1){
				parent[child] = node;
				dfsTree(parent, child);
			}
		}
	}
	
	//to = index based on path
	//transfer from start of path to 'to' with val, leaving in between untouched
	static void transfer(List<Integer> path, int to, int val, StringBuffer log){
		if(to == 0)return;
		int before = path.get(to-1);
		int after = path.get(to);
		if(amount[before] >= val){
			log.append(before + " " + after + " " + val + "\n");
			amount[before] -= val;
			amount[after] += val;
			k++;
			transfer(path, to-1, val, log);
		}else{
			if(v - amount[before] >= val){
				transfer(path, to-1, val, log);
				log.append(before + " " + after + " " + val + "\n");
				amount[before] -= val;
				amount[after] += val;
				k++;
			}else{
				int remFlow = val - amount[before];
				//allowed because t'.val < amount
				log.append(before + " " + after + " " + amount[before] + "\n");
				amount[after] += amount[before];
				amount[before] = 0;
				k++;
				
				//allowed because amount <= cap, and t' is empty
				transfer(path, to-1, val, log);
				
				//because amount >= remFlow = amount - t'.val
				log.append(before + " " + after + " " + remFlow + "\n");
				amount[before] -= remFlow;
				amount[after] += remFlow;
				k++;
				//t'. val is restored!
			}
		}
	}

	public static void findConnectedComponents(){
		for(int vi=1; vi <= n; vi++){
			if(!isComp[vi]){
				List<Integer> comp = new ArrayList<Integer>();
				dfs(vi, comp);
				comps.add(comp);
			}
		}
	}

	public static void dfs(int vi, List<Integer> comp){
		comp.add(vi);
		isComp[vi] = true;
		for(int out : edges[vi]){
			if(!isComp[out]){
				dfs(out, comp);
			}
		}
	}


	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
