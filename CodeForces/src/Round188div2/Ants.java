package Round188div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Stack;
import java.util.StringTokenizer;

public class Ants {
	static int SIZE = 80;
	static int[][] m = new int[2*SIZE+1][2*SIZE+1];
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = sc.nextInt();
		int t = sc.nextInt();
		m[SIZE][SIZE] = n;
		
		while(spread()){}
		for(int i=0; i< t; i++){
			int x = sc.nextInt();
			int y = sc.nextInt();
			
			if(x+SIZE >= m.length || y+SIZE >= m.length ||
					x+SIZE < 0 || y + SIZE < 0){
				out.println(0);
			}else{
				out.println(m[x+SIZE][y+SIZE]);
			}
		}
		out.close();
	}

	static boolean spread(){
		boolean canSpread = false;
		
		for(int x=0;x<m.length;x++){
			for(int y=0; y<m.length;y++){
				if(m[x][y] >=4){
					int spreadVal = m[x][y]/4;
					m[x][y]%=4;
					m[x][y+1] += spreadVal;
					m[x][y-1] += spreadVal;
					m[x+1][y] += spreadVal;
					m[x-1][y] += spreadVal;
					canSpread = true;
				}
			}
		}
		return canSpread;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
