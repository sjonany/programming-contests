package Round188div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class StringsOfPower {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		String s = sc.nextToken();
		
		List<Integer> heavy = new ArrayList<Integer>();
		List<Integer> metal = new ArrayList<Integer>();
		String heavyS = "heavy";
		String metalS = "metal";
		for(int i=0 ; i<s.length()-4; i++){
			boolean isHeavy = true;
			boolean isMetal = true;
			for(int j = 0; j<= 4; j++){
				if(heavyS.charAt(j) != s.charAt(i+j)){
					isHeavy = false;
				}
				if(metalS.charAt(j) != s.charAt(i+j)){
					isMetal = false;
				}
			}
			
			if(isHeavy){
				heavy.add(i);
			}
			if(isMetal){
				metal.add(i);
			}
		}
		
		int curH = 0;
		int curM = 0;
		long count = 0;
		if(!(heavy.size() == 0 || metal.size() == 0)){
			while(true){
				if(heavy.get(curH) < metal.get(curM)){
					count += (long)(metal.size() - curM);
					curH++;
				}else{
					curM++;
				}
				if(curH == heavy.size() || curM == metal.size()){
					break;
				}
			}
		}
		out.println(count);
		out.close();
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
