package Round188div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class PerfectPair {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		long x = sc.nextLong();
		long y = sc.nextLong();
		long m = sc.nextLong();
		if(x <= 0 && y <= 0){
			if(Math.max(x, y) >= m){
				out.println(0);
			}else{
				out.println(-1);
			}
		}else{
			long prev = Math.min(x, y);
			long next = Math.max(x, y);
			long count = 0;
			while(next < m){
				if(prev < 0){
					long div = -prev / next;
					prev += div * next;
					count += div;
					if(prev < 0){
						prev += next;
						count++;
						if(next < prev){
							long temp = next;
							next = prev;
							prev = temp;
						}
					}
					continue;
				}
				long temp = next;
				next = next + prev;
				prev = temp;
				
				if(next < prev){
					temp = next;
					next = prev;
					prev = temp;
				}
				count++;
			}
			out.println(count);
		}
		out.close();
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
