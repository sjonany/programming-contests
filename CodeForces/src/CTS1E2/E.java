package CTS1E2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class E {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
	
		int test = 1;
		while(true){
			tk = tk(in.readLine());
			int degree = Integer.parseInt(tk.nextToken());
			if(degree == 0)break;
			//from highest degree
			long[] arr = new long[degree+1];
			for(int i = 0; i <= degree; i++){
				arr[i] = Long.parseLong(tk.nextToken());
			}
			int val = Integer.parseInt(tk.nextToken());
			
			long ans = 0;
			for(int i = 0; i <= degree; i++){
				int pow = degree - i;
				if(val == 1){
					ans += arr[i];
				}else{
					ans += ((pow%2 == 0) ? 1 : -1) * arr[i];
				}
			}
			
			out.println(String.format("Polynomial %d: %d %d", test, ans, 1+solve(arr, degree)));
			test++;
		}
		
		out.close();
	}
	
	static long solve(long[] coeff, int degree) {
		long ans = 0;
		if(coeff[degree] != 0){
			ans += dig(coeff[degree]) + 1;
		}
		
		int lowestPow = 0;
		for(int i = 1; i <= degree; i++){
			if(coeff[degree-i] != 0){
				lowestPow = i;
				break;
			}
		}
		
		if(lowestPow == degree){
			ans += degree + degree - 1;
		}else{
			ans += 2 * lowestPow + solve(coeff, degree - lowestPow);
		}
		
		return ans;
	}
	
	static int dig(long num){
		return String.valueOf(num).length();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
