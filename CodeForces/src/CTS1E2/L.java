package CTS1E2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.StringTokenizer;

public class L {
	public static void main(String args[]) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		int n = Integer.parseInt(in.readLine());
		int[] arr = new int[n];
		tk = tk(in.readLine());

		for(int i = 0; i < n; i++){
			arr[i] = Integer.parseInt(tk.nextToken());
		}

		//is a[i] to a[j] non decreasing
		boolean[][] nondec = new boolean[n][n];
		for(int i = 0; i < n; i++){
			nondec[i][i] = true;
			for(int j = i+1; j < n; j++){
				if(nondec[i][j-1] && arr[j] > arr[j-1]){
					nondec[i][j] = true;
				}
			}
		}

		//if above row = i->j, what is the rightmost k >= j+1 such that arr[j+1] to arr[k] complies with above row?
		//by comply, I mean each element is greater than the above element
		int[][] maxcomply = new int[n][n];
		for(int[] a : maxcomply){
			Arrays.fill(a, -1);
		}
		for(int i = n-2; i >= 0; i--){ 
			for(int j = n-2; j >= i; j--){
				if(arr[i] < arr[j+1]){
					//slide, above row is i+1 -> j+1, so next row starts with j+2 below i+1
					maxcomply[i][j] = Math.min(2*j-i+1,maxcomply[i+1][j+1]);
					if(maxcomply[i][j] == -1)
						maxcomply[i][j] = j+1;
				}
			}
		}

		//num ways if consider i -> n, such that the first row's right most guy is <= j 
		BigInteger[][] dp = new BigInteger[n][n];

		for(int i = n-1; i >= 0; i--){
			for(int j = i; j < n; j++){
				dp[i][j] = BigInteger.ZERO;
				//either rightmost guy <= j-1
				if(j-1 >= 0) {
					dp[i][j] = dp[i][j-1];
					if(dp[i][j] == null)
						dp[i][j] = BigInteger.ZERO;
				}
				//or the first row is i->j
				if(nondec[i][j]){
					if(j == n-1){
						//nobody else following me
						dp[i][j] = dp[i][j].add(BigInteger.ONE);
					}else{
						//at least a second row exists
						//rightmost compliant ending for second row
						int rightmost = maxcomply[i][j];
						if(rightmost != -1){
							dp[i][j] = dp[i][j].add(dp[j+1][rightmost]);
						}
					}
				}
			}
		}

		out.println(dp[0][n-1]);
		out.close();
	}

	static StringTokenizer tk (String st){
		return new StringTokenizer(st);
	}
}