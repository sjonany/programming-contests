package CTS1E2;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class C {
	static int MAX = 1000000;
	static int[] seq;
	static int[] primeLocs;
	static boolean[] used;
	static List<Integer> primes;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		boolean[] isPrime = sieve(MAX);
		primes = new ArrayList<Integer>();
		for(int i = 2; i <= MAX; i++){
			if(isPrime[i])primes.add(i);
		}

		used = new boolean[MAX+1];
		seq = new int[MAX+1];
		primeLocs = new int[MAX+1];

		seq[0] = 1;
		seq[1] = 2;
		int maxPrimeIndex = 1;
		int maxPrime = 3;

		int count = 0;
		for(int i = 2; i <= MAX;i++){
			//factorize
			List<Integer> relevantPrimes;

			relevantPrimes = new ArrayList<Integer>();
			int num = seq[i-1];
			for(int primeIndex = 0; primeIndex < maxPrimeIndex; primeIndex++){
				int p = primes.get(primeIndex);
				if(p*p > num) break;
				if(num % p == 0){
					relevantPrimes.add(p);
					do{
						num /= p;
					}while(num % p == 0);
				}
			}
			if(isPrime[num]){
				relevantPrimes.add(num);
			}
			
			if(num % maxPrime == 0){
				relevantPrimes.add(maxPrime);
				maxPrimeIndex++;
				maxPrime = primes.get(maxPrimeIndex);
			}

			int next = Integer.MAX_VALUE;
			for(int fac : relevantPrimes){
				if(primeLocs[fac] == 0){
					primeLocs[fac] = fac;
				}

				int nextNum = primeLocs[fac];
				if(nextNum > MAX) continue;
				while(used[nextNum]){
					nextNum += fac;
					if(nextNum > MAX) break;
				}
				primeLocs[fac] = nextNum;
				next = Math.min(nextNum, next);
			}
			
			used[next] = true;
			seq[i] = next;

			if(seq[i] <=300000)count++;
			if(count >= 300000-2) break;
		}
		int[] inv = new int[300001];
		for(int i = 0 ; i < seq.length; i++){
			if(seq[i] <= 300000){
				inv[seq[i]] = i;
			}
		}

		while(true){
			int n = Integer.parseInt(in.readLine());
			if(n == 0)break;
			out.println(String.format("The number %d appears in location %d.", n, inv[n]));
		}	

		out.close();
	}

	public static boolean[] sieve(int N){
		boolean[] isPrime = new boolean[N + 1];
		for (int i = 2; i <= N; i++) {
			isPrime[i] = true;
		}

		for (int i = 2; i*i <= N; i++) {
			if (isPrime[i]) {
				for (int j = i; i*j <= N; j++) {
					isPrime[i*j] = false;
				}
			}
		}
		return isPrime;
	}

	public static Area makePoly(double[][] points){
		Path2D.Double poly = new Path2D.Double();
		poly.moveTo(points[0][0], points[0][1]);
		for(int i=1;i<points.length;i++){
			poly.lineTo(points[i][0], points[i][1]);
		}
		poly.closePath();
		return new Area(poly);
	}

	//rotation
	//rotate area
	//Area rotRect = oriRect.createTransformedArea(AffineTransform.getRotateInstance(Math.toRadians(a)));
	//rotate point
	//Point rotated = AffineTransform.getRotateInstance(Math.toRadians(-45)).transform(point, null);

	public static double calcPolyArea(Area poly){
		PathIterator points = poly.getPathIterator(null);
		List<double[]> inPoints= new ArrayList<double[]>();
		while(!points.isDone()){
			double[] p = new double[2];
			switch(points.currentSegment(p)){
			case PathIterator.SEG_CLOSE:
				break;
			default:
				inPoints.add(p);
			}
			points.next();
		}

		double area = 0.0;
		for(int i=0;i<inPoints.size();i++){
			if(i==inPoints.size()-1){
				area += inPoints.get(i)[0]*inPoints.get(0)[1] - inPoints.get(0)[0]*inPoints.get(i)[1];
			}else{
				area += inPoints.get(i)[0]*inPoints.get(i+1)[1] - inPoints.get(i+1)[0]*inPoints.get(i)[1];
			}
		}
		area = Math.abs(area/2.0);
		return area;
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
