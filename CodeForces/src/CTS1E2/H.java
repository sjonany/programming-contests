package CTS1E2;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class H {
	static double min;
	static double ar1;
	static double ar2;
	
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		int test = 1;
		while(true){
			tk = tk(in.readLine());
			double x1 = Double.parseDouble(tk.nextToken());
			double y1 = Double.parseDouble(tk.nextToken());
			double x2 = Double.parseDouble(tk.nextToken());
			double y2 = Double.parseDouble(tk.nextToken());
			double x3 = Double.parseDouble(tk.nextToken());
			double y3 = Double.parseDouble(tk.nextToken());
			double x4 = Double.parseDouble(tk.nextToken());
			double y4 = Double.parseDouble(tk.nextToken());
			if(x1==0 && y1 == 0 &&
					x2==0 && y2 == 0 &&
					x3==0 && y3 == 0 &&
					x4==0 && y4 == 0) break;
			
			double[] v1 = new double[]{x1,y1};
			double[] v2 = new double[]{x2,y2};
			double[] v3 = new double[]{x3,y3};
			double[] v4 = new double[]{x4,y4};
			double[] m1 = new double[]{(x1+x2)/2, (y1+y2)/2};
			double[] m2 = new double[]{(x2+x3)/2, (y2+y3)/2};
			double[] m3 = new double[]{(x3+x4)/2, (y3+y4)/2};
			double[] m4 = new double[]{(x4+x1)/2, (y4+y1)/2};
			
			min = Double.MAX_VALUE;
			ar1 = 0.0;
			ar2 = 0.0;
			//two vertices
			go(makePts(v1,v2,v3), makePts(v3,v4,v1));
			go(makePts(v4,v1,v2), makePts(v2,v3,v4));
			
			//v and m
			go(makePts(v4,v1,m3), makePts(v1,v2,v3,m3));
			go(makePts(v1,v2,m2), makePts(m2,v3,v4,v1));
			
			go(makePts(v2,v3,m3), makePts(m3,v4,v1,v2));
			go(makePts(v2,v3,v4,m4), makePts(m4,v1,v2));
			
			go(makePts(v2,v3,m1), makePts(v3,v4,v1,m1));
			go(makePts(v3,m4,v1,v2), makePts(v3,v4,m4));

			go(makePts(m2,v3,v4), makePts(v4,v1,v2,m2));
			go(makePts(v4,v1,m1), makePts(m1,v2,v3,v4));
			
			//m and m
			go(makePts(m4,v1,m1), makePts(m1,v2,v3,v4,m4));
			go(makePts(m1,v2,m2), makePts(m2,v3,v4,v1,m1));
			go(makePts(m2,v3,m3), makePts(m2,v3,v4,v1,m1));
			go(makePts(m3,v4,m4), makePts(m4,v1,v2,v3,m3));
			go(makePts(v1,v2,m2,m4), makePts(m2,v3,v4,m4));
			go(makePts(v4,v1,m1,m3), makePts(m1,v2,v3,m3));
			
			out.println(String.format("Cake %d: %.3f %.3f", test, Math.min(ar1, ar2), Math.max(ar1,ar2)));
			test++;
		}
		
		out.close();
	}
	
	static void go(List<double[]> pts1, List<double[]> pts2){
		double a1 = calcPolyArea(makePoly(pts1));
		double a2 = calcPolyArea(makePoly(pts2));
		double diff = Math.abs(a1-a2);
		if(diff < min){
			min = diff;
			ar1 = a1;
			ar2 = a2;
		}
	}
	
	static List<double[]> makePts(double[]... pts){
		List<double[]> points = new ArrayList<double[]>();
		for(double[] pt : pts){
			points.add(pt);
		}
		return points;
	}
	
	public static Area makePoly(List<double[]> points){
		Path2D.Double poly = new Path2D.Double();
		poly.moveTo(points.get(0)[0], points.get(0)[1]);
		for(int i=1;i<points.size();i++){
			poly.lineTo(points.get(i)[0], points.get(i)[1]);
		}
		poly.closePath();
		return new Area(poly);
	}
	
	//rotation
	//rotate area
	//Area rotRect = oriRect.createTransformedArea(AffineTransform.getRotateInstance(Math.toRadians(a)));
	//rotate point
	//Point rotated = AffineTransform.getRotateInstance(Math.toRadians(-45)).transform(point, null);

	public static double calcPolyArea(Area poly){
		PathIterator points = poly.getPathIterator(null);
		List<double[]> inPoints= new ArrayList<double[]>();
		while(!points.isDone()){
			double[] p = new double[2];
			switch(points.currentSegment(p)){
				case PathIterator.SEG_CLOSE:
					break;
				default:
					inPoints.add(p);
			}
			points.next();
		}
		
		double area = 0.0;
		for(int i=0;i<inPoints.size();i++){
			if(i==inPoints.size()-1){
				area += inPoints.get(i)[0]*inPoints.get(0)[1] - inPoints.get(0)[0]*inPoints.get(i)[1];
			}else{
				area += inPoints.get(i)[0]*inPoints.get(i+1)[1] - inPoints.get(i+1)[0]*inPoints.get(i)[1];
			}
		}
		area = Math.abs(area/2.0);
		return area;
	}
	
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
