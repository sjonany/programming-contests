package CTS1E2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class A {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		while(true){
			String s1 = in.readLine();
			if(s1.equals("THEEND")){
				break;
			}
			String s2 = in.readLine();
			List<Integer> order = new ArrayList<Integer>();
			int count = 0;
			while(order.size() < s1.length()){
				for(char c = 'A'; c <= 'Z'; c++){
					for(int i = 0; i < s1.length(); i++){
						char cur = s1.charAt(i);
						if(cur == c){
							order.add(i);
						}
					}
				}
			}
			
			char[][] board = new char[s2.length() / s1.length()][s1.length()];
			int point = 0;
			for(int r = 0; r < s1.length(); r++){
				int c = order.get(r);
				for(int row = 0; row < s2.length() / s1.length(); row++){
					if(point == s2.length()) break;
					board[row][c] = s2.charAt(point++);
				}
			}
			StringBuilder sb = new StringBuilder();
			for(int r = 0 ; r < board.length; r++){
				for(int c = 0 ; c < board[0].length; c++){
					sb.append(board[r][c]);
				}	
			}
			out.println(sb.toString());
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
