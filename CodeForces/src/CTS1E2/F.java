package CTS1E2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class F {
	static int h;
	static int w;
	static boolean[][] board;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		int test = 1;
		while(true){
			tk = tk(in.readLine());
			h = Integer.parseInt(tk.nextToken());
			w = Integer.parseInt(tk.nextToken());

			if(h==0 && w==0)break;
			tk = tk(in.readLine());
			board = new boolean[h][w];
			int n = Integer.parseInt(tk.nextToken());
			for(int i = 0; i < n; i++){
				int r = Integer.parseInt(tk.nextToken());
				int c = Integer.parseInt(tk.nextToken());
				board[r][c] = true;
			}
			while(true){
				tk = tk(in.readLine());
				String command = tk.nextToken();
				if(command.equals("done")) break;
				int num = Integer.parseInt(tk.nextToken());
				if(command.equals("down")){
					down(num);
				}else if(command.equals("up")){
					up(num);
				}else if(command.equals("left")){
					left(num);
				}else {
					right(num);
				}
			}
			
			int[][] locs = new int[n][2];
			int loci = 0;
			for(int r = 0; r < h; r++){
				for(int c = 0; c < w; c++){
					if(board[r][c]){
						locs[loci][0] = r;
						locs[loci][1] = c;
						loci++;
					}
				}	
			}
			
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < locs.length; i++){
				sb.append("(" + locs[i][0] + "," + locs[i][1] + ")");
				if(i!=locs.length-1)sb.append(" ");
			}
			
			out.println(String.format("Data set %d ends with boxes at locations %s.", test, sb.toString()));
			test++;
		}

		out.close();
	}

	static int[] countCol(){
		int[] count = new int[w];
		for(int c = 0; c < w; c++){
			for(int r = 0; r < h; r++){
				if(board[r][c]) count[c]++;
			}
		}
		return count;
	}

	static int[] countRow(){
		int[] count = new int[h];
		for(int r = 0; r < h; r++){
			for(int c = 0; c < w; c++){
				if(board[r][c]) count[r]++;
			}
		}
		return count;
	}

	static int max(int[] arr){
		int m = 0;
		for(int i : arr){
			m = Math.max(m, i);
		}
		return m;
	}
	
	static void down(int num){
		int[] countCol = countCol();
		int maxBox = max(countCol);
		int move = Math.min(num, h-maxBox);
		
		for(int c = 0; c < w; c++){
			int numBoxRemoved = 0;
			for(int r = 0; r < move; r++){
				if(board[r][c]){
					numBoxRemoved++;
					board[r][c] = false;
				}
			}
			int curRow = move;
			while(numBoxRemoved > 0){
				if(!board[curRow][c]){
					board[curRow][c] = true;
					numBoxRemoved--;
				}
				curRow++;
			}
		}
	}

	static void up(int num){
		int[] countCol = countCol();
		int maxBox = max(countCol);
		int move = Math.min(num, h-maxBox);
		
		for(int c = 0; c < w; c++){
			int numBoxRemoved = 0;
			for(int r = 0; r < move; r++){
				if(board[h-r-1][c]){
					numBoxRemoved++;
					board[h-r-1][c] = false;
				}
			}
			int curRow = move;
			while(numBoxRemoved > 0){
				if(!board[h-curRow-1][c]){
					board[h-curRow-1][c] = true;
					numBoxRemoved--;
				}
				curRow++;
			}
		}
	}

	static void left(int num){
		int[] countRow = countRow();
		int maxBox = max(countRow);
		int move = Math.min(num, w-maxBox);
		
		for(int r = 0; r < h; r++){
			int numBoxRemoved = 0;
			for(int c = 0; c < move; c++){
				if(board[r][w-1-c]){
					numBoxRemoved++;
					board[r][w-1-c] = false;
				}
			}
			int curCol = move;
			while(numBoxRemoved > 0){
				if(!board[r][w-curCol-1]){
					board[r][w-curCol-1] = true;
					numBoxRemoved--;
				}
				curCol++;
			}
		}
	}

	static void right(int num){
		int[] countRow = countRow();
		int maxBox = max(countRow);
		int move = Math.min(num, w-maxBox);
		
		for(int r = 0; r < h; r++){
			int numBoxRemoved = 0;
			for(int c = 0; c < move; c++){
				if(board[r][c]){
					numBoxRemoved++;
					board[r][c] = false;
				}
			}
			int curCol = move;
			while(numBoxRemoved > 0){
				if(!board[r][curCol]){
					board[r][curCol] = true;
					numBoxRemoved--;
				}
				curCol++;
			}
		}
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
