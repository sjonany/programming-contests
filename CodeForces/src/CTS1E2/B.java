package CTS1E2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class B {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		while(true){
			tk = tk(in.readLine());
			int n = Integer.parseInt(tk.nextToken());
			int l = Integer.parseInt(tk.nextToken());
			int m = Integer.parseInt(tk.nextToken());
			
			if(n==0 && l==0 && m==0) break;
			
			Set<String> combs = new HashSet<String>();
			tk = tk(in.readLine());
			for(int i= 0 ;i < m; i++){
				combs.add(tk.nextToken());
			}
			String[] paths = combs.toArray(new String[0]);
			List<Integer>[] prevs = (List<Integer>[]) Array.newInstance(List.class, combs.size());
			for(int i = 0; i < prevs.length; i++){
				prevs[i] = new ArrayList<Integer>();
			}
			
			for(int i = 0 ;i  < paths.length; i++){
				for(int j = 0 ;j < paths.length; j++){
					boolean good = true;
					for(int k = 0; k < paths[i].length() - 1; k++){
						if(paths[i].charAt(k) != paths[j].charAt(k+1)){
							good = false;
							break;
						}
					}
					if(good) prevs[i].add(j);
				}	
			}
			
			//how many times to repeat rules to generate stream
			int numRep = l - paths[0].length() + 1;
			if(numRep < 1) {
				out.println(0);
				continue;
			}
			
			//num ways to get ending = paths.length, with k repetition
			int[][] dp = new int[paths.length][numRep+1];
			
			for(int ending = 0 ; ending < paths.length; ending++){
				dp[ending][1] = 1;
			}
			
			for(int rep = 2; rep <= numRep; rep++){
				for(int ending = 0; ending < paths.length; ending++){
					for(int prev : prevs[ending]){
						dp[ending][rep] += dp[prev][rep-1];
					}
				}
			}
			
			int ans = 0;
			for(int ending = 0 ; ending < paths.length; ending++){
				ans += dp[ending][numRep];
			}
			out.println(ans);
		}
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
