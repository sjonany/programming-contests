package CTS1E2;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;


public class IRedo {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		tk = tk (in.readLine());
		int n  = Integer.parseInt(tk.nextToken());
		int m  = Integer.parseInt(tk.nextToken());
		
		int[] arr = new int[n];
		//num to my left bigger than me
		int[] left = new int[n];
		//num to my right smaller than me
		int[] right = new int[n];
		
		for(int i = 0; i < m; i++){
			tk = tk(in.readLine());
			int a = Integer.parseInt(tk.nextToken());
			int b = Integer.parseInt(tk.nextToken());
			
			int min = Math.min(a, b);
			int max = Math.max(a, b);
			left[max]++;
			right[min]++;
		}
		
		for(int i = 0; i < n; i++){
			arr[i] = (i-left[i]) + right[i]; 
		}
		
		//valid pairs from 0 -> i
		List<Integer>[] prev = (List<Integer>[]) Array.newInstance(List.class, n);
		for(int i = 0 ; i <n; i++){
			prev[i] = new ArrayList<Integer>();
		}
		
		for(int r = 0; r < n; r++){
			//two conditions: left greater than me, or right smaller than me
			//max of all who doesn't satisfy right smaller than me, and so must
			//be satisfied by left greater than me
			int maxUnsatisfied = Integer.MIN_VALUE;
			for(int l = r-1; l >= 0; l--){
				if(arr[l] > arr[r]){
					//satisfy first condition, but can't be chosen
				}else{
					//IS
					if(arr[l] > maxUnsatisfied){
						//valid 
						prev[r].add(l);
					}
					maxUnsatisfied = Math.max(maxUnsatisfied, arr[l]);
				}
			}
			//can start only if everything > r
			if(maxUnsatisfied == Integer.MIN_VALUE){
				prev[r].add(-1);
			}
		}
		
		//number of valid LIS starting at -1, ending exactly at a[i]
		long[] dp = new long[n];
		long ans = 0;
		for(int i = 0; i < n; i++){
			for(int j : prev[i]){
				if(j == -1){
					dp[i]++;
				}else{
					dp[i] += dp[j];
				}
			}
			
			boolean end = true;
			for(int j = i+1; j < n;j++){
				if(arr[i] < arr[j]){
					end = false;
					break;
				}
			}
			if(end){
				ans += dp[i];
			}
		}
		
		out.println(ans);
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
