package CTS1E2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class J {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		while(true){
			String line = in.readLine();
			if(line == null)break;
			String s1 = line;
			String s2 = in.readLine();
			int[] count1 = count(s1);
			int[] count2 = count(s2);

			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < 26; i++){
				char c = (char)(i + 'a');
				while(count1[i] > 0 && count2[i] > 0){
					sb.append(c);
					count1[i]--;
					count2[i]--;
				}
			}
			out.println(sb.toString());
		}

		out.close();
	}

	static int[] count(String s){
		int[] count = new int[26];
		for(int i = 0 ; i < s.length(); i++){
			char c = s.charAt(i);
			count[c-'a']++;
		}
		return count;
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
