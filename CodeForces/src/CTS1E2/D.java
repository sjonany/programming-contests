package CTS1E2;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;


public class D {
	static List<Integer>[] edges;
	static int n;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		int test = 1;
		while(true){
			n = Integer.parseInt(in.readLine());
			if(n==0) break;
			double[][] pts = new double[n][2];
			tk = tk(in.readLine());
			for(int i = 0; i < n; i++){
				pts[i][0] = Double.parseDouble(tk.nextToken());
				pts[i][1] = Double.parseDouble(tk.nextToken());
			}
			edges = (List<Integer>[]) Array.newInstance(List.class, n);
			for(int i = 0; i < n; i++){
				edges[i] = new ArrayList<Integer>();
			}
			for(int i = 0; i < n ; i++){
				for(int j = i+1; j < n; j++){
					double dx = pts[i][0] - pts[j][0];
					double dy = pts[i][1] - pts[j][1];
					double d = dx*dx + dy*dy;
					if(d <= 400){
						edges[i].add(j);
						edges[j].add(i);
					}
				}
			}

			int good = -1;
			for(int numCol = 1; numCol <= 4; numCol++){
				if(colorable(numCol)){
					good = numCol;
					break;
				}
			}

			if(good == -1){
				good = 5;
			}

			out.println(String.format("The towers in case %d can be covered in %d frequencies.", test, good));
			test++;
		}

		out.close();
	}

	static boolean colorable(int numcol){
		goodcol = new boolean[numcol+1];
		int[] color = new int[n];
		color[0] = 1;
		for(int i = 1; i < n; i++){
			if(color[i] != 0) continue;
			if(!dfs(i,color, numcol))
				return false;
		}
		return true;
	}

	//pre: node is not colored
	//post: node is colored iff exist valid coloring
	static boolean[] goodcol;
	static boolean dfs(int node, int[] color, int maxcol){
		Arrays.fill(goodcol, true);
		for(int neigh : edges[node]){
			goodcol[color[neigh]] = false;
		}
	
		for(int col = 1; col <= maxcol; col++){
			if(goodcol[col]){
				boolean good = true;
				color[node] = col;
				for(int neigh : edges[node]){
					if(color[neigh] == 0){
						if(!dfs(neigh, color, maxcol)){
							good = false;
							break;
						}
					}
				}
				if(good){
					return true;
				}
			}
		}
		color[node] = 0;
		return false;
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
