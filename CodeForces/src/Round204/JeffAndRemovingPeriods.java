package Round204;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

// n log n offline solution to count distinct (subarray) 
// for n log n online, use persistent segment tree http://codeforces.com/blog/entry/10315#comment-157097
// http://codeforces.com/contest/351/submission/4673602
public class JeffAndRemovingPeriods {
	static int N;
	static int[] arr;
	static int Q;
	static Interval[] queries;
	static int MAX = 100005;

	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		N = Integer.parseInt(in.readLine());
		arr = new int[N + 1];
		tk = tk(in.readLine());
		for (int i = 1; i <= N; i++) {
			arr[i] = Integer.parseInt(tk.nextToken());
		}

		Q = Integer.parseInt(in.readLine());
		queries = new Interval[Q];
		for (int i = 0; i < Q; i++) {
			tk = tk(in.readLine());
			queries[i] = new Interval(i, Integer.parseInt(tk.nextToken()),
					Integer.parseInt(tk.nextToken()));
		}

		Arrays.sort(queries, new Comparator<Interval>() {
			@Override
			public int compare(Interval o1, Interval o2) {
				return o1.r - o2.r;
			}
		});

		// for each element, keep the rightmost one, then, each distinct element
		// will be counted exactly once
		Fenwick1 fen = new Fenwick1(N);
		// an element is good up until he is no longer arith spaced from the right
		// make a[i] = 1 for that element always, then put -1 on the left the moment it is no longer good
		Fenwick1 fen2 = new Fenwick1(N);
		int curr = 0;
		// one indexed. 0 means none yet
		int[] prev = new int[MAX];
		int[] pprev = new int[MAX];
		int[] ans = new int[Q];
		// 0 means no period yet
		int[] period = new int[MAX];
		// index for which the element's arith is broken
		int[] bad = new int[MAX];
		for (Interval query : queries) {
			if (query.r != curr) {
				while (curr < query.r) {
					curr++;
					int el = arr[curr];
					fen.increment(curr, 1);
					if (prev[el] > 0) {
						// reset the non-rightmost back to 0
						fen.increment(prev[el], -1);
					}
					
					if(prev[el] > 0) {
						int newperiod = curr - prev[el] + 1;
						// reset the good point back to 0
						fen2.increment(prev[el], -1);
						if(period[el] == newperiod) {
							// don't change last bad stopping point, it's still the same
						} else {
							period[el] = newperiod;
							if(bad[el] != 0) {
								// set the previous bad back to 0
								fen2.increment(bad[el], 1);
							}
							// make a new bad
							bad[el] = pprev[el];
							if(bad[el] != 0)
								fen2.increment(bad[el], -1);
						}
					}
					
					fen2.increment(curr, 1);
					pprev[el] = prev[el];
					prev[el] = curr;
				}
			}

			ans[query.id] = fen.getTotalFreq(query.r) - fen.getTotalFreq(query.l - 1);
			if(fen2.getTotalFreq(query.r) - fen2.getTotalFreq(query.l - 1) == 0) {
				ans[query.id]++;
			}
		}

		for (int qid = 0; qid < Q; qid++) {
			System.out.println(ans[qid]);
		}

		out.close();
	}

	static class Fenwick1 {
		int[] tree;

		public Fenwick1(int n) {
			tree = new int[n + 1];
		}

		public void increment(int i, int val) {
			while (i < tree.length) {
				tree[i] += val;
				i += (i & -i);
			}
		}

		public int getTotalFreq(int i) {
			int sum = 0;
			while (i > 0) {
				sum += tree[i];
				i -= (i & -i);
			}
			return sum;
		}
	}

	static class Interval {
		public int l;
		public int r;
		public int id;

		public Interval(int id, int l, int r) {
			this.id = id;
			this.l = l;
			this.r = r;
		}
	}

	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
