package Round204;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class JeffAndBrackets {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	static long MAX = Long.MAX_VALUE / 3;
	static int MAXBALANCE;
	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		tk = tk(in.readLine());
		
		int N = Integer.parseInt(tk.nextToken());
		int M = Integer.parseInt(tk.nextToken());
		
		int[] open = new int[N];
		int[] close = new int[N];
		
		tk = tk(in.readLine());
		for(int i=0;i<N;i++){
			open[i] = Integer.parseInt(tk.nextToken());
		
		}
		tk = tk(in.readLine());
		for(int i=0;i<N;i++){
			close[i] = Integer.parseInt(tk.nextToken());
		}

		MAXBALANCE = 2 * N;
		// min cost to convert from balance k to l in j moves if starting with ith modulo index
		long[][][][] init = new long[N][N+1][MAXBALANCE+1][MAXBALANCE+1];
		for(long[][][] i: init) {
			for(long[][] j : i) {
				for(long[] k : j) {
					Arrays.fill(k, MAX);
				}
			}
		}
		
		for(int mod = 0; mod < N; mod++) {
			for(int from = 0; from <= MAXBALANCE; from++) {
				init[mod][0][from][from] = 0;
			}
		}
		
		for(int move = 0; move < N; move++) {
			for(int mod = 0; mod < N; mod++) {
				for(int from = 0; from <= MAXBALANCE; from++) {
					for(int to = 0; to <= MAXBALANCE; to++) {
						if(to + 1 <= MAXBALANCE)
							init[(mod+1) % N][move+1][from][to+1] = Math.min(init[(mod+1) % N][move+1][from][to+1], 
									init[mod][move][from][to] + open[mod]);
						if(to - 1 >= 0)
							init[(mod+1) % N][move+1][from][to-1] = Math.min(init[(mod+1) % N][move+1][from][to-1], 
									init[mod][move][from][to] + close[mod]);
					}
				}
			}
		}
		
		// min cost to convert from balance i to j in N moves 
		long[][] mat = init[0][N];
		long[][] result = new long[2*N+1][2*N+1];
		long[][] temp = new long[2*N+1][2*N+1];
		
		// mat ^ k = min cost... in Nk moves
		exp(mat, result,  temp, M);
		System.out.println(result[0][0]);
	}
	
	static void exp(long[][] mat, long[][] result, long[][] temp, int pow) {
		if(pow == 1) {
			for(int i=0;i<result.length;i++) {
				result[i] = mat[i].clone();
			}
		} else if(pow % 2 == 0){
			exp(mat, temp, result, pow / 2);
			for(long[] i : result) {
				Arrays.fill(i, MAX);
			}
			mul(temp, temp, result);
		} else {
			exp(mat, temp, result, pow -1);
			for(long[] i : result) {
				Arrays.fill(i, MAX);
			}
			mul(mat, temp, result);
		}
	}
	
	static void mul(long[][] a, long[][] b, long[][] res) {
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a.length; j++) {
				for(int k = 0; k < a.length; k++) {
					res[i][k] = Math.min(res[i][k], a[i][j] + b[j][k]);
				}	
			}	
		}
	}
	
	static StringTokenizer tk (String s) {return new StringTokenizer(s);}
}
