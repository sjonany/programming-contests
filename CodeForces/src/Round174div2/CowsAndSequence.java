package Round174div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class CowsAndSequence {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = sc.nextInt();
		long sum = 0;
		int count = 1;
		
		BIT tree = new BIT(n+2);
		for(int i = 0; i < n; i++){
			int q = sc.nextInt();
			switch(q){
			case 1:
				int num = sc.nextInt();
				int val = sc.nextInt();
				tree.increment(1, val);
				tree.increment(num+1, -val);
				sum += val * num;
				break;
			case 2:
				val = sc.nextInt();
				tree.increment(count+1, val);
				tree.increment(count+2, -val);
				sum += val;
				count++;
				break;
			case 3:
				long last = tree.getTotalFreq(count);
				tree.increment(count, -last);
				tree.increment(count+1, last);
				sum -= last;
				count--;
				break;
			}
			out.println(1.0 * sum / count);
		}
		out.close();
	}
	
	public static class BIT{
		long[] tree;
		public BIT(int n){
			tree = new long[n+1];
		}

		//initArray[] is 0-based
		public BIT(long[] initArray){
			tree = new long[initArray.length+1];

			for(int i=0;i<initArray.length;i++){
				increment(i+1, initArray[i]);
			}
		}

		public void increment(int idx, long val){
			while(idx < tree.length){
				tree[idx] += val;
				idx += (idx & -idx);
			}
		}

		public long getTotalFreq(int from, int to){
			return getTotalFreq(to) - getTotalFreq(from-1);
		}

		public long getTotalFreq(int idx){
			long sum = 0;
			while(idx > 0){
				sum += tree[idx];
				idx -= (idx & -idx);
			}
			return sum;
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
