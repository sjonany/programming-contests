package Round174div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class CowProgram {
	//if u start here before doing step 2, y val, or -1
	static long[] prep2;
	//if u start here before doing step 3, y val
	static long[] prep3;
	static long[] a;
	static int n;
	static boolean[] isVisit2;//true is done step 2 here
	static boolean[] isVisit3;
	static boolean[] isPrep3Set;
	static boolean[] isPrep2Set;
	static boolean[] prep2Loop;//true if never end
	static boolean[] prep3Loop;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		n = sc.nextInt();
		a = new long[n+1];
		prep2 = new long[n+1];
		prep3 = new long[n+1];
		isVisit2 = new boolean[n+1];
		isVisit3 = new boolean[n+1];
		isPrep2Set = new boolean[n+1];
		isPrep3Set = new boolean[n+1];
		prep2Loop = new boolean[n+1];
		prep3Loop = new boolean[n+1];
		
		for(int i=2; i <= n; i++){
			a[i] = sc.nextInt();
		}
	
		for(int i=1; i <= n-1; i++){
			long val = getPrep3(1+i);
			if(prep3Loop[i+1]){
				out.println(-1);
			}else{
				out.println((long)i + val);
			}
		}
		
		out.close();
	}
	
	static long getPrep3(int xInit){
		if(isVisit3[xInit]){
			prep3Loop[xInit] = true;
			isPrep3Set[xInit] = true;
			isVisit3[xInit] = false;
			return Integer.MAX_VALUE;
		}
		isVisit3[xInit] = true;
		
		if(isPrep3Set[xInit]){
			isVisit3[xInit] = false;
			return prep3[xInit];
		}
		long x = (long)xInit - a[xInit];
		if(x <= 0 || x > n){
			prep3[xInit] = a[xInit];
		}else{
			long val = getPrep2((int)x);
			if(prep2Loop[(int)x]){
				prep3Loop[xInit] = true;
			}else{
				prep3[xInit] = a[xInit] + val;
			}
		}
		
		isPrep3Set[xInit] = true;
		isVisit3[xInit] = false;
		return prep3[xInit];
	}
	
	static long getPrep2(int xInit){
		if(isVisit2[xInit]){
			prep2Loop[xInit] = true;
			isPrep2Set[xInit] = true;
			isVisit2[xInit] = false;
			return Integer.MAX_VALUE;
		}
		isVisit2[xInit] = true;
		
		if(isPrep2Set[xInit]){
			isVisit2[xInit] = false;
			return prep2[xInit];
		}
		long x = (long)xInit + a[(int)xInit];
		if(x <= 0 || x > n){
			prep2[xInit] = a[(int)xInit];
		}else{
			long val = getPrep3((int)x);
			if(prep3Loop[(int)x]){
				prep2Loop[xInit] = true;
			}else{
				prep2[xInit] = a[xInit] + val;
			}
		}
		
		isPrep2Set[xInit] = true;
		isVisit2[xInit] = false;
		return prep2[xInit];
	}
	
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
