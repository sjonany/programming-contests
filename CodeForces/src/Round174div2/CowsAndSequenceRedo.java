package Round174div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class CowsAndSequenceRedo {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = sc.nextInt();
		long sum = 0;
		int count = 1;
		
		//diff[count] = difference between a[count] and a[count-1], index by 1, so a[0] = 0 no matter what
		int[] diff = new int[n+2];
		//a[count]
		int last = 0;
		
		for(int i = 0; i < n; i++){
			int q = sc.nextInt();
			switch(q){
			case 1:
				int num = sc.nextInt();
				int val = sc.nextInt();
				diff[1] += val;
				diff[num+1] -= val;
				if(num == count){
					last += val;
				}
				sum += val * num;
				break;
			case 2:
				val = sc.nextInt();
				sum += val;
				count++;
				diff[count] = val - last;
				last = val;
				break;
			case 3:
				sum -= last;
				last -= diff[count];
				count--;
				break;
			}
			out.println(1.0 * sum / count);
		}
		out.close();
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
