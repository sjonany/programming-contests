package Round174div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class CowsAndPokerGame {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		PrintWriter out = new PrintWriter(System.out);
		
		int n = sc.nextInt();
		int totA = 0;
		int totI = 0;
		int totF = 0;
		String s = sc.nextToken();
		for(int i = 0; i< s.length(); i++){
			char c = s.charAt(i);
			if(c == 'A')
				totA++;
			else if(c == 'I')
				totI++;
			else
				totF++;
		}
		int count = 0;
		for(int i = 0; i< s.length(); i++){
			char c = s.charAt(i);
			if(c != 'F'){
				if(totI == 0){
					count++;
				}else if(totI == 1 && c == 'I'){
					count++;
				}
			}
		}
		
		out.println(count);
		out.close();
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
