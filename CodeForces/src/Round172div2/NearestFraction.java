package Round172div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class NearestFraction{
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		long x = sc.nextInt();
		long y = sc.nextInt();
		long n = sc.nextInt();
		
		long num = 0;
		long den = 1;
		//System.out.println(distance);
		//System.out.println(goal);
		for(long i=1; i<=n; i++){
			long num1 = (long)((double)x * i / y) +1; 
			long num2 = num1 >= 0 ? num1-1 : num1;
			
			long d1 = Math.abs(x * i - y * num1);
			long d2 = Math.abs(x * i - y * num2);
			long bestNum;
			
			if(d1 == d2){
				bestNum = num2;
			}else{
				bestNum = d1 < d2 ? num1: num2;
			}
			
			//System.out.println(bestNum + "\\" + i + "->" + bestDist);
			if(Math.abs(x*i*den-bestNum*y*den) < Math.abs(x*den*i - num*y*i)){
				num = bestNum;
				den = i;
			}
		}
		
		System.out.println(num+"/"+den);
	}
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
