package Round172div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class RectanglePuzzle{
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		long w = sc.nextLong();
		long h = sc.nextLong();
		double a = sc.nextDouble();
		if(a > 90){
			a = 180-a;
		}
		
		if(a>89.9999999){
			System.out.println(w*w);
			return;
		}
		if(a<=0.00000000001){
			System.out.println(w*h);
			return;
		}
		
		if(h>w){
			long temp = h;
			h=w;
			w=temp;
		}
		
		a = Math.toRadians(a);
		
		Point UR = new Point();
		UR.x = (double)w/2;
		UR.y = (double)h/2;
		Point UL = new Point();
		UL.x = (double)-w/2;
		UL.y = (double)h/2;
	
		Point URr = UR.rotate(a);
		Point ULr = UL.rotate(a);
		double mUp = getSlope(URr.x, URr.y, ULr.x, ULr.y);
		double cUp = getIntercept(mUp, URr.x, URr.y);
		
		double alphaBound = 2*Math.atan(1.0*h/w);
		if(a > alphaBound){
			//case 2: 2 trapesiums
			Point tipTop = intersect(mUp, cUp, 0, (double)h/2);
			Point tipBottom = intersect(mUp, cUp, 0, (double)-h/2);
			double wTop = Math.abs(tipTop.x - (double)-w/2);
			double wBot = Math.abs(tipBottom.x - (double)-w/2);
			System.out.println(w*h - (wTop+wBot)* h);
			return;
		}
		//case 1: 4 triangles
		//upper triangle
		
		double ar1 = 0.0;
		Point tipTop = intersect(mUp,cUp, 0, (double)h/2);
		Point tipLeft = intersect(mUp,cUp, (double)-w/2);
		ar1 = Math.abs(tipTop.x - ((double)-w/2)) * Math.abs(tipLeft.y - (double)h/2) / 2.0;
		
		//lower triangle
		double ar2= 0.0;
		Point BL = new Point();
		BL.x = (double)-w/2;
		BL.y = (double)-h/2;
		Point BLr = BL.rotate(a);
		double mBot = getSlope(ULr.x, ULr.y, BLr.x, BLr.y);
		double cBot = getIntercept(mBot, ULr.x, ULr.y);
		
		Point tipRight = intersect(mBot,cBot, 0, (double)-h/2);
		Point tipBot = intersect(mBot,cBot, (double)-w/2);
		ar2 = Math.abs(tipRight.x - ((double)-w/2)) * Math.abs(tipBot.y - ((double)-h/2)) / 2.0;
		
		//done
		
		System.out.println(w*h - 2*(ar1+ar2));
	}
	
	public static double getIntercept(double slope, double x1, double y1){
		return y1 - slope*x1;
	}
	
	public static double getSlope(double x1, double y1, double x2, double y2){
		return (y2-y1)/(x2-x1);
	}
	
	public static Point intersect(double m1, double c1, double x){
		Point p = new Point();
		p.x = x;
		p.y = m1*x+c1;
		return p;
	}
	
	public static Point intersect(double m1, double c1, double m2, double c2){
		Point result = new Point();
		result.x = (c2-c1)/(m1-m2);
		result.y = m1*result.x+ c1;
	
		return result;
	}
	
	
	public static class Point{
		public double x;
		public double y;
		
		public Point rotate(double a){
			Point p = new Point();
			p.x = x*Math.cos(a) - y*Math.sin(a);
			p.y = x*Math.sin(a) + y*Math.cos(a);
			return p;
		}
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
