package Round172div2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class GameOnTree {
	/*
	 X = numMoves = sum i=1->n, Xi
	 Pr(Xi=1) = sum step=1->infinity {Pr(Xi=1 | Xi's fate is decided exactly on step k)Pr(Xi...on step k)}
	 assuming root is of depth = 0, Pr(Xi=1|Xi's fate decided on step k) = 1/(d+1), 
	 there are d+1 ways to end the game, only 1 results in Xi = 1
	 Pr(Xi=1) = 1/(d+1) * 1 = 1/(d+1)
	 lin of exp -> E[X] = sum for i=1->n, d(i)
	 */
	
	static double exp = 0;
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		Map<Integer, List<Integer>> edges = new HashMap<Integer, List<Integer>>();
		
		for(int i=1;i<=n;i++){
			edges.put(i, new ArrayList<Integer>());
		}
		
		for(int i=1;i<=n-1;i++){
			int v1 = sc.nextInt();
			int v2 = sc.nextInt();
			edges.get(v1).add(v2);
			edges.get(v2).add(v1);
		}
		dfs(-1,1,0, edges);
		System.out.println(exp);
	}
	
	public static void dfs(int parent, int cur, int depth, Map<Integer, List<Integer>> edges){
		exp += 1.0/(depth+1);
		for(Integer next : edges.get(cur)){
			if(next != parent){
				dfs(cur, next, depth+1, edges);
			}
		}
	}
	
	public static class FastScanner{
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
