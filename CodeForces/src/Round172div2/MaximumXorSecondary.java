package Round172div2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class MaximumXorSecondary{
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int n = sc.nextInt();
		int[] a = new int[n];
		for(int i=0; i<n; i++){
			a[i] = sc.nextInt();
		}

		//second... first pattern
		int maxFirst = getMaxXor(a);
		
		int[] rev = new int[n];
		for(int i=0;i<n;i++){
			rev[i] = a[n-i-1];
		}
		//first...second pattern
		System.out.println(Math.max(maxFirst, getMaxXor(rev)));
		
	}
	
	public static int getMaxXor(int[] a){
		Stack<Integer> s = new Stack<Integer>();
		int max = -1;
		s.add(a[0]);
		//proof: assume exist a second..first pair that is not handled
		//if second is still in stack -> that means exist second.. x...first such that x > second, that's why
		//second is not popped. But this contradicts the fact that everybody in between first and second are smaller than them both
		//or, if second is no longer in stack, that means an element x > second has already caused second to pop, and x comes before first
		//then, second .. first is not a valid second-first pair
		//second...first pattern
		for(int i=1;i<a.length;i++){
			while(!s.isEmpty() && s.peek() < a[i]){
				max = Math.max(max,s.pop() ^ a[i]);
			}
			s.push(a[i]);
		}
		return max;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
