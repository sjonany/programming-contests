package Round172div2;

import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class RectanglePuzzleRedo{
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		long w = sc.nextLong();
		long h = sc.nextLong();
		double a = sc.nextDouble();
		
		Area oriRect = makePoly(Arrays.asList(new double[][]{{w/2.0,h/2.0},{-w/2.0,h/2.0},{-w/2.0,-h/2.0},{w/2.0,-h/2.0}}));
		Area rotRect = oriRect.createTransformedArea(AffineTransform.getRotateInstance(Math.toRadians(a)));
		
		rotRect.intersect(oriRect);
		System.out.println(calcPolyArea(rotRect));
	}
	
	public static Area makePoly(List<double[]> points){
		Path2D.Double poly = new Path2D.Double();
		poly.moveTo(points.get(0)[0], points.get(0)[1]);
		for(int i=1;i<points.size();i++){
			poly.lineTo(points.get(i)[0], points.get(i)[1]);
		}
		poly.closePath();
		return new Area(poly);
	}
	
	public static double calcPolyArea(Area poly){
		PathIterator points = poly.getPathIterator(null);
		List<double[]> inPoints= new ArrayList<double[]>();
		while(!points.isDone()){
			double[] p = new double[2];
			switch(points.currentSegment(p)){
				case PathIterator.SEG_CLOSE:
					break;
				default:
					inPoints.add(p);
			}
			points.next();
		}
		
		double area = 0.0;
		for(int i=0;i<inPoints.size();i++){
			if(i==inPoints.size()-1){
				area += inPoints.get(i)[0]*inPoints.get(0)[1] - inPoints.get(0)[0]*inPoints.get(i)[1];
			}else{
				area += inPoints.get(i)[0]*inPoints.get(i+1)[1] - inPoints.get(i+1)[0]*inPoints.get(i)[1];
			}
		}
		area = Math.abs(area/2.0);
		return area;
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
