package pingPractice2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class G {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		while(line != null){
			Map<Character, List<Integer>> edges = new HashMap<Character, List<Integer>>();
			for(char i='a'; i<='z';i++){
				edges.put(i, new ArrayList<Integer>());
			}

			int[][] adjMap = new int[26][26];
			for(int i=0;i<26;i++){
				for(int j=0;j<26;j++){
					adjMap[i][j] = Integer.MAX_VALUE;
				}
			}
			
			int totalCost = 0;
			while(!line.trim().equals("deadend")){
				char from = line.charAt(0);
				char to = line.charAt(line.length()-1);
				int cost = line.length();
				totalCost += cost;
				edges.get(from).add(cost);
				edges.get(to).add(cost);
				adjMap[from-'a'][to-'a'] = cost;
				adjMap[to-'a'][from-'a'] = cost;
				line = reader.readLine();
			}	
			
			List<Character> badNodes = new ArrayList<Character>();
			for(char i='a'; i<='z';i++){
				if(edges.get(i).size() %2 ==1){
					badNodes.add(i);
				}
			}
			
			if(badNodes.size()!=0){
				int[] sp = shortestPath((badNodes.get(0)-'a'), adjMap);
				totalCost += sp[badNodes.get(1)-'a'];
			}
			System.out.println(totalCost);
			line = reader.readLine();
		}
	}
	
	public static int[] shortestPath(int v, int[][]adjMap){
		int[] distance = new int[26];
		boolean[] visited = new boolean[26];
		for(int i=0;i<26;i++){
			distance[i] = Integer.MAX_VALUE;
		}
		
		distance[v] = 0;
		int countVisit = 0;
		
		while(countVisit < 26){
			int closestV = 0;
			int minDistance = Integer.MAX_VALUE;
			for(int i=0;i<26;i++){
				if(!visited[i] && distance[i] < minDistance){
					minDistance = distance[i];
					closestV = i;
				}
			}
			visited[closestV] = true;
			countVisit++;
			for(int i=0;i<26;i++){
				if(adjMap[closestV][i] != 0){
					if(adjMap[closestV][i] != Integer.MAX_VALUE)
					if(distance[closestV] + adjMap[closestV][i] < distance[i]){
						distance[i] = distance[closestV] + adjMap[closestV][i];
					}
				}
			}
		}
		
		return distance;
	}
}
