package pingPractice2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;


public class A {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		while(line != null){
			int[] cur = new int[7];
			for(int i=0;i<line.length();i++){
				cur[line.charAt(i)-'0']++;
			}
			boolean isAMove = line.length()%2 == 0;
			if(isGood(cur)){
				if(isAMove){
					System.out.println(line + " A");
				}else{
					System.out.println(line + " B");
				}
			}else{
				if(!isAMove){
					System.out.println(line + " A");
				}else{
					System.out.println(line + " B");
				}
			}
			line = reader.readLine();
		}
	}
	
	public static boolean isGood(int[] num){
		int total = 0;
		int minMove = 7;
		for(int i=1;i<num.length;i++){
			if(num[i]!=4){minMove = Math.min(minMove,i);}
			total += num[i] * i;
		}
		
		if(total > 31){
			return false;
		}
		
		for(int i=1;i<=6;i++){
			if(num[i] != 4 && total + i <= 31){
				num[i]++;
				boolean isGood = isGood(num);
				num[i]--;
				if(!isGood){
					return true;
				}
			}
		}
		return false;
	}
}
