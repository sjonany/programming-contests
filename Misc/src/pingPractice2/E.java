package pingPractice2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class E {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		Set<String> subs = new TreeSet<String>();
		List<String> s = new ArrayList<String>();
		while(line != null && line.trim().length()!=0){
			s.add(line);
			line = reader.readLine();
		}
		
		Collections.sort(s, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.length()-o2.length();
			}
		});
		
		List<String> res = new ArrayList<String>();
		for(String str : s){
			for(int i=1;i<str.length();i++){
				if(subs.contains(str.substring(i)) && subs.contains(str.substring(0, i))){
					res.add(str);
					break;
				}
			}
			subs.add(str);
		}
		
		Collections.sort(res);
		StringBuffer out = new StringBuffer();
		for(String str : res){
			out.append(str + "\n");
		}
		System.out.print(out);
	}
	
	public static boolean isGood(int[] num){
		int total = 0;
		int minMove = 7;
		for(int i=1;i<num.length;i++){
			if(num[i]!=4){minMove = Math.min(minMove,i);}
			total += num[i] * i;
		}
		
		if(total > 31){
			return false;
		}
		
		for(int i=1;i<=6;i++){
			if(num[i] != 4 && total + i <= 31){
				num[i]++;
				boolean isGood = isGood(num);
				num[i]--;
				if(!isGood){
					return true;
				}
			}
		}
		return false;
	}
}
