package pingPractice2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class F {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		int T = Integer.parseInt(line);
		for(int i=0;i<T;i++){
			StringTokenizer tkz = new StringTokenizer(reader.readLine());
			int M  = Integer.parseInt(tkz.nextToken());
			int N  = Integer.parseInt(tkz.nextToken());
			int Q  = Integer.parseInt(tkz.nextToken());
			
			char[][] board = new char[M][N];
			for(int r= 0;r<M;r++){
				line = reader.readLine();
				for(int c=0;c<N;c++){
					board[r][c] = line.charAt(c);
				}
			}
			StringBuffer out = new StringBuffer();
			out.append(M + " " + N + " " + Q + "\n");
			for(int q=0;q<Q;q++){
				tkz = new StringTokenizer(reader.readLine());
				int R = Integer.parseInt(tkz.nextToken());
				int C = Integer.parseInt(tkz.nextToken());
				char target = board[R][C];
				int maxSize = 1;
				int s = 1;
				for(;;s++){
					boolean good = true;
					int ulR = R - s;
					int ulC = C - s;
					int brR = R + s;
					int brC = C + s;
					if(ulR <0 || ulC < 0 || brR >= M || brC >=N){
						break;
					}
					
					for(int c = ulC; c<=brC && good;c++){
						if(board[ulR][c] != target){
							good = false;
							break;
						}
					}
					
					for(int c = ulC; c<=brC && good;c++){
						if(board[brR][c] != target){
							good = false;
							break;
						}
					}
					
					for(int r = ulR; r<=brR && good;r++){
						if(board[r][ulC] != target){
							good = false;
							break;
						}
					}
					
					for(int r = ulR; r<=brR && good;r++){
						if(board[r][brC] != target){
							good = false;
							break;
						}
					}
					
					if(!good)
						break;
					maxSize = Math.max(s*2+1, maxSize);
				}
				out.append(maxSize+"\n");
			}
			System.out.print(out);
		}
	}
	
	public static boolean isGood(int[] num){
		int total = 0;
		int minMove = 7;
		for(int i=1;i<num.length;i++){
			if(num[i]!=4){minMove = Math.min(minMove,i);}
			total += num[i] * i;
		}
		
		if(total > 31){
			return false;
		}
		
		for(int i=1;i<=6;i++){
			if(num[i] != 4 && total + i <= 31){
				num[i]++;
				boolean isGood = isGood(num);
				num[i]--;
				if(!isGood){
					return true;
				}
			}
		}
		return false;
	}
}
