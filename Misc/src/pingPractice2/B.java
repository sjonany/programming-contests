package pingPractice2;
import java.awt.Polygon;
import java.awt.geom.PathIterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class B {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		int T = Integer.parseInt(line);
		for(int t=0;t<T;t++){
			int n = Integer.parseInt(reader.readLine());
			Polygon poly = new Polygon();
			for(int i=0;i<n;i++){
				StringTokenizer tkz = new StringTokenizer(reader.readLine());
				int x = Integer.parseInt(tkz.nextToken());
				int y = Integer.parseInt(tkz.nextToken());
				poly.addPoint(x, y);
			}
			PathIterator path = poly.getPathIterator(null);
			for(int i=0;i<n+1;i++){
				double[] coords = new double[4];
				path.currentSegment(coords);
				path.next();
			}
		}
	}
	
	public static boolean isGood(int[] num){
		int total = 0;
		int minMove = 7;
		for(int i=1;i<num.length;i++){
			if(num[i]!=4){minMove = Math.min(minMove,i);}
			total += num[i] * i;
		}
		
		if(total > 31){
			return false;
		}
		
		for(int i=1;i<=6;i++){
			if(num[i] != 4 && total + i <= 31){
				num[i]++;
				boolean isGood = isGood(num);
				num[i]--;
				if(!isGood){
					return true;
				}
			}
		}
		return false;
	}
}
