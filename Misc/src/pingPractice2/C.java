package pingPractice2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


public class C {
	static int count = 0;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		
		while(!line.equals("0")){
			int numHand = Integer.parseInt(line);
			count = 0;
			Node root = new Node();
			for(int i=0;i<numHand;i++){
				StringTokenizer tkz  = new StringTokenizer(reader.readLine());
				Integer numCard = Integer.parseInt(tkz.nextToken());
				String[] hand = new String[numCard];
				for(int j=0; j<numCard;j++){
					hand[j] = tkz.nextToken();
				}
				add(root, hand, numCard-1);
			}

			System.out.println(count);
			line = reader.readLine();
		}
	}
	
	public static void add(Node root, String[] hand, int curIndex){
		if(curIndex != -1){
			if(root.children.containsKey(hand[curIndex])){
				add(root.children.get(hand[curIndex]), hand, curIndex-1);
			}else{
				Node next = new Node();
				root.children.put(hand[curIndex], next);
				count++;
				add(next, hand, curIndex-1);
			}
		}
	}
	
	static class Node{
		public Map<String, Node> children;
		public Node(){
			this.children = new HashMap<String, Node>();
		}
	}
}
