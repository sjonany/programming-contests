import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;


public class ParallelSumThread {
	public static void main(String[] args) throws Exception{
		int[] arr = createRandomArray(1000);
		long parAns = sumParallel(arr);
		long seqAns = sumNormal(arr);
		System.out.printf("parallel=%d sequential=%d sumFj=%d\n", parAns, seqAns, sumFj(arr));
	}
	
	public static int[] createRandomArray(int n){
		int[] arr = new int[n];
		for(int i = 0; i < n; i++){
			arr[i] = (int) (Math.random() * 100);
		}
		return arr;
	}
	
	static ForkJoinPool fjPool = new ForkJoinPool();
	public static long sumFj(int[] arr){
		return fjPool.invoke(new SumArray(0,arr.length-1, arr));
	}
	
	static class SumArray extends RecursiveTask<Long>{
		private static final int CUTOFF = 9;
		private int lo;
		private int hi;
		private int[] arr;
		
		public SumArray(int lo, int hi, int[] arr){
			this.lo = lo;
			this.hi = hi;
			this.arr = arr;
		}
		
		@Override
		protected Long compute() {
			if(hi - lo < CUTOFF){
				long sum = 0;
				for(int i = lo; i <= hi; i++){
					sum += arr[i];
				}
				return sum;
			}else{
				int mid = (lo+hi)/2;
				SumArray other = new SumArray(lo,mid,arr);
				other.fork();
				SumArray cur = new SumArray(mid+1,hi, arr);
				return cur.compute() + other.join();
			}
		}
		
	}
	
	public static long sumNormal(int[] arr){
		long sum = 0;
		for(int i : arr){
			sum += i;
		}
		return sum;
	}

	public static long sumParallel(int[] arr){
		SumThread t = new SumThread(0,arr.length-1,arr);
		t.run();
		return t.sum;
	}
	
	public static class SumThread extends java.lang.Thread{
		private static final int CUTOFF = 9;
		
		public long sum;
		public int lo;
		public int hi;
		public int[] arr;
		
		public SumThread(int lo, int hi, int[] arr){
			this.sum = 0;
			this.lo = lo;
			this.hi = hi;
			this.arr = arr;
		}
		
		@Override
		public void run(){
			if(hi - lo < CUTOFF){
				for(int i = lo; i <= hi; i++){
					sum += arr[i];
				}
			}else{
				int mid = (lo+hi)/2;
				SumThread other = new SumThread(lo,mid, arr);
				other.start();
				this.lo = mid+1;
				this.run();
				try {
					other.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.sum += other.sum;
			}
		}
	}
}
