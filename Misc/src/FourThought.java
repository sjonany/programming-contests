import java.util.ArrayList;
import java.util.List;


public class FourThought {
	public static void main(String[] args){
		List<Integer> operands = new ArrayList<Integer>();
		operands.add(1);
		operands.add(2);
		operands.add(3);
		operands.add(4);
		List<String> operations = new ArrayList<String>();
		operations.add("x");
		operations.add("-");
		operations.add("/");
		
		System.out.println(solve(operands, operations));
	}
	
	static int solve(List<Integer> args, List<String> ops){
		if(ops.size() == 0){
			return args.get(0);
		}else{
			System.out.println(args);
			System.out.println(ops);
			int importantIndex = -1;
			for(int i = 0; i < ops.size(); i++){
				if(ops.get(i).equals("x") || ops.get(i).equals("/")){
					importantIndex = i;
					break;
				}
			}
			
			if(importantIndex == -1){
				importantIndex = 0;
			}
			
			int left = args.get(importantIndex);
			int right = args.get(importantIndex+1);
			int result = 0;
			String op = ops.get(importantIndex);
			if(op.equals("+")){
				result = left + right;
			}else if(op.equals("-")){
				result = left - right;
			}else if(op.equals("x")){
				result = left * right;
			}else {
				result = left / right;
			}
			
			args.remove(importantIndex);
			args.remove(importantIndex);
			args.add(importantIndex, result);
			ops.remove(importantIndex);
			return solve(args, ops);
		}
	}
	
}
