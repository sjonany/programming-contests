
public class ReverseLinkedList {
	public static void main(String[] args){
		int[] arr = new int[]{1,2};
		Node head = fromArrayToLinkedList(arr);
		//printList(head);
		Node tail = reverse(head);
		printList(tail);
	}
	
	public static Node reverse(Node head){
		Node cur = head;
		Node next = head.next;

		if(next == null){
			return head;
		}
		
		Node nextnext = head.next.next;
		cur.next = null;
		
		while(nextnext != null){
			next.next = cur;
			cur = next;
			next = nextnext;
			nextnext = nextnext.next;
		}
		
		next.next = cur;
		return next;
	}
	
	public static void printList(Node node){
		Node cur = node;
		while(cur != null){
			System.out.print(cur.id  + " " );
			cur = cur.next;
		}
	}
	
	public static Node fromArrayToLinkedList(int[] arr){
		Node head = new Node();
		head.id = arr[0];
		Node cur = head;
		for(int i=1; i<arr.length; i++){
			Node next = new Node();
			next.id = arr[i];
			cur.next = next;
			cur = next;
		}
		
		return head;
	}
	
	public static class Node{
		public int id;
		public Node next;
	}
}
