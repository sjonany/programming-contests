
public class fact {
	/*
	 first remove 2's and 5's
	 since there can be no more zeroes
	 it's enough to just keep multiplying only the -last- digits of the remaining facs
	 
	 
	 */
	static int[] dataMul;
	static int[] dataAdd;
	public static void main(String[] args){
		dataMul = new int[10];
		dataAdd = new int[10];
		update(0,10,3);
		System.out.println(q(0));
	}
	private static void update(int left, int right, int by) {
	    internalUpdate(left, by, -by * (left - 1));
	    internalUpdate(right, -by, by * right);
	}

	private static void internalUpdate(int at, int mul, int add) {
	    while (at < dataMul.length) {
	        dataMul[at] += mul;
	        dataAdd[at] += add;
	        at |= (at + 1);
	    }
	}

	private static int q(int at) {
	    int mul = 0;
	    int add = 0;
	    int start = at;
	    while (at >= 0) {
	        mul += dataMul[at];
	        add += dataAdd[at];
	        at = (at & (at + 1)) - 1;
	    }
	    return mul * start + add;
	}
	
	
	//number of pos ints <= N who ends with X
	static int mine(int N, int X){
		return N/10 + 1;
		/*
		int numDig = 0;
		int leadDig = -1;
		while(N > 0){
			leadDig = N;
			N/=10;
			numDig++;
		}
		
		if(numDig >= 3){
			((int)Math.pow(9, numDig-2)) * (leadDig-1) ;
		}*/
	}
	
	static int cal(int N, int X){
        if(N == 0)
            return 0;
        int extra = N%10>=X ? 1 : 0;
        return N / 10 + extra + cal(N / 5, X);
	}

    static int getLastDigitWithX(int N, int X){
	    if(N == 0)
	            return 0;
	    return getLastDigitWithX(N / 2, X) + cal(N, X);
    }
}
