package pingPractice3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class C {
	public static void main(String[] args) throws NumberFormatException, IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(reader.readLine());
		
		int[] countRight = new int[1000001];
		long total = 0;
		for(int i=0;i<N;i++){
			StringTokenizer tkz = new StringTokenizer(reader.readLine());
			int q = Integer.parseInt(tkz.nextToken());
			int a = Integer.parseInt(tkz.nextToken());
			int x = Integer.parseInt(tkz.nextToken());
			if(a == 1){
				long add = countRight[q]*10;
				if(x == 1){
					total += add + 40;
				}else{
					total += add + 20;
				}
				countRight[q] ++;
			}else{
				total += 10;
			}
		}
		System.out.println(total);
	}
}
