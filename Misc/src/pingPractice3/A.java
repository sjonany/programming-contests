package pingPractice3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;


public class A {
	static Set<String> results;
	public static void main(String[] args) throws NumberFormatException, IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		results = new HashSet<String>();
		StringTokenizer tkz = new StringTokenizer(reader.readLine());
		int N = Integer.parseInt(tkz.nextToken());
		List<Point> points = new ArrayList<Point>();
		int minX = 1000;
		int minY = 1000;
		for(int i=0;i<N;i++){
			if(!tkz.hasMoreTokens()){
				tkz = new StringTokenizer(reader.readLine());
			}
			int x = Integer.parseInt(tkz.nextToken());
			if(!tkz.hasMoreTokens()){
				tkz = new StringTokenizer(reader.readLine());
			}
			int y = Integer.parseInt(tkz.nextToken());
			points.add(new Point(x,y));
			minX = Math.min(minX, x);
			minY = Math.min(minY, y);
		}

		for(int i=0;i<N;i++){
			Point p = points.get(i);
			p.x = p.x -minX;
			p.y = p.y -minY;
			if(p.x >= 300 || p.y >= 300){
				kill();
				return;
			}
		}
		
		for(int xOff1=0;xOff1<=99;xOff1++){
			for(int yOff1=0;yOff1<=99;yOff1++){
				List<Point> converted = new ArrayList<Point>();
				int maxConvX=-1;
				int maxConvY=-1;
				boolean bad = false;
				//StringBuffer key = new StringBuffer();
				for(Point p : points){
					int convX = (p.x+xOff1) / 100;
					int convY = (p.y+yOff1) / 100;
				
					converted.add(new Point(convX, convY));
					maxConvX = Math.max(maxConvX, convX);
					maxConvY = Math.max(maxConvY, convY);
					
					if(convX >3 || convY > 3){
						bad = true;
						break;
					}
					//key.append(convX + "" + convY);
				}
				if(bad)
					continue;
				
				for(int xOff=0;xOff<=2-maxConvX;xOff++){
					for(int yOff=0;yOff<=2-maxConvY;yOff++){
						StringBuffer cur = new StringBuffer();
						for(Point convP : converted){
							cur.append((convP.x+1+xOff) + (convP.y+yOff)*3);
						}
						results.add(cur.toString());
					}
				}
			}
		}
		
		List<String> sorted = new ArrayList<String>();
		for(String s : results){
			sorted.add(s);
		}
		
		Collections.sort(sorted);
		StringBuffer out = new StringBuffer();
		for(int i=0;i<sorted.size();i++){
			if(i!=sorted.size()-1){
				out.append(sorted.get(i) + "\n" );
			}else{
				out.append(sorted.get(i));
			}
		}
		System.out.println(out);
	}
	
	public static void solve(List<Point> points){
		List<Point> converted = new ArrayList<Point>();
		int maxConvX=-1;
		int maxConvY=-1;
		//StringBuffer key = new StringBuffer();
		for(Point p : points){
			int convX = p.x / 100;
			int convY = p.y / 100;
			converted.add(new Point(convX, convY));
			maxConvX = Math.max(maxConvX, convX);
			maxConvY = Math.max(maxConvY, convY);
			//key.append(convX + "" + convY);
		}
		
		List<String> results = new ArrayList<String>();
		for(int xOff=0;xOff<=2-maxConvX;xOff++){
			for(int yOff=0;yOff<=2-maxConvY;yOff++){
				StringBuffer cur = new StringBuffer();
				for(Point convP : converted){
					cur.append((convP.x+1+xOff) + (convP.y+yOff)*3);
				}
				results.add(cur.toString());
			}
		}
	}
	
	public static void kill(){
		System.out.println("NONE");
	}
	
	public static class Point{
		public int x;
		public int y;
		public Point(int x, int y){
			this.x = x;
			this.y = y;
		}
	}
}
