package pingPractice3;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class G {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(reader.readLine());
		for(int t=1;t<=T;t++){
			solve(t, reader);
		}
	}
	
	static void solve(int t, BufferedReader reader) throws Exception{
		reader.readLine();
		int n = Integer.parseInt(reader.readLine());
		int total = 0;
		int[][] adj = new int[n][n];
		for(int i=0;i<n;i++){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			for(int j=0;j<n;j++){
				adj[i][j] = intt(tk.nextToken());
				total += adj[i][j];
			}
		}
		
		List<Edge>[] adjMap = (List<Edge>[]) Array.newInstance(ArrayList.class, n);
		for(int i=0;i<n;i++){
			adjMap[i] = new ArrayList<Edge>();
		}
		for(int i=0;i<n;i++){
			for(int j=i+1;j<n;j++){
				if(i!=j){
					if(adj[i][j] == 0){
						if(adj[j][i] != 0){
							adjMap[i].add(new Edge(j, adj[j][i]));
							adjMap[j].add(new Edge(i, adj[j][i]));
						}
					}else{
						if(adj[j][i] != 0){
							adjMap[i].add(new Edge(j, Math.min(adj[j][i], adj[i][j])));
							adjMap[j].add(new Edge(i, Math.min(adj[j][i], adj[i][j])));
						}else{
							adjMap[i].add(new Edge(j, adj[i][j]));
							adjMap[j].add(new Edge(i, adj[i][j]));
						}
					}
				}
			}
		}
		
		int[] distance = new int[n];
		boolean[] visited = new boolean[n];
		for(int i=0;i<n;i++){
			distance[i] = Integer.MAX_VALUE;
		}
		
		distance[0] = 0;
		int countVisit = 0;
		int mstWeight = 0;
		
		while(countVisit < n){
			int closestV = -1;
			int minDistance = Integer.MAX_VALUE;
			for(int i=0;i<n;i++){
				if(!visited[i] && distance[i] < minDistance){
					minDistance = distance[i];
					closestV = i;
				}
			}
			
			if(closestV == -1){
				break;
			}
			mstWeight += minDistance;
			visited[closestV] = true;
			countVisit++;
			for(Edge e : adjMap[closestV]){
				if(e.weight < distance[e.to]){
					distance[e.to] = e.weight;
				}
			}
		}
		
		if(countVisit != n){
			System.out.println("Case " + t + ": -1");
		}else{
			System.out.println("Case " + t + ": " + (total - mstWeight));
		}
	}

	static class Edge{
		public int to;
		public int weight;
		
		public Edge(int to, int w){
			this.to = to;
			this.weight = w;
		}
	}
	
	static int intt(String s){
		return Integer.parseInt(s);
	}
}
