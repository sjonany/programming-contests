package pingPractice3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class F {
	public static void main(String[] args) throws NumberFormatException, IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String commandline = reader.readLine();
		List<String> argList = new ArrayList<String>();
		String line = reader.readLine();
		while(line != null){
			argList.add(line);
			line = reader.readLine();
		}
		
		//is prev = %
		boolean isCommanded = false;
		StringBuffer out = new StringBuffer();
		int argIndex = 0;
		for(int i=0;i<commandline.length();i++){
			char cur = commandline.charAt(i);
			if(isCommanded){
				if(cur == '%'){
					out.append("%");
				}else if(cur == 's'){
					if(argIndex == argList.size()){
						error();
						return;
					}
					out.append(argList.get(argIndex));
					argIndex++;
				}else if(cur == 'd'){
					if(argIndex == argList.size()){
						error();
						return;
					}
					String arg = argList.get(argIndex);
					if(arg.length() ==0){
						error();
						return;
					}
					
					for(int j=0;j<arg.length();j++){
						if(!Character.isDigit(arg.charAt(j))){
							error();
							return;
						}
					}
					int start = 0;
					for(;start<arg.length();start++){
						if(arg.charAt(start) != '0'){
							break;
						}
					}
					if(start == arg.length()){
						out.append(0);
					}else
						out.append(arg.substring(start));
					argIndex++;
				}else{
					error();
					return;
				}
				isCommanded = false;
			}else{
				if(cur == '%'){
					isCommanded = true;
				}else{
					out.append(cur);
				}
			}
		}
		if(isCommanded || argIndex != argList.size()){
			error();
			return;
		}
		System.out.println(out);
	}
	public static void error(){
		System.out.println("ERROR");
	}
}
