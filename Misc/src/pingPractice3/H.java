package pingPractice3;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class H {

	static LazySegmentTree tree;
	static int[] mod = new int[100001];
	public static void main(String[] args) throws Exception{
		int cur=0;
		for(int i=0;i<mod.length;i++){
			mod[i] = cur;
			cur++;
			if(cur == 3)
				cur = 0;
		}

		StringBuffer out = new StringBuffer();
		InputReader reader = new InputReader(System.in);
		PrintWriter outer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
		tree = new LazySegmentTree(100000);

		int T = reader.nextInt();
		for(int t=1;t<=T;t++){
			out.append("Case " + t + ":\n");
			int n = reader.nextInt();
			int q = reader.nextInt();
			for(int i=0;i<q;i++){
				int id = reader.nextInt();
				int s = reader.nextInt();
				int e = reader.nextInt();
				if(id == 0){
					tree.update(1, 0, n-1, s, e, 1);
				}else{
					out.append(tree.query(1, 0 , n-1, s, e) + "\n");
				}
			}
			if(t !=T) tree.reset();
		}
		outer.print(out.toString());
		outer.close();
	}

	private static class InputReader {

		BufferedInputStream bin;
		int numChars,curChar;
		byte[] buffer;
		static final int MAXBUF = 15000000;

		public InputReader(InputStream in) {
			bin = new BufferedInputStream(in);
			buffer = new byte[MAXBUF];
		}

		private int read() {
			if(numChars == -1)
				throw new InputMismatchException();

			if(curChar >= numChars) {
				curChar = 0;
				try {
					numChars = bin.read(buffer);
				} catch(IOException e) {
					throw new InputMismatchException();
				}
				if(numChars <= 0)
					return -1;
			}
			return buffer[curChar++];
		}

		public int nextInt() {
			int ch = read();
			while(isSpaceChar(ch))
				ch = read();

			int sgn = 1,res = 0;
			if(ch == '-') {
				sgn = -1;
				ch = read();
			}

			do {
				if(ch < '0' || ch > '9')
					throw new InputMismatchException();
				res *= 10;
				res += ch - '0';
				ch = read();
			} while(!isSpaceChar(ch));

			return res*sgn;
		}

		public boolean isSpaceChar(int ch) {
			return ch == ' ' || ch == '\n' || ch == '\r' || ch == -1 || ch == '\t' || ch =='$';
		}

	}


	public static class LazySegmentTree {
		//we don't actually keep track of tree.mod[0]. so we don't need to build tree!
		//since tree.mod[0] = range of resp - mod[1] - mod[2]
		int[][] tree;
		int[] lazy;

		//num els in the original array
		int n;
		//initialize exactly once, with the max size
		public LazySegmentTree(int n){
			this.n = n;
			int height = (int)Math.ceil(Math.log(n)/Math.log(2)) + 1;
			int numNodes = (int)(Math.pow(2,height))-1;
			tree = new int[3][numNodes+1];
			lazy = new int[numNodes+1];
			for(int i=0;i<3;i++){
				tree[i] = new int[numNodes+1];
			}
		}

		public void reset(){
			for(int i=0;i<lazy.length;i++){
				lazy[i] = 0;
				tree[1][i] = 0;
				tree[2][i] = 0;
			}
		}

		//guarantee: i,j is between a,b. a,b, is current node's range
		/**
		 * Increment elements within range [i, j] with value value
		 */
		private void update(int node, int a, int b, int i, int j, int value) {
			if(a == i && b == j){
				lazy[node]++;
				if(lazy[node] == 3)lazy[node] = 0;
				return;
			}else{
				int left = node << 1;
				int right = left + 1;

				lazy[left] += lazy[node];
				if(lazy[left] >= 3) lazy[left]-=3;
				lazy[right] += lazy[node];
				if(lazy[right] >= 3) lazy[right]-=3;
				lazy[node] = 0;

				int mid = (a+b)/2;
				if(i <= mid) update(node * 2, a, mid, i, Math.min(j, mid), value);
				if(j > mid) update(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j, value);

				tree[0][left] = mid - a + 1 - tree[1][left] - tree[2][left];
				tree[0][right] = b - mid - tree[1][right] - tree[2][right]; 
				for(int k = 1; k < 3; k++ ){
					tree[k][node] = tree[mod[3 - lazy[left] + k]][left] + 
							tree[mod[3 - lazy[right] + k]][right];
				}
			}
		}

		/**
		 * Query tree to get max element value within range [i, j]
		 */
		private int query(int node, int a, int b, int i, int j) {
			if(a == i && b == j){
				int modVal = 3-lazy[node];
				if(modVal == 3)modVal = 0;
				if(modVal != 0){
					return tree[modVal][node];
				}else{
					return b-a+1 - tree[1][node] - tree[2][node];
				}
			}
			else {
				int left = node << 1;
				int right = left + 1;
				lazy[left] += lazy[node];
				if(lazy[left] >= 3) lazy[left]-=3;
				lazy[right] += lazy[node];
				if(lazy[right] >= 3) lazy[right]-=3;
				lazy[node] = 0;

				int res = 0;
				int mid = (a+b)/2;

				if(i <= mid) res += query(left, a, mid, i, Math.min(j, mid));
				if(j > mid) res += query(right, mid+1, b, Math.max(mid + 1, i), j);

				tree[0][left] = mid - a + 1 - tree[1][left] - tree[2][left];
				tree[0][right] = b - mid - tree[1][right] - tree[2][right]; 
				for(int k = 1; k < 3; k++ ){
					tree[k][node] = tree[mod[3 - lazy[left] + k]][left] + 
							tree[mod[3 - lazy[right] + k]][right];
				}
				return res;
			}
		}
	}
}
