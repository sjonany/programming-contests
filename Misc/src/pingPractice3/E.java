package pingPractice3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class E {
	public static void main(String[] args) throws NumberFormatException, IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tkz = new StringTokenizer(reader.readLine());
		long N = Integer.parseInt(tkz.nextToken());
		int L = Integer.parseInt(tkz.nextToken());
		long[] heights = new long[(int)N];
		tkz = new StringTokenizer(reader.readLine());
		long total = 0;
		for(int i=0;i<N;i++){
			heights[i] = Integer.parseInt(tkz.nextToken());
			if(heights[i] != 0){
				heights[i] %= L;
			}
			total += heights[i];
		}

		
		Arrays.sort(heights);
		long minCost = total;
		int yrToWait = 0;
		
		for(int i=0;i<N;i++){
			long curCost = total - N*heights[i] + (long)i*L;
			if(curCost < minCost){
				minCost = curCost;
				yrToWait = L-(int)heights[i];
			}else if(curCost == minCost){
				int newYr = L-(int)heights[i];
				if(yrToWait > newYr){
					yrToWait = newYr;
				}
			}
		}
		System.out.println(yrToWait);
	}
	
	public static int bf(long n, int l, long[] ho){
		long[] h = new long[ho.length];
		for(int i=0;i<n;i++){
			h[i] = ho[i];
		}
		int bestCost = Integer.MAX_VALUE;
		int bestYear = -1;
		for(int k=0;k<l;k++){
			int curCost = 0;
			for(int i=0;i<h.length;i++){
				curCost += h[i]%l;
			}
			if(curCost < bestCost){
				bestCost = curCost;
				bestYear = k;
			}
			for(int i=0;i<h.length;i++){
				h[i]++;
			}
		}
		System.out.println("bestCost = " + bestCost);
		return bestYear;
	}
}
