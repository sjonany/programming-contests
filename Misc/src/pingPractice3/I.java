package pingPractice3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Set;
import java.util.StringTokenizer;


public class I {
	static Set<String> results;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(reader.readLine());
		for(int t=1;t<=T;t++){
			solve(t,reader);
		}
	}
	
	static void solve(int t, BufferedReader reader) throws Exception{
		reader.readLine();
		StringTokenizer tkz = new StringTokenizer(reader.readLine());
		int N = Integer.parseInt(tkz.nextToken());
		int w = Integer.parseInt(tkz.nextToken());
		int k = Integer.parseInt(tkz.nextToken());
		int[] els = new int[N];
		for(int i=0;i<N;i++){
			tkz = new StringTokenizer(reader.readLine());
			tkz.nextToken();
			els[i] = Integer.parseInt(tkz.nextToken());
		}
		
		Arrays.sort(els);
		//last[i] = k -> k is the left most index reachable with length w
		int[] last = new int[N];
		last[0] = 0;
		int leftmost = 0;
		for(int i=1;i<N;i++){
			while(els[i] - els[leftmost] > w){
				leftmost++;
			}
			last[i] = leftmost;
		}
		
		//dp[i][j] = consider els[0..i], with j swipes, max = ?
		int[][] dp = new int[N][k+1];
		for(int i=0;i<N;i++){
			for(int j=0;j<=k;j++){
				if(j==0){
					dp[i][j] = 0;
				}else{
					if(i==0){
						dp[i][j] = 1;
					}else{
						if(last[i] == 0){
							dp[i][j] = i+1;
						}else{
							dp[i][j] = Math.max(dp[i-1][j], dp[last[i]-1][j-1] + (i-last[i]+1));
						}
					}
				}
			}
		}
		
		System.out.println("Case " + t + ": " + dp[N-1][k]);
	}
}
