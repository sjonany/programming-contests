import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class A {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	public static void main(String[] args) throws Exception{
		char[][] b = {{'A','B'},
					  {'D','E'}};
		bruteForce(b);
	}
	
	//brute force all the possible words formable from b by going left, up ,down, right
	static void bruteForce(char[][] b) {
		int R = b.length;
		int C = b[0].length;
		
		boolean[][] visit = new boolean[R][C];
		for(int r = 0; r < R; r++) {
			for(int c = 0; c < C; c++) {
				dfs(b,r,c, new ArrayList<Character>(), visit);
			}
		}
	}
	
	static int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
	
	//pre: visit[r][c] is not true && (r,c) is valid, cur is the current string excluding (r,c)
	//post: cur + all possible substrings rooted at r,c, and never visiting visit[][] will be outputted already
	//also, visit[][] and cur should remain unchanged at the end of the call
	static void dfs(char[][] b, int r, int c, List<Character> cur, boolean[][] visit) {
		visit[r][c] = true;
		cur.add(b[r][c]);
		System.out.println(cur);
		for(int[] dir : dirs){
			int nr = r + dir[0];
			int nc = c + dir[1];
			if(nr < b.length && nr >= 0 && nc < b[0].length && nc >= 0 && !visit[nr][nc]){
				dfs(b, nr, nc, cur, visit);
			}
		}
		cur.remove(cur.size()-1);
		visit[r][c] = false;
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
