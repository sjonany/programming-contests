import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class Dropbox {
	public static void main(String[] args) throws Exception{
		List<String> ans = genPerms("abcd");
		System.out.println(ans.size());
		System.out.println(ans);
	}
	
	static List<String> getAnagrams(Set<String> dict, String input) {
		List<String> result = new ArrayList<String>();
		List<String> perms = genPerms(input);
		for(String word : perms){
			if(dict.contains(word)) {
				result.add(word);
			}
			
		}
		return result;
	}
	
	static List<String> genPerms(String input) {
		Set<String> result = new HashSet<String>();
		genPerms(0, new boolean[input.length()], input, result, "");
		List<String> out = new ArrayList<String>();
		out.addAll(result);
		return out;
	}
	
	// if input has lenght 5, b a x _ _ 
	//
	static void genPerms(int toSet, boolean[] hasUsed, String input, Set<String> out, String currentResult) {
		if(toSet == input.length()){
			out.add(currentResult.toString());
		} else{
			for(int i = 0; i < hasUsed.length; i++) {
				if(!hasUsed[i]){
					hasUsed[i] = true;
					currentResult += input.charAt(i);
					genPerms(toSet+1, hasUsed, input, out, currentResult);
					currentResult = currentResult.substring(0, currentResult.length()-1);
					hasUsed[i] = false;
				}
			}
		}
	}
}
