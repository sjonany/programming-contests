package pingPractice1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class J {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			StringTokenizer tkz = new StringTokenizer(reader.readLine());
			long H = Long.parseLong(tkz.nextToken());
			long K = Long.parseLong(tkz.nextToken());
			
			if(H==0 && K==0)return;
			
			int l=0;
			long N =-1;
			boolean isFound = false;
			while(!isFound){
				if(l==0){
					if(K == 1 && H == 1){
						break;
					}
				}else if(l == 1){
					if(H-1 == K){
						N = K;
						break;
					}
				}else{
					//if exist N such that N^L == K?
					N = binSearch(l,K);
					if(N != -1){
						if(pow(N+1,l) == H){
							isFound=true;
							break;
						}
					}
				}
				l++;
			}
			if(N== -1 || N == 0){
				//single cat
				System.out.println("0 1");
			}else if(N==1){
				System.out.println(l + " " +  (H*(N+1)-K*N));
			}else{
				System.out.println((K-1)/(N-1) + " " + (H*(N+1)-K*N));
			}
		}
		
		
	}
	
	public static int binSearch(int l, long k){
		//System.out.println("bin search l = " + l + ",k = " +  k);
		int lo=0;
		int hi=2*(int)Math.pow(k, 1.0/l);
		
		while(lo<hi){
			int mid = lo+(hi-lo+1)/2; 
			if(pow(mid, l) >= k){
				hi = mid-1;
			}else{
				lo = mid;
			}
		}
		
		int n = lo+1;
		if(pow(n,l) != k){
			return -1;
		}else{
			return n;
		}
	}
	

	public static long pow(long a, long exp){
		if(exp == 0){
			return 1;
		}
		
		if(exp % 2 ==0){
			long r = pow(a,exp/2);
			return r*r;
		}else{
			return a * pow(a, exp-1);
		}
	}
}
