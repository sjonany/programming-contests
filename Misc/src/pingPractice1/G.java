package pingPractice1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
public class G{
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(reader.readLine());
		for(int t=1;t<=T;t++){
			StringTokenizer tkz = new StringTokenizer(reader.readLine());
			int N = Integer.parseInt(tkz.nextToken());
			int M = Integer.parseInt(tkz.nextToken());
			int L = Integer.parseInt(tkz.nextToken());
			Map<Integer, List<Integer>> adj = new HashMap<Integer, List<Integer>>();
			for(int i=1;i<=N;i++){
				adj.put(i,  new ArrayList<Integer>());
			}
			for(int i=0;i<M;i++){
				tkz = new StringTokenizer(reader.readLine());
				int from = Integer.parseInt(tkz.nextToken());
				int to = Integer.parseInt(tkz.nextToken());
				adj.get(from).add(to);
			}
			
			boolean[] isKnocked = new boolean[N+1];
			
			for(int i=0;i<L;i++){
				int knock = Integer.parseInt(reader.readLine());
				knock(isKnocked, knock, adj);
			}

			int count = 0;
			for(int i=1;i<=N;i++){
				if(isKnocked[i])count++;
			}
			System.out.println(count);
		}
	}
	
	public static void knock(boolean[] isKnocked, int knock, Map<Integer, List<Integer>> adj){
		isKnocked[knock] = true;
		for(int next : adj.get(knock)){
			if(!isKnocked[next]){
				isKnocked[next] = true;
				knock(isKnocked, next, adj);
			}
		}
	}
}

