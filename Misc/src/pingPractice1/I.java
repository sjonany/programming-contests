package pingPractice1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class I {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int t=1;
		while(true){
			StringTokenizer tkz = new StringTokenizer(reader.readLine());
			int R = Integer.parseInt(tkz.nextToken());
			if(R == 0)return;
			if(t>1){
				System.out.println();
			}
			System.out.println("puzzle #" + t + ":");
			t++;
			
			int C = Integer.parseInt(tkz.nextToken());
			char[][] board = new char[R][C];
			int[][] qNumBoard = new int[R][C];
			int qNum = 1;
			for(int r=0;r<R;r++){
				String line = reader.readLine();
				for(int c=0;c<C;c++){
					board[r][c] = line.charAt(c);
					if(board[r][c] != '*' && (r==0 || c == 0 || board[r-1][c]=='*' || board[r][c-1] == '*')){
						qNumBoard[r][c] = qNum;
						qNum++;
					}
				}
			}
			
			//across
			StringBuffer output = new StringBuffer();
			output.append("Across\n");
			for(int r=0;r<R;r++){
				for(int c=0;c<C;c++){
					if(qNumBoard[r][c] != 0 && (c==0 || board[r][c-1] == '*')){
						int qNumTemp = qNumBoard[r][c];
						StringBuffer ans = new StringBuffer();
						while(c<C && board[r][c] != '*'){
							ans.append(board[r][c]);
							c++;
						}
						output.append(String.format("%3d" + "." + ans + "\n", qNumTemp));
					}
				}
			}
			//bottom
			output.append("Down\n");
			for(int r=0;r<R;r++){
				for(int c=0;c<C;c++){
					if(qNumBoard[r][c] != 0 && (r==0 || board[r-1][c] == '*')){
						StringBuffer ans = new StringBuffer();
						int tempR = r;
						while(tempR<R && board[tempR][c] != '*'){
							ans.append(board[tempR][c]);
							tempR++;
						}

						output.append(String.format("%3d" + "." + ans + "\n", qNumBoard[r][c]));
					}
				}
			}
			System.out.print(output);
			
		}		
	}
}
