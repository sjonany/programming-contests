package pingPractice1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class C {
	static long min;
	static int n;
	static long[] m;
	static long[] sum;
	
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringBuffer out = new StringBuffer();
		while(true){
			StringTokenizer tkz = new StringTokenizer(reader.readLine());
			n = Integer.parseInt(tkz.nextToken());
			if(n==0)break;
			
			if(n==1){
				out.append(Integer.parseInt(reader.readLine()) + "\n");
				continue;
			}

			sum = new long[n+1];
			min = Long.MAX_VALUE;
			m = new long[n];
			for(int i=0;i<n;i++){
				m[i] = Integer.parseInt(reader.readLine());
			}	
			Arrays.sort(m);
			for(int i=0;i<n/2;i++){
				long temp = m[i];
				m[i] = m[n-i-1];
				m[n-i-1] = temp;
			}
			sum[n] = 0;
			for(int i = n - 1; i >= 0; i-- ){
				sum[i] = m[i] + sum[i + 1];
			}
			dfs(0,0,0,0);
			out.append(min + "\n");
		}
		
		System.out.print(out);
	}
	
	static void dfs(int at, long sum1, long sum2, long sold){
		if(sold >= min) return ;
		if(sum1 == sum2){
			min = Math.min(min, sold + sum[at]);
		}
		if(at == n) return ;
		if(Math.abs(sum1 - sum2) > sum[at]) return ;
		
		if(sum1 == sum2){
			dfs(at + 1, sum1 + m[at], sum2, sold);
			dfs(at + 1, sum1, sum2, sold + m[at]);
		}else{
			dfs(at + 1, sum1 + m[at], sum2, sold);
			dfs(at + 1, sum1, sum2 + m[at], sold);
			dfs(at + 1, sum1, sum2, sold + m[at]);
		}
	}
}
