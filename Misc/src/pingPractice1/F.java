package pingPractice1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class F {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(reader.readLine());
		for(int t=1;t<=T;t++){
			int target = Integer.parseInt(reader.readLine().trim());
			int N = Integer.parseInt(reader.readLine().trim());
			int[] val = new int[N];
			for(int i=0;i<N;i++){
				val[i] = Integer.parseInt(reader.readLine().trim());
			}
			int V = 20005;
			int[][] dp = new int[N][V+1];
			for(int v=0;v<V+1;v++){
				dp[0][v] = -1;
			}
			dp[0][0] = 0;
			dp[0][val[0]] = 1;
			
			int best = 101;
			int paid = -1;
			
			if(val[0] >= target){
				best = 1;
				paid = val[0];
			}
			for(int i=1;i<N;i++){
				dp[i][0] = 0;
				for(int j=1;j<V+1;j++){
					//if don't use i'th coin
					dp[i][j] = dp[i-1][j];
					//if use i'th coin
					if(j-val[i] >=0 && dp[i-1][j-val[i]] != -1){
						int ifUse = dp[i-1][j-val[i]]+1;
						if(dp[i][j] == -1){
							dp[i][j] = ifUse;
						}else{
							dp[i][j] = Math.min(dp[i][j], ifUse);
						}
					}
					if(j >= target && dp[i][j] != -1){
						if(j < paid || paid == -1){
							paid = j;
							best = dp[i][j];
						}
						
						if(j== paid){
							best = Math.min(best, dp[i][j]);
						}
					}
				}
			}
			System.out.println(paid + " " + best);
		}
		
		
	}
}
