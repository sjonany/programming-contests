package pingPractice1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class D {
	static int X = 0;
	static int Y = 1;
	static int R = 2;
	
	static double[][] enemies;
	static int N;
	static boolean good = true;
	static boolean[] visited;
	static double right;
	static double left;
	
	public static void main(String[] args) throws Exception{
		

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			String line = reader.readLine();
			if(line == null)return;
			N = Integer.parseInt(line);
			enemies = new double[N][3];
			good = true;
			visited = new boolean[N];
			right = 1000;
			left = 1000;
			for(int i=0;i<N;i++){
				StringTokenizer tkz = new StringTokenizer(reader.readLine());
				enemies[i][X] = Double.parseDouble(tkz.nextToken());
				enemies[i][Y] = Double.parseDouble(tkz.nextToken());
				enemies[i][R] = Double.parseDouble(tkz.nextToken());
			}
			
			for(int i=0;i<N;i++){
				if(!visited[i] && enemies[i][Y] + enemies[i][R] > 1000){
					dfs(i);
				}
			}
			
			if(good){
				System.out.printf("%.2f %.2f %.2f %.2f\n",0.0,left,1000.0,right);
			}else{
				System.out.println("IMPOSSIBLE");
			}
		}
	}
	
	static void dfs(int i){
		visited[i] = true;
		for(int j=0;j<N;j++){
			if(!visited[j]){
				double dx = enemies[i][X] - enemies[j][X];
				double dy = enemies[i][Y] - enemies[j][Y];
				double rTot = enemies[i][R] + enemies[j][R];
				//is touching
				if(dx*dx + dy*dy < rTot * rTot){
					dfs(j);
				}
			}
		}
		
		//since we started from the top, and we reached bottom through touching circles, deadend
		if(enemies[i][Y] - enemies[i][R] <0){
			good = false;
			return;
		}
		
		//we started at top, and somehow reach corner, so this must be a blocking circle
		if(enemies[i][X] + enemies[i][R] > 1000){
			double r = enemies[i][R];
			double x = 1000.0 - enemies[i][X];
			right = Math.min(right, enemies[i][Y] - Math.sqrt(r*r-x*x));
		}
		
		if(enemies[i][X] - enemies[i][R] < 0){
			double r = enemies[i][R];
			double x = enemies[i][X];
			left = Math.min(left, enemies[i][Y] - Math.sqrt(r*r-x*x));
		}
	}
}
