package pingPractice1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class A {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			StringTokenizer tkz = new StringTokenizer(reader.readLine());
			int N = Integer.parseInt(tkz.nextToken());
			int M = Integer.parseInt(tkz.nextToken());
			if(N==0 && M==0)return;
			Set<Integer> id = new HashSet<Integer>();
			for(int i=0;i<N;i++){
				id.add(Integer.parseInt(reader.readLine()));
			}
			int intersect = 0;
			for(int i=0;i<M;i++){
				if(id.contains(Integer.parseInt(reader.readLine()))){
					intersect++;
				}
			}
			System.out.println(intersect);
		}
		
		
	}
}
