package pingPractice1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class E {
	static int X = 0;
	static int Y = 1;
	static int R = 2;
	
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		int numTestcase = Integer.parseInt(line);
		for(int i=0;i<numTestcase;i++){
			int numCranes = Integer.parseInt(reader.readLine());
			int[][] crane = new int[numCranes][3];
			for(int j=0;j<numCranes;j++){
				StringTokenizer tkz = new StringTokenizer(reader.readLine());
				crane[j][X] = Integer.parseInt(tkz.nextToken());
				crane[j][Y] = Integer.parseInt(tkz.nextToken());
				crane[j][R] = Integer.parseInt(tkz.nextToken());
			}
			

			long maxArea = 0;
			for(int subset =0;subset<(1<<numCranes);subset++){
				boolean isGood = true;
				long area = 0;
				for(int i1=0;i1<numCranes && isGood ;i1++){
					if((subset & (1<<i1)) != 0){
						for(int i2=i1+1;i2<numCranes;i2++){
							if((subset & (1<<i2)) != 0){
								int x1 = crane[i1][X];
								int y1 = crane[i1][Y];
								int x2 = crane[i2][X];
								int y2 = crane[i2][Y];
								long dy = y2-y1;
								long dx = x2-x1;
								long totalR = crane[i1][R] + crane[i2][R];
								
								if(dy*dy+dx*dx <= totalR*totalR){
									isGood = false;
									break;
								}
							}
						}
						area += crane[i1][R]*crane[i1][R];
					}
				}
				
				if(isGood){
					maxArea = Math.max(area, maxArea);
				}
			}
			System.out.println(maxArea);
		}
	}
}
