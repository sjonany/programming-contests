package pingPractice1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class H {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			String line = reader.readLine();
			if(line == null){
				return;
			}
			int N = Integer.parseInt(line);
			double[][] edge = new double[N][N];
			for(int from=0;from<N;from++){
				StringTokenizer tkz = new StringTokenizer(reader.readLine());
				for(int to=0;to<N;to++){
					if(from==to){
						edge[from][to] = 1.0;
					}else{
						edge[from][to] = Double.parseDouble(tkz.nextToken());
					}
				}
			}
			
			//DERP: didn't realize it has to be any country..., plus [from][to] now
			//i,j when going from 0 to i in j steps, what's the first backpointer?
			int[][][] backpointer = new int[N][N][N+1];
			double[][][] maxVal = new double[N][N][N+1];
			
			for(int from=0;from<N;from++){
				for(int to=0;to<N;to++){
					maxVal[from][to][0] = -1;
				}
			}
			
			for(int i=0;i<N;i++){
				maxVal[i][i][0] = 1.0;
			}
			
			boolean isFound = false;
			for(int step=1; step<=N; step++){
				for(int from=0;from<N;from++){
					for(int to=0;to<N;to++){
						for(int k=0;k<N;k++){
							double newVal = maxVal[from][k][step-1] * edge[k][to];
							if(maxVal[from][to][step] < newVal){
								maxVal[from][to][step] = newVal;
								backpointer[from][to][step] = k;
							}
						}
					}	
				}
				for(int i=0;i<N;i++){
					if(maxVal[i][i][step] > 1.01){
						isFound = true;
						StringBuffer sb = new StringBuffer();
						int curPointer = i;
						for(int s = step; s>=0; s--){
							if(s == step){
								sb.insert(0, (curPointer+1));
							}else{
								sb.insert(0, (curPointer+1) + " ");
							}
							curPointer = backpointer[i][curPointer][s];
						}
						System.out.println(sb);
						break;
					}
				}
				if(isFound)break;
			}
			
			if(!isFound){
				System.out.println("no arbitrage sequence exists");
			}
		}
	}
}
