package pingPractice1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;


public class B {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		while(line != null){
			int numStats = Integer.parseInt(line);
			if(numStats == 0){
				return;
			}
			int[] dist=  new int[numStats+2];
			dist[0] = 0;
			dist[numStats+1] = 1422;
			for(int i=1;i<=numStats;i++){
				dist[i] = Integer.parseInt(reader.readLine());
			}
			
			Arrays.sort(dist);
			
			boolean isGood = true;
			for(int i=0;i<dist.length-1;i++){
				if(dist[i+1]-dist[i] > 200){
					isGood=false;
					break;
				}
			}
			if((1422- dist[dist.length-2]) *2 > 200){
				isGood = false;
			}
			if(isGood){
				System.out.println("POSSIBLE");
			}else{
				System.out.println("IMPOSSIBLE");
			}
		
			line = reader.readLine();
		}
	}
}
