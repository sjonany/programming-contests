import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class ALotOfJoy {
	public static void main(String[] args){
		FastScanner sc = new FastScanner();
		int[] countRep = new int[1000];
		String s = sc.nextToken();
		long totalNum = 0;
		long totalDen = s.length();
		for(int i=0;i<s.length();i++){
			countRep[s.charAt(i)]++; 
		}
		
		for(int i=0;i<s.length();i++){
			totalNum += countRep[s.charAt(i)];
		}
		
		System.out.println(1.0 * totalNum / totalDen);
	}
	
	public static class FastScanner {
		BufferedReader br;
		StringTokenizer st;

		public FastScanner(String s) {
			try {
				br = new BufferedReader(new FileReader(s));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public FastScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String nextToken() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(nextToken());
		}

		long nextLong() {
			return Long.parseLong(nextToken());
		}

		double nextDouble() {
			return Double.parseDouble(nextToken());
		}
	}
}
