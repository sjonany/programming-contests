package FacebookQual;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;
// too lazy
public class Basketball {
	static PrintWriter out;
	static BufferedReader in;
	static String DIR = "/Users/sjonany/Workspaces/ProgrammingContests/FacebookHackerCup/src/FacebookQual/";
	
	public static void main(String[] args) throws Exception {
		out = new PrintWriter(System.out);
		in = new BufferedReader(new InputStreamReader(System.in));
		
		//out = new PrintWriter(DIR + "myout.txt");
		//in = new BufferedReader(new FileReader(DIR + "square_detector.txt"));
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 0; test < T; test++) {
			solve(test);
		}
		out.close();
	}
	
	static void solve(int test) throws Exception {
		StringTokenizer tk = new StringTokenizer(in.readLine());
		int N = Integer.parseInt(tk.nextToken());
		int M = Integer.parseInt(tk.nextToken());
		int P = Integer.parseInt(tk.nextToken());
		
		Player[] players = new Player[N];
		
		for(int i = 0; i < N; i++) {
			tk = new StringTokenizer(in.readLine());
			String name = tk.nextToken();
			int percent = Integer.parseInt(tk.nextToken());
			int height = Integer.parseInt(tk.nextToken());
			players[i] = new Player(name, percent, height);
		}
	}
	
	static class Player {
		String name;
		int percent;
		int height;
		
		public Player(String name, int percent, int height) {
			this.name = name;
			this.percent = percent;
			this.height = height;
		}
		
	}
}
