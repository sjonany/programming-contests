package FacebookQual;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Tennis {
	static PrintWriter out;
	static BufferedReader in;
	static String DIR = "/Users/sjonany/Workspaces/ProgrammingContests/FacebookHackerCup/src/FacebookQual/";

	public static void main(String[] args) throws Exception {
		//out = new PrintWriter(System.out);
		//in = new BufferedReader(new InputStreamReader(System.in));

		out = new PrintWriter(DIR + "myout.txt");
		in = new BufferedReader(new FileReader(DIR + "tennison.txt"));

		int T = Integer.parseInt(in.readLine());
		for (int test = 0; test < T; test++) {
			solve(test+1);
		}
		out.close();
	}

	static void solve(int test) throws Exception {
		StringTokenizer tk = new StringTokenizer(in.readLine());
		int K = Integer.parseInt(tk.nextToken());
		double winSun = Double.parseDouble(tk.nextToken());
		double winRain = Double.parseDouble(tk.nextToken());
		int pSunI = (int) Math.round(Double.parseDouble(tk.nextToken()) * 1000);
		int sunInc = (int) Math.round(Double.parseDouble(tk.nextToken()) * 1000);
		double pInc = Double.parseDouble(tk.nextToken());
		int sunDec = (int) Math.round(Double.parseDouble(tk.nextToken()) * 1000);
		double pDec = Double.parseDouble(tk.nextToken());

		// Pr win a times, lose b times, and pSun of day+1 = c, such that a+bth day is a win/lose
		double[][][] dpWin = new double[K+2][K+2][1001];
		double[][][] dpLose = new double[K+2][K+2][1001];

		dpWin[0][0][pSunI] = 1;

		for(int day = 0; day <= 2*K; day++) {
			for(int win = 0; win <= Math.min(K,day); win++) {
				for(int psun = 0; psun <= 1000; psun++){
					int lose = day - win;
					if(lose < 0 || lose >= K) continue;
					double prob = dpLose[win][lose][psun] + dpWin[win][lose][psun];
					//if sunny
					
					//if win
					//if change
					dpWin[win+1][lose][Math.min(1000, psun + sunInc)] += prob * winSun * pInc * psun / 1000.0; 
					//if not
					dpWin[win+1][lose][psun] += prob * winSun * (1-pInc) * psun / 1000.0; 
					
					//if lose
					//if change
					dpLose[win][lose+1][Math.max(0, psun - sunDec)] += prob * (1-winSun) * pDec * psun / 1000.0; 
					//if not
					dpLose[win][lose+1][psun] += prob * (1-winSun) * (1-pDec) * psun / 1000.0; 	
					
					//if rain
					//if win
					//if change
					dpWin[win+1][lose][Math.min(1000, psun + sunInc)] += prob * winRain * pInc * (1000-psun) / 1000.0; 
					//if not
					dpWin[win+1][lose][psun] += prob * winRain * (1-pInc) * (1000-psun) / 1000.0; 
					
					//if lose
					//if change
					dpLose[win][lose+1][Math.max(0, psun - sunDec)] += prob * (1-winRain) * pDec * (1000-psun) / 1000.0; 
					//if not
					dpLose[win][lose+1][psun] += prob * (1-winRain) * (1-pDec) * (1000-psun) / 1000.0; 	
				}
			}
		}

		double ans = 0;
		for(int lose = 0; lose < K; lose++) {
			for(int p = 0; p <= 1000; p++){
				ans += dpWin[K][lose][p];
			}
		}

		out.printf("Case #%d: %.6f\n", test, ans);
	}

	static class Player {
		String name;
		int percent;
		int height;

		public Player(String name, int percent, int height) {
			this.name = name;
			this.percent = percent;
			this.height = height;
		}

	}
}
