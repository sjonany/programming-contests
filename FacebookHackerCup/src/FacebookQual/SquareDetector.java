package FacebookQual;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class SquareDetector {
	static PrintWriter out;
	static BufferedReader in;
	static int[] sq;
	static String DIR = "/Users/sjonany/Workspaces/ProgrammingContests/FacebookHackerCup/src/FacebookQual/";
	
	public static void main(String[] args) throws Exception {
		//out = new PrintWriter(System.out);
		//in = new BufferedReader(new InputStreamReader(System.in));
		
		out = new PrintWriter(DIR + "myout.txt");
		in = new BufferedReader(new FileReader(DIR + "square_detector.txt"));
		
		int T = Integer.parseInt(in.readLine());
		sq = new int[30*30+5];
		Arrays.fill(sq, -1);
		for(int i = 1; i <= 30; i++) {
			sq[i*i] = i;
		}
		
		for(int test = 0; test < T; test++) {
			if(isGood()) {
				out.printf("Case #%d: YES\n", test+1);
			}else {
				out.printf("Case #%d: NO\n", test+1);
			}
		}
		out.close();
	}
	
	static boolean isGood() throws Exception {
		int N = Integer.parseInt(in.readLine());
		boolean[][] board = new boolean[N][N];
		int count = 0;
		for(int r = 0; r < N; r ++) {
			String s = in.readLine();
			for(int c = 0; c < N; c++) {
				board[r][c] = s.charAt(c) == '#';
				if(board[r][c]) count++;
			}	
		}
		
		int len = sq[count];
		if(len == -1) {
			return false;
		}
		
		int nr = -1;
		int nc = -1;
		
		for(int r = 0; r < N; r ++) {
			for(int c = 0; c < N; c++) {
				if(board[r][c]) {
					nr = r;
					nc = c;
					break;
				}
			}	
			if(nr != -1) break;
		}
		
		for(int r = nr; r < nr + len; r++) {
			for(int c = nc; c < nc + len; c++) {
				if(r >= N || c >= N || !board[r][c]) {
					return false;
				}
			}
		}
		
		return true;
	}
}
