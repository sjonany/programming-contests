package Round1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class LabelMaker {
	static PrintWriter out;
	static BufferedReader in;
	static StringTokenizer tk;
	static String DIR = "/Users/sjonany/Workspaces/ProgrammingContests/FacebookHackerCup/src/Round1/";
	
	public static void main(String[] args) throws Exception {
		//out = new PrintWriter(System.out);
		//in = new BufferedReader(new InputStreamReader(System.in));
		
		out = new PrintWriter(DIR + "myout.txt");
		in = new BufferedReader(new FileReader(DIR + "labelmaker.txt"));
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++) {
			solve(test);
		}
		out.close();
	}
	

	static void solve(int test) throws Exception {
		tk = tk (in.readLine());
		out.printf("Case #%d: %s\n", test, solve(tk.nextToken(), Long.parseLong(tk.nextToken())));
	}
	
	static String solve(String s, long n) {
		BigInteger N = BigInteger.valueOf(n-1);
		StringBuilder ans = new StringBuilder();
		BigInteger slen = BigInteger.valueOf(s.length());
		
		BigInteger period = BigInteger.ONE;
		BigInteger gap = BigInteger.ZERO;
		while(true) {
			BigInteger offset = N.subtract(gap);
			if(offset.compareTo(BigInteger.ZERO) < 0) break;
			int index = (int) offset.divide(period).mod(slen).longValue();
			ans.append(s.charAt(index));
			
			period = period.multiply(slen);
			gap = gap.add(period);
		}
		return ans.reverse().toString();
	}
	
	static StringTokenizer tk (String s) {
		return new StringTokenizer(s);
	}
}
