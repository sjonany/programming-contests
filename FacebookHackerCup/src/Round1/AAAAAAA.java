package Round1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class AAAAAAA {
	static PrintWriter out;
	static BufferedReader in;
	static StringTokenizer tk;
	static String DIR = "/Users/sjonany/Workspaces/ProgrammingContests/FacebookHackerCup/src/Round1/";
	
	public static void main(String[] args) throws Exception {
		//out = new PrintWriter(System.out);
		//in = new BufferedReader(new InputStreamReader(System.in));
		
		out = new PrintWriter(DIR + "myout.txt");
		in = new BufferedReader(new FileReader(DIR + "aaaaaa.txt"));
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++) {
			solve(test);
		}
		out.close();
	}
	
	static StringTokenizer tk (String s) {
		return new StringTokenizer(s);
	}
	
	
	static void solve(int test) throws Exception {
		tk = tk(in.readLine());
		int R = Integer.parseInt(tk.nextToken());
		int C = Integer.parseInt(tk.nextToken());
		
		boolean[][] isempty = new boolean[R][C];
		for(int r=0;r<R;r++){
			String s = in.readLine();
			for(int c=0;c<C;c++){
				isempty[r][c] = s.charAt(c) == '.';
			}	
		}
		
		//starting at r,c, and its subrectangle is untouched, what is the best answer
		int[][] dpnormal = new int[R][C];
		for(int r = R-1; r >= 0; r--) {
			for(int c = C-1;c >= 0; c--) {
				if(!isempty[r][c]){
					dpnormal[r][c] =0;
				}else {
					int moveRight = c+1 < C ? dpnormal[r][c+1] : 0;
					int moveDown = r+1 < R ? dpnormal[r+1][c] : 0;
					dpnormal[r][c] = 1 + Math.max(moveRight, moveDown);
				}
			}
		}
		
		//haven't used my super power, and i come from left
		int[][] dpspecialleft = new int[R][C];
		int[][] dpspecialup = new int[R][C];
		
		for(int r = R-1; r >= 0; r--) {
			for(int c = C-1;c >= 0; c--) {
				if(!isempty[r][c]){
					dpspecialleft[r][c] = 0;
					dpspecialup[r][c] = 0;
				}else {
					int moveRight = c+1 < C ? dpspecialleft[r][c+1] : 0;
					int moveDown = r+1 < R ? dpspecialup[r+1][c] : 0;
					
					dpspecialleft[r][c] = 1 + Math.max(moveRight, moveDown);
					dpspecialup[r][c] = 1 + Math.max(moveRight, moveDown);
					
					//special power
					//if i am from left, i can only use my special power to go up
					int step = 1;
					while(true) {
						int nr = r - step;
						if(nr < 0 || !isempty[nr][c]) {
							break;
						}
						
						//must go to the right next
						int nextnormal = c+1 < C ? dpnormal[nr][c+1]: 0;
						dpspecialleft[r][c] = Math.max(dpspecialleft[r][c], 1 + step + nextnormal);
						step++;
					}
					
					step = 1;
					//i am from top, i can only use special power to go left
					while(true) {
						int nc = c - step;
						if(nc < 0 || !isempty[r][nc]) {
							break;
						}
						
						//must go down next
						int nextnormal = r+1 < R ? dpnormal[r+1][nc]: 0;
						dpspecialup[r][c] = Math.max(dpspecialup[r][c], 1 + step + nextnormal);
						step++;
					}
				}
			}
		}
		
		out.printf("Case #%d: %d\n", test, dpspecialleft[0][0]);
		
	}
}
