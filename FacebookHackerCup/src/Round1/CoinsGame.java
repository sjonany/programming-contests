package Round1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class CoinsGame {
	static PrintWriter out;
	static BufferedReader in;
	static StringTokenizer tk;
	static String DIR = "/Users/sjonany/Workspaces/ProgrammingContests/FacebookHackerCup/src/Round1/";
	
	public static void main(String[] args) throws Exception {
		//out = new PrintWriter(System.out);
		//in = new BufferedReader(new InputStreamReader(System.in));
		
		out = new PrintWriter(DIR + "myout.txt");
		in = new BufferedReader(new FileReader(DIR + "coins_game.txt"));
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++) {
			solve(test);
		}
		out.close();
	}
	

	static void solve(int test) throws Exception {
		tk = tk (in.readLine());
		out.printf("Case #%d: %s\n", test, solve(Integer.parseInt(tk.nextToken()), 
				Integer.parseInt(tk.nextToken()), Integer.parseInt(tk.nextToken())));
	}
	
	static int solve(int J, int C, int T) {
		if(C < J) {
			return J - C + T;
		} else {
			int h = C / J;
			if(T <= h * J) {
				return T;
			} else {
				int numHigher = C % J;
				int numLow = J - C % J;
				
				while(true) {
					if(numLow > 0){	
						if(numLow >= h+1) {
							numLow -= 1;
							numLow -= h;
							numHigher += h;
						} else {
							numHigher += numLow-1;
							break;
						}
					} else {
						break;
					}
				}
				
				return J - numHigher + T;
			}
		}
	}
	
	static StringTokenizer tk (String s) {
		return new StringTokenizer(s);
	}
}
