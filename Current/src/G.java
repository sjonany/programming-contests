import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;


public class G {
	static int m;
	static int n;
	
	//largest rectangle from corner to i,j
	static int[][] ur = new int[251][251];
	static int[][] ul = new int[251][251];
	static int[][] ll = new int[251][251];
	static int[][] lr = new int[251][251];
	//best[i][j] = max rec from row/col [i->j]
	static int[][] bestRow = new int[251][251];
	static int[][] bestCol = new int[251][251];
	
	static boolean[][] board = new boolean[251][251];
	
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		while(true) {
			tk = tk(in.readLine());
			m = Integer.parseInt(tk.nextToken());
			n = Integer.parseInt(tk.nextToken());
			if(m == 0 && n == 0) break;
			
			for(int r = 0 ; r < m; r++){
				String line = in.readLine();
				for(int c = 0 ; c < n; c++){
					board[r][c] = line.charAt(c) == '.';
				}	
			}
			
			out.println(solve());
		}
		
		out.close();
	}
	
	static int solve() {
		int ans = 0;
		
		if(m * n < 3) {
			for(int r = 0 ; r < m; r++){
				for(int c = 0 ; c < n; c++){
					if(board[r][c]) ans++;
				}	
			}
			return ans;
		}
		
		initCorners();
		initLineCuts();
		ans = Math.max(ans, solveStripes());
		ans = Math.max(ans, solveHard());
		/*
		p("UR", ur);
		p("UL", ul);
		p("LL", ll);
		p("LR", lr);
		p("bestRow", bestRow);
		p("bestCol", bestCol);*/
		return ans;
	}
	

	static int[][] dp = new int[4][251];
	static int solveStripes(){
		int ans = 0;
		
		for(int i = 1; i<=3;i++){
			Arrays.fill(dp[i], 0);
		}
		//dp[i][j] = max area if row 0->j & i rectangles
		for(int rec = 1 ; rec <= 3; rec++) {
			for(int r = 0; r < m; r++) {
				if(r == 0) {
					if(rec == 1) {
						dp[rec][r] = bestRow[0][0];
					}
				}else{
					for(int prevRow = 0; prevRow < r; prevRow++) {
						dp[rec][r] = Math.max(dp[rec-1][prevRow] + bestRow[prevRow+1][r], dp[rec][r]); 
					}
				}
				ans = Math.max(ans, dp[rec][r]);
			}
		}
		for(int i = 1; i<=3;i++){
			Arrays.fill(dp[i], 0);
		}
		//dp[i][j] = max area if col 0->j & i rectangles
		for(int rec = 1 ; rec <= 3; rec++) {
			for(int c = 0; c < n; c++) {
				if(c == 0) {
					if(rec == 1) {
						dp[rec][c] = bestCol[0][0];
					}
				}else{
					for(int prevCol = 0; prevCol < c; prevCol++) {
						dp[rec][c] = Math.max(dp[rec-1][prevCol] + bestCol[prevCol+1][c], dp[rec][c]); 
					}
				}
				ans = Math.max(ans, dp[rec][c]);
			}
		}
		return ans;
	}
	
	static int solveHard() {
		int ans = 0;
		for(int rCut = 0; rCut < m-1; rCut++){
			for(int cCut = 0; cCut < n-1; cCut++) {
				//left = 1 piece, right = 2 pieces
				ans = Math.max(ans, bestCol[0][cCut] + ur[rCut][cCut+1] + lr[rCut+1][cCut+1]);
				
				//right = 1 piece, left = 2 pieces
				ans = Math.max(ans, bestCol[cCut+1][n-1] + ul[rCut][cCut] + ll[rCut+1][cCut]);
				
				//top = 1 piece, bottom = 2 pieces
				ans = Math.max(ans, bestRow[0][rCut] + ll[rCut+1][cCut] + lr[rCut+1][cCut+1]);
				
				//top = 2 pieces, bottom = 1 piece
				ans = Math.max(ans, bestRow[rCut+1][m-1] + ul[rCut][cCut] + ur[rCut][cCut+1]);
			}
		}
		
		return ans;
	}
	
	static void initLineCuts() {
		for(int topR = 0; topR < m; topR++) {
			Arrays.fill(hist, 0);
			for(int botR = topR; botR < m; botR++){
				for(int c = 0; c < n; c++) {
					hist[c] = board[botR][c] ? hist[c] + 1 : 0;
					bestRow[topR][botR] = solveHist(hist, n);
				}
			}
		}
		
		for(int leftC = 0; leftC < n; leftC++) {
			Arrays.fill(hist, 0);
			for(int rightC = leftC; rightC < n; rightC++){
				for(int r = 0; r < m; r++) {
					hist[r] = board[r][rightC] ? hist[r] + 1 : 0;
					bestCol[leftC][rightC] = solveHist(hist, m);
				}
			}
		}
	}
	
	static int[] hist = new int[251];
	static void initCorners() {
		//ul
		for(int r = 0; r < m; r++) {
			//r = height
			//histogram grows to the left
			Arrays.fill(hist, 0);
			
			for(int c = 0; c < n; c++) {
				for(int rr = 0; rr < r+1; rr++) {
					hist[rr] = board[rr][c] ? hist[rr] + 1 : 0;
				}
				int prev = c == 0 ? 0 : ul[r][c-1];
				ul[r][c] = Math.max(prev, solveHist(hist, r+1));
			}
		}
		
		//ur, rotate mat by 90 ccw
		for(int r = 0; r < m; r++) {
			Arrays.fill(hist, 0);
			for(int c = n-1; c >= 0; c--) {
				for(int rr = 0; rr < r+1; rr++) {
					hist[rr] = board[rr][c] ? hist[rr] + 1 : 0;
				}
				int prev = c == n-1 ? 0 : ur[r][c+1];
				ur[r][c] = Math.max(prev, solveHist(hist, r+1));
			}
		}
		
		//ll
		for(int r = m-1; r >= 0; r--) {
			Arrays.fill(hist, 0);
			for(int c = 0; c < n; c++) {
				for(int rr = r; rr < m; rr++) {
					hist[m-rr-1] = board[rr][c] ? hist[m-rr-1] + 1 : 0;
				}
				int prev = c == 0 ? 0 : ll[r][c-1];
				ll[r][c] = Math.max(prev, solveHist(hist, m-r));
			}
		}
		
		//lr
		for(int r = m-1; r >= 0; r--) {
			Arrays.fill(hist, 0);
			for(int c = n-1; c >= 0; c--) {
				for(int rr = r; rr < m; rr++) {
					hist[m-rr-1] = board[rr][c] ? hist[m-rr-1] + 1 : 0;
				}
				int prev = c == n-1 ? 0 : lr[r][c+1];
				lr[r][c] = Math.max(prev, solveHist(hist, m-r));
			}
		}
	}

	//left[i] -> right[i] >= hist[i]
	static int[] left = new int[251];
	static int[] right = new int[251];
	static int[] hStack = new int[251];
	static int[] idStack = new int[251];
	//max area under histogram
	static int solveHist(int[] hist, int hl) {
		int ans = 0;
		int top = -1;
		
		for(int i = 0; i < hl; i++) {
			while(top != -1 && hStack[top] > hist[i]) {
				right[idStack[top]] = i-1;
				top--;
			}
			top++;
			hStack[top] = hist[i];
			idStack[top] = i;
		}
		
		while(top != -1){
			right[idStack[top]] = idStack[top];
			top--;
		}

		for(int i = hl-1; i >= 0; i--) {
			while(top != -1 && hStack[top] > hist[i]) {
				left[idStack[top]] = i+1;
				top--;
			}
			top++;
			hStack[top] = hist[i];
			idStack[top] = i;
		}

		while(top != -1){
			left[idStack[top]] = idStack[top];
			top--;
		}
		
		for(int i = 0 ; i < hl; i++) {
			ans = Math.max(ans, hist[i] * (right[i] - left[i] + 1));
		}
		
		return ans;
	}
	
	static void p(String header, int[][] mat) {
		System.out.println(header);
		for(int r = 0; r < mat.length; r++) {
			for(int c = 0; c < mat[0].length; c++) {
				System.out.printf("%3d", mat[r][c]);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
