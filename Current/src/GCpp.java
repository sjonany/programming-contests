
//still TLE sigh
public class GCpp {
/*
#include <cstdio>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <set>
#include <queue>
#include <stack>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <ctype.h>
#include <string.h>

using namespace std;

int m;
int n;

//largest rectangle from corner to i,j
int ur[251][251];
int ul[251][251];
int ll[251][251];
int lr[251][251];
//best[i][j] = max rec from row/col [i->j]
int bestRow[251][251];
int bestCol[251][251];
char line[105];
int cnt[26];
int dp[4][251];

int hist[251];

bool board[251][251];

//leftStack[i] -> rightStack[i] >= hist[i]
int leftStack[251];
int rightStack[251];
int hStack[251];
int idStack[251];

//max area under histogram
int solveHist(int hist[], int hl) {
    int ans = 0;
    int top = -1;

    for(int i = 0; i < hl; i++) {
        while(top != -1 && hStack[top] > hist[i]) {
            rightStack[idStack[top]] = i-1;
            top--;
        }
        top++;
        hStack[top] = hist[i];
        idStack[top] = i;
    }

    while(top != -1){
        rightStack[idStack[top]] = idStack[top];
        top--;
    }

    for(int i = hl-1; i >= 0; i--) {
        while(top != -1 && hStack[top] > hist[i]) {
            leftStack[idStack[top]] = i+1;
            top--;
        }
        top++;
        hStack[top] = hist[i];
        idStack[top] = i;
    }

    while(top != -1){
        leftStack[idStack[top]] = idStack[top];
        top--;
    }

    for(int i = 0 ; i < hl; i++) {
        ans = max(ans, hist[i] * (rightStack[i] - leftStack[i] + 1));
    }

    return ans;
}


void initCorners() {
    //ul
    for(int r = 0; r < m; r++) {
        //r = height
        //histogram grows to the left
        memset(hist, 0, sizeof hist);
        
        for(int c = 0; c < n; c++) {
            for(int rr = 0; rr < r+1; rr++) {
                hist[rr] = board[rr][c] ? hist[rr] + 1 : 0;
            }
            int prev = c == 0 ? 0 : ul[r][c-1];
            ul[r][c] = max(prev, solveHist(hist, r+1));
        }
    }

    //ur, rotate mat by 90 ccw
    for(int r = 0; r < m; r++) {
        memset(hist, 0, sizeof hist);
        for(int c = n-1; c >= 0; c--) {
            for(int rr = 0; rr < r+1; rr++) {
                hist[rr] = board[rr][c] ? hist[rr] + 1 : 0;
            }
            int prev = c == n-1 ? 0 : ur[r][c+1];
            ur[r][c] = max(prev, solveHist(hist, r+1));
        }
    }

    //ll
    for(int r = m-1; r >= 0; r--) {
        memset(hist, 0, sizeof hist);
        for(int c = 0; c < n; c++) {
            for(int rr = r; rr < m; rr++) {
                hist[m-rr-1] = board[rr][c] ? hist[m-rr-1] + 1 : 0;
            }
            int prev = c == 0 ? 0 : ll[r][c-1];
            ll[r][c] = max(prev, solveHist(hist, m-r));
        }
    }

    //lr
    for(int r = m-1; r >= 0; r--) {
        memset(hist, 0, sizeof hist);
        for(int c = n-1; c >= 0; c--) {
            for(int rr = r; rr < m; rr++) {
                hist[m-rr-1] = board[rr][c] ? hist[m-rr-1] + 1 : 0;
            }
            int prev = c == n-1 ? 0 : lr[r][c+1];
            lr[r][c] = max(prev, solveHist(hist, m-r));
        }
    }
}

int solveStripes(){
    int ans = 0;

    for(int i = 1; i<=3;i++){
        memset(dp[i], 0, sizeof dp[i]);
    }
    //dp[i][j] = max area if row 0->j & i rectangles
    for(int rec = 1 ; rec <= 3; rec++) {
        for(int r = 0; r < m; r++) {
            if(r == 0) {
                if(rec == 1) {
                    dp[rec][r] = bestRow[0][0];
                }
            }else{
                for(int prevRow = 0; prevRow < r; prevRow++) {
                    dp[rec][r] = max(dp[rec-1][prevRow] + bestRow[prevRow+1][r], dp[rec][r]); 
                }
            }
            ans = max(ans, dp[rec][r]);
        }
    }
    for(int i = 1; i<=3;i++){
        memset(dp[i], 0, sizeof dp[i]);
    }
    //dp[i][j] = max area if col 0->j & i rectangles
    for(int rec = 1 ; rec <= 3; rec++) {
        for(int c = 0; c < n; c++) {
            if(c == 0) {
                if(rec == 1) {
                    dp[rec][c] = bestCol[0][0];
                }
            }else{
                for(int prevCol = 0; prevCol < c; prevCol++) {
                    dp[rec][c] = max(dp[rec-1][prevCol] + bestCol[prevCol+1][c], dp[rec][c]); 
                }
            }
            ans = max(ans, dp[rec][c]);
        }
    }
    return ans;
}

int solveHard() {
    int ans = 0;
    for(int rCut = 0; rCut < m-1; rCut++){
        for(int cCut = 0; cCut < n-1; cCut++) {
            //left = 1 piece, right = 2 pieces
            ans = max(ans, bestCol[0][cCut] + ur[rCut][cCut+1] + lr[rCut+1][cCut+1]);

            //right = 1 piece, left = 2 pieces
            ans = max(ans, bestCol[cCut+1][n-1] + ul[rCut][cCut] + ll[rCut+1][cCut]);

            //top = 1 piece, bottom = 2 pieces
            ans = max(ans, bestRow[0][rCut] + ll[rCut+1][cCut] + lr[rCut+1][cCut+1]);

            //top = 2 pieces, bottom = 1 piece
            ans = max(ans, bestRow[rCut+1][m-1] + ul[rCut][cCut] + ur[rCut][cCut+1]);
        }
    }

    return ans;
}

void initLineCuts() {
    for(int topR = 0; topR < m; topR++) {
        memset(hist, 0, sizeof hist);
        for(int botR = topR; botR < m; botR++){
            for(int c = 0; c < n; c++) {
                hist[c] = board[botR][c] ? hist[c] + 1 : 0;
                bestRow[topR][botR] = solveHist(hist, n);
            }
        }
    }

    for(int leftC = 0; leftC < n; leftC++) {
        memset(hist, 0, sizeof hist);
        for(int rightC = leftC; rightC < n; rightC++){
            for(int r = 0; r < m; r++) {
                hist[r] = board[r][rightC] ? hist[r] + 1 : 0;
                bestCol[leftC][rightC] = solveHist(hist, m);
            }
        }
    }
}

int solve() {
    int ans = 0;

    if(m * n < 3) {
        for(int r = 0 ; r < m; r++){
            for(int c = 0 ; c < n; c++){
                if(board[r][c]) ans++;
            }   
        }
        return ans;
    }

    initCorners();
    initLineCuts();
    ans = max(ans, solveStripes());
    ans = max(ans, solveHard());

    return ans;
}

int main(){
    while(true) {
        scanf("%d %d", &m, &n);
        if(m == 0 && n == 0) break;

        for(int r = 0 ; r < m; r++){
            scanf("%s", line);
            for(int c = 0 ; c < n; c++){
                board[r][c] = line[c] == '.';
            }   
        }

        printf("%d\n",solve());
    }

    return 0;
}
 */
}
