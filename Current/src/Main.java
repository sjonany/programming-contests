import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class Main {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int n = Integer.parseInt(in.readLine());
		int[] nums = new int[n];
		for(int i = 0 ; i < n ;i++){
			nums[i] = Integer.parseInt(in.readLine());
		}
		
		out.println(solve(nums));
		
		out.close();
	}
	
	static int solve(int[] nums) {
		int N = nums.length;
		Arrays.sort(nums);
		int[][] C = new int[N][N];
		List<Integer>[] slouts = (List<Integer>[]) Array.newInstance(List.class, N);
		for(int i = 0 ; i < N; i++){
			slouts[i] = new ArrayList<Integer>();
		}
		for(int i = 0; i < N; i++){
			for(int j = i+1; j < N; j++){
				int g = gcd(nums[i], nums[j]);
				if(g!=1){
					slouts[i].add(j);
					slouts[j].add(i);
					C[i][j] = g;
					C[j][i] = g;
				}
			}	
		}
		int[][] outEdges = new int[N][];
		for(int i = 0 ; i < N; i++){
			outEdges[i] = new int[slouts[i].size()];
			for(int j = 0; j < slouts[i].size(); j++){
				outEdges[i][j] = slouts[i].get(j);
			}
		}
		
		return dinic(C, outEdges, N, 0, N-1);
	}
	
	static int gcd(int a, int b){
		while(b>0){
			int temp = b;
			b = a%b;
			a = temp;
		}
		return a;
	}

	public static int dinic(int[][] C, int[][] outEdges, int numNode, int source, int sink){
		Queue<Integer> q = new LinkedList<Integer>();
		int[] l = new int[numNode];
		int[][] F = new int[numNode][numNode];
		int total = 0;
		while (true) {
			q.offer(source);
			Arrays.fill(l,-1);      
			l[source]=0;
			while (!q.isEmpty()) {
				int u = q.remove();
				for (int v: outEdges[u]) {
					if (l[v]==-1 && C[u][v] > F[u][v]) {
						l[v] = l[u]+1;
						q.offer(v);
					}
				}
			}
			if (l[sink]==-1)              
				return total;
			total += dinic(C, outEdges, numNode, source,sink, F,l, Integer.MAX_VALUE);
		}		
	}

	public static int dinic(int[][] C, int[][] outEdges, int numNode, int u, int t, int[][]F, int[]l, int val) {
		if (val<=0)
			return 0;
		if (u==t)  
			return val;
		int a=0, av;                               
		for (int v: outEdges[u]) 
			if (l[v]==l[u]+1 && C[u][v] > F[u][v]) {
				av = dinic(C, outEdges, numNode, v,t, F,l, Math.min(val-a, C[u][v]-F[u][v]));
				F[u][v] += av;
				F[v][u] -= av;
				a += av;
			}
		if (a==0) //                                         
			l[u] = -1;
		return a;
	}

}
