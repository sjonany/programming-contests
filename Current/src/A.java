import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class A {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int n = Integer.parseInt(in.readLine());
		for(int i = 0 ; i< n;i++){
			String n1 = in.readLine();
			StringTokenizer tk  = new StringTokenizer(in.readLine());
			double x1 = Double.parseDouble(tk.nextToken());
			double y1 = Double.parseDouble(tk.nextToken());
			double z1 = Double.parseDouble(tk.nextToken());
			String n2 = in.readLine();
			tk  = new StringTokenizer(in.readLine());
			double x2 = Double.parseDouble(tk.nextToken());
			double y2 = Double.parseDouble(tk.nextToken());
			double z2 = Double.parseDouble(tk.nextToken());
			
			double dx = x1-x2;
			double dy = y1-y2;
			double dz = z1-z2;
			double ans = Math.sqrt(dx*dx+dy*dy+dz*dz);
			out.println(String.format("%s to %s: %.2f", n1, n2, ans));
		}
		
		out.close();
	}
}
