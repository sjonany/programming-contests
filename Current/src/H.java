import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;


public class H {
	
	static List<Rule> rulers;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		int n = Integer.parseInt(in.readLine().trim());
		rulers = new ArrayList<Rule>();
		for(int i = 0; i < n; i++){
			String name = in.readLine().trim();
			tk = new StringTokenizer(in.readLine());
			Rule r = new Rule(name,(int) (Double.parseDouble(tk.nextToken())*10),(int) (Double.parseDouble(tk.nextToken())*10));
			rulers.add(r);
		}
		
		Collections.sort(rulers, new Comparator<Rule>(){
			@Override
			public int compare(Rule o1, Rule o2) {
				return o1.t1 - o2.t1;
			}
		});
		
		int c = Integer.parseInt(in.readLine().trim());
		for(int i= 0 ; i < c; i++){
			int t = Integer.parseInt(in.readLine().trim());
			int t1 = t * 10;
			int t2 = t1 + 9;
			
			List<String> names = new ArrayList<String>();
			for(Rule r : rulers){
				if(!(t1 > r.t2 || t2 < r.t1)){
					names.add(r.name);
				}
			}
			
			StringBuilder sb = new StringBuilder();
			sb.append(String.format("Galactic year %d: ", t));
			if(names.size() == 0){
				sb.append("None");
			}
			for(int j = 0; j < names.size(); j++){
				sb.append(names.get(j));
				if(j != names.size()-1){
					sb.append(", ");
				}
			}
			out.println(sb);
		}
		
		out.close();
	}
	
	static class Rule{
		public String name;
		public Rule(String name, int t1, int t2) {
			this.name = name;
			this.t1 = t1;
			this.t2 = t2;
		}
		public int t1;
		public int t2;
		
		
	}
}
