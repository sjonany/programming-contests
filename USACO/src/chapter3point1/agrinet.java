package chapter3point1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: agrinet
 */
public class agrinet {
	static int N;
	static int[][] adjMap;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("agrinet.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("agrinet.out")));
		
		N = Integer.parseInt(f.readLine());
		String line = f.readLine();
		
		adjMap = new int[N][N];
		int ii = 0;
		while(line != null && line.length()!=0){
			String[] toks = line.split(" ");
			for(int j=0;j<toks.length;j++){
				adjMap[ii/N][ii%N] = Integer.parseInt(toks[j]);
				ii++;
			}
			line = f.readLine();
		}
		
		/*for(int i=0;i<N;i++){
			System.out.println(Arrays.toString(adjMap[i]));
		}*/
		
		//prim
		int[] distance = new int[N];
		for(int i=0;i<N;i++){
			distance[i] = 100001;
		}
		
		int mstWeight = 0;
		boolean[] isInCloud = new boolean[N];
		isInCloud[0] = true;
		distance[0] = 0;
		
		int cloudSize = 1;
		for(int j=1;j<N;j++){
			distance[j] = adjMap[0][j];
		}
		
		while(cloudSize < N){
			int minCost = 100001;
			int nextNode = 0;
			for(int i=0;i<N;i++){
				if(!isInCloud[i] && distance[i] < minCost){
					minCost = distance[i];
					nextNode = i;
				}
			}
			
			isInCloud[nextNode] = true;
			mstWeight += minCost;
			
			for(int j=0;j<N;j++){
				if(distance[j] > adjMap[nextNode][j]){
					distance[j] = adjMap[nextNode][j];
				}
			}
			cloudSize++;
		}
		
		out.println(mstWeight);
		out.close();
	}
}