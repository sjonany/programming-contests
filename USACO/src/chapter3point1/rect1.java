package chapter3point1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/*
ID: xenoslash
LANG: JAVA
TASK: rect1
 */
public class rect1 {
	static int A;
	static int B;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("rect1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("rect1.out")));
		
		String[] toks = f.readLine().split(" ");
		
		//A wide, B long
		A = Integer.parseInt(toks[0]);
		B = Integer.parseInt(toks[1]);
		int N = Integer.parseInt(toks[2]);
		
		List<Rectangle> board = new ArrayList<Rectangle>();
		board.add(new Rectangle(0,B-1,A-1,0,1));
		//printBoard(board);
		
		for(int i=0;i<N;i++){
			toks =  f.readLine().split(" ");
			int llx = Integer.parseInt(toks[0]);
			int lly = Integer.parseInt(toks[1]);
			int urx = Integer.parseInt(toks[2]);
			int ury = Integer.parseInt(toks[3]);
			int color = Integer.parseInt(toks[4]);
			
			Rectangle newRect =  new Rectangle(llx, B-lly-1, urx-1, B-ury, color);
			List<Rectangle> newBoard = new ArrayList<Rectangle>();
			newBoard.add(newRect);
			for(Rectangle r: board){
				List<Rectangle> splits = r.getSplit(newRect);
				for(Rectangle rs: splits){
					newBoard.add(rs);
				}
			}
			board = newBoard;
			//printBoard(board);
		}
		
		int[] area = new int[2501];
		for(Rectangle r: board){
			area[r.color] += r.getArea();
		}
		
		for(int i=1;i<=2500;i++){
			if(area[i] != 0){
				out.println(i + " " + area[i]);
			}
		}
		out.close();
	}
	
	public static void printBoard(List<Rectangle> board){
		int[][] colors = new int[B][A];
		int count = 0;
		for(Rectangle r:board){
			for(int row=r.ury; row<=r.lly; row++){
				for(int col=r.llx; col<=r.urx; col++){
					if(colors[row][col]!=0){
						throw new IllegalArgumentException("REPAINT ERROR!");
					}
					colors[row][col] = r.color;
					count++;
				}
			}
		}
		if(count != A*B){
			throw new IllegalArgumentException("NOT ALL COLORED");
		}
		for(int row=0;row<B;row++){
			for(int col=0;col<A;col++){
				System.out.print(colors[row][col]);
			}
			System.out.println();
		}
		System.out.println();
	}
	public static class Rectangle{
		public int llx;
		public int lly;
		public int urx;
		public int ury;
		public int color;
		
		public Rectangle(int llx, int lly, int urx, int ury, int color){
			this.llx = llx;
			this.lly = lly;
			this.urx = urx;
			this.ury = ury;
			this.color = color;
		}
		
		public int getArea(){
			return (urx-llx+1) * (lly- ury+1);
		}
		
		public List<Rectangle> getSplit(Rectangle newRect){
			List<Rectangle> splits = new ArrayList<Rectangle>();
			if(llx <= newRect.urx && urx >= newRect.llx &&
					ury <= newRect.lly && lly >= newRect.ury){
				//intersection
				Rectangle in = new Rectangle(
						Math.max(llx,  newRect.llx), Math.min(lly, newRect.lly),
						Math.min(urx, newRect.urx), Math.max(ury, newRect.ury),-1);
				
				//UL
				Rectangle ul = new Rectangle(llx, in.ury-1, in.llx-1, ury, color);
				if(ul.isNonEmpty()){
					splits.add(ul);
				}
				
				//U
				Rectangle u = new Rectangle(in.llx, in.ury-1, in.urx, ury, color);
				if(u.isNonEmpty()){
					splits.add(u);
				}
				
				//UR
				Rectangle ur = new Rectangle(in.urx+1, in.ury-1, urx, ury, color);
				if(ur.isNonEmpty()){
					splits.add(ur);
				}
				
				//R
				Rectangle r = new Rectangle(in.urx+1, in.lly, urx, in.ury, color);
				if(r.isNonEmpty()){
					splits.add(r);
				}
				
				
				//BR
				Rectangle br = new Rectangle(in.urx+1, lly, urx, in.lly+1, color);
				if(br.isNonEmpty()){
					splits.add(br);
				}
				
				//B
				Rectangle b = new Rectangle(in.llx, lly, in.urx, in.lly+1, color);
				if(b.isNonEmpty()){
					splits.add(b);
				}
				
				
				//BL
				Rectangle bl = new Rectangle(llx, lly, in.llx-1, in.lly+1, color);
				if(bl.isNonEmpty()){
					splits.add(bl);
				}
				
				//L
				Rectangle l = new Rectangle(llx, in.lly, in.llx-1, in.ury, color);
				if(l.isNonEmpty()){
					splits.add(l);
				}
				
				return splits;
				
			}else{
				//no intersection
				splits.add(this);
				return splits;
			}
		}
		
		public boolean isNonEmpty(){
			return this.getArea() > 0;
		}
	}
}