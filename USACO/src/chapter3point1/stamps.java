package chapter3point1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: stamps
 */
//passes, but is not that efficient. please see stamps2
public class stamps {
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("stamps.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("stamps.out")));
		//long start = System.nanoTime();
		StringTokenizer toks = new StringTokenizer(f.readLine());
		int K  = Integer.parseInt(toks.nextToken());
		int N = Integer.parseInt(toks.nextToken());
		
		StringTokenizer sz = new StringTokenizer(f.readLine());
		int[] stamps = new int[N];
		int maxStamp = 0;
		for(int i=0;i<N;i++){
			stamps[i] = Integer.parseInt(sz.nextToken());
			maxStamp = Math.max(stamps[i], maxStamp);
			if(!sz.hasMoreTokens() && i!=N-1){
				sz = new StringTokenizer(f.readLine());
			}
		}
		
		//isReachable[i] = values reachable with exactly i stamps
		Set<Integer> isReachable = new HashSet<Integer>();
		boolean[] canReach = new boolean[maxStamp*K+1];
		canReach[0] = true;
		isReachable.add(0);
		
		for(int i=1; i<= K ;i++){
			Set<Integer> nextReachable = new HashSet<Integer>();
			for(Integer reached : isReachable){
				for(int stamp : stamps){
					int nextReach = reached + stamp;
					if(!canReach[nextReach]){
						canReach[nextReach] = true;
						nextReachable.add(nextReach);
					}
				}
			}
			isReachable = nextReachable;
		}
		
		int maxRight = 0;
		for(; maxRight<canReach.length;maxRight++){
			if(!canReach[maxRight]){
				break;
			}
		}
		
		out.println(maxRight-1);
		//long end = System.nanoTime();
		//System.out.println((end-start)/1000000000.0);
		out.close();
	}
}