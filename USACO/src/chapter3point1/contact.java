package chapter3point1;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

/*
ID: xenoslash
LANG: JAVA
TASK: contact
 */
public class contact {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("contact.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("contact.out")));
		
		StringBuffer sb = new StringBuffer();
		int A;
		int B;
		int N;
		String[] toks = f.readLine().split(" ");
		A = Integer.parseInt(toks[0]);
		B = Integer.parseInt(toks[1]);
		N = Integer.parseInt(toks[2].trim());
		
		String line = f.readLine();
		while(line != null && line.trim().length()!=0){
			sb.append(line);
			line = f.readLine();
		}
		
		Map<String, Integer> count = new HashMap<String, Integer>();
		//i = where substring exactly ends
		for(int i=A-1; i<sb.length(); i++){
			//j=where substring starts
			for(int j = Math.max(0,i-B+1); i-j+1>=A; j++){
				String sub = sb.substring(j, i+1);
				Integer c= count.get(sub);
				if(c == null){
					c = 1;
				}else{
					c = c+1;
				}
				count.put(sub, c);
			}	
		}
		
		Map<Integer, PriorityQueue<String>> occPatt = new HashMap<Integer, PriorityQueue<String>>();
		for(Entry<String, Integer> entry : count.entrySet()){
			PriorityQueue<String> patts = occPatt.get(entry.getValue());
			if(patts == null){
				patts = new PriorityQueue<String>(50, 
						new Comparator<String>(){
							@Override
							public int compare(String o1, String o2) {
								if(o1.length() != o2.length()){
									return o1.length() - o2.length();
								}else{
									return o1.compareTo(o2);
								}
							}
					
				});
				occPatt.put(entry.getValue(), patts);
			}
			patts.add(entry.getKey());
		}
		
		PriorityQueue<Entry<Integer, PriorityQueue<String>>> res = 
				new PriorityQueue<Entry<Integer, PriorityQueue<String>>>(50,
						new Comparator<Entry<Integer, PriorityQueue<String>>>(){
					@Override
					public int compare(
							Entry<Integer, PriorityQueue<String>> o1,
							Entry<Integer, PriorityQueue<String>> o2) {
						return o2.getKey().compareTo(o1.getKey());
					}	
				});
		for(Entry<Integer, PriorityQueue<String>> ent : occPatt.entrySet()){
			res.add(ent);
		}
		
		for(int i=0;i<N;i++){
			if(res.isEmpty()){
				break;
			}
			Entry<Integer, PriorityQueue<String>> ent = res.remove();
			out.println(ent.getKey());
			int countPrint = 0;
			PriorityQueue<String> q = ent.getValue();
			StringBuffer sbq = new StringBuffer();
			while(!q.isEmpty()){
				sbq.append(q.remove());
				countPrint++;
				if(countPrint %6 == 0 && !q.isEmpty()){
					sbq.append("\n");
				}else{
					if(!q.isEmpty()){
						sbq.append(" ");
					}
				}
			}
			out.println(sbq);
		}
		out.close();
	}
}