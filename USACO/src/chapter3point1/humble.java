package chapter3point1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: humble
 */
public class humble {
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		BufferedReader f = new BufferedReader(new FileReader("humble.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("humble.out")));
		String[] toks = f.readLine().split(" ");
		int K = Integer.parseInt(toks[0]);
		int N = Integer.parseInt(toks[1]);
		
		long[] S = new long[K];
		StringTokenizer tz = new StringTokenizer(f.readLine());
		
		for(int i=0;i<K;i++){
			S[i] = Integer.parseInt(tz.nextToken());
		}
		
		//humble[i] = the ith humble number
		long[] humble = new long[N+1];
		humble[0] = 1;
		
		//previous Lo for each S
		int[] prevLo = new int[K];
		for(int i=1;i<=N;i++){
			long minClosest = Long.MAX_VALUE;
			//derp :X no need to binsearch. can guarantee that always 1 increment at most,
			//since we are building minClosest incrementally
			/*
			for(int j=0;j<K;j++){
				long s = S[j];
				double below = 1.0 * humble[i-1] / s;
				//bin search for smallest min[<i] > below
				int hi = i-1;
				int lo = prevLo[j];
				if(humble[lo]>below){
					lo = lo-1;
				}else{
					while(lo < hi){
						int mid = lo + (hi-lo+1)/2;
						if(humble[mid] <= below){
							lo = mid;
						}else{
							hi = mid-1;
						}
					}
				}
				//lo+1 contains the smallest[<i]>below
				
				if(minClosest == -1 || humble[lo+1]*s < minClosest){
					minClosest = humble[lo+1]*s;
				}
				prevLo[j] = Math.max(0,lo);
			}*/
			
			for(int j=0;j<K;j++){
				if(humble[prevLo[j]] * S[j] <= humble[i-1]){
					prevLo[j]++;
				}
				minClosest = Math.min(minClosest, humble[prevLo[j]] * S[j]);
			}
			
			humble[i] = minClosest;
		}
		
		//System.out.println(Arrays.toString(humble));
		out.println(humble[N]);
		out.close();
	}
}