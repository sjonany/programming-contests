package chapter3point1;




import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: inflate
 */
public class inflate {
	static int M;
	static int N;
	static Problem[] problems;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("inflate.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("inflate.out")));
		
		StringTokenizer toks = new StringTokenizer(f.readLine());
		M = Integer.parseInt(toks.nextToken());
		N = Integer.parseInt(toks.nextToken());
		problems = new Problem[N];
		
		for(int i=0;i<N;i++){
			StringTokenizer sz = new StringTokenizer(f.readLine());
			problems[i] = (new Problem(Integer.parseInt(sz.nextToken()),Integer.parseInt(sz.nextToken())));
		}
		//max points if i minutes  = dp[i]
		int[] dp = new int[M+1];
		for(int m = 1; m<=M; m++){
			for(int i=0;i<N;i++){
				if(m - problems[i].minute >= 0){
					dp[m] = Math.max(dp[m], problems[i].point + dp[m-problems[i].minute]);
				}
			}
		}
		/*
		Arrays.sort(problems);
		//System.out.println(problems);
		long sum = 0;
		for(Problem p: problems){
			int rep = M / p.minute;
			sum += rep * p.point;
			M -= rep * p.minute;
		}*/
		
		out.println(dp[M]);
		out.close();
	}
	
	public static class Problem implements Comparable<Problem>{ 
		public int point;
		public int minute;
		
		public Problem(int p, int m){
			this.point = p;
			this.minute = m;
		}
		
		
		public double efficiency(){
			return 1.0 * point/minute;
		}

		@Override
		public int compareTo(Problem o) {
			return this.efficiency() > o.efficiency() ? -1 : 1;
		}
	}
}