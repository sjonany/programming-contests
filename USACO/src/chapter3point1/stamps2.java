package chapter3point1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: stamps
 */

//more efficient solution using dp
public class stamps2 {
	
	public static void main(String[] args) throws Exception{
		BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
	   
		//BufferedReader f = new BufferedReader(new FileReader("stamps.in"));
		//PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("stamps.out")));
		//long start = System.nanoTime();
		StringTokenizer toks = new StringTokenizer(f.readLine());
		int K  = Integer.parseInt(toks.nextToken());
		int N = Integer.parseInt(toks.nextToken());
		
		StringTokenizer sz = new StringTokenizer(f.readLine());
		int[] stamps = new int[N];
		int maxStamp = 0;
		for(int i=0;i<N;i++){
			stamps[i] = Integer.parseInt(sz.nextToken());
			maxStamp = Math.max(stamps[i], maxStamp);
			if(!sz.hasMoreTokens() && i!=N-1){
				sz = new StringTokenizer(f.readLine());
			}
		}
		
		//minStamp[i] = min number of stamps to get val = i 
		int[] minStamp = new int[maxStamp * K +1];
		minStamp[0] = 0;
		for(int i=1;i<minStamp.length;i++){
			int newMin = Integer.MAX_VALUE;
			for(int stamp: stamps){
				int prevVal = i-stamp;
				if(prevVal >= 0){
					newMin = Math.min(newMin, minStamp[prevVal]+1);
				}
			}
			
			if(newMin > K){
				out.println(i-1);
				out.close();
				return;
			}else{
				minStamp[i] = newMin;
			}
		}
		

		out.println(maxStamp*K);
		out.close();
		return;
		
	}
}