package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: friday
 */
public class friday {
	static int year = 1900;
	static int month = 1;
	static int date = 1;
	static int[] countDays = new int[7];
	//MONDAY
	static int day = 0;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("friday.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("friday.out")));
		int N = Integer.parseInt(f.readLine());
		int endYear = 1900 + N - 1;
		int endMonth = 12;
		int endDate = 31;
		
		while(!(year == endYear && month == endMonth && date == endDate)){
			if(date == 13){
				countDays[day]++;
			}
			date++;
			
			int numDays = 31;
			switch(month){
			case 4:
			case 6:
			case 9:
			case 11:
				numDays = 30;
				break;
			case 2:
				numDays = 28;
				if(year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0)){
					numDays = 29;
				}
				break;
			}
			if(date > numDays ){
				date = 1;
				month++;
				if(month == 13){
					month = 1;
					year++;
				}
			}
			
			day = (day+1) % 7;
		}
		
		for(int i=5; i<7;i++){
			out.print(countDays[i] + " ");
		}
		for(int i=0; i<5;i++){
			if(i!=4){
				out.print(countDays[i] + " ");
			}else{
				out.print(countDays[i]);
			}
		}
		out.println();
		out.close();
	}
}
