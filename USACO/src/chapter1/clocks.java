package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
/*
ID: xenoslash
LANG: JAVA
TASK: clocks
 */
public class clocks {
	static BigInteger minMoveString = new BigInteger("111122223333444455556666777788889999");
	static int minMoveCount = Integer.MAX_VALUE;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("clocks.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("clocks.out")));
		int[] clocks = new int[9];
		
		String[] tok1 = f.readLine().split(" ");
		clocks[0] = Integer.parseInt(tok1[0]);	
		clocks[1] = Integer.parseInt(tok1[1]);		
		clocks[2] = Integer.parseInt(tok1[2]);			
		
		String[] tok2 = f.readLine().split(" ");
		clocks[3] = Integer.parseInt(tok2[0]);	
		clocks[4] = Integer.parseInt(tok2[1]);		
		clocks[5] = Integer.parseInt(tok2[2]);			
		
		String[] tok3 = f.readLine().split(" ");
		clocks[6] = Integer.parseInt(tok3[0]);	
		clocks[7] = Integer.parseInt(tok3[1]);		
		clocks[8] = Integer.parseInt(tok3[2]);		
		
		dfs(1, new int[10], clocks);
		String s = minMoveString.toString();
		for(int i=0;i<s.length();i++){
			out.print(s.charAt(i));
			if(i!=s.length()-1){
				out.print(" ");
			}else{
				out.println();
			}
		}
		out.close();
	}
	
	public static void dfs(int numToSet, int[] moves, int[] clocks){
		if(numToSet == 10){
			boolean isGoal = true;
			for(int hand : clocks){
				if(hand != 12){
					isGoal = false;
					break;
				}
			}
			
			if(isGoal){
				String code = "";
				for(int moveCode = 1; moveCode<=9;moveCode++){
					for(int rep=1;rep<=moves[moveCode];rep++){
						code += moveCode;
					}
				}
				if(code.length() > minMoveCount){
					return;
				}
				BigInteger encry = new BigInteger(code);
				if(encry.compareTo(minMoveString) < 0){
					minMoveString = encry;
					minMoveCount = code.length();
				}
			}
		}else{
			for(int rep=0;rep<=3;rep++){
				moves[numToSet] = rep;
				for(int i=1;i<=rep;i++){
					move(clocks, numToSet);
				}
				dfs(numToSet+1, moves, clocks);
				for(int i=1;i<=4-rep;i++){
					move(clocks, numToSet);
				}
			}
		}
	}
	
	
	public static void bfs(int[] clocks){/*
	//bfs
		Queue<Integer[]> moveQ = new LinkedList<Integer[]>();
		Queue<Integer[]> clockQ = new LinkedList<Integer[]>();
		Queue<Integer> moveCountQ = new LinkedList<Integer>();
		moveQ.add(new Integer[]{0,0,0,0,0,0,0,0,0,0});
		moveCountQ.add(0);
		Integer[]clockC = new Integer[9];
		for(int i=0;i<9;i++){
			clockC[i] = clocks[i];
		}
		clockQ.add(clockC);
		while(true){
			Integer[] parentMov = moveQ.remove();
			Integer[] parentClock = clockQ.remove();
			Integer moveCount = moveCountQ.remove();
			
			if(moveCount > minMoveCount){
				return;
			}
			for(int move=1;move<=9;move++){
				if(parentMov[move] < 3){
					Integer[] nextMove = Arrays.copyOf(parentMov, 10);
					nextMove[move]++;
					Integer[] nextClock = Arrays.copyOf(parentClock, 9);
					move(nextClock, move);
					Integer nextMoveCount = moveCount+1;
					boolean isGoal = false;
					for(int hand : nextClock){
						if(hand != 12){
							isGoal = false;
							break;
						}
					}
					
					if(isGoal){
						String code = "";
						for(int moveCode = 1; moveCode<=9;moveCode++){
							for(int rep=1;rep<=nextMove[moveCode];rep++){
								code += moveCode;
							}
						}

						minMoveCount = nextMoveCount;
						int encry = Integer.parseInt(code);
						if(encry < minMoveString){
							minMoveString = encry;
						}
					}
					moveQ.add(nextMove);
					clockQ.add(nextClock);
					moveCountQ.add(nextMoveCount);
				}
			}
		}*/
		/*
		 *
		//idfs
		if(numMove == 0){
			boolean isGoal = true;
			for(int hand : clocks){
				if(hand != 12){
					isGoal = false;
					break;
				}
			}
			
			if(isGoal){
				String code = "";
				for(int moveCode = 1; moveCode<=9;moveCode++){
					for(int rep=1;rep<=moveLog[moveCode];rep++){
						code += moveCode;
					}
				}
				int encry = Integer.parseInt(code);
				if(encry < minMoveString){
					minMoveString = encry;
				}
			}
		}else{
			for(int moveCode=1; moveCode<=9;moveCode++){
				if(moveLog[moveCode] < 4){
					moveLog[moveCode]++;
					move(clocks, moveCode);
					idfs(moveLog, numMove-1, clocks);
					//uncheck
					move(clocks, moveCode);
					move(clocks, moveCode);
					move(clocks, moveCode);
					moveLog[moveCode]--;
				}
			}
		}*/
	}
	
	static int[][]moves = new int[][]{
		{},//0
		{0,1,3,4},//ABDE
		{0,1,2},//ABC
		{1,2,4,5},//BCEF
		{0,3,6},//ADG
		{1,3,4,5,7},//BDEFH
		{2,5,8},//CFI
		{3,4,6,7},//DEGH
		{6,7,8},//GHI
		{4,5,7,8}//EFHI
	};
	
	public static void move(int[] clocks, int move){
		int[] affected = moves[move];
		for(int i: affected){
			switch(clocks[i]){
			case 12:
				clocks[i] = 3;
				break;
			case 3: 
				clocks[i]=6;
				break;
			case 6: 
				clocks[i] = 9;
				break;
			case 9:
				clocks[i] = 12;
				break;
			}
		}
	}
}
