package chapter1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: sprime
 */
public class sprime{
	static int N;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("sprime.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("sprime.out")));
		N = Integer.parseInt(f.readLine());
		sPrime(1, ""+2,out);
		sPrime(1, ""+3,out);
		sPrime(1, ""+5,out);
		sPrime(1, ""+7,out);
		out.close();
	}
	
	public static void sPrime(int curDig, String num, PrintWriter out){
		if(curDig == N){
			out.println(num);
		}else{
			for(int i=1;i<=9;i++){
				int sprime = Integer.parseInt(num+i);
				if(isPrime(sprime)){
					sPrime(curDig+1, "" + sprime, out);
				}
			}
		}
	}
	
	public static boolean isPrime(int n){
		if(n==1) return false;
		if(n==2) return true;
		if(n%2 == 0) return false;
		for(int i=3; i*i <=n; i+=2){
			if(n%i == 0)
				return false;
		}
		
		return true;
	}
}
