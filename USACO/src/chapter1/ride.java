package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: ride
 */
public class ride {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("ride.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ride.out")));
	    String comet = f.readLine().trim();
	    String group = f.readLine().trim();
	       
	    if(getNum(comet) == getNum(group)){
	    	out.println("GO");
	    }else{
	    	out.println("STAY");
	    }
	    f.close();
	    out.close();                                 
	    System.exit(0);                               
	}
	
	public static int getNum(String s){
		int c = 1;
		for(int i=0; i<s.length(); i++){
			c *= (s.charAt(i)-'A'+1);
		}
		
		return c % 47;
	}
}
