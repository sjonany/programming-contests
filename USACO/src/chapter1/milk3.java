package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/*
ID: xenoslash
LANG: JAVA
TASK: milk3
 */

public class milk3 {
	static Set<String> past = new HashSet<String>();
	static PriorityQueue<Integer> results = new PriorityQueue<Integer>();
	static Set<Integer> resultsUn = new HashSet<Integer>();
	static int[] cap = new int[3];
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);

		BufferedReader f = new BufferedReader(new FileReader("milk3.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("milk3.out")));
		
		String[] toks = f.readLine().split(" " );
		for(int i=0;i<3;i++){
			cap[i] = Integer.parseInt(toks[i]);
		}
		resultsUn.add(cap[2]);
		results.add(cap[2]);
		
		int[] milk = new int[3];
		milk[2] = cap[2];
		past.add(conv(milk));
		dfs(milk);
		
		int size = results.size();
		for(int i=0;i<size;i++){
			if(i!=size-1){
				out.print(results.remove() + " ");
			}else{
				out.println(results.remove());
			}
		}
		out.close();
	}
	
	public static void dfs(int[] milk){
		for(int from=0;from<3;from++){
			for(int to=0;to<3;to++){
				if(from != to && milk[to] != cap[to] && milk[from] != 0){
					int transfer = Math.min(milk[from], cap[to]-milk[to]);
					milk[from] -= transfer;
					milk[to] += transfer;
					String saveState = conv(milk);
					if(!past.contains(saveState)){
						past.add(saveState);
						dfs(milk);
						if(milk[0]==0){
							int newRes = milk[2];
							if(!resultsUn.contains(newRes)){
								resultsUn.add(newRes);
								results.add(newRes);
							}
						}
					}
					milk[from] += transfer;
					milk[to] -= transfer;
				}
			}
		}
	}
	
	public static String conv(int[] state){
		String s="";
		for(int i=0;i<3; i++){
			if(state[i] < 10){
				s+= "" + 0 + state[i];
			}else{
				s+= state[i];
			}
		}
		return s;
	}
}
