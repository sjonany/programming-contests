package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: crypt1
 */
public class crypt1 {
	static int[] nums;
	static boolean[] isNum;
	static int count =0;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("crypt1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("crypt1.out")));
		int N = Integer.parseInt(f.readLine());
		nums = new int[N];
		isNum = new boolean[10];
		String[] toks = f.readLine().split(" ");
		for(int i=0;i<N;i++){
			nums[i] = Integer.parseInt(toks[i]);
			isNum[nums[i]] = true;
		}
		
		search(1,new int[5]);
		
		out.println(count);
		out.close();
	}
	
	/*
	 
	 0 1 2
	   3 4
	 */
	public static void search(int curDigit, int[] filled){
		if(curDigit == 6){
			if(isValid(filled)){
				count++;
			}
		}else{
			for(int i = 0; i < nums.length; i++){
				filled[curDigit-1] = nums[i];
				search(curDigit+1, filled);
			}
		}
		
	}
	
	public static boolean isValid(int[] filled){
		int first = filled[0] * 100 + filled[1] * 10 + filled[2];
		int firstPartial = first * filled[4];
		if(firstPartial >= 1000){
			return false;
		}
		if(!isNum[firstPartial %10] || !isNum[(firstPartial/10)%10] || !isNum[(firstPartial/100) %10]){
			return false;
		}
		
		int secondPartial = first * filled[3];
		if(secondPartial >= 1000){
			return false;
		}
		if(!isNum[secondPartial %10] || !isNum[(secondPartial/10)%10] || !isNum[(secondPartial/100) %10]){
			return false;
		}
		
		int finalMult = first * (filled[3] * 10 + filled[4]);
		if(finalMult>= 10000){
			return false;
		}
		if(!isNum[finalMult %10] || !isNum[(finalMult/10)%10] || !isNum[(finalMult/100) %10] || 
				!isNum[(finalMult/1000) %10]){
			return false;
		}
		return true;
	}
}
