package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: dualpal
 */
public class dualpal {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("dualpal.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("dualpal.out")));
	    
		String[] tokens = f.readLine().trim().split(" ");
		int N = Integer.parseInt(tokens[0]);
		int S = Integer.parseInt(tokens[1]);
		
		int cur = S + 1;
		for(int i=0; i<N; i++){
			while(!valid(cur)){
				cur++;
			}
			out.println(cur);
			cur++;
		}
	    out.close();                                  
	}
	
	public static boolean valid(int n){
		int countVal = 0;
		for(int base=2;base<=10;base++){
			if(isPal(convertFromDecToBase(n,base))){
				countVal++;
			}
			if(countVal == 2){
				return true;
			}
		}
		return false;
	}
	
	public static boolean isPal(String s){
		for(int i=0; i<s.length()/2; i++){
			if(s.charAt(i) != s.charAt(s.length()-i-1)){
				return false;
			}
		}
		return true;
	}
	
	public static String convertFromDecToBase(int n, int base){
		String s = "";
		while(n > 0){
			s = getChar(n % base) + s;
			n/=base;
		}
		return s;
	}
	
	public static String getChar(int n){
		if(n>=0 && n<=9){
			return "" + n;
		}else{
			return "" + (char)((n-10) + 'A');
		}
	}
	
}
