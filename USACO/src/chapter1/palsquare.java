package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: palsquare
 */
public class palsquare {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("palsquare.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("palsquare.out")));
	    
		int base = Integer.parseInt(f.readLine());
		for(int i=1;i<=300; i++){
			int sq = i*i;
			String sqStr = convertFromDecToBase(sq, base);
			if(isPal(sqStr)){
				String iStr = convertFromDecToBase(i, base);
				out.println(iStr + " " + sqStr);
			}
		}
	    out.close();                                  
	}
	
	public static boolean isPal(String s){
		for(int i=0; i<s.length()/2; i++){
			if(s.charAt(i) != s.charAt(s.length()-i-1)){
				return false;
			}
		}
		return true;
	}
	
	public static String convertFromDecToBase(int n, int base){
		String s = "";
		while(n > 0){
			s = getChar(n % base) + s;
			n/=base;
		}
		return s;
	}
	
	public static String getChar(int n){
		if(n>=0 && n<=9){
			return "" + n;
		}else{
			return "" + (char)((n-10) + 'A');
		}
	}
	
}
