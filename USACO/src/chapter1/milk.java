package chapter1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

/*
ID: xenoslash
LANG: JAVA
TASK: milk
 */
public class milk {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("milk.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("milk.out")));
		String[] toks = f.readLine().split(" ");
		int N = Integer.parseInt(toks[0]);
		int M = Integer.parseInt(toks[1]);
		pair[] arr = new pair[M];
		for(int i=0; i<M; i++){
			String line = f.readLine();
			String[] tokens = line.split(" ");
			pair p = new pair();
			p.price = Integer.parseInt(tokens[0]);
			p.cap = Integer.parseInt(tokens[1]);
			arr[i] = p;
		}
		
		Arrays.sort(arr, new Comparator<pair>(){
			@Override
			public int compare(pair o1, pair o2) {
				return o1.price - o2.price;
			}
		});
		
		long totalCost = 0;
		long totalCap = 0;
		for(int i=0; i<arr.length; i++){
			long capBought = Math.min(arr[i].cap, N-totalCap);
			totalCost += capBought * arr[i].price;
			totalCap += capBought;
			if(totalCap == N){
				break;
			}
		}
		
		out.println(totalCost);
		out.close();
	}
	
	public static class pair{
		int price;
		int cap;
		
		@Override
		public String toString(){
			return cap + " " + price;
		}
	}
}
