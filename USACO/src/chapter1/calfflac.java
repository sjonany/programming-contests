package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: calfflac
 */
public class calfflac {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);

		BufferedReader f = new BufferedReader(new FileReader("calfflac.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("calfflac.out")));
		String content = "";
		String line  = f.readLine();
		while(line != null){
			content += (line + "\n");
			line = f.readLine();
		}

		/*
		String preprocessedLine = "";
		List<Integer> indices = new ArrayList<Integer>();
		for(int i=0; i<content.length();i++){
			char lo = Character.toLowerCase(content.charAt(i));
			if(lo <= 'z' && lo >='a'){
				preprocessedLine += lo;
				indices.add(i);
			}
		}
		*/
		int maxLeft = 0;
		int maxRight = 0;
		int maxPalLength = 1;
		
		for(int center = 0; center<content.length(); center++){
			char lo = Character.toLowerCase(content.charAt(center));
			if(lo>'z' || lo<'a'){
				continue;
			}
			
			//odd
			int oddLength = 1;
			int oddLeft = center;
			int oddRight = center;
			
			while(true){
				//expand left
				int newOddLeft = oddLeft-1;
				for(; newOddLeft >=0; newOddLeft--){
					char oddLeftLo = Character.toLowerCase(content.charAt(newOddLeft));
					if(oddLeftLo <= 'z' && oddLeftLo >= 'a'){
						break;
					}
				}
				
				//expand right
				int newOddRight = oddRight+1;
				for(; newOddRight < content.length(); newOddRight++){
					char oddRightLo = Character.toLowerCase(content.charAt(newOddRight));
					if(oddRightLo <= 'z' && oddRightLo >= 'a'){
						break;
					}
				}
				
				if(newOddLeft == -1 || newOddRight == content.length() || 
						Character.toLowerCase(content.charAt(newOddLeft)) != 
						Character.toLowerCase(content.charAt(newOddRight))){
					break;
				}else{
					oddLength += 2;
					oddLeft = newOddLeft;
					oddRight = newOddRight;
				}
			}
			if(oddLength > maxPalLength){
				maxRight = oddRight;
				maxLeft = oddLeft;
				maxPalLength = oddLength;
			}

			//even
			int evenLength = 0;
			int evenLeft = center+1;
			int evenRight = center;
			
			while(true){
				//expand left
				int newEvenLeft = evenLeft-1;
				for(; newEvenLeft >=0; newEvenLeft--){
					char evenLeftLo = Character.toLowerCase(content.charAt(newEvenLeft));
					if(evenLeftLo <='z' && evenLeftLo>='a'){
						break;
					}
				}
				
				//expand right
				int newEvenRight = evenRight+1;
				for(; newEvenRight < content.length(); newEvenRight++){
					char evenRightLo = Character.toLowerCase(content.charAt(newEvenRight));
					if(evenRightLo <= 'z' && evenRightLo >= 'a'){
						break;
					}
				}
				
				if(newEvenLeft == -1 || newEvenRight == content.length() || 
						Character.toLowerCase(content.charAt(newEvenLeft)) != 
						Character.toLowerCase(content.charAt(newEvenRight))){
					break;
				}else{
					evenLength += 2;
					evenLeft = newEvenLeft;
					evenRight = newEvenRight;
				}
			}

			if(evenLength > maxPalLength){
				maxRight = evenRight;
				maxLeft = evenLeft;
				maxPalLength = evenLength;
			}
		}
		out.println(maxPalLength);
		out.println(content.substring(maxLeft, maxRight+1));
		out.close();
	}
}

