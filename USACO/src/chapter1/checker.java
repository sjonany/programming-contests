package chapter1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: checker
 */
public class checker{
	static int N;
	static int NUM_PRINT = 3;
	static int NUM_SOL = 0;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("checker.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("checker.out")));
		N = Integer.parseInt(f.readLine());
		place(new int[N], 0, new boolean[N], out, new boolean[2*N+1], new boolean[2*N+1]);
		out.println(NUM_SOL);
		out.close();
	}
	
	//row = next row to place
	//colUsed[row] = bit string that is 1 if col is used, 0 if not
	//cols[row] = where checker is placed in that row
	//posDiag = from upperleft to bottom right. 0 -N-1 = if edge is at left
	//	N-1 = top left corner
	//	N to 2N-2 for edges at up
	//negDiag = from 
	public static void place(int[] cols, int row, boolean[] colUsed, PrintWriter out,
			boolean[] negDiagUsed, boolean[] posDiagUsed){
		if(row == N){
			if(NUM_PRINT > 0){
				for(int i=0;i<cols.length;i++){
					if(i!=cols.length-1){
						out.print((cols[i]+1) + " ");
					}else{
						out.println(cols[i]+1);
					}
				}
				NUM_PRINT--;
			}
			NUM_SOL++;
		}else{
			for(int col=0;col<N; col++){
				int negDiagInd = row-col + N;
				int posDiagInd = row+col;
				
				if(!colUsed[col] && !posDiagUsed[posDiagInd] && !negDiagUsed[negDiagInd]){
					colUsed[col] = true;
					negDiagUsed[negDiagInd] = true;
					posDiagUsed[posDiagInd] = true;
					cols[row] = col;
					
					place(cols, row+1, colUsed, out, negDiagUsed, posDiagUsed);
					
					colUsed[col] = false;
					negDiagUsed[negDiagInd] = false;
					posDiagUsed[posDiagInd] = false;
				}
			}
		}
	} 
	
}
