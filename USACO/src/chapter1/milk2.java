package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

/*
ID: xenoslash
LANG: JAVA
TASK: milk2
 */
public class milk2 {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("milk2.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("milk2.out")));
		int N = Integer.parseInt(f.readLine());
		pair[] arr = new pair[N];
		for(int i=0; i<N; i++){
			String line = f.readLine();
			String[] tokens = line.split(" ");
			pair p = new pair();
			p.start = Integer.parseInt(tokens[0]);
			p.end = Integer.parseInt(tokens[1]);
			arr[i] = p;
		}
		
		Arrays.sort(arr, new Comparator<pair>(){
			@Override
			public int compare(pair o1, pair o2) {
				if(o1.start - o2.start != 0){
					return o1.start - o2.start;
				}
				
				return o1.end - o2.end;
			}
		});
		
		int maxNoWork = 0;
		int maxWork = 0;
		
		pair curPair = arr[0];
		maxWork = curPair.end - curPair.start;
		
		for(int i=1; i<arr.length; i++){
			if(arr[i].start > curPair.end){
				maxWork = Math.max(maxWork, curPair.end-curPair.start);
				maxNoWork = Math.max(maxNoWork, arr[i].start - curPair.end);
				curPair = arr[i];
			}else{
				curPair.end = Math.max(arr[i].end, curPair.end);
				maxWork = Math.max(maxWork, curPair.end-curPair.start);
			}
		}
		
		out.println(maxWork + " " + maxNoWork);
		out.close();
	}
	
	public static class pair{
		int start;
		int end;
		
		@Override
		public String toString(){
			return start + " " + end;
		}
	}
}
