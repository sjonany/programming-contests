package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.PriorityQueue;

/*
ID: xenoslash
LANG: JAVA
TASK: ariprog
 */
public class ariprog {
	static boolean biSquare[] = new boolean[125001];
	static int N;
	static int M;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("ariprog.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ariprog.out")));
		N = Integer.parseInt(f.readLine());
		M = Integer.parseInt(f.readLine());
		
		for(int p=0;p<=M; p++){
			for(int q=p; q<=M; q++){
				biSquare[p*p + q*q] = true;
			}
		}
		
		int maxSquare = 2 * M * M;
		boolean hasPrinted = false;
		for(int diff=1; diff<=maxSquare/(N-1);diff++){
			PriorityQueue<Integer> orderedQ = new PriorityQueue<Integer>();
			for(int start = 0; start <= diff; start++){
				int chain = 0;
				for(int i =start; i<=maxSquare; i+=diff){
					if(biSquare[i]){
						chain++;
						if(chain >= N){
							orderedQ.add(i-(N-1)*diff);
						}
					}else{
						chain = 0;
					}
				}
			}
			
			while(!orderedQ.isEmpty()){
				out.println(orderedQ.remove() + " " + diff);
				hasPrinted = true;
			}
		}
		if(!hasPrinted){
			out.println("NONE");
		}
		out.close();
	}
}
