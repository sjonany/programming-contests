package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;

/*
ID: xenoslash
LANG: JAVA
TASK: barn1
 */
public class barn1 {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("barn1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("barn1.out")));
		String[] toks = f.readLine().split(" ");
		int M = Integer.parseInt(toks[0]);
		int S = Integer.parseInt(toks[1]);
		int C = Integer.parseInt(toks[2]);
		
		
		int[] stalls = new int[C];
		for(int i=0; i<C; i++){
			stalls[i] = Integer.parseInt(f.readLine());
		}
		
		Arrays.sort(stalls);
		int[] dist = new int[stalls.length-1];
		for(int i=0;i<dist.length;i++){
			dist[i] = stalls[i+1]-stalls[i]-1;
		}
		
		Arrays.sort(dist);
		
		long totalLength = stalls[stalls.length-1] - stalls[0]+1;
		int numCut = 0;
		
		while(numCut < M-1 && totalLength > C){
			totalLength -= dist[dist.length - numCut - 1];
			numCut++;
		}
		
		out.println(totalLength);
		out.close();
	}
}
