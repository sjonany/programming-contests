package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: transform
 */
public class transform {
	static int N;
	static char[][] goal;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		
		BufferedReader f = new BufferedReader(new FileReader("transform.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("transform.out")));
		N = Integer.parseInt(f.readLine());
		char[][] m = new char[N][N];
		goal = new char[N][N];
		
		for(int i=0; i<N; i++){
			String s = f.readLine();
			for(int j=0; j<N; j++){
				m[i][j] = s.charAt(j);
			}
		}
		
		for(int i=0; i<N; i++){
			String s = f.readLine();
			for(int j=0; j<N; j++){
				goal[i][j] = s.charAt(j);
			}
		}
		
/*
		System.out.println("result:");
		System.out.println(Arrays.deepToString(m) + "\n");
		System.out.println(Arrays.deepToString(flip(m))+ "\n");
		System.out.println(Arrays.deepToString(rotateClockwise90(m)));
		
		boolean x = true;
		if(true)return;
		*/

		int numRot = getNumRotate(m);
		
		int result = -1;
		switch(numRot){
		case 1: 
		case 2:
		case 3:
			result = numRot;
			break;
		}
		if(result != -1){
			out.println(result);
			out.close();
			return;
		}
		
		char[][] flip = flip(m);
		if(isEqual(flip,goal)){
			out.println(4);
			out.close();
			return;
		}
		
		numRot = getNumRotate(flip);
		result = -1;
		switch(numRot){
		case 1: 
		case 2:
		case 3:
			result = 5;
			break;
		}
		
		if(result != -1){
			out.println(result);
			out.close();
			return;
		}else{
			if(isEqual(goal, m)){
				out.println(6);
				out.close();
				return;
			}else{
				out.println(7);
				out.close();
				return;
			}
		}
	}
	
	public static int getNumRotate(char[][] m){
		char[][] c90 = rotateClockwise90(m);
		
		if(isEqual(c90, goal)){
			return 1;
		}
		
		char[][] c180 = rotateClockwise90(c90);
		if(isEqual(c180, goal)){
			return 2;
		}
		
		char[][] c270 = rotateClockwise90(c180);
		if(isEqual(c270, goal)){
			return 3;
		}
		
		return -1;
	}
	
	public static char[][] rotateClockwise90(char[][] m){
		char[][] copy = new char[N][N];
		for(int row=0; row<N; row++){
			for(int col=0; col<N; col++){
				copy[row][col] = m[N-1-col][row];
			}
		}
		return copy;
	}
	
	public static char[][] flip(char[][] m){
		char[][] copy = new char[N][N];
		for(int row=0 ; row<N; row++){
			for(int col=0; col<N; col++){
				copy[row][col] = m[row][N-1-col];
			}
		}
		return copy;
	}
	
	public static boolean isEqual(char[][] m1, char[][]m2){
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				if(m1[i][j] != m2[i][j])
					return false;
			}
		}
		return true;
	}
}
