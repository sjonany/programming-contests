package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: numtri
 */

public class numtri {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);

		BufferedReader f = new BufferedReader(new FileReader("numtri.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("numtri.out")));
		
		int R = Integer.parseInt(f.readLine());
		int[] prev = new int[]{Integer.parseInt(f.readLine())};
		for(int i=2;i<=R;i++){
			String[] toks = f.readLine().split(" ");
			int[] cur= new int[i];
			cur[0] = prev[0] + Integer.parseInt(toks[0]);
			for(int j=1;j<i-1;j++){
				cur[j] = Integer.parseInt(toks[j]) + Math.max(prev[j], prev[j-1]);
			}
			cur[i-1] = prev[i-2] + Integer.parseInt(toks[i-1]);
			prev = cur;
		}
		
		int max = -1;
		for(int i=0;i<prev.length;i++){
			max = Math.max(prev[i], max);
		}
		
		out.println(max);
		
		out.close();
	}
	
}
