package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: test
 */
public class test {
	public static void main(String[] args) throws Exception{
		BufferedReader f = new BufferedReader(new FileReader("test.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("test.out")));
	    StringTokenizer st = new StringTokenizer(f.readLine());
	    int i1 = Integer.parseInt(st.nextToken());   
	    int i2 = Integer.parseInt(st.nextToken());  
	    out.println(i1+i2);                          
	    out.close();                                 
	    System.exit(0);                               
	}
}
