package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
ID: xenoslash
LANG: JAVA
TASK: packrec
 */
public class packrec {
	static int minArea = 2000*2000;
	//dims that correspond to minArea
	static Set<Rec> minRecs = new HashSet<Rec>();
	static int count = 0;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("packrec.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("packrec.out")));
		Rec[] recs = new Rec[4];
		for(int i=0;i<4;i++){
			String[] toks = f.readLine().split(" ");
			recs[i] = new Rec(Integer.parseInt(toks[0]),Integer.parseInt(toks[1]));
		}
		rotateSearch(recs, 1, new Rec[4]);
		
		out.println(minArea);
		Rec[] sortedRecs = minRecs.toArray(new Rec[0]);
		Arrays.sort(sortedRecs, new Comparator<Rec>(){
			@Override
			public int compare(Rec o1, Rec o2) {
				return o1.w - o2.w;
			}
			
		});
		
		for(Rec rec: sortedRecs){
			out.println(rec.w + " " + rec.h);
		}
		
		out.close();
	}

	//permute rotated or not plus the ordering
	public static void rotateSearch(Rec[] original, int toSet, Rec[] filled){
		if(toSet == 5){
			orderSearch(filled, 1, new Rec[4], new boolean[4]);
		}else{
			filled[toSet-1] = original[toSet-1];
			rotateSearch(original, toSet+1, filled);
			filled[toSet-1] = new Rec(original[toSet-1].h, original[toSet-1].w);
			rotateSearch(original, toSet+1, filled);
		}
	}
	
	public static void orderSearch(Rec[] original, int toSet, Rec[] filled, boolean[] used){
		if(toSet == 5){
			packSearch(filled, new OccRec[4], 1);
		}else{
			for(int i=0; i<original.length; i++){
				if(!used[i]){
					filled[toSet-1] = original[i];
					used[i] = true;
					orderSearch(original, toSet+1, filled, used);
					used[i] = false;
				}
			}
		}
	}
	
	//try all possible ways to place the boxes, rotation is fixed and ordering is fixed
	public static void packSearch(Rec[] ori, OccRec[] filled, int toSet){
		if(toSet == 5){
			//find area
			int maxX = 0;
			int maxY = 0;
			for(OccRec rec : filled){
				maxX = Math.max(maxX, rec.x + rec.w - 1);
				maxY = Math.max(maxY, rec.y + rec.h - 1);
			}
			
			int area = (maxX+1) * (maxY+1);
			if(area <= minArea){
				if(area < minArea){
					minArea = area;
					minRecs = new HashSet<Rec>();
				}
					
				Rec dim = new Rec(Math.min(maxX+1, maxY+1), Math.max(maxX+1, maxY+1));
				minRecs.add(dim);
			}
		}else{
			if(toSet == 1){
				OccRec topLeftRec = new OccRec();
				topLeftRec.x = 0;
				topLeftRec.y = 0;
				topLeftRec.w = ori[0].w;
				topLeftRec.h = ori[0].h;
				filled[toSet-1] = topLeftRec;
				packSearch(ori, filled, toSet+1);
			}else if(toSet == 2){
				//to the right of first guy or bottom
				OccRec right = new OccRec();
				right.x = filled[0].x + filled[0].w;
				right.y = 0;
				right.w = ori[1].w;
				right.h = ori[1].h;
				filled[toSet-1] = right;
				packSearch(ori, filled, toSet+1);
			}else{
				Rec newRec = ori[toSet-1];
				//3rd or 4th triangle, brute force around the corners of previously set rects
				for(int recI = 0; recI<toSet-1; recI++){
					List<OccRec> candidates = new ArrayList<OccRec>();
					OccRec prevRec = filled[recI];
					//try 4 spots for each rectangle
					//bottom left
					OccRec bottomLeft = new OccRec();
					bottomLeft.x = prevRec.x;
					bottomLeft.y = prevRec.y + prevRec.h; 
					bottomLeft.w = newRec.w;
					bottomLeft.h = newRec.h;
					
					candidates.add(bottomLeft);
					
					// bottom right
					OccRec bottomRight = new OccRec();
					bottomRight.x = prevRec.x + prevRec.w - newRec.w;
					bottomRight.y = prevRec.y + prevRec.h; 
					bottomRight.w = newRec.w;
					bottomRight.h = newRec.h;
					
					candidates.add(bottomRight);
					
					//right up
					OccRec rightUp = new OccRec();
					rightUp.x = prevRec.x + prevRec.w;
					rightUp.y = prevRec.y; 
					rightUp.w = newRec.w;
					rightUp.h = newRec.h;
					
					candidates.add(rightUp);
					
					//right bottom
					OccRec rightBottom = new OccRec();
					rightBottom.x = prevRec.x + prevRec.w;
					rightBottom.y = prevRec.y + prevRec.h - newRec.h; 
					rightBottom.w = newRec.w;
					rightBottom.h = newRec.h;
					
					candidates.add(rightBottom);
					
					for(OccRec candidate: candidates){
						if(isNonIntersect(filled, toSet-2, candidate)){
							filled[toSet-1] = candidate;
							packSearch(ori, filled, toSet+1);
						}
					}
				}
			}
		}
	}
	
	//returns true if adding newRec does not cause intersect with prev[0..prevIndex]
	public static boolean isNonIntersect(OccRec[] prev, int prevIndex, OccRec newRec){
		if(newRec.x <0 || newRec.y <0){
			return false;
		}
		
		for(int i=0; i<= prevIndex; i++){
			if(prev[i].intersects(newRec)){
				return false;
			}
		}
		return true;
	}
	
	public static class OccRec{
		public int x;
		public int y;
		public int w;
		public int h;
		
		public boolean intersects(OccRec other){
			int ax1 = x;
			int ax2 = x+w-1;
			int ay1 = y;
			int ay2 = y+h-1;
			
			int bx1 = other.x;
			int bx2 = other.x + other.w -1;
			int by1 = other.y;
			int by2 = other.y + other.h -1;
			return (ax1 <= bx2 && ax2 >= bx1 &&
				    ay1 <= by2 && ay2 >= by1);
		}
		
		@Override
		public String toString(){
			return "(x=" + x +",y=" + y+",w="+w + ", h=" + h+")";
		}
		
	}
	
	public static class Rec{
		public int w;
		public int h;
		
		public Rec(int w, int h){
			this.w = w;
			this.h = h;
		}
		
		@Override
		public String toString(){
			return "(w="+w + ", h=" + h+")";
		}
		
		@Override
		public int hashCode(){
			return w + h * 31;
		}
		
		@Override
		public boolean equals(Object o){
			if(!(o instanceof Rec)){
				return false;
			}
			
			Rec other = (Rec) o;
			return other.w == this.w && other.h == this.h;
		}
	}
}
