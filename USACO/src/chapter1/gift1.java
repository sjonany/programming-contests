package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
ID: xenoslash
LANG: JAVA
TASK: gift1
 */
public class gift1 {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("gift1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("gift1.out")));
		Map<String, Integer> count = new HashMap<String, Integer>();
	    
		List<String> people = new ArrayList<String>();
		int NP = Integer.parseInt(f.readLine().trim());
	    for(int i=0; i< NP; i++){
	    	String name = f.readLine().trim();
	    	count.put(name, 0);
	    	people.add(name);
	    }
	    
	    String line = f.readLine();
	    while(line != null){
	    	String giver = line.trim();
	    	String givs[] = f.readLine().trim().split(" ");
	    	int amount = Integer.parseInt(givs[0]);
	    	int numGiv = Integer.parseInt(givs[1]);
	    	int eachAmount = 0;
	    	int rem = amount;
	    	if(numGiv != 0){
		    	eachAmount = amount/numGiv;
		    	rem = amount - eachAmount * numGiv;
	    	}
	    	count.put(giver, count.get(giver) + rem - amount);
	    	for(int i=0; i< numGiv; i++){
	    		String name = f.readLine().trim();
	    		count.put(name, count.get(name) + eachAmount);
	    	}
	    	line = f.readLine();
	    }
	    
	    for(String s : people){
	    	out.println(s + " " + count.get(s));
	    }
	    
	    
	    f.close();
	    out.close();                                 
	    System.exit(0);                               
	}
}
