package chapter1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: beads
 */
public class beads{
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	    
		BufferedReader f = new BufferedReader(new FileReader("beads.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("beads.out")));
	
		int N = Integer.parseInt(f.readLine());
		String s = f.readLine().trim();
		
		int maxSum = 0;
		
		//cut = between i and i-1
		for(int i=0;i<N;i++){
			int before = i-1;
			if(i-1 <0){
				before = N-1;
			}
			int after = i;
			
			if(s.charAt(before) != s.charAt(after)){
				int sum = 2;
				char beforeState = s.charAt(before);
				char afterState = s.charAt(after);
				
				while(before != (after+1)%N){
					int next = before-1;
					if(next <0)
						next = N-1;
					
					if(beforeState == 'w'){
						if(s.charAt(next) == 'w'){
							before = next;
							sum++;
						}else{
							beforeState = s.charAt(next);
							before = next;
							sum++;
						}
					}else{
						if(s.charAt(next) == 'w' || s.charAt(next) == beforeState){
							before = next;
							sum++;
						}else{
							break;
						}
					}
				}
				
				while(after != (before-1)%N){
					int next = (after+1) %N;
					
					if(afterState == 'w'){
						if(s.charAt(next) == 'w'){
							after = next;
							sum++;
						}else{
							afterState = s.charAt(next);
							after = next;
							sum++;
						}
					}else{
						if(s.charAt(next) == 'w' || s.charAt(next) == afterState){
							after = next;
							sum++;
						}else{
							break;
						}
					}
				}
				
				if(sum > maxSum)
					maxSum = sum;
			}
		}
		
		if(maxSum == 0){
			maxSum = N;
		}
		out.println(maxSum);
		out.close();
	}
}
