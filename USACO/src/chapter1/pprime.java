package chapter1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
/*
ID: xenoslash
LANG: JAVA
TASK: pprime
 */
public class pprime {
	//static PrintWriter out = new PrintWriter(System.out);
	//static BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
	static int lo;
	static int hi;
	static BufferedReader f;
	static PrintWriter out;
	public static void main(String[] args) throws Exception{
		f =  new BufferedReader(new FileReader("pprime.in"));
		out = new PrintWriter(new BufferedWriter(new FileWriter("pprime.out")));
		String[] toks = f.readLine().split(" ");
		lo = Integer.parseInt(toks[0]);
		hi = Integer.parseInt(toks[1]);
		
		int[] oneDig = new int[]{2,3,5,7};
		for(int i:oneDig){
			if(i>=lo && i<=hi)
				out.println(i);
		}
		
		int hiDig = Integer.toString(hi).length();
		
		for(int i=2;i<=hiDig;i++){
			if(i%2 == 0){
				for(int j=1;j<=9;j+=2){
					dfsEven(i/2-1, "" + j);
				}
			}else{
				for(int j=1;j<=9;j+=2){
					dfsOdd((i-1)/2, "" + j);
				}
			}
		}
		out.close();
	}
	
	//numDig = remaining number of digits to fill
	public static void dfsEven(int numDig, String halfFilled){
		if(numDig == 0){
			String otherHalf = reverse(halfFilled.substring(0,halfFilled.length()));
			String complete = halfFilled + otherHalf;
			int num = Integer.parseInt(complete);
			if(num >= lo && num <= hi && isPrime(num)){
				out.println(num);
			}
		}else{
			for(int i=0;i<=9;i++){
				dfsEven(numDig-1, halfFilled + i);
			}
		}
	}
	
	//stores the first half+1 of odd palindrome, starting outside most
	//numDig = how many digs left to fill
	public static void dfsOdd(int numDig, String halfFilled){
		if(numDig == 0){
			String otherHalf = reverse(halfFilled.substring(0,halfFilled.length()-1));
			String complete = halfFilled + otherHalf;
			int num = Integer.parseInt(complete);
			if(num >= lo && num <= hi && isPrime(num)){
				out.println(num);
			}
		}else{
			for(int i=0;i<=9;i++){
				dfsOdd(numDig-1, halfFilled + i);
			}
		}
	}
	
	public static String reverse(String s){
		String rev = "";
		for(int i=0; i<s.length();i++){
			rev+=s.charAt(s.length()-1-i);
		}
		return rev;
	}
	
	public static boolean isPrime(int n){
		if(n==1) return false;
		if(n==2) return true;
		if(n%2 == 0) return false;
		for(int i=3; i*i <=n; i+=2){
			if(n%i == 0)
				return false;
		}
		
		return true;
	}
}
