package chapter1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/*
ID: xenoslash
LANG: JAVA
TASK: namenum
 */
public class namenum {
	static int N;
	static Set<String> validNames;
	static char[][] goal;
	static char[][] map = new char[][]{
		{},
		{},
		{'A','B','C'},
		{'D','E','F'},
		{'G','H','I'},
		{'J','K','L'},
		{'M','N','O'},
		{'P','R','S'},
		{'T','U','V'},
		{'W','X','Y'},
	};	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		
		
		BufferedReader f = new BufferedReader(new FileReader("namenum.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("namenum.out")));

		String id = f.readLine().trim();		
		
		List<String> names = new ArrayList<String>();
		Scanner sc = new Scanner(new File("dict.txt"));
		while(sc.hasNextLine()){
			String name = sc.nextLine().trim();
			if(name.length() == id.length()){
				names.add(name);
			}
		}
		sc.close();
		
		boolean hasPrinted = false;
		
		int[] index = new int[1000];
		for(char i = 'A'; i <= 'Y'; i++){
			if(i=='Q')continue;
			for(int j=2; j<=9;j++){
				char[] maps = map[j];
				for(char k : maps){
					if(k == i){
						index[i] = j;
						break;
					}
				}
			}
		}
		
		//STUPID!!! :(((((( iterate over the super small size dictionary!!!
		
		for(String name : names){
			boolean isValid = true;
			for(int i=0; i<name.length(); i++){
				if(index[(int)name.charAt(i)] != (id.charAt(i)-'0')){
					isValid = false;
					break;
				}
			}
			
			if(isValid){
				out.println(name);
				hasPrinted = true;
			}
		}
		
		/*
		int trial = 0;
		int nameIndex = 0;
		int endTrial = (int) Math.pow(3,id.length());
		while(trial < endTrial && nameIndex < names.size()){
			String curPerm = Integer.toString(trial,3);
			while(curPerm.length() != id.length()){
				curPerm = "0" + curPerm;
			}
			String trialName = "";
			for(int i=0; i<curPerm.length(); i++){
				trialName += map[id.charAt(i)-'0'][(curPerm.charAt(i) -'0')];
			}
			
			int compare = trialName.compareTo(names.get(nameIndex));
			if(compare<0){
				trial++;
			}else if(compare>0){
				nameIndex++;
			}else{
				trial++;
				nameIndex++;
				out.println(trialName);
				hasPrinted = true;
			}
		}
		*/
		if(!hasPrinted)
			out.println("NONE");
		
		out.close();
	}
}
