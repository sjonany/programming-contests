package chapter3point4;


import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: fence4
 */
public class fence4fail {
	static int N;
	static Point2D obs;
	static int[][] input;
	public static void main(String[] args) throws Exception{
		List<Result> results = new ArrayList<Result>();
		BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
	   
		//BufferedReader f = new BufferedReader(new FileReader("fence4.in"));
		//PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("fence4.out")));
		
		N = Integer.parseInt(f.readLine());
		input = new int[N][2];
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		int ox = Integer.parseInt(tkz.nextToken());
		int oy = Integer.parseInt(tkz.nextToken());
		obs = new Point2D.Double(ox,oy);
		
		List<Line2D.Double> lines = new ArrayList<Line2D.Double>();
		
		double curX;
		double curY;

		tkz = new StringTokenizer(f.readLine());
		curX = Double.parseDouble(tkz.nextToken());
		curY = Double.parseDouble(tkz.nextToken());
		input[0][0] = (int)curX;
		input[0][1] = (int)curY;
		
		for(int i=0;i<N-1;i++){
			tkz = new StringTokenizer(f.readLine());
			double nextX = Double.parseDouble(tkz.nextToken());
			double nextY = Double.parseDouble(tkz.nextToken());

			Line2D.Double newLine = new Line2D.Double(curX, curY, nextX, nextY);
			//dont' compare with the previously touching fence
			for(int j=0;j<lines.size()-1;j++){
				Line2D oldLine = lines.get(j);
				if(oldLine.intersectsLine(newLine)){
					out.println("NOFENCE");
					out.close();
					return;
				}
			}
			lines.add(newLine);
			curX = nextX;
			curY = nextY;
			input[i+1][0] = (int)curX;
			input[i+1][1] = (int)curY;
		}
		
		//close path, put in the first position
		Line2D.Double newLine = new Line2D.Double(curX, curY, lines.get(0).x1, lines.get(0).y1);
		for(int j=1;j<lines.size()-1;j++){
			Line2D oldLine = lines.get(j);
			if(oldLine.intersectsLine(newLine)){
				out.println("NOFENCE");
				out.close();
				return;
			}
		}
		lines.add(newLine);
		
		for(int i=0;i<N;i++){
			Line2D.Double remLine = (Line2D.Double)lines.get(i).clone();
			//System.out.println("------------------------------------------------");
			//System.out.println("Line to check = " + str(remLine));
			if(isParallel(remLine)){
				continue;
			}
			boolean isVisible = true;
			for(int j=1; j<N && isVisible;j++){
				Line2D.Double blocker = lines.get((i+j)%N);
				//System.out.println("blocker = " + str(blocker));
				if(isParallel(blocker)){
					continue;
				}
				
				
				//if it's the neighboring segment
				Point2D sharedEndpt = touchesAtEndpoint(remLine, blocker);
				if(sharedEndpt != null){
					//System.out.println("neighbors!");
					//point that is not intersecting
					Point2D viPoint = remLine.getP1().equals(sharedEndpt) ? remLine.getP2() : remLine.getP1();
					Point2D blockingNonIntersect = blocker.getP1().equals(sharedEndpt) ?
							blocker.getP2() : blocker.getP1();
					//System.out.println("viPoint = " + str(viPoint));
					//System.out.println("blockNonI = " + str(blockingNonIntersect));
					
					if(isCross(new Line2D.Double(viPoint, obs), blocker)){
						//can't go from obs to viPoint directly, blockage
						isVisible = false;
						//System.out.println("chomped off completely...");
						continue;
					}else{
						if(isSameSideOfLine(blocker, viPoint, obs)){
							//System.out.println("no chomping");
							continue;
						}
						//might need to split into two
						Line2D obsToBlock = new Line2D.Double(obs,blockingNonIntersect); 
						Point2D intersect = getIntersection(obsToBlock, remLine);
						//System.out.println("intersection = " + str(intersect));
						if(intersect.equals(viPoint)){
							//might get chomped off cleanly
							isVisible = false;
							//System.out.println("chomped off completely...");
							continue;
						}else if(!isPointOnLine(remLine, intersect)){
							//System.out.println("no chomping");
							continue;
						}else{
							
							//chomping
							
							remLine = new Line2D.Double(viPoint, intersect);
							//System.out.println("chomping " + str(remLine));
							continue;
						}
					}		
				}
				
				//if not neighboring segment
				//chomp of rem line using blocker, and see what is remaining
				//from how we traverse the blocker, our chomped result will only be 1 segment at most
				//so if there is chomping, at least one of the endpoints must be in the invisible side
				
				boolean isE1Visible = isVisible(remLine.getP1(),obs,blocker);
				boolean isE2Visible = isVisible(remLine.getP2(),obs,blocker);
				
				if(!isE1Visible && !isE2Visible){
					//everything gets chomped off
					isVisible = false;
					//System.out.println("chomped off completely...");
					break;
				}else if(isE1Visible && isE2Visible){
					//actually, this case should indicate that there is no chomping to be done
					//because we traverse in ccw, impossible to get middle split
					//but because of round off errors.. had to do some sanity check :(
					Point2D midPoint = new Point2D.Double((remLine.x1+remLine.x2)/2.0, (remLine.y1+ remLine.y2) / 2.0);
					if(!isVisible(midPoint, obs, blocker)){
						isVisible = false;
						//System.out.println("chomped off completely...");
						break;
					}else{
						//System.out.println("no chomping");
						//nothing gets chomped off, since we traverse in ccw, impossible to get middle split
						continue;
					}
				}else{
					//split into two
					Point2D viPoint;
					viPoint = isE1Visible ? remLine.getP1() : remLine.getP2();
			
					//one of the two lines have to cut me off
					Line2D obsFrom = new Line2D.Double(obs, blocker.getP1());
					Line2D obsTo = new Line2D.Double(obs, blocker.getP2());
					Point2D intersect = getIntersection(obsFrom, remLine);
					if(!isPointOnLine(remLine,intersect)){
						intersect = getIntersection(obsTo, remLine);
					}
					
					if(viPoint.equals(intersect)){
						isVisible = false;
						System.out.println("chomped off completely...");
						break;
					}
					remLine = new Line2D.Double(viPoint, intersect);
					//System.out.println("chomping " + str(remLine) + " by " + str(blocker));
				}
			}

			if(length(remLine) < 0.001){
				isVisible = false;
			}
			if(isVisible){
				System.out.println("survived");
				System.out.println(str(remLine));
				results.add(new Result(i,(i+1)%N));
			}else{
				System.out.println("invisible");
			}
		}
		Collections.sort(results);
		out.println(results.size());
		for(Result r : results){
			out.println(input[r.p1][0] + " " + input[r.p1][1] + " " 
					+ input[r.p2][0] + " " + input[r.p2][1]);
		}
		out.close();
	}
	
	public static double length(Line2D l){
		return Point2D.distance(l.getX1(), l.getY1(), l.getX2(), l.getY2());
	}
	
	//get intersection of two line segments given they are not parallel, when they are fully extended
	public static Point2D getIntersection(Line2D l1, Line2D l2){
		double dy1 = l1.getY2()-l1.getY1();
		double dx1 = l1.getX2()-l1.getX1();
		double dy2 = l2.getY2()-l2.getY1();
		double dx2 = l2.getX2()-l2.getX1();
		
		if(Math.abs(dx1) < 0.00001){
			double m2 = dy2/dx2;
			double c2 = l2.getY1() - m2*l2.getX1();
			//since not parallel, the other guy must not be vertical
			return new Point2D.Double(l1.getX1(),m2*l1.getX1()+c2);
		}
		
		if(Math.abs(dx2) < 0.00001){
			double m1 = dy1/dx1;
			double c1 = l1.getY1() - m1*l1.getX1();
			//since not parallel, the other guy must not be vertical
			return new Point2D.Double(l2.getX1(),m1*l2.getX1() + c1);
		}
		
		//no vertical lines, both not parallel
		double m1 = dy1/dx1;
		double m2 = dy2/dx2;
		double c1 = l1.getY1() - m1*l1.getX1();
		double c2 = l2.getY1() - m2*l2.getX1();
		double xi = (c2-c1)/(m1-m2);
		double yi = m1*xi+c1;
		
		return new Point2D.Double(xi,yi);
	}
	
	//assumes that segment is either entirely visible, or completely blocked from the observer
	//true iff visible
	public static boolean isVisible(Line2D segment, Point2D obs, Line2D blocker){
		//with the assumption, we just pick the midpoint to determine it's visible
		Point2D interest = new Point2D.Double((segment.getX1()+segment.getX2())/2.0,
				(segment.getY1()+segment.getY2())/2.0);
		return isVisible(interest, obs, blocker);
	}
	
	//is target point visible from obs if there is blocker?
	public static boolean isVisible(Point2D target, Point2D obs, Line2D blocker){
		Line2D obsFrom = new Line2D.Double(obs, blocker.getP1());
		Line2D obsTo = new Line2D.Double(obs, blocker.getP2());
		
		Point2D triangleCenter = new Point2D.Double((obs.getX()+blocker.getX1()+blocker.getX2())/3.0,
				(obs.getY()+blocker.getY1()+blocker.getY2())/3.0);
		
		//visible only if not at the dark shadow behind the triangle formed by o,blocker
		return !(isSameSideOfLine(obsFrom, triangleCenter, target) &&
				isSameSideOfLine(obsTo, triangleCenter, target) &&
				!isSameSideOfLine(blocker, triangleCenter, target));
	}
	
	public static boolean isSameSideOfLine(Line2D divide, Point2D p1, Point2D p2){
		int side1 = getSideOfLine(divide, p1);
		int side2 = getSideOfLine(divide, p2);
		return side1 == side2  || side1*side2 == 0;
	}
	
	//true iff remLine is parallel to the observer
	public static boolean isParallel(Line2D line){
		int dy2 = (int) line.getY2()-(int)obs.getY();
		int dx2 = (int) line.getX2()-(int)obs.getY();
		int dy1 = (int) line.getY1()-(int)obs.getY();
		int dx1 = (int) line.getX1()-(int)obs.getX();
		return dy1*dx2 == dy2*dx1;
	}
	
	//true iff l1 and l2 are crossing
	public static boolean isCross(Line2D l1, Line2D l2){
		return !isSameSideOfLine(l2, l1.getP1(), l1.getP2()) &&
				!isSameSideOfLine(l1,l2.getP1(), l2.getP2());
	}
	
	//0 if on line, -1 or 1 if on some side
	public static int getSideOfLine(Line2D divide, Point2D p1){
		double[] vDiv = new double[]{divide.getX2()-divide.getX1(),
				divide.getY2()-divide.getY1(),0};
		double[] v1 = new double[]{p1.getX()-divide.getX1(),
				p1.getY()-divide.getY1(),0};
		
		double c = crossProduct(vDiv,v1)[2];
		if(Math.abs(c) < 0.00001){
			return 0;
		}else if(c>0){
			return 1;
		}else{
			return -1;
		}
	}
	
	
	public static boolean isPointOnLine(Line2D l, Point2D p){
		return l.ptSegDist(p) < 0.00001;
	}
	public static Point2D touchesAtEndpoint(Line2D l1, Line2D l2){
		if(l1.getP1().equals(l2.getP1()) || l1.getP1().equals(l2.getP2())){
			return l1.getP1();
		}
		
		if(l1.getP2().equals(l2.getP1()) || l1.getP2().equals(l2.getP2())){
			return l1.getP2();
		}
		
		return null;
	}
	
	public static double[] crossProduct(double[] v1, double[] v2){
		return new double[]{(v1[1]*v2[2]-v1[2]*v2[1]), 
				(v1[2]*v2[0]-v1[0]*v2[2]),
				(v1[0]*v2[1] - v1[1]*v2[0])};
	}
	
	public static String str(Line2D l){
		return str(l.getP1()) + "->" + str(l.getP2());
	}
	public static String str(Point2D pt){
		return "(" + pt.getX() + "," + pt.getY() + ")";
	}
	
	//indices of points wrt to how early they come in the inputs
	public static class Result implements Comparable<Result>{
		public int p1;
		public int p2;
		
		public Result(int p1, int p2){
			this.p1 = Math.min(p1,p2);
			this.p2 = Math.max(p1, p2);
		}

		@Override
		public int compareTo(Result o) {
			int k =  this.p2 - o.p2;
			if(k!=0)return k;
			return this.p1 - o.p1;
		}
	}
}