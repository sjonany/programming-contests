package chapter3point4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: heritage
 */
public class heritage {
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("heritage.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("heritage.out")));
		String inTree = f.readLine();
		String preTree = f.readLine();
		
		Node root = buildTree(inTree, preTree);
		postOrder(root, out);
		out.write("\n");
		out.close();
	}
	
	public static void postOrder(Node root, PrintWriter out){
		if(root == null){
			return;
		}
		postOrder(root.left, out);
		postOrder(root.right, out);
		out.write("" + root.id);
	}
	
	//given inTree and preTree string, return the node root of tree
	public static Node buildTree(String inTree, String preTree){
		if(preTree.length() == 0){
			return null;
		}
		Node root = new Node(preTree.charAt(0), null,null);
		int i = inTree.indexOf(root.id);
		String inTreeLeft = inTree.substring(0,i);
		String inTreeRight = inTree.substring(i+1);
		int sizeLeftTree = inTreeLeft.length();
		String preTreeLeft = preTree.substring(1,1+sizeLeftTree);
		String preTreeRight = preTree.substring(1+ sizeLeftTree);
		
		root.left = buildTree(inTreeLeft, preTreeLeft);
		root.right = buildTree(inTreeRight, preTreeRight);
		return root;
	}
	
	public static class Node{
		public char id;
		public Node left;
		public Node right;
		
		public Node(char id, Node left, Node right){
			this.id = id;
			this.left = left;
			this.right = right;
		}
	}
}
