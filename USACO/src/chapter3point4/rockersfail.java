package chapter3point4;

/*
 Tests:
 10 20 10 
 81
 
 7 5 10
 20
 
 100 200 50
 4901
 */


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

/*
urgh misunderstood the problem.
This should be the answer if there is no restriction

ID: xenoslash
LANG: JAVA
TASK: rockers
 */

public class rockersfail {
	static int maxFit = -1;
	static int N;
	static int T;
	static int M;
	static int[] songs;
	public static void main(String[] args) throws Exception{
		BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
	   
		//BufferedReader f = new BufferedReader(new FileReader("rockers.in"));
		//PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("rockers.out")));
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		
		N = Integer.parseInt(tkz.nextToken());
		T = Integer.parseInt(tkz.nextToken());
		M = Integer.parseInt(tkz.nextToken());
		
		songs = new int[N];
		tkz = new StringTokenizer(f.readLine());
		for(int i=0; i<N; i++){
			songs[i] = Integer.parseInt(tkz.nextToken());
		}
		
		Arrays.sort(songs);
		int[] remSpace = new int[T+1];
		remSpace[T] = M;
		choose(0, remSpace, 0);
		out.println(maxFit);
		out.close();
	}
	
	public static void choose(int toSet, int[] remSpace, int usedCount){
		maxFit = Math.max(maxFit, usedCount);
		if(toSet != songs.length){
			if(usedCount + songs.length - toSet <= maxFit){
				return;
			}
			
			boolean canFit = false;
			int songLength = songs[toSet];
			//keep trying different sizes of non-empty partitions that are bigger than this song
			for(int size=songLength; size<remSpace.length;size++){
				if(remSpace[size] > 0){
					canFit = true;
					
					remSpace[size]--;
					remSpace[size-songLength]++;
					choose(toSet+1, remSpace, usedCount+1);
					remSpace[size]++;
					remSpace[size-songLength]--;
				}
			}
			//don't put
			if(canFit)
				choose(toSet+1, remSpace, usedCount);
		}
	}
}
