package chapter3point4;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: rockers
 */

public class rockers {
	static int N;
	static int T;
	static int M;
	static int[] songs;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("rockers.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("rockers.out")));
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		
		N = Integer.parseInt(tkz.nextToken());
		T = Integer.parseInt(tkz.nextToken());
		M = Integer.parseInt(tkz.nextToken());
		
		songs = new int[N];
		tkz = new StringTokenizer(f.readLine());
		for(int i=0; i<N; i++){
			songs[i] = Integer.parseInt(tkz.nextToken());
		}
		
		//max number of songs if use i disks exactly, ith disk has used up <=j, and last song on disk i is index k
		int[][][] dp = new int[M+1][T+1][N];
		
		
		for(int i=1;i<=M;i++){
			for(int j=0;j<=T;j++){
				for(int k=0;k<N;k++){
					dp[i][j][k] = -1;		
					//time remaining for other songs in the same disk
					int remainTime = j-songs[k];
					if(remainTime >= 0){
						dp[i][j][k] = 1;
						for(int prevSong = 0; prevSong < k; prevSong++){
							//if k is on this new disk alone
							if(i!=1){
								if(dp[i-1][T][prevSong] != -1)
									dp[i][j][k] = Math.max(dp[i][j][k], dp[i-1][T][prevSong]+1);
							}
							
							//if k is on this last disk with someone else
							if(dp[i][remainTime][prevSong] != -1)
								dp[i][j][k] = Math.max(dp[i][j][k], dp[i][remainTime][prevSong]+1);
						}
					}
				}
			}
		}
		
		int max = 0;
		for(int k=0;k<N;k++){
			max = Math.max(max, dp[M][T][k]);
		}
		out.println(max);
		out.close();
	}
}
