package chapter3point4;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: fence4
 */
public class fence4 {
	static int N;
	static Point2D obs;
	static int[][] input;
	public static void main(String[] args) throws Exception{
		List<Result> results = new ArrayList<Result>();
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("fence4.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("fence4.out")));
		
		N = Integer.parseInt(f.readLine());
		input = new int[N][2];
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		int ox = Integer.parseInt(tkz.nextToken());
		int oy = Integer.parseInt(tkz.nextToken());
		obs = new Point2D.Double(ox,oy);
		
		List<Line2D.Double> lines = new ArrayList<Line2D.Double>();
		
		double curX;
		double curY;

		tkz = new StringTokenizer(f.readLine());
		curX = Double.parseDouble(tkz.nextToken());
		curY = Double.parseDouble(tkz.nextToken());
		input[0][0] = (int)curX;
		input[0][1] = (int)curY;
		
		for(int i=0;i<N-1;i++){
			tkz = new StringTokenizer(f.readLine());
			double nextX = Double.parseDouble(tkz.nextToken());
			double nextY = Double.parseDouble(tkz.nextToken());

			Line2D.Double newLine = new Line2D.Double(curX, curY, nextX, nextY);
			//dont' compare with the previously touching fence
			for(int j=0;j<lines.size()-1;j++){
				Line2D oldLine = lines.get(j);
				if(oldLine.intersectsLine(newLine)){
					out.println("NOFENCE");
					out.close();
					return;
				}
			}
			lines.add(newLine);
			curX = nextX;
			curY = nextY;
			input[i+1][0] = (int)curX;
			input[i+1][1] = (int)curY;
		}
		
		//close path, put in the first position
		Line2D.Double newLine = new Line2D.Double(curX, curY, lines.get(0).x1, lines.get(0).y1);
		for(int j=1;j<lines.size()-1;j++){
			Line2D oldLine = lines.get(j);
			if(oldLine.intersectsLine(newLine)){
				out.println("NOFENCE");
				out.close();
				return;
			}
		}
		lines.add(newLine);
		
		boolean[] isVisible = new boolean[N];
		//check if midpoint is visible

		for(int i=0;i<N;i++){
			Point2D mid = getMidpoint(lines.get(i));
			boolean isMidVisible = true;
			for(int j=0;j<N;j++){
				if(j!=i){
					if(!isVisible(mid, obs,lines.get(j))){
						isMidVisible = false;
						break;
					}
				}
			}
			
			isVisible[i] = isMidVisible;
		}
		
		//check ray from obs to each corner and see which is the first line
		for(int i=0;i<N;i++){
			Point2D corner= new Point2D.Double(input[i][0], input[i][1]);
			Line2D obsCorner = new Line2D.Double(obs, corner);
			
			double minDistance = Double.MAX_VALUE;
			int bestSegmentIndex = -1;
			Point2D bestSegmentIntersection = null;
			//see which segment this ray hits first

			//if there has been brushing, which side?
			List<Point2D> brushedPoints = new ArrayList<Point2D>();
			List<Integer> brushSides = new ArrayList<Integer>();
			for(int j=0;j<N;j++){
				Line2D segment = lines.get(j);
				Point2D intersect = getIntersection(obsCorner, segment);
				
				if(isParallel(segment,obsCorner)){
					Point2D closerPoint = length(new Line2D.Double(segment.getP1(),obs)) < length(new Line2D.Double(segment.getP2(),obs)) ?
							segment.getP1() : segment.getP2();

					double angle = getAngleBetweenVec(new Line2D.Double(obs, closerPoint), obsCorner);
					if(Math.abs(angle) < Math.PI/2.0){
						double dist = length(new Line2D.Double(closerPoint,obs));
						//this ray is blocked
						if(dist < minDistance){
							minDistance = dist;
							bestSegmentIndex = -1;
						}
					}
					continue;
				}
				
				if(isPointOnLine(segment,intersect)){
					Line2D obsInt = new Line2D.Double(obs, intersect);
					double angle = getAngleBetweenVec(obsInt, obsCorner);
					if(Math.abs(angle) < Math.PI/2.0){
						//is the same direction as ray
						//if touch intersection, is this brushing the intersection, or penetrating it?
						if(isPointAtCorner(intersect, segment)){
							int jPrev = j-1 < 0 ? N-1:j-1;
							//the endpoint of the touching segment
							Point2D nextSegmentOtherPoint = arePointsClose(intersect, segment.getP1()) ? 
									lines.get(jPrev).getP1() :
									lines.get((j+1)%N).getP2();
							
							Point2D thisSegmentOtherPoint = arePointsClose(intersect, segment.getP1()) ? 
									lines.get(j%N).getP2() :
									lines.get(j%N).getP1();
									

							boolean isBrush = isSameSideOfLine(obsInt, nextSegmentOtherPoint, thisSegmentOtherPoint);
							if(isBrush){
								int newSide = getSideOfLine(obsInt, nextSegmentOtherPoint);
								brushedPoints.add(intersect);
								brushSides.add(newSide);
							}else{
								double dist = length(obsInt);
								if(dist < minDistance){
									minDistance =dist;
									bestSegmentIndex = -1;
									//ray is blocked
								}
							}
						}else{
							double dist = length(obsInt);
							if(dist < minDistance){
								minDistance =dist;
								bestSegmentIndex = j;
								bestSegmentIntersection = intersect;
							}
						}
					}
				}
			}
			
			//go through all the brushed points and see if there exists two opposing brush points
			boolean isBlockedByBrush = false;
			int brushSide = 0;//neutral
			for(int k=0;k<brushedPoints.size();k++){
				if(length(new Line2D.Double(brushedPoints.get(k), obs)) < minDistance){
					int newSide = brushSides.get(k);
					if(brushSide == 0){
						brushSide = newSide;
					}else{
						if(brushSide != newSide){
							isBlockedByBrush = true;
							break;
						}
					}
				}
			}
			
			if(bestSegmentIndex != -1 && ! isBlockedByBrush){
				isVisible[bestSegmentIndex] = true;
			}
		}
		
		for(int i=0;i<N;i++){
			if(isVisible[i]){
				results.add(new Result(i,(i+1)%N));
			}
		}
		
		Collections.sort(results);
		out.println(results.size());
		for(Result r : results){
			out.println(input[r.p1][0] + " " + input[r.p1][1] + " " 
					+ input[r.p2][0] + " " + input[r.p2][1]);
		}
		out.close();
	}
	
	public static boolean isPointAtCorner(Point2D p, Line2D l){
		return arePointsClose(p,l.getP1()) || arePointsClose(p, l.getP2());
	}
	
	public static boolean arePointsClose(Point2D p1, Point2D p2){
		return Point2D.distance(p1.getX(), p1.getY(), p2.getX(), p2.getY()) < 0.0001;
	}
	
	public static double getAngleBetweenVec(Line2D v1, Line2D v2){
		double dx1 = v1.getX2() - v1.getX1();
		double dy1 = v1.getY2() - v1.getY1();

		double dx2 = v2.getX2() - v2.getX1();
		double dy2 = v2.getY2() - v2.getY1();
		
		return Math.atan2(dy2,dx2) - Math.atan2(dy1,dx1);
	}
	
	public static double length(Line2D l){
		return Point2D.distance(l.getX1(), l.getY1(), l.getX2(), l.getY2());
	}
	
	public static Point2D getMidpoint(Line2D l){
		return new Point2D.Double((l.getX1()+l.getX2())/2.0,(l.getY1()+l.getY2())/2.0);
	}
	
	
	//get intersection of two line segments given they are not parallel, when they are fully extended
	public static Point2D getIntersection(Line2D l1, Line2D l2){
		double dy1 = l1.getY2()-l1.getY1();
		double dx1 = l1.getX2()-l1.getX1();
		double dy2 = l2.getY2()-l2.getY1();
		double dx2 = l2.getX2()-l2.getX1();
		
		if(Math.abs(dx1) < 0.00001){
			double m2 = dy2/dx2;
			double c2 = l2.getY1() - m2*l2.getX1();
			//since not parallel, the other guy must not be vertical
			return new Point2D.Double(l1.getX1(),m2*l1.getX1()+c2);
		}
		
		if(Math.abs(dx2) < 0.00001){
			double m1 = dy1/dx1;
			double c1 = l1.getY1() - m1*l1.getX1();
			//since not parallel, the other guy must not be vertical
			return new Point2D.Double(l2.getX1(),m1*l2.getX1() + c1);
		}
		
		//no vertical lines, both not parallel
		double m1 = dy1/dx1;
		double m2 = dy2/dx2;
		double c1 = l1.getY1() - m1*l1.getX1();
		double c2 = l2.getY1() - m2*l2.getX1();
		double xi = (c2-c1)/(m1-m2);
		double yi = m1*xi+c1;
		
		return new Point2D.Double(xi,yi);
	}
	
	//assumes that segment is either entirely visible, or completely blocked from the observer
	//true iff visible
	public static boolean isVisible(Line2D segment, Point2D obs, Line2D blocker){
		//with the assumption, we just pick the midpoint to determine it's visible
		Point2D interest = new Point2D.Double((segment.getX1()+segment.getX2())/2.0,
				(segment.getY1()+segment.getY2())/2.0);
		return isVisible(interest, obs, blocker);
	}
	
	//is target point visible from obs if there is blocker?
	public static boolean isVisible(Point2D target, Point2D obs, Line2D blocker){
		Line2D obsFrom = new Line2D.Double(obs, blocker.getP1());
		Line2D obsTo = new Line2D.Double(obs, blocker.getP2());
		
		Point2D triangleCenter = new Point2D.Double((obs.getX()+blocker.getX1()+blocker.getX2())/3.0,
				(obs.getY()+blocker.getY1()+blocker.getY2())/3.0);
		
		//visible only if not at the dark shadow behind the triangle formed by o,blocker
		return !(isSameSideOfLine(obsFrom, triangleCenter, target) &&
				isSameSideOfLine(obsTo, triangleCenter, target) &&
				!isSameSideOfLine(blocker, triangleCenter, target));
	}
	
	public static boolean isSameSideOfLine(Line2D divide, Point2D p1, Point2D p2){
		int side1 = getSideOfLine(divide, p1);
		int side2 = getSideOfLine(divide, p2);
		return side1 == side2  || side1*side2 == 0;
	}
	
	//true iff remLine is parallel to the observer
	public static boolean isParallel(Line2D l1, Line2D l2){
		int dy2 = (int) l2.getY2()-(int)l2.getY1();
		int dx2 = (int) l2.getX2()-(int)l2.getX1();
		int dy1 = (int) l1.getY2()-(int)l1.getY1();
		int dx1 = (int) l1.getX2()-(int)l1.getX1();
		return dy1*dx2 == dy2*dx1;
	}
	
	//true iff l1 and l2 are crossing
	public static boolean isCross(Line2D l1, Line2D l2){
		return !isSameSideOfLine(l2, l1.getP1(), l1.getP2()) &&
				!isSameSideOfLine(l1,l2.getP1(), l2.getP2());
	}
	
	//0 if on line, -1 or 1 if on some side
	public static int getSideOfLine(Line2D divide, Point2D p1){
		double[] vDiv = new double[]{divide.getX2()-divide.getX1(),
				divide.getY2()-divide.getY1(),0};
		double[] v1 = new double[]{p1.getX()-divide.getX1(),
				p1.getY()-divide.getY1(),0};
		
		double c = crossProduct(vDiv,v1)[2];
		if(Math.abs(c) < 0.00001){
			return 0;
		}else if(c>0){
			return 1;
		}else{
			return -1;
		}
	}
	
	
	public static boolean isPointOnLine(Line2D l, Point2D p){
		return l.ptSegDist(p) < 0.001;
	}
	public static Point2D touchesAtEndpoint(Line2D l1, Line2D l2){
		if(l1.getP1().equals(l2.getP1()) || l1.getP1().equals(l2.getP2())){
			return l1.getP1();
		}
		
		if(l1.getP2().equals(l2.getP1()) || l1.getP2().equals(l2.getP2())){
			return l1.getP2();
		}
		
		return null;
	}
	
	public static double[] crossProduct(double[] v1, double[] v2){
		return new double[]{(v1[1]*v2[2]-v1[2]*v2[1]), 
				(v1[2]*v2[0]-v1[0]*v2[2]),
				(v1[0]*v2[1] - v1[1]*v2[0])};
	}
	
	public static String str(Line2D l){
		return str(l.getP1()) + "->" + str(l.getP2());
	}
	public static String str(Point2D pt){
		return "(" + pt.getX() + "," + pt.getY() + ")";
	}
	
	//indices of points wrt to how early they come in the inputs
	public static class Result implements Comparable<Result>{
		public int p1;
		public int p2;
		
		public Result(int p1, int p2){
			this.p1 = Math.min(p1,p2);
			this.p2 = Math.max(p1, p2);
		}

		@Override
		public int compareTo(Result o) {
			int k =  this.p2 - o.p2;
			if(k!=0)return k;
			return this.p1 - o.p1;
		}
	}
}