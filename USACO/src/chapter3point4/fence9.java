package chapter3point4;

/*
 Tests:
 10 20 10 
 81
 
 7 5 10
 20
 
 100 200 50
 4901
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: fence9
 */
public class fence9 {
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("fence9.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("fence9.out")));
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		
		long n = Integer.parseInt(tkz.nextToken());
		long m = Integer.parseInt(tkz.nextToken());
		long p = Integer.parseInt(tkz.nextToken());
		
		if(p >= n){
			long count = countDots(n,m,p, false);
			out.write(count + "\n");
			out.close();
			return;
		}else{
			long countAll = countDots(n,m,n, false);
			long countSmall = countDots(n-p,m,n-p, true);
			
			out.write(countAll - countSmall + "\n");
			out.close();
			return;
		}
	}
	
	//count only if p>=n
	public static long countDots(long n , long m, long p, boolean countAlongLine){
		long count = 0;
		if(n!=0){
			long up = n == p? n-1: n;
			for(int x = 1; x<=up; x++){
				int y = (int) (1.0*m*x/n);
				count += y;
				if(m*x % n == 0 && !countAlongLine){
					count--;
				}
			}
		}
		
		if(n != p){
			for(long x = n+1; x<p; x++){
				int y = (int)(1.0*m*(x-p)/(n-p));
				count+=y;
				if(m*(x-p) % (n-p) == 0){
					count--;
				}
			}
		}
		return count;
	}
}
