package chapter2point2;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: preface
 */
public class preface {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("preface.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("preface.out")));
		
		int N = Integer.parseInt(f.readLine());
		
		char[] romans = new char[]{'I','V','X','L','C','D','M'};
		int[] count = new int[1000];
		for(int i=1;i<=N;i++){
			String s = romanize(i);
			for(int j=0;j<s.length();j++){
				count[s.charAt(j)]++;
			}
		}
		
		for(int i=0;i<romans.length;i++){
			if(count[romans[i]]!=0)
				out.println(romans[i] + " " + count[romans[i]]);
		}
		/*
		int highestNonZero = romans.length-1;
		for(;highestNonZero>=0;highestNonZero--){
			if(count[romans[highestNonZero]] != 0){
				break;
			}
		}
		
		for(int i=0;i<=highestNonZero;i++){
			out.println(romans[i] + " " + count[romans[i]]);
		}*/
		out.close();
	}
	
	private static String[] cache = new String[3501];
	
	public static String romanize(int i){
		if(cache[i] != null){
			return cache[i];
		}
		String s = "";
		int curNum = i;
		int digCount = 1;
		while(curNum > 0){
			int lastDig = curNum % 10;
			
			s = getRoman(lastDig, digCount) + s;
			
			curNum/=10;
			digCount++;
		}
		cache[i] = s;
		return s;
	}
	
	//dig, one five ten
	private static char[][] digmap = new char[][]{
		{'I','V','X'},
		{'X','L','C'},
		{'C','D','M'},
		{'M','?','?'}
	};
	
	public static String getRoman(int dig, int digCount){
		char one = digmap[digCount-1][0];
		char five = digmap[digCount-1][1];
		char ten = digmap[digCount-1][2];
		
		switch(dig){
		case 0:
			return "";
		case 1:
			return "" + one;
		case 2:
			return "" + one + one;
		case 3:
			return "" + one + one + one;
		case 4:
			return "" + one + five;
		case 5:
			return "" + five;
		case 6:
			return "" + five + one;
		case 7:
			return "" + five + one +one;
		case 8: 
			return "" + five + one + one + one;
		case 9:
			return "" + one + ten;
		}
		
		return "" + -1;
	}
}