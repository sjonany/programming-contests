package chapter2point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/*
ID: xenoslash
LANG: JAVA
TASK: lamps
 */
public class lamps {
	
	private static int N;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("lamps.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("lamps.out")));
		
		N = Integer.parseInt(f.readLine().trim());
		int C = Integer.parseInt(f.readLine().trim());
		
		List<Integer> ons = new ArrayList<Integer>();
		List<Integer> offs = new ArrayList<Integer>();
		
		String[] ontoks = f.readLine().split(" ");
		for(String s : ontoks){
			Integer i = Integer.parseInt(s);
			if(i==-1){
				break;
			}
			
			ons.add(i);
		}
		
		String[] offtoks = f.readLine().split(" ");
		for(String s : offtoks){
			Integer i = Integer.parseInt(s);
			if(i==-1){
				break;
			}
			
			offs.add(i);
		}
		
		PriorityQueue<String> resQ = new PriorityQueue<String>();
		List<Integer> posMoves = getStates(C);
		/*
		for(int i:posMoves){
			System.out.print(Integer.toBinaryString(i) + " ");
		}
		System.out.println();*/
		
		for(int move : posMoves){
			boolean[] states = new boolean[N+1];
			for(int i=1;i<=N;i++){
				states[i] = true;
			}
			
			makeMoves(move, states);
			
			boolean isValid = true;
			for(int on: ons){
				if(!states[on]){
					isValid = false;
					break;
				}
			}
			
			if(isValid){
				for(int off: offs){
					if(states[off]){
						isValid = false;
						break;
					}
				}
			}
			
			if(isValid){
				String ans = "";
				for(int i=1;i<=N;i++){
					if(states[i]){
						ans += "1";
					}else{
						ans += "0";
					}
				}
				resQ.add(ans);
			}
		}
		
		if(resQ.isEmpty()){
			out.println("IMPOSSIBLE");
		}else{
			while(!resQ.isEmpty()){
				out.println(resQ.remove());
			}
		}
		out.close();
	}

	public static void makeMoves(int move, boolean[] states){
		if((move & 1) != 0){
			for(int i=1; i<= N;i++){
				states[i] = !states[i];
			}
		}
		
		if((move & 2) != 0){
			for(int i=1; i<=N;i+=2){
				states[i] = !states[i];
			}
		}
		
		if((move & 4) != 0){
			for(int i=2; i<=N;i+=2){
				states[i] = !states[i];
			}
		}
		
		if((move & 8) != 0){
			for(int i=1; i<=N;i+=3){
				states[i] = !states[i];
			}
		}
	}
	
	//a list of move states, where each move state = bin string of length 4
	public static List<Integer> getStates(int C){
		List<Integer> res = new ArrayList<Integer>();
		if(C==0){
			res.add(0);
			return res;
		}
		
		for(int i=0; i<=15;i++){
			int bitC = Integer.bitCount(i);
			if(C % 2 ==1){
				if(C==1){
					if(bitC == 1){
						res.add(i);
					}
				}else if(bitC % 2 ==1){
					res.add(i);
				}
			}else{
				if(C==2){
					if(bitC == 0 || bitC == 2){
						res.add(i);
					}
				}else if(bitC % 2 == 0){
					res.add(i);
				}
			}
		}
		return res;
	}
}