package chapter2point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: subset
 */
public class subset {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("subset.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("subset.out")));
		
		int N = Integer.parseInt(f.readLine());
		int totalSum = N*(N+1)/2;
		
		if(totalSum %2 != 0){
			out.println(0);
		}else{
			int targetSum = totalSum/2;
			long[][] dp = new long[N][targetSum+1];
			//dp[i][j] = #sets to sum to equal j if consider elements 0 to i
			dp[0][0] = 1;
			dp[0][1] = 1;
			for(int i=1;i<N;i++){
				for(int j=0;j<=targetSum;j++){
					int el = i+1;
					if(j<el){
						dp[i][j] = dp[i-1][j];
					}else{
						dp[i][j] = dp[i-1][j] + dp[i-1][j-el];
					}
				}
			}
			//note: don't forget to divide answer by 2, because of symmetry
			out.println(dp[N-1][targetSum]/2);
		}
		
		out.close();
	}
}