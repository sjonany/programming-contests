package chapter2point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: runround
 */
public class runround {
	private static Long answer = null;
	private static long M;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("runround.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("runround.out")));
		
		M = Integer.parseInt(f.readLine().trim());
		int dig = Long.toString(M).length();
		while(answer == null && dig < 10){
			comb(dig, 1, 1, new int[dig]);
			dig++;
		}
		out.println(answer);
		out.close();
	}

	//digit = constant. goal number of digits
	//toSet = which digit to set now, base 1
	//curChoice = 1-> digit, which one of these is being considered now
	public static void comb(int digit, int toSet, int curChoice, int[] filled){
		int digitsIcanUse = 9-curChoice+1;
		int spotsLeftToFill = digit-toSet + 1; 
		if(digitsIcanUse < spotsLeftToFill){
			//can't fulfill the size requirement
			return;
		}
		if(toSet == digit+1){
			permute(digit, 1, "", new boolean[digit], filled);
		}else if(curChoice == 10){
			return;
		}else{
			filled[toSet-1] = curChoice;
			comb(digit, toSet+1, curChoice+1, filled);
			comb(digit, toSet, curChoice+1, filled);
		}
	}
	
	public static void permute(int digit, int toSet, String filled, boolean[] used, int[] usable){
		if(toSet == digit+1){
			long filledLong = Long.parseLong(filled);
			if(isrunround(filled) && filledLong > M && (answer == null || answer > filledLong)){
				answer = filledLong;
			}
		}else{
			for(int i =0; i<usable.length; i++){
				if(!used[i]){
					used[i] = true;
					permute(digit, toSet+1, filled+usable[i], used, usable);
					used[i] = false;
				}
			}
		}
	}
	
	public static boolean isrunround(String s){
		boolean[] isVisited = new boolean[s.length()];
		int curIndex = 0;
		for(int move=0;move<s.length();move++){
			if(isVisited[curIndex])
				return false;
			isVisited[curIndex] = true;
			curIndex = (curIndex + s.charAt(curIndex)-'0') % s.length();
		}
		return s.charAt(curIndex) == s.charAt(0);
	}
}