package chapter2point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.PriorityQueue;

/*
ID: xenoslash
LANG: JAVA
TASK: money
 */
public class money {
	static int N;
	static int V;
	static PriorityQueue<String> dos = new PriorityQueue<String>();
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("money.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("money.out")));
		
		String[] toks = f.readLine().split(" ");
		V = Integer.parseInt(toks[0]);
		N = Integer.parseInt(toks[1]);
		
		int[] coins = new int[V];
		int i =0;
		
		String line = f.readLine();
		while(line != null && i < V){
			toks = line.split(" ");
			for(int j=0;j<toks.length;j++){
				coins[i] = Integer.parseInt(toks[j]);
				i++;
			}
			line = f.readLine();
		}
		//dp[i][j] = how many ways if consider 0..i coins to sum exactly to j
		
		long[][] dp = new long[V][N+1];
		
		for(int ci = 0; ci<V; ci++){
			for(int v=1;v<N+1; v++){
				//ways to get val without using this new coin
				long prevDp = ci > 0 ? dp[ci-1][v] : 0;
				if(v - coins[ci] < 0){
					dp[ci][v] = prevDp;
				}else if(v == coins[ci]){
					dp[ci][v] = prevDp + 1;
				}else{
					dp[ci][v] = prevDp + dp[ci][v-coins[ci]];
				}
			}
		}
		
		//for(long[] ar : dp){
		//	System.out.println(Arrays.toString(ar));
		//}
		out.println(dp[V-1][N]);
		out.close();
	}
	
}