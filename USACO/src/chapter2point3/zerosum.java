package chapter2point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.PriorityQueue;

/*
ID: xenoslash
LANG: JAVA
TASK: zerosum
 */
public class zerosum {
	static int N;
	static PriorityQueue<String> dos = new PriorityQueue<String>();
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("zerosum.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("zerosum.out")));
		//System.out.println(eval("1+2"));
		
		N = Integer.parseInt(f.readLine());
		dfs(0, new String[N-1]);
		
		while(!dos.isEmpty()){
			out.println(dos.remove());
		}
		out.close();
	}
	
	public static void dfs(int toSet, String[] filled){
		if(toSet == filled.length){
			if(eval(filled) == 0){
				String s = "" + 1;
				for(int i=2;i<=N;i++){
					s += (filled[i-2] + i);
				}
				dos.add(s);
			}
		}else{
			filled[toSet]="+";
			dfs(toSet+1, filled);
			
			filled[toSet]="-";
			dfs(toSet+1, filled);
			
			filled[toSet]=" ";
			dfs(toSet+1, filled);
		}
	}
	
	public static int eval(String s){
		boolean lastReadOp = true;
		String buildingNum = "";
		char op = ' ';
		int result = 0;
		
		for(int i=0;i<s.length();i++){
			char c = s.charAt(i);
			if(c != ' '){
				if(lastReadOp){
					//now for sure read number
					buildingNum += c;
					lastReadOp = false;
				}else{
					if(c == '+' || c == '-'){
						if(op == '+'){
							result += Integer.parseInt(buildingNum);
						}else if(op == '-'){
							result -= Integer.parseInt(buildingNum);
						}else if(op == ' '){
							//hack - short init
							result = Integer.parseInt(buildingNum);
						}else{
							System.out.println("?!!!");
						}
						op = c;
						buildingNum = "";
						lastReadOp = true;
					}else{
						buildingNum += c;
					}
				}
			}
		}
		
		if(op == '+'){
			result += Integer.parseInt(buildingNum);
		}else{
			result -= Integer.parseInt(buildingNum);
		}
		
		return result;
	}
	
	public static int eval(String[] ops){
		String s = "" + 1;
		for(int i=2;i<=N;i++){
			s += (ops[i-2] + i);
		}
		return eval(s);
	}
}