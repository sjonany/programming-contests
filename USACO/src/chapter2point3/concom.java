package chapter2point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/*
ID: xenoslash
LANG: JAVA
TASK: concom
 */
public class concom {
	//adjMath[i][j] = comp i owns comp j by adjMat[i][j] percent
	static int[][] adjMat = new int[101][101];
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("concom.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("concom.out")));
		
		int lineC = Integer.parseInt(f.readLine());
		for(int i=0;i<lineC;i++){
			String[] toks = f.readLine().split(" ");
			int from = Integer.parseInt(toks[0]);
			int to = Integer.parseInt(toks[1]);
			int perc = Integer.parseInt(toks[2]);
			adjMat[from][to] = perc;
		}
		
		for(int from=1;from<=100;from++){
			PriorityQueue<Integer> controlled = getControl(from);
			while(!controlled.isEmpty()){
				int toC = controlled.remove();
				if(from != toC)
					out.println(from + " " + toC);
			}
		}
		out.close();
	}
	
	public static PriorityQueue<Integer> getControl(int from){
		PriorityQueue<Integer> conquered = new PriorityQueue<Integer>();
		boolean[] isCollected = new boolean[101];
		int[] perMat = new int[101];
		Queue<Integer> col = new LinkedList<Integer>();
		col.add(from);
		isCollected[from] = true;
		while(!col.isEmpty()){
			int curSource = col.remove();
			conquered.add(curSource);
			for(int dest =1; dest<=100; dest++){
				perMat[dest] += adjMat[curSource][dest];
				if(perMat[dest] > 50 && !isCollected[dest]){
					col.add(dest);
					isCollected[dest] = true;
				}
			}
		}
		
		return conquered;
	}
}