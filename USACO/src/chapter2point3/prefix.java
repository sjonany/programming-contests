package chapter2point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: prefix
 */
public class prefix {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("prefix.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("prefix.out")));
		
		List<String> prims = new ArrayList<String>();
		String line = f.readLine();
		while(!line.trim().equals(".")){
			StringTokenizer tknz = new StringTokenizer(line);
			while(tknz.hasMoreTokens()){
				prims.add(tknz.nextToken());
			}
			line = f.readLine();
		}
		
		line = f.readLine();
		StringBuilder s = new StringBuilder();
		
		while(line != null){
			s.append(line.trim());
			line = f.readLine();
		}
		
		//isValid[i] = is s[0..i] constructable?
		boolean[] isValid = new boolean[s.length()];
		int maxLength = 0;
		for(int i=0;i<s.length();i++){
			for(int j=0;j<prims.size();j++){
				String prim = prims.get(j);
				if(prim.length() > i+1){
					continue;
				}
				
				boolean match = true;
				for(int k=0;k<prim.length();k++){
					if(prim.charAt(k) != s.charAt(k + i-prim.length()+1)){
						match = false;
						break;
					}
				}
				
				if(match && (i+1 == prim.length() || isValid[i - prim.length()])){
					isValid[i] = true;
					maxLength = i+1;
					break;
				}
			}
		}
		
		out.println(maxLength);
		out.close();
	}
}