package chapter2point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: nocows
 */
public class nocows {
	static int MOD = 9901;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("nocows.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("nocows.out")));
		
		String[] toks = f.readLine().split(" ");
		int N = Integer.parseInt(toks[0]);
		int H = Integer.parseInt(toks[1]);
		
		if(N % 2 == 0){
			out.println(0);
			out.close();
			return;
		}
		
		//num branches
		int B = (N-1)/2;
		//dp[b][h] = number of distinct pedigrees if b branches and height = h
		int[][] dp = new int[B+1][H+1];
		//sum[b][h] = sum from height=1 to h of dp[b][h];
		int[][] sum = new int[B+1][H+1];
		dp[0][1] = 1;
		for(int h=1;h<=H;h++){
			sum[0][h] = 1;
		}
		
		for(int b=1; b <= B; b++){
			for(int h=2; h <= H; h++){
				//System.out.println("b=" + b + ", h=" + h);
				//if T1 is the deeper one
				dp[b][h] = 0;
				for(int bT1= 0; bT1<=b-1; bT1++){
					//System.out.println("bT1 = " + bT1 + ", dp[bT1][h-1] = " + dp[bT1][h-1]);
					dp[b][h] = (dp[b][h] + dp[bT1][h-1] * sum[b-bT1-1][h-2])% MOD;
				}
				
				//System.out.println("if T1 is deeper = " + dp[b][h]);
				//if T2 is deeper one, symmetric
				dp[b][h] = (2*dp[b][h]) % MOD;
				
				//if T1 and T2 are of the same height
				for(int bT1= 0; bT1<=b-1; bT1++){
					dp[b][h] = (dp[b][h] + dp[bT1][h-1] * dp[b-bT1-1][h-1])% MOD;
				}
				
				//System.out.println("if t1 and t2 are of the same height = " + test);
				//System.out.println("total = " + dp[b][h]);
				sum[b][h] = (sum[b][h-1] + dp[b][h]) % MOD;
			}
		}
		
		
		/*for(int b=0; b <= B; b++){
			System.out.println("b = " + b + " -> " + Arrays.toString(dp[b]));
		}*/
		
		out.println(dp[B][H]);
		out.close();
	}
}