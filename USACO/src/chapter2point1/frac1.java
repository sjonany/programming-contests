package chapter2point1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.PriorityQueue;

/*
ID: xenoslash
LANG: JAVA
TASK: frac1
 */
public class frac1 {
	//x/y , if used already in reduced form, true
	public static boolean[][] isUsed;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("frac1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("frac1.out")));
		
		int N = Integer.parseInt(f.readLine());
		isUsed = new boolean[N+1][N+1];
		PriorityQueue<Fraction> q = new PriorityQueue<Fraction>();
		for(int denom = 1; denom<=N; denom++){
			for(int num=0; num<=denom; num++){
				if(!isUsed[num][denom]){
					isUsed[num][denom] = true;
					for(int scale =1; scale <= N / denom; scale++){
						isUsed[num*scale][denom*scale] = true;
					}
					
					q.add(new Fraction(num,denom));
				}
			}
		}
		
		while(!q.isEmpty()){
			Fraction fr = q.remove();
			out.println(fr.num + "/" + fr.denom);
		}
		
		
		out.close();
	}
	
	public static class Fraction implements Comparable{
		public int num;
		public int denom;
	
		public Fraction(int n, int d){
			this.num = n;
			this.denom = d;
		}
	
		@Override
		public int compareTo(Object o) {
			Fraction other = (Fraction)o;

			return this.num * other.denom - this.denom * other.num;
		}
	}
}