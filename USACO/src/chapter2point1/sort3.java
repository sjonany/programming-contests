package chapter2point1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: sort3
 */
public class sort3 {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("sort3.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("sort3.out")));
		int N = Integer.parseInt(f.readLine());
		int[] arr = new int[N];
		int[] count = new int[4];
		for(int i=0;i<N;i++){
			arr[i] = Integer.parseInt(f.readLine());
			count[arr[i]]++;
		}
		
		int countSwap = 0;
		int[][] easySwap= new int[4][4];
		int mismatch = 0;
		//easySwap [x][y] = number of x's that lies in y's sorted spot
		
		for(int i=0;i<N;i++){
			if(i<count[1]){
				if(arr[i] != 1){
					mismatch++;
				}
				//1's spot
				easySwap[arr[i]][1]++;
			}else if(i>=count[2]+count[1]){
				if(arr[i] != 3){
					mismatch++;
				}
				//3's spot
				easySwap[arr[i]][3]++;
			}else{
				if(arr[i] != 2){
					mismatch++;
				}
				//2's spot
				easySwap[arr[i]][2]++;
			}
		}
		
		int oneThreeSwap = Math.min(easySwap[1][3], easySwap[3][1]);
		int twoThreeSwap = Math.min(easySwap[2][3], easySwap[3][2]);
		int oneTwoSwap = Math.min(easySwap[1][2], easySwap[2][1]);
		countSwap += (oneThreeSwap + twoThreeSwap + oneTwoSwap);
		mismatch -= 2*(oneThreeSwap + twoThreeSwap + oneTwoSwap); 
		//System.out.println(mismatch);
		countSwap += 2*(mismatch/3);
		
		out.println(countSwap);
		out.close();
	}
}