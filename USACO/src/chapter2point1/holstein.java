package chapter2point1;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
ID: xenoslash
LANG: JAVA
TASK: holstein
 */
public class holstein {
	//x/y , if used already in reduced form, true
	public static boolean[][] isUsed;
	public static int minScoop;
	public static List<Integer> minFeeds = new ArrayList<Integer>();
	public static int minSum = -1;
	public static int V;
	public static int G;
	static int[][]feeds;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("holstein.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("holstein.out")));
		
		V = Integer.parseInt(f.readLine());
		int[] req = new int[V];
		
		String[] reqTok = f.readLine().split(" ");
		for(int i=0;i<V;i++){
			req[i] = Integer.parseInt(reqTok[i]);
		}
		
		G = Integer.parseInt(f.readLine());
		minScoop = G;
		
		feeds = new int[G][V];
		for(int lineC = 0; lineC < G; lineC++){
			String[] vit = f.readLine().split(" ");
			List<String> nonemptyToks = new ArrayList<String>();
			for(String s : vit){
				if(s.trim().length()!=0){
					nonemptyToks.add(s);
				}
			}
			for(int i=0;i<V;i++){
				feeds[lineC][i] = Integer.parseInt(nonemptyToks.get(i));
			}
			minFeeds.add(lineC);
			minSum += lineC;
		}
		
		List<Integer> toSatisfy = new ArrayList<Integer>();
		for(int i=0;i<V;i++){
			toSatisfy.add(i);
		}
		tryFeed(req, toSatisfy, 0, new LinkedList<Integer>());
		out.print(minScoop + " ");
		for(int i=0;i<minFeeds.size();i++){
			if(i!=minFeeds.size()-1){
				out.print((minFeeds.get(i)+1) + " ");
			}else{
				out.println((minFeeds.get(i)+1));
			}
		}
		out.close();
	}
	
	//remainReq = how much more of each vitamin the cow needs
	//toSet goes from 0 to G-1, which feed to decide
	//feeds = feeds that cause remainReq 
	//toSatisfy = 0 to V-1 which vitamins are still lacking
	public static void tryFeed(int[] remainReq, List<Integer> toSatisfy, int toSet, LinkedList<Integer> usedFeeds){
		if(toSatisfy.size() == 0){
			if(usedFeeds.size() < minScoop){
				minScoop = usedFeeds.size();
				minFeeds = new ArrayList<Integer>(usedFeeds);
				int sum = 0;
				for(Integer usedFeed : usedFeeds){
					sum+=usedFeed;
				}
				minSum = sum;
			}else if(usedFeeds.size() == minScoop){
				int sum = 0;
				for(Integer usedFeed : usedFeeds){
					sum+=usedFeed;
				}
				if(sum < minSum){
					minSum = sum;
					minFeeds = new ArrayList<Integer>(usedFeeds);
				}
			}
		}else if(toSet < G){
			int[] feed = feeds[toSet];
			//go without
			tryFeed(remainReq, toSatisfy, toSet+1, usedFeeds);
			//go with
			List<Integer> newToSatisfy = new ArrayList<Integer>();
			for(Integer vitLack : toSatisfy){
				remainReq[vitLack] -= feed[vitLack];
				if(remainReq[vitLack] > 0){
					newToSatisfy.add(vitLack);
				}
			}
			usedFeeds.add(toSet);
			tryFeed(remainReq, newToSatisfy, toSet+1, usedFeeds);
			//uncheck
			for(Integer vitLack : toSatisfy){
				remainReq[vitLack] += feed[vitLack];
			}
			usedFeeds.removeLast();	
		}
	}
}