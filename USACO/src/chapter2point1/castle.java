package chapter2point1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: castle
 */
public class castle {
	static int ROW;
	static int COL;
	static int area[][];
	static int wall[][];
	static int compId[][];
	
	static int UP = 2;
	static int RIGHT = 4;
	static int DOWN = 8;
	static int LEFT = 1;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("castle.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("castle.out")));
		String[] tok1 = f.readLine().split(" ");
		ROW = Integer.parseInt(tok1[1]);
		COL = Integer.parseInt(tok1[0]);
	
		wall = new int[ROW][COL];
		area = new int[ROW][COL];
		compId = new int[ROW][COL];
		
		for(int r=0;r<ROW;r++){
			String[] toks = f.readLine().split(" ");
			for(int c=0;c<COL;c++){
				wall[r][c] = Integer.parseInt(toks[c]);
			}
		}
		
		int numComp = 0;
		int maxArea = 0;
		for(int r=0;r<ROW;r++){
			for(int c=0;c<COL;c++){
				if(area[r][c] == 0){
					numComp++;
					int area = getArea(r,c, numComp);
					updateArea(r,c,area, numComp);
					maxArea = Math.max(maxArea, area);
				}
			}
		}
		
		/*
		for(int[] row : area)
		{
			System.out.println(Arrays.toString(row));
		}*/
		
		int maxTotArea = 0;
		int wallR = 0;
		int wallC = 0;
		char dir = 'x';
		for(int c=0;c<COL;c++){
			for(int r=ROW-1;r>=0;r--){
				if(r>0 && compId[r][c] != compId[r-1][c]){
					int totArea = area[r][c] + area[r-1][c];
					if(totArea > maxTotArea){
						maxTotArea = totArea;
						wallR = r;
						wallC = c;
						dir = 'N';
					}
				}
				
				if(c<COL-1 && compId[r][c] != compId[r][c+1]){
					int totArea = area[r][c] + area[r][c+1];
					if(totArea > maxTotArea){
						maxTotArea = totArea;
						wallR = r;
						wallC = c;
						dir = 'E';
					}
				}
			}
		}
		
		
		out.println(numComp);
		out.println(maxArea);
		out.println(maxTotArea);
		out.println((wallR+1) + " " + (wallC+1) + " " + dir);
		out.close();
	}
	
	//mark the connected ones with -1
	public static int getArea(int r, int c, int numComp){
		compId[r][c] = numComp;
		int thisArea = 1;
		//up 
		if(r > 0 && (wall[r][c] & UP) == 0 && compId[r-1][c] == 0){
			thisArea += getArea(r-1,c, numComp);
		}
		
		//down
		if(r < ROW-1 && (wall[r][c] & DOWN) == 0 && compId[r+1][c] == 0){
			thisArea += getArea(r+1,c, numComp);
		}
		
		//left
		if(c > 0 && (wall[r][c] & LEFT) == 0 && compId[r][c-1] == 0){
			thisArea += getArea(r,c-1, numComp);
		}
		
		//right
		if(c < COL-1 && (wall[r][c] & RIGHT) == 0 && compId[r][c+1] == 0){
			thisArea += getArea(r,c+1, numComp);
		}
		
		return thisArea;
	}
	
	//change all connected -1 to area
	public static void updateArea(int r, int c, int totArea, int numComp){
		if(area[r][c] !=0)return;
		area[r][c] = totArea;
		//up 
		if(r > 0 && (wall[r][c] & UP) == 0 && compId[r-1][c] == numComp){
			updateArea(r-1,c,totArea,numComp);
		}
		
		//down
		if(r < ROW-1 && (wall[r][c] & DOWN) == 0 && compId[r+1][c] == numComp){
			updateArea(r+1,c,totArea,numComp);
		}
		
		//left
		if(c > 0 && (wall[r][c] & LEFT) == 0 && compId[r][c-1] == numComp){
			updateArea(r,c-1,totArea, numComp);
		}
		
		//right
		if(c < COL-1 && (wall[r][c] & RIGHT) == 0 && compId[r][c+1] == numComp){
			updateArea(r,c+1,totArea, numComp);
		}
	}
}
