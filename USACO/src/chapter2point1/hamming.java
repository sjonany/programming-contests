package chapter2point1;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/*
ID: xenoslash
LANG: JAVA
TASK: hamming
 */
public class hamming {
	static int N;
	static int B;
	static int D;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("hamming.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("hamming.out")));
		String[] toks = f.readLine().split(" ");
		N = Integer.parseInt(toks[0]);
		B = Integer.parseInt(toks[1]);
		D = Integer.parseInt(toks[2]);
		
		List<Integer> hamSet = new ArrayList<Integer>();
		for(int i=0;i<(1<<B) && hamSet.size()<N; i++){
			boolean isValid = true;
			for(int el : hamSet){
				if(Integer.bitCount(el ^ i) < D){
					isValid = false;
					break;
				}
			}
			
			if(isValid){
				hamSet.add(i);
			}
		}
		
		int count = 0;
		for(int ham: hamSet){
			out.print(ham);
			count++;
			if(count % 10 == 0 && count != hamSet.size()){
				out.println();
			}else if(count != hamSet.size()){
				out.print(" ");
			}
		}
		out.println();
		
		out.close();
	}
}