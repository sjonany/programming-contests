package chapter3point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: shopping
 */
public class shopping {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("shopping.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("shopping.out")));
		
		int s = Integer.parseInt(f.readLine());
		StringBuffer offerInput = new StringBuffer();
		for(int i=0;i<s;i++){
			offerInput.append(f.readLine() + "\n");
		}
		
		int b = Integer.parseInt(f.readLine());
		int[] REQ = new int[1000];
		int[] ORI_PRICE = new int[1000];
		PriorityQueue<Integer> buyCodes = new PriorityQueue<Integer>();
		for(int i=0;i<b;i++){
			StringTokenizer tkz = new StringTokenizer(f.readLine());
			int code = Integer.parseInt(tkz.nextToken());
			int amount = Integer.parseInt(tkz.nextToken());
			int price = Integer.parseInt(tkz.nextToken());
			
			buyCodes.add(code);
			ORI_PRICE[code] = price;
			REQ[code] = amount;
		}
		
		int[] sortedAmount = new int[5];
		int[] sortedPrice = new int[5];
		//map from code -> sorted Index
		int[] index = new int[1000];
		for(int i=0;i<1000;i++){index[i] = -1;}
		int sortedI = 0;
		while(!buyCodes.isEmpty()){
			int code = buyCodes.remove();
			index[code] = sortedI;
			sortedAmount[sortedI] = REQ[code];
			sortedPrice[sortedI] = ORI_PRICE[code];
			sortedI++;
		}
		//System.out.println(Arrays.toString(sortedAmount));
		//System.out.println(Arrays.toString(sortedPrice));
		
		List<Offer> offers = new ArrayList<Offer>();
		StringTokenizer tkz = new StringTokenizer(offerInput.toString());
		for(int i=0;i<s;i++){
			int n = Integer.parseInt(tkz.nextToken());
			int[] amount =  new int[5];
			boolean isRelevant = true;
			for(int j=0;j<n;j++){
				int c = Integer.parseInt(tkz.nextToken());
				int k = Integer.parseInt(tkz.nextToken());
				if(index[c] == -1){
					isRelevant = false;
					break;
				}else{
					amount[index[c]] = k;
				}
			}
			
			if(isRelevant){
				Offer o = new Offer();
				o.amount = amount;
				o.price = Integer.parseInt(tkz.nextToken());
				offers.add(o);
			}
		}
		//System.out.println(offers);
		
		//minimum cost to completely consume that amount using offers
		int[][][][][] dp = new int[6][6][6][6][6];
		int overallMinCost = Integer.MAX_VALUE;
		dp[0][0][0][0][0] = 0;
		for(int aa=0;aa<=sortedAmount[0];aa++){
			for(int bb=0;bb<=sortedAmount[1];bb++){
				for(int cc=0;cc<=sortedAmount[2];cc++){
					for(int dd=0;dd<=sortedAmount[3];dd++){
						for(int ee=0;ee<=sortedAmount[4];ee++){
							if(aa+bb+cc+dd+ee == 0){
								int nonDiscountPrice = ((sortedAmount[0]-aa) * sortedPrice[0]) +
									((sortedAmount[1]-bb) * sortedPrice[1])+
									((sortedAmount[2]-cc) * sortedPrice[2])+
									((sortedAmount[3]-dd) * sortedPrice[3])+
									((sortedAmount[4]-ee) * sortedPrice[4]);
								overallMinCost = Math.min(overallMinCost, nonDiscountPrice);
								continue;
							}
							int minCost = Integer.MAX_VALUE;
							for(Offer o : offers){
								int aM = aa - o.amount[0];
								int bM = bb - o.amount[1];
								int cM = cc - o.amount[2];
								int dM = dd - o.amount[3];
								int eM = ee - o.amount[4];
								if(aM < 0 || bM < 0|| cM < 0|| dM < 0|| eM <0 || dp[aM][bM][cM][dM][eM] == Integer.MAX_VALUE){
									continue;
								}else{
									minCost = Math.min(minCost,  dp[aM][bM][cM][dM][eM] + o.price);
								}
							}
							dp[aa][bb][cc][dd][ee] = minCost;
							if(minCost != Integer.MAX_VALUE){
								//System.out.println(aa + " " + bb + " " + cc  + " " + dd + " " + ee);
								
								int nonDiscountPrice = ((sortedAmount[0]-aa) * sortedPrice[0]) +
										((sortedAmount[1]-bb) * sortedPrice[1])+
										((sortedAmount[2]-cc) * sortedPrice[2])+
										((sortedAmount[3]-dd) * sortedPrice[3])+
										((sortedAmount[4]-ee) * sortedPrice[4]);
								//System.out.println("non discount = " + nonDiscountPrice +", minCost = " + minCost );
								overallMinCost = Math.min(overallMinCost, nonDiscountPrice + minCost);
							}
						}
					}
				}
			}
		}
		
		out.println(overallMinCost);
		out.close();
	}
	
	public static class Offer{
		public int[] amount;
		public int price;
		
		@Override
		public String toString(){
			return (Arrays.toString(amount) + " -> " + price);
		}
	}
}