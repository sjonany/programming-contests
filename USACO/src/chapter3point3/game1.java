package chapter3point3;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: game1
 */
public class game1 {
	static int N;
	static int[][] field;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("game1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("game1.out")));
		
		int N = Integer.parseInt(f.readLine());
		int[] board = new int[N];
		
		String line = f.readLine();
		int boardI = 0;
		while(line != null){
			StringTokenizer tkz = new StringTokenizer(line);
			while(tkz.hasMoreTokens()){
				board[boardI] = Integer.parseInt(tkz.nextToken());
				boardI++;
			}
			if(boardI ==N)break;
			line = f.readLine();
		}
		
		int[] sum = new int[N];
		sum[0] = board[0];
		for(int i=1;i<N;i++){
			sum[i] = sum[i-1] + board[i];
		}
		
		//if move right now, best if board [i->j]
		int[][] dp = new int[N][N];
		
		for(int i=0;i<N;i++){
			dp[i][i] = board[i];
		}
		
		for(int k=2;k<=N;k++){
			for(int i=0;i<N;i++){
				for(int j=i+k-1;j<N;j++){
					int total;
					if(i==0)total = sum[j];
					else total = sum[j]-sum[i-1];
					dp[i][j] = Math.max(board[i]+ (total-board[i]-dp[i+1][j]), 
							board[j] + (total-board[j] -dp[i][j-1]));
				}
			}
		}
		out.println(dp[0][N-1] + " " + (sum[N-1] - dp[0][N-1]));
		out.close();
	}
}