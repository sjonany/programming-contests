package chapter3point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: camelot
 */
public class camelot {
	static int R;
	static int C;
	//king
	static int kr;
	static int kc;
	
	//horseBfs row,col -> r2,c2 in horse moves, min dist
	static int[][][][] horseBfs;
	//totWeight[r][c] = total cost for all horses to come here
	static int[][] totWeight;
	
	//horse moves
	static int[] moveC = new int[]{2,1,-1,-2,-2,-1,1,2};
	static int[] moveR = new int[]{-1,-2,-2,-1,1,2,2,1};
	
	static List<Horse> horses;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("camelot.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("camelot.out")));
		
		String[] toks = f.readLine().split(" ");
		R = Integer.parseInt(toks[0]);
		C = Integer.parseInt(toks[1]);
		
		toks = f.readLine().split(" ");
		kc = (int)(toks[0].charAt(0) -'A');
		kr = Integer.parseInt(toks[1])-1;
		
		horses = new ArrayList<Horse>();
		String line = f.readLine();
		while(line != null && line.trim().length()!=0){
			StringTokenizer tkz = new StringTokenizer(line);
			while(tkz.hasMoreTokens()){
				Horse h = new Horse();
				h.col= (int)(tkz.nextToken().charAt(0)-'A');
				h.row = Integer.parseInt(tkz.nextToken())-1;
				horses.add(h);
			}
			line = f.readLine();
		}
		
		if(horses.size() == 0){
			//move king only
			out.println(0);
			out.close();
			return;
		}
		
		//fill in horseBfs
		horseBfs = new int[R][C][R][C];
		for(int r1=0;r1<R;r1++){
			for(int c1=0;c1<C;c1++){
				for(int r2=0;r2<R;r2++){
					for(int c2=0;c2<C;c2++){
						horseBfs[r1][c1][r2][c2] = -1;
					}
				}
			}
		}
		for(int r=0;r<R; r++){
			for(int c=0;c<C;c++){
				//bfs tree starting from r,c
				Queue<Horse> q = new LinkedList<Horse>();
				Horse source = new Horse();
				source.row = r;
				source.col = c;
				
				horseBfs[r][c][r][c] = 0;
				q.add(source);
				while(!q.isEmpty()){
					Horse h = q.remove();
					for(int move=0;move<moveC.length;move++){
						int newR = h.row + moveR[move];
						int newC = h.col + moveC[move];
						if(newR >= 0 && newR<R && newC >=0 && newC<C && horseBfs[r][c][newR][newC] == -1){
							horseBfs[r][c][newR][newC] = horseBfs[r][c][h.row][h.col]+1;
							horseBfs[newR][newC][r][c] = horseBfs[r][c][h.row][h.col]+1;
							Horse child = new Horse();
							child.row = newR;
							child.col = newC;
							q.add(child);
						}
					}
				}
			}
		}
		
		//init totWeight
		totWeight = new int[R][C];
		for(int r=0;r<R;r++){
			for(int c=0;c<C;c++){
				for(Horse h: horses){
					//exist horse that can't meet
					if(horseBfs[h.row][h.col][r][c] == -1){
						totWeight[r][c] = -1;
						break;
					}
					totWeight[r][c] += horseBfs[h.row][h.col][r][c];
				}
			}
		}
		
		long minCost = Long.MAX_VALUE;
		for(int r=0;r<R;r++){
			for(int c=0;c<C;c++){
				if(totWeight[r][c] != -1){
					for(Horse h: horses){
						//r,c, = final destination
						//vary meeting place to be 5x5 around king
						for(int mr = kr-2; mr<=kr+2;mr++){
							for(int mc = kc-2; mc<=kc+2;mc++){
								if(mr >= 0 && mr<R && mc >=0 && mc<C && horseBfs[mr][mc][r][c]!=-1 && horseBfs[h.row][h.col][r][c]!=-1
										&& horseBfs[h.row][h.col][mr][mc]!=-1){
									int kingCost=  Math.max(Math.abs(kr-mr), Math.abs(kc-mc));
									minCost = Math.min(minCost, totWeight[r][c]-horseBfs[h.row][h.col][r][c]
											+horseBfs[mr][mc][r][c]+horseBfs[h.row][h.col][mr][mc]+kingCost);
								}
							}
						}
					}
				}
			}
		}
		out.println(minCost);
		out.close();
	}

	static class Horse{
		public int row;
		public int col;
	}
}