package chapter3point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: fence
 */
public class fence {
	static Map<Integer, List<Integer>> adjMap = new HashMap<Integer, List<Integer>>();
	static int F;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("fence.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("fence.out")));
		
		F = Integer.parseInt(f.readLine());
		
		int startNode = Integer.MAX_VALUE;
		for(int i=1; i<=F;i++){
			StringTokenizer tkz = new StringTokenizer(f.readLine());
			int from = Integer.parseInt(tkz.nextToken());
			int to = Integer.parseInt(tkz.nextToken());
			
			List<Integer> outEdges = adjMap.get(from);
			if(outEdges == null){
				outEdges = new ArrayList<Integer>();
				adjMap.put(from, outEdges);
			}

			outEdges.add(to);
			
			List<Integer> inEdges = adjMap.get(to);
			if(inEdges == null){
				inEdges = new ArrayList<Integer>();
				adjMap.put(to, inEdges);
			}

			inEdges.add(from);
			
			startNode = Math.min(startNode, from);
		}
		
		for(int node: adjMap.keySet()){
			Collections.sort(adjMap.get(node));
		}
		
		//find node with odd edges
		List<Integer> oddNodes = new ArrayList<Integer>();
		for(int node : adjMap.keySet()){
			if(adjMap.get(node).size() %2 == 1){
				oddNodes.add(node);
			}
		}
		if(oddNodes.size() != 0){
			startNode = oddNodes.get(0);
			if(oddNodes.size() == 2){
				startNode = Math.min(startNode, oddNodes.get(1));
			}
		}
		
		Stack<Integer> eulerCircuit = new Stack<Integer>();
		findEulerCircuit(eulerCircuit, startNode);
		
		while(!eulerCircuit.isEmpty()){
			out.println(eulerCircuit.pop());
		}
		out.close();
	}

	//mutates the graph :D
	public static void findEulerCircuit(Stack<Integer> circuit, int node){
		if(adjMap.get(node).size() == 0){
			circuit.add(node);
		}else{
			List<Integer> neighbors = adjMap.get(node);
			while(neighbors.size() != 0){
				int neighbor = neighbors.get(0);
				//delete edge
				neighbors.remove(0);
				List<Integer> nn = adjMap.get(neighbor);
				nn.remove(nn.indexOf(node));
				findEulerCircuit(circuit, neighbor);
			}
			circuit.add(node);
		}
	}
}