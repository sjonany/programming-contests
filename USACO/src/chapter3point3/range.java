package chapter3point3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: range
 */
public class range {
	static int N;
	static int[][] field;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("range.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("range.out")));
		
		N = Integer.parseInt(f.readLine());
		field = new int[N][N];
		
		for(int r=0;r<N;r++){
			String line = f.readLine();
			for(int i=0;i<line.length();i++){
				field[r][i] = line.charAt(i)-'0';
			}
		}
		
		//how many squares fo size N
		long[] tot = new long[N+1];
		//if top left = r,c, what's the largest square there
		//size 1
		int[][] dp = new int[N][N];
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				if(field[r][c] == 1){
					dp[r][c] = 1;
				}
			}
		}
	
		for(int s =2;s<=N;s++){
			for(int r=0;r<=N-s;r++){
				for(int c=0;c<=N-s;c++){
					if(dp[r][c] >= s-1 && dp[r+1][c+1] >= s-1 &&
							field[r+s-1][c] == 1 && field[r][c+s-1] == 1){
						dp[r][c]= s;
						tot[s]++;
					}
				}
			}
		}
		
		for(int size =2; size<=N;size++){
			if(tot[size] !=0)
			{
				out.println(size + " " + tot[size]);
			}
		}
		out.close();
	}
}