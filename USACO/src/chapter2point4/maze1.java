package chapter2point4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/*
ID: xenoslash
LANG: JAVA
TASK: maze1
 */
public class maze1 {
	static char[][] map;
	//distance[id1][id2] shortest distance between the two vertices
	//note: given that it's a perfect maze
	static int W;
	static int H;
	static int ex1 = -1;
	static int ex2 = -1;
	static int maxDistance = 0;
	static int maxId;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("maze1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("maze1.out")));
		
		String[] toks = f.readLine().split(" ");
		W = Integer.parseInt(toks[0]);
		H = Integer.parseInt(toks[1]);
		
		map = new char[2*H+1][2*W+1];
		for(int row = 0; row < 2*H+1; row++){
			String line = f.readLine();
			for(int i=0;i<line.length();i++){
				map[row][i] = line.charAt(i);
			}
		}
		
		maxId = (H-1) * W +(W-1);
		findExits();
		
/*
		for(int r = 0; r < H; r++){
			for(int c=0; c<W; c++){
				int id = getId(r,c);
				System.out.print("[r=" + r + ",c=" + c+"] ->");
				for(int vi=0;vi<=maxId;vi++){
					if(adjMat[id][vi]){
						System.out.print("[r=" + getR(vi) + ",c=" + getC(vi) + "] ");
					}
				}
				System.out.println();
			}
		}
		*/
		
		//all pair shortest distance using bfs
		int[] d1 = bfs(ex1);
		int[] d2 = bfs(ex2);
		
		for(int i=0;i<maxId;i++){
			maxDistance = Math.max(maxDistance, Math.min(d1[i],d2[i]));
		}
		
		//note: don't forget to increment 1 to account for step to exit
		out.println(maxDistance+1);
		out.close();
	}
	
	public static int[] bfs(int sourceId){
		int[] distance = new int[maxId+1];
		Queue<Integer> idq = new LinkedList<Integer>();
		idq.add(sourceId);
		while(!idq.isEmpty()){
			int id = idq.remove();
			int r = id/W;
			int c = id%W;
			int mapR = 2*r + 1;
			int mapC = 2*c+1;
			List<Integer> outV = new ArrayList<Integer>();
			if(map[mapR][mapC+1] == ' ' && c+1 < W){
				outV.add(r*W+c+1);
			}
			if(map[mapR][mapC-1] == ' ' && c-1 >= 0){
				outV.add(r*W+c-1);
			}
			if(map[mapR+1][mapC] == ' ' && r+1 < H){
				outV.add(W*(r+1)+c);
			}
			if(map[mapR-1][mapC] == ' ' && r-1 >= 0){
				outV.add(W*(r-1)+c);
			}
			

			for(Integer vout : outV){
				if(distance[vout] == 0 && vout != sourceId){
					distance[vout] = distance[id] + 1;
					idq.add(vout);
				}
			}
		}
		return distance;
	}
	
	
	public static void findExits(){
		//horizontal exits
		for(int c = 0; c < W; c++){
			if(map[0][2*c+1] == ' '){
				if(ex1 == -1){
					ex1 = c;
				}else{
					if(ex2 == -1){
						ex2 = c;
					}else{
						System.out.println("more than 2 exits?!!!");
					}
				}
				
			}
			
			if(map[2*H][2*c+1] == ' '){
				if(ex1 == -1){
					ex1 = (H-1) * W + c;
				}else{
					if(ex2 == -1){
						ex2 = (H-1)*W+c;
					}else{
						System.out.println("more than 2 exits?!!!");
					}
				}
			}
		}
		
		//vertical exits
		for(int r = 0; r < H; r++){
			if(map[2*r+1][0] == ' '){
				if(ex1 == -1){
					ex1 = r * W;
				}else{
					if(ex2 == -1){
						ex2 = r * W;
					}else{
						System.out.println("more than 2 exits?!!!");
					}
				}
				
			}
			
			if(map[2*r+1][2*W] == ' '){
				if(ex1 == -1){
					ex1 = r * W + W-1;
				}else{
					if(ex2 == -1){
						ex2 = r * W + W-1;
					}else{
						System.out.println("more than 2 exits?!!!");
					}
				}
			}
		}
	}
}
