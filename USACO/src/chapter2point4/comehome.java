package chapter2point4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

/*
ID: xenoslash
LANG: JAVA
TASK: comehome
 */
public class comehome {
	//first low, then upper chars
	static int N = 52;
	static int[][] adjMap = new int[N][N];
	static int P;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("comehome.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("comehome.out")));
		P = Integer.parseInt(f.readLine());
		
		Set<Integer> cows = new HashSet<Integer>();
		for(int i=0; i<P;i++){
			String[] toks = f.readLine().split(" ");
			char from = toks[0].charAt(0);
			char to = toks[1].charAt(0);
			int dist = Integer.parseInt(toks[2]);
			int fromIndex = Character.isUpperCase(from) ? from - 'A' + 26 : from -'a';
			int toIndex =  Character.isUpperCase(to) ? to - 'A' + 26 : to -'a';
			
			if(Character.isUpperCase(from) && from != 'Z'){
				cows.add(fromIndex);
			}
			if(Character.isUpperCase(to) && to != 'Z'){
				cows.add(toIndex);
			}
			if(adjMap[fromIndex][toIndex] == 0){
				adjMap[fromIndex][toIndex] = dist;
				adjMap[toIndex][fromIndex] = dist;
			}else{
				adjMap[fromIndex][toIndex] = Math.min(adjMap[fromIndex][toIndex], dist);
				adjMap[toIndex][fromIndex] = Math.min(adjMap[toIndex][fromIndex], dist);
			}
		}
		/*
		for(int i=0;i<26;i++){
			System.out.println(Arrays.toString(adjMap[i]));
		}*/
		int[] dists = shortestPath(N-1);
		
		int minDist = N * 10000;
		int minCow = 0;
		//System.out.println(cows);
		for(int cow: cows){
			if(dists[cow] < minDist){
				minCow = cow;
				minDist = dists[cow];
			}
		}
		
		char minCowName = minCow <26 ? (char) ('a' + minCow) : (char)('A' + (minCow-26));
		out.println(minCowName + " " + minDist);
		out.close();
	}
	
	public static int[] shortestPath(int v){
		int[] distance = new int[N];
		boolean[] visited = new boolean[N];
		for(int i=0;i<N;i++){
			distance[i] = Integer.MAX_VALUE;
		}
		
		distance[v] = 0;
		int countVisit = 0;
		
		while(countVisit < N){
			int closestV = 0;
			int minDistance = 10000*N;
			for(int i=0;i<N;i++){
				if(!visited[i] && distance[i] < minDistance){
					minDistance = distance[i];
					closestV = i;
				}
			}
			visited[closestV] = true;
			countVisit++;
			for(int i=0;i<N;i++){
				if(adjMap[closestV][i] != 0){
					if(distance[closestV] + adjMap[closestV][i] < distance[i]){
						distance[i] = distance[closestV] + adjMap[closestV][i];
					}
				}
			}
		}
		
		return distance;
	}
}