package chapter2point4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: ttwo
 */
public class ttwo {
	static int UP =0;
	static int RIGHT =1;
	static int DOWN =2;
	static int LEFT =3;
	
	static int[][] MOVES = new int[][]{
		{-1,0},
		{0,1},
		{1,0},
		{0,-1}
	};
	
	static int fr;
	static int fc;
	static int fd;
	static int cr;
	static int cc;
	static int cd;
	
	//farmer, then cow
	static boolean[][][][][][] visited = new boolean[4][10][10][4][10][10];
	
	static boolean[][] obs = new boolean[10][10];
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("ttwo.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ttwo.out")));
		for(int i=0;i<10;i++){
			String line = f.readLine();
			for(int j=0;j<line.length(); j++){
				char c= line.charAt(j);
				if(c == '*'){
					obs[i][j] = true;
				}else if(c == 'F'){
					fr = i;
					fc = j;
					fd = UP;
				}else if(c == 'C'){
					cr = i;
					cc = j;
					cd = UP;
				}
			}
		}
		
		int time = 0;
		while(true){
			if(fr == cr && fc == cc){
				out.println(time);
				out.close();
				return;
			}
			
			if(visited[fd][fr][fc][cd][cr][cc] || visited[cd][cr][cc][fd][fr][fc]){
				out.println(0);
				out.close();
				return;
			}
			visited[fd][fr][fc][cd][cr][cc] = true; 
			moveFarmer();
			moveCow();
			time++;
		}
		
	}
	
	public static void moveFarmer(){
		int nextR = fr + MOVES[fd][0];
		int nextC = fc + MOVES[fd][1];
		if(nextR < 0 || nextR > 9 || nextC <0 || nextC > 9 || obs[nextR][nextC]){
			fd = (fd+1)%4;
		}else{
			fr = nextR;
			fc = nextC;
		}
	}
	
	public static void moveCow(){
		int nextR = cr + MOVES[cd][0];
		int nextC = cc + MOVES[cd][1];
		if(nextR < 0 || nextR > 9 || nextC <0 || nextC > 9 || obs[nextR][nextC]){
			cd = (cd+1)%4;
		}else{
			cr = nextR;
			cc = nextC;
		}
	}
}