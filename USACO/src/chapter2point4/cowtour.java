package chapter2point4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/*
ID: xenoslash
LANG: JAVA
TASK: cowtour
 */
public class cowtour {
	//key = comp id, 0 based, val = node id's in that comp
	static List<List<Integer>> comps = new ArrayList<List<Integer>>();
	//key = vertex id, val = is in a comp?
	static boolean [] isComp;
	//stores weights
	static double[][] adjMap;
	//pasPos[id][0 for X, 1 for Y]
	static int[][] pasPos;
	static double[][] dist;
	static int N;
	//key= vi, value = distance of furthest vertex in its componenent
	static double[] furthest;
	//key = compid, value = diameter of comp
	static double[] diam;
	static double minConnDiam = Double.MAX_VALUE;
	
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("cowtour.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("cowtour.out")));
		
		
		N = Integer.parseInt(f.readLine());
		
		pasPos = new int[N][2];
		for(int i=0;i<N;i++){
			String[] toks = f.readLine().split(" ");
			pasPos[i][0] = Integer.parseInt(toks[0]);
			pasPos[i][1] = Integer.parseInt(toks[1]);
		}
		
		//create graph
		adjMap = new double[N][N];
		for(int i=0;i<N;i++){
			String line = f.readLine();
			for(int j=0;j<N;j++){
				if(line.charAt(j) == '1' || i ==j){
					int dx = pasPos[i][0] - pasPos[j][0];
					int dy = pasPos[i][1] - pasPos[j][1];
					adjMap[i][j] = Math.sqrt(dx*dx + dy*dy);
				}else{
					adjMap[i][j] = -1;
				}
			}
		}
	
		isComp = new boolean[N];
		findConnectedComponents();
		
		/*for(int compid =0;compid<comps.size();compid++){
			System.out.println(compid + ": " + comps.get(compid));
		}*/
		
		findShortestDistance();
		/*
		for(int i=0;i<N;i++){
			System.out.println(Arrays.toString(adjMap[i]));
		}*/
		
		int numComp = comps.size();
		diam = new double[numComp];
		furthest = new double[N];
		for(int compid =0; compid<comps.size();compid++){
			List<Integer> comp = comps.get(compid);
			for(int i=0; i<comp.size(); i++){
				for(int j=0;j<comp.size();j++){
					int vi = comp.get(i);
					int vj = comp.get(j);
					diam[compid] = Math.max(adjMap[vi][vj], diam[compid]);
					furthest[vi] = Math.max(adjMap[vi][vj], furthest[vi]);
					furthest[vj] = Math.max(adjMap[vi][vj], furthest[vj]);
				}
			}
		}
		/*
		System.out.println(Arrays.toString(furthest));
		System.out.println(Arrays.toString(diam));*/
		
		//consider all pairs of comps
		for(int ci1 = 0; ci1<numComp; ci1++){
			for(int ci2=ci1+1;ci2<numComp;ci2++){
				//find diameter of connected component
				//either individual, or cross
				double comDiam = Math.max(diam[ci1], diam[ci2]);
				double acrossDiam = Double.MAX_VALUE;
				List<Integer> comp1 = comps.get(ci1);
				List<Integer> comp2 = comps.get(ci2);
				for(int i=0; i<comp1.size();i++){
					for(int j=0;j<comp2.size();j++){
						int vi = comp1.get(i);
						int vj = comp2.get(j);
						int dx = pasPos[vi][0] - pasPos[vj][0];
						int dy = pasPos[vi][1] - pasPos[vj][1];
						double d = Math.sqrt(dx*dx + dy*dy);
						
						acrossDiam = Math.min(acrossDiam, 
								furthest[vi] + furthest[vj] + d);
					}
				}
				minConnDiam = Math.min(minConnDiam, Math.max(comDiam,acrossDiam));
			}
		}
		
		out.println(String.format("%.6f", minConnDiam));
		out.close();
	}
	
	public static void findShortestDistance(){
		//we don't need adjMap anymore, so just mutate it
		/*
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				dist[i][j] = adjMap[i][j];
			}
		}*/
		
		for(int k=0;k<N;k++){
			for(int i=0;i<N;i++){
				for(int j=0;j<N;j++){
					if(adjMap[i][k] != -1 && adjMap[k][j] != -1){
						if(adjMap[i][k] + adjMap[k][j] < adjMap[i][j] || adjMap[i][j] == -1){
							adjMap[i][j] = adjMap[i][k] + adjMap[k][j];
						}
					}
				}
			}
		}
	}
	
	public static void findConnectedComponents(){
		for(int vi=0; vi<N; vi++){
			if(!isComp[vi]){
				List<Integer> comp = new ArrayList<Integer>();
				dfs(vi, comp);
				comps.add(comp);
			}
		}
	}
	
	public static void dfs(int vi, List<Integer> comp){
		comp.add(vi);
		isComp[vi] = true;
		for(int vout =0; vout<N; vout++){
			if(adjMap[vi][vout] >=0 && !isComp[vout]){
				dfs(vout, comp);
			}
		}
	}
}