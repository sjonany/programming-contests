package chapter2point4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: fracdec
 */
public class fracdec {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("fracdec.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("fracdec.out")));
		
		String[] toks = f.readLine().split(" ");
		int N = Integer.parseInt(toks[0]);
		int D = Integer.parseInt(toks[1]);
		
		StringBuffer result = new StringBuffer();
		int lead = N/D;
		result.append(lead);
		result.append(".");
		
		StringBuffer trail = new StringBuffer();
		int rem = N % D;
		int[] index = new int[(D+1) * 10];
		int curIndex = 1;
		while(rem != 0){
			if(index[rem] != 0){
				int lastOcc = index[rem];
				result.append(trail.substring(0, lastOcc-1) + "(" + trail.substring(lastOcc-1) +")");
				print(result, out);
				return;
			}
			index[rem] = curIndex;
			rem *= 10;
			trail.append(rem / D);
			rem %= D;
			curIndex++;
		}
		if(trail.length() == 0){
			trail.append("0");
		}
		result.append(trail);
		print(result, out);
		out.close();
	}	
	
	public static void print(StringBuffer result, PrintWriter out){
		int numPrint = 0;
		for(int i=0; i<result.length();i++){
			out.print(result.charAt(i));
			numPrint++;
			if(numPrint%76 == 0){
				out.println();
			}
		}
		
		if(numPrint %76 != 0){
			out.println();
		}
		out.close();
	}
}