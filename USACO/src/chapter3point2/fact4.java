package chapter3point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
ID: xenoslash
LANG: JAVA
TASK: fact4
 */
public class fact4 {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("fact4.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("fact4.out")));
		
		int N = Integer.parseInt(f.readLine());
		int pow2 = 0;
		int pow5 = 0;
		int lastDig = 1;
		
		for(int i=1; i<=N;i++){
			int num = i;
			while(num%2 == 0){
				pow2++;
				num/=2;
			}
			while(num%5 == 0){
				pow5++;
				num/=5;
			}
			
			lastDig *= num;
			lastDig %= 10;
		}
		
		for(int i=1; i<=pow2-pow5;i++){
			lastDig *=2;
			lastDig %= 10;
		}
		
		out.println(lastDig);
		out.close();
	}
}