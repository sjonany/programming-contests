package chapter3point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: ratios
 */
public class ratios {
	static int[] goal = new int[3];
	static int[][] mixtures = new int[3][3];
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("ratios.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ratios.out")));
		
		StringTokenizer tkz =  new StringTokenizer(f.readLine());
		goal[0] = Integer.parseInt(tkz.nextToken());
		goal[1] = Integer.parseInt(tkz.nextToken());
		goal[2] = Integer.parseInt(tkz.nextToken());
		
		for(int i=0;i<3;i++){
			tkz =  new StringTokenizer(f.readLine());
			mixtures[i][0] = Integer.parseInt(tkz.nextToken());
			mixtures[i][1] = Integer.parseInt(tkz.nextToken());
			mixtures[i][2] = Integer.parseInt(tkz.nextToken());
		}
		
		//sum of feeds if i,j,k of mixtures are used
		int[] sum = new int[3];
		for(int i=0;i<=100;i++){
			for(int j=0;j<=100;j++){
				for(int k=0;k<=100;k++){
					//boolean debug = i==0 && j==38 && k==7;
					//if(debug)System.out.println(i + "," + j +"," +k + " -> " + sum[0]+","+sum[1]+","+sum[2]);
					int rat1=-1;
					int rat2=-1;
					int rat3=-1;
					boolean isValid = true;
					if(goal[0] == 0){
						if(sum[0] == 0){
							rat1 = 0;
						}else{
							isValid = false;
						}
					}else{
						if(sum[0] == 0){
							isValid = false;
						}else{
							if(sum[0] % goal[0] == 0){
								rat1 = sum[0]/goal[0];
							}else{
								isValid = false;
							}
						}
					}
					
					if(isValid)
					if(goal[1] == 0){
						if(sum[1] == 0){
							rat2 = 0;
						}else{
							isValid = false;
						}
					}else{
						if(sum[1] == 0){
							isValid = false;
						}else{
							if(sum[1] % goal[1] == 0){
								rat2 = sum[1]/goal[1];
							}else{
								isValid = false;
							}
						}
					}
					
					if(isValid)
					if(goal[2] == 0){
						if(sum[2] == 0){
							rat3 = 0;
						}else{
							isValid = false;
						}
					}else{
						if(sum[2] == 0){
							isValid = false;
						}else{
							if(sum[2] % goal[2] == 0){
								rat3 = sum[2]/goal[2];
							}else{
								isValid = false;
							}
						}
					}
					
					if(isValid){
						if(sum[0]==rat1*goal[0] && sum[1]==rat1*goal[1] && sum[2]==rat1*goal[2]){
							out.println(i + " " + j + " " + k + " " + rat1);
							out.close();
							return;
						}else if(sum[0]==rat2*goal[0] && sum[1]==rat2*goal[1] && sum[2]==rat2*goal[2]){
							out.println(i + " " + j + " " + k + " " + rat2);
							out.close();
							return;
						}else if(sum[0]==rat3*goal[0] && sum[1]==rat3*goal[1] && sum[2]==rat3*goal[2]){
							out.println(i + " " + j + " " + k + " " + rat3);
							out.close();
							return;
						}
					}
					for(int ty=0;ty<3;ty++){
						sum[ty]+=mixtures[2][ty];
					}
				}
				for(int ty=0;ty<3;ty++){
					sum[ty]+=mixtures[1][ty];
					sum[ty]-=101*mixtures[2][ty];
				}
			}
			for(int ty=0;ty<3;ty++){
				sum[ty]+=mixtures[0][ty];
				sum[ty]-=101*mixtures[1][ty];
			}
		}
		
		out.println("NONE");
		out.close();
	}
}