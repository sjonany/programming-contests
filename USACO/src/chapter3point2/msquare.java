package chapter3point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: msquare
 */
public class msquare {
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("msquare.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("msquare.out")));
		
		String goal = "";
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		for(int i=0;i<8;i++){
			goal += tkz.nextToken();
		}
		
		String initPerm = "12345678";
		if(initPerm.equals(goal)){
			out.println(0);
			out.println("");
			out.close();
			return;
		}
		//key = state, val = moves
		Map<String, String> stateMap = new HashMap<String, String>();
		Queue<String> q = new LinkedList<String>();
		
		q.add(initPerm);
		stateMap.put(initPerm, "");
		while(!q.isEmpty()){
			String curState = q.remove();
			String prevTrail = stateMap.get(curState);
			
			//A
			String nextState = moveA(curState);
			if(stateMap.get(nextState) == null){
				if(nextState.equals(goal)){
					out.println(prevTrail.length()+1);
					out.println(prevTrail + "A");
					out.close();
					return;
				}
				stateMap.put(nextState, prevTrail + "A");
				q.add(nextState);
			}
			
			//B
			nextState = moveB(curState);
			if(stateMap.get(nextState) == null){
				if(nextState.equals(goal)){
					out.println(prevTrail.length()+1);
					out.println(prevTrail +"B");
					out.close();
					return;
				}
				stateMap.put(nextState, prevTrail + "B");
				q.add(nextState);
			}
			
			//C
			nextState = moveC(curState);
			if(stateMap.get(nextState) == null){
				if(nextState.equals(goal)){
					out.println(prevTrail.length()+1);
					out.println(prevTrail + "C");
					out.close();
					return;
				}
				stateMap.put(nextState, prevTrail + "C");
				q.add(nextState);
			}
		}
		out.println("fail");
		out.close();
	}
	
	
	public static String moveA(String permS){
		char[] perm = permS.toCharArray();
		for(int i=0;i<4;i++){
			char temp = perm[i];
			perm[i] = perm[7-i];
			perm[7-i] = temp;
		}
		
		return String.valueOf(perm);
	}
	
	public static String moveB(String permS){
		char[] perm = permS.toCharArray();
		char right = perm[3];
		for(int i=2;i>=0;i--){
			perm[i+1] = perm[i];
		}
		perm[0] = right;

		char left = perm[4];
		for(int i=4;i<7;i++){
			perm[i] = perm[i+1];
		}
		perm[7] = left;
		return String.valueOf(perm);
	}

	static int[] rotateOrder = new int[]{1,2,5,6};
	public static String moveC(String permS){
		char[] perm = permS.toCharArray();
		char right = perm[rotateOrder[3]];
		for(int i=2;i>=0;i--){
			perm[rotateOrder[i+1]] = perm[rotateOrder[i]];
		}
		perm[rotateOrder[0]] = right;
		return String.valueOf(perm);

	}
	
	public static void printFormat(int[] perm){
		for(int i=0;i<4;i++){
			System.out.print(perm[i] + " ");
		}
		System.out.println();
		for(int i=7;i>=4;i--){
			System.out.print(perm[i] + " ");
		}
		System.out.println();
		System.out.println();
	}
}