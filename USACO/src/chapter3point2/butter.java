
package chapter3point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: butter
 */
public class butter {
	static Map<Integer, List<Edge>> adjMap = new HashMap<Integer, List<Edge>>();
	static List<Integer> cowPastures = new ArrayList<Integer>();
	static int N;
	static int P;
	static int C;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("butter.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("butter.out")));
		
		StringTokenizer tkz =  new StringTokenizer(f.readLine());
		N = Integer.parseInt(tkz.nextToken());
		P = Integer.parseInt(tkz.nextToken());
		C = Integer.parseInt(tkz.nextToken());
		
		for(int i=1;i<=N;i++){
			cowPastures.add(Integer.parseInt(f.readLine()));
		}
		
		for(int i=1; i<=C;i++){
			tkz = new StringTokenizer(f.readLine());
			int from = Integer.parseInt(tkz.nextToken());
			int to = Integer.parseInt(tkz.nextToken());
			int weight = Integer.parseInt(tkz.nextToken());
			
			List<Edge> outEdges = adjMap.get(from);
			if(outEdges == null){
				outEdges = new ArrayList<Edge>();
				adjMap.put(from, outEdges);
			}

			outEdges.add(new Edge(to, weight));
			
			List<Edge> inEdges = adjMap.get(to);
			if(inEdges == null){
				inEdges = new ArrayList<Edge>();
				adjMap.put(to, inEdges);
			}

			inEdges.add(new Edge(from, weight));
		}
		
		int minTotalWeight=  Integer.MAX_VALUE;
		
		for(int p=1;p<=P;p++){
			minTotalWeight=  Math.min(minTotalWeight, getTotalWeight(p));
		}
		
		out.println(minTotalWeight);
		out.close();
	}
	
	public static int getTotalWeight(int source){
		int[] distance = new int[P+1];
		boolean[] visited = new boolean[P+1];
		for(int i=0;i<P+1;i++){
			distance[i] = Integer.MAX_VALUE;
		}
		
		distance[source] = 0;
		//hack. actually node
		PriorityQueue<Node> nextNode = new PriorityQueue<Node>(800, new Comparator<Node>(){
			@Override
			public int compare(Node o1, Node o2) {
				return o1.weight - o2.weight;
			}
		});
		
		nextNode.add(new Node(source, 0));
		while(!nextNode.isEmpty()){
			Node node = nextNode.remove();
			if(node.weight > distance[node.id]){
				//java doesn't have decrease key operator :(
				//so i put multiple same objects with differing keys
				continue;
			}
			
			int closestV = node.id;
			visited[closestV] = true;
			
			List<Edge> outEdges = adjMap.get(closestV);
			if(outEdges == null){
				continue;
			}
			for(Edge outE: outEdges){
				if(!visited[outE.dest] && distance[closestV] + outE.weight < distance[outE.dest]){
					distance[outE.dest] = distance[closestV] + outE.weight;
					nextNode.add(new Node(outE.dest, distance[closestV] + outE.weight));
				}
			}
		}
		
		int sum = 0;
		for(int cowPast : cowPastures){
			sum += distance[cowPast]; 
		}
		
		return sum;
	}
	public static class Node{
		public int id;
		public int weight;
		public Node(int id, int we){
			this.id = id;
			weight= we;
		}
	}
	
	public static class Edge{
		public int dest;
		public int weight;
		
		public Edge(int d, int w){
			dest = d;
			weight= w;
		}
	}
}