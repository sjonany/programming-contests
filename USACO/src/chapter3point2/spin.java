package chapter3point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: spin
 */
public class spin {
	static int[] speed;
	static List<Wedge>[] wedges;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("spin.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("spin.out")));
		
		wedges = (ArrayList<Wedge>[])new ArrayList[5];
		speed = new int[5];
		for(int i=0;i<5;i++){
			StringTokenizer tkz =  new StringTokenizer(f.readLine());
			speed[i] = Integer.parseInt(tkz.nextToken());
			int numWedge = Integer.parseInt(tkz.nextToken());
			wedges[i] = new ArrayList<Wedge>();
			for(int j=0;j<numWedge;j++){
				wedges[i].add(new Wedge(Integer.parseInt(tkz.nextToken()), Integer.parseInt(tkz.nextToken())));
			}
		}
		
		int MAX_ITER = 1000;
		for(int iter = 0; iter <= MAX_ITER; iter++){
			
			//check if exist hole
			int[] countHole = new int[360];
			for(int i=0;i<5;i++){
				for(Wedge w: wedges[i]){
					for(int j=0; j<w.length; j++){
						int ind = (j+w.start) % 360;
						countHole[ind]++;
						if(countHole[ind] >= 5){
							out.println(iter);
							out.close();
							return;
						}
					}
				}
			}
			
			//rotate wedges
			for(int i=0; i<5;i++){
				for(Wedge w : wedges[i]){
					w.start = (w.start + speed[i])%360;
				}
			}
		}
		out.println("none");
		out.close();
	}
	
	
	public static class Wedge{
		public int start;
		public int length;
		
		public Wedge(int s, int l){
			this.start = s;
			this.length = l+1;
		}
		
		@Override
		public String toString(){
			return start + "-" + (start+length-1);
		}
	}
}