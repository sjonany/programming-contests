package chapter3point2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: xenoslash
LANG: JAVA
TASK: kimbits
 */
public class kimbits {
	static int N;
	static int L;
	static long I;
	public static void main(String[] args) throws Exception{
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("kimbits.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("kimbits.out")));
		
		StringTokenizer tkz =  new StringTokenizer(f.readLine());
		N = Integer.parseInt(tkz.nextToken());
		L = Integer.parseInt(tkz.nextToken());
		I = Long.parseLong(tkz.nextToken());
		
		//dp[i][j] = how many strings of length exactly i bits, with less than or equal to j ones
		long[][] dp = new long[32][32];
		
		dp[0][0]=1;
		dp[1][0] = 1;
		for(int j=1;j<=31;j++){
			dp[0][j] = 1;
			dp[1][j] = 2;
		}
		
		for(int i=2; i<=31;i++){
			dp[i][0] = 1;
			for(int j=1;j<=31;j++){
				//first char is 1, or 0
				dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
			}
		}
		/*for(int i=0;i<=31;i++){
			System.out.println(Arrays.toString(dp[i]));
		}*/
		//how many numbers am I greater against
		long curRank = 0;
		//my bits, starting from left most
		int[] bits = new int[N];
		//how many ones have i used up
		int numOnes = 0;
		for(int i=0;i<N;i++){
			//if current bit is 1, 
			if(numOnes < L){
				long newRank = curRank + dp[N-i-1][L-numOnes];
				if(newRank < I){
					//System.out.println("i = " + i);
					//System.out.println("length = " + (N-i-1) + ", numOnes <= " + (L-numOnes) + " = " + dp[N-i-1][L-numOnes]);
					numOnes++;
					curRank = newRank;
					//System.out.println("curRank = " + curRank);
					bits[i] = 1;
					//System.out.println(Arrays.toString(bits));
					continue;
				}
			}
			
			//cur bit is 0
		}
		
		//System.out.println(curRank);
		//System.out.println(curRank);
		StringBuilder s = new StringBuilder();
		for(int i=0;i<bits.length;i++){
			s.append(bits[i]);
		}
		out.println(s);
		out.close();
	}
}