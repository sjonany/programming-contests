package LightOJ;

import java.io.IOException;
import java.io.InputStream;
import java.util.InputMismatchException;

public class Investigation1068 {
	static int K_MAX = 90;
	static int L_MAX = 10;
	static int dpAllPrev[][] = new int[K_MAX][K_MAX];
	static int dpAllCur[][] = new int[K_MAX][K_MAX];
	static int dpPrev[][] = new int[K_MAX][K_MAX];
	static int dpCur[][] = new int[K_MAX][K_MAX];
	static int summary[] = new int[L_MAX+1];
	static int sumDigArr[] = new int [L_MAX+1];
	static int totValArr[] = new int[L_MAX+1];
	public static void main(String[] args) throws Exception{
		InputReader reader = new InputReader(System.in);
		int T = reader.readInt();
		for(int t=1;t<=T;t++){
	
			//we want [A ->B], A included
			int A = reader.readInt();
			int B = reader.readInt();
			int K = reader.readInt();
			if(K > K_MAX){
				System.out.println("Case " + t + ": " +  0);
				continue;
			}
			
			int length = (int)Math.log10(B) + 1;
			//dp[l][i][j] = count integers with exactly l digits, sum digits % K = i, number % K = i
			clear(dpAllPrev);
			clear(dpAllCur);
			
			//l=1
			for(int i=1;i<=9;i++){
				dpAllPrev[i%K][i%K]++;
			}
			//at this length, what's dp[l][0][0]
			clear(summary);
			summary[1] = dpAllPrev[0][0];
			for(int l=1;l<=length-1;l++){
				for(int sumDig = 0; sumDig < K; sumDig++){
					for(int val = 0; val < K; val++){
						for(int nextDig = 0; nextDig <= 9; nextDig++){
							int nextVal = (val * 10%K + nextDig%K) %K;
							dpAllCur[(sumDig+nextDig) %K][nextVal]+= dpAllPrev[sumDig][val];
						}
					}
				}
				
				for(int i=0;i<K;i++){
					for(int j=0;j<K;j++){
						dpAllPrev[i][j] = dpAllCur[i][j];
					}
				}
				summary[l+1] = dpAllPrev[0][0];
				clear(dpAllCur); 
			}
			System.out.println("Case " + t + ": " +  (count(B, summary, K) - count(A-1, summary, K)));
		}
	}
	
	//return number of satisfying dpAll <= upper
	static int count(int upper, int[] summary, int K){
		if(upper == 0){
			return 0;
		}
		String up = "" + upper;
		//same description, but digits  <= prefix of length l of A and B
		//0 ->   < prefix
		//1 ->   = prefix exactly all digits
		clear(dpPrev);
		clear(dpCur);
		clear(sumDigArr);
		clear(totValArr);
		
		//sumDigArr[i] = sum digits of the first i chars of upper, mod K
		//totVal si same, but the value of the prefix mod K
		int digSum = 0;
		int valSum = 0;
		for(int l=1;l<=up.length();l++){	
			int curDig = up.charAt(l-1)-'0';
			digSum += curDig;
			valSum *= 10;
			valSum += curDig;
			sumDigArr[l] = digSum % K;
			totValArr[l] = valSum % K;
		}
		
		for(int i=1;i<(up.charAt(0)-'0');i++){
			dpPrev[i%K][i%K]++;
		}
		
		
		for(int l=1;l < up.length();l++){
			int curDig = (up.charAt(l)-'0');
			for(int sumDig = 0; sumDig < K; sumDig++){
				for(int val = 0; val < K; val++){
					for(int nextDig = 0; nextDig <= 9; nextDig++){
						int nextVal = (val * 10%K + nextDig%K) %K;
						int nextSum = (sumDig+nextDig) %K;
						dpCur[nextSum][nextVal] += dpPrev[sumDig][val];
						
						//dp...[1] case
						if(nextDig < curDig && sumDigArr[l] == sumDig && totValArr[l] == val){
							dpCur[nextSum][nextVal] ++;
						}
					}
				}
			}
			for(int i=0;i<K;i++){
				for(int j=0;j<K;j++){
						dpPrev[i][j] = dpCur[i][j];
				}
			}
			clear(dpCur);
		}
		
		int total = 0;
		for(int l=1;l<up.length();l++){
			total += summary[l];
		}
		total += dpPrev[0][0];
		if(sumDigArr[up.length()] == 0 && totValArr[up.length()] == 0){
			total++;
		}
		return total;
	}
	
	static void clear(int[] dp){
		for(int i=0;i<dp.length;i++){
			dp[i] = 0;
		}
	}
	static void clear(int[][] dp){
		for(int i=0;i<dp.length;i++){
			for(int j=0;j<dp[0].length;j++){
				dp[i][j] = 0;
			}
		}
	}
	
	static class InputReader {

	    private InputStream stream;
	    private byte[] buf = new byte[1024];
	    private int curChar;
	    private int numChars;

	    public InputReader(InputStream stream) {
	        this.stream = stream;
	    }

	    public int read() {
	        
	    	if (numChars == -1)
	            throw new UnknownError();
	        if (curChar >= numChars) {
	            curChar = 0;
	            try {
	                numChars = stream.read(buf);
	            } catch (IOException e) {
	                throw new UnknownError();
	            }
	            if (numChars <= 0)
	                return -1;
	        }
	        return buf[curChar++];
	    }

	    public int readInt() {
	        int c = read();
	        while (isSpaceChar(c))
	            c = read();
	        int sgn = 1;
	        if (c == '-') {
	            sgn = -1;
	            c = read();
	        } else if (c == '+') {
	            c = read();
	        }
	        int res = 0;
	        do {
	            if (c < '0' || c > '9')
	                throw new InputMismatchException();
	            res *= 10;
	            res += c - '0';
	            c = read();
	        } while (!isSpaceChar(c));
	        return res * sgn;
	    }

	    public long readLong() {
	        int c = read();
	        while (isSpaceChar(c))
	            c = read();
	        int sgn = 1;
	        if (c == '-') {
	            sgn = -1;
	            c = read();
	        }
	        long res = 0;
	        do {
	            if (c < '0' || c > '9')
	                throw new InputMismatchException();
	            res *= 10;
	            res += c - '0';
	            c = read();
	        } while (!isSpaceChar(c));
	        return res * sgn;
	    }

	    public static boolean isSpaceChar(int c) {
	        return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
	    }

	}
}
