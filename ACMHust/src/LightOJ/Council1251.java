package LightOJ;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class Council1251 {
	static int MAXN= 8000;
	static int[] res = new int[MAXN];
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		initTwoSat();
		int T = in.nextInt();
		for(int t=0;t<T;t++) {
			int m = in.nextInt();
			int n = in.nextInt();
			TwoSat sat = new TwoSat(n);
			for(int i=0;i<m;i++){
				int v1 = in.nextInt();
				int v2 = in.nextInt();
				sat.add(v1>0, Math.abs(v1)-1, v2>0, Math.abs(v2)-1);
			}
			sat.solve();
			if(isAns) {
				out.printf("Case %d: Yes\n", t+1);
				int ri = 0;
				
				for(int i=0;i<n;i++) {
					if(ans[i]) {
						res[ri++] = i;
					}
				}
				out.print(ri);
				for(int i=0;i<ri;i++){
					out.print(" " + (res[i] + 1));
				}
				out.println();
			} else {
				out.printf("Case %d: No\n", t+1);	
			}
		}
		
		out.close();
	}
	
	static void initTwoSat() {
		revedges = new ArrayList[2*MAXN];
		edges = new ArrayList[2*MAXN];
		comp = new int[2*MAXN];
		stack = new int[2*MAXN];
		visit = new boolean[2*MAXN];
		compColor = new int[2*MAXN];
		ans = new boolean[MAXN];
		comps = new ArrayList[2*MAXN];
		for(int i=0;i<2*MAXN;i++)comps[i] = new ArrayList();
		for(int i=0;i<2*MAXN;i++) {edges[i] = new ArrayList();revedges[i]= new ArrayList();}
	}
	
	static void resetTwoSat(int n) {
		for(int i=0;i<2*n;i++) {
			revedges[i].clear();
			edges[i].clear();
			comps[i].clear();
			compColor[i] = 0;
			visit[i] = false;
		}
		for(int i=0;i<n;i++) {
			ans[i]= false;
		}

		si = 0;
		compi = 0;
	}
	
	static boolean[] ans;
	static boolean isAns;
	static List<Integer>[] edges;
	static List<Integer>[] revedges;
	static int[] comp;
	static int[] stack;
	static int si;
	static int compi;
	static boolean[] visit;
	static int[] compColor;
	static List<Integer>[] comps;
	
	static class TwoSat {
		int n;
		public TwoSat(int n) {
			this.n = n;
			resetTwoSat(n);
		}
		
		// i1 or i2, if ~i1, pos1 = false
		public void add(boolean pos1, int i1, boolean pos2, int i2) {
			edges[node(!pos1, i1)].add(node(pos2, i2));
			edges[node(!pos2, i2)].add(node(pos1, i1));
		}
		
		//return satisifying assignment, isAns = false if there isnt
		public void solve() {
			for(int i=0;i<2*n;i++){for(int j:edges[i]) revedges[j].add(i);}
			scc(2*n);
			for(int i=0;i<n;i++) {
				if(comp[i*2] == comp[i*2+1]){
					isAns = false;
					return;
				}
			}
			for(int i=0;i<2*n;i++){
				comps[comp[i]].add(i);
			}
			
			for(int ci=0;ci<compi;ci++) {
				if(compColor[ci] == 0){
					compColor[ci] = -1;
					for(int vi : comps[ci]){
						ans[vi/2] = false;
						if((vi & 1) == 1) ans[vi/2] = !ans[vi/2];
						compColor[comp[vi^1]] = 1;
					}
				} else {
					for(int vi : comps[ci]){
						ans[vi/2] = compColor[ci] == 1;
						if((vi & 1) == 1) ans[vi/2] = !ans[vi/2];
						compColor[comp[vi^1]] =  -compColor[ci];
					}
				}
			}
			isAns = true;
		}
		
		static int node(boolean pos, int i) {
			return pos ? i*2 : i*2+1;
		}
	}
	
	static void scc(int n) {
		for(int i=0;i<n;i++) {
			if(!visit[i])
				dfs(i);
			if(si == n) break;
		}
		
		si--;
		while(si >= 0) {
			//reuse visit[si] = is on stack
			if(visit[stack[si]]) {
				dfs2(stack[si]);
				compi++;
			}
			si--;
		}
	}
	
	static void dfs(int cur) {
		visit[cur] = true;
		for(int vo : edges[cur]) {
			if(!visit[vo]) dfs(vo);
		}
		stack[si++] = cur;
	}
	
	static void dfs2(int cur) {
		visit[cur] = false;
		comp[cur] = compi;
		for(int vo : revedges[cur]) {
			if(visit[vo]) dfs2(vo);
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			} else if(c == '+') {
				sgn = 1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
