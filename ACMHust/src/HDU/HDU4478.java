package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class HDU4478 {
	static int ODD = 1;
	static int EVEN = 0;
	static int[] dir = {1,0,-1};
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int TEST = Integer.parseInt(in.readLine());
		for(int test = 1; test <= TEST; test++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int N = Integer.parseInt(tk.nextToken());
			int T = Integer.parseInt(tk.nextToken());
			int R = Integer.parseInt(tk.nextToken())-1;
			int C = Integer.parseInt(tk.nextToken())-1;
			boolean[][] board = new boolean[N][N];
			for(int r = 0; r < N; r++){
				String line = in.readLine();
				for(int c = 0; c < N; c++){
					if(line.charAt(c) == '.'){
						board[r][c] = true;
					}else{
						board[r][c] = false;
					}
				}	
			}
			
			boolean isTrapped = true;
			for(int rDir : dir){
				for(int cDir: dir){
					if(rDir == 0 && cDir == 0){
						continue;
					}
					int nr = R+rDir;
					int nc = C+cDir;
					if(nr < 0 || nr >= N || nc < 0 || nc >= N){
						continue;
					}
					if(board[nr][nc]){
						isTrapped =false;
						break;
					}
				}
			}
			
			if(isTrapped){
				out.println(1);
				continue;
			}
			
			int[][][] minStep = new int[2][N][N];
			for(int r = 0; r < N ; r ++){
				for(int c = 0; c < N ; c ++){
					minStep[ODD][r][c] = -1;
					minStep[EVEN][r][c] = -1;
				}
			}
			minStep[EVEN][R][C] = 0;
			boolean[][][] visit = new boolean[2][N][N];
			visit[EVEN][R][C] = true;
			Queue<Node> q = new LinkedList<Node>();
			q.add(new Node(EVEN, R, C, 0));
			
			while(!q.isEmpty()){
				Node n = q.poll();
				int nIsOdd = n.isOdd ^ 1;
				for(int rDir : dir){
					for(int cDir: dir){
						if(rDir == 0 && cDir == 0){
							continue;
						}
						int nr = n.r+rDir;
						int nc = n.c+cDir;
						if(nr < 0 || nr >= N || nc < 0 || nc >= N || !board[nr][nc] || visit[nIsOdd][nr][nc]){
							continue;
						}
						visit[nIsOdd][nr][nc] = true;
						minStep[nIsOdd][nr][nc] = n.step+1;
						q.add(new Node(nIsOdd, nr,nc, n.step+1));
					}
				}
			}
			
			int count = 0;
			for(int r = 0; r < N ; r ++){
				for(int c = 0; c < N ; c ++){
					boolean good = false;
					for(int parity = 0; parity < 2; parity++){
						if(minStep[parity][r][c] != -1 && minStep[parity][r][c] <= T && (T - minStep[parity][r][c]) % 2 == 0){
							good = true;
							break;
						}
					}
					
					if(good) count++;
				}
			}
			
			out.println(count);
		}
		
		out.close();
	}
	
	static class Node{
		public int r;
		public int c;
		public int isOdd;
		public int step;
		public Node(int isOdd, int r, int c, int step) {
			this.r = r;
			this.c = c;
			this.isOdd = isOdd;
			this.step = step;
		}
		
	}
	
}
