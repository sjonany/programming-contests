package HDU;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class HDU4491 {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int P = Integer.parseInt(in.readLine());
		for (int dataset = 1; dataset <= P; dataset++) {
			StringTokenizer tk = new StringTokenizer(in.readLine());
			tk.nextToken();
			int numPoints = Integer.parseInt(tk.nextToken());
			int numOutput = Integer.parseInt(tk.nextToken());
			int prevPivot = -1;
			int curPivot = Integer.parseInt(tk.nextToken());
			//ccw offset from x axis
			double curAngle = Math.toRadians(Double.parseDouble(tk.nextToken()));
			
			List<Integer> pivots = new ArrayList<Integer>();
			Point2D.Double[] points = new Point2D.Double[numPoints+1];
			for(int i = 0 ;i < numPoints; i++){
				tk = new StringTokenizer(in.readLine());
				int pointIndex = Integer.parseInt(tk.nextToken());
				points[pointIndex] = new Point2D.Double(Double.parseDouble(tk.nextToken()),
						Double.parseDouble(tk.nextToken()));
			}
			
			for(int i = 0; i < numOutput; i++){
				Line2D curLine = prevPivot == -1 ? 
						new Line2D.Double(points[curPivot], 
								new Point2D.Double(points[curPivot].x + Math.cos(curAngle), 
										points[curPivot].y + Math.sin(curAngle)))
						:
						new Line2D.Double(points[curPivot], points[prevPivot]);
				int nextPivot = -1;
				double closestAngle = Double.MAX_VALUE;
				for(int j = 1; j < points.length; j++){
					if(j == prevPivot || j == curPivot) continue;
					Line2D targetLine = new Line2D.Double(points[curPivot], points[j]);
					double distAngle = getAngleBetweenVec(curLine, targetLine);
					//the other end of the line
					if(distAngle > Math.PI)
						distAngle -= Math.PI;
					if(distAngle < closestAngle){
						closestAngle = distAngle;
						nextPivot = j;
					}
				}
				prevPivot = curPivot;
				curPivot = nextPivot;
				curAngle += closestAngle;
				if(curAngle >= 2.0 * Math.PI){
					curAngle -= 2.0 * Math.PI;
				}
				pivots.add(curPivot);
			}
			out.print(dataset + " ");
			for(int i = 0; i < pivots.size(); i++){
				out.print(pivots.get(i));
				if(i != pivots.size()-1){
					out.print(" " );
				}else{
					out.println();
				}
			}
		}
		out.close();
	}

	//from v1 swing to v2
	//always between 0 and 2PI
	public static double getAngleBetweenVec(Line2D v1, Line2D v2){
		double dx1 = v1.getX2() - v1.getX1();
		double dy1 = v1.getY2() - v1.getY1();

		double dx2 = v2.getX2() - v2.getX1();
		double dy2 = v2.getY2() - v2.getY1();
		
		double result = Math.atan2(dy2,dx2) - Math.atan2(dy1,dx1);
		if(result < 0) return result + 2.0 * Math.PI;
		return result;
	}
}
