package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class HDU4489 {
	static int M = 0;
	static int W = 1;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		//# configs if shape = M/W, the first guy is such that i people < him, j people > him
		long[][][] dp = new long[2][20][20];
		dp[M][0][0] = 1;
		dp[W][0][0] = 1;
		long[] ans = new long[21];
		ans[1] = 1;
		//k+1 = total number of people
		for(int k = 1; k < 20; k++){
			for(int less = 0; less <= k; less++){
				int more = k-less;
				dp[W][less][more] = 0;
				//rank = num people less than first guy
				for(int rank = 0; rank < less; rank++){
					dp[W][less][more] += dp[M][rank][k-rank-1];
				}
				
				dp[M][less][more] = 0;
				//rank = num people greater than first guy
				for(int rank = 0; rank < more; rank++){
					dp[M][less][more] += dp[W][k-rank-1][rank];
				}
				ans[k+1] += dp[M][less][more];
				ans[k+1] += dp[W][less][more];
			}
		}
		int T = Integer.parseInt(in.readLine());
		for(int t = 1; t <= T; t++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			tk.nextToken();
			out.println(t + " " + ans[Integer.parseInt(tk.nextToken())]);
		}
		
		out.close();
	}	
}
