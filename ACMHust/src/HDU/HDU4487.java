package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class HDU4487 {
	static double l;
	static double r;
	static int step;
	static double[][][] dp;
	static int PREV;
	static int CUR;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		//dp[i][j][k] = expected rightmost if have i steps left, and is currently at position j, and rightmost = j
		dp = new double[2][2002][2002];
		for(int t = 1; t <= T; t++){
			PREV = 0;
			CUR = 1;
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int dataset = Integer.parseInt(tk.nextToken());
			step = Integer.parseInt(tk.nextToken());
			l = Double.parseDouble(tk.nextToken());
			r = Double.parseDouble(tk.nextToken());
			
			for(int rightmost = -step; rightmost <= step; rightmost++){
				for(int pos = -step; pos <= rightmost; pos++){
					dp[PREV][pos+step][rightmost+step] = rightmost;
				}
			}
			
			for(int i = 1; i <= step; i++){
				for(int rightmost = -step+i; rightmost <= step-i; rightmost++){
					for(int pos = -step+i; pos <= rightmost; pos++){
						int right = rightmost + step;
						dp[CUR][pos+step][right] = 
								l*dp[PREV][pos-1+step][right] +
								r*dp[PREV][pos+1+step][step + Math.max(rightmost, pos+1)] +
								(1.0-r-l) * dp[PREV][pos+step][right];
					}
				}	
				CUR ^= 1;
				PREV ^= 1;
			}
			
			out.printf("%d %.4f\n",dataset, dp[PREV][step][step]);
		}
		
		out.close();
	}
}
