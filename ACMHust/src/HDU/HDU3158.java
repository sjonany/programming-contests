package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class HDU3158 {
	static Set<Point>[] reachables = (Set<Point>[]) Array.newInstance(
			Set.class, 25);
	static Map<Key, Integer> timeMap = new HashMap<Key, Integer>();
	static int MAX_TIME = 24;
	static double[] dx;
	static double[] dy;

	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		dx = new double[8];
		dy = new double[8];
		for (int i = 0; i < 8; i++) {
			dx[i] = 10 * Math.cos(Math.PI * i / 4);
			dy[i] = 10 * Math.sin(Math.PI * i / 4);

			switch (i) {
			case 0:
				dx[i] = 10;
				dy[i] = 0;
				break;
			case 2:
				dx[i] = 0;
				dy[i] = 10;
				break;
			case 4:
				dx[i] = -10;
				dy[i] = 0;
				break;
			case 6:
				dx[i] = 0;
				dy[i] = -10;
				break;
			}
		}

		for (int i = 0; i <= MAX_TIME; i++) {
			reachables[i] = new HashSet<Point>();
		}

		choose(0, 0, 0, 0);
		int T = Integer.parseInt(in.readLine());

		for (int test = 1; test <= T; test++) {
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int time = Integer.parseInt(tk.nextToken());
			double x = Double.parseDouble(tk.nextToken());
			double y = Double.parseDouble(tk.nextToken());

			double minDist = Integer.MAX_VALUE;

			Point target = new Point(x, y);
			for (int t = 0; t <= time; t++) {
				for (Point p : reachables[t]) {
					minDist = Math.min(minDist, dist(p, target));
				}
			}
			out.println(String.format("%.6f", Math.sqrt(minDist)));
		}

		out.close();
	}

	static double dist(Point p1, Point p2) {
		double dx = p1.x - p2.x;
		double dy = p1.y - p2.y;
		return dx * dx + dy * dy;
	}

	// at the start -> at end of time, that state
	static void choose(int time, double x, double y, int turn) {
		Point p = new Point(x, y);
		Key key = new Key(p, turn);

		Integer prevTime = timeMap.get(key); 
		if(prevTime != null && time >= prevTime){ 
			return; 
		} 

		timeMap.put(key, time);

		reachables[time].add(p);
		for (int numTurn = 0; numTurn <= 7; numTurn++) {
			int newTurn = turn - numTurn;
			if (newTurn < 0) {
				newTurn += 8;
			}

			int newTime = time + 1 + numTurn;
			if (newTime <= MAX_TIME) {
				choose(newTime, x + dx[newTurn], y + dy[newTurn], newTurn);
			}
		}
	}

	static class Key {
		public Point p;
		public int turn;

		public Key(Point p, int turn) {
			this.p = p;
			this.turn = turn;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((p == null) ? 0 : p.hashCode());
			result = prime * result + turn;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Key other = (Key) obj;
			if (p == null) {
				if (other.p != null)
					return false;
			} else if (!p.equals(other.p))
				return false;
			if (turn != other.turn)
				return false;
			return true;
		}

	}

	static class Point {
		@Override
		public String toString() {
			return "Point [x=" + x + ", y=" + y + "]";
		}

		public double x;
		public double y;

		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public int hashCode() {
			return t(x) + 31 * t(y);
		}

		public int t(double v) {
			return (int) Math.round(v*1000000);
		}

		@Override
		public boolean equals(Object obj) {
			Point o = (Point) obj;
			return t(x) == t(o.x) && t(y) == t(o.y);
		}

	}
}
