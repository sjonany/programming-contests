package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class HDU3157 {
	// flow that obeys min lower bound on G= 
	// isFeasibleCirculation( lower, upper= INF, D (all 0 except source and sink to enforce flow conservation), G)
	// so just binary search on D for source and sink 
	
	static final int MAX_CAP = 10000;
	
	static int N;
	static int M;
	static int PLUS;
	static int MINUS;
	static int S;
	static int T;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		while(true){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			N = Integer.parseInt(tk.nextToken());
			M = Integer.parseInt(tk.nextToken());
			if(N ==0 && M==0)break;
			
			MaximumFlowFordFulkerson maxFlow = new MaximumFlowFordFulkerson(2 + 2 + N + 1);
			
			PLUS = N+1;
			MINUS = N+2;
			S = N+3;
			T = N+4;
			
			int[] netIn = new int[N+3];

			for(int i = 0; i < M ; i++){
				tk = new StringTokenizer(in.readLine());
			
				int from = parseNode(tk.nextToken());
				int to = parseNode(tk.nextToken());
				int minLim = Integer.parseInt(tk.nextToken());
				maxFlow.addEdge(from, to, MAX_CAP - minLim);
				
				netIn[from] -= minLim;
				netIn[to] += minLim;
			}
			
			//each node's demand after the lower bound is dV- lV = 0 - netIn 
			int expectFlow = 0;
			for(int i = 1; i <= N; i++){
				int nodeDemand = -netIn[i];
				if(nodeDemand > 0){
					maxFlow.addEdge(i, T, nodeDemand);
				}else if(nodeDemand < 0){
					expectFlow -= nodeDemand;
					maxFlow.addEdge(S, i, -nodeDemand);
				}
			}
			
			int left = 1;
			int right = MAX_CAP;
			
			boolean found = false;
			
			List<FlowEdge> edges = new ArrayList<FlowEdge>();
			edges.add(maxFlow.addEdge(S, PLUS, 0));
			edges.add(maxFlow.addEdge(PLUS, T, 0));
			edges.add(maxFlow.addEdge(S, MINUS, 0));
			edges.add(maxFlow.addEdge(MINUS, T, 0));
			
			while(left < right) {
				int mid = (left+right)/2;
				
				for(FlowEdge edge : edges){
					edge.capacity = 0;
					edge.reverse.capacity = 0;
				}
				
				int additionalFlow = 0;
				int plusDemand = -mid - netIn[PLUS];
				if(plusDemand > 0){
					edges.get(1).capacity = plusDemand;
				}else{
					additionalFlow -= plusDemand;
					edges.get(0).capacity = -plusDemand;
				}
				
				int minusDemand = mid - netIn[MINUS];
				if(minusDemand > 0){
					edges.get(3).capacity = minusDemand;
				}else{
					additionalFlow -= minusDemand;
					edges.get(2).capacity = -minusDemand;
				}
				
				maxFlow.resetFlow();
				
				int F = maxFlow.getMaximumFlow(S, T);
				if(F == expectFlow + additionalFlow){
					right = mid;
					found = true;
				}else{
					left = mid+1;
				}
			}
			
			if(!found){
				out.println("impossible");
			}else{
				out.println(left);
			}
			
		}
		
		out.close();
	}
	
	static int parseNode(String tok) {
		if(tok.equals("+")){
			return PLUS;
		}else if(tok.equals("-")){
			return MINUS;
		}else{
			return Integer.parseInt(tok);
		}
	}
	
    public static class FlowEdge {
		public int dest;
        public int capacity;
        //flow left allowed, NOT currently flowing
        public int flow;
        public FlowEdge reverse;
        
    	public FlowEdge(int v, int capacity, int flow) {
			this.dest = v;
			this.capacity = capacity;
			this.flow = flow;
		}
    }
	
	private static class MaximumFlowFordFulkerson {
	 
	    private int N; // node from 0 to N - 1
	    public List<FlowEdge>[] adjMap;
	    int mark;   // global variable for checking if a node is already visited
	    int[] seen;  // status of each node
	 
	    MaximumFlowFordFulkerson(int numNode){
	        N = numNode;
	        adjMap = (List<FlowEdge>[]) Array.newInstance(List.class, N);
	        for(int i = 0; i < adjMap.length; i++){
	        	adjMap[i] = new ArrayList<FlowEdge>();
	        }
	        
	        seen = new int[N];
	        mark = 0;
	    }
	 
	    public FlowEdge addEdge(int from, int to, int cap){
	    	FlowEdge forward = new FlowEdge(to, cap, cap);
	    	FlowEdge back = new FlowEdge(from, 0, 0);
	    	adjMap[from].add(forward);
	        adjMap[to].add(back);
	        forward.reverse = back;
	        back.reverse = forward;
	        return forward;
	    }
	 
	    public void resetFlow(){
	        for(int i = 0; i < N; i++ ){
	            int sz = adjMap[i].size();
	            for(int j = 0; j < sz; j++ ){
	            	FlowEdge e = adjMap[i].get(j);
	            	e.flow = e.capacity;
	            }
	        }
	    }
	 
	    private int findAugmentingPath(int at, int sink, int val){
	        int sol = 0;
	        seen[at] = mark;
	        if(at == sink) return val;
	        int sz = adjMap[at].size();
	        for(int i = 0; i < sz; i++ ){
	            FlowEdge flow = adjMap[at].get(i);
	            int v = flow.dest;
	            int f = flow.flow;
	            if(seen[v] != mark && f > 0){
	                sol = findAugmentingPath(v, sink, Math.min(f, val));
	                if(sol > 0){
	                    flow.flow -= sol;
	                    flow.reverse.flow += sol;
	                    return sol;
	                }
	            }
	        }
	        return 0;
	    }
	 
	    public int getMaximumFlow(int S, int T){
	        int res = 0;
	        while(true){
	            mark++;
	            int flow = findAugmentingPath(S, T, Integer.MAX_VALUE);
	            if(flow == 0) break;
	            else res += flow;
	        }
	        return res;
	    }
	};
	
}
