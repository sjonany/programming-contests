package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class HDU3157Faster {
	// jaro's idea.
	/*
	 Get f* >= low caps
	 construct graph G' = f*-lowcaps, and run max flow
	 f* - maxFlow(G') is the answer. 
	
	 This flow is a valid flow.
	 	- conservation
	 	- capacity (can't be < lower bound)
	 assume not, that means the optimal soln f' has lower answer.
	 f' can be constructed by deducting from f* for sure.
	 This implies that maxFlow(G') is not max. contradiction.
	 */
	
	static final int MAX_CAP = 10000;
	
	static int N;
	static int M;
	static int PLUS;
	static int MINUS;
	//hack for a flow > lows
	//cap = lower bound capacity
	//flow > cap
	static List<FlowEdge>[] aboveFlow;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		while(true){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			N = Integer.parseInt(tk.nextToken());
			M = Integer.parseInt(tk.nextToken());
			if(N ==0 && M==0)break;
			
			PLUS = N+1;
			MINUS = N+2;
			
			aboveFlow = (List<FlowEdge>[]) Array.newInstance(List.class, 2+N+1);
			for(int i = 0; i < aboveFlow.length; i++){
				aboveFlow[i] = new ArrayList<FlowEdge>();
			}
			
			for(int i = 0; i < M ; i++){
				tk = new StringTokenizer(in.readLine());
			
				int from = parseNode(tk.nextToken());
				int to = parseNode(tk.nextToken());
				int minLim = Integer.parseInt(tk.nextToken());
				aboveFlow[from].add(new FlowEdge(to, minLim, 0));
			}
			
			boolean canAllReachSink = true;
			
			for(int source = 0; source < aboveFlow.length; source++){
				List<FlowEdge> outs = aboveFlow[source];
				for(FlowEdge edge : outs){
					if(edge.flow == 0){
						edge.flow += MAX_CAP;
						int val1 = pushFlow(PLUS, source, MAX_CAP, new boolean[aboveFlow.length]);
						int val2 = pushFlow(edge.dest, MINUS, MAX_CAP, new boolean[aboveFlow.length]);
						if(val1 == 0 || val2 == 0){
							canAllReachSink = false;
							break;
						}
					}
				}
				if(!canAllReachSink)
					break;
			}
			
			if(!canAllReachSink){
				out.println("impossible");
				continue;
			}
			
			int curFlow = 0;
			for(FlowEdge flow : aboveFlow[PLUS]){
				curFlow += flow.flow;
			}
			
			MaximumFlowFordFulkerson maxFlow = new MaximumFlowFordFulkerson(2 + N + 1);
			for(int i = 0; i < aboveFlow.length; i++){
				List<FlowEdge> outs = aboveFlow[i];
				for(FlowEdge fe : outs){
					maxFlow.addEdge(i, fe.dest, fe.flow - fe.capacity);
				}
			}
			out.println(curFlow - maxFlow.getMaximumFlow(PLUS, MINUS));
		}
		
		out.close();
	}
	
    private static int pushFlow(int at, int sink, int val, boolean[] seen){
        int sol = 0;
        seen[at] = true;
        if(at == sink) return val;
        int sz = aboveFlow[at].size();
        for(int i = 0; i < sz; i++ ){
            FlowEdge flow = aboveFlow[at].get(i);
            int v = flow.dest;
            if(!seen[v]){
                sol = pushFlow(v, sink, val, seen);
                if(sol > 0){
                    flow.flow += sol;
                    return sol;
                }
            }
        }
        return 0;
    }
	
	static int parseNode(String tok) {
		if(tok.equals("+")){
			return PLUS;
		}else if(tok.equals("-")){
			return MINUS;
		}else{
			return Integer.parseInt(tok);
		}
	}
	
    public static class FlowEdge {
		public int dest;
        public int capacity;
        //flow left allowed, NOT currently flowing
        public int flow;
        public FlowEdge reverse;
        
    	public FlowEdge(int v, int capacity, int flow) {
			this.dest = v;
			this.capacity = capacity;
			this.flow = flow;
		}
    }
	
	private static class MaximumFlowFordFulkerson {
	 
	    private int N; // node from 0 to N - 1
	    public List<FlowEdge>[] adjMap;
	    int mark;   // global variable for checking if a node is already visited
	    int[] seen;  // status of each node
	 
	    MaximumFlowFordFulkerson(int numNode){
	        N = numNode;
	        adjMap = (List<FlowEdge>[]) Array.newInstance(List.class, N);
	        for(int i = 0; i < adjMap.length; i++){
	        	adjMap[i] = new ArrayList<FlowEdge>();
	        }
	        
	        seen = new int[N];
	        mark = 0;
	    }
	 
	    public FlowEdge addEdge(int from, int to, int cap){
	    	FlowEdge forward = new FlowEdge(to, cap, cap);
	    	FlowEdge back = new FlowEdge(from, 0, 0);
	    	adjMap[from].add(forward);
	        adjMap[to].add(back);
	        forward.reverse = back;
	        back.reverse = forward;
	        return forward;
	    }
	 
	    public void resetFlow(){
	        for(int i = 0; i < N; i++ ){
	            int sz = adjMap[i].size();
	            for(int j = 0; j < sz; j++ ){
	            	FlowEdge e = adjMap[i].get(j);
	            	e.flow = e.capacity;
	            }
	        }
	    }
	 
	    private int findAugmentingPath(int at, int sink, int val){
	        int sol = 0;
	        seen[at] = mark;
	        if(at == sink) return val;
	        int sz = adjMap[at].size();
	        for(int i = 0; i < sz; i++ ){
	            FlowEdge flow = adjMap[at].get(i);
	            int v = flow.dest;
	            int f = flow.flow;
	            if(seen[v] != mark && f > 0){
	                sol = findAugmentingPath(v, sink, Math.min(f, val));
	                if(sol > 0){
	                    flow.flow -= sol;
	                    flow.reverse.flow += sol;
	                    return sol;
	                }
	            }
	        }
	        return 0;
	    }
	 
	    public int getMaximumFlow(int S, int T){
	        int res = 0;
	        while(true){
	            mark++;
	            int flow = findAugmentingPath(S, T, Integer.MAX_VALUE);
	            if(flow == 0) break;
	            else res += flow;
	        }
	        return res;
	    }
	};
	
}
