package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class HDU3149 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int n = Integer.parseInt(in.readLine());
		List<Integer> probs = new ArrayList<Integer>();
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < n; i++){
			int x = Integer.parseInt(in.readLine());
			probs.add(x);
			max = Math.max(x, max);
		}
		
		long[] dp = new long[max+1];
		dp[1] =1;
		for(int i = 2; i <= max; i++){
			dp[i] = (long)i * (i+1) /2 + dp[i-1];
		}
		
		for(int test = 1 ; test <= n; test++){
			out.printf("%d: %d %d\n", test, probs.get(test-1), dp[probs.get(test-1)]);
		}
		
		
		out.close();
	}
}
