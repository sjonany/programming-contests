package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class HDU4492 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int P = Integer.parseInt(in.readLine());
		for(int p = 1; p <= P; p++){
			in.readLine();
			String str = in.readLine();
			in.readLine();
			StringTokenizer tk = new StringTokenizer(in.readLine());
			StringBuilder sb = new StringBuilder();
			int curIndex = 0;
			while(tk.hasMoreTokens()){
				int move = Integer.parseInt(tk.nextToken());
				curIndex += move;
				if(curIndex < 0){
					curIndex += str.length();
				}else{
					curIndex %= str.length();
				}
				sb.append(str.charAt(curIndex));
			}
			out.println(p + " " + sb);
		}
		
		out.close();
	}
	
}
