package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class HDU4488 {
	static Fraction[][] dp;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		dp = new Fraction[401][402];

		dp[0][1] = new Fraction(1,1);
		for(int r = 1 ; r < dp.length; r++){
			Fraction sum = new Fraction(0,1);
			for(int c = 2; c <= r+1; c++){
				dp[r][c] = new Fraction(r,c).mul(dp[r-1][c-1]);
				sum = sum.add(dp[r][c]);
			}
			dp[r][1] = new Fraction(sum.d.subtract(sum.n), sum.d);
		}
		for(int t = 1; t <= T; t++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int dataset = Integer.parseInt(tk.nextToken());
			int m = Integer.parseInt(tk.nextToken());
			int k = Integer.parseInt(tk.nextToken());
			
			out.println(t + " " + dp[m][k]);
		}
		out.close();
	}
	
	static class Fraction{
		@Override
		public String toString(){
			if(d.equals(BigInteger.ONE)){
				return n.toString();
			}else
				return n + "/" + d;
		}
		
		public BigInteger n;
		public BigInteger d;
		
		public Fraction(long n, long d){
			this(BigInteger.valueOf(n), BigInteger.valueOf(d));
		}
		
		public Fraction(BigInteger n, BigInteger d) {
			this.n = n;
			this.d = d;
			norm();
		}
		
		public void norm(){
			BigInteger gcd = gcd(n,d);
			n = n.divide(gcd);
			d = d.divide(gcd);
		}
		
		public Fraction mul(Fraction other){
			return new Fraction(this.n.multiply(other.n), this.d.multiply(other.d));
		}
		
		public Fraction add(Fraction other){
			return new Fraction(this.n.multiply(other.d).add(this.d.multiply(other.n)), this.d.multiply(other.d));
		}

		public static BigInteger gcd(BigInteger a, BigInteger b) //valid for positive BigIntegeregers.
		{
			while(b.compareTo(BigInteger.ZERO) > 0)
			{
				BigInteger c = a.mod(b);
				a = b;
				b = c;
			}
			return a;
		} 
	}
}
