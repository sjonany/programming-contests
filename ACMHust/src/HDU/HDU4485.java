package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class HDU4485 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int t = 1; t <= T; t++){
			while(in.read() != ' '){}
			
			int base = -1;
			int dig1char = in.read();
			int dig2char = in.read();
			if(dig2char == ' '){
				//one digit
				base = dig1char-'0';
			}else{
				base= 10;
				in.read();
			}
			int ans = 0;
			while(true){
				char nextchar = (char)in.read();
				if(!Character.isDigit(nextchar))break;
				int dig = (nextchar-'0');
				if(dig < 0 || dig > 9)break;
				ans +=  dig%(base-1);
				ans %= (base-1);
			}
			out.println(t + " " + ans);
		}
		out.close();
	}
}
