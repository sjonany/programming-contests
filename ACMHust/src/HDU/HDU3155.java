package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class HDU3155 {
	static List<Character> UN = Arrays.asList(new Character[]{'N'});
	static List<Character> BI = Arrays.asList(new Character[]{'C','K','A','D','E','J'});
	
	static int status;
	static int OK = 0;
	static int TOO_SHORT = 1;
	static int TOO_LONG = 2;
	static int index = 0;
	static String input;
	
	static int[][] logicC = new int[][]{{1,1},{0,1}};
	static int[][] logicD = new int[][]{{1,1},{1,0}};
	static int[][] logicE = new int[][]{{1,0},{0,1}};
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		while(true){
			input = in.readLine();
			if(input == null || input.trim().length() == 0){
				break;
			}
			
			if(!isValidChar(input)){
				out.println(String.format("%s is invalid: invalid character", input));
				continue;
			}
			
			status = OK;
			index = 0;
			Node root = parseOneExpression();
			if(status == OK && index != input.length())
				status = TOO_LONG;
			
			if(status == TOO_LONG){
				out.println(String.format("%s is invalid: extraneous text", input));
			}else if(status == TOO_SHORT){
				out.println(String.format("%s is invalid: insufficient operands", input));
			}else{
				Set<Character> vars = new HashSet<Character>();
				for(int i = 0 ; i < input.length(); i++){
					char c = input.charAt(i);
					if(Character.isLowerCase(c)){
						vars.add(c);
					}
				}
				int numVar = vars.size();
				List<Character> varList = new ArrayList<Character>();
				varList.addAll(vars);
				
				boolean hasTrue = false;
				boolean hasFalse = false;
				Map<Character, Boolean> assignment = new HashMap<Character, Boolean>();
				
				for(int bit = 0; bit < 1<<(numVar); bit++){
					int bCopy = bit;
					for(int vi = 0; vi < numVar; vi++){
						assignment.put(varList.get(vi), (bCopy % 2 == 0) ? false : true);
						bCopy >>= 1;
					}
					
					boolean result = eval(root, assignment);
					if(result){
						hasTrue = true;
					}
					
					if(!result){
						hasFalse = true;
					}
					
					if(hasTrue && hasFalse)break;
				}
				
				if(hasTrue && hasFalse){
					out.println(input + " is valid: contingent");
				}else if(hasTrue && !hasFalse){
					out.println(input + " is valid: tautology");
				}else{
					out.println(input + " is valid: contradiction");
				}
			}
		}
		
		out.close();
	}
	
	static boolean eval(Node n, Map<Character, Boolean> assignment){
		if(Character.isLowerCase(n.c)){
			return assignment.get(n.c);
		}else{
			if(n.c == 'N'){
				return !eval(n.left, assignment);
			}else{
				boolean l = eval(n.left, assignment);
				boolean r = eval(n.right, assignment);
				int ln = l ? 1: 0;
				int rn = r ? 1: 0;
				
				switch(n.c){
				case 'C':
					return logicC[ln][rn] == 1;
				case 'K':
					return l && r;
				case 'A':
					return l || r;
				case 'D':
					return logicD[ln][rn] == 1;
				case 'E':
					return logicE[ln][rn] == 1;
				case 'J':
					return l ^ r;
				}
			}
		}
		throw new IllegalArgumentException();
	}
	
	//invariant = index is the start of expression to read
	static Node parseOneExpression(){
		if(status == TOO_SHORT)
			return null;
		if(index >= input.length()){
			status = TOO_SHORT;
			return null;
		}
		char c = input.charAt(index);
		if(Character.isLowerCase(c)){
			index++;
			return new Node(c, null, null);
		}else{
			if(c == 'N'){
				index++;
				Node child = parseOneExpression();
				return new Node(c, child, null);
			}else{
				//binary
				index++;
				Node left = parseOneExpression();
				Node right = parseOneExpression();
				return new Node(c, left, right);
			}
		}
	}
	
	static class Node{
		public char c;
		public Node left;
		public Node right;
		
		public Node(char c, Node left, Node right) {
			this.c = c;
			this.left = left;
			this.right = right;
		}	
	}
	
	static boolean isValidChar(String str){
		for(int i = 0 ;i < str.length(); i++){
			char c = str.charAt(i);
			if(Character.isDigit(c)){
				return false;
			}
			if(Character.isUpperCase(c)){
				if(!UN.contains(c) && !BI.contains(c)){
					return false;
				}
			}
			
		}
		return true;
	}
}
