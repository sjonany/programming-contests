package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;


public class HDU3154 {
	static Problem[] problems;
	static int K;
	static int maxSolve;
	static int bestTime;
	static int[] curTime;
	static List<Problem>[] work = (List<Problem>[]) Array.newInstance(List.class, 3);
	static String bestResult;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int n = Integer.parseInt(in.readLine());
		for(int test = 1; test <= n; test++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			K = Integer.parseInt(tk.nextToken());
			problems = new Problem[K];
			for(int order = 0; order < K; order++){
				problems[order] = new Problem(Integer.parseInt(tk.nextToken()), order);
			}
			
			Arrays.sort(problems);
			for(int i = 0 ; i < 3; i++){
				work[i] = new ArrayList<Problem>();
			}	
			
			bestTime = Integer.MAX_VALUE;
			maxSolve = Integer.MIN_VALUE;
			curTime = new int[3];
			work[0].add(problems[0]);
			curTime[0] += problems[0].time;
			bestResult = null;
			choose(1, problems[0].time);
			
			
			out.print("Data set " + test +": ");
			for(int i = 0; i < bestResult.length(); i++){
				out.print(bestResult.charAt(i) + " ");
			}
			out.println(maxSolve + " " + bestTime);
		}
		
		out.close();
	}
	
	static void choose(int pIdx, int penalty){
		boolean canStillPut = false;
		
		if(pIdx != problems.length){
			int maxPut = 0;
			for(int i = 0 ; i < 3 ; i++){
				maxPut += (300-curTime[i])/problems[pIdx].time;
			}
			if(maxPut + pIdx < maxSolve) return;
			
			Problem p = problems[pIdx];
			for(int i = 0; i < 3; i++){
				if(p.time + curTime[i] <= 300){
					curTime[i] += p.time;
					work[i].add(p);
					choose(pIdx+1, penalty + curTime[i]);
					work[i].remove(work[i].size()-1);
					curTime[i] -= p.time;
					canStillPut = true;
				}
			}
		}
		
		if(!canStillPut){
			//record
			boolean shouldUpdate = false;
			boolean forceUpdate = false;
			if(maxSolve < pIdx){
				maxSolve = pIdx;
				bestTime = penalty;
				shouldUpdate = true;
				forceUpdate = true;
			}else if(maxSolve == pIdx){
				if(bestTime > penalty){
					bestTime = penalty;
					shouldUpdate = true;
					forceUpdate = true;
				}else if(bestTime == penalty){
					shouldUpdate = true;
				}
			}
			if(shouldUpdate){
				List<Problem> solved = new ArrayList<Problem>();
				for(int i = 0 ; i < 3; i++){
					int time = 0;
					for(Problem p :  work[i]){
						time += p.time;
						solved.add(new Problem(time,p.order));
					}
				}
				Collections.sort(solved);
				StringBuilder output = new StringBuilder();
				for(Problem p : solved){
					output.append((char)(p.order + 'A'));
				}
				String result = output.toString();
				
				if(forceUpdate || bestResult == null || result.compareTo(bestResult) < 0){
					bestResult = result;
				}
			}
		}
		
	}
	
	static class Problem implements Comparable<Problem>{
		public Problem(int time, int order) {
			this.time = time;
			this.order = order;
		}
		public int time;
		public int order;
		@Override
		public int compareTo(Problem o) {
			if(this.time != o.time){
				return this.time - o.time;
			}
			return this.order - o.order;
		}
	}
}
