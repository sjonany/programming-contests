package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;


public class HDU3159 {
	//key = cost, val = nums with that cost
	static Map<Integer, Set<Integer>> costMap;
	//key = num, val = cost
	static Map<Integer, Integer> costInvMap;
	static int[] res = new int[4];
	static int resIndex = 0;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		initMap();
		
		while(true){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int xOriCost = Integer.parseInt(tk.nextToken());
			if(xOriCost==0)break;
			int yOriCost = Integer.parseInt(tk.nextToken());
			int zOriCost = Integer.parseInt(tk.nextToken());

			int numSoln = 0;
			if(xOriCost % 5 == 0 && yOriCost % 5 == 0 && zOriCost % 5 == 0){
				int xCost = xOriCost / 5;
				int yCost = yOriCost / 5;
				int zCost = zOriCost / 5;
				
				if(costMap.containsKey(xCost) && costMap.containsKey(yCost) && costMap.containsKey(zCost)){
					Set<Integer> xSet = costMap.get(xCost);
					Set<Integer> ySet = costMap.get(yCost);
					for(int x : xSet){
						for(int y : ySet){
							resIndex = 0;
							res[resIndex++] = x+y;
							res[resIndex++] = x-y;
							res[resIndex++] = x*y;
							if(y!=0){
								res[resIndex++] = x/y;
							}
							
							for(int resultIndex = 0; resultIndex < resIndex; resultIndex++){
								int result = res[resultIndex];
								if(Math.abs(result) <= 999){
									if(costInvMap.get(result) == zCost){
										numSoln++;
									}
								}
							}
						}
					}
				}
			}
			if(numSoln == 1){
				out.println(numSoln + " solution for " + xOriCost + " " + yOriCost + " " + zOriCost);
			}else{
				out.println(numSoln + " solutions for " + xOriCost + " " + yOriCost + " " + zOriCost);
			}
		}
		
		out.close();
	}
	
	static int[] cost = new int[]{6,2,5,5,4,5,6,3,7,6};
	static void initMap(){
		costMap = new HashMap<Integer, Set<Integer>>();
		costInvMap = new HashMap<Integer, Integer>();
		
		costInvMap.put(0, cost[0]);
		for(int val = 1; val <= 999; val++){
			int x = val;
			int cur = 0;
			while(x>0){
				cur+=cost[x%10];
				x/=10;
			}
			costInvMap.put(val, cur);
		}
		
		for(int val = -1; val >= -999; val--){
			costInvMap.put(val, 1+costInvMap.get(-val));
		}
		
		for(int i = -999; i<=999;i++){
			int curCost = costInvMap.get(i);
			Set<Integer> set = costMap.get(curCost);
			if(set == null){
				set = new HashSet<Integer>();
				costMap.put(curCost, set);
			}
			set.add(i);
		}
	}
}
