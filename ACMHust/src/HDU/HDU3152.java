package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.PriorityQueue;
import java.util.StringTokenizer;


public class HDU3152 {
	static int[][] mat;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int test = 1;
		while(true){
			int N = Integer.parseInt(in.readLine());
			if(N==0)break;
			mat = new int[N][N];
			for(int i = 0; i < N; i++){
				StringTokenizer tk = new StringTokenizer(in.readLine());
				for(int j = 0; j < N; j++){
					mat[i][j] = Integer.parseInt(tk.nextToken());
				}	
			}
			out.println("Problem " + test + ": " + shortestPath());
			test++;
		}
		
		out.close();
	}
	
	static int[][] dirs = new int[][]{{0,1},{1,0},{-1,0},{0,-1}};
	
	public static int shortestPath(){
		int[][] distance = new int[mat.length][mat.length];
		for(int r = 0 ;r < distance.length; r++){
			for(int c = 0 ;c < distance.length; c++){
				distance[r][c] = Integer.MAX_VALUE;
			}	
		}
		
		distance[0][0] = mat[0][0];
		PriorityQueue<PQNode> pq = new PriorityQueue<PQNode>();
		Node start = new Node(0,0);
		pq.add(new PQNode(start, mat[0][0]));
		
		while(!pq.isEmpty()){
			PQNode pqNode = pq.poll();
			Node node = pqNode.node;
			
			if(node.row == mat.length-1 && node.col == mat.length-1){
				return pqNode.weight;
			}
			
			if(distance[node.row][node.col] < pqNode.weight){
				continue;
			}
			
			for(int[] dir : dirs){
				int r = node.row + dir[0];
				int c = node.col + dir[1];
				
				if(r >= 0 && r < mat.length && c >=0 && c<mat.length){
					Node next = new Node(r,c);
					int nextWeight = pqNode.weight + mat[r][c];
					if(distance[r][c] > nextWeight){
						distance[r][c] = nextWeight;
						pq.add(new PQNode(next, nextWeight));
					}
				}
			}
		}
		return -1;
	}
	
	static class PQNode implements Comparable<PQNode>{
		public int weight;
		public Node node;
		
		public PQNode(Node node, int weight) {
			this.weight = weight;
			this.node = node;
		}

		@Override
		public int compareTo(PQNode o) {
			return this.weight - o.weight;
		}
	}
	
	static class Node{
		public int row;
		public int col;
		public Node(int row, int col) {
			this.row = row;
			this.col = col;
		}
	}
}
