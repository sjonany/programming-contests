package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;


public class HDU3150 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			out.printf("Test set %d:\r\n", test);
			int numLine = Integer.parseInt(in.readLine());
			Set<String> targets = new HashSet<String>();
			List<String> inputs = new ArrayList<String>();
			for(int i = 0; i < numLine; i++){
				String target = in.readLine().trim();
				targets.add(target);
				inputs.add(target);
			}
			
			Set<String> result = new HashSet<String>();
			int streamLine = Integer.parseInt(in.readLine());
			for(int i = 0 ; i< streamLine; i++){
				StringTokenizer tk = new StringTokenizer(in.readLine());
				while(tk.hasMoreTokens()){
					String tok = tk.nextToken();
					if(targets.contains(tok)){
						result.add(tok);
					}
				}
			}
			
			for(String input : inputs){
				if(result.contains(input)){
					out.println(input + " is present");
				}else{
					out.println(input + " is absent");
				}
			}
			out.println();
		}
		out.close();
	}
}
