package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;


public class HDU3153 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		//400 50 25 = 1200 + 750 + 50
		//4000 = 2400 + 1500+
		
		int test = 1;
		while(true){
			int n = Integer.parseInt(in.readLine());
			if(n==0)break;
			
			out.println("Case " + test + ":");
			boolean found = false;
			out.println(n + " pencils for " + n + " cents");
			
			int want = n * 100;
			for(int fourHundreds = 1; fourHundreds <= want/400+1; fourHundreds++){
				if(fourHundreds * 400 > want) break;
				int remAmount = want - fourHundreds * 400;
				int remCoin = n - fourHundreds;
				if(remCoin < 2)continue;
				
				//50x + 25y = remAmount
				//x+y = remCoin
				//x = remCoin -y
				//50(remCoin-y) + 25y = remAmount
				//25y = 50*remCoin - remAmount
				if(remAmount%25!=0)continue;
				int numTwentyFive = 2*remCoin - remAmount/25;
				int numFifty = remCoin -numTwentyFive;
				if(numFifty > 1 && numTwentyFive > 1){
					found = true;
					out.println(fourHundreds + " at four cents each");
					out.println(numFifty + " at two for a penny");
					out.println(numTwentyFive + " at four for a penny");
					out.println();
				}
			}
			
			if(!found){
				out.println("No solution found.");
				out.println();
			}	
			test++;
		}
		
		
		out.close();
	}
}
