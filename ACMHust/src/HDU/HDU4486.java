package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class HDU4486 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int t = 1; t <= T; t++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int dataset = Integer.parseInt(tk.nextToken());
			int n = Integer.parseInt(tk.nextToken());
			long count = 0;
			
			//c = longest triangle side
			//for a triangle, a+b>c, etc, but since c is the longest ,the only condition to
			//care about is a+b > c
			for(int c = (int) Math.ceil(n/3.0); c < n-1; c++){
				if(c *3 == n){
					count++;
				}else{
					//we need a+b > c, a+b = n-c, and max(a,b) <= c
					if(n-c > c){
						//max value b can take - min value b can take + 1
						count += c - (n-2*c) + 1;
						//a|b == c, overcount
						count--;
					}
				}
			}
			out.println(t + " " + count);
		}
		
		out.close();
	}
	
}
