package HDU;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class HDU4484 {
	static Map<Long, Long> dp;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		dp = new HashMap<Long, Long>();
		dp.put((long)1, (long)1);
		for(int i = 1; i < 100001; i++){
			solve(i);
		}
		
		int T = Integer.parseInt(in.readLine());
		for(int t = 1; t <= T; t++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int dataset = Integer.parseInt(tk.nextToken());
			int n = Integer.parseInt(tk.nextToken());
			out.println(dataset + " " + dp.get((long)n));
		}
		
		out.close();
	}
	
	static long solve(long i){
		if(dp.containsKey(i)){
			return dp.get(i);
		}else{
			if(i % 2 == 0){
				long ans = solve(i/2);
				dp.put(i, Math.max(i,ans));
			}else{
				long ans = solve(3*i+1);
				dp.put(i, Math.max(i,ans));
			}
			return dp.get(i);
		}
	}
}
