package UVA;
import java.awt.geom.Line2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class uva10167 {
	static int[][] cherries;
	static int R = 100;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		boolean[][] checkpos = new boolean[501][501];
		for(int A = 0; A <= 500; A++){
			for(int B = 0; B<=500; B++){
				if(A==0 || B ==0) continue;
				int g = gcd(A,B);
				if(!checkpos[A/g][B/g]){
					checkpos[A][B] = true;
				}
			}
		}
		checkpos[0][1] = true;
		checkpos[1][0] = true;
		
		while(true){
			int n = Integer.parseInt(in.readLine());
			if(n==0)break;
			cherries = new int[2*n][2];
			for(int i = 0; i < 2*n; i++){
				tk = tk(in.readLine());
				int x = Integer.parseInt(tk.nextToken());
				int y = Integer.parseInt(tk.nextToken());
				cherries[i][0] = x;
				cherries[i][1] = y;
			}
			
			int ansA = -1;
			int ansB = -1;
			for(int A = 0; A <= 500; A++){
				if(ansA != -1) break;
				for(int B = 0; B <= 500; B++){
					if(checkpos[A][B]){
						int count = count(A,B);
						if(count == n){
							ansA=A;
							ansB=B;
							break;
						}
						int count2 = count(A,-B);
						if(count2 == n){
							ansA=A;
							ansB=-B;
							break;
						}
					}
				}
			}
			out.println(ansA + " " + ansB);
		}
		
		out.close();
	}
	
	//count how many negs, if on line, return -1
	static int count(double A, double B){
		Line2D line = null;
		if(A == 0){
			line = new Line2D.Double(-R, 0, R, 0);
		}else if(B == 0){
			line = new Line2D.Double(0,R, 0, -R);
		}else{
			line = new Line2D.Double(-R, -A/B*-R , R, -A/B*R);
		}
		int count = 0;
		for(int[] cher : cherries){
			int s = side(line, cher[0], cher[1]);
			if(s==0)return -1;
			if(s<0)count++;
		}
		
		return count;
	}
	
	static int side(Line2D divide, double x, double y){
		double[] vDiv = new double[]{divide.getX2()-divide.getX1(),
				divide.getY2()-divide.getY1(),0};
		double[] v1 = new double[]{x-divide.getX1(), y-divide.getY1(),0};
		double c = vDiv[0] * v1[1] - vDiv[1]*v1[0];
		if(Math.abs(c) < 0.00001){
			return 0;
		}else if(c>0){
			return 1;
		}else{
			return -1;
		}
	}
	
	static int gcd(int a, int b){
		while(b>0){
			int c = a%b;
			a = b;
			b = c;
		}
		return a;
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
