package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class uva10282 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		Map<String, String> dic = new HashMap<String, String>();
		while(true){
			String line = in.readLine().trim();
			if(line.length() == 0)break;
			tk = tk(line);
			String en = tk.nextToken();
			String foreign = tk.nextToken();
			dic.put(foreign, en);
		}
		
		while(true){
			String line = in.readLine();
			if(line == null)break;
			line = line.trim();
			if(line.length() == 0)break;
			if(dic.containsKey(line)){
				out.println(dic.get(line));
			}else{
				out.println("eh");
			}
		}
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
