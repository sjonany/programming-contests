package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class UVA11695 {
	static int T;
	static int N;
	static Set<Integer>[] edges;
	static List<Edge> allEdges;
	static int MAX_N = 2501;
	static int[] parent;
	static int minDiam;
	
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		T = Integer.parseInt(reader.readLine());
		edges = (Set<Integer>[]) Array.newInstance(Set.class, MAX_N);
		for(int i = 0; i < edges.length; i++){
			edges[i] = new HashSet<Integer>();
		}
		allEdges = new ArrayList<Edge>();
		parent = new int[MAX_N];
		for(int t = 0; t < T; t++){
			
			N = Integer.parseInt(reader.readLine());
			for(int i = 1; i <= N; i++){
				edges[i].clear();
			}
			allEdges.clear();
			
			for(int i = 1; i <= N-1; i++){
				StringTokenizer tk = new StringTokenizer(reader.readLine());
				int a = Integer.parseInt(tk.nextToken());
				int b = Integer.parseInt(tk.nextToken());
				edges[a].add(b);
				edges[b].add(a);
				allEdges.add(new Edge(a,b));
			}
			
			minDiam = Integer.MAX_VALUE;
			Edge bestCut = null;
			Edge bestAdd = null;
			
			for(Edge cutEdge : allEdges){
				Node n1 = getFurthest(cutEdge.b, -1, 0, cutEdge);
				if(n1 == null) continue;
				Node other1 = getFurthest(n1.id, -1, 0, cutEdge);
				if(other1 == null) continue;
				int diam1 = other1.depth;
				
				Node n2 = getFurthest(cutEdge.a, -1, 0, cutEdge);
				if(n2 == null) continue;
				if(n2.depth >= minDiam) continue;
				Node other2 = getFurthest(n2.id, -1, 0, cutEdge);
				if(other2 == null) continue;
				int diam2 = other2.depth;
				int diam = Math.max(diam1, diam2);
				diam = Math.max(diam, 1 + (diam1+1)/2 + (diam2+1)/2);
				
				if(diam < minDiam){
					minDiam = diam;
					bestCut = cutEdge;
					bestAdd = new Edge(getCenter(diam1, other1.id), getCenter(diam2, other2.id));
				}
			}
			
			out.println(minDiam);
			out.println(bestCut.a + " " + bestCut.b);
			out.println(bestAdd.a + " " + bestAdd.b);
		}	
		
		out.close();
	}

	static int getCenter(int diam, int cornerNode){
		int midNode = cornerNode;
		for(int i = 1; i <= diam / 2 && parent[midNode] != -1; i++){
			midNode = parent[midNode];
		}
		return midNode;
	}
	
	//get node deepest from this root, depth of root given 
	static Node getFurthest(int root, int par, int depth, Edge cutEdge){
		//optimization :D
		if(depth >= minDiam){
			return null;
		}
		parent[root] = par;
		Node deepest = new Node(root, depth);
		for(int child : edges[root]){
			if(child != par && !((child == cutEdge.a && root == cutEdge.b) || (child == cutEdge.b && root == cutEdge.a))){
				Node n = getFurthest(child, root, depth+1, cutEdge);
				if(n == null) return null;
				if(n.depth > deepest.depth){
					deepest = n;
				}
			}
		}
		return deepest;
	}
	
	static class Node{
		public int depth;
		public int id;
		public Node(int i, int d){
			depth = d;
			id = i;
		}
	}
	
	static class Edge{
		public int a;
		public int b;
		public Edge(int a, int b){
			if(a > b){
				int c = a;
				a = b;
				b = c;
			}
			this.a = a;
			this.b = b;
		}
	}
}
