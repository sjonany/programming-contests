package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class UVA10300 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			int f = Integer.parseInt(in.readLine());
			
			int total = 0;
			for(int i = 0; i<f; i++){
				StringTokenizer tk = new StringTokenizer(in.readLine());
				int size = Integer.parseInt(tk.nextToken());
				int numAnimal = Integer.parseInt(tk.nextToken());
				int envFriend = Integer.parseInt(tk.nextToken());
				total += size*envFriend;
			}
			out.println(total);
		}
		
		out.close();
	}
}
