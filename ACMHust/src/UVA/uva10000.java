package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;


public class uva10000 {
	static List<Integer>[] inedges;
	static int[] dp;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		int test = 1;
		while(true){
			int n = Integer.parseInt(in.readLine());
			//max distance from s to i, -1 if not possible
			dp = new int[n+1];
			Arrays.fill(dp, -2);
			if(n==0)break;
			inedges = (List<Integer>[]) Array.newInstance(List.class, n+1);
			for(int i= 1; i <= n; i++){
				inedges[i] = new ArrayList<Integer>();
			}
			int s = Integer.parseInt(in.readLine());
			while(true){
				tk = tk(in.readLine());
				int from = Integer.parseInt(tk.nextToken());
				int to = Integer.parseInt(tk.nextToken()); 
				if(from==0 && to==0)break;
				inedges[to].add(from);
			}
			dp[s] = 0;
			int maxDist = 0;
			for(int i = 1; i<= n; i++){
				int dist = get(i);
				maxDist = Math.max(maxDist, dist);
			}

			int minNode = Integer.MAX_VALUE;
			for(int i = 1; i<= n; i++){
				if(get(i) == maxDist){
					minNode = Math.min(minNode, i);
				}
			}
			
			out.println(String.format("Case %d: The longest path from %d has length %d, finishing at %d.", test, s, maxDist, minNode));
			out.println();
			test++;
		}
		
		out.close();
	}
	
	static int get(int v){
		if(dp[v] != -2) return dp[v];
		dp[v] = -1;
		for(int from : inedges[v]){
			int prev = get(from);
			if(prev != -1)
				dp[v] = Math.max(dp[v], 1+get(from));
		}
		return dp[v];
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
