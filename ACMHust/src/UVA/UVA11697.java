package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class UVA11697 {
	static int R = 0;
	static int C = 1;
	static int r, c;
	
	//cypher['b'-'a'][R] stores the row of char b
	static int[][] cypher;
	static char[][] cyph;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		cypher = new int[26][2];
		cyph = new char[5][5];
		int T = Integer.parseInt(reader.readLine());
		for(int t = 0 ; t < T ; t++){
			r = 0;
			c = 0;
			Set<Character> used = new HashSet<Character>();
			used.add('q');
			String cypherKey = reader.readLine().toLowerCase();
			
			for(int i = 0; i < cypherKey.length(); i++){
				char c = cypherKey.charAt(i);
				if(c > 'z' || c < 'a'){
					continue;
				}
				
				if(!used.contains(c)){
					fillCypher(c);
					used.add(c);
				}
			}
			
			for(char ch = 'a'; ch <= 'z'; ch++){
				if(!used.contains(ch)){
					fillCypher(ch);
				}
			}
		
			
			StringTokenizer tk = new StringTokenizer( reader.readLine().toLowerCase() );
			StringBuffer target = new StringBuffer();
			while(tk.hasMoreTokens()){
				target.append(tk.nextToken());
			}			
			
			
			StringBuffer res = new StringBuffer();
			int i = 0;
			while(i < target.length()){
				if(i == target.length() - 1){
					target.append('x');
				}else{
					char first = target.charAt(i);
					char second = target.charAt(i+1);
					
					boolean sameRow = cypher[first-'a'][R] == cypher[second-'a'][R];
					boolean sameCol = cypher[first-'a'][C] == cypher[second-'a'][C];
					if(sameRow && sameCol){
						target.insert(i+1, 'x');
					}else if(sameRow && !sameCol){
						res.append(nextRight(first));
						res.append(nextRight(second));
						i+=2;
					}else if(!sameRow && sameCol){
						res.append(nextBelow(first));
						res.append(nextBelow(second));
						i+=2;
					}else{
						res.append(getCharSameRow(cypher[second-'a'][C], first));
						res.append(getCharSameRow(cypher[first-'a'][C], second));
						i+=2;
					}
				}
			}
			out.println(res.toString().toUpperCase());
		}
		
		out.close();
	}
	
	static char getCharSameRow(int col, char ch){
		return cyph[cypher[ch-'a'][R]][col];
	}
	
	static char nextBelow(char ch){
		return cyph[(cypher[ch-'a'][R]+1)%5][cypher[ch-'a'][C]];
	}
	
	static char nextRight(char ch){
		return cyph[cypher[ch-'a'][R]][(cypher[ch-'a'][C]+1)%5];
	}
	
	static void fillCypher(char ch){
		cypher[ch - 'a'][R] = r;
		cypher[ch - 'a'][C] = c;
		cyph[r][c] = ch;
		c++;
		if(c == 5){
			c = 0;
			r++;
		}
	}
}
