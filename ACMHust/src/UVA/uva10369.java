package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;


public class uva10369 {
	static int S;
	static int P;
	static double MAXD = Math.sqrt(2*10000*10000);
	static double[][] coords;
	static double[][] dist;
	static double m;
	//static List<Integer>[] outs;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		int TT = Integer.parseInt(in.readLine());
		for(int test = 1; test <= TT; test++){
			tk = tk (in.readLine());
			S = Integer.parseInt(tk.nextToken());
			P = Integer.parseInt(tk.nextToken());
			
			coords = new double[P][2];
			for(int i = 0; i < P; i++){
				tk = tk (in.readLine());
				coords[i][0] = Double.parseDouble(tk.nextToken());
				coords[i][1] = Double.parseDouble(tk.nextToken());
			}

			dist = new double[P][P];
			MAXD = 0.0;
			for(int i = 0; i < P; i++){
				for(int j = 0; j < P; j++){
					double dx = coords[i][0] - coords[j][0];
					double dy = coords[i][1] - coords[j][1];
					dist[i][j]= Math.sqrt(dx*dx+dy*dy);
					MAXD = Math.max(dist[i][j], MAXD);
				}	
			}
			//outs = (List<Integer>[]) Array.newInstance(List.class, P);
			double left = 0.0;
			double right = MAXD;
			while(right-left > 0.0001){
				m = (left+right)/2;
				if(check()){
					right = m;
				}else{
					left = m;
				}
			}
			out.println(String.format("%.2f", left));
		}
		
		out.close();
	}
	
	static int countcomp = 0;
	static boolean[] isComp;
	
	static boolean check(){
		countcomp = 0;
		isComp = new boolean[P];
		for(int vi = 0; vi < P; vi++){
			if(!isComp[vi]){
				dfs(vi);
				countcomp++;
			}
		}
		return countcomp <= S;
	}
	
	static void dfs(int vi){
		isComp[vi] = true;
		for(int vout = 0; vout<P; vout++){
			if(!isComp[vout] && dist[vi][vout] <= m){
				dfs(vout);
			}
		}
	}
	
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
