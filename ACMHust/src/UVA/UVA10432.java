package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;


public class UVA10432 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		String line = in.readLine();
		while(line != null){
			StringTokenizer tk = new StringTokenizer(line);
			double r = Double.parseDouble(tk.nextToken());
			int n = Integer.parseInt(tk.nextToken());
			double a = 1.0 * (n-2) * Math.PI/ n;
			out.printf("%.3f\n", 0.5*Math.sin(a)*r*r*n);
			line = in.readLine();
		}
		
		out.close();
	}
}
