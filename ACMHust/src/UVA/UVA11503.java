package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class UVA11503 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			Map<String, Integer> id = new HashMap<String, Integer>();
			int idCount = 0;
			int F = Integer.parseInt(in.readLine());
			DisjointSet dj = new DisjointSet(2*F);
			for(int i = 0; i < F; i++){
				StringTokenizer tk = new StringTokenizer(in.readLine());
				String it1 = tk.nextToken();
				String it2 = tk.nextToken();
				if(!id.containsKey(it1)){
					id.put(it1, idCount);
					idCount++;
				}
				if(!id.containsKey(it2)){
					id.put(it2, idCount);
					idCount++;
				}
				dj.union(id.get(it1), id.get(it2));
				out.println(dj.getSize(id.get(it1)));
			}			
		}
		
		out.close();
	}
	
	public static class DisjointSet{
		int[] parent;
		int[] size;
		
		public DisjointSet(int s){
			parent = new int[s];
			size = new int[s];
			for(int i = 0; i < s; i++){
				parent[i] = i;
				size[i] = 1;
			}
		}
		
		public int getLeader(int x){
			if(parent[x] == x){
				return x;
			}
			parent[x] = getLeader(parent[x]);
			return parent[x];
		}
		
		public int getSize(int x){
			return size[getLeader(x)];
		}
		
		public boolean union(int x, int y){
			x = getLeader(x);
			y = getLeader(y);
			if(x == y){
				return true;
			}
			//y as root
			if(size[x] < size[y]){
				int temp = x;
				x = y;
				y = temp;
			}
			
			parent[x] = y;
			size[y] += size[x];
			return false;
		}
	}
}
