package UVA;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class uva12247 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int[] a = new int[3];
			int[] b = new int[3];
			a[0] = Integer.parseInt(tk.nextToken());
			a[1] = Integer.parseInt(tk.nextToken());
			a[2] = Integer.parseInt(tk.nextToken());
			b[0] = Integer.parseInt(tk.nextToken());
			b[1] = Integer.parseInt(tk.nextToken());
			
			if(a[0] == 0)break;
			
			boolean[] used = new boolean[53];
			used[a[0]] = true;
			used[a[1]] = true;
			used[a[2]] = true;
			used[b[0]] = true;
			used[b[1]] = true;
			int best = -1;
			for(int i=1; i<=52;i++){
				if(used[i])continue;
				b[2] = i;
				if(isGood(a,b)){
					best = i;
					break;
				}
			}
			out.println(best);
		}
		out.close();
	}

	static int[][] orders = new int[][]{{0,1,2}, {0,2,1}, {1,0,2}, {1,2,0}, {2,0,1}, {2,1,0}};
	static boolean isGood(int[] a, int[] b){
		for(int[] order : orders){
			int count = 0;
			for(int i=0; i< 3; i++){
				if(b[order[i]] > a[i]){
					count++;
				}
			}
			if(count < 2){
				return false;
			}
		}
		return true;
	}
}
