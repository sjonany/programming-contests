package UVA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class uva247 {
	static Map<String, Integer> peopleToId;
	static String[] idToPeople;
	static List<Integer>[] edges;
	static List<Integer>[] revedges;
	
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		int test = 1;
		while(true) {
			int n = sc.nextInt();
			int m = sc.nextInt();
			if(n==0 && m==0)break;
			if(test != 1) System.out.println();
			
			edges = new List[n];
			revedges = new List[n];
			peopleToId = new HashMap();
			idToPeople = new String[n];
			
			for(int i=0;i<n;i++){
				edges[i]=new ArrayList(); 
				revedges[i]=new ArrayList();
			}
			
			for(int i=0;i<m;i++) {
				String s1 = sc.next();
				String s2 = sc.next();
				addMapping(s1);
				addMapping(s2);
				int i1 = peopleToId.get(s1);
				int i2 = peopleToId.get(s2);
				edges[i1].add(i2);
				revedges[i2].add(i1);
			}
			
			scc(n);
			List<String>[] comps = new List[compi];
			for(int i=0;i<compi;i++)comps[i] = new ArrayList();
			for(int i=0;i<n;i++) {
				comps[comp[i]].add(idToPeople[i]);
			}
			System.out.println("Calling circles for data set " + test + ":");
			for(List<String> lst : comps){
				for(int i=0;i<lst.size();i++) {
					System.out.print(lst.get(i));
					if(i!=lst.size()-1) {
						System.out.print(", ");
					}
				}
				System.out.println();
			}
			
			test++;
		}
		
	}
	
	static int[] comp;
	static int[] stack;
	static int si;
	static int compi;
	static boolean[] visit;
	
	static void scc(int n) {
		comp = new int[n];
		stack = new int[n];
		visit = new boolean[n];
		si = 0;
		compi = 0;
		for(int i=0;i<n;i++) {
			if(!visit[i])
				dfs(i);
			if(si == n) break;
		}
		
		si--;
		while(si >= 0) {
			//reuse visit[si] = is on stack
			if(visit[stack[si]]) {
				dfs2(stack[si]);
				compi++;
			}
			si--;
		}
	}
	
	static void dfs(int cur) {
		visit[cur] = true;
		for(int vo : edges[cur]) {
			if(!visit[vo]) dfs(vo);
		}
		stack[si++] = cur;
	}
	
	static void dfs2(int cur) {
		visit[cur] = false;
		comp[cur] = compi;
		for(int vo : revedges[cur]) {
			if(visit[vo]) dfs2(vo);
		}
	}
	
	static void addMapping(String s) {
		if(!peopleToId.containsKey(s)) {
			int newId = peopleToId.size();
			peopleToId.put(s, newId);
			idToPeople[newId] = s;
		}
	}
	
}
