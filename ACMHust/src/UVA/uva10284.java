package UVA;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class uva10284 {
	//too lazy to finish
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		int t = 1;
		String line = reader.readLine();
		while(line != null){
			//pieces
			char[][] board = new char[8][8];
			boolean[][] isAttacked = new boolean[8][8];
			int curRow = 0;
			int curCol = 0;
			
			
			for(int i = 0; i < line.length(); i++){
				char c = line.charAt(i);
				if(c == '/'){
					curRow++;
					curCol = 0;
				}else if(Character.isDigit(c)){
					curCol += c-'0';
				}else{
					board[curRow][curCol] = c;
					isAttacked[curRow][curCol] = true;
				}
			}
			
			//TODO: parse
			for(int r = 0; r < 8; r++){
				for(int c = 0; c < 8; c++){
					if(board[r][c] != '\u0000'){
						switch(Character.toLowerCase(board[r][c])){
							//cas
						}
					}
				}
			}
			
			for(int r = 0; r < 8; r++){
				for(int c = 0; c < 8; c++){
					if(board[r][c] != ' '){
						switch(Character.toLowerCase(board[r][c])){
							//cas
						}
					}
				}
			}
			
			
			
			line = reader.readLine();
		}
		out.close();
	}
	
	static int[][] moves = new int[][]{{2,1},{1,2},{-1,2},{-2,1},{-2,-1}, {-1, -2} , {1,-2}, {2,-1}};
	static void knight(char[][] board, boolean[][] isAttacked, int r, int c){
		for(int[] move : moves){
			int nextR = move[0] + r;
			int nextC = move[1] + c;
			if(nextR >= 0 && nextR <= 7 && nextC >= 0 && nextC <= 7){
				isAttacked[nextR][nextC] = true;
			}
		}
	}
	
	static void rook(char[][] board, boolean[][] isAttacked, int row, int col){
		for(int c = col+1; c <= 7; c++){
			if(board[row][c] == ' ')
				isAttacked[row][c] = true;
		}
		
		for(int c = col-1; c >= 0; c--){
			if(board[row][c] == ' ')
				isAttacked[row][c] = true;
		}
		
		for(int r = row-1; r >= 0; r--){
			if(board[r][col] == ' ')
				isAttacked[r][col] = true;
		}
		
		for(int r = row+1; r <= 7; r++){
			if(board[r][col] == ' ')
				isAttacked[r][col] = true;
		}
	}
	
	static int[][] dir = new int[][]{{1,1},{1,-1},{-1,1},{-1,-1}};
	static void bishop(char[][] board, boolean[][] isAttacked, int row, int col){
		for(int[] move : dir){
			int r = move[0] + row;
			int c = move[1] + col;
			while(r >= 0 && c >= 0 && r <= 7 && c <= 7 &&
					board[r][c] == ' '){
				isAttacked[r][c] = true;
				r += move[0];
				c += move[1];
			}
		}
	}
}
