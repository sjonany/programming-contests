package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class UVA11692 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(reader.readLine());
		for(int t = 0; t < T; t++){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			double L = Double.parseDouble(tk.nextToken());
			double K = Double.parseDouble(tk.nextToken());
			double T1 = Double.parseDouble(tk.nextToken());
			double T2 = Double.parseDouble(tk.nextToken());
			double H = Double.parseDouble(tk.nextToken());
			
			if(H < L){
				p(H,H, out);
			}else if(H == L){
				double b = -K*(T1+T2) - H;
				double max = (-b + Math.sqrt(b*b-4*T1 * K*L))/2/T1;
				p(H, max*T1, out);
			}else{
				double b = -K*(T1+T2) - H;
				double max = (-b + Math.sqrt(b*b-4*T1 * K*L))/2/T1;
				p(max*T1, max*T1, out);
			}
		}
		
		out.close();
	}
	
	static void p(double min, double max, PrintWriter out){
		out.printf("%.6f %.6f \n", min, max);
	}
}
