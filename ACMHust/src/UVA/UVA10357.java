package UVA;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class UVA10357 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int numPlayer = Integer.parseInt(in.readLine().substring(8));
		int X = 0;
		int Y = 1;
		int V = 2;
		double[][] players = new double[numPlayer][3];

		for(int i = 0; i < numPlayer; i++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			for(int j=0;j<3;j++){
				players[i][j] = Double.parseDouble(tk.nextToken());
			}
		}

		int numBalls = Integer.parseInt(in.readLine().substring(6));
		for(int i= 0 ;i <  numBalls;i++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			long a = Long.parseLong(tk.nextToken());
			long b = Long.parseLong(tk.nextToken());
			long c = Long.parseLong(tk.nextToken());
			long d = Long.parseLong(tk.nextToken());
			long e = Long.parseLong(tk.nextToken());
			long f = Long.parseLong(tk.nextToken());
			long g = Long.parseLong(tk.nextToken());

			//find t
			List<Long> candidates = new ArrayList<Long>();
			candidates.add(1L);
			if(a!=0){
				double root1 = (-b + Math.sqrt(b*b-4*a*c))/2/a;
				double root2 = (-b - Math.sqrt(b*b-4*a*c))/2/a;
				candidates.add((long) Math.floor(root1));
				candidates.add((long) Math.ceil(root1));
				candidates.add((long) Math.floor(root2));
				candidates.add((long) Math.ceil(root2));
			}
			Collections.sort(candidates);
			long T = -1;
			for(long candidate : candidates){
				if(candidate > 0 && a*candidate*candidate +b*candidate+c <= 0){
					T = candidate;
					break;
				}
			}

			long x = d*T+e;
			long y = f*T+g;
			String status = "";

			if(x<0 || y < 0){
				status = "foul";
			}else{
				for(int player =0 ; player < numPlayer; player++){
					double dist = Point2D.distance(x, y, players[player][X], players[player][Y]);
					double time = dist / players[player][V];
					if(time <= T){
						status = "caught";
						break;
					}
				}

				if(status.length() == 0){
					status = "safe";
				}
			}

			out.printf("Ball %d was %s at (%d,%d)\n", i+1, status, x, y);
		}
		out.close();
	}
}
