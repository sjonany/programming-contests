package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class UVA350 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int Z = Integer.parseInt(tk.nextToken());
			int I = Integer.parseInt(tk.nextToken());
			int M = Integer.parseInt(tk.nextToken());
			int L = Integer.parseInt(tk.nextToken());
			if(Z==0&&I==0&&M==0&&L==0){
				break;
			}
			int[] mod = new int[M];
			int now = 1;
	        mod[L] = now;
	        int length;
	        while(true){
	            now++;
	            int next = (Z * L + I) % M;
	            if(mod[next] > 0){
	            	length = now - mod[next];
	                break;
	            }
	            mod[next] = now;
	            L = next;
	        }
			out.printf("Case %d: %d\n", t,length);
			t++;
		}
		
		out.close();
	}
}