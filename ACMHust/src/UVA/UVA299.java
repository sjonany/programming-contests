package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class UVA299 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			int L = Integer.parseInt(in.readLine());
			int[] arr = new int[L];
			StringTokenizer tk = new StringTokenizer(in.readLine());
			for(int i = 0; i<L; i++){
				arr[i] = Integer.parseInt(tk.nextToken());
			}
			int numInv = 0;
			
			for(int i = 0; i < L; i++){
				for(int j = i+1; j < L; j++){
					if(arr[i] > arr[j]){
						numInv++;
					}
				}
			}
			
			out.printf("Optimal train swapping takes %d swaps.\n",numInv);
		}
		
		out.close();
	}
}
