package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class uva227 {
	static char ABOVE = 'A';
	static char BELOW = 'B';
	static char LEFT = 'L';
	static char RIGHT = 'R';
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		int test = 1;
		boolean first = true;
		while(true){
			char[][] board = new char[5][5];
			boolean end = false;
			int er = -1;
			int ec = -1;
			for(int r = 0; r < 5; r++){
				String line = in.readLine();
				if(r==0 && line.trim().equals("Z")){
					end=true;
					break;
				}
				for(int c=0;c<5;c++){
					board[r][c] = line.charAt(c);
					if(board[r][c] == ' '){
						er=r;
						ec=c;
					}
				}
			}
			
			if(end)break;

			if(first){
				first = false;
			}else{
				out.println();
			}
			
			boolean good = true;
			while(true){
				boolean done = false;
				String command = in.readLine();
				for(int i =0 ; i < command.length(); i++){
					char co = command.charAt(i);
					if(co == '0'){
						done = true;
						break;
					}
					if(!good)continue;
					if(co==ABOVE){
						if(er == 0){
							good = false;
							continue;
						}
						board[er][ec] = board[er-1][ec];
						board[er-1][ec] = ' ';
						er--;
					}else if(co==BELOW){
						if(er == 4){
							good = false;
							continue;
						}
						board[er][ec] = board[er+1][ec];
						board[er+1][ec] = ' ';
						er++;
					}else if(co==LEFT){
						if(ec == 0){
							good = false;
							continue;
						}
						board[er][ec] = board[er][ec-1];
						board[er][ec-1] = ' ';
						ec--;
					}else{
						//RIGHT
						if(ec==4){
							good = false;
							continue;
						}
						board[er][ec] = board[er][ec+1];
						board[er][ec+1] = ' ';
						ec++;
					}
				}
				if(done)break;
			}
			out.println(String.format("Puzzle #%d:", test));
			if(good){
				for(int r = 0; r < 5; r++){
					for(int c = 0; c < 5; c++){
						out.print(board[r][c]);
						if(c!=4){
							out.print(" ");
						}
					}
					out.println();
				}
			}else{
				out.println("This puzzle has no final configuration.");
			}
			test++;
		}
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
