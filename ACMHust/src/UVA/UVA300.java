package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class UVA300 {
	static List<String> months2;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		String monthStr = "pop, no, zip, zotz, tzec, xul, yoxkin, mol, chen, yax, zac, ceh, mac, kankin, muan, pax, koyab, cumhu,";
		StringTokenizer tk = new StringTokenizer(monthStr);
		List<String> months = new ArrayList<String>();
		while(tk.hasMoreTokens()){
			String tok = tk.nextToken();
			months.add(tok.substring(0,tok.length()-1));
		}
		String monthsStr2 = "imix, ik, akbal, kan, chicchan, cimi, manik, lamat, muluk, ok, chuen, eb, ben, ix, mem, cib, caban, eznab, canac, ahau,";
		tk = new StringTokenizer(monthsStr2);
		months2 = new ArrayList<String>();
		while(tk.hasMoreTokens()){
			String tok = tk.nextToken();
			months2.add(tok.substring(0,tok.length()-1));
		}
		
		int TEST = Integer.parseInt(in.readLine());
		out.println(TEST);
		for(int test = 0; test < TEST; test++){
			tk = new StringTokenizer(in.readLine());
			String tok = tk.nextToken();
			int numDay = Integer.parseInt(tok.substring(0,tok.length()-1));
			String month = tk.nextToken();
			int year = Integer.parseInt(tk.nextToken());
			int numMonth = months.indexOf(month);
			
			int extraDay = 0;
			if(numMonth == -1){
				extraDay = 18*20;
			}else{
				extraDay = numMonth * 20;
			}
			int totDay = year * 365 + numDay + extraDay;
			out.println(getAns(totDay));
			
		}
		
		out.close();
	}
	
	static String getAns(int day){
		int numDayInYear = 13 * 20;
		int year2 = day / numDayInYear;
		day %= numDayInYear;
		
		int num = 0;
		int ind = 0;
		while(day > 0){
			num = (num+1) % 13;
			ind = (ind+1) % 20;
			day--;
		}
		
		return (num+1) + " " + months2.get(ind) + " " + year2;
	}
}
