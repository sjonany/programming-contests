package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;


public class uva10000b {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		int test = 1;
		while(true){
			int n = Integer.parseInt(in.readLine());
			//max distance from s to i, -1 if not possible
			int[] dp = new int[n];
			Arrays.fill(dp, -2);
			if(n==0)break;
			List<Integer>[] outedges = (List<Integer>[]) Array.newInstance(List.class, n);
			List<Integer>[] inedges = (List<Integer>[]) Array.newInstance(List.class, n);
			for(int i= 0; i < n; i++){
				outedges[i] = new ArrayList<Integer>();
				inedges[i] = new ArrayList<Integer>();
			}
			
			int s = Integer.parseInt(in.readLine());
			s--;
			while(true){
				tk = tk(in.readLine());
				int from = Integer.parseInt(tk.nextToken());
				int to = Integer.parseInt(tk.nextToken()); 
				if(from==0 && to==0)break;
				from--;
				to--;
				outedges[from].add(to);
				inedges[to].add(from);
			}
			
			Queue<Integer> order = topologicalSort(outedges, n);
			
			Arrays.fill(dp, -1);
			while(!order.isEmpty()){
				int v = order.poll();
				if(v == s){
					dp[v] = 0;
				}else{
					for(int vi : inedges[v]){
						int prev = dp[vi];
						if(prev != -1){
							dp[v] = Math.max(dp[v], 1 + dp[vi]);
						}
					}
				}
			}
			
			int maxDist = 0;
			for(int i = 0; i< n; i++){
				maxDist = Math.max(maxDist, dp[i]);
			}

			int minNode = Integer.MAX_VALUE;
			for(int i = 0; i < n; i++){
				if(dp[i] == maxDist){
					minNode = Math.min(minNode, i);
				}
			}
			
			out.println(String.format("Case %d: The longest path from %d has length %d, finishing at %d.", test, s+1, maxDist, minNode+1));
			out.println();
			test++;
		}
		
		out.close();
		
		out.close();
	}
	
	//return null if exist cycle
	static Queue<Integer> topologicalSort(List<Integer>[] outEdges, int n){
		int[] inCount = new int[n];
		for(int i = 0; i < outEdges.length; i++){
			for(int vo : outEdges[i]){
				inCount[vo]++;
			}
		}
		
		Queue<Integer> noIncoming = new LinkedList<Integer>();
		for(int i = 0; i < n; i++){
			if(inCount[i] == 0){
				noIncoming.add(i);
			}
		}
		
		Queue<Integer> order = new LinkedList<Integer>();
		while(!noIncoming.isEmpty()){
			int v = noIncoming.remove();
			order.add(v);
			for(int vo : outEdges[v]){
				inCount[vo]--;
				if(inCount[vo] == 0){
					noIncoming.add(vo);
				}
			}
		}
		
		if(order.size() == n){
			return order;
		}else{
			return null;
		}
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
