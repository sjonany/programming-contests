package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class uva10032 {
	static BufferedReader in;
	static PrintWriter out;

	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0; test < TEST; test++) {
			in.readLine();
			int[] arr = new int[Integer.parseInt(in.readLine())];
			for(int i = 0; i <  arr.length; i++) {
				arr[i] = Integer.parseInt(in.readLine());
			}
			
			solve(arr);
			if(test != TEST-1)out.println();
		}
		
		out.close();
	}
	
	static void solve(int[] arr) {
		//boolean[][][] dp = new boolean[arr.length][45000][arr.length];
		//dp[i][j][k] = true iff consider arr[0...i], can make value of j with k people
		//dp[i][j][k] = dp[i-1][j][k] | dp[i-1][j-a[i]][k-1]
		int numPeople = arr.length / 2;
		int sum = 0;
		for(int w : arr){
			sum += w;
		}
		
		long[] dp = new long[sum+1];
		//dp*[i][j][k] = (dp[i][j] >> K) & 1;
		
		dp[0] = 1;
		
		for(int i = 0; i < arr.length; i++) {
			// this way, i preserve the [j-a[i]] state
			for(int val = sum; val >= 0; val--) {
				int nval = val - arr[i];
				if(nval < 0) break;
				//it's ok to overflow. we only care about at most 50 people
				dp[val] |= (dp[nval] << 1);
			}
		}
		
		int bestVal = -1;
		int minDiff = Integer.MAX_VALUE;
		for(int val = 0; val <= sum; val++) {
			int otherVal = sum - val;
			if(((dp[val] >> numPeople) & 1) != 0) {
				int diff = Math.abs(val- otherVal);
				if(diff < minDiff){
					minDiff = diff;
					bestVal = val;
				}
			}
		}
		
		if(bestVal > sum - bestVal) {
			bestVal = sum - bestVal;
		}
		out.println(bestVal + " " + (sum - bestVal));
	}
}
