package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class UVA11693 {
	static int N;
	static int[][] adjMap;
	static int[][] d;
	static boolean[] isExit;
	static int b;
	static int p;
	
	static double MAX_SPEED = 100.0 * 100.0 * 200.0;
	
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		adjMap = new int[101][101];
		d = new int[101][101];
		isExit = new boolean[101];
		
		int T = Integer.parseInt(reader.readLine());
		for(int t = 0; t < T; t++){
			for(int i = 0; i < 101; i++){
				for(int j = 0; j < 101; j++){
					adjMap[i][j] = Integer.MAX_VALUE;
					d[i][j] = Integer.MAX_VALUE;
				}
				adjMap[i][i] = 0;
				d[i][i] = 0;
			}
			for(int i = 0; i < 101; i++){
				isExit[i] = false;
			}
			
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			N = Integer.parseInt(tk.nextToken());
			int M = Integer.parseInt(tk.nextToken());
			int E = Integer.parseInt(tk.nextToken());
			
			for(int i = 0; i < M; i++){
				tk = new StringTokenizer(reader.readLine());
				int a = Integer.parseInt(tk.nextToken());
				int b = Integer.parseInt(tk.nextToken());
				int l = Integer.parseInt(tk.nextToken());
				adjMap[a][b] = l;
				adjMap[b][a] = l;
				d[a][b] = l;
				d[b][a] = l;
			}
			
			tk = new StringTokenizer(reader.readLine());
			for(int i = 0; i < E; i++){
				isExit[Integer.parseInt(tk.nextToken())] = true;
			}
			
			tk = new StringTokenizer(reader.readLine());
			b = Integer.parseInt(tk.nextToken());
			p = Integer.parseInt(tk.nextToken());
			
			for(int k = 0; k < 101; k++){
				for(int i = 0; i < 101; i++){
					for(int j = 0; j < 101; j++){
						if(d[i][k] != Integer.MAX_VALUE && d[k][j] != Integer.MAX_VALUE ){
							int newVal = d[i][k] + d[k][j];
							if(newVal < d[i][j]){
								d[i][j] = newVal;
							}
						}
					}
				}
			}
			
			double l = 0.0;
			double r = MAX_SPEED;
			boolean good = true;
			while(r - l > 0.0000001){
				double m = (r+l)/2.0;
				if(canEscape(m)){
					r = m;
				}else{
					l = m;
				}
				if(l > 1600000){
					good = false;
					break;
				}
			}
			
			if(!good){
				out.println("IMPOSSIBLE");
			}else{
				out.println(l);
			}
		}
		out.close();
	}
	
	static boolean canEscape(double speed){
		int[] distance = new int[N+1];
		for(int i = 0; i < distance.length; i++){
			distance[i] = Integer.MAX_VALUE;
		}
		
		distance[b] = 0;
		boolean[] known = new boolean[N+1];
		
		while(true){
			int next = -1;
			for(int i = 1; i <= N; i++){
				if(known[i] || distance[i] == Integer.MAX_VALUE){
					continue;
				}
				
				if(next == -1 || distance[i] < distance[next]){
					next = i;
				}
			}
			
			if(next == -1)break;
			
			for(int out = 1; out <= N ;out++){
				if(adjMap[next][out] != Integer.MAX_VALUE){
					int newDist = distance[next] + adjMap[next][out];
					if(newDist / speed < d[p][out] / 160.0){
						if(isExit[out]) return true;
						//by induction, exist path from cloud to this node that all consist of uninterceptable nodes
						distance[out] = Math.min(distance[out], newDist);
					}
					//if not, then don't extend path. we want our path to consist of purely uninterceptable nodes
				}
			}
			
			known[next] = true;
		}
		
		return false;
	}
}