package UVA;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class uva12279 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		int t = 1;
		while(true){
			int N = Integer.parseInt(reader.readLine().trim());
			if(N==0)break;
			int c = 0;
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			for(int i=0;i<N;i++){
				if(Integer.parseInt(tk.nextToken()) == 0)c++;
			}
			out.println("Case " + t + ": " + (N-2*c));
			t++;
		}
		out.close();
	}
}
