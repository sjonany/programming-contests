package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;


public class uva477 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		List<Rec> recs = new ArrayList<Rec>();
		List<Circle> cirs = new ArrayList<Circle>();

		int id = 1;
		while(true){
			String line = in.readLine();
			if(line.startsWith("*"))break;
			tk = tk(line);
			char shape = tk.nextToken().charAt(0);
			if(shape=='r'){
				Rec r = new Rec(id, Double.parseDouble(tk.nextToken()),
						Double.parseDouble(tk.nextToken()),
						Double.parseDouble(tk.nextToken()), 
						Double.parseDouble(tk.nextToken()));
				recs.add(r);
			}else{
				Circle c = new Circle(id, Double.parseDouble(tk.nextToken()),
						Double.parseDouble(tk.nextToken()),
						Double.parseDouble(tk.nextToken()));
				cirs.add(c);
			}
			id++;
		}
		
		int test = 1;
		while(true){
			String line = in.readLine();
			if(line.trim().equals("9999.9 9999.9")){
				break;
			}
			
			tk = tk(line);
			double x = Double.parseDouble(tk.nextToken());
			double y = Double.parseDouble(tk.nextToken());
			List<Integer> ids = new ArrayList<Integer>();
			for(Rec r : recs){
				if(r.contains(x, y)){
					ids.add(r.id);
				}
			}	
			for(Circle r : cirs){
				if(r.contains(x, y)){
					ids.add(r.id);
				}
			}
			Collections.sort(ids);
			
			if(ids.isEmpty()){
				out.println(String.format("Point %d is not contained in any figure",test));
			}else{
				for(int idd : ids){
					out.println(String.format("Point %d is contained in figure %d",test,idd));
				}
			}
			test++;
		}

		out.close();
	}

	static class Rec{
		public int id;
		public double ulx;
		public double uly;
		public double brx;
		public double bry;

		public Rec(int id, double ulx, double uly, double brx, double bry) {
			this.id = id;
			this.ulx = ulx;
			this.uly = uly;
			this.brx = brx;
			this.bry = bry;
		}

		public boolean contains(double x, double y){
			return x > ulx && x < brx && y < uly && y > bry;
		}
	}

	static class Circle{
		public int id;
		public double cx;
		public double cy;
		public double r;

		public Circle(int id, double cx, double cy, double r) {
			this.id = id;
			this.cx = cx;
			this.cy = cy;
			this.r = r;
		}

		public boolean contains(double x, double y){
			double dx = x-cx;
			double dy = y-cy;
			return dx*dx+dy*dy < r*r;
		}
	}

	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
