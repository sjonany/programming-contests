package UVA;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class uva12086 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		int t = 1;
		while(true){
			int N = Integer.parseInt(reader.readLine());
			if(N == 0)break;
			if(t>1) System.out.println();
			long[] val = new long[N];
			for(int i=0;i<N;i++){
				val[i] = Integer.parseInt(reader.readLine());
			}
			
			BIT tree = new BIT(val);
			String line = reader.readLine();
			StringBuffer out = new StringBuffer();
			while(!line.equals("END")){
				StringTokenizer tk = new StringTokenizer(line);
				if(tk.nextToken().equals("S")){
					int x = Integer.parseInt(tk.nextToken());
					int r = Integer.parseInt(tk.nextToken());
					tree.increment(x, r-val[x-1]);
					val[x-1] = r;
				}else{
					int x = Integer.parseInt(tk.nextToken());
					int y = Integer.parseInt(tk.nextToken());
					out.append(tree.getTotalFreq(x, y) + "\n");
				}
				line = reader.readLine();
			}
			System.out.println("Case " + t + ":");
			System.out.print(out);
			t++;
		}
	}

	//index is 1-based, not 0-based
	static class BIT{
		long[] tree;
		public BIT(int n){
			tree = new long[n+1];
		}
		
		//initArray[] is 0-based
		public BIT(long[] initArray){
			tree = new long[initArray.length+1];
			
			for(int i=0;i<initArray.length;i++){
				increment(i+1, initArray[i]);
			}
		}
		
		public void increment(int idx, long val){
			while(idx < tree.length){
				tree[idx] += val;
				idx += (idx & -idx);
			}
		}
		
		public long getTotalFreq(int from, int to){
			return getTotalFreq(to) - getTotalFreq(from-1);
		}
		
		public long getTotalFreq(int idx){
			long sum = 0;
			while(idx > 0){
				sum += tree[idx];
				idx -= (idx & -idx);
			}
			return sum;
		}
	}
}
