package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


//cool solution inspired by http://codeforces.com/blog/entry/2509
//actually can be done in O(n) if count inversion using cartesian tree
public class UVA10716Best {
	static PrintWriter out;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		int n = Integer.parseInt(in.readLine());
		for(int i = 0;  i < n; i++){
			solve(in.readLine());
		}
		
		out.close();
	}
	
	static void solve(String input){
		//check if can be palindrome
		int[] count = new int[26];
		for(int i = 0; i < input.length(); i++){
			count[input.charAt(i)-'a']++;
		}
		
		int numOdd = 0;
		for(int i = 0 ; i < 26; i++){
			if(count[i] % 2 == 1){
				numOdd++;
			}
		}
		
		if(numOdd > 1){
			out.println("Impossible");
			return;
		}
		
		//each character is either 0,1, or 2
		//0 means it's the left counter part to the same character label with 2
		//1 means it's in the middle. There can be at most one 1
		int[] partition = new int[input.length()];
		List<Integer>[] charIndices = (List<Integer>[]) Array.newInstance(List.class, 26);
		
		for(int i = 0; i < charIndices.length; i++){
			charIndices[i] = new ArrayList<Integer>();
		}
		
		for(int i = 0 ; i< input.length(); i++){
			int ch = input.charAt(i) - 'a';
			charIndices[ch].add(i);
		}
		
		for(int ch = 0; ch < 26; ch++){
			int left = 0;
			int right = charIndices[ch].size()-1;
			while(right >= left){
				if(left == right){
					partition[charIndices[ch].get(left)] = 1;
					break;
				}else{
					partition[charIndices[ch].get(left)] = 0;
					partition[charIndices[ch].get(right)] = 2;
					right--;
					left++;
				}
			}
		}
		
		//convert partition to  0...12..., which is first step to the transformation
		int firstStepCost = countInv(partition);
		
		//now string is X O Y form. We want Y' O Y.
		List<Integer> left = new ArrayList<Integer>();
		List<Integer> right = new ArrayList<Integer>();
		for(int i = 0 ; i < partition.length; i++){
			if(partition[i] == 0){
				left.add(i);
			}else if(partition[i] == 2){
				right.add(i);
			}
		}
		
		Collections.reverse(right);
		
		//now question is min adj swap to transform left -> right
		int[] arrLeft = new int[left.size()];
		//if right = abdab
		//and left = bdaba
		//right's representation = 12345
		//for left, even with duplicate, it's best to place leftmost occurrence with that number
		//when our goal is to minimize # inversion
		//so left = 23154
		Queue<Integer>[] charLocs = (Queue<Integer>[]) Array.newInstance(Queue.class, 26);
		for(int i = 0; i < 26; i++){
			charLocs[i] = new LinkedList<Integer>();
		}
		
		for(int i = 0; i < right.size(); i++){
			int ch = input.charAt(right.get(i)) - 'a';
			charLocs[ch].add(i);
		}
		
		for(int i = 0; i < left.size(); i++){
			int ch = input.charAt(left.get(i)) - 'a';
			arrLeft[i] = charLocs[ch].poll();
		}
		
		out.println(firstStepCost + countInv(arrLeft));
	}
	
	static int countInv(int[] arr){
		return countInv(arr.clone(), 0 , arr.length-1);
	}
	
	static int countInv(int[] arr, int left, int right){
		if(left >= right){
			return 0;
		}
		int mid = (left + right)/2;
		int leftInv = countInv(arr, left, mid);
		int rightInv = countInv(arr, mid+1, right);
		int combInv = 0;
		
		int i = left;
		int j = mid+1;
		int tempI = 0;
		int[] temp = new int[right-left+1];
		while(true){
			if(arr[i] > arr[j]){
				temp[tempI++] = arr[j];
				j++;
			}else{
				combInv += j-mid-1;
				temp[tempI++] = arr[i];
				i++;
			}
			
			if(i == mid+1 || j == right+1){
				break;
			}
		}
		
		while(i <= mid){
			temp[tempI++] = arr[i];
			combInv += j-mid-1;
			i++;
		}
		
		while(j <= right){
			temp[tempI++] = arr[j];
			j++;
		}
		
		for(int ii = 0; ii < temp.length; ii++){
			arr[ii+left] = temp[ii];
		}
		
		return leftInv + rightInv + combInv;
	}
	
}
