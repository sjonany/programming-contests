package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class uva357 {
	static int[] CHANGES = {1,5,10,25,50};

	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		//num ways to construct i if considering changes[0-j]
		long[][] dp = new long[30001][CHANGES.length];
		dp[0][0] = 1;
		dp[0][1] = 1;
		dp[0][2] = 1;
		dp[0][3] = 1;
		dp[0][4] = 1;

		for(int change = 0; change < CHANGES.length; change++){
			if(change==0){
				for(int v = 1; v < dp.length; v++){
					dp[v][change] = 1;
				}
			}else{
				for(int v = 1; v < dp.length; v++){
					int numBig = 0;
					while(true){
						int bigAmount = numBig * CHANGES[change];
						if(v - bigAmount < 0) break;
						dp[v][change] += dp[v-bigAmount][change-1];
						numBig++;
					}
				}
			}
		}
		
		while(true){
			String line = in.readLine();
			if(line == null) break;
			if(line.trim().length() == 0)break;
			int q = Integer.parseInt(line.trim());
			long ans = dp[q][CHANGES.length-1];
			if(ans != 1){
				out.println(String.format("There are %d ways to produce %d cents change.", ans, q));
			}else{
				out.println(String.format("There is only 1 way to produce %d cents change.", q));
			}
		}

		out.close();
	}

	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
