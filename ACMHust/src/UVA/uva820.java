package UVA;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class uva820 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		int te = 1;
		while(line != null){
			int n = Integer.parseInt(line);
			if(n==0){
				break;
			}
			int[][] cap = new int[n][n];
			List<Integer>[] outEdges = (List<Integer>[]) Array.newInstance(List.class,n);
			for(int i=0;i<outEdges.length;i++){
				outEdges[i] = new ArrayList<Integer>();
			}

			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int s = Integer.parseInt(tk.nextToken())-1;
			int t = Integer.parseInt(tk.nextToken())-1;
			int c = Integer.parseInt(tk.nextToken());

			for(int i=0;i<c;i++){
				tk = new StringTokenizer(reader.readLine());
				int v1 = Integer.parseInt(tk.nextToken())-1;
				int v2 = Integer.parseInt(tk.nextToken())-1;
				int capa = Integer.parseInt(tk.nextToken());
				cap[v1][v2] += capa;
				cap[v2][v1] += capa;
				outEdges[v1].add(v2);
				outEdges[v2].add(v1);
			}
			
			int maxFlow = maxFlow(cap, outEdges, n, s, t);
			System.out.println("Network " + te);
			System.out.println("The bandwidth is " + maxFlow + ".");
			System.out.println();

			line = reader.readLine();
			te++;
		}
	}
	
	public static int maxFlow(int[][] caps, List<Integer>[] outEdges, int numNode, int source, int sink){
		int flow = 0;
		while(true){
			int pathCapacity = augmentPath(caps, outEdges, numNode, source, sink);
			if (pathCapacity == 0){
				break;
			}
			
			flow += pathCapacity;
		}
		return flow;
	}
	
	private static void findPathByDfs(int[][] caps, List<Integer>[] outEdges, boolean[] isVisited, int[] from, int cur, int sink){
		if(from[sink] == -1){
			for(Integer next : outEdges[cur]){
				if(!isVisited[next] && caps[cur][next] > 0){
					isVisited[next] = true;
					from[next] = cur;
					if(next == sink){
						return;
					}
					findPathByDfs(caps, outEdges, isVisited, from, next, sink);
				}
			}
		}
	}
	
	private static int augmentPath(int[][] caps, List<Integer>[] outEdges, int numNode, int source, int sink){
		//dfs
		boolean[] isVisited = new boolean[numNode];

		int[] from = new int[numNode];
		for(int i=0;i<from.length;i++){
			from[i] = -1;
		}
		isVisited[source] = true;
		findPathByDfs(caps, outEdges, isVisited, from, source, sink);
		//get min cap along dfs path
		
		//no more augmenting path
		if(from[sink] == -1){
			return 0;
		}
		
		int cur = sink;
		int minCap = Integer.MAX_VALUE;
		while(from[cur] != -1){
			minCap = Math.min(minCap, caps[from[cur]][cur]);
			cur = from[cur];
		}


		if(minCap == Integer.MAX_VALUE){
			//no more augmenting path
			return 0;
		}

		//update residual network

		cur = sink;
		while(from[cur] != -1){
			caps[from[cur]][cur] -= minCap;
			caps[cur][from[cur]] += minCap;
			cur = from[cur];
		}

		return minCap;
	}
}
