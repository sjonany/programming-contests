package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class UVA11689 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(reader.readLine());
		for(int t = 0; t < T; t++){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int e = Integer.parseInt(tk.nextToken());
			int f = Integer.parseInt(tk.nextToken());
			int c = Integer.parseInt(tk.nextToken());
			e += f;
			int count = 0;
			while(e >= c){
				int newBottle = e/c;
				count += newBottle;
				e = e%c;
				e += newBottle;
			}
			out.println(count);
		}
		
		out.close();
	}
}
