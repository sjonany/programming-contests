package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;


public class UVA10487 {
	static List<String> months2;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			int n = Integer.parseInt(in.readLine());
			if(n == 0) break;
			int[] arr = new int[n];
			for(int i= 0; i < n; i ++){
				arr[i] = Integer.parseInt(in.readLine());
			}
			
			int m = Integer.parseInt(in.readLine());
			out.println("Case " + t + ":");
			Arrays.sort(arr);
			for(int i =0; i<m;i++){
				int quer = Integer.parseInt(in.readLine());
				
				int closestSum = arr[0] + arr[1];
				for(int j = 0; j < arr.length; j++){
					int closestOther = Arrays.binarySearch(arr, quer - arr[j]);
					List<Integer> candidates = new ArrayList<Integer>();
					if(closestOther <0) closestOther = -1-closestOther;
					if(closestOther < arr.length)
						candidates.add(closestOther);
					if(closestOther >0){
						candidates.add(closestOther-1);
					}
					if(closestOther < arr.length-1){
						candidates.add(closestOther+1);
					}
					for(int index : candidates){
						if(j== index) continue;
						int sum = arr[j] + arr[index];
						if(Math.abs(closestSum-quer) > Math.abs(sum-quer)){
							closestSum = sum;
						}
					}
				}
				
				
				out.printf("Closest sum to %d is %d.\n", quer, closestSum);
			}
			t++;
		}
		
		out.close();
	}
}
