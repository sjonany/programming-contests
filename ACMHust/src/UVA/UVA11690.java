package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class UVA11690 {
	static int N;
	static int M;
	static List<Integer>[] edges;
	static int[] owe;
	static boolean[] isComp;
	
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		edges = (List<Integer>[]) Array.newInstance(List.class, 10000);
		for(int i = 0; i < edges.length; i++){
			edges[i] = new ArrayList<Integer>();
		}
		owe = new int[10000];
		isComp = new boolean[10000];
		
		int T = Integer.parseInt(reader.readLine());
		
		for(int t = 0; t < T; t++){
			for(int i = 0; i < edges.length; i++){
				edges[i].clear();
			}
			for(int i = 0; i < isComp.length; i++){
				isComp[i] = false;
			}
			
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			N = Integer.parseInt(tk.nextToken());
			M = Integer.parseInt(tk.nextToken());
			
			for(int i = 0; i < N; i++){
				owe[i] = Integer.parseInt(reader.readLine());
			}
			
			for(int i = 0; i < M; i++){
				tk = new StringTokenizer(reader.readLine());
				int a = Integer.parseInt(tk.nextToken());
				int b = Integer.parseInt(tk.nextToken());
				edges[a].add(b);
				edges[b].add(a);
			}
			List<List<Integer>> comps = findConnected();
			boolean good = true;
			for(List<Integer> comp : comps){
				long sum = 0;
				for(Integer person : comp){
					sum += owe[person] ;
				}
				if(sum != 0){
					good = false;
					break;
				}
			}
			
			if(good){
				out.println("POSSIBLE");
			}else{
				out.println("IMPOSSIBLE");
			}
			
		}
		
		out.close();
	}
	
	static List<List<Integer>> findConnected(){
		List<List<Integer>> comps = new ArrayList<List<Integer>>();
		for(int vi = 0; vi < N; vi++){
			if(!isComp[vi]){
				List<Integer> comp = new ArrayList<Integer>();
				dfs(vi, comp);
				comps.add(comp);
			}
		}
		return comps;
	}
	
	static void dfs(int vi, List<Integer> comp){
		comp.add(vi);
		isComp[vi] = true;
		for(int vout : edges[vi]){
			if(!isComp[vout])
				dfs(vout, comp);
		}
	}
}
