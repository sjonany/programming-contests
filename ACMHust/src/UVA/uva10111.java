package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class uva10111 {
	static int[][] board;
	static int X = 0;
	static int O = 1;
	static int E = 2;
	static int LOSE = 0;
	static int DRAW = 1;
	static int WIN = 2;

	static Map<String, Integer> dp;

	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		dp = new HashMap<String, Integer>();
		while(true){
			String line = in.readLine();
			if(line.startsWith("$")){
				break;
			}
			board = new int[4][4];
			for(int r = 0; r < 4; r++){
				line = in.readLine();
				for(int c = 0; c < 4; c++){
					char cc = line.charAt(c);
					int ii=0;
					if(cc=='x')ii=X;
					else if(cc=='o')ii=O;
					else ii=E;
					board[r][c] = ii;
				}	
			}

			boolean found = false;
			int fr = 0;
			int fc = 0;
			for(int r = 0; r < 4; r++){
				for(int c = 0; c < 4; c++){
					if(board[r][c] == E){
						board[r][c] = X;
						if(play(O,X) == LOSE){
							fr = r;
							fc = c;
							found = true;
							break;
						}
						board[r][c] = E;
					}
				}
				if(found)break;
			}

			if(found){
				out.println(String.format("(%d,%d)", fr, fc));
			}else{
				out.println("#####");
			}
		}

		out.close();
	}

	static String key(int p, int[][]board){
		StringBuilder sb = new StringBuilder();
		sb.append(p);
		for(int r = 0; r < board.length; r++){
			for(int c = 0; c < board[0].length; c++){
				sb.append(board[r][c]);
			}
		}
		return sb.toString();
	}

	//true if player's turn to move, what is his best condition?
	static int play(int player, int enemy){
		String k = key(player, board);
		if(dp.containsKey(k)){
			return dp.get(k);
		}
		for(int r = 0; r < 4; r++){
			boolean isRowFull = true;
			for(int c = 0; c < 4 ; c++){
				if(board[r][c] != enemy){
					isRowFull = false;
					break;
				}
			}
			if(isRowFull){
				dp.put(k, LOSE);
				return LOSE;
			}
		}
		for(int c = 0; c < 4; c++){
			boolean isColFull = true;
			for(int r = 0; r < 4 ; r++){
				if(board[r][c] != enemy){
					isColFull = false;
					break;
				}
			}
			if(isColFull){
				dp.put(k, LOSE);
				return LOSE;
			}
		}
		boolean diag1 = true;
		for(int i = 0; i < 4; i++){
			if(board[i][i] != enemy){
				diag1 = false;
				break;
			}
		}
		if(diag1){
			dp.put(k, LOSE);
			return LOSE;
		}

		boolean diag2 = true;
		for(int i = 0; i < 4; i++){
			if(board[i][3-i] != enemy){
				diag2 = false;
				break;
			}
		}
		if(diag2){
			dp.put(k, LOSE);
			return LOSE;
		}
		boolean isDraw = true;
		for(int r = 0; r < 4 ;r++){
			for(int c = 0; c < 4 ; c++){
				if(board[r][c] == E) {
					isDraw = false;
					break;
				}
			}
			if(!isDraw)break;
		}

		if(isDraw){
			dp.put(k, DRAW);
			return DRAW;
		}
		int bestMove = LOSE;
		for(int r = 0; r < 4; r++){
			for(int c = 0; c < 4; c++){
				if(board[r][c] == E){
					board[r][c] = player;
					int enemyCond = play(enemy, player);
					board[r][c] = E;
					if(enemyCond == LOSE){
						dp.put(k, WIN);
						return WIN;
					}else if(enemyCond == DRAW){
						if(bestMove == LOSE){
							bestMove = DRAW;
						}
					}else{
						//enemy wins, skip
					}
				}
			}	
		}

		dp.put(k, bestMove);
		return bestMove;
	}

	static char[] MAP = new char[]{'x','o','e'};
	static String[] MAP2 = new String[]{"LOSE","DRAW","WIN"};
	static void p(){
		for(int r = 0; r < 4; r++){
			for(int c = 0; c < 4; c++){
				System.out.print(MAP[board[r][c]]);
			}
			System.out.println();
		}
		System.out.println();
	}

	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
