package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class uva414 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		while(true){
			int R = Integer.parseInt(in.readLine());
			if(R==0)break;
			int maxMove = Integer.MAX_VALUE;
			int numB = 0;
			for(int i = 0; i < R; i++){
				String s = in.readLine();
				int numGap = 0;
				for(int j = 0; j < s.length(); j++){
					char c = s.charAt(j);
					if(c!='X'){
						numB++;
						numGap++;
					}
				}
				maxMove = Math.min(numGap, maxMove);
			}
			out.println(numB - R * maxMove);
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
