package UVA;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

public class UVA10716 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int TEST = Integer.parseInt(in.readLine());
		for(int t = 1; t<= TEST;t++){
			String str = in.readLine();
			int[] tally = new int[26];
			for(int i = 0; i < str.length();i++){
				tally[str.charAt(i)-'a']++;
			}
			int numOdd= 0;
			for(char ch = 'a' ;ch<='z'; ch++){
				if(tally[ch-'a'] %2 ==1)numOdd++;
			}

			if(numOdd > 1){
				out.println("Impossible");
				continue;
			}
			if(isPal(str)){
				out.println(0);
				continue;
			}
			
			str = reduce(str);
			Set<String> seen = new HashSet<String>();
			
			seen.add(str);
			PriorityQueue<Node> q = new PriorityQueue<Node>();
			q.add(new Node(0,str));

			boolean found = false;
			int bestCost = -1;
			while(!q.isEmpty()){
				Node n = q.poll();
				String s = n.str;
				
				//if left is fixed
				int leftAcc = 0;
				for(int right = s.length()-1; right>=0; right--){
					if(s.charAt(right) == s.charAt(0)){
						leftAcc = right;
						break;
					}
				}
				
				if(leftAcc != 0){
					int cost = n.weight + s.length()-1 - leftAcc;
					
					String newSt = n.str.substring(1, leftAcc) + 
							n.str.substring(leftAcc+1);
					
					newSt = reduce(newSt);
					if(newSt.length()<=1){
						found=true;
						bestCost=cost;
						break;
					}
					if(!seen.contains(newSt)){
						seen.add(newSt);
						q.add(new Node(cost,newSt));
					}
				}
				
				//if right is fixed
				int rightAcc = s.length()-1;
				for(int left = 0; left<s.length();left++){
					if(s.charAt(left) == s.charAt(s.length()-1)){
						rightAcc = left;
						break;
					}
				}
				
				if(rightAcc!= s.length()-1){
					int cost = n.weight + rightAcc;
				
					String newSt = n.str.substring(0, rightAcc) + 
							n.str.substring(rightAcc+1, s.length()-1);
					
					newSt = reduce(newSt);
					if(newSt.length()<=1){
						found=true;
						bestCost=cost;
						break;
					}
					if(!seen.contains(newSt)){
						seen.add(newSt);
						q.add(new Node(cost,newSt));
					}
				}
				if(found)break;
			}
			out.println(bestCost);
		}
		
		out.close();
	}

	static String reduce(String newSt){
		int shell = 0;
		for(int i = 0; i < newSt.length()/2; i++){
			if(newSt.charAt(i) != newSt.charAt(newSt.length()-i-1)){
				shell = i;
				break;
			}
		}
		return newSt.substring(shell, newSt.length()-shell);
	}
	
	public static class Node implements Comparable<Node>{
		public int weight;
		public String str;
		public Node(int weight, String str) {
			this.weight = weight;
			this.str = str;
		}
		
		@Override
		public int compareTo(Node o) {
			return this.weight - o.weight;
		}
	}
	
	static boolean isPal(String st){
		for(int i = 0; i < st.length()/2; i++){
			if(st.charAt(i) != st.charAt(st.length()-i-1)){
				return false;
			}
		}
		return true;
	}
}
