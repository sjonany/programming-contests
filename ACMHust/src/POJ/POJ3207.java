package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class POJ3207 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		StringTokenizer tk = new StringTokenizer(reader.readLine());
		int N = Integer.parseInt(tk.nextToken());
		int M = Integer.parseInt(tk.nextToken());
		List<Pair> inp = new ArrayList<Pair>();
		List<Pair> outp = new ArrayList<Pair>();
		
		for(int i=0;i<M;i++){
			tk = new StringTokenizer(reader.readLine());
			int a = Integer.parseInt(tk.nextToken());
			int b = Integer.parseInt(tk.nextToken());
			Pair newP = new Pair(a,b);
			boolean touchIn = false;
			for(Pair p : inp){
				if(newP.intersect(p)){
					touchIn = true;
					break;
				}
			}
			if(touchIn){
				for(Pair p : outp){
					if(newP.intersect(p)){
						out.println("the evil panda is lying again");
						out.close();
						return;
					}
				}
				outp.add(newP);
			}else{
				inp.add(newP);
			}
		}
		out.println("panda is telling the truth...");
		
		out.close();
	}
	
	static class Pair{
		public int a;
		public int b;
		public Pair(int a, int b){
			this.a=Math.min(a,b);
			this.b=Math.max(a,b);
		}
		
		public boolean intersect(Pair other){
			return !((this.a > other.a && this.b < other.b) ||
					(this.a < other.a && this.b > other.b) || 
					(this.b < other.a) ||
					(this.a > other.b));
		}
	}
}
