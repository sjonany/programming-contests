package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NavigableSet;
import java.util.StringTokenizer;

public class POJ3636 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			int m = Integer.parseInt(in.readLine());
			StringTokenizer tk = new StringTokenizer(in.readLine());
			Pair[] pairs = new Pair[m];
			for(int i = 0 ;i < m; i++){
				pairs[i] = new Pair(Integer.parseInt(tk.nextToken()), Integer.parseInt(tk.nextToken()));
			}

			Arrays.sort(pairs);
			
			//the height of current nested dolls, invariant that this is decreasing is always held
			//if there is tie in height, increasing width.
			
			List<Integer> curDollHeights = new ArrayList<Integer>();
			for(int i = 0; i < pairs.length; i++){
				//find doll with highest height < me. If ties, pick one with smallest width
				int nextDollIndex = binSearch(curDollHeights, pairs[i].h);
				if (nextDollIndex == curDollHeights.size()) {
					curDollHeights.add(pairs[i].h);
				} else {
					curDollHeights.set(nextDollIndex, pairs[i].h);
				}
			}
			out.println(curDollHeights.size());
		}

		out.close();
	}
	
	//get first index in arr < s, given arr is descending
	//no need to check for width because we are going down anyways
	public static int binSearch(List<Integer> arr, int s){
		int lo=0;
		int hi=arr.size()-1;
		
		if(arr.size() == 0 || arr.get(0)<s){
			return 0;
		}else{
			while(lo < hi){
				int mid = lo + (hi-lo+1)/2;
				if(arr.get(mid) < s){
					hi = mid-1;
				}else{
					lo = mid;
				}
			}
		}
		return lo+1;
	}
	

	static class Pair implements Comparable<Pair>{
		public int w;
		public int h;

		public Pair(int w, int h) {
			this.w = w;
			this.h = h;
		}

		@Override
		public String toString() {
			return "Pair [w=" + w + ", h=" + h + "]";
		}

		// (w,h) ordering, (3,5)(3,4)(3,3) (4,10)(4,9)... etc
		@Override
		public int compareTo(Pair o) {
			if(o.w != this.w){
				return this.w - o.w;
			}
			return o.h - this.h;
		}
	}
}
