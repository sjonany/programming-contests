package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class POJ3812 {
	static int S = 6;
	static char special;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		while(true){
			special = in.readLine().charAt(0);
			if(special == '*')break;
			char[][] BOARD = new char[S][S];

			Set<Character> pieces = new HashSet<Character>();

			for(int r = 0; r < S; r++){
				String line = in.readLine();
				for(int c = 0; c < S; c++){
					BOARD[r][c] = line.charAt(c);
					if(BOARD[r][c] != '.')
						pieces.add(BOARD[r][c]);
				}
			}

			Set<State> seen = new HashSet<State>();
			State first = new State(BOARD, 0);
			seen.add(first);
			Queue<State> q = new LinkedList<State>();
			q.add(first);
			int moveCount = -1;

			if(win(first)){
				moveCount = 0;
			}else{
				while(!q.isEmpty()){
					State state = q.remove();

					for(char piece : pieces){
						int r = 0;
						int c = 0;
						boolean found = false;
						for(int tr = 0; tr < S; tr++){
							for(int tc = 0; tc < S ; tc++){
								if(state.board[tr][tc] == piece){
									r = tr;
									c = tc;
									found = true;
									break;
								}
							}
							if(found)break;
						}

						boolean isH;
						if(r == S-1){
							isH = true;
						}else{
							if(c == S-1){
								isH = false;
							}else{
								if(state.board[r][c+1] == piece){
									isH = true;
								}else{
									isH = false;
								}
							}
						}

						int length;

						if(isH){
							if(c+2 < S && state.board[r][c+2] == piece){
								length = 3;
							}else{
								length = 2;
							}
						}else{
							if(r+2 < S && state.board[r+2][c] == piece){
								length = 3;
							}else{
								length = 2;
							}
						}

						if(!isH){
							int top = r;
							int bot = r;
							while(top-1 >= 0 && state.board[top-1][c] == '.'){
								top--;
							}
							while(bot+length < S && state.board[bot+length][c] == '.'){
								bot++;
							}
							for(int tr = top; tr <= bot; tr++){
								if(tr == r)continue; 
								char[][] newBoard = copy(state.board);
								erase(newBoard, piece);
								for(int k = 0; k < length; k++){
									newBoard[tr+k][c] =piece;
								}

								State newState = new State(newBoard, state.move+1);
								if(!seen.contains(newState)){
									if(win(newState)){
										moveCount = newState.move;
										break;
									}
									seen.add(newState);
									q.add(newState);
								}
							}	
							if(moveCount > 0)break;
						}else{
							int left = c;
							int right = c;
							while(left-1 >= 0 && state.board[r][left-1] == '.'){
								left--;
							}
							while(right+length < S && state.board[r][right+length] == '.'){
								right++;
							}
							for(int tc = left; tc <= right; tc++){
								if(tc == c)continue; 
								char[][] newBoard = copy(state.board);
								erase(newBoard, piece);
								for(int k = 0; k < length; k++){
									newBoard[r][tc+k] =piece;
								}

								State newState = new State(newBoard, state.move+1);
								if(!seen.contains(newState)){
									if(win(newState)){
										moveCount = newState.move;
										break;
									}
									seen.add(newState);
									q.add(newState);
								}
							}	
							if(moveCount > 0)break;
						}

						if(moveCount > 0)break;
					}
					if(moveCount > 0) break;
				}
			}
			if(moveCount < 0)out.println(-1);
			else out.println(moveCount+1);
		}

		out.close();
	}

	static boolean win(State s){
		for(int r = 0; r < S ; r++){
			for(int c = 0;  c < S; c++){
				if(s.board[r][c] == special){
					for(int cp = c; cp < S; cp++){
						if(s.board[r][cp] != '.' && s.board[r][cp] != special){
							return false;
						}
					}
				}
			}
		}

		return true;
	}

	static void erase(char[][] board, char cj){
		for(int r = 0; r < S; r++){
			for(int c = 0; c < S; c++){
				if(board[r][c] == cj)
					board[r][c] = '.';
			}
		}
	}

	static char[][] copy(char[][] board){
		char[][] cop = new char[board.length][board.length];
		for(int r = 0; r < S; r++){
			for(int c = 0; c < S; c++){
				cop[r][c] = board[r][c];
			}
		}
		return cop;
	}

	static class State{
		public char[][] board;
		public int move;
		public State(char[][] board, int move){
			this.move = move;
			this.board = board;
		}

		@Override
		public boolean equals(Object other){
			State o = (State) other;
			for(int i = 0; i < board.length; i++){
				for(int j = 0; j < board.length; j++){
					if(board[i][j] != o.board[i][j])
						return false;
				}
			}
			return true;
		}

		@Override
		public int hashCode(){
			return Arrays.deepHashCode(board);
		}
	}
}
