package POJ;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.PriorityQueue;
import java.util.Set;


public class POJ3867 {
	static Set<Count> expanded = new HashSet<Count>();
	static Set<String> result = new HashSet<String>();
	static Set<String> oriString = new HashSet<String>();
	static Count[] arr;
	static int m;
	static int n;
	
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		n = in.readInt();
		m = in.readInt();
		
		arr = new Count[m];
		for(int i = 0; i < m; i++){
			String st = in.readString();
			oriString.add(st);
			arr[i] = new Count(st);
		}
		
		PriorityQueue<Count> pq = new PriorityQueue<Count>();
		
		for(int i = 0 ; i < 26; i++){
			Count alphabet = new Count();
			alphabet.chars = new int[26];
			alphabet.chars[i]++;
			alphabet.countChar = 1;
			alphabet.numIntersect = countIntersect(alphabet);
			pq.add(alphabet);
		}
		
		while(result.size() < n){
			Count cur = pq.remove();
			generate(cur);
			expanded.add(cur);
			for(int i = 0; i < 26; i++){
				Count extended = new Count();
				extended.chars = cur.chars.clone();
				extended.chars[i]++;
				extended.countChar = cur.countChar+1;
				extended.numIntersect = countIntersect(extended);
				if(!expanded.contains(extended))
					pq.add(extended);
			}
		}
		
		for(String s : result){
			out.println(s);
		}
		
		out.close();
	}
	
	static void generate(Count stats){
		generate(0, stats.chars.clone(), new char[stats.countChar]);
	}
	
	static void generate(int toSet, int[] stats, char[] built){
		if(result.size() >= n){
			return;
		}
		if(toSet == built.length){
			StringBuffer str = new StringBuffer();
			for(int i = 0; i < built.length; i++){
				str.append(built[i]);
			}
			String s = str.toString();
			if(!oriString.contains(s))
				result.add(s);
		}else{
			for(int i = 0 ; i < stats.length; i++){
				if(stats[i] != 0){
					stats[i]--;
					built[toSet] = (char)(i + 'A');
					generate(toSet+1, stats, built);
					stats[i]++;
				}
			}
		}
	}
	
	public static int countIntersect(Count cur){
		int ans = 0;
		for(int i = 0; i < m; i++){
			if(cur.isContainedUnder(arr[i])){
				ans++;
			}
		}
		return ans;
	}
	
	static class Count implements Comparable<Count>{
		public int[] chars;
		public int numIntersect;
		public int countChar;
		
		public Count(String s){
			chars = new int[26];
			for(int i = 0; i < s.length(); i++){
				chars[s.charAt(i)-'A']++;
			}
			countChar = s.length();
		}
		
		public Count(){}
		
		public boolean isContainedUnder(Count other){
			if(this.countChar > other.countChar){
				return false;
			}
			for(int i = 0; i < chars.length; i++){
				if(chars[i] > other.chars[i]){
					return false;
				}
			}
			return true;
		}
		
		@Override
		public int hashCode(){
			return Arrays.hashCode(chars);
		}
		
		@Override
		public boolean equals(Object other){
			return Arrays.equals(chars, ((Count)other).chars);
		}

		@Override
		public int compareTo(Count o) {
			return -this.numIntersect + o.numIntersect;
		}
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int readInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public String readString() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
