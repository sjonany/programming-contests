package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class POJ2567 {
	static List<Integer>[] edges;
	static int N;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		edges = (List<Integer>[]) Array.newInstance(List.class, 51);
		for(int i=1;i<=50;i++){
			edges[i] = new ArrayList<Integer>();
		}
		
		String line = reader.readLine();
		while(line != null && line.trim().length()!=0){
			Node tree = buildTree(line);
			for(int i=1;i<=50;i++){
				edges[i].clear();
			}
			N = 0;
			dfs(tree);
			
			for(int i=0; i < N-1;i++){
				int min = N;
				for(int j= 1; j<=N; j++){
					if(edges[j].size() == 1){
						min = Math.min(j, min);
					}
				}
				int other = edges[min].get(0);
				out.print(other);
				edges[min].remove(0);
				edges[other].remove(edges[other].indexOf(min));
				
				if(i != N-1){
					out.print(" ");
				}
			}
			out.println();
			line = reader.readLine();
		}
		
		out.close();
	}
	
	static void dfs(Node node){
		N = Math.max(N, node.n);
		for(Node child : node.children){
			edges[node.n].add(child.n);
			edges[child.n].add(node.n);
			dfs(child);
		}
	}
	
	//s is valid form, return rooted tree based on s
	public static Node buildTree(String s){
		s = s.substring(1,s.length()-1);
		if(s.matches("\\d+")){
			return new Node(Integer.parseInt(s));
		}
		
		Stack<String> st = new Stack<String>();
		for(int i=0; i < s.length(); i++){
			char c = s.charAt(i);
			if(c == ')'){
				StringBuffer el = new StringBuffer();
				el.insert(0, c);
				while(true){
					String top = st.pop();
					if(top.equals("(")){
						el.insert(0, top);
						st.add(el.toString());
						break;
					}else{
						el.insert(0, top);
					}
				}
			}else{
				if(Character.isDigit(c)){
					StringBuffer dig = new StringBuffer();
					while(Character.isDigit(c)){
						dig.append(c);
						c = s.charAt(i+1);
						i++;
					}
					i--;
					st.add(dig.toString());
				}else{
					st.add(c + "");
				}
			}
		}
		Stack<String> revSt = new Stack<String>();
		while(!st.isEmpty()){
			String tok = st.pop();
			if(tok.trim().length() != 0)
				revSt.add(tok);
		}
		
		Node root = new Node(Integer.parseInt(revSt.pop()));
		while(!revSt.isEmpty()){
			root.add(buildTree(revSt.pop()));
		}
		return root;
	}
	
	static class Node{
		public int n;
		public List<Node> children;
		
		public Node(int n){
			this.n = n;
			this.children = new ArrayList<Node>();
		}
		
		public void add(Node child){
			children.add(child);
		}
	}
}
