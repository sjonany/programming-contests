package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class POJ3817 {
	static int X = 0;
	static int Y = 1;
	static int C = 2;
	static int[][] points;
	static double[] totCost;
	static double[] dp;
	static int MAX_N = 1005;
	static int N;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		points = new int[MAX_N][3];
		totCost = new double[MAX_N];
		dp = new double[MAX_N];
		
		while(true){
			N = Integer.parseInt(in.readLine().trim());
			if(N == 0)break;
			
			points[0][X] = 0;
			points[0][Y] = 0;
			points[0][C] = 0;
			for(int i = 1; i <= N; i++){
				StringTokenizer tk = new StringTokenizer(in.readLine());
				points[i][X] = Integer.parseInt(tk.nextToken());
				points[i][Y] = Integer.parseInt(tk.nextToken());
				points[i][C] = Integer.parseInt(tk.nextToken());
			}

			points[N+1][X] = 100;
			points[N+1][Y] = 100;
			points[N+1][C] = 0;
			
			totCost[0] = points[0][C];
			for(int i= 1; i <= N+1; i++){
				totCost[i] = totCost[i-1] + points[i][C];
			}
			
			dp[0] = 0;
			for(int i = 1; i <= N+1 ; i++){
				dp[i] = Double.MAX_VALUE;
				for(int j = 0; j < i; j++){
					dp[i] = Math.min(dp[i], dp[j] + 1 + totCost[i-1] - totCost[j] + dist(i, j));
				}
			}
			
			double ans = Math.round(dp[N+1] * 1000.0)/1000.0;
			out.printf("%.3f\n", ans);
			out.flush();
		}
		
		out.close();
	}
	
	static double dist(int i, int j){
		double dx = points[i][X]-points[j][X] ;
		double dy = points[i][Y]-points[j][Y] ;
		return Math.sqrt(dx*dx + dy*dy);
	}
}
