package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;


public class POJ3637 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1 ;test <= T; test++){
			int n = Integer.parseInt(in.readLine());
			long sum = 0;
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int[] arr = new int[n];
			for(int i = 0; i < n; i++){
				arr[i] = Integer.parseInt(tk.nextToken());
			}
			
			Arrays.sort(arr);
			
			for(int i = n-1; i >= 0; i--){
				if((n-1-i)%3 == 2){
					sum += arr[i];
				}
			}
			out.println(sum);
		}
		
		out.close();
	}
}
