package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class POJ3630 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1 ;test <= T; test++){
			int n = Integer.parseInt(in.readLine());
			Map<String, Integer> pref = new HashMap<String, Integer>();
			String[] input = new String[n];
			
			for(int i = 0 ; i < n; i++){
				String str = in.readLine();
				input[i] = str;
				for(int j = 0; j < str.length(); j++){
					String pr = str.substring(0, j+1);
					Integer count = pref.get(pr);
					if(count == null){
						count = 0;
					}
					count++;
					pref.put(pr, count);
				}
			}
			
			boolean good = true;
			for(String str : input){
				if(pref.get(str) > 1){
					good = false;
					break;
				}
			}
			if(good){
				out.println("YES");
			}else{
				out.println("NO");
			}
		}
		
		out.close();
	}
}
