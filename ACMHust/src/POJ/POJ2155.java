package POJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class POJ2155 {
	static int[][] tree;
	static int N;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		int T = Integer.parseInt(reader.readLine());
		tree = new int[1001][1001]; 	
		for(int t=0;t<T;t++){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			N = Integer.parseInt(tk.nextToken());
			int M = Integer.parseInt(tk.nextToken());
			for(int i=0;i<=N;i++){
				for(int j=0; j<=N;j++){
					tree[i][j] = 0;
				}
			}

			for(int i=0;i<M;i++){
				tk = new StringTokenizer(reader.readLine());
				if(tk.nextToken().equals("C")){
					int ux = Integer.parseInt(tk.nextToken());
					int uy = Integer.parseInt(tk.nextToken());
					int bx = Integer.parseInt(tk.nextToken());
					int by = Integer.parseInt(tk.nextToken());
					update(ux,uy, 1);
					update(bx+1,by+1, 1);
					update(bx+1,uy, -1);
					update(ux, by+1, -1);
				}else{
					int x = Integer.parseInt(tk.nextToken());
					int y = Integer.parseInt(tk.nextToken());
					out.println(query(x,y)%2);
				}
			}
			if(t!=T-1)
				out.println();
		}
		out.close();
	}

	static int query(int x, int y){
		int sum = 0;
		for(int i = x; i > 0; i -= (i & -i)){
			for(int j = y; j > 0; j -= (j & -j)){
				sum += tree[i][j];
			}
		}
		return sum;
	}

	static void update(int x, int y, int val){
		for (int i = x; i <= N; i += (i & -i)) {
			for (int j = y; j <= N; j += (j & -j)) {
				tree[i][j] += val;
			}
		}
	}
}
