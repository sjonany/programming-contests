package POJ;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.StringTokenizer;

public class POJ3969 {
	static List<Integer>[] edges;
	//index = color, value = true/false i have neighbor of that color. time T =  true
	static int[] bad;
	static int[] color;
	static int MAX_N = 10000; 
	static int N;
	static int M;
	static int K;
	static int T;
	static int timestamp;
	
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		edges = (List<Integer>[]) Array.newInstance(List.class, MAX_N);
		bad = new int[MAX_N];
		color = new int[MAX_N];
		
		for(int i = 0; i < MAX_N; i++){
			edges[i] = new ArrayList<Integer>();
		}
		
		T = 1;
		int k = 1;
		while(true){
			timestamp = 1;
			N = 0;
			try{
				N = in.readInt();
			}catch(Exception e){
				break;
			}
			
			M = in.readInt();

			for(int i = 1; i <= N; i++){
				edges[i].clear();
				color[i] = -1;
			}
			
			for(int i = 0; i < M; i++){
				int a = in.readInt();
				int b = in.readInt();
				edges[a].add(b);
				edges[b].add(a);	
			}
			
			for(int i = 1; i <= N; i++){
				K = Math.max(K, edges[i].size());	
			}
			
			if(K % 2 == 0)K++;
			dfs(1);
			
			out.println(K);
			for(int i = 1; i <= N; i++){
				out.println(color[i]);
			}
			
			T++;
		}
		
		out.close();
	}
	
	static void dfs(int node){
		timestamp++;
		for(int v : edges[node]){
			if(color[v] != -1){
				bad[color[v]] =  timestamp;
			}
		}

		int goodCol = 1;
		
		while(true){
			if(bad[goodCol] != timestamp){
				break;
			}
			goodCol++;
		}
		
		color[node] = goodCol;

		for(int v : edges[node]){
			if(color[v] == -1){
				dfs(v);
			}
		}
	}	
}

class InputReader {
	private InputStream stream;
	private byte[] buf = new byte[1024];
	private int curChar;
	private int numChars;
	private SpaceCharFilter filter;

	public InputReader(InputStream stream) {
		this.stream = stream;
	}
	
	
	public int read() {
		if (numChars == -1)
			throw new InputMismatchException();
		if (curChar >= numChars) {
			curChar = 0;
			try {
				numChars = stream.read(buf);
			} catch (IOException e) {
				throw new InputMismatchException();
			}
			if (numChars <= 0)
				return -1;
		}
		return buf[curChar++];
	}

	public int readInt() {
		int c = read();
		while (isSpaceChar(c))
			c = read();
		int sgn = 1;
		if (c == '-') {
			sgn = -1;
			c = read();
		}
		int res = 0;
		do {
			if (c < '0' || c > '9')
				throw new InputMismatchException();
			res *= 10;
			res += c - '0';
			c = read();
		} while (!isSpaceChar(c));
		return res * sgn;
	}

	public String readString() {
		int c = read();
		while (isSpaceChar(c))
			c = read();
		StringBuilder res = new StringBuilder();
		do {
			res.appendCodePoint(c);
			c = read();
		} while (!isSpaceChar(c));
		return res.toString();
	}

	public boolean isSpaceChar(int c) {
		if (filter != null)
			return filter.isSpaceChar(c);
		return isWhitespace(c);
	}

	public static boolean isWhitespace(int c) {
		return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
	}

	public interface SpaceCharFilter {
		public boolean isSpaceChar(int ch);
	}
}