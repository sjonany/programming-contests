package POJ;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class POJ3635 {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int n = in.nextInt();
		int m = in.nextInt();
		List<Edge>[] edges = (List<Edge>[]) Array.newInstance(List.class, n);
		for(int i = 0 ; i < edges.length; i++){
			edges[i] = new ArrayList<Edge>();
		}
		int[] cityCost = new int[n];
		for(int i = 0 ; i < n; i++){
			cityCost[i] = in.nextInt();
		}
		
		for(int i = 0; i < m;i++){
			int u = in.nextInt();
			int v = in.nextInt();
			int d = in.nextInt();
			edges[u].add(new Edge(v, d));
			edges[v].add(new Edge(u, d));
		}
		
		int q = in.nextInt();
		for(int i = 0; i < q; i++){
			int c = in.nextInt();
			int s = in.nextInt();
			int e = in.nextInt();
			
			PriorityQueue<PqNode> pq = new PriorityQueue<PqNode>();
			pq.add(new PqNode(new Node(0,s),0));
			Set<Node> visit = new HashSet<Node>();
			visit.add(new Node(0,s));
			
			int[][] distance = new int[c+1][n];
			for(int j= 0 ; j<distance.length; j++){
				Arrays.fill(distance[j], 100000000);
			}
			distance[0][s] = 0;
			while(!pq.isEmpty()){
				PqNode node = pq.poll();
				if(node.weight > distance[node.node.fuel][node.node.city]){
					continue;
				}
				if(node.node.city == e) break;
				visit.add(node.node);
				//refuel
				if(node.node.fuel < c){
					Node refuelNode = new Node(node.node.fuel+1, node.node.city);
					int newCost = node.weight + cityCost[node.node.city];
					if(!visit.contains(refuelNode) && newCost < distance[refuelNode.fuel][refuelNode.city]){
						distance[refuelNode.fuel][refuelNode.city] = newCost;
						pq.add(new PqNode(refuelNode, newCost));
					}
				}
				
				//move
				for(Edge outEdge : edges[node.node.city]){
					if(node.node.fuel >= outEdge.dist){
						int newFuel = node.node.fuel - outEdge.dist;
						Node destNode = new Node(newFuel, outEdge.to);
						if(!visit.contains(destNode) && node.weight < distance[destNode.fuel][destNode.city]){
							distance[destNode.fuel][destNode.city] = node.weight;
							pq.add(new PqNode(destNode, node.weight));
						}
					}
				}
			}
			
			if(distance[0][e] == 100000000){
				out.println("impossible");
			}else{
				out.println(distance[0][e]);
			}
		}
		
		out.close();
	}
	
	static class PqNode implements Comparable<PqNode>{
		public PqNode(Node node, int weight) {
			this.node = node;
			this.weight = weight;
		}
		public Node node;
		public int weight;
		@Override
		public int compareTo(PqNode o) {
			return this.weight - o.weight;
		}
	}
	
	static class Node{
		public int fuel;
		public int city;
		
		public Node(int fuel, int city) {
			this.fuel = fuel;
			this.city = city;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + city;
			result = prime * result + fuel;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (city != other.city)
				return false;
			if (fuel != other.fuel)
				return false;
			return true;
		}
	}
	
	static class Edge{
		public int to;
		public int dist;
		
		public Edge(int to, int dist) {
			this.to = to;
			this.dist = dist;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
