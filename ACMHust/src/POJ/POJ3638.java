package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class POJ3638 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		
		for(int test = 1 ;test <= T; test++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int h = Integer.parseInt(tk.nextToken());
			int numHoles = h - Integer.parseInt(tk.nextToken());
			int[] x = new int[h];
			tk = new StringTokenizer(in.readLine());
			for(int i =0 ; i < x.length; i++){
				x[i] = Integer.parseInt(tk.nextToken());
			}
			
			//min error if consider 1->h, 1 and h are stored, and there are j holes
			double[][] dp = new double[h][numHoles+1];
			//error if endpts are i and j
			double[][] error = new double[h][h];
			for(int i = 0; i < error.length; i++){
				for(int j = i+1; j < error.length; j++){
					for(int k = i+1; k < j; k++){
						error[i][j] += Math.abs(x[i]+(x[j]-x[i]) * 1.0 * (k-i)/(j-i) - x[k]);
					}
				}
			}
			
			for(int i = 1; i < dp.length; i++){
				for(int j = 0; j < dp[0].length; j++){
					if(j == 0){
						dp[i][j] = 0;
						continue;
					}
					dp[i][j] = Double.MAX_VALUE;
					if(j >= i){
						break;
					}
					//k is the closest stored house to the right
					for(int k = i-1; k >= 0; k--){
						int holeCount = i-k-1;
						if(holeCount > j) break;
						dp[i][j] = Math.min(dp[i][j], dp[k][j-holeCount] + error[k][i]);
					}
				}
			}
			
			out.printf("%.4f\n",1.0*dp[h-1][numHoles]/h);
		}
		
		out.close();
	}
}
