package POJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.StringTokenizer;

/*
 Tried to understand ping's code
 */
public class POJ1150 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintStream out = new PrintStream(System.out);
		String line = reader.readLine();
		while(line != null){
			StringTokenizer tkz = new StringTokenizer(line);
			int n = Integer.parseInt(tkz.nextToken());
			int m = Integer.parseInt(tkz.nextToken());
			
			int big = n;
			int small = n-m;
			//want : mult small+1.... big
			//drain out 2's and 5's, 
			int extraTwo = countFactors(big, 2) - countFactors(big, 5)
					- (countFactors(small,2) - countFactors(small, 5));
			int[] dig = new int[]{2,4,8,6};
			int lastDig = 1;
			if(extraTwo != 0){
				lastDig = dig[(extraTwo-1) % dig.length];
			}
			
			//now 2's and 5's have been drained out
			//we note that we only now care about the last digit, since all mults of 10 have disapeared
			//3,7,9
			
			int[] dig3 = new int[]{3,9,7,1};
			int[] dig7 = new int[]{7,9,3,1};
			int[] dig9 = new int[]{9,1};
			
			int count3 = countLastDig(big,3) - countLastDig(small,3);
			if(count3 != 0) lastDig *= dig3[(count3-1) % dig3.length];
			int count7 = countLastDig(big,7) - countLastDig(small,7);
			if(count7 != 0) lastDig *= dig7[(count7-1) % dig7.length];
			int count9 = countLastDig(big,9) - countLastDig(small,9);
			if(count9 != 0) lastDig *= dig9[(count9-1) % dig9.length];
			
			out.println(lastDig % 10);
			line = reader.readLine();
		}
		out.close();
	}
	
	//how many nums 1...n, such that if we drain out all 2's and 5's, will have last dig = last
	static int countLastDig(int n, int last){
		if(n == 0){
			return 0;
		}
		
		return countLastDig(n/2, last) + countLastDig5(n, last);
	}
	
	//same as previous, but just drain out 5's
	static int countLastDig5(int n, int last){
		if(n == 0){
			return 0;
		}
		int cur = n / 10;
		if(n % 10 >= last) cur++;
		return cur + countLastDig5(n/5, last);
	}
	
	//x!, how many factors of div?
	static int countFactors(int x, int div){
		int num = 0;
		while(x > 0){
			num += x/div;
			x /= div;
		}
		return num;
	}
}
