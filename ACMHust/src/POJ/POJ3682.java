package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class POJ3682 {
	//E[i] = exp total pay if K=i
	//E[i] = p E[i|succeed first day] + (1-p) E[i|fail first day]
	//     = p (1 + E[i-1] + -missing add cost, mults of 2-)  + 
	//      (1-p) (1 + -missing cost- + E[i])
	//E[i] = p(1+E[i-1]+2(i-1)/p) + (1-p)(1+E[i]+2i/p)
	//E[i] = 1+E[i-1]+2(i-1)/p + (1-p)/p(1+2i/p)
	static double[] E;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		E = new double[1001];
		while(true){
			String line = reader.readLine();
			StringTokenizer tk = new StringTokenizer(line);
			int K = Integer.parseInt(tk.nextToken());
			if(K==0)break;
			double p = Double.parseDouble(tk.nextToken());
			for(int i=1;i<=K;i++){
				E[i] = 1 + E[i-1] + 2*(i-1)/p + (1-p)/p*(1+2*i/p);
			}
			out.printf("%.3f %.3f\n", K/p, E[K]);
		}
		out.close();
	}
}
