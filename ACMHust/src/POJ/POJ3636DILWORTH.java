package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NavigableSet;
import java.util.StringTokenizer;

public class POJ3636DILWORTH {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);

		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			int m = Integer.parseInt(in.readLine());
			StringTokenizer tk = new StringTokenizer(in.readLine());
			Pair[] pairs = new Pair[m];
			for(int i = 0 ;i < m; i++){
				pairs[i] = new Pair(Integer.parseInt(tk.nextToken()), Integer.parseInt(tk.nextToken()));
			}

			Arrays.sort(pairs);
			
			//Dilworth : min chain = length of longest anti chain
			int[] arr = new int[pairs.length];
			for(int i = 0 ; i < pairs.length; i++){
				arr[i] = pairs[i].h;
			}
			
			//longest antichain <-> longest non increasing subsequence of arr
			out.println(getLongestNonIncreasingSubsequence(arr));
		}

		out.close();
	}
	
	/**
longest non increasing subsequence
dp[i] = length of lnis ending exactly at i
dp[0] = 1;

v[i] = largest ending value of a longest subsequence of length i.
m[i] = index of the v[i] value in arr[]

So, arr[m[i]] = v[i]

We note that v[i] is non-increasing, 
if v[i+1] > v[i], then the chain before v[i+1], since lnis, must be >= v[i+1], and this is of length i
 	this means v[i] >= v[i+1], contradiction

When extending dp[i], I look at all j such that arr[j] >= arr[i], and take max dp[j] + 1
We note that if max valid dp[j] = k, then v[k] must fit the description (since it stores the largest ending value),
Now, we just have to find the largest k such that v[k] >= arr[i].
Since v[k] is non-increasing, we can binary search.
Then, dp[i] = k + 1

	 */
	static int getLongestNonIncreasingSubsequence(int[] arr){
		if(arr.length <= 1) return arr.length;
		
		int[] dp = new int[arr.length];
		int mSize = 2;
		int[] m = new int[arr.length+1];
		
		dp[0] = 1;
		m[1] = 0;
		int maxLength = -1;
		for(int i = 1; i < dp.length; i++){
			int j = binSearch(arr, arr[i], mSize-1, m);
			dp[i] = 1 + j;
			
			if(dp[i] >= mSize){
				m[dp[i]] = i;
				mSize++;
			}else{
				if(arr[m[dp[i]]] < arr[i]){
					m[dp[i]] = i;
				}
			}
			maxLength = Math.max(dp[i], maxLength);
		}
		
		return maxLength;
	}
	
	//get largest index in arr > s, given arr is descending
	public static int binSearch(int[] arr, int s, int hi, int[] m){
		int lo=1;
		
		if(arr[m[1]] < s){
			return 0;
		}else{
			while(lo < hi){
				int mid = lo + (hi-lo+1)/2;
				if(arr[m[mid]] < s){
					hi = mid-1;
				}else{
					lo = mid;
				}
			}
		}
		return lo;
	}
	

	static class Pair implements Comparable<Pair>{
		public int w;
		public int h;

		public Pair(int w, int h) {
			this.w = w;
			this.h = h;
		}

		// (w,h) ordering, (3,5)(3,4)(3,3) (4,10)(4,9)... etc
		@Override
		public int compareTo(Pair o) {
			if(o.w != this.w){
				return this.w - o.w;
			}
			return o.h - this.h;
		}
	}
}
