package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class POJ3632 {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		
		for(int test =1 ;test<=T; test++){
			int n = Integer.parseInt(in.readLine());
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int max = Integer.MIN_VALUE;
			int min = Integer.MAX_VALUE;
			for(int i = 0; i <n; i++){
				int it = Integer.parseInt(tk.nextToken());
				max = Math.max(it,  max);
				min = Math.min(it,  min);
			}
			
			out.println(2*(max-min));
		}
		
		out.close();
	}
}
