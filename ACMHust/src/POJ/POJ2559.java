package POJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Stack;
import java.util.StringTokenizer;

public class POJ2559 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int n = Integer.parseInt(tk.nextToken());
			if(n==0)break;
			Bar[] bars = new Bar[n];
			for(int i=0;i<n;i++){
				bars[i] = new Bar(Integer.parseInt(tk.nextToken()), i);
			}
			
			int[] left = new int[n];
			int[] right = new int[n];
			Stack<Bar> barLeft = new Stack<Bar>();
			barLeft.add(new Bar(-1,-1));
			for(Bar bar : bars){
				while(barLeft.peek().height >= bar.height){
					barLeft.pop();
				}
				left[bar.id] = barLeft.peek().id;
				barLeft.push(bar);
			}
			
			Stack<Bar> barRight = new Stack<Bar>();
			barRight.add(new Bar(-1,n));
			for(int i=n-1;i>=0;i--){
				Bar bar = bars[i];
				while(barRight.peek().height >= bar.height){
					barRight.pop();
				}
				right[bar.id] = barRight.peek().id;
				barRight.push(bar);
			}
			
			long maxArea = 0;
			for(int i=0;i<n;i++){
				maxArea = Math.max(((long)(right[i]-1-left[i])) * bars[i].height, maxArea);
			}
			out.println(maxArea);
		}
		out.close();
	}
	
	static class Bar{
		public int height;
		public int id;
		
		public Bar(int h, int id){
			this.height = h;
			this.id = id;
		}
	}
}
