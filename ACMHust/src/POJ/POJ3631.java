package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;


public class POJ3631 {
	static int m;
	static int n;
	static Set<Integer>[] incidentEdges;
	static Edge[] edges;
	static boolean[] checked;
	
	public static void main(String[] args) throws Exception{
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			m = Integer.parseInt(tk.nextToken());
			n = Integer.parseInt(tk.nextToken());
			
			//edge Ids incident to this node
			incidentEdges = (Set<Integer>[]) Array.newInstance(Set.class, n);
			for(int i = 0; i < incidentEdges.length; i++){
				incidentEdges[i] = new HashSet<Integer>();
			}
			
			// edge Ids
			edges = new Edge[m];
			
			for(int i = 0; i < m; i++){
				tk = new StringTokenizer(in.readLine());
				int n1 = Integer.parseInt(tk.nextToken());
				int n2 = Integer.parseInt(tk.nextToken());
				Edge e = new Edge(i, n1, n2);
				edges[i] = e;
				incidentEdges[n1].add(i);
				incidentEdges[n2].add(i);
			}
			
			/*
			System.out.println("edges = " + Arrays.toString(edges));
			System.out.println("incident edges");
			for(int i = 0 ;i < incidentEdges.length; i++){
				if(incidentEdges[i].size()!=0)
					System.out.println(i + " " + incidentEdges[i]);
			}*/
			
			checked = new boolean[n];
			boolean[] dfsVisited = new boolean[n];
			boolean[] usedEdge = new boolean[m];
			int[] edgeDirection = new int[m];
			boolean[] nodePointedTo = new boolean[n];
			
			boolean good = true;
			for(int i = 0; i < n; i++){
				if(checked[i]) continue;
				dfsVisited[i] = true;
				Edge cycleEdge = getCycleEdge(i, dfsVisited, usedEdge);
				//System.out.println("cycle edge in node " + i  +" = " + cycleEdge);
				if(cycleEdge == null){
					continue;
				}else{
					boolean push1 = push(cycleEdge, 1, edgeDirection, nodePointedTo);
					if(!push1){
						edgeDirection = new int[m];
						nodePointedTo = new boolean[n];
						boolean push2 = push(cycleEdge, 2, edgeDirection, nodePointedTo);
						if(!push2){
							good =false;
							break;
						}
					}
				}
				
				mark(i);
			}
			//System.out.println("DONE TEST = " + test);
			if(good){
				out.println("successful hashing");
			}else{
				out.println("rehash necessary");
			}
		}
		
		out.close();
	}

	static void mark(int i){
		checked[i] = true;
		for(int edgeId : incidentEdges[i]){
			Edge e = edges[edgeId];
			int next = e.getOpposite(i);
			if(!checked[next]){
				mark(next);
			}
		}
	}
	
	static boolean push(Edge edge, int dir, int[] edgeDirection, boolean[] nodePointedTo){
		edgeDirection[edge.id] = dir;
		int targetNode = edge.getTarget(dir);
		nodePointedTo[targetNode] = true;
		
		for(int edgeId : incidentEdges[targetNode]){
			Edge e = edges[edgeId];
			if(edgeDirection[edgeId] == 0){
				int nextDir = e.getDirection(targetNode);
				int nextTargetNode = e.getTarget(nextDir);
				if(nodePointedTo[nextTargetNode]){
					return false;
				}
				boolean isPush = push(e, nextDir, edgeDirection, nodePointedTo);
				if(!isPush){
					return false;
				}
			}
		}
		
		return true;
	}
	
	//return an edge on a cycle in the connected component of this node if there is one
	static Edge getCycleEdge(int node, boolean[] visited, boolean[] usedEdge){
		Edge cycleEdge = null;
		for(int edgeId : incidentEdges[node]){
			if(!usedEdge[edgeId]){
				Edge out = edges[edgeId];
				usedEdge[edgeId] = true;
				int nextNode = out.getOpposite(node);
				if(visited[nextNode]){
					return out;
				}else{
					visited[nextNode] = true;
					cycleEdge = getCycleEdge(nextNode, visited, usedEdge);
					if(cycleEdge != null)
						return cycleEdge;
				}
			}
		}
		
		return null;
	}
	
	
	static class Edge{
		public int id;
		public int n1;
		public int n2;
		public Edge(int id, int n1, int n2){
			this.id = id;
			this.n1 = Math.min(n1,n2);
			this.n2 = Math.max(n1,n2);
		}
		
		public int getOpposite(int n){
			if(this.n1==n)return n2;
			return n1;
		}
		
		public int getTarget(int direction){
			if(direction == 1){
				return n2;
			}else{
				return n1;
			}
		}
		
		public int getDirection(int from){
			int target = getOpposite(from);
			if(from < target){
				return 1;
			}else{
				return 2;
			}
		}
		
		@Override
		public String toString(){
			return id + " " + n1 + " " + n2;
		}
	}
}
