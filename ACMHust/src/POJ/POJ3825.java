package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class POJ3825 {
	static List<Integer>[] factors;
	static int[][] mat;
	static int R;
	static int C;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		factors = (List<Integer>[]) Array.newInstance(List.class, 65537);
		
		while(true){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			R = Integer.parseInt(tk.nextToken());
			C = Integer.parseInt(tk.nextToken());
			if(R == 0 && C == 0)break;
			mat = new int[R][C];
			for(int r = 0; r < R; r++){
				tk = new StringTokenizer(in.readLine());
				for(int c = 0; c < C; c++){
					mat[r][c] = Integer.parseInt(tk.nextToken());
				}
			}
			
			out.println(solve());
			
		}
		out.close();
	}
	
	static long solve(){
		List<Integer> rowFacs = getFactors(R);
		List<Integer> colFacs = getFactors(C);
		long ans = 0;
		for(int baseR : rowFacs){
			for(int baseC : colFacs){
				if(baseR * baseC != 1){
					int mulR = R / baseR;
					int mulC = C / baseC;
					if(mulR * mulC != 1){
						int gcd = mat[0][0];
						for(int r = 0; r < baseR; r++){
							for(int c = 0; c < baseC; c++){
								gcd = gcd(gcd, mat[r][c]);
							}
						}
						
						int[][] smallestBase = new int[baseR][baseC];
						for(int r = 0; r < baseR; r++){
							for(int c = 0; c < baseC; c++){
								smallestBase[r][c] = mat[r][c] / gcd;
							}
						}
						
						boolean isTensorable = true;
						int mulGcd = gcd;
						for(int plotR = 0; plotR < mulR; plotR++){
							for(int plotC = 0; plotC < mulC; plotC++){
								if(plotR == 0 && plotC == 0) continue;
								int topLeftR = plotR * baseR;
								int topLeftC = plotC * baseC;
								if(mat[topLeftR][topLeftC] % smallestBase[0][0] != 0){
									isTensorable = false;
									break;
								}else{
									int curMul = mat[topLeftR][topLeftC] / smallestBase[0][0];
									mulGcd = gcd(mulGcd, curMul);
									for(int r = 0; r < baseR; r++){
										for(int c = 0; c < baseC; c++){
											if(curMul * smallestBase[r][c] != mat[topLeftR+r][topLeftC+c]){
												isTensorable = false;
												break;
											}
										}
										if(!isTensorable)break;
									}
								}
							}
							if(!isTensorable)break;
						}
						
						if(!isTensorable){
							continue;
						}else{
							ans += getFactors(mulGcd).size();
						}
					}
				}
			}
		}
		return ans;
	}
	
	static void p(int[][] m){
		for(int r = 0 ; r < m.length ;r++){
			for(int c = 0 ; c < m[0].length ;c++){
				System.out.print(m[r][c] + " " );
			}	
			System.out.println();
		}
	}
	
	static int gcd(int a, int b){
		while(b>0){
			int temp = a % b;
			a = b;
			b = temp;
		}
		return a;
	}
	
	static List<Integer> getFactors(int x){
		if(factors[x] != null) return factors[x];
		
		List<Integer> ans = new ArrayList<Integer>();
		ans.add(1);
		for(int i = 2; i <= Math.sqrt(x); i++){
			if(x % i == 0){
				ans.add(i);
			}
		}
		
		int halfSize = ans.size();
		for(int index = halfSize-1; index >= 0; index--){
			int nextFac = x / ans.get(index);
			//square
			if(nextFac == ans.get(halfSize-1)) continue;
			ans.add(nextFac);
		}
		
		factors[x] = ans;
		return ans;
	}
}
