package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/*
 Pr(touch) = integrate theta {Pr(touch|theta)f(theta)}
 but theta ~ unif(0,pi)
 2 cases: if slanted line fits gap, or not
 If not fit, pr touch = 1, easy
 if fit, pr touch = vertical height/1
 */
public class POJ2598 {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		String line = reader.readLine();
		while(line != null && line.trim().length()!=0){
			double L = Double.parseDouble(line);
			double prob;
			if(L > 1){
				double theta = Math.asin(1/L);
				prob = (1 - 2*(theta - L + Math.sqrt(L*L-1))/Math.PI);
			}else{
				prob = (2 * L / Math.PI);
			}
			
			int a = (int)Math.ceil(prob*1000);
			int b = (int)Math.floor(prob*1000);
			if(a==b+1){
				//can't just round, must find argmax k (p=k)
				//just compare the floor and ceil, which has higher prob
				//divide p(x=k)/p(x=k+1) and if >1, pick k
				if(1.0*(b+1)/(1000-b) * (1-prob)/prob > 1.0){
					out.println(b);
				}else{
					out.println(a);
				}
			}else{
				out.println(a);
			}
			line = reader.readLine();
		}
		out.close();
	}
}
