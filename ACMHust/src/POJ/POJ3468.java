package POJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class POJ3468 {
	static long[] tree;
	static long[] lazy;
	static int N;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk = new StringTokenizer(reader.readLine());
		N = Integer.parseInt(tk.nextToken());
		int Q = Integer.parseInt(tk.nextToken());
		
		tree = new long[4*N];
		lazy = new long[4*N];
		
		long[] arr = new long[N];
		tk = new StringTokenizer(reader.readLine());
		for(int i=0; i<N; i++){
			arr[i] = Long.parseLong(tk.nextToken());
		}
		buildTree(1, 0, N-1, arr);
		for(int i=0; i<Q; i++){
			tk = new StringTokenizer(reader.readLine());
			if(tk.nextToken().equals("C")){
				int l = Integer.parseInt(tk.nextToken())-1;
				int r = Integer.parseInt(tk.nextToken())-1;
				int by = Integer.parseInt(tk.nextToken());
				update(l,r,by);
			}else{
				int l = Integer.parseInt(tk.nextToken())-1;
				int r = Integer.parseInt(tk.nextToken())-1;
				out.println(query(l,r));
			}
		}
		out.close();
	}
	
	static void buildTree(int node, int a, int b, long[] arr){
		if(a>b)return;
		if(a == b){
			tree[node] = arr[a];
			return;
		}
		
		int mid = (a+b)/2;
		buildTree(2*node, a, mid, arr);
		buildTree(2*node+1, mid+1, b, arr);
		
		tree[node] = tree[2*node] + tree[2*node+1];
	}
	
	static long query(int l, int r){
		return query(1, 0, N-1, l, r);
	}
	
	static void update(int l, int r, int val){
		update(1, 0, N-1, l, r, val);
	}
	
	static long query(int node, int a, int b, int l, int r){
		if(a == l && b == r){
			return lazy[node] * (r-l+1) + tree[node];
		}
		lazy[node*2] += lazy[node];
		lazy[node*2+1] += lazy[node];
		lazy[node] = 0;
		
		long res = 0;
		int mid = (a+b)/2;

		if(l <= mid) res += query(node * 2, a, mid, l, Math.min(r, mid));
		if(r > mid) res += query(node * 2 + 1, mid+1, b, Math.max(mid + 1, l), r);
		tree[node] = lazy[2*node+1] * (b-mid) + tree[2*node+1] +
				lazy[2*node] * (mid-a+1) + tree[2*node];
		return res;
	}
	
	static void update(int node, int a, int b, int l, int r, int val){
		if(a == l && b == r){
			lazy[node] += val;
			return;
		}
		lazy[node*2] += lazy[node];
		lazy[node*2+1] += lazy[node];
		lazy[node] = 0;
		
		int mid = (a+b)/2;

		if(l <= mid) update(node * 2, a, mid, l, Math.min(r, mid), val);
		if(r > mid) update(node * 2 + 1, mid+1, b, Math.max(mid + 1, l), r, val);
		tree[node] = lazy[2*node+1] * (b-mid) + tree[2*node+1] +
				lazy[2*node] * (mid-a+1) + tree[2*node];
	}
}
