package POJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class POJ2044 {
	static int N;
	static boolean[][][] sunny;
	static int[][][][][][][] cache;
	static int t;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		t = 1;
		//just store bottom left
		cache = new int[3][3][7][7][7][7][365];
		sunny = new boolean[365][3][3];
		boolean[] market = new boolean[17];
		while(true){
			N = Integer.parseInt(reader.readLine());
			if(N==0)break;
			
			boolean impossible = false;
			for(int d=0; d<N; d++){
				StringTokenizer tk = new StringTokenizer(reader.readLine());
				for(int i = 1; i<=16; i++){
					market[i] = tk.nextToken().equals("1");
				}
				boolean good = false;
				for(int r = 0; r < 3; r++){
					for(int c = 0; c < 3; c++){
						int num = (r+1)*4+c+1;
						sunny[d][r][c] = market[num] || market[num+1] || market[num-4] || market[num-3];
						if(!sunny[d][r][c])good = true;
					}
				}
				if(!good){
					impossible = true;
				}
			}
			if(impossible){
				out.println(0);
			}else{
				if(dfs(1,1,0,7,7,7,7)){
					out.println(1);
				}else{
					out.println(0);
				}
			}
			t++;
		}
		
		out.close();
	}
	
	//at start of day, at r,c with previous lives 
	static boolean dfs(int r, int c, int day, int ul, int ur, int br, int bl){
		if(day == N){
			return true;
		}
		int cacheVal = cache[r][c][ul-1][ur-1][br-1][bl-1][day];
		
		int k0 = r;
		int k1 = c;
		int k2 = ul-1;
		int k3 = ur-1;
		int k4 = br-1;
		int k5 = bl-1;
		int k6 = day;
		
		if(Math.abs(cacheVal) == t){
			return cacheVal > 0;
		}
		ul--;
		ur--;
		br--;
		bl--;
		if(r == 0){
			if(c == 0){
				ul = 7;
			}else if(c == 2){
				ur = 7;
			}
		}else if(r == 2){
			if(c == 0){
				bl = 7;
			}else if(c == 2){
				br = 7;
			}
		}
		if(ul * ur * br * bl == 0){
			cache[k0][k1][k2][k3][k4][k5][k6] = -t;
			return false;
		}
		
		if(sunny[day][r][c]){
			cache[k0][k1][k2][k3][k4][k5][k6] = -t;
			return false;
		}else{
			if(dfs(r,c,day+1, ul,ur,br,bl)){
				cache[k0][k1][k2][k3][k4][k5][k6] = t;
				return true;
			}
			for(int cx = 0; cx <= 2; cx++){
				if(cx != c){
					if(dfs(r, cx, day+1, ul, ur, br, bl)){
						cache[k0][k1][k2][k3][k4][k5][k6] = t;
						return true;
					}
				}
			}
			for(int rx = 0; rx <= 2; rx++){
				if(rx != r){
					if(dfs(rx, c, day+1, ul, ur, br, bl)){
						cache[k0][k1][k2][k3][k4][k5][k6] = t;
						return true;
					}
				}
			}		
		}

		cache[k0][k1][k2][k3][k4][k5][k6] = -t;
		return false;
	}
}
