package POJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

//slower, but was my first intuitive thought
public class POJ2559Redo {
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		while(true){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			int n = Integer.parseInt(tk.nextToken());
			if(n==0)break;
			Bar[] bars = new Bar[n];
			for(int i=0;i<n;i++){
				bars[i] = new Bar(Integer.parseInt(tk.nextToken()), i);
			}
			
			//key = height, val = list of bars
			TreeMap<Integer, List<Bar>> map = new TreeMap<Integer, List<Bar>>();
			for(Bar bar : bars){
				if(!map.containsKey(bar.height)){
					List<Bar> barLst = new ArrayList<Bar>();
					map.put(bar.height, barLst);
					barLst.add(bar);
				}else{
					map.get(bar.height).add(bar);
				}
			}
			
			Integer[] sortedHeight = map.keySet().toArray(new Integer[0]);
			Arrays.sort(sortedHeight);
			
			long maxArea = 0;
			
			TreeSet<Integer> lowHeights = new TreeSet<Integer>();
			for(int h : sortedHeight){
				List<Bar> barLst = map.get(h);
				for(Bar bar : barLst){
					Integer r = lowHeights.higher(bar.id);
					if(r == null) r = n;
					Integer l = lowHeights.lower(bar.id);
					if(l == null) l = -1;
					maxArea = Math.max(maxArea, (long)(r-l-1) * h);
				}
				for(Bar bar : barLst){
					lowHeights.add(bar.id);
				}
			}
			
			out.println(maxArea);
		}
		out.close();
	}
	
	static class Bar{
		public int height;
		public int id;
		
		public Bar(int h, int id){
			this.height = h;
			this.id = id;
		}
	}
}
