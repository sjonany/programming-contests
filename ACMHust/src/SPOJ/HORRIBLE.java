package SPOJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class HORRIBLE {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 1 ;test <= T; test++){ 
			tk = tk (in.readLine());
			int N = Integer.parseInt(tk.nextToken());
			int C = Integer.parseInt(tk.nextToken());
			Fenwick3 tree = new Fenwick3(N);
			for(int i = 0 ; i < C ; i++){
				tk = tk (in.readLine());
				if(Integer.parseInt(tk.nextToken()) == 0){
					int ii = Integer.parseInt(tk.nextToken());
					int jj = Integer.parseInt(tk.nextToken());
					int v = Integer.parseInt(tk.nextToken());
					tree.update(ii, jj, v);
				}else{
					int ii = Integer.parseInt(tk.nextToken());
					int jj = Integer.parseInt(tk.nextToken());
					out.println(tree.query(ii, jj));
				}
			}
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}

	//range update range query
	static class Fenwick3 {
		Fenwick2 mul;
		Fenwick2 add;
		int n;
		
		public Fenwick3(int n) {
			mul = new Fenwick2(n);
			add = new Fenwick2(n);
			this.n = n;
		}
		
		public void update(int i, int j, long v) {
			mul.update(i, j, v);
			add.update(i, j, (1-i) * v);
			add.update(j+1, n, (j-i+1) * v);
		}
		
		public long query(int i, int j) {
			return query(j) - query(i-1);
		}
		
		public long query(int i) {
			return mul.query(i) * i + add.query(i);
		}
	}
	
	//range update point query
	static class Fenwick2{ 
		public BIT tree;
		
		public Fenwick2(int n){
			tree = new BIT(n);
		}
		
		public long query(int i) {
			return tree.getTotalFreq(i);
		}
		
		public void update(int i, int j, long v){
			tree.increment(i, v);
			tree.increment(j+1, -v);
		}
	}
	
	public static class BIT{
		long[] tree;
		public BIT(int n){
			tree = new long[n+1];
		}

		//initArray[] is 0-based
		public BIT(long[] initArray){
			tree = new long[initArray.length+1];

			for(int i=0;i<initArray.length;i++){
				increment(i+1, initArray[i]);
			}
		}

		public void increment(int idx, long val){
			while(idx < tree.length){
				tree[idx] += val;
				idx += (idx & -idx);
			}
		}

		public long getTotalFreq(int idx){
			long sum = 0;
			while(idx > 0){
				sum += tree[idx];
				idx -= (idx & -idx);
			}
			return sum;
		}
	}
}
