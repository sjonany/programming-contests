package SPOJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class CNTTREE {
	static int T;
	static int N;
	static int K;
	static List<Integer>[] edges;
	
	//for both below, diameter <= K 
	//dpEq[v][l] = num subtrees rooted at v, and has depth of exactly l
	//for le, depth <= l
	static long[][] dpEq;
	static long[][] dpLe;
	
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		T = Integer.parseInt(reader.readLine().trim());
		edges= (List<Integer>[])Array.newInstance(List.class, 60);
		
		for(int i=0;i<60;i++){
			edges[i] = new ArrayList<Integer>();
		}
		dpEq = new long[60][60];
		dpLe = new long[60][60];
		
		for(int t=0; t<T; t++){
			StringTokenizer tk = new StringTokenizer(reader.readLine());
			N = Integer.parseInt(tk.nextToken());
			K = Integer.parseInt(tk.nextToken());
			for(int i=0; i<N; i++){
				edges[i].clear();
				for(int j=0; j<N; j++){
					dpEq[i][j] = 0;
					dpLe[i][j] = 0;
				}
			}	
			
			for(int i=1; i <= N-1; i++){
				tk = new StringTokenizer(reader.readLine());
				int from = Integer.parseInt(tk.nextToken());
				int to = Integer.parseInt(tk.nextToken());
				edges[from].add(to);
				edges[to].add(from);
			}
			
			dfs(0,-1);
			long total = 0;
			for(int node = 0; node < N; node++){
				total += dpLe[node][K]-1;
			}
			out.println(total);
			if(t!=T-1)reader.readLine();
		}
		
		out.close();
	}
	
	static void dfs(int cur, int from){
		List<Integer> children = new ArrayList<Integer>();
		for(int child : edges[cur]){
			if(child != from){
				dfs(child, cur);
				children.add(child);
			}
		}
		
		//for dpEq[cur][height], for any valid subtree, can be uniquely identified
		//by the leftmost support node, so if we vary the leftmost support, no overcounting
		//including empty = h= -1
		dpLe[cur][0] = 2;
		dpEq[cur][0] = 1;
		if(children.size() == 0){
			for(int h = 1; h <= K ; h++){
				dpLe[cur][h] = 2;
			}
			return;
		}
		
		for(int h = 1; h <= K; h++){
			long[] prefMult = new long[children.size()];
			
			int leftH = Math.min(h-2, K-h-1);
			prefMult[0] = leftH < 0 ? 1 : dpLe[children.get(0)][leftH];
			for(int childIndex = 1; childIndex < prefMult.length; childIndex++){
				prefMult[childIndex] = prefMult[childIndex-1] * 
						(leftH < 0 ? 1 : dpLe[children.get(childIndex)][leftH]);
			}
			
			long[] suffMult = new long[children.size()];
			int rightH = Math.min(h-1, K - h - 1);
			suffMult[children.size()-1] = 
					rightH < 0 ? 1:
					dpLe[children.get(children.size()-1)][rightH];
			for(int childIndex = children.size()-2; childIndex>= 0; childIndex--){
				suffMult[childIndex] = suffMult[childIndex+1] * 
						(rightH < 0 ? 1:dpLe[children.get(childIndex)][rightH]);
			}
			
			dpEq[cur][h] = 0;
			for(int childIndex = 0; childIndex < children.size(); childIndex++){
				long pref = childIndex == 0 ? 1 : prefMult[childIndex-1];
				long suff = childIndex == children.size()-1 ? 1: suffMult[childIndex+1];
				dpEq[cur][h] += pref
						* dpEq[children.get(childIndex)][h-1]
						* suff;
			}
			
			dpLe[cur][h] = dpLe[cur][h-1] + dpEq[cur][h];
		}
	}
}
