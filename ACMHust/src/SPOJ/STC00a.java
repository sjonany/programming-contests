package SPOJ;

//my cpp solution which passed in spoj as well :)
public class STC00a {

}
/*
#include <cstdio>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <set>
#include <queue>
#include <stack>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <ctype.h>
#include <string.h>

#include <assert.h>

using namespace std;

typedef long long LL;
LL MAX_LL = 100000000LL*1000000/2;
long MAX_L = (long)1000000000;
long N;
long K;
long edges[200][200];
string strs[200];

char s[100000];
long z[100000];
long length[200];

//get longest prefix of s2 that matches suffix of s1
long getEdgeWeight(int i, int j) {
    int s2l = length[j];
    int s1l = length[i];
        string s2 = strs[j];
        string s1 = strs[i];
        
    for(int i = 0; i < s2l; i++) {
        s[i] = s2[i];
    }

    for(int i = 0; i < s1l; i++) {
        s[s2l + i] = s1[i];
    }
    
    int sz = s1l + s2l;

    int longestPref = 0;
    int n = sz;
    int L = 0, R = 0;
    for (int i = 1; i < n; i++) {
        if (i > R) {
            L = R = i;
            while (R < n && s[R-L] == s[R]) R++;
            z[i] = R-L; R--;
        } else {
            int k = i-L;
            if (z[k] < R-i+1) z[i] = z[k];
            else {
                L = i;
                while (R < n && s[R-L] == s[R]) R++;
                z[i] = R-L; R--;
            }
        }
        if(i > s2l && i + z[i] >= n){
            longestPref = z[i];
            break;
        }
    }
    return s2l - longestPref;
}

LL ans[2][200][200];
void solve(long numSteps, long SWITCH) {
    long OTHER = SWITCH ^ 1;
    if(numSteps == 1){
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                ans[SWITCH][i][j] = edges[i][j];
            }
        }
    } else if(numSteps % 2 == 1){
        solve(numSteps-1, OTHER);
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                ans[SWITCH][i][j] = MAX_LL;
                for(int k = 0; k < N; k++) {
                  LL newans = ans[OTHER][i][k] + edges[k][j];
                  if(newans < ans[SWITCH][i][j]) {
                    ans[SWITCH][i][j] = newans;
                  }
                }
            }   
        }
    } else {
        solve(numSteps/2, OTHER);
        for(long i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                ans[SWITCH][i][j] = MAX_LL;
                for(int k = 0; k < N; k++) {      
                  LL newans = ans[OTHER][i][k] + ans[OTHER][k][j];
                  if(ans[SWITCH][i][j] > newans) {
                    ans[SWITCH][i][j] = newans;
                  }
                }
            }   
        }
    }
}

int main() {
    scanf("%ld %ld", &N, &K);
    long minLength = MAX_L;
    for(long i = 0; i < N; i++) {
        cin >> strs[i];
        length[i] = (long)strs[i].length();
        minLength = min(minLength, length[i]);
    }

    if(K == 1) { 
        printf("%ld\n", minLength);
        return 0;
    }
    
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            edges[i][j] = getEdgeWeight(i, j);
        }   
    }
    
    solve(K-1, 0);
    LL minn = MAX_LL;
    for(int r = 0; r < N; r ++) {
        for(int c = 0; c < N; c ++) {
            if(ans[0][r][c] != MAX_LL)
                minn = min(minn, length[r]+ ans[0][r][c]);
        }   
    }
    printf("%lld\n", minn);
    return 0;
}

*/
