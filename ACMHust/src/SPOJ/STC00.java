package SPOJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

//AC When converted to cpp in that polish website, 4.67 seconds at worst,
//But still TLE in SPOJ. Oh well. AC is AC :))
//update: STC00a.java contains the cpp code where I AC-ed in spoj too
public class STC00 {
	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static PrintWriter out = new PrintWriter(System.out);
	static int N;
	static int K;
	static int[][] edges;
	static String[] strs;
	static long MAX_LL = 100000000l*1000000/2;
	
	public static void main(String[] args) throws Exception {
		StringTokenizer tk = new StringTokenizer(in.readLine());
		N = Integer.parseInt(tk.nextToken());
		K = Integer.parseInt(tk.nextToken());

		strs = new String[N];
		int minLength = Integer.MAX_VALUE;
		for(int i = 0; i < N; i++) {
			strs[i] = in.readLine();
			minLength = Math.min(minLength, strs[i].length());
		}

		if(K == 1) { 
			out.println(minLength);
			out.close();
			return;
		}

		edges = new int[N][N];
		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				edges[i][j] = getEdgeWeight(strs[i], strs[j]);
			}	
		}
		ans = new long[2][N][N];
		solve(K-1, 0);
		long min = Long.MAX_VALUE;
		for(int r = 0; r < N; r ++) {
			for(int c = 0; c < N; c ++) {
				if(ans[0][r][c] != Long.MAX_VALUE)
					min = Math.min(min, strs[r].length() + ans[0][r][c]);
			}	
		}
		out.println(min);
		out.close();
	}

	static long[][][] ans;
	static void solve(int numSteps, int SWITCH) {
		int OTHER = SWITCH ^ 1;
		if(numSteps == 1){
			for(int i = 0; i < N; i++) {
				for(int j = 0; j < N; j++) {
					ans[SWITCH][i][j] = edges[i][j];
				}
			}
		} else if(numSteps % 2 == 1){
			solve(numSteps-1, OTHER);
			for(int i = 0; i < N; i++) {
				for(int j = 0; j < N; j++) {
					ans[SWITCH][i][j] = MAX_LL;
	                for(int k = 0; k < N; k++) {
	                  long newans = ans[OTHER][i][k] + edges[k][j];
	                  if(newans < ans[SWITCH][i][j]) {
	                    ans[SWITCH][i][j] = newans;
	                  }
	                }
				}	
			}
		} else {
			solve(numSteps/2, OTHER);
			for(int i = 0; i < N; i++) {
				for(int j = 0; j < N; j++) {
					ans[SWITCH][i][j] = MAX_LL;
					for(int k = 0; k < N; k++) {      
						long newans = ans[OTHER][i][k] + ans[OTHER][k][j];
						if(ans[SWITCH][i][j] > newans) {
							ans[SWITCH][i][j] = newans;
						}
					}
				}	
			}
		}
	}

	static char[] s = new char[100000];
	static int[] z = new int[100000];
	//get longest prefix of s2 that matches suffix of s1
	static int getEdgeWeight(String s1, String s2) {
		int s2l = s2.length();
		for(int i = 0; i < s2.length(); i++) {
			s[i] = s2.charAt(i);
		}

		for(int i = 0; i < s1.length(); i++) {
			s[s2l + i] = s1.charAt(i);
		}
		int sz = s1.length() + s2.length();

		int longestPref = 0;
		int n = sz;
		int L = 0, R = 0;
		for (int i = 1; i < n; i++) {
			if (i > R) {
				L = R = i;
				while (R < n && s[R-L] == s[R]) R++;
				z[i] = R-L; R--;
			} else {
				int k = i-L;
				if (z[k] < R-i+1) z[i] = z[k];
				else {
					L = i;
					while (R < n && s[R-L] == s[R]) R++;
					z[i] = R-L; R--;
				}
			}
			if(i > s2.length() && i + z[i] >= n){
				longestPref = z[i];
				break;
			}
		}
		return s2.length() - longestPref;
	}
}
