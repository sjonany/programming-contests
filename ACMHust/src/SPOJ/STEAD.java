package SPOJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class STEAD {
	static int[][] invPref;
	static int[] barnCaps;
	static int N;
	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer tk = new StringTokenizer(reader.readLine());
		N = Integer.parseInt(tk.nextToken());
		int B = Integer.parseInt(tk.nextToken());
		//[i][j] = k -> cow i's jth preferred = k
		invPref = new int[N][B];
		for(int i=0;i<N;i++){
			tk = new StringTokenizer(reader.readLine());
			for(int pref=0;pref<B;pref++){
				Integer barnId = Integer.parseInt(tk.nextToken());
				invPref[i][pref] = barnId-1;
			}
		}
		barnCaps = new int[B];

		tk = new StringTokenizer(reader.readLine());
		for(int i=0;i<B;i++){
			barnCaps[i] = Integer.parseInt(tk.nextToken());
		}

		//back edges for each barn
		List<Integer>[] back = (List<Integer>[])Array.newInstance(List.class, B);
		for(int i=0;i<B;i++){
			back[i] = new ArrayList<Integer>();
		}
		//there is no need to store the forward edges, because we are
		//augmenting in order of cow, and so forward edges that do not exist
		//anymore must be because it's a backedge, and that node will already be visited
		
		for(int diff = 1; diff < B; diff++){
			for(int min=0; min <= B-diff;min++){
				int max = min + diff - 1;

				//when was this barn visited
				int[] visited = new int[B+N];
				for(int i=0;i<visited.length;i++){
					visited[i] = -1;
				}
				for(List<Integer> backedges : back){
					backedges.clear();
				}
				
				boolean maxFlow = true;
				for(int cow=0;cow<N;cow++){
					if(!augment(cow, min, max, back, visited,cow)){
						maxFlow=  false;
						break;
					}
				}
				if(maxFlow){
				  System.out.println(diff);
				  return;
				}
			}
		}
		System.out.println(B);
	}

	//if true, means there is an augmenting path, and will mutate back to update backedges
	public static boolean augment(int cow, int minPref, int maxPref, List<Integer>[] back, int[] visited, int timestamp){
		if(visited[cow] == timestamp)return false;
		visited[cow] = timestamp;
		for(int pref = minPref; pref <= maxPref; pref++){
			int barn = invPref[cow][pref];
			if(visited[barn+N] != timestamp){
				visited[barn+N] = timestamp;
				if(back[barn].size() < barnCaps[barn]){
					//augment success!
					back[barn].add(cow);
					return true;
				}else{
					//need to dfs back
					for(int nextCowIndex =0; nextCowIndex < back[barn].size(); nextCowIndex++){
						int nextCow = back[barn].get(nextCowIndex);
						if(augment(nextCow, minPref, maxPref, back, visited, timestamp)){
							//remove the old back edge
							//add this new back edge
							back[barn].set(nextCowIndex, cow);
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
}