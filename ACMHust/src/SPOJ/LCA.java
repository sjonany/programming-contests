package SPOJ;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class LCA {
	static BufferedReader in;
	static PrintWriter out;

	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);

		int TEST = Integer.parseInt(in.readLine());
		for (int test = 0; test < TEST; test++) {
			out.println("Case " + (test + 1) + ":");
			solve();
		}

		out.close();
	}

	static List<Integer>[] edges;

	static void solve() throws Exception {
		int N = Integer.parseInt(in.readLine());
		edges = (List<Integer>[]) Array.newInstance(List.class, N);
		for (int i = 0; i < N; i++) {
			edges[i] = new ArrayList<Integer>();
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int M = Integer.parseInt(tk.nextToken());
			for (int j = 0; j < M; j++) {
				edges[i].add(Integer.parseInt(tk.nextToken()) - 1);
			}
		}

		initLca(N, 0);

		int Q = Integer.parseInt(in.readLine());
		for (int q = 0; q < Q; q++) {
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int a = Integer.parseInt(tk.nextToken()) - 1;
			int b = Integer.parseInt(tk.nextToken()) - 1;
			out.println(lca(a, b) + 1);
		}
	}

	// dp[i][j] = 2^jth ancestor of ith node, dp[i][0] = parent, -1 if not exist
	static List<Integer>[] dp;
	static int[] depth;

	// preprocess lca, O(nlogn)
	static void initLca(int N, int root) {
		depth = new int[N];
		dp = (List<Integer>[]) Array.newInstance(List.class, N);
		for (int i = 0; i < N; i++) {
			dp[i] = new ArrayList<Integer>();
		}
		dfsLca(root, -1);
	}

	static void dfsLca(int cur, int par) {
		if (par == -1) {
			depth[cur] = 0;
		} else {
			depth[cur] = depth[par] + 1;
			// dp[i][j] = dp[dp[i][j-1]][j-1]
			dp[cur].add(par);
			for (int j = 1;; j++) {
				int halfwayNode = dp[cur].get(j - 1);
				if (dp[halfwayNode].size() >= j) {
					dp[cur].add(dp[halfwayNode].get(j - 1));
				} else {
					break;
				}
			}
		}

		for (int vo : edges[cur]) {
			if (vo != par)
				dfsLca(vo, cur);
		}
	}

	// return lca of nodes a and b, O(log n)
	static int lca(int a, int b) {
		int lo = a;
		int hi = b;

		if (depth[lo] < depth[hi]) {
			int temp = lo;
			lo = hi;
			hi = temp;
		}

		if (depth[hi] == 0)
			return hi;

		int diff = depth[lo] - depth[hi];

		// bring lo to hi depth
		int pow = 0;
		while (diff > 0) {
			if ((diff & 1) != 0) {
				lo = dp[lo].get(pow);
			}
			diff >>= 1;
			pow++;
		}

		if (lo == hi)
			return lo;
		for (int log = (int) (Math.log(depth[lo]) + 1); log >= 0; log--) {
			if (dp[lo].size() > log) {
				int nlo = dp[lo].get(log);
				int nhi = dp[hi].get(log);
				if (nlo != nhi) {
					lo = nlo;
					hi = nhi;
				}
			}
		}

		return dp[lo].get(0);
	}
}
