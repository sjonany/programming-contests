package SPOJ;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Queue;

/**

nodes in cutset A = + who stay the same, and - who changed
cut cost = sum disagreement + sum flips

prove
cut costs:
edge from A to B have these types:
1. s->+  = cost of flipping + 
2. - -> t  = cost of flipping -
3. + -> - = this means both are either flipped, or non-flipped. cost of disgreement
4. + -> +  = disagree between flipped + and nonflipped
5. - -> - same
 */

public class COCONUTS {
	static int[][] CAP = new int[302][302];
	static int[][] edges= new int[302][302];
	static int[] edgeSize = new int[302];
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		while(true) {
			int n = in.nextInt();
			int m = in.nextInt();
			if(n == 0 && m == 0) {
				break;
			}

			Arrays.fill(edgeSize, 0);

			int S = n;
			int T = n+1;
			boolean[] believe = new boolean[n];
			for(int i =0 ; i < n; i++) {
				believe[i] = in.nextInt() == 1;
			}

			for(int i = 0; i < n+2; i++) {
				for(int j = 0; j < n+2; j++) {
					CAP[i][j] = 0;
				}
			}
			
			for(int i = 0; i < m; i++) {
				int a = in.nextInt() - 1;
				int b = in.nextInt() - 1;
				edges[a][edgeSize[a]++] = b;
				edges[b][edgeSize[b]++] = a;
				CAP[a][b] = 1;
				CAP[b][a] = 1;
			}

			for(int i = 0; i < n; i++) {
				if(believe[i]) {
					edges[S][edgeSize[S]++] = i;
					edges[i][edgeSize[i]++] = S;
					CAP[S][i] = 1;
					CAP[i][S] = 1;
				} else {
					edges[i][edgeSize[i]++] = T;
					edges[T][edgeSize[T]++] = i;
					CAP[i][T] = 1;
					CAP[T][i] = 1;
				}
			}

			out.println(dinic(CAP, n+2, S, T));
		}
		out.close();
	}

	static int[][] F = new int[302][302];
	static int[] l = new int[302];
	public static int dinic(int[][] C, int numNode, int source, int sink){
		Queue<Integer> q = new LinkedList<Integer>();
		for(int i = 0; i < numNode; i++) {
			for(int j = 0; j < numNode; j++) {
				F[i][j] = 0;
			}
		}
		int total = 0;
		while (true) {
			q.offer(source);
			Arrays.fill(l,-1);      
			l[source]=0;
			while (!q.isEmpty()) {
				int u = q.remove();
				for (int i = 0; i < edgeSize[u]; i++) {
					int v = edges[u][i];
					if (l[v]==-1 && C[u][v] > F[u][v]) {
						l[v] = l[u]+1;
						q.offer(v);
					}
				}
			}
			if (l[sink]==-1)              
				return total;
			total += dinic(C, numNode, source,sink, F,l, Integer.MAX_VALUE);
		}		
	}

	public static int dinic(int[][] C, int numNode, int u, int t, int[][]F, int[]l, int val) {
		if (val<=0)
			return 0;
		if (u==t)  
			return val;
		int a=0, av;                               
		for (int i = 0; i < edgeSize[u]; i++){ 
			int v = edges[u][i];
			if (l[v]==l[u]+1 && C[u][v] > F[u][v]) {
				av = dinic(C, numNode, v,t, F,l, Math.min(val-a, C[u][v]-F[u][v]));
				F[u][v] += av;
				F[v][u] -= av;
				a += av;
			}
		}
		if (a==0) //                                         
			l[u] = -1;
		return a;
	}

	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		
		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}