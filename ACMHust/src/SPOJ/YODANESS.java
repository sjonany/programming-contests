package SPOJ;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class YODANESS {
	public static void main(String[] args) throws Exception{
		BufferedReader reader  = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(reader.readLine());
		for(int t=1; t<=T;t++){
			solve(reader);
		}
	}
	
	public static void solve(BufferedReader reader) throws Exception{
		reader.readLine();
		String yoda = reader.readLine();
		String truth = reader.readLine();
		Map<String, Integer> order = new HashMap<String, Integer>();
		
		StringTokenizer tk = new StringTokenizer(truth);
		int i=1;
		while(tk.hasMoreTokens()){
			order.put(tk.nextToken(), i);
			i++;
		}
		
		int[] cur = new int[i-1];
		tk = new StringTokenizer(yoda);
		for(int j=0;j<cur.length;j++){
			cur[j] = order.get(tk.nextToken());
		}
		System.out.println(countInv(cur, 0, cur.length-1));
	}
	
	public static long countInv(int[] arr, int left, int right){
		if(left >= right){
			return 0;
		}else{
			int mid = (left+right)/2;
			long leftCount = countInv(arr, left, mid);
			long rightCount = countInv(arr, mid+1, right);
			
			int i = left;
			int j = mid+1;
			long midCount = 0;
			int[] temp = new int[right-left+1];
			int curIndex = 0;
			while(i <= mid && j <= right){
				if(arr[i] < arr[j]){
					temp[curIndex] = arr[i];
					i++;
				}else{
					temp[curIndex] = arr[j];
					j++;
					midCount += mid-i+1;
				}
				curIndex++;
			}
			
			while(i <= mid){
				temp[curIndex] = arr[i];
				i++;
				curIndex++;
			}
			
			while(j <= right){
				temp[curIndex] = arr[j];
				j++;
				curIndex++;
			}

			for(int k=0;k<temp.length;k++){
				arr[k+left] = temp[k];
			}
			return midCount + leftCount + rightCount;
		}
	}
}

