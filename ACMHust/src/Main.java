import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class Main {

	static Map<Integer, Integer> demap;
	public static void main(String[] args) throws Exception{
		demap = new HashMap<Integer, Integer>();
		for(int i = 1; i <= 700; i++){
			int j = i*(i-1)/2;
			demap.put(j, i);
		}
		
		int T = 1;
		int numPoints = 5;
		for(int test = 0; test < T; test++){
			List<Point> pts = new ArrayList<Point>();
			Set<Point> pointSet = new HashSet<Point>();
			/*
			while(pointSet.size() < numPoints){
				int x = (int)(Math.random() * 10);
				int y = (int)(Math.random() * 10);
				pointSet.add(new Point(x,y));
			}*/
			pts.addAll(pointSet);
			pts.add(new Point(4, 1));
			pts.add(new Point(2, 0));
			pts.add(new Point(0, 1));
			pts.add(new Point(1, 2));
			pts.add(new Point(7, 1));
			int rightAns = solveRight(pts);
			System.out.println("right Ans = " + rightAns);
			int wrongAns = solveWrong(pts);
			System.out.println("wrong Ans = " + wrongAns);
			if(wrongAns != rightAns){
				System.out.println(pts);
				break;
			}
		}
	}
	
	static int solveWrong(List<Point> pts) {
		Map<Line, Integer> count = new HashMap<Line, Integer>();
		for(int i = 0; i < pts.size(); i++){
			for(int j = i+1; j < pts.size(); j++) {
				Line l = new Line(pts.get(i).x, pts.get(i).y, pts.get(j).x, pts.get(j).y);
				System.out.println(l.a + " " + l.b + " " + l.c + " " + pts.get(i) + " " + pts.get(j));
				if(!count.containsKey(l)){
					count.put(l, 0);
				}
				count.put(l, count.get(l) + 1);
			}
		}
		
		int max = 1;
		for(Line l : count.keySet()){
			int c = count.get(l);
			max = Math.max(max, c);
		}
		System.out.println(pts + " " + max);
		return demap.get(max);
	}
	
	static int solveRight(List<Point> pts){
		int max = 0;
		for(int i = 0; i < pts.size(); i++){
			Map<Line, Integer> count = new HashMap<Line, Integer>();
			for(int j = 0; j < pts.size(); j++) {
				if(i==j)continue;
				Line l = new Line(pts.get(i).x, pts.get(i).y, pts.get(j).x, pts.get(j).y);
				if(!count.containsKey(l)){
					count.put(l, 0);
				}
				count.put(l, count.get(l) + 1);
			}
			for(Line l : count.keySet()){
				int c = count.get(l);
				max = Math.max(max, c);
			}
		}
		
		return (max+1);
	}
	
	static class Point{
		public int x;
		public int y;
		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Point other = (Point) obj;
			if (x != other.x)
				return false;
			if (y != other.y)
				return false;
			return true;
		}
		@Override
		public String toString() {
			return "[" + x + ", " + y + "]";
		}
	}
	
	static class Line {
		public long a;
		public long b;
		public long c;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (a ^ (a >>> 32));
			result = prime * result + (int) (b ^ (b >>> 32));
			result = prime * result + (int) (c ^ (c >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Line other = (Line) obj;
			if (a != other.a)
				return false;
			if (b != other.b)
				return false;
			if (c != other.c)
				return false;
			return true;
		}

		public Line(long x1, long y1, long x2, long y2){
			long A = y2 - y1;
			long B = x1 - x2;
			long C = -A * x1 - B * y1;
			if(A < 0) {A = -A; B = -B; C = -C;};
			if(A==0 && B < 0) {B= -B; C = -C;}
			this.a = A;
			this.b = B;
			this.c = C;
			reduce();
		}
		
		void reduce() {
			long g = gcd(a, gcd(b,c));
			a/=g; b/=g; c/=g;
		}
		
		long gcd(long a, long b){
			a = Math.abs(a);
			b = Math.abs(b);
			if(a==0 || b==0) return a+b;
			while(b > 0){
				long temp = b;
				b = a %b;
				a = temp;
			}
			return a;
		}
	}
	
	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}