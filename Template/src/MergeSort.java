import java.util.Arrays;


public class MergeSort implements Sorter{

	@Override
	public void sort(int[] arr) {
		int[] tmpArr = new int[arr.length];
		sort(arr,0,arr.length-1, tmpArr);
	}
	
	private void sort(int[] arr, int l, int r, int[] tmpArr){
		if(l>=r)return;
		
		int mid = (l+r)/2;
		sort(arr, l, mid, tmpArr);
		sort(arr, mid+1, r, tmpArr);
		
		int curL = l;
		int curR = mid+1;
		
		
		int tIndex = 0;
		while(curL <= mid && curR <= r){
			if(arr[curL] < arr[curR]){
				tmpArr[tIndex++] = arr[curL];
				curL++;
			}else{
				tmpArr[tIndex++] = arr[curR];
				curR++;
			}
		}
		
		while(curL <= mid){
			tmpArr[tIndex++] = arr[curL++];
		}
		
		while(curR <= r){
			tmpArr[tIndex++] = arr[curR++];
		}
		
		for(int i = 0; i < tIndex; i++){
			arr[l+i] = tmpArr[i];
		}
	}
	
}
