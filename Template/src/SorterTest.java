import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;


public class SorterTest {
	static final int NUM_TEST = 100;
	static final int MAX_VAL = 1000;
	
	public void testSort(Sorter sorter){
		for(int i = 0; i < NUM_TEST; i++){
			int[] arr = new int[100];
			for(int j = 0; j < arr.length; j++){
				arr[j] = (int)(Math.random() * MAX_VAL);
				
				int[] actualArr = arr.clone();
				sorter.sort(actualArr);
				
				int[] expArr = arr.clone();
				Arrays.sort(expArr);
				Assert.assertArrayEquals(expArr, actualArr);
			}
		}
	}
	
	public void testEasy(Sorter sorter){
		int[] arr = new int[]{5,4,3,2,1};
		sorter.sort(arr);
		for(int i = 1; i <= 5; i++){
			Assert.assertEquals(i, arr[i-1]);
		}
	}
	
	
	@Test
	public void insertionSortTest() {
		testEasy(new InsertionSort());
		testSort(new InsertionSort());
	}

	@Test
	public void selectionSortTest() {
		testEasy(new SelectionSort());
		testSort(new SelectionSort());
	}
	
	@Test
	public void bubbleSortTest() {
		testEasy(new BubbleSort());
		testSort(new BubbleSort());
	}
	
	@Test
	public void quickSortTest() {
		testEasy(new QuickSort());
		testSort(new QuickSort());
	}	
	
	@Test
	public void mergeSortTest() {
		testEasy(new MergeSort());
		testSort(new MergeSort());
	}
}
