import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;


public class Security_hw2_q5 {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		List<Integer> input = new ArrayList<Integer>();
		String s = sc.nextLine();
		StringTokenizer tk = new StringTokenizer(s);
		while(tk.hasMoreTokens()){
			input.add(Integer.parseInt(tk.nextToken()));
		}
		solve(input.toArray(new Integer[0]));
	}
	
	static void solve(Integer[] input) {
		char[] mapping = new char[27];
		for(int i = 1; i <= 26; i++) {
			mapping[i] = (char)('A' + i-1);
		}
		
		//bf[i] = j  means (j ^ 3) % 33 = i;
		int[] bf = new int[33];
		Arrays.fill(bf, -1);
		for(int j = 1; j <= 26; j++) {
			int res = (int) (Math.pow(j, 3) % 33);
			bf[res] = j;
		}
		
		for(int i : input) {
			System.out.print(mapping[bf[i]]);
		}
	}
}
