package Algorithm;

import java.util.List;

public class MathStuff {

	static long MOD = 1000000007;
	
	public static boolean isPrime(int n){
		if(n==1) return false;
		if(n==2) return true;
		if(n%2 == 0) return false;
		for(int i=3; i*i <=n; i+=2){
			if(n%i == 0)
				return false;
		}
		
		return true;
	}
	
	public static int lcm(int a, int b){
		return (a * b) / gcd(a,b);
	}
	
	public static boolean[] sieve(int N){
		boolean[] isPrime = new boolean[N+1];
		for(int i = 2; i <= N; i++){
			isPrime[i] = true;
		}

		for(int i = 2; i*i <= N; i++){
			if(isPrime[i]){
				for(int  j = i*i; j <= N; j+=i){
					isPrime[j] = false;
				}
			}
		}
		return isPrime;
	}
	
	//smallest prime factor
	public static int[] divisor(int max) {
		int[] divisor = new int[max+1];
		for(int i = 1; i <= max; i++){
			divisor[i] = i;
		}
		
		for(int i = 2; i * i <= max; i++){
			if(divisor[i] == i) {
				for(int j = i * i; j <= max; j+=i) {
					divisor[j] = i;
				}
			}
		}
		
		return divisor;
	}
	
	//check POSITIVE!!!!
	public static int gcd(int a, int b) 
	{
		while(b > 0)
		{
			int c = a % b;
			a = b;
			b = c;
		}
		return a;
	}
	 

	static long[][] comb = new long[1001][1001];
	{
		comb[1][1] = 1;
		for(int i=2;i<comb.length;i++){
			for(int j=1;j<=i;j++){
				if(j==1){
					comb[i][j] = i;
				}else{
					comb[i][j] = (comb[i-1][j] + comb[i-1][j-1]) % MOD;
				}
			}
		}
	}
	
	//return x of ax mod m = 1, aka, the equivalent of 1/a, only exist if a and m are coprime 
	//AND m has to be prime
	//FERMAT'S LITTLE THEOREM
	static long modInverse(long a, long m) {
	    return exp(a,m-2,m);
	}
	
	//or just use this :D
	//return BigInteger.valueOf(a).modInverse(BigInteger.valueOf(mod)).longValue();
	
	/*
	 * O(log m) combination -> choose the inverse method carefully, coprime or not?
	static long Comb (int n, int k) {	
		long num = fac[n];
		long denom = (fac[k] * fac[n-k]) % MOD;
		return (num * modInverse(denom, MOD))%MOD;
	}*/
	
	//invArray[i] = 1/i in mod m
	//O(N)
	public static long[] inverseArray(int n, long m){
		long[] invArray = new long[n+1];
		invArray[1] = 1;
		for(long i=2;i<=n;i++){
			invArray[(int)i] = (-(m/i) * invArray[(int)(m % i)]) % m + m;
		}
		return invArray;
	}
	
	//1/v in mod t. works if a and m are coprime
	public static long euclidInverse(long v, long t){
		long a = v;
		long b = t;
	    long x = 1, y = 0;
	    long xLast = 0, yLast = 1;
	    long q, r, m, n;
	    while(a != 0) {
	        q = b / a;
	        r = b % a;
	        m = xLast - q * x;
	        n = yLast - q * y;
	        xLast = x; 
	        yLast = y;
	        x = m;
	        y = n;
	        b = a; 
	        a = r;
	    }
	    return (xLast+ t) % t;
	}
	
	//Chinese remainder theorem
	/*
	 Given x = ai (mod mi), for a bunch of i's, where all m's are pairwise coprime, find x
	 M = mul mi's
	 yi =  M/mi
	 zi = yi^-1 mod mi (possible because gcd(yi, mi) = 1, since pairwise coprime)
	 x = (sum ai*yi*zi)  % M
	 
	 HARDWAY if not coprime
	 in general, a soln exists iff ai = aj (mod gcd(mi, mj))
	 1. factorize mi's into prime powers, break x = ai mod (p^k) 
	 2. group together conditions that have same p's, and resolve in ascending power
	 	only highest power remains or contradiction.
	 3. x = ai mod p^b1 and x = aj mod p ^b2 is solvable iff ai=aj mod (min(pb)) is solvable
	 4. now the conditions are all relatively prime 
	 
	 easier way: keep solving pairs of eqns and reduce or stop 
	 x = a1 (mod m1)
	 x = a2 (mod m2) 
	 there exists a soln iff gcd(m1, m2) | a1-a2
	 the new eqn is x = a1 + m1 * k (mod c*m1)
	 c = m2/gcd(m1,m2)
	 k = {(a2-a1)/gcd(m1, m2) * (m1/gcd(m1,m2))^-1} (mod c) 
	 */
	
	//multinomial high! / small1!/small2!etc.
	public static long multnom(int high, List<Integer> smalls){
		long res = 1;
		int curHigh = high;
		for(int i=0;i<smalls.size();i++){
			res  = (res * comb[curHigh][smalls.get(i)]) % MOD;
			curHigh -= smalls.get(i);
		}
		return res;
	}

	public static long exp(long a, long n, long mod){
		if(n==0){
			return 1;
		}

		if(n % 2 == 0){
			long r = exp(a, n/2, mod);
			return (r*r) % mod;
		}else{
			return (a * exp(a,n-1, mod)) % mod;
		}
	}

	public static long exp(long a, long n){
		if(n==0){
			return 1;
		}

		if(n % 2 == 0){
			long r = exp(a, n/2);
			return (r*r);
		}else{
			return (a * exp(a,n-1));
		}
	}

	//length of bits without leading zeroes
	public static long countBinLength(long value) {
	    return Long.SIZE-Long.numberOfLeadingZeros(value);
	}
}
