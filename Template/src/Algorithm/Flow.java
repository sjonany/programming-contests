package Algorithm;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Flow {
	//O(Ef), f = maxflow
	private static class MaximumFlowFordFulkerson {
	    public static class FlowEdge {
			public int dest;
	        public int capacity;
	        //flow left allowed, NOT currently flowing
	        public int flow;
	        public FlowEdge reverse;
	        
	    	public FlowEdge(int v, int capacity, int flow) {
				this.dest = v;
				this.capacity = capacity;
				this.flow = flow;
			}
	    }
	 
	    private int N; // node from 0 to N - 1
	    public List<FlowEdge>[] adjMap;
	    int mark;   // global variable for checking if a node is already visited
	    int[] seen;  // status of each node
	 
	    MaximumFlowFordFulkerson(int numNode){
	        N = numNode;
	        adjMap = (List<FlowEdge>[]) Array.newInstance(List.class, N);
	        for(int i = 0; i < adjMap.length; i++){
	        	adjMap[i] = new ArrayList<FlowEdge>();
	        }
	        
	        seen = new int[N];
	        mark = 0;
	    }
	 
	    public void addEdge(int from, int to, int cap){
	    	FlowEdge forward = new FlowEdge(to, cap, cap);
	    	FlowEdge back = new FlowEdge(from, 0, 0);
	    	adjMap[from].add(forward);
	        adjMap[to].add(back);
	        forward.reverse = back;
	        back.reverse = forward;
	    }
	    //OPTIONAL
	    public void resetFlow(){
	        for(int i = 0; i < N; i++ ){
	            int sz = adjMap[i].size();
	            for(int j = 0; j < sz; j++ ){
	            	FlowEdge e = adjMap[i].get(j);
	            	e.flow = e.capacity;
	            }
	        }
	    }
	 
	    private int findAugmentingPath(int at, int sink, int val){
	        int sol = 0;
	        seen[at] = mark;
	        if(at == sink) return val;
	        int sz = adjMap[at].size();
	        for(int i = 0; i < sz; i++ ){
	            FlowEdge flow = adjMap[at].get(i);
	            int v = flow.dest;
	            int f = flow.flow;
	            if(seen[v] != mark && f > 0){
	                sol = findAugmentingPath(v, sink, Math.min(f, val));
	                if(sol > 0){
	                    flow.flow -= sol;
	                    flow.reverse.flow += sol;
	                    return sol;
	                }
	            }
	        }
	        return 0;
	    }
	 
	    public int getMaximumFlow(int S, int T){
	        int res = 0;
	        while(true){
	            mark++;
	            int flow = findAugmentingPath(S, T, Integer.MAX_VALUE);
	            if(flow == 0) break;
	            else res += flow;
	        }
	        return res;
	    }
	};

	/*
	 edmond karp. augmentation using bfs O(m^2n)
	 
	private static int augmentPath(int[][] caps, List<Integer>[] outEdges, int numNode, int source, int sink){
		//bfs
		boolean[] isVisited = new boolean[numNode];
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(source);
		isVisited[source] = true;
		
		int[] from = new int[numNode];
		for(int i=0;i<from.length;i++){
			from[i] = -1;
		}
		
		boolean isSinkFound = false;
		while(!q.isEmpty() && !isSinkFound){
			int cur = q.remove();
			for(Integer next : outEdges[cur]){
				if(!isVisited[next] && caps[cur][next] > 0){
					isVisited[next] = true;
					q.add(next);
					from[next] = cur;
					if(next == sink){
						isSinkFound = true;
						break;
					}
				}
			}
		}
		
		//get min cap along bfs path
		int cur = sink;
		int minCap = Integer.MAX_VALUE;
		while(from[cur] != -1){
			minCap = Math.min(minCap, caps[from[cur]][cur]);
			cur = from[cur];
		}
		

		if(minCap == Integer.MAX_VALUE){
			//no more augmenting path
			return 0;
		}
		
		//update residual network
		
		cur = sink;
		while(from[cur] != -1){
			caps[from[cur]][cur] -= minCap;
			caps[cur][from[cur]] += minCap;
			cur = from[cur];
		}
		
		return minCap;
	}
	*/
	////////////////////////////////////////
	//dinic O(V^2E)

	public static int dinic(int[][] C, List<Integer>[] outEdges, int numNode, int source, int sink){
		Queue<Integer> q = new LinkedList<Integer>();
		int[] l = new int[numNode];
		int[][] F = new int[numNode][numNode];
		int total = 0;
		while (true) {
			q.offer(source);
			Arrays.fill(l,-1);      
			l[source]=0;
			while (!q.isEmpty()) {
				int u = q.remove();
				for (int v: outEdges[u]) {
					if (l[v]==-1 && C[u][v] > F[u][v]) {
						l[v] = l[u]+1;
						q.offer(v);
					}
				}
			}
			if (l[sink]==-1)              
				return total;
			total += dinic(C, outEdges, numNode, source,sink, F,l, Integer.MAX_VALUE);
		}		
	}

	public static int dinic(int[][] C, List<Integer>[] outEdges, int numNode, int u, int t, int[][]F, int[]l, int val) {
		if (val<=0)
			return 0;
		if (u==t)  
			return val;
		int a=0, av;                               
		for (int v: outEdges[u]) {
			if (l[v]==l[u]+1 && C[u][v] > F[u][v]) {
				av = dinic(C, outEdges, numNode, v,t, F,l, Math.min(val-a, C[u][v]-F[u][v]));
				F[u][v] += av;
				F[v][u] -= av;
				a += av;
			}
		}
		if (a==0) //                                         
			l[u] = -1;
		return a;
	}
	
	/*
	 * Feasible Circulation
	 	note that flow conservation is not maintained, since flow conservation means D=0 except S T
		existFeasibleCirculation(G, D, Caps) = maxFlow(G') == sum positive d == - sum negative d 
		G' = newSource -> negative demands by |D|
			positiveDemands -> newSink by |D|
		maxFlow(G') = sum positive demands <-> exist feasible circulation
		
		If have lower bound on demand, existFeasible
		Make G' = caps = Caps - lowerbounds, node.demand = node.d - (in.lowerbounds - out.lowerbounds) 
		and do feasible circulation on new graph
	 */
}
