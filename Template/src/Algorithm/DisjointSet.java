package Algorithm;

public class DisjointSet{
	int[] parent;
	int[] size;
	
	public DisjointSet(int s){
		parent = new int[s];
		size = new int[s];
		for(int i = 0; i < s; i++){
			parent[i] = i;
			size[i] = 1;
		}
	}
	
	public int getLeader(int x){
		if(parent[x] == x){
			return x;
		}
		parent[x] = getLeader(parent[x]);
		return parent[x];
	}
	
	public int getSize(int x){
		return size[getLeader(x)];
	}
	
	public boolean union(int x, int y){
		x = getLeader(x);
		y = getLeader(y);
		if(x == y){
			return true;
		}
		//y as root
		if(size[x] < size[y]){
			int temp = x;
			x = y;
			y = temp;
		}
		
		parent[x] = y;
		size[y] += size[x];
		return false;
	}
}