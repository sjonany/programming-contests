package Algorithm;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Graph {
	//FLOYD WARSHALL, negative cycle iff dij + dji < 0 for some i j
	/*
	static void floydWarshall(){
		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++){
				costMap[i][j] = Integer.MAX_VALUE;
			}
		}

		for(int i = 0; i < N; i++){
			costMap[i][i] = 0;
		}

		for(int i = 0; i < N; i++){
			for(int j = 0; j < N; j++){
				if(adjMap[i][j] != 0){
					costMap[i][j] = adjMap[i][j];
				}
			}
		}

		for(int k = 0; k < N; k++){
			for(int i = 0; i < N; i++){
				for(int j = 0; j < N; j++){
					if(costMap[i][k] != Integer.MAX_VALUE && costMap[k][j] != Integer.MAX_VALUE){
						if(costMap[i][k] + costMap[k][j] < costMap[i][j]){
							costMap[i][j] = costMap[i][k] + costMap[k][j];
						}
					}
				}
			}
		}
	}
	 */

	//FIND CONNECTED COMPONENTS
	/*
//key = comp id, 0 based, val = node id's in that comp
	static List<List<Integer>> comps = new ArrayList<List<Integer>>();
static boolean [] isComp;
	public static void findConnectedComponents(){
		for(int vi=0; vi<N; vi++){
			if(!isComp[vi]){
				List<Integer> comp = new ArrayList<Integer>();
				dfs(vi, comp);
				comps.add(comp);
			}
		}
	}

	public static void dfs(int vi, List<Integer> comp){
		comp.add(vi);
		isComp[vi] = true;
		for(int vout =0; vout<N; vout++){
			if(adjMap[vi][vout] >=0 && !isComp[vout]){
				dfs(vout, comp);
			}
		}
	}
	 */

	//DIJKSTRA WITH PQ -> E+ VlogV
	//don't forget to do two ways for undirected!
	/*
  public static int[] shortestPath(int source){
 	int[] distance = new int[P+1];
		boolean[] visited = new boolean[P+1];
		for(int i=0;i<P+1;i++){
			distance[i] = Integer.MAX_VALUE;
		}

		distance[source] = 0;
		//hack. actually node
		PriorityQueue<Node> nextNode = new PriorityQueue<Node>(800, new Comparator<Node>(){
			@Override
			public int compare(Node o1, Node o2) {
				return o1.weight - o2.weight;
			}
		});

		nextNode.add(new Node(source, 0));
		while(!nextNode.isEmpty()){
			Node node = nextNode.remove();
			if(node.weight > distance[node.id]){
				//java doesn't have decrease key operator :(
				//so i put multiple same objects with differing keys
				continue;
			}

			int closestV = node.id;
			visited[closestV] = true;

			List<Edge> outEdges = adjMap.get(closestV);
			if(outEdges == null){
				continue;
			}
			for(Edge outE: outEdges){
				if(!visited[outE.dest] && distance[closestV] + outE.weight < distance[outE.dest]){
					distance[outE.dest] = distance[closestV] + outE.weight;
					nextNode.add(new Node(outE.dest, distance[closestV] + outE.weight));
				}
			}
		}

		return distance;
		}
		public static class Node{
		public int id;
		public int weight;
		public Node(int id, int we){
			this.id = id;
			weight= we;
		}
	}

	public static class Edge{
		public int dest;
		public int weight;

		public Edge(int d, int w){
			dest = d;
			weight= w;
		}
	}
	 */

	/*
 Prim without PQ O(n^2)
		int[] distance = new int[n];
		boolean[] visited = new boolean[n];
		for(int i=0;i<n;i++){
			distance[i] = Integer.MAX_VALUE;
		}

		distance[0] = 0;
		int countVisit = 0;
		int mstWeight = 0;

		while(countVisit < n){
			int closestV = -1;
			int minDistance = Integer.MAX_VALUE;
			for(int i=0;i<n;i++){
				if(!visited[i] && distance[i] < minDistance){
					minDistance = distance[i];
					closestV = i;
				}
			}

			if(closestV == -1){
				break;
			}
			mstWeight += minDistance;
			visited[closestV] = true;
			countVisit++;
			for(Edge e : adjMap[closestV]){
				if(e.weight < distance[e.to]){
					distance[e.to] = e.weight;
				}
			}
		}	
	 */

	/*
 EULER CIRCUIT for undirected graph. If graph contains odd node, must start there.
 first check that graph is connected, and either contains exactly 2 odd nodes, or all even nodes
 	//mutates the graph :D
	public static void findEulerCircuit(Stack<Integer> circuit, int node){
		if(adjMap.get(node).size() == 0){
			circuit.add(node);
		}else{
			List<Integer> neighbors = adjMap.get(node);
			while(neighbors.size() != 0){
				int neighbor = neighbors.get(0);
				//delete edge
				neighbors.remove(0);
				List<Integer> nn = adjMap.get(neighbor);
				nn.remove(nn.indexOf(node));
				findEulerCircuit(circuit, neighbor);
			}
			circuit.add(node);
		}
	}
	 */

	/*
	Euler tour on directed graph:
	 A directed graph has an eulerian circuit if and only if it is connected and each vertex has the same in-degree as out-degree.
A directed graph has an eulerian path if and only if it is connected and each vertex except 2 have the same in-degree as out-degree,
 and one of those 2 vertices has out-degree with one greater than in-degree (this is the start vertex), and the other vertex has 
 in-degree with one greater than out-degree (this is the end vertex).
 I don't have code :(
 Start with an empty stack and an empty circuit (eulerian path).
- If all vertices have same out-degrees as in-degrees - choose any of them.
- If all but 2 vertices have same out-degree as in-degree, and one of those 2 vertices has out-degree with one greater than its in-degree, and the other has in-degree with one greater than its out-degree - then choose the vertex that has its out-degree with one greater than its in-degree.
- Otherwise no euler circuit or path exists.
If current vertex has no out-going edges (i.e. neighbors) - add it to circuit, remove the last vertex from the stack and set it as the current one. Otherwise (in case it has out-going edges, i.e. neighbors) - add the vertex to the stack, take any of its neighbors, remove the edge between that vertex and selected neighbor, and set that neighbor as the current vertex.
Repeat step 2 until the current vertex has no more out-going edges (neighbors) and the stack is empty.
	 */
	
	//return null if exist cycle
	static Queue<Integer> topologicalSort(List<Integer>[] outEdges, int n){
		int[] inCount = new int[n];
		for(int i = 0; i < outEdges.length; i++){
			for(int vo : outEdges[i]){
				inCount[vo]++;
			}
		}

		Queue<Integer> noIncoming = new LinkedList<Integer>();
		for(int i = 0; i < n; i++){
			if(inCount[i] == 0){
				noIncoming.add(i);
			}
		}

		Queue<Integer> order = new LinkedList<Integer>();
		while(!noIncoming.isEmpty()){
			int v = noIncoming.remove();
			order.add(v);
			for(int vo : outEdges[v]){
				inCount[vo]--;
				if(inCount[vo] == 0){
					noIncoming.add(vo);
				}
			}
		}

		if(order.size() == n){
			return order;
		}else{
			return null;
		}
	}

	/*
//coloring. graph of d degree is colorable in d+1 cols
	static boolean colorable(int numcol){
		goodcol = new boolean[numcol+1];
		int[] color = new int[n];
		color[0] = 1;
		for(int i = 1; i < n; i++){
			if(color[i] != 0) continue;
			if(!dfs(i,color, numcol))
				return false;
		}
		return true;
	}

	//pre: node is not colored
	//post: node is colored iff exist valid coloring
	static boolean[] goodcol;
	static boolean dfs(int node, int[] color, int maxcol){
		Arrays.fill(goodcol, true);
		for(int neigh : edges[node]){
			goodcol[color[neigh]] = false;
		}

		for(int col = 1; col <= maxcol; col++){
			if(goodcol[col]){
				boolean good = true;
				color[node] = col;
				for(int neigh : edges[node]){
					if(color[neigh] == 0){
						if(!dfs(neigh, color, maxcol)){
							good = false;
							break;
						}
					}
				}
				if(good){
					return true;
				}
			}
		}
		color[node] = 0;
		return false;
	}
	 */

	/**
	CENTROID of tree = node with smallest largest subtree
	//http://www.stringology.org/event/1999/p5.html
	//proof that centroid is the vertex with N(v) <= N/2
	//at most 2 centroids, is connected to the other centroid
	//search for first node >= totalNodes/2
	static int centroid;
	static void findCentroid(int v, int parent, int totalNodes){
		if(centroid != -1){
			return;
		}
		for(int child : effEdges[v]){
			if(child != parent){
				findCentroid(child, v, totalNodes);
			}
		}
		if(subtreeSize[v] >= totalNodes/2 && centroid == -1){
			centroid = v;
		}
	}
	 */

	/**
Erdos-Gallai theorem.
non-negative d1...dn can be a degree sequence of simple graph iff
1. sum d1...dn is even and
2. for k = 1->n, this holds: sum i=1->k d_i <= k(k-1)+{sum_i=k+1 to n {min d_i, k}}
	 */
}
