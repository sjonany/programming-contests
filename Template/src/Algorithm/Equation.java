package Algorithm;

public class Equation {
	/*
	 to find rank - ref, count non zero rows
	inconsistent when rank of augmented matrix > rank of coefficient matrix
	solution is unique iff rank = # vars, otherwise, k free params where k = # vars - rank
	to solve, ref, check if exist unique, then back substitution
	 */

	//input = augmented matrix, will mutate to almost row echelon form- leading cols not 1
	static void ref(double[][] mat){
		int numRow = mat.length;
		int numCol = mat[0].length;
		int maxRank = Math.min(numRow, numCol);
		for(int pCol = 0; pCol < maxRank; pCol++) {
			int startRow = pCol;
			int maxPivotRowIndex = startRow;
			for(int row = startRow+1; row < numRow; row++) {
				if(Math.abs(mat[row][pCol]) > Math.abs(mat[maxPivotRowIndex][pCol])) {
					maxPivotRowIndex = row;
				}
			}
			if(mat[maxPivotRowIndex][pCol] == 0){
				continue;
			}

			double[] temp = mat[startRow];
			mat[startRow] = mat[maxPivotRowIndex];
			mat[maxPivotRowIndex] = temp;
			
			for(int r = startRow + 1; r < numRow; r++) {
				if(mat[r][pCol] == 0) continue;
				double mul = mat[r][pCol] / mat[startRow][pCol];
				for(int c = pCol; c < numCol; c++) {
					mat[r][c] -= mat[startRow][c] * mul;
				}
			}
		}
	}

	// assume matrix has unique soln and is in almost-ref
	static double[] backSubstitution(double[][] mat) {
		int numCol = mat[0].length;
		int numRow = mat.length;
		double[] x = new double[numCol-1];

		for(int r = numRow-1; r >= 0; r--){
			double sum = 0.0;
			for(int c = numCol-2; c > r; c--) {
				sum += mat[r][c] * x[c];
			}
			x[r] = (mat[r][numCol-1] - sum) / mat[r][r];
		}
		return x;
	}
}
