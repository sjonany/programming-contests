package Algorithm;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class LCA{
	//ping's fast lca template
	/*
	static int[] tin, tout;
	static int[][] up;
	static int timer;
	static int l;
	static int[] depth;

	static void dfs(int v, int p){
		depth[v] = depth[p] + 1;
		tin[v] = ++timer;
		up[v][0] = p;
	    for(int i = 1; i <= l; i++ )
			up[v][i] = up[up[v][i-1]][i-1];
	    
		for(int to : edges[v]){
			if (to != p)
				dfs(to, v);
		}
		tout[v] = ++timer;
	}

	static boolean upper(int a, int b){
		return tin[a] <= tin[b] && tout[a] >= tout[b];
	}

	static int lca(int a, int b){
		if(upper(a, b)) return a;
		if(upper(b, a)) return b;
		for (int i = l; i >= 0; i--)
			if(!upper(up[a][i], b))
				a = up[a][i];
		return up[a][0];
	}

	static void initLCA(int n, int root){
		tin = new int[n];
		tout = new int[n];
		up = new int[n][];
		depth = new int[n];
		
		l = 1;
		while((1 << l) <= n) ++l;
		for (int i = 0; i <n; i++) 
	        up[i] = new int[l + 1];
		depth[root] = -1;
		dfs(root, root);
	}*/
	
	//my slow LCA, and might be buggy.. Idk
	/*
	static List<Integer>[] edges;
	// dp[i][j] = 2^jth ancestor of ith node, dp[i][0] = parent, -1 if not exist
	static List<Integer>[] dp;
	static int[] depth;

	// preprocess lca, O(nlogn)
	static void initLca(int N, int root) {
		depth = new int[N];
		dp = (List<Integer>[]) Array.newInstance(List.class, N);
		for (int i = 0; i < N; i++) {
			dp[i] = new ArrayList<Integer>();
		}
		dfsLca(root, -1);
	}

	static void dfsLca(int cur, int par) {
		if (par == -1) {
			depth[cur] = 0;
		} else {
			depth[cur] = depth[par] + 1;
			// dp[i][j] = dp[dp[i][j-1]][j-1]
			dp[cur].add(par);
			for (int j = 1;; j++) {
				int halfwayNode = dp[cur].get(j - 1);
				if (dp[halfwayNode].size() >= j) {
					dp[cur].add(dp[halfwayNode].get(j - 1));
				} else {
					break;
				}
			}
		}

		for (int vo : edges[cur]) {
			if (vo != par)
				dfsLca(vo, cur);
		}
	}

	// return lca of nodes a and b, O(log n)
	static int lca(int a, int b) {
		int lo = a;
		int hi = b;

		if (depth[lo] < depth[hi]) {
			int temp = lo;
			lo = hi;
			hi = temp;
		}

		if (depth[hi] == 0)
			return hi;

		int diff = depth[lo] - depth[hi];

		// bring lo to hi depth
		int pow = 0;
		while (diff > 0) {
			if ((diff & 1) != 0) {
				lo = dp[lo].get(pow);
			}
			diff >>= 1;
			pow++;
		}

		if (lo == hi)
			return lo;
		for (int log = (int) (Math.log(depth[lo]) + 1); log >= 0; log--) {
			if (dp[lo].size() > log) {
				int nlo = dp[lo].get(log);
				int nhi = dp[hi].get(log);
				if (nlo != nhi) {
					lo = nlo;
					hi = nhi;
				}
			}
		}

		return dp[lo].get(0);
	}*/
}
