package Algorithm;

import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

//more untested stuff at chapter3point4.fence
public class Geometry {
	//ax+by=c
	static class Line {
		public long a;
		public long b;
		public long c;
		
		public Line(long x1, long y1, long x2, long y2){
			long A = y2 - y1;
			long B = x1 - x2;
			long C = -A * x1 - B * y1;
			if(A < 0) {A = -A; B = -B; C = -C;};
			if(A==0 && B < 0) {B= -B; C = -C;}
			this.a = A; this.b = B; this.c = C;
			long g = gcd(a, gcd(b,c));
			a/=g; b/=g; c/=g;
		}
		
		long gcd(long a, long b){ 
			a = Math.abs(a);
			b = Math.abs(b);
			if(a == 0 || b == 0) return a + b;
			while(b > 0) {
				long c = b;
				b = a % b;
				a = c;
			}
			return a;
		}
	}
	
	//note: points must be in clockwise/anti clockwise order
	//no need to close the poly, i will do it for u
	public static Area makePoly(List<double[]> points){
		Path2D.Double poly = new Path2D.Double();
		poly.moveTo(points.get(0)[0], points.get(0)[1]);
		for(int i=1;i<points.size();i++){
			poly.lineTo(points.get(i)[0], points.get(i)[1]);
		}
		poly.closePath();
		return new Area(poly);
	}
	
	//rotation
	//rotate area
	//Area rotRect = oriRect.createTransformedArea(AffineTransform.getRotateInstance(Math.toRadians(a)));
	//rotate point
	//Point rotated = AffineTransform.getRotateInstance(Math.toRadians(-45)).transform(point, null);

	public static double calcPolyArea(Area poly){
		PathIterator points = poly.getPathIterator(null);
		List<double[]> inPoints= new ArrayList<double[]>();
		while(!points.isDone()){
			double[] p = new double[2];
			switch(points.currentSegment(p)){
				case PathIterator.SEG_CLOSE:
					break;
				default:
					inPoints.add(p);
			}
			points.next();
		}
		
		double area = 0.0;
		for(int i=0;i<inPoints.size();i++){
			if(i==inPoints.size()-1){
				area += inPoints.get(i)[0]*inPoints.get(0)[1] - inPoints.get(0)[0]*inPoints.get(i)[1];
			}else{
				area += inPoints.get(i)[0]*inPoints.get(i+1)[1] - inPoints.get(i+1)[0]*inPoints.get(i)[1];
			}
		}
		area = Math.abs(area/2.0);
		return area;
	}
	
	static double calcArea(List<double[]> p) {
	    double area = 0;
	    for(int i = 0; i < p.size(); i++) {
	        int j = (i+1) % p.size();
	        area += p.get(i)[0]*p.get(j)[1] - p.get(j)[0]*p.get(i)[1];
	    }
	    return area / 2.0;
	}
	
	public static double[] crossProduct(double[] v1, double[] v2){
		return new double[]{(v1[1]*v2[2]-v1[2]*v2[1]), 
				(v1[2]*v2[0]-v1[0]*v2[2]),
				(v1[0]*v2[1] - v1[1]*v2[0])};
	}
	
	//0 if on line, -1 or 1 if on some side
	public static int getSideOfLine(Line2D divide, Point2D p1){
		double[] vDiv = new double[]{divide.getX2()-divide.getX1(),
				divide.getY2()-divide.getY1(),0};
		double[] v1 = new double[]{p1.getX()-divide.getX1(),
				p1.getY()-divide.getY1(),0};
		
		double c = vDiv[0] * v1[1] - vDiv[1] * v1[0];
		if(Math.abs(c) < 0.00001){
			return 0;
		}else if(c>0){
			return 1;
		}else{
			return -1;
		}
	}
	
	static boolean isSegmentIntersect(double x1, double y1, double x2, double y2, 
			double wx1, double wy1, double wx2, double wy2){
		return	new Line2D.Double(wx1, wy1, wx2, wy2).intersectsLine(
				new Line2D.Double(x1,y1,x2,y2));
	}
	
	//ccw rotation by a in radian
	static double[] ccw(double a, double[] pt){
		double cos = Math.cos(a);
		double sin = Math.sin(a);
		double x = pt[0];
		double y = pt[1];
		return new double[]{x*cos - y * sin, x * sin + y * cos};
	}
	
	//between v1 and v2, not necessarily v1 swing to v2
	//always between 0 and 2PI
	public static double getAngleBetweenVec(Line2D v1, Line2D v2){
		double dx1 = v1.getX2() - v1.getX1();
		double dy1 = v1.getY2() - v1.getY1();

		double dx2 = v2.getX2() - v2.getX1();
		double dy2 = v2.getY2() - v2.getY1();
		
		double result = Math.atan2(dy2,dx2) - Math.atan2(dy1,dx1);
		if(result < 0) return result + 2.0 * Math.PI;
		return result;
	}
}
