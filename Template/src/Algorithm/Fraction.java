package Algorithm;

class Fraction {
	int num;
	int den;

	public Fraction(int n, int d) {
		this.num = n;
		this.den = d;
		reduce();
	}

	public void reduce() {
		if(den < 0) {
			num*=-1;
			den*=-1;
		}
		if(num == 0){
			den = 1;
			return;
		}
		int g = gcd(Math.abs(num),den);
		num/=g;
		den/=g;
	}

	public Fraction mul (Fraction f){
		return new Fraction(f.num * num, f.den * den);
	}

	public Fraction add (Fraction f) {
		return new Fraction(f.num * den + num * f.den, den * f.den);
	}

	public Fraction sub (Fraction f) {
		return new Fraction(-f.num * den + num * f.den, den * f.den);
	}

	public Fraction div(Fraction f){
		return new Fraction(num * f.den, den * f.num);
	}

	public int gcd(int a, int b) {
		while(b > 0) {
			int c = a % b;
			a = b;
			b = c;
		}
		return a;
	}

	public boolean greater(Fraction f){
		return num * f.den > f.num * den;
	}

	@Override
	public String toString(){
		if(num == 0) return "0";
		if(den == 1) return ""+num;
		return num + "/" + den;
	}
}