package Algorithm;

import java.util.HashSet;
import java.util.Set;

/*
Two players take turns opening closed gates arranged left to right.
first player to open a gate which caused 3 consecutive open gates will win
Which n = #gates will the first player win?
Sprague grandue
 */
public class SpragueGrundy {
	static int MAX_GATE = 100;
	//sg[STATE] = 0 iff cur player loses
	static int[] sg = new int[MAX_GATE+1];
	public static void main(String[] args){
		for(int i=0;i<sg.length;i++){
			sg[i] = -1;
		}
		sg[0] = 0;
		
		for(int numGate =1; numGate<=MAX_GATE;numGate++){
			if(solve(numGate) == 0){
				System.out.println(numGate);
			}
		}
	}

	public static int solve(int numGate){
		if(sg[numGate] != -1){
			return sg[numGate];
		}
		
		Set<Integer> childSg = new HashSet<Integer>();
		//try all possible moves opening some doors
		for(int i=0;i<numGate;i++){
			if(i < 3){
				childSg.add(solve(Math.max(0, numGate-i-3)));
			}else if(i >= numGate-3){
				childSg.add(solve(i-2));
			}else{
				//two games
				childSg.add(solve(i-2) ^ solve(numGate-i-3));
			}
		}
		int mex =0;
		while(childSg.contains(mex)){
			mex++;	
		}
		sg[numGate] = mex;
		return mex;
	}
}
