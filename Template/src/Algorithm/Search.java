package Algorithm;

public class Search {
	public static void main(String[] args){
		//binsearch
		int n=5;
		int[] s = new int[]{0,1,2,3,4,5,6,7,8,9,10};
		int[] arr = new int[]{1,3,5,7,9};

		for(int sp: s){
			System.out.println(sp + " " + binSearch(arr,sp));
		}
	}
	
	public static int binSearch(int[] arr, int s){
		int lo=0;
		int hi=arr.length-1;
		
		//in this case B = val[i] >= s, A = val[i]<s
		//inv: lo...hi contains 'A' -> AAAABB
		if(arr[0]>=s){
			lo = -1;
		}else{
			while(lo < hi){
				int mid = lo + (hi-lo+1)/2;
				if(arr[mid] >= s){
					hi = mid-1;
				}else{
					lo = mid;
				}
			}
		}
		//lo points to highest index where A is true
		//EXCEPT for when everything is BBBBBB, then output still 0.
		//need to check case when arr[0] is B, if so, lo = -1
		return lo;
	}
	
/*
In general, define condition A and B so search space can be AAAAA...BBBBBB
int lo = initLo;
int hi = initHi;
if(f(initLo) == B){
	return -1;
}else{
	while(lo<hi){
		int mid = lo + (hi-lo+1)/2;
		if(f(mid) == B){
			hi = mid-1;
		}else{
		lo= mid;
		}
	}
	return lo;
}	 
	 
*/
}
