package Algorithm;

import java.util.ArrayList;
import java.util.List;

// TEST : UVA 247
// linear alg to find SCC - slower than tarjan, because it does 2 dfs, 
// but makes 2SAT very nice, since the the components are in topo ordering
// Result is stored in comp[] , where comp[i] stores the comp index, >= 0
// Note that the comps in ascending order are in topo order.

// For 2SAT, add (a or b) -> add ~a->b and ~b->a --- CODE AT THE BOTTOM
// if (a) , add ~a->a
// SCC, (scc's must be in topoorder, which kosaraju gives!) -> exist soln iff no a and ~a in same comp
// Each scc has to have the same assignment. If scc has no color, give it T
// Then, the negations must be F. If scc has color already, assign that color
public class KosarajuSCC {
	static List<Integer>[] edges;
	static List<Integer>[] revedges;
	static int[] comp;
	static int[] stack;
	static int si;
	static int compi;
	static boolean[] visit;
	
	static void scc(int n) {
		comp = new int[n];
		stack = new int[n];
		visit = new boolean[n];
		si = 0;
		compi = 0;
		for(int i=0;i<n;i++) {
			if(!visit[i])
				dfs(i);
			if(si == n) break;
		}
		
		si--;
		while(si >= 0) {
			//reuse visit[si] = is on stack
			if(visit[stack[si]]) {
				dfs2(stack[si]);
				compi++;
			}
			si--;
		}
	}
	
	static void dfs(int cur) {
		visit[cur] = true;
		for(int vo : edges[cur]) {
			if(!visit[vo]) dfs(vo);
		}
		stack[si++] = cur;
	}
	
	static void dfs2(int cur) {
		visit[cur] = false;
		comp[cur] = compi;
		for(int vo : revedges[cur]) {
			if(visit[vo]) dfs2(vo);
		}
	}
	
	
	///////////////////////////////////////////////////
	// TWO SAT - See LightOJ / Council1251 for memory efficient impl handling multiple queries
	
	static class TwoSat {
		int n;
		public TwoSat(int n) {
			this.n = n;
			revedges = new List[2*n];
			edges = new List[2*n];
			for(int i=0;i<2*n;i++) {edges[i] = new ArrayList();}
		}
		
		// i1 or i2, if ~i1, pos1 = false
		public void add(boolean pos1, int i1, boolean pos2, int i2) {
			edges[node(!pos1, i1)].add(node(pos2, i2));
			edges[node(!pos2, i2)].add(node(pos1, i1));
		}
		
		//return satisifying assignment, or null if there isn't
		public boolean[] solve() {
			for(int i=0;i<2*n;i++) revedges[i] = new ArrayList();
			for(int i=0;i<2*n;i++){for(int j:edges[i]) revedges[j].add(i);}
			scc(2*n);
			for(int i=0;i<n;i++) {
				if(comp[i*2] == comp[i*2+1]){
					return null;
				}
			}
			List<Integer>[] comps = new List[compi];
			for(int i=0;i<compi;i++)comps[i] = new ArrayList();
			for(int i=0;i<2*n;i++){
				comps[comp[i]].add(i);
			}
			
			int[] compColor = new int[compi];
			boolean[] ans = new boolean[n];
			for(int ci=0;ci<compi;ci++) {
				if(compColor[ci] == 0){
					compColor[ci] = -1;
					for(int vi : comps[ci]){
						ans[vi/2] = false;
						if((vi & 1) == 1) ans[vi/2] = !ans[vi/2];
						compColor[comp[vi^1]] = 1;
					}
				} else {
					for(int vi : comps[ci]){
						ans[vi/2] = compColor[ci] == 1;
						if((vi & 1) == 1) ans[vi/2] = !ans[vi/2];
						compColor[comp[vi^1]] =  -compColor[ci];
					}
				}
			}
			return ans;
		}
		
		static int node(boolean pos, int i) {
			return pos ? i*2 : i*2+1;
		}
		
		static String p(int node) {
			return (node%2==0 ? "+":"-") + (node/2+1);
		}
	}
}
