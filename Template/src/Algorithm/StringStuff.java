package Algorithm;

public class StringStuff {
	
	// O(n), F[m] = i, consider first m chars of s, the length of longest PROPER suffix-prefix is i 
	static int[] buildPrefixFcn(String s){
		int[] F = new int[s.length()+1];
		F[0] = F[1] = 0;
		for(int i = 2; i <= s.length(); i++){
			int j = F[i-1];
			while(true) {
				if(s.charAt(j) == s.charAt(i-1)) {
					F[i] = j+1;
					break;
				}

				if(j == 0) {
					F[i] = 0;
					break;
				}
				j = F[j];
			}
		}
		return F;
	}

	//return index in s where pattern p is found, -1 if not
	static int kmp(char[] s, char[] p){
		int[] F = buildPrefixFcn(new String(s));
		int i = 0;
		int j = 0;
		
		while(true) {
			if(j == s.length) break;
			if(s[j] == p[i]){
				i++;
				j++;
				if(i==p.length){
					return j;
				}else if( i > 0){
					i = F[i];
				}else{
					j++;
				}
			}
		}
		return -1;
	}
	
	//which state will I be if at state i, and encounter char c, assume UPPERCASE
	static int[][] buildAutomaton(String s) {
		int[] F = buildPrefixFcn(s);
		int[][] auto = new int[F.length][26];

		auto[0][s.charAt(0)-'A'] = 1;
		for(int i = 1; i < F.length; i++){
			System.arraycopy(auto[F[i]], 0, auto[i], 0, 26);
			if(i != s.length())
				auto[i][s.charAt(i)-'A'] = i+1;
		}
		return auto;
	}
	
	//Z[i] is the length of the longest substring starting from S[i] 
	//which is also a prefix of S
	
	public static int[] zAlg(char[] s){
		int[] z = new int[s.length];
		int n = s.length;
		int L = 0, R = 0;
		for (int i = 1; i < n; i++) {
		  if (i > R) {
		    L = R = i;
		    while (R < n && s[R-L] == s[R]) R++;
		    z[i] = R-L; R--;
		  } else {
		    int k = i-L;
		    if (z[k] < R-i+1) z[i] = z[k];
		    else {
		      L = i;
		      while (R < n && s[R-L] == s[R]) R++;
		      z[i] = R-L; R--;
		    }
		  }
		}
		return z;
	}
	
	//return the length of the longest palindrome substring in input
	static int manacher(String input) {
		StringBuilder sb = new StringBuilder();
		sb.append("#");
		for (int i = 0; i < input.length(); i++) {
			sb.append(input.charAt(i));
			sb.append("#");
		}

		String str = sb.toString();

		// center and rightmost of the current longest palindrome
		// invariant: at the beginning of the loop, c and r defines the longest palindrome, with c < i
		int c = 0;
		int r = 0;
		// length of longest palindrome centered at i in the original string
		int[] P = new int[str.length()];

		// longest.length - 1 because we don't care about the stub character
		for (int i = 1; i < P.length - 1; i++) {
			int im = c - (i - c);
			P[i] = (r > i) ? Math.min(r - i, P[im]) : 0;

			// Attempt to expand palindrome centered at i
			int nr = i + 1 + P[i];
			int nl = i - 1 - P[i];
			while (nl >= 0 && nr < str.length() && str.charAt(nl) == str.charAt(nr)){
				P[i]++;
				nr++;
				nl--;
			}

			// If palindrome centered at i expand past R,
			// adjust center based on expanded palindrome.
			if (i + P[i] > r) {
				c = i;
				r = i + P[i];
			}
		}

		int max = 0;
		int centerIndex = 0;
		for (int i = 0; i < str.length(); i++) {
			if (P[i] > max) {
				max = P[i];
				centerIndex = i;
			}
		}

		// System.out.println(input.substring((centerIndex - 1 - max) / 2,
		// max));
		return max;
	}
}
