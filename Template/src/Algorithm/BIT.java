package Algorithm;

//fenwick tree, binary indexed tree
//INDEX is ONE-BASED, not 0-based
//range query, point update
class Fenwick1{
	long[] tree;
	public Fenwick1(int n){
		tree = new long[n+1];
	}

	public void increment(int i, long val){
		while(i < tree.length){
			tree[i] += val;
			i += (i & -i);
		}
	}

	public long getTotalFreq(int i){
		long sum = 0;
		while(i > 0){
			sum += tree[i];
			i -= (i & -i);
		}
		return sum;
	}

	// get largest value with cumulative sum less than or equal to x;
	// for smallest, pass x-1 and add 1 to result
	public int getind(int x) {
		int N = tree.length-1;
		int idx = 0, mask = N;
		while(mask>0 && idx < N) {
			int t = idx + mask;
			if(x >= tree[t]) {
				idx = t;
				x -= tree[t];
			}
			mask >>= 1;
		}
		return idx;
	}
}

//range update point query
class Fenwick2{ 
	public Fenwick1 tree;
	
	public Fenwick2(int n){
		tree = new Fenwick1(n);
	}
	
	public long query(int i) {
		return tree.getTotalFreq(i);
	}
	
	public void update(int i, int j, long v){
		tree.increment(i, v);
		tree.increment(j+1, -v);
	}
}

//range update range query
class Fenwick3 {
	Fenwick2 mul;
	Fenwick2 add;
	int n;
	
	public Fenwick3(int n) {
		mul = new Fenwick2(n);
		add = new Fenwick2(n);
		this.n = n;
	}
	
	public void update(int i, int j, long v) {
		mul.update(i, j, v);
		add.update(i, j, (1-i) * v);
		add.update(j+1, n, (j-i+1) * v);
	}
	
	public long query(int i, int j) {
		return query(j) - query(i-1);
	}
	
	public long query(int i) {
		return mul.query(i) * i + add.query(i);
	}
}

// 2D fenwick
/*
static int[][] tree;
static int N;

//range query
static int query(int x, int y){
	int sum = 0;
	for(int i = x; i > 0; i -= (i & -i)){
		for(int j = y; j > 0; j -= (j & -j)){
			sum += tree[i][j];
		}
	}
	return sum;
}

//point update
static void update(int x, int y, int val){
	for (int i = x; i <= N; i += (i & -i)) {
		for (int j = y; j <= N; j += (j & -j)) {
			tree[i][j] += val;
		}
	}
}
*/