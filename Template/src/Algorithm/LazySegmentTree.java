package Algorithm;

//@see CF round 185 Interval cubing 
//@see POJ 3468
//if change assoc fcn,
//1. change merge
//2. change res in private query()
public class LazySegmentTree {
	//any associative fcn that a parent stores over 2 children
	private int merge(int val1, int val2){
		return val1 + val2;
	}
	//an overridable value, where any value of node, when merged with this, 
	//will return the value of that node
	//for sum, use 0, for min, -inf
	static int DEFAULT = 0;

	//1-based
	int[] tree;
	int[] lazy;

	//num els in the original array
	int n;
	public LazySegmentTree(int n){
		this.n = n;
		//num nodes in tree = 2*n-1 <= 2*2^ceil(log_2 (n)) - 1 
		int numNodes = 2 * (int)(Math.pow(2,Math.ceil(Math.log(n)/Math.log(2))))-1;
		tree = new int[numNodes+1];
		lazy = new int[numNodes+1];
	}

	public LazySegmentTree(int[] arr){
		this(arr.length);
		buildTree(1, 0, arr.length-1, arr);
	}

	//a and b are 0-based indices of the original array
	private void buildTree(int node, int a, int b, int[] arr){
		if(a > b) return;

		//leaf node
		if(a == b){
			tree[node] = arr[a];
			return;
		}
		int mid = (a+b)/2;
		buildTree(2*node, a, mid, arr);
		buildTree(2*node+1, mid+1, b, arr);

		tree[node] = merge(tree[2*node], tree[2*node+1]);
	}

	/**
	 * perform operation on i->j based on original array index
	 */
	public void update(int i, int j, int val){
		update(1, 0, n-1, i, j,val);
	}

	public int query(int i, int j){
		return query(1,0,n-1,i,j);
	}
	
	//guarantee: i,j is between a,b. a,b, is current node's range
	/**
	 * Increment elements within range [i, j] with value value
	 */
	private void update(int node, int a, int b, int i, int j, int value) {
		if(a == i && b == j){
			lazy[node] += value;
			return;
		}else{
			lazy[node * 2] += lazy[node];
			lazy[node * 2 + 1] += lazy[node];
			lazy[node] = 0;
			
			int mid = (a+b)/2;
			if(i <= mid) update(node * 2, a, mid, i, Math.min(j, mid), value);
			if(j > mid) update(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j, value);
			tree[node] = merge(val(2*node, a , mid) ,val(2*node+1, mid+1, b));
		}
	}

	/**
	 * Query tree to get max element value within range [i, j]
	 */
	private int query(int node, int a, int b, int i, int j) {
		if(a == i && b == j){
			return val(node, a, b);
		}
		else {
			lazy[node * 2] += lazy[node];
			lazy[node * 2 + 1] += lazy[node];
			lazy[node] = 0;
			
			int res = DEFAULT;
			int mid = (a+b)/2;

			if(i <= mid) res = merge(res, query(node * 2, a, mid, i, Math.min(j, mid)));
			if(j > mid) res = merge(res, query(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j));
			tree[node] = merge(val(2*node, a, mid) ,val(2*node+1, mid+1,b));
			return res;
		}
	}
	
	//get the true value of the node, lazy or not
	//left-> right = this node's responsibility
	private int val(int node, int left, int right){
		return tree[node] + lazy[node] * (right-left+1);
	}
}