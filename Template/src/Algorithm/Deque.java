package Algorithm;
//non circular deque, optimized becuase don't have to handle circular
public class Deque{
	public int[] deq;
	public int front;
	public int back;
	public int cap;
	public int dsize;

	public Deque(int cap) {
		this.deq = new int[cap];
		this.cap = cap;
		reset();
	}

	public void reset() {
		front = 0;
		back = -1;
		dsize = 0;
	}

	public void addBack(int i) {
		dsize++;
		back++;
		//if(back==cap)back = 0;
		deq[back] = i;
	}

	public void addFront(int i) {
		dsize++;
		front--;
		//if(front<0)front=cap-1;
		deq[front] = i;
	}

	public int removeFront() {
		dsize--;
		int it = deq[front];
		front++;
		//if(front==cap)front=0;
		return it;
	}

	public int removeBack() {
		dsize--;
		int it = deq[back];
		back--;
		//if(back<0)back=cap-1;
		return it;
	}
}