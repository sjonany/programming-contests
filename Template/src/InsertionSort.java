import java.util.Arrays;


public class InsertionSort implements Sorter{

	@Override
	public void sort(int[] arr) {
		for(int i = 0 ; i < arr.length; i++){
			int cur = i;
			int ori = arr[i];
			while(cur != 0 && ori < arr[cur-1]){
				arr[cur] = arr[cur-1];
				cur--;
			}
			
			arr[cur] = ori;
		}
	}
	
}
