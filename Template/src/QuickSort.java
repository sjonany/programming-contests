import java.util.Arrays;


public class QuickSort implements Sorter{

	@Override
	public void sort(int[] arr) {
		sort(arr,0,arr.length-1);
	}
	
	private void sort(int[] arr, int l, int r){
		if(l>=r)return;
		
		//actually med 3 better, and this is no better than the l/r, but want to practice
		//if index not in place
		int pIndex = (l+r)/2;
		int pivot = arr[pIndex];
		
		//save pivot
		arr[pIndex] = arr[l];
		arr[l] = pivot;
		
		int curL = l;
		int curR = r+1;
		
		//begin loop, condition = l+1 to curL inclusive <= pivot, r->curR inclusive > pivot 
		while(curL < curR - 1){
			if(arr[curL+1] <= pivot){
				curL++;
			}else{
				//swap with curR,but don't advance
				int temp = arr[curL+1];
				arr[curL+1] = arr[curR-1];
				arr[curR-1] = temp;
				curR--;
			}
		}
		
		arr[l] = arr[curL];
		arr[curL] = pivot;
		
		//pivot is located in curL
		sort(arr, l, curL-1);
		sort(arr, curL+1, r);
	}
	
}
