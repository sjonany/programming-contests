import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

/**
 * Find an english word-pair of the form : 1 2 3 4 - 1 2 5 1
 * Where 1,2,3,4,5 represent 5 different letters.
 * @author sjonany
 */

public class CrypExtraCredit {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/usr/share/dict/words"));
		Set<String> fourLetters = new HashSet<String>();

		String line = in.readLine();
		while(line != null) {
			if(line.length() == 4)
				fourLetters.add(line.toLowerCase());
			line = in.readLine();
		}

		// There are 4994 words: System.out.println(fourLetters.size());
		// OK.. fast enough to do n^2..
		boolean[] seen = new boolean[26];
		int numPossibilities = 0;
		for(String s1 : fourLetters) {
			for(String s2 : fourLetters) {
				if(s1.charAt(0) == s2.charAt(0) && s2.charAt(3) == s2.charAt(0) // 1
						&& s1.charAt(1) == s2.charAt(1) // 2
						) {
					// count the total number of unique chars in s1 U s2
					for(int i = 0; i < s1.length(); i++){
						seen[s1.charAt(i) - 'a'] = true;
					}

					for(int i = 0; i < s2.length(); i++){
						seen[s2.charAt(i) - 'a'] = true;
					}

					int count = 0;
					for(int i = 0; i < 26; i++) {
						if(seen[i]) {
							count++;
							seen[i] = false;
						}
					}
					
					// if count == 5, this is a match!
					if(count == 5){
						System.out.println(s1 + " " + s2);
						numPossibilities++;
					}
				}
			}	
		}
		// Total possibilities = 3394
		System.out.println("Total possibilities = " + numPossibilities);
		System.out.println("No... too many possibilities :(");
		in.close();
	}
}
