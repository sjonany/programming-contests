import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class ColorfulChocolates {
	public static void main(String[] args) throws Exception {
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		out.println(maximumSpread("QASOKZNHWNFODOQNHGQKGLIHTPJUVGKLHFZTGPDCEKSJYIWFOO",77));
		out.close();
	}
	
	public static int maximumSpread(String chocolates, int maxSwaps){
		int ans = -1;
		char[] arr = new char[chocolates.length()];
		for(int i = 0; i < arr.length; i++){
			arr[i] = chocolates.charAt(i);
		}
		for(int i = 0; i < chocolates.length(); i++){
			char c = arr[i];
			int curSwap = 0;
			int lclump = i;
			int rclump = i;
			int lastl = i;
			int lastr = i;
			while(true){
				int lFound = -1;
				for(int nl = lastl-1; nl >= 0; nl--){
					if(arr[nl] == c){
						lFound = nl;
						break;
					}
				}
				
				int rFound = -1;
				for(int nr = lastr+1; nr < arr.length; nr++){
					if(arr[nr] == c){
						rFound = nr;
						break;
					}
				}
				
				int lCost = lFound == -1 ? 100000 : lclump - lFound - 1;
				int rCost = rFound == -1 ? 100000 : rFound - rclump - 1;
				if(curSwap + Math.min(lCost, rCost) > maxSwaps) break;
				if(lCost > rCost){
					rclump++;
					lastr = rFound;
					curSwap += rCost;
				}else{
					lclump--;
					lastl = lFound;
					curSwap += lCost;	
				}
			}
			
			ans = Math.max(ans, rclump - lclump + 1);
		}
		return ans;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
