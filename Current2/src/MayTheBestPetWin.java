public class MayTheBestPetWin {
	public static void main(String[] args){
		int[]A = new int[]{1,3,5,4,5};
		int[]B = new int[]{2,5,6,8,7};
		System.out.println(calc(A,B));
	}
	
	public static int calc(int[] A, int[] B){
		int[] C = new int[A.length];
		int sumC = 0;
		int sumA = 0;
		for(int i = 0 ; i < C.length; i++){
			C[i] = A[i] + B[i];
			sumC += C[i];
			sumA += A[i];
		}
		
		int MAXV = 50*10000*2+1;
		boolean[][] dp = new boolean[2][MAXV];
		int prev = 0;
		int next = 1;
		dp[prev][0] = true;
		dp[prev][C[0]] = true;
		int prevMaxTrue = C[0];
		for(int i = 1; i < C.length; i++){
			int curMaxTrue = prevMaxTrue;
			for(int j = 0; j <= MAXV; j++) {
				if(j - C[i] > prevMaxTrue){
					break;
				}
				dp[next][j] = dp[prev][j];
				if(j - C[i] >= 0) {
					dp[next][j] |= dp[prev][j - C[i]];
				}
				if(dp[next][j]) {
					curMaxTrue = j;
				}
			}
			prevMaxTrue = curMaxTrue;
			next ^= 1;
			prev ^= 1;
		}
		
		for(int j = (sumC+1) / 2; j <= sumC; j++){
			if(dp[prev][j]){
				return j - sumA;
			}
		}
		return -1;
	}
}
