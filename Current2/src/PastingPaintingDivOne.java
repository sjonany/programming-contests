public class PastingPaintingDivOne {
	public long[] countColors(String[] b, int T){
		char[][] prev = new char[b.length][b[0].length()];
		
		char[][] board = new char[b.length][b[0].length()];
		for(int r = 0; r < b.length; r++){
			for(int c = 0; c < b[0].length(); c++){
				board[r][c] = b[r].charAt(c);
				prev[r][c] = board[r][c];
			}	
		}
		
		//delta[i] = difference from ith to i+1
		int[][] delta = new int[51][26];
		
		for(int iter = 0; iter <= 50; iter++){
			char[][] next = new char[board.length][board[0].length];
			for(int r = 0; r < board.length; r++){
				for(int c = 0; c < board[0].length; c++){
					if(board[r][c] != '.' || (r+1 >= board.length) || c+1 >= board[0].length){
						next[r][c] = board[r][c];
					}else{
						next[r][c] = prev[r+1][c+1];
					}
				}	
			}
			
			for(int r = 0; r < board.length; r++) {
				for(int c = 0; c < board[0].length; c++) {
					if(r >= 1 && c >= 1){
						if(prev[r][c] != '.') {
							delta[iter][prev[r][c] - 'A']--;
						}
					}
					if(next[r][c] != '.') {
						delta[iter][next[r][c] - 'A']++;
					}
				}	
			}
			prev = next;
		}
		
		long[] ans = new long[26];
		
		for(int r = 0; r < board.length; r++){
			for(int c = 0; c < board[0].length; c++){
				if(board[r][c] != '.')
					ans[board[r][c]-'A']++;
			}	
		}
		
		for(int t = 0; t < T - 1; t++){
			if(t >= 50) break;
			for(int i = 0; i < 26; i++) {
				ans[i] += delta[t][i];
			}
		}
		
		if(T > 50) {
			for(int i = 0; i < 26; i++) {
				ans[i] += (long) (T - 51) * delta[50][i];
			}
		}
		
		return new long[]{ans['R'-'A'], ans['G'-'A'], ans['B'-'A']};
	}
}
