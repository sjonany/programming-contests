
public class LittleElephantAndIntervalsDiv1 {
	public long getNumber(int M, int[] L, int[] R) {
		long ans = 1;

		for(int round = R.length-1; round >= 0; round--) {
			if(L[round] > R[round]) continue;
			if(M == 0){
				return ans;
			}

			ans *= 2;
			int it = R[round] - L[round] + 1;
			M -= it;
			for(int i = 0; i < round; i++) {
				if(L[i] >= L[round] && L[i] <= R[round]) {
					L[i] = R[round] + 1;
				}

				if(R[i] >= L[round] && R[i] <= R[round]) {
					R[i] = L[round] - 1;
				}
				
				if(L[i] >= R[round]) {
					L[i] -= it;
				}
				
				if(R[i] >= R[round]) {
					R[i] -= it;
				}
			}
		}

		return ans;
	}
}
