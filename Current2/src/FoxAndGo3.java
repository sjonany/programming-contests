import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FoxAndGo3 {	
	static Map<Node, Integer> index;
	static int id = 0;
	
	static char BLACK = 'x';
	static char WHITE = 'o';
	static char EMPTY = '.';
	
	static int[][] dirs = new int[][]{{0,1},{1,0},{-1,0},{0,-1}};
	public int maxEmptyCells(String[] board) {
		int empty = 0;
		index = new HashMap<Node, Integer>();
		
		char[][] b = new char[board.length][board[0].length()];
		int R = b.length;
		int C = b[0].length;
		
		for(int r = 0; r < board.length; r++) {
			for(int c = 0; c < board[r].length(); c++) {
				b[r][c] = board[r].charAt(c);
			}
		}

		FF maxflow = new FF(3000);
		int S = 0;
		int T = 1;
		id = 2;
		
		for(int r = 0; r < b.length; r++) {
			for(int c = 0; c < b[r].length; c++) {
				if(b[r][c] == WHITE) {
					boolean allBlack = true;
					for(int[] dir : dirs) {
						int nr = r + dir[0];
						int nc = c + dir[1];
						if(nr >= 0 && nc >= 0 && nr < R && nc < C && b[nr][nc] != BLACK){
							allBlack = false;
							break;
						}
					}
					if(allBlack) {
						empty++;
					} else {
						Node source = new Node(r, c);
						int sourceId = id;
						if(!index.containsKey(source)) {
							maxflow.addEdge(S, sourceId, 1);
							index.put(source, id);
							id++;
						}
						
						for(int[] dir : dirs) {
							int nr = r + dir[0];
							int nc = c + dir[1];
							if(nr >= 0 && nc >= 0 && nr < R && nc < C && b[nr][nc] == EMPTY){
								Node dest = new Node(nr, nc);
								if(!index.containsKey(dest)) {
									index.put(dest, id);
									maxflow.addEdge(id, T, 1);
									id++;
								}
								int destId = index.get(dest);
								maxflow.addEdge(sourceId, destId, 1);
							}
						}
					}
				} else if(b[r][c] == EMPTY){
					boolean hasWhite = false;
					for(int[] dir : dirs) {
						int nr = r + dir[0];
						int nc = c + dir[1];
						if(nr >= 0 && nc >= 0 && nr < R && nc < C && b[nr][nc] == WHITE){
							hasWhite = true;
							break;
						}
					}
					if(!hasWhite) {
						empty++;
					}
				}
			}	
		}
		return empty + id - 2 - maxflow.getMaximumFlow(S, T);
	}
	
	private static class FF {
	    public static class FlowEdge {
			public int dest;
	        public int capacity;
	        //flow left allowed, NOT currently flowing
	        public int flow;
	        public FlowEdge reverse;
	        
	    	public FlowEdge(int v, int capacity, int flow) {
				this.dest = v;
				this.capacity = capacity;
				this.flow = flow;
			}
	    }
	 
	    private int N; // node from 0 to N - 1
	    public List<FlowEdge>[] adjMap;
	    int mark;   // global variable for checking if a node is already visited
	    int[] seen;  // status of each node
	 
	    FF(int numNode){
	        N = numNode;
	        adjMap = (List[]) new List[N];
	        for(int i = 0; i < adjMap.length; i++){
	        	adjMap[i] = new ArrayList<FlowEdge>();
	        }
	        
	        seen = new int[N];
	        mark = 0;
	    }
	 
	    public void addEdge(int from, int to, int cap){
	    	FlowEdge forward = new FlowEdge(to, cap, cap);
	    	FlowEdge back = new FlowEdge(from, 0, 0);
	    	adjMap[from].add(forward);
	        adjMap[to].add(back);
	        forward.reverse = back;
	        back.reverse = forward;
	    }
	    //OPTIONAL
	    public void resetFlow(){
	        for(int i = 0; i < N; i++ ){
	            int sz = adjMap[i].size();
	            for(int j = 0; j < sz; j++ ){
	            	FlowEdge e = adjMap[i].get(j);
	            	e.flow = e.capacity;
	            }
	        }
	    }
	 
	    private int findAugmentingPath(int at, int sink, int val){
	        int sol = 0;
	        seen[at] = mark;
	        if(at == sink) return val;
	        int sz = adjMap[at].size();
	        for(int i = 0; i < sz; i++ ){
	            FlowEdge flow = adjMap[at].get(i);
	            int v = flow.dest;
	            int f = flow.flow;
	            if(seen[v] != mark && f > 0){
	                sol = findAugmentingPath(v, sink, Math.min(f, val));
	                if(sol > 0){
	                    flow.flow -= sol;
	                    flow.reverse.flow += sol;
	                    return sol;
	                }
	            }
	        }
	        return 0;
	    }
	 
	    public int getMaximumFlow(int S, int T){
	        int res = 0;
	        while(true){
	            mark++;
	            int flow = findAugmentingPath(S, T, Integer.MAX_VALUE);
	            if(flow == 0) break;
	            else res += flow;
	        }
	        return res;
	    }
	};
	
	static class Node {
		public int r;
		public int c;
		public Node(int r, int c) {
			this.r = r;
			this.c = c;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + c;
			result = prime * result + r;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (c != other.c)
				return false;
			if (r != other.r)
				return false;
			return true;
		}
	}
}
