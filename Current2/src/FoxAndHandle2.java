
public class FoxAndHandle2 {
	
	public String lexSmallestName(String S){
		int[] count = new int[26];
		for(int i = 0; i < S.length(); i++){
			count[S.charAt(i)- 'a']++;
		}
		int[] discard = new int[26];
		for(int i = 0; i < 26; i++){
			discard[i] = count[i] / 2;
		}
		
		return lexSmallestName(S, discard);
	}
	
	String lexSmallestName(String s, int[] req){
		int sum = 0;
		for(int i = 0; i < req.length; i++){
			sum += req[i];
		}
		if(sum == 0)return "";
		for(int i = 0; i < 26; i++){
			if(req[i] == 0) continue;
			int found = -1;
			for(int j = 0; j < s.length(); j++){
				if(s.charAt(j)-'a' == i){
					found = j;
					break;
				}
			}
			if(found==-1)continue;
			int[] count = new int[26];
			for(int k = found+1; k < s.length();k++){
				count[s.charAt(k)- 'a']++;
			}
			req[i] --;
			boolean good = true;
			for(int k = 0; k < 26; k++){
				if(req[k] > count[k]){
					good = false;
					break;
				}
			}
			if(good){
				return ""+(char)('a'+i) + lexSmallestName(s.substring(found+1), req);
			}
			req[i]++;
		}
		
		return "";
	}
}
