import java.util.Scanner;


public class Timecodes {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		String line = reader.nextLine().trim();
		String[] tokens = line.split(" ");
		int framerate = Integer.parseInt(tokens[0]);
		int numFrames =  Integer.parseInt(tokens[1])-1;
		
		int h = (int)(numFrames / 3600.0 / framerate);
		numFrames -= h * 3600 * framerate; 
		int m = (int)(numFrames / 60.0 / framerate);
		numFrames -= m * 60 * framerate; 
		int s = (int)(numFrames / framerate);
		numFrames -= s * framerate;
		int f = numFrames;
		
		System.out.println(format(h) + ":" + format(m) + ":" + format(s) + ":" + format(f));
	}
	
	public static String format(int k){
		if(k%10 == k) return ("0" + k);
		return "" + k;
	}
}
