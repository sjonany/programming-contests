import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;


public class CheapskateElectrician {
	public static void main(String[] args){
		HashSet<String> colors = new HashSet<String>();
		Scanner s = new Scanner(System.in);
		
		String input = "";
		int count = 0;
		while(s.hasNextLine()){
			String line = s.nextLine().trim();
			if(line.length()==0)break;
			input += line + "\n";
			String[] tokens = line.split(",");
			String c1 = tokens[0];
			String c2 = tokens[1];
			colors.add(c1);
			colors.add(c2);
			count++;
		}
		
		int extension = 0;
		HashSet<ColorPair> colorTrails = new HashSet<ColorPair>();
		
		for(String color : colors){
			colorTrails.add(new ColorPair(color,color));
		}
		
		while(extension <= count){
			Scanner reader = new Scanner(input);
			HashSet<ColorPair> newPairs = new HashSet<ColorPair>();
			while(reader.hasNextLine()){
				String line = reader.nextLine().trim();
				if(line.length()==0)break;
				String[] tokens = line.split(",");
				String c1 = tokens[0];
				String c2 = tokens[1];
				for(ColorPair pair : colorTrails){
					newPairs.add(new ColorPair(c1,c2));
					newPairs.add(pair);
					if(pair.c1.equals(c1)){
						ColorPair newPair1 = new ColorPair(pair.c2,c2);
						ColorPair newPair2 = new ColorPair(c2,pair.c2);
						if(isRight(newPair1) || isRight(newPair2)){
							System.out.println(extension +1);
							return;
						}
						newPairs.add(newPair1);
						newPairs.add(newPair2);
					}
					if(pair.c1.equals(c2)){
						ColorPair newPair1 = new ColorPair(c1,pair.c2);
						ColorPair newPair2 = new ColorPair(pair.c2,c1);
						if(isRight(newPair1) || isRight(newPair2)){
							System.out.println(extension +1);
							return;
						}
						newPairs.add(newPair1);
						newPairs.add(newPair2);
					}
					if(pair.c2.equals(c1)){
						ColorPair newPair1 = new ColorPair(pair.c1,c2);
						ColorPair newPair2 = new ColorPair(c2,pair.c1);
						if(isRight(newPair1) || isRight(newPair2)){
							System.out.println(extension +1);
							return;
						}
						newPairs.add(newPair1);
						newPairs.add(newPair2);
					}
					if(pair.c2.equals(c2)){
						ColorPair newPair1 = new ColorPair(c1,pair.c1);
						ColorPair newPair2 = new ColorPair(pair.c1,c1);
						if(isRight(newPair1) || isRight(newPair2)){
							System.out.println(extension +1);
							return;
						}
						newPairs.add(newPair1);
						newPairs.add(newPair2);
					}
				}
			}
			colorTrails = newPairs;
			extension++;
		}
		
		System.out.println(-1);
		return;
	}
	
	public static boolean isRight(ColorPair pair){
		return pair.c1.equals( "pink") && pair.c2.equals("orange");
	}
	
	public static class ColorPair{
		public String c1;
		public String c2;
		
		public ColorPair(String c1, String c2){
			this.c1 = c1;
			this.c2 = c2;
		}
		
		@Override
		public int hashCode(){
			return c1.hashCode() + c2.hashCode();
		}
		
		@Override
		public boolean equals(Object o){
			ColorPair other = (ColorPair)o;
			return other.c1.equals(c1) && other.c2.equals(c2);
		}
		
		@Override
		public String toString(){
			return c1 + " " + c2;
		}
	}
}
