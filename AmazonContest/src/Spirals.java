import java.util.Scanner;


public class Spirals {
	public static void main(String[] args){
		try{
		Scanner reader = new Scanner(System.in);
		String line = reader.nextLine().trim();
		String[] tokens = line.split(" ");
		int rowCount = Integer.parseInt(tokens[0]);
		int colCount = Integer.parseInt(tokens[1]);
		if(rowCount < 0 || colCount < 0){
			System.out.println("Unexpected end of input");
			return;
		}
		int[][] maze = new int[rowCount][colCount];
		boolean[][] touched = new boolean[rowCount][colCount];
		
		for(int i=0; i<rowCount; i++){
			for(int j=0; j<colCount; j++){
				touched[i][j] = false;
			}
		}
		try{
			for(int row=0; row < rowCount; row++){
				String curLine = reader.nextLine().trim();
				if(curLine.length() == 0){
					System.out.println("Unexpected end of input");
					return;
					
				}
				String[] token = curLine.split(" ");
				if(token.length < colCount){
					System.out.println("Unexpected end of input");
					return;
				}
				
				for(int col=0; col < colCount; col++){
					maze[row][col] = Integer.parseInt(token[col]);
				}
			}
		}catch(Exception e){
			System.out.println("Unexpected end of input");
			return;
		}
		String result = "";
		
		int col = 0;
		int row = 0;
		if(rowCount == 0 || colCount == 0){
			System.out.println("  ");
			return;
		}
		result += (maze[row][col] + " ");
		touched[row][col] = true;
		
		int count = 1;
		int dir = 0;
		//0=right 1 = down 2= left 3= up
		try{
			while(true){
				if(deadend(touched, row, col, rowCount, colCount)){
					System.out.println(result + " ");
					return;
				}
				boolean stuck = false;
				int newRow=-123;
				int newCol=-123;
				switch(dir){
				case 0:
					newCol = col+1;
					if(newCol >= colCount || touched[row][newCol]){
						stuck = true;
					}else{
						col = newCol;
						result += maze[row][col] + " ";
						touched[row][col] = true;
						count++;
					}
					break;
				case 1:
					newRow = row+1;
					if(newRow >= rowCount || touched[newRow][col]){
						stuck = true;
					}else{
						row = newRow;
						result += maze[row][col] + " ";
						touched[row][col] = true;
						count++;
					}
					break;
				case 2:
					newCol = col-1;
					if(newCol <= -1 || touched[row][newCol]){
						stuck = true;
					}else{
						col = newCol;
						result += maze[row][col] + " ";
						touched[row][col] = true;
						count++;
					}
					break;
				case 3:
					newRow = row-1;
					if(newRow <= -1 || touched[newRow][col]){
						stuck = true;
					}else{
						row = newRow;
						result += maze[row][col] + " ";
						touched[row][col] = true;
						count++;
					}
					break;
				}
					
				if(stuck)
					dir = (dir+1) % 4;
			}
		}catch(Exception e){
			System.out.println("Unexpected end of input");
			return;
		}
		}catch(Exception e){
			System.out.println("Unexpected end of input");
			return;
		}
		
	}
	
	private static boolean deadend(boolean[][]touched, int row, int col, int numRow, int numCol){
		boolean leftDeadend = (col-1 <0) || (touched[row][col-1]==true);
		boolean upDeadend = (row-1 <0) || (touched[row-1][col]==true);
		boolean rightDeadend = (col+1 >=numCol) || (touched[row][col+1]==true);
		boolean downDeadend = (row+1 >= numRow) || (touched[row+1][col]==true);
		return leftDeadend && upDeadend && rightDeadend && downDeadend;
	}	
}
