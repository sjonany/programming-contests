import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;


public class CheapskateElectrician2 {
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		List<String[]> connections = new ArrayList<String[]>();
		
		while(s.hasNextLine()){
			String line = s.nextLine().trim();
			if(line.length()==0)break;
			String[] tokens = line.split(",");
			String c1 = tokens[0];
			String c2 = tokens[1];
			connections.add(new String[]{c1,c2});
			connections.add(new String[]{c2,c1});
		}
		System.out.println(getMinConnect(0, "orange", connections));
	}
	
	/**
	 * @param step the current number of extensions made has made me reach this color
	 * @param color
	 * @param connections
	 * @return given that I have made this step & reached this color,'
	 * 		what is the minimum number of total steps that will get me to pink
	 */
	private static int getMinConnect(int step, String color, List<String[]> connections){
		if(color.equals("pink"))
			return step;
		if(step >= connections.size())
			return -1;
		int minStep = Integer.MAX_VALUE;
		int totalStep = -1;
		for(String[] connection : connections){
			if(connection[0].equals(color)){
				totalStep = getMinConnect(step+1,connection[1], connections);
			}
			
			if(totalStep != -1 && minStep > totalStep)
				minStep = totalStep;
			
			if(connection[1].equals(color)){
				totalStep = getMinConnect(step+1,connection[0], connections);
			}
			
			if(totalStep != -1 && minStep > totalStep)
				minStep = totalStep;
		}
		
		if(minStep == Integer.MAX_VALUE)
			return -1;
		else 
			return minStep;
	}
}
