import java.util.Scanner;


public class Gifts {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		double total = 0.0;
		
		String line = reader.nextLine().trim();
		if(line.length()!=0){
			int day = Integer.parseInt(line);
			for(int i=1; i<= day; i++){
				double cost = getMoney(reader.nextLine().trim());
				total += cost * i * (day-i+1);
			}
		}
		
		System.out.println("$" + String.format("%.2f", total));
	}
	
	private static double getMoney(String s){
		return Double.parseDouble(s.substring(1));
	}
}
