// codility oxygenium n log^2
// find number of subarrays whose max-min <= K
// opt soln is O(n) -> two pointer + min max dequeue
public class SolutionB {
	public int solution(int K, int[] A) {
		MinSegmentTree minTree = new MinSegmentTree(A);
		MaxSegmentTree maxTree = new MaxSegmentTree(A);
		long ans = 0;
		
		for(int i = 0; i < A.length; i++) {
			int lo = 0;
			int hi = i;
			
			if(maxTree.query(0, i) - minTree.query(0, i) <= K) {
				ans += i+1;
			} else {
				
				//find highest index such that diff > K
				while(lo < hi) {
					int mid = (lo + hi + 1) / 2;
					int max = maxTree.query(mid, i);
					int min = minTree.query(mid, i);
					int diff = max - min;
					if(diff > K) {
						lo = mid;
					} else {
						hi = mid-1;
					}
				}
				ans += i-lo;
			}
		}

		return (int) Math.min(ans, 1000000000);
	}
	
	static class MaxSegmentTree {
		//any associative fcn that a parent stores over 2 children
		private int merge(int val1, int val2){
			return Math.max(val1, val2);
		}

		//1-based
		int[] tree;

		//num els in the original array
		int n;
		public MaxSegmentTree(int n){
			this.n = n;
			//num nodes in tree = 2*n-1 <= 2*2^ceil(log_2 (n)) - 1 
			int numNodes = 2 * (int)(Math.pow(2,Math.ceil(Math.log(n)/Math.log(2))))-1;
			tree = new int[numNodes+1];
		}

		public MaxSegmentTree(int[] arr){
			this(arr.length);
			buildTree(1, 0, arr.length-1, arr);
		}

		//a and b are 0-based indices of the original array
		private void buildTree(int node, int a, int b, int[] arr){
			if(a > b) return;

			//leaf node
			if(a == b){
				tree[node] = arr[a];
				return;
			}
			int mid = (a+b)/2;
			buildTree(2*node, a, mid, arr);
			buildTree(2*node+1, mid+1, b, arr);

			tree[node] = merge(tree[2*node], tree[2*node+1]);
		}

		/**
		 * perform operation on i->j based on original array index
		 */
		public void update(int i, int j, int val){
			update(1, 0, n-1, i, j,val);
		}

		public int query(int i, int j){
			return query(1,0,n-1,i,j);
		}
		
		//guarantee: i,j is between a,b. a,b, is current node's range
		/**
		 * Increment elements within range [i, j] with value value
		 */
		private void update(int node, int a, int b, int i, int j, int value) {
			if(a == b){
				tree[node] += value;
			}else{
				int mid = (a+b)/2;
				if(i <= mid) update(node * 2, a, mid, i, Math.min(j, mid), value);
				if(j > mid) update(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j, value);
				tree[node] = merge(tree[2*node] ,tree[2*node+1]);
			}
		}

		/**
		 * Query tree to get sum of values within range [i, j]
		 */
		private int query(int node, int a, int b, int i, int j) {
			if(a == i && b == j){
				return tree[node];
			}
			else {
				//TODO: CHANGE IF WANT MAX -> -INF
				int res = Integer.MIN_VALUE;
				int mid = (a+b)/2;

				if(i <= mid) res = merge(res, query(node * 2, a, mid, i, Math.min(j, mid)));
				if(j > mid) res = merge(res, query(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j));
				tree[node] = merge(tree[2*node] ,tree[2*node+1]);
				return res;
			}
		}
	}

	static class MinSegmentTree {
		//any associative fcn that a parent stores over 2 children
		private int merge(int val1, int val2){
			return Math.min(val1, val2);
		}

		//1-based
		int[] tree;

		//num els in the original array
		int n;
		public MinSegmentTree(int n){
			this.n = n;
			//num nodes in tree = 2*n-1 <= 2*2^ceil(log_2 (n)) - 1 
			int numNodes = 2 * (int)(Math.pow(2,Math.ceil(Math.log(n)/Math.log(2))))-1;
			tree = new int[numNodes+1];
		}

		public MinSegmentTree(int[] arr){
			this(arr.length);
			buildTree(1, 0, arr.length-1, arr);
		}

		//a and b are 0-based indices of the original array
		private void buildTree(int node, int a, int b, int[] arr){
			if(a > b) return;

			//leaf node
			if(a == b){
				tree[node] = arr[a];
				return;
			}
			int mid = (a+b)/2;
			buildTree(2*node, a, mid, arr);
			buildTree(2*node+1, mid+1, b, arr);

			tree[node] = merge(tree[2*node], tree[2*node+1]);
		}

		/**
		 * perform operation on i->j based on original array index
		 */
		public void update(int i, int j, int val){
			update(1, 0, n-1, i, j,val);
		}

		public int query(int i, int j){
			return query(1,0,n-1,i,j);
		}
		
		//guarantee: i,j is between a,b. a,b, is current node's range
		/**
		 * Increment elements within range [i, j] with value value
		 */
		private void update(int node, int a, int b, int i, int j, int value) {
			if(a == b){
				tree[node] += value;
			}else{
				int mid = (a+b)/2;
				if(i <= mid) update(node * 2, a, mid, i, Math.min(j, mid), value);
				if(j > mid) update(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j, value);
				tree[node] = merge(tree[2*node] ,tree[2*node+1]);
			}
		}

		/**
		 * Query tree to get sum of values within range [i, j]
		 */
		private int query(int node, int a, int b, int i, int j) {
			if(a == i && b == j){
				return tree[node];
			}
			else {
				//TODO: CHANGE IF WANT MAX -> -INF
				int res = Integer.MAX_VALUE;
				int mid = (a+b)/2;

				if(i <= mid) res = merge(res, query(node * 2, a, mid, i, Math.min(j, mid)));
				if(j > mid) res = merge(res, query(node * 2 + 1, mid+1, b, Math.max(mid + 1, i), j));
				tree[node] = merge(tree[2*node] ,tree[2*node+1]);
				return res;
			}
		}
	}
}
