import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

// count number of distinct increasing subsequences O(n log n)
public class Solution {
	static long MOD = 1000000007l;
	int solution(int A[]) {
		if(A.length <= 1) {
			return 0;
		}
		
		// compress A[i]
		TreeSet<Integer> treeset = new TreeSet<Integer>();
		for(int el : A){
			treeset.add(el);
		}
		
		int index = 0;
		// orival to copmressed val
		Map<Integer, Integer> compress = new HashMap();
		for(int el : treeset) {
			compress.put(el, index+1);
			index++;
		}
		
		for(int i=0; i < A.length;i++) {
			A[i] = compress.get(A[i]);
		}

		// fen[i] = number of distinct subsequences that at at value = i
		Fenwick fen = new Fenwick(treeset.last() + 5);
		
		// num sub ending exactly at i
		long[] dp1 = new long[A.length];
		// num sub up to i, but NOT include i
		long[] dp2 = new long[A.length];
		
		dp1[0] = 1;
		dp2[0] = 0;
		fen.increment(A[0], 1);
		
		for(int i=1; i < A.length; i++) {
			dp2[i] = (dp1[i-1] + dp2[i-1]) % MOD;
			dp1[i] = (1 + fen.getTotalFreq(A[i]-1)) % MOD;
			fen.increment(A[i], dp1[i]);
		}
		
		int ans = (int) ((dp1[A.length-1] + dp2[A.length-1]) % MOD);
		// minus len 1 subsequence
		ans -= A.length;
		if(ans < 0) ans += MOD;
		return ans;
	}
	
	static class Fenwick{
		long[] tree;
		public Fenwick(int n){
			tree = new long[n+1];
		}

		public void increment(int i, long val){
			while(i < tree.length){
				tree[i] += val;
				tree[i] %= MOD;
				i += (i & -i);
			}
		}

		public long getTotalFreq(int i){
			long sum = 0;
			while(i > 0){
				sum += tree[i];
				sum %= MOD;
				i -= (i & -i);
			}
			return sum;
		}
	}
}
