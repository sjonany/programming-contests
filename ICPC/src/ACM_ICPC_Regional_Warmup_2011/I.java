package ACM_ICPC_Regional_Warmup_2011;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Set;
import java.util.StringTokenizer;

public class I {
	static InputReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	static Set<Long> pow2;
	public static void main(String[] args) throws Exception{
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		pow2 = new HashSet<Long>();
		long cur = 1;
		for(int i = 0; i < 64; i++){
			pow2.add(cur);
			cur<<=1;
		}
		
		//out.println(solve(0,3));
		for(int t = 0; t < 100; t++){
			long p = (long) (Math.random() * 100);
			long q = (long) (Math.random() * 100);
			if(p>q){
				long temp = q;
				q = p;
				p = temp;
			}
			p++;
			q++;
			long ans = solveNaive(p,q);
			long mine = solve(p,q);
			if(mine!=ans){
				out.println(p + " " +q  + " " + mine + " " + ans);
			}
		}
		/*
		int T = in.nextInt();
		for(int t = 0; t < T; t++) {
			long p = in.nextLong();
			long q = in.nextLong();
			out.printf("Case %d: %d\n", t+1, solve(p,q));
		}*/

		out.close();
	}
	
	static long solveNaive(long p, long q){
		int count = getLength(q);
		long ans = p;
		for(int i = 1; i <= count - getLength(p);i++){
			ans<<=1;
		}
		for(long i = p+1; i <= q; i++){
			long m = i;
			int cm = getLength(m);
			for(;cm < count;cm++){
				m<<=1;
			}
			ans ^= m;
		}
		return ans;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

	static long[] betweens = new long[64];
	static long solve(long a, long b) {
		int bi = 0;
		int acount = getLength(a);
		int bcount = getLength(b);
		
		if(acount == bcount) {
			if(pow2.contains(a)){
				return pref(b);
			}else{
				return pref(b) ^ pref(a-1);
			}
		}
		//a -> 11111 (if a is 5 digs)
		if(pow2.contains(a)){
			betweens[bi++] = (pref((1l<<acount)-1));
		}else{
			betweens[bi++] = (pref((1l<<acount)-1) ^ pref(a-1));
		}
		for(int c = acount+1; c < bcount; c++){
			betweens[bi++] = (pref((1l<<c)-1));
		}
		betweens[bi++] = pref(b);
		//keep shifting one bit at a time.
		long curans = betweens[0] << 1;
		for(int i = 1; i < bi; i++){
			curans ^= betweens[i];
			if(i != bi-1)
				curans <<= 1;
		}
		return curans;
	}

	//if a count = 5, solve 10000 -> a
	static long pref(long a) {
		int count = getLength(a);
		long ans = 0;
		long curweight = 1;
		long diff = a - (1l<<(count-1)) + 1;
		for(int i = 0; i < count-1; i++){
			boolean shouldAdd = false;
			//how many cont 0s
			long period = 2*curweight;
			long rep = diff / period;
			long rem = diff % period;
			long numones = rep * curweight;
			if(rem > period /2){
				rem -= period/2;
				if(rem%2 == 1){
					numones++;
				}
			}
			shouldAdd = numones % 2 == 1;
			if(shouldAdd){
				ans += curweight;
			}
			curweight <<= 1;
		}
		if((diff-1) %2 == 0){
			ans += curweight;
		}
		return ans;
	}

	static int getLength(long x){
		int count = 0;
		while(x>0){
			x/=2;
			count++;
		}
		return count;
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}

/*
my AC cpp code
#include <cstdio>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <set>
#include <queue>
#include <stack>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <ctype.h>
#include <string.h>
#include <assert.h>

using namespace std;

typedef long long LL;

set<LL> pow2;
LL betweens[64];

static LL solve(LL a, LL b);
static LL pref(LL a);
static LL pref(LL a);
static int getLength(LL x);

int main(){
    LL cur = 1;
    for(int i = 0; i < 64; i++){
        pow2.insert(cur);
        cur<<=1;
    }
    int T;
    scanf("%d", &T);
    for(int t = 0; t < T; t++) {
        LL p;
        LL q;
        scanf("%lld %lld", &p, &q);
        printf("Case %d: %lld\n", t+1, solve(p,q));
    }
    return 0;
}

LL solve(LL a, LL b) {
    int bi = 0;
    int acount = getLength(a);
    int bcount = getLength(b);
    if(acount == bcount) {
				if(pow2.find(a) != pow2.end()) {
					return pref(b);
				}else{
        	return pref(b) ^ pref(a-1);
				}
    }
    //a -> 11111 (if a is 5 digs)
    if(pow2.find(a) != pow2.end()){
        betweens[bi++] = (pref((1l<<acount)-1));
    }else{
        betweens[bi++] = (pref((1l<<acount)-1) ^ pref(a-1));
    }
    for(int c = acount+1; c < bcount; c++){
        betweens[bi++] = (pref((1l<<c)-1));
    }
    betweens[bi++] = pref(b);
    //keep shifting one bit at a time.
    LL curans = betweens[0] << 1;
    for(int i = 1; i < bi; i++){
        curans ^= betweens[i];
        if(i != bi-1)
            curans <<= 1;
    }
    return curans;
}

//if a count = 5, solve 10000 -> a
LL pref(LL a) {
    int count = getLength(a);
    LL ans = 0;
    LL curweight = 1;
    LL diff = a - (1l<<(count-1)) + 1;
    for(int i = 0; i < count-1; i++){
        bool shouldAdd = false;
        //how many cont 0s
        LL period = 2*curweight;
        LL rep = diff / period;
        LL rem = diff % period;
        LL numones = rep * curweight;
        if(rem > period /2){
            rem -= period/2;
            if(rem%2 == 1){
                numones++;
            }
        }
        shouldAdd = numones % 2 == 1;
        if(shouldAdd){
            ans += curweight;
        }
        curweight <<= 1;
    }
    if((diff-1) %2 == 0){
        ans += curweight;
    }
    return ans;
}

int getLength(LL x){
    int count = 0;
    while(x>0){
        x/=2;
        count++;
    }
    return count;
}*/

