package ACM_ICPC_Regional_Warmup_2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class C {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	static int[][] dirs = new int[][]{{0,1},{1,0},{0,-1},{-1,0}};
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int t = 0; t < T ;t++){
			String s = in.readLine();
			int ansN = -1;
			int b = s.length();
			for(int n = 2; n <= b; n++){
				if(b % n != 0){
					continue;
				}
				int m = b / n;
				if(m <= 1) continue;
				int[][] bo = new int[m][n];
				
				int cr = 0;
				int cc = 0;
				
				int dir = 0;
				
				for(int curStep = 0; curStep < b; curStep++){
					bo[cr][cc] = s.charAt(curStep);
					if(curStep == b-1){
						break;
					}
					int nr = cr + dirs[dir][0];
					int nc = cc + dirs[dir][1];
					if(nr < 0 || nr >= m || nc < 0 || nc >= n || bo[nr][nc] != 0){
						dir++;
						if(dir >= 4){
							dir = 0;
						}
						curStep--;
					}else{
						cr = nr;
						cc = nc;
					}
				}
				
				boolean ok = true;
				for(int c = 0; c < n; c++){
					for(int r = 1; r < m; r++){
						if(bo[r][c] != bo[r-1][c]) {
							ok = false;
							break;
						}
					}
					if(!ok){
						break;
					}
				}
				if(ok){
					if(ansN == -1 || (n+m) < (ansN + (b/ansN))){
						ansN = n;
					}
				}
			}
			out.println(String.format("Case %d: %d", t+1, ansN == -1 ? -1 : ansN + b/ansN));
		}
		
		
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
