package ACM_ICPC_Regional_Warmup_2011;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

//sigh tle
public class B {
	static InputReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	static long R;
	static long C;
	static int N;
	static Set<Point> stoneSet;
	static Point[] stones;
	static Shape[] shapes;
	static int[][] dirs = {{-2,1},{-1,2},{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1}};
	//choices [i] = all the bit patterns for a length 4 bit to have i ones
	static int[][] choices;

	public static void main(String[] args) throws Exception{
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);

		Set<Shape> shapeSet = new HashSet<Shape>();

		List<Integer>[] pats = (List<Integer>[]) Array.newInstance(List.class, 5);
		for(int i = 0; i < pats.length; i++){
			pats[i] = new ArrayList<Integer>();
		}

		for(int i = 0; i < (1<<4); i++) {
			pats[Integer.bitCount(i)].add(i);
		}

		choices = new int[5][];
		for(int i = 1; i <= 4; i++) { 
			choices[i] = new int[pats[i].size()];
			for(int j = 0; j  < pats[i].size(); j++){
				choices[i][j] = pats[i].get(j);
			}
		}

		for(int[] dir1: dirs) {
			for(int[] dir2: dirs) {
				for(int[] dir3: dirs) {
					for(int[] dir4: dirs) {
						Point[] pts = new Point[4];
						pts[0] = new Point(0,0);
						pts[1] = pts[0].add(dir1);
						pts[2] = pts[1].add(dir2);
						pts[3] = pts[2].add(dir3);
						if(pts[3].add(dir4).equals(pts[0])){
							Set<Point> allPts = new HashSet<Point>();
							for(Point p : pts) {
								allPts.add(p);
							}

							if(allPts.size() == 4) {
								shapeSet.add(new Shape(pts));
							}
						}
					}
				}
			}
		}

		shapes = new Shape[shapeSet.size()];
		int shapeIndex = 0;
		for(Shape s : shapeSet){
			shapes[shapeIndex++] = s;
		}

		initDeterminedShapes();
		
		int TEST = in.nextInt();
		for(int t = 0; t < TEST; t++) {
			R = in.nextInt();
			C = in.nextInt();
			N = in.nextInt();
			stones = new Point[N];
			stoneSet = new TreeSet<Point>();
			for(int i = 0; i < N; i++) {
				Point p = new Point(
						in.nextInt() - 1
						,in.nextInt() - 1);
				stones[i] = p;
				stoneSet.add(p);
			}
			out.println("Case " + (t+1) + ": " + solve());
		}
		out.close();
	}

	static long solve() {
		long ans = 0;
		for(int shapeId = 0; shapeId < shapes.length; shapeId++) {
			Shape shape = shapes[shapeId];
			long valPositions = (R - shape.height() + 1) * (C - shape.width() + 1);
			long badPositions = countBadPositions(shapeId);
			valPositions -= badPositions;
			ans += valPositions;
		}
		return ans * 8;
	}

	//how many ways does this shape crash with a bad guy
	static long countBadPositions(int shapeId){
		long ans = 0;
		int mul = 1;
		for(int bitCount = 1; bitCount <= 4; bitCount++){
			for(int pattern : choices[bitCount]) {
				ans += mul * countShapeCollide(shapeId, pattern);
			}
			mul *= -1;
		}
		return ans;
	}
	
	//shifts[shapeId][pattern]
	static Point[][][] shiftsSaved;
	static void initDeterminedShapes() {
		shiftsSaved = new Point[6][16][4];
		for(int sid = 0; sid < shapes.length; sid++){
			for(int p = 1; p < (1<<4); p++) {
				Shape s = shapes[sid];
				List<Point> sloshifts = new ArrayList<Point>();
				Point anchor = null;
				for(int i = 0; i < 4; i++) {
					if((p&(1<<i)) != 0) {
						anchor = s.pts[i];
						break;
					}
				}

				for(int i = 0; i < 4; i++) {
					sloshifts.add(new Point(s.pts[i].r - anchor.r,
							s.pts[i].c - anchor.c));
				}

				for(int i = 0; i < 4; i++){
					shiftsSaved[sid][p][i] = sloshifts.get(i);
				}
			}
		}
	}

	//count number of times this set of active pieces will collide with positions
	//pattern = 4bits indicating which vertices are active
	static int countShapeCollide(int shapeId, int pattern) {
		int ans = 0;
		Point[] shifts = shiftsSaved[shapeId][pattern];
		//let this stone hit our anchor
		for(Point stone : stones) {
			boolean good = true;
			boolean invalid = false;
			for(int i = 0; i < 4; i++) {
				Point target = new Point(stone.r + shifts[i].r, stone.c + shifts[i].c);
				if((pattern & (1<<i)) != 0) {
					if(!stoneSet.contains(target)){
						good = false;
						break;
					}
				}else{
					if(target.r < 0 || target.r >= R || target.c < 0 || target.c >= C){
						invalid = true;
						break;
					}
				}
			}
			if(invalid)continue;
			if(good){
				ans++;
			}
		}

		return ans;
	}
	
	//pts sorted from low row -> hi row, then col
	//the first pt is always at origin
	static class Shape{ 
		public Point[] pts;

		public Shape(Point[] pts) {
			this.pts = pts.clone();
			Arrays.sort(this.pts);
			//search for lowest row and col, and make that the origin
			int dr = this.pts[0].r;
			int dc = this.pts[0].c;
			for(Point p : this.pts){
				p.r -= dr;
				p.c -= dc;
			}
		}

		public int width() {
			int maxC = Integer.MIN_VALUE;
			int minC = Integer.MAX_VALUE;
			for(Point p : pts) {
				maxC = Math.max(p.c, maxC);
				minC = Math.min(p.c, minC);
			}
			return maxC - minC + 1;
		}

		public int height() {
			int maxR= Integer.MIN_VALUE;
			int minR = Integer.MAX_VALUE;
			for(Point p : pts) {
				maxR = Math.max(p.r, maxR);
				minR = Math.min(p.r, minR);
			}
			return maxR - minR + 1;
		}

		@Override
		public String toString() {
			return "Shape [pts=" + Arrays.toString(pts) + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode(pts);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Shape other = (Shape) obj;
			if (!Arrays.equals(pts, other.pts))
				return false;
			return true;
		}
	}

	static class Point implements Comparable<Point>{
		public int r;
		public int c;

		public Point(int r, int c) {
			this.r = r;
			this.c = c;
		}

		public Point add(int[] dir) {
			return new Point(r + dir[0], c + dir[1]);
		}

		@Override
		public int compareTo(Point o) {
			if(this.r != o.r) {
				return this.r - o.r;
			}else {
				return this.c - o.c;
			}
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + c;
			result = prime * result + r;
			return result;
		}

		@Override
		public String toString() {
			return "Point [r=" + r + ", c=" + c + "]";
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Point other = (Point) obj;
			if (c != other.c)
				return false;
			if (r != other.r)
				return false;
			return true;
		}
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
