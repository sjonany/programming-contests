package ACM_ICPC_Regional_Warmup_2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.StringTokenizer;

public class A {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	static BigInteger[] fac;
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		fac = new BigInteger[21];
		fac[0] = BigInteger.ONE;
		for(int i = 1; i <= 20; i++){
			fac[i] = fac[i-1].multiply(BigInteger.valueOf(i));
		}
		
		for(int t = 0; t < T; t++){
			tk = tk(in.readLine());
			String s = tk.nextToken();
			BigInteger val = new BigInteger(tk.nextToken()).subtract(BigInteger.ONE);
			out.println("Case " + (t+1) + ": " + solve(s, val));
		}
		
		out.close();
	}
	
	static String solve(String s, BigInteger order){
		if(s.length() == 1 || order.equals(BigInteger.ZERO)){
			return s;
		}else{
			BigInteger mul = fac[s.length()-1];
			int index = (int)order.divide(mul).longValue();	
			order = order.mod(mul);
			
			String rec = solve(s.substring(1), order);
			
			StringBuilder ans = new StringBuilder();
			
			for(int i = 0; i < index; i++){
				ans.append(rec.charAt(i));
			}
			ans.append(s.charAt(0));
			for(int i = index; i < rec.length(); i++){
				ans.append(rec.charAt(i));
			}
			return ans.toString();
		}
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
