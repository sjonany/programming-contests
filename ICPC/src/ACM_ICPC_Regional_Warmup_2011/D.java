package ACM_ICPC_Regional_Warmup_2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class D {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	public static void main(String[] args) throws Exception{
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int test = 0; test < T; test++){
			out.println("Case " + (test+1) + ":");
			solve();
		}
		
		out.close();
	}
	
	static int N;
	static int[] sidToNodeId;
	//id -> node
	static Node[] nodes;
	static int maxId;
	static List<Integer>[] sloedges;
	static int[][] edges;
	static int[] lengths;
	
	static void solve() throws Exception{
		N = Integer.parseInt(in.readLine());
		maxId = 0;
		lengths = new int[N];
		Node root = new Node(maxId++);
		nodes = new Node[1000000];
		nodes[0] = root;
		sidToNodeId = new int[N];
		for(int i = 0; i < N; i++){
			String tok = in.readLine();
			lengths[i] = tok.length();
			insert(root, tok, 0, i);
		}
		
		sloedges = (List<Integer>[]) Array.newInstance(List.class, maxId);
		edges = new int[maxId][];
		for(int i = 0; i < maxId; i++){
			sloedges[i] = new ArrayList<Integer>();
			//System.out.println(i + " " + nodes[i]);
			for(int j = 0; j < 26; j++){
				if(nodes[i].children[j] != null) {
					sloedges[i].add(nodes[i].children[j].id);
				}
			}
		}
		
		for(int i = 0; i < maxId; i++){
			edges[i] = new int[sloedges[i].size()];
			for(int j = 0; j < sloedges[i].size(); j++){
				edges[i][j] = sloedges[i].get(j);
			}
		}
		
		initLCA(maxId, 0);
		int Q = Integer.parseInt(in.readLine());
		for(int i = 0; i < Q; i++){
			tk = tk(in.readLine());
			int sid1 =  Integer.parseInt(tk.nextToken())-1;
			int sid2 =  Integer.parseInt(tk.nextToken())-1;
			int n1 = sidToNodeId[sid1];
			int n2 = sidToNodeId[sid2];
			int ance = lca(n1, n2);
			out.println(depth[ance]);
		}
	}
	
	static int[] tin, tout;
	static int[][] up;
	static int timer;
	static int l;
	static int[] depth;

	static void dfs(int v, int p){
		depth[v] = depth[p] + 1;
		tin[v] = ++timer;
		up[v][0] = p;
	    for(int i = 1; i <= l; i++ )
			up[v][i] = up[up[v][i-1]][i-1];
	    
		for(int to : edges[v]){
			if (to != p)
				dfs(to, v);
		}
		tout[v] = ++timer;
	}

	static boolean upper(int a, int b){
		return tin[a] <= tin[b] && tout[a] >= tout[b];
	}

	static int lca(int a, int b){
		if(upper(a, b)) return a;
		if(upper(b, a)) return b;
		for (int i = l; i >= 0; i--)
			if(!upper(up[a][i], b))
				a = up[a][i];
		return up[a][0];
	}

	static void initLCA(int n, int root){
		tin = new int[n];
		tout = new int[n];
		up = new int[n][];
		depth = new int[n];
		
		l = 1;
		while((1 << l) <= n) ++l;
		for (int i = 0; i <n; i++) 
	        up[i] = new int[l + 1];
		depth[root] = -1;
		dfs(root, root);
	}
	
	//insert tok[i...] to root
	static void insert(Node root, String tok, int i, int sid){
		Node cur = root;
		while(i != tok.length()) {
			int ch = tok.charAt(i) - 'a';
			if(cur.children[ch] == null){
				nodes[maxId] = new Node(maxId++);
				cur.children[ch] = nodes[maxId-1];
			}
			cur = cur.children[ch];
			i++;
		}
		
		sidToNodeId[sid] = cur.id;
	}
	
	static class Node{
		public int id;
		//children['a']
		public Node[] children;
		public Node(int id) {
			this.id = id;
			this.children = new Node[26];
		}
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
