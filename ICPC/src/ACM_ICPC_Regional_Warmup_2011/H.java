package ACM_ICPC_Regional_Warmup_2011;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.StringTokenizer;


public class H {
	static InputReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	public static void main(String[] args) throws Exception{
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int T = in.nextInt();
		for(int t = 0; t < T ; t++){
			int k = in.nextInt();
			long anss = solve(k);
			long ans = anss/100;
			if(anss % 100 != 0){
				ans++;
			}
			ans = k <= 180000 ? 0 : Math.max(2000,ans);
			System.out.printf("Case %d: %d\n", t+1, ans);
		}
		//out.close();
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
	
	static long solve(int i){
		long k = i*100l;
		long tax = 0;
		if(k <= 18000000){
			return 0;
		}
		k -= 18000000;
		tax += Math.min(k,30000000) /10;
		if(k <= 30000000){
			return tax;
		}
		k -= 30000000;
		tax += Math.min(k,40000000) * 15l/ 100;
		if(k <= 40000000){
			return tax;
		}
		
		k-= 40000000;
		tax += Math.min(k,30000000) * 2l/10;
		if(k <= 30000000) {
			return tax;
		}
		k-= 30000000;
		return tax + k /4;
	}
}
