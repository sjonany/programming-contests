package SFU_QUAL_1_2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class D {
	static final String IN = "/Users/sjonany/Downloads/data/D.in";
	static final String OUT = "/Users/sjonany/Downloads/data/D.mine";
	public static void main(String[] args) throws Exception{
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		BufferedReader in = new BufferedReader(new FileReader(IN));
		PrintWriter out = new PrintWriter(OUT);
		StringTokenizer tk;
		
		while(true){
			tk = tk(in.readLine());
		
			long N = Long.parseLong(tk.nextToken());
			long M = Long.parseLong(tk.nextToken());
			if(N==0 && M==0)break;
			Person[] arr = new Person[(int)N];
			for(int i = 0 ;i < N; i++){
				tk = tk(in.readLine());
				String tok = tk.nextToken();
				while(tok.charAt(tok.length()-1) != ':'){
					tok = tk.nextToken();
				}
				arr[i] = new Person(Long.parseLong(tk.nextToken()), Long.parseLong(tk.nextToken()));
			}
			
			List<Person> rise = new ArrayList<Person>();
			List<Person> fall = new ArrayList<Person>();
			for(Person p : arr){
				if(p.give > p.receive){
					rise.add(p);
				}else{
					fall.add(p);
				}
			}
			
			Collections.sort(rise, new Comparator<Person>(){
				@Override
				public int compare(Person o1, Person o2) {
					return Long.compare(o1.receive, o2.receive);
				}
			});
			
			long cur = M;
			boolean good = true;
			for(Person p : rise){
				if(p.receive > cur){
					good = false;
					break;
				}
				cur += (p.give - p.receive);
			}
			
			for(Person p : fall){
				if(!good) break;
				if(p.receive > cur){
					good = false;
					break;
				}
				cur += (p.give - p.receive);
			}
			
			if(good){
				out.println("BENEVOLENT DICTATORSHIP");
			}else{
				out.println("SOCIETY RESISTS CHANGE");
			}
		}
		out.close();
	}
	
	static class Person{
		public long give;
		public long receive;
		public Person(long receive, long give) {
			this.give = give;
			this.receive = receive;
		}
		
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
