package SFU_QUAL_1_2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class E {
	static final String IN = "/Users/sjonany/Downloads/data/E.in";
	static final String OUT = "/Users/sjonany/Downloads/data/E.mine";

	static int H;
	static int D;
	static int R;
	static int DEST = 1;
	static int[] N;
	static int[] P;
	static int[][] STOPS;
	static int[][] TIMES;
	static List<Edge>[] edges;

	public static void main(String[] args) throws Exception {
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		BufferedReader in = new BufferedReader(new FileReader(IN));
		PrintWriter out = new PrintWriter(OUT);
		StringTokenizer tk;

		while (true) {
			tk = tk(in.readLine());
			H = Integer.parseInt(tk.nextToken());
			D = Integer.parseInt(tk.nextToken());
			R = Integer.parseInt(tk.nextToken());
			if (H == 0 && D == 0 && R == 0)
				break;
			N = new int[R+1];
			P = new int[R+1];
			STOPS = new int[R+1][];
			TIMES = new int[R+1][];
			edges = (List<Edge>[]) Array.newInstance(List.class, 501);
			for(int i = 1 ;i <= 500; i++){
				edges[i] = new ArrayList<Edge>();
			}
			
			for(int route = 1; route <= R; route++){
				tk = tk(in.readLine());
				N[route] = Integer.parseInt(tk.nextToken());
				P[route] = Integer.parseInt(tk.nextToken());
				STOPS[route] = new int[N[route]];
				TIMES[route] = new int[N[route]];
				tk = tk(in.readLine());
				for(int i = 0; i < N[route]; i++){
					STOPS[route][i] = Integer.parseInt(tk.nextToken());
				}
				tk = tk(in.readLine());
				for(int i = 0; i < N[route]; i++){
					TIMES[route][i] = Integer.parseInt(tk.nextToken());
				}
				
				for(int fromI = 0; fromI < N[route]-1; fromI++){
					int travelTime = TIMES[route][fromI+1] - TIMES[route][fromI];
					int from = STOPS[route][fromI];
					int to = STOPS[route][fromI+1];
					int arrivalTime = TIMES[route][fromI];
					int period = P[route];
					edges[from].add(new Edge(arrivalTime,period, from, to, travelTime, route,fromI));
				}
			}
			
			//earliest time to visit node v
			int[] distance = new int[501];
			boolean[] visit = new boolean[501];
			Arrays.fill(distance, Integer.MAX_VALUE);
			
			distance[H] = 0;
			while(true){
				boolean chosen = false;
				int closestV = 0;
				for(int i = 1; i<=500; i++){
					if(!visit[i] && (distance[i] < distance[closestV] || !chosen)){
						closestV = i;
						chosen = true;
					}
				}
				
				visit[closestV] = true;
				if(closestV == DEST){
					out.println(distance[DEST]);
					break;
				}
				
				for(Edge e : edges[closestV]) {
					int curTime = D + distance[closestV];
					curTime = e.getNextArrival(curTime);
					
					curTime += e.travelTime;
					if(curTime < distance[e.dest]){
						distance[e.dest] = curTime;
					}
					
					for(int destIndex = e.stopIndex+2; destIndex < N[e.routeIndex]-1; destIndex++){
						int dest = STOPS[e.routeIndex][destIndex];
						curTime += TIMES[e.routeIndex][destIndex] - TIMES[e.routeIndex][destIndex-1];
						distance[dest] = Math.min(curTime, distance[dest]);
					}
				}
			}	
		}
		out.close();
	}
	
	static class Edge{
		public int arrivalTime;
		public int period;
		public int from;
		public int dest;
		public int travelTime;
		public int routeIndex;
		public int stopIndex;
		
		public Edge(int time, int period, int from, int dest, int travelTime, int routeIndex, int stopIndex) {
			this.arrivalTime = time;
			this.period = period;
			this.from = from;
			this.dest = dest;
			this.travelTime = travelTime;
			this.routeIndex = routeIndex;
			this.stopIndex = stopIndex;
		}
		
		//next arrival time >= t
		public int getNextArrival(int t){
			int k = (t-arrivalTime) / period;
			while(arrivalTime + period * k < t){
				k++;
			}

			while(arrivalTime + period * (k-1) >= t){
				k--;
			}
			return arrivalTime + period * k;
		}
	}

	static StringTokenizer tk(String s) {
		return new StringTokenizer(s);
	}
}
