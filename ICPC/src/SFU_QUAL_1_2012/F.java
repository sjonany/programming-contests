package SFU_QUAL_1_2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class F {
	static final String IN = "/Users/sjonany/Downloads/data/F.in";
	static final String OUT = "/Users/sjonany/Downloads/data/F.mine";
	
	static int dp[][];
	public static void main(String[] args) throws Exception{

		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		BufferedReader in = new BufferedReader(new FileReader(IN));
		PrintWriter out = new PrintWriter(OUT);
		StringTokenizer tk;
		
		dp = new int[1001][1001];
		for(int[] a : dp){
			Arrays.fill(a, -1);
		}
		
		String line = in.readLine();
		while(line != null && line.trim().length()!=0){
			tk = tk(line);
			int lo = Integer.parseInt(tk.nextToken());
			int hi = Integer.parseInt(tk.nextToken());
			out.println(get(lo, hi));
			line = in.readLine();
		}
		
		out.close();
	}
	
	static int get(int l, int h){
		if(l>=h)return 0;
		if(l==h-1)return l;
		if(dp[l][h] != -1) return dp[l][h];
		int minCost = Integer.MAX_VALUE;
		
		for(int amount = l+1 ; amount < h; amount++){
			//if not succeed
			int ifnotsucceed = amount + 1 == h ? 0 : get(amount+1, h);
			//if succeed
			int ifsucceed = l == amount ? 0 : get(l, amount);
			int curCost = amount + Math.max(ifsucceed, ifnotsucceed); 
			minCost = Math.min(minCost, curCost);
		}
		
		dp[l][h] = minCost;
		return dp[l][h];
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
