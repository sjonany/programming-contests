package SFU_QUAL_1_2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/**
Problem sources
http://www.cs.sfu.ca/SeminarsAndEvents/ACM/120922/
 */
public class A {
	static final String IN = "/Users/sjonany/Downloads/data/A.in";
	static final String OUT = "/Users/sjonany/Downloads/data/A.mine";
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(IN));
		PrintWriter out = new PrintWriter(OUT);
		StringTokenizer tk;
		
		String line = in.readLine();
		
		while(line != null && line.trim().length()!=0){
			String word = line;
			int n = Integer.parseInt(in.readLine());
			for(int i = 0; i < n; i++){
				String w = in.readLine();
				boolean good = true;
				if(w.length() < 4) good = false;
				boolean[] touch = new boolean[9];
				for(int j = 0; j < w.length(); j++){
					boolean found = false;
					if(!touch[4] && w.charAt(j) == word.charAt(4)){
						touch[4] = true;
						continue;
					}
					for(int k = 0; k < word.length(); k++){
						if(touch[k]){
							continue;
						}
						if(w.charAt(j) == word.charAt(k)){
							touch[k] = true;
							found = true;
							break;
						}
					}
					if(!found){
						good= false;
						break;
					}
				}
				
				if(!touch[4]) good= false;
				out.printf("%s is %s\n", w, good ? "valid" : "invalid");
			}
			line = in.readLine();
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
