package SFU_QUAL_1_2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class B {
	static final String IN = "/Users/sjonany/Downloads/data/B.in";
	static final String OUT = "/Users/sjonany/Downloads/data/B.mine";
	public static void main(String[] args) throws Exception{
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		BufferedReader in = new BufferedReader(new FileReader(IN));
		PrintWriter out = new PrintWriter(OUT);
		StringTokenizer tk;
		
		while(true){
			int S = Integer.parseInt(in.readLine());
			if(S==0)break;
			tk = tk(in.readLine());
			double x = Double.parseDouble(tk.nextToken());
			double y = Double.parseDouble(tk.nextToken());
			out.printf("%.6f\n",solve(S, x, y));
		}
		
		out.close();
	}
	
	static double solve(double s, double x, double y){
		if(s < 0.0000091) return 0;
		double p = s/3;
		int xp = (int)(x / p);
		int yp = (int)(y / p);
		if(xp == yp && xp == 1){
			double dx = Math.min(x - p, 2*p-x);
			double dy = Math.min(y-p, 2*p-y);
			return Math.min(dx,dy);
		}
		double nx = x - xp * p;
		double ny = y - yp * p;
		
		return solve(p, nx, ny);
	}
	
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
