package SFU_QUAL_1_2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class C {
	static final String IN = "/Users/sjonany/Downloads/data/C.in";
	static final String OUT = "/Users/sjonany/Downloads/data/C.mine";
	public static void main(String[] args) throws Exception{
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		BufferedReader in = new BufferedReader(new FileReader(IN));
		PrintWriter out = new PrintWriter(OUT);
		StringTokenizer tk;

		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			int M = Integer.parseInt(in.readLine());
			List<Integer>[] arrs = (List<Integer>[]) Array.newInstance(List.class, M);
			for(int i = 0 ;i < M; i++){
				arrs[i] = new ArrayList<Integer>();
				tk = tk (in.readLine());
				int l = Integer.parseInt(tk.nextToken());
				Set<Integer> its = new HashSet<Integer>();
				for(int j = 0; j < l; j++){
					its.add(Integer.parseInt(tk.nextToken()));
				}
				for(int it : its){
					arrs[i].add(it);
				}
				Collections.sort(arrs[i]);
			}

			List<Long>[] dp = (List<Long>[]) Array.newInstance(List.class, M);

			for(int i = 0; i < M; i++){
				dp[i] = new ArrayList<Long>();
				if(i == 0){
					for(int j = 0; j < arrs[i].size(); j++){
						dp[i].add(0l);
					}
				}else{
					for(int j = 0; j < arrs[i].size(); j++){
						//min diff to end there
						boolean found = false;
						long min = Long.MAX_VALUE;

						int curEnd = arrs[i].get(j);
						//last index <= curEnd
						int ind = Collections.binarySearch(arrs[i-1], curEnd);
						if(ind >= 0){

						}else{
							ind = -1-ind;
							ind--;
						}
						
						if(ind >= 0 && dp[i-1].get(ind) != -1l){
							min = Math.min(min, dp[i-1].get(ind) - arrs[i-1].get(ind) + curEnd);
							found = true;
						}

						if(!found){
							dp[i].add(-1l);
						}else{
							dp[i].add(min);
						}
					}
				}
			}

			boolean found = false;
			long min = Integer.MAX_VALUE;
			for(long ans : dp[M-1]){
				if(ans != -1){
					found = true;
					min = Math.min(ans,  min);
				}
			}
			if(found)
				out.println(min);
			else out.println(-1);
		}

		out.close();
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
