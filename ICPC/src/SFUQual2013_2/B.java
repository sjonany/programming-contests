package SFUQual2013_2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class B {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		String line = in.readLine();
		while(line != null){
			tk = tk (line);
			double A = Double.parseDouble(tk.nextToken());
			double B = Double.parseDouble(tk.nextToken());
			
			double R = A+B;
			double H = 2*B;
			double K = R-H;
			
			double ans = 0;
			double dy = 0.00001;
			double R2 = R*R;
			for(double y = dy; y < H; y += dy){
				double width = Math.sqrt(R2 - (y + K) * (y+K));
				ans += dy * width;
			}
			ans *= 2;
			ans -= Math.PI * B * B;
			out.println(String.format("%.3f", ans));
			
			line = in.readLine();
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
