package SFUQual2013_2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;
//http://acm.cs.sfu.ca/completedContests/2013FQ2/
public class A {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		int T = Integer.parseInt(in.readLine());
		for(int test = 0; test <T ; test++){
			String s = in.readLine();
			boolean[] arr = new boolean[26];
			for(int i = 0 ;i < s.length(); i++){
				arr[s.charAt(i) - 'A'] = true;
			}
			int F = Integer.parseInt(in.readLine());
			boolean[][] contains = new boolean[F][26];
			for(int i = 0; i < F; i++){
				contains[i] = parse(in.readLine());
			}
			boolean[] initSet = new boolean[F];
			for(int i = 0; i < F; i++){
				boolean edible = true;
				for(int  j = 0; j < 26; j++){
					if(arr[j]){
						if(contains[i][j]){

						}else{
							edible = false;
							break;
						}
					}
				}
				initSet[i] = edible;
			}

			int found = -1;
			for(int j = 0; j < 26; j++){
				boolean[] myset = new boolean[F];
				arr = new boolean[26];
				arr[j] = true;
				boolean good = true;
				for(int i = 0; i < F; i++){
					boolean edible = true;
					for(int k = 0; k < 26; k++){
						if(arr[k]){
							if(contains[i][k]){

							}else{
								edible = false;
								break;
							}
						}
					}
					myset[i] = edible;
					if(myset[i] != initSet[i]){
						good = false;
						break;
					}
				}
				if(good) {
					found = j;
					break;
				}
			}

			if(found == -1){
				out.println("No Solution");
			}else{
				out.println((char)('A'+found));
			}
		}

		out.close();
	}

	static boolean[] parse(String s) {
		boolean[] arr = new boolean[26];
		for(int i = 0 ;i < s.length(); i++){
			arr[s.charAt(i) - 'A'] = true;
		}
		return arr;
	}

	static StringTokenizer tk(String line) {
		return new StringTokenizer(line);
	}
}
