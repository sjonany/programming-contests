package SFUQual2013_2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class E {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		boolean[] sieve = sieve(1000000);
		sieve[1] = true;
		List<Integer> slops = new ArrayList<Integer>();
		for(int i = 0; i < sieve.length; i++){
			if(sieve[i]) slops.add(i);
		}
		int[] ps = new int[slops.size()];
		for(int i = 0; i < slops.size(); i++){
			ps[i] = slops.get(i);
		}

		int[] mem = new int[1000001];
		Arrays.fill(mem, -1);
		String line = in.readLine();
		while(line != null){
			int N = Integer.parseInt(line);
			if(N == 0) out.println(0);
			else if(N == 1) out.println(1);
			else{
				if(mem[N] != -1){
					out.println(mem[N]);
				}else{
					int ori = N;
					int count = 0;
					while(N > 0){
						int ind = Arrays.binarySearch(ps, N);
						if(ind < 0){
							ind = -1-ind;
							ind--;
						}

						while(true){
							if(N % ps[ind] == 0){
								N-=ps[ind];
								break;
							}
							ind--;
						}
						if(mem[N] != -1){
							count += mem[N]+1;
							N = 0;
						}else{
							count++;
						}
					}
					out.println(count);
					mem[ori] = count;
				}
			}
			line = in.readLine();
		}

		out.close();
	}

	public static boolean[] sieve(int N){
		boolean[] isPrime = new boolean[N+1];
		for(int i = 2; i <= N; i++){
			isPrime[i] = true;
		}

		for(int i = 2; i*i <= N; i++){
			if(isPrime[i]){
				for(int  j = i*i; j <= N; j+=i){
					isPrime[j] = false;
				}
			}
		}
		return isPrime;
	}

	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
