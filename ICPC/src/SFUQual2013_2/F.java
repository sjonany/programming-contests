package SFUQual2013_2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class F {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		int T = Integer.parseInt(in.readLine());
		for(int test = 1; test <= T; test++){
			solve(in,out);
		}

		out.close();
	}

	static int N;
	static int M;
	static List<Integer>[] edges;

	static void solve(BufferedReader in, PrintWriter out) throws IOException{
		StringTokenizer tk = tk(in.readLine());
		N = Integer.parseInt(tk.nextToken());
		M = Integer.parseInt(tk.nextToken());
		edges = (List<Integer>[]) Array.newInstance(List.class, N);
		for(int i = 0; i < N; i++){
			edges[i] = new ArrayList<Integer>();
		}
		for(int i = 0; i < M; i++){
			tk = tk(in.readLine());
			int from = Integer.parseInt(tk.nextToken());
			int to = Integer.parseInt(tk.nextToken());
			from--;
			to--;
			edges[from].add(to);
			edges[to].add(from);
		}
		if(N==0){
			out.println(0);
			return;
		}
		
		fcc();

		// subtree rooted at node i, i is MID/CORNER, what is min # list to touch all?
		dp = new int[N][2];
		int[] solveComp = new int[comps.size()];
		for(int i = 0; i < comps.size(); i++){
			int root = comps.get(i).get(0);
			solve(root, -1);
			solveComp[i] = Math.min(dp[root][CORNER],dp[root][MID]);
		}

		int ans = N;
		for(int i = 0; i < solveComp.length; i++){
			ans += solveComp[i];
		}
		ans--;
		out.println(ans);
	}

	static int CORNER = 0;
	static int MID = 1;
	static int[][] dp;

	static int NO = 1000000;
	static void solve(int node, int par){
		boolean isLeaf = true;
		List<Integer> children = new ArrayList<Integer>();
		for(int vout : edges[node]){
			if(vout == par) continue;
			solve(vout, node);
			isLeaf = false;
			children.add(vout);
		}

		dp[node][CORNER] = NO;
		dp[node][MID] = NO;
		if(isLeaf){
			dp[node][CORNER] = 1;
			dp[node][MID] = NO;
		}else{
			//PREPROCESS
			//min of corners and mid
			int[] a = new int[children.size()];
			//mids
			int[] b = new int[children.size()];
			int A = 0;
			for(int i = 0; i < a.length; i++){
				a[i] = Math.min(dp[children.get(i)][CORNER], dp[children.get(i)][MID]);
				b[i] = dp[children.get(i)][CORNER];
				A += a[i];
			}
			int[] c = new int[children.size()];
			for(int i = 0; i < c.length; i++){
				if(b[i] == NO){
					c[i] = NO;
				}else{
					c[i] = b[i] - a[i];
				}
			}

			Arrays.sort(c);
			//if MID
			if(children.size() >= 2){
				//which of the i's to upgrade to CORNER and minimize?
				if(c[0] != NO && c[1] != NO){
					dp[node][MID] = A + c[0] + c[1] - 1;
				} else{
					dp[node][MID] = NO;
				}
			}else {
				dp[node][MID] = NO;
			}

			//if CORNER
			//if alone
			dp[node][CORNER] = Math.min(dp[node][CORNER], 1+A);

			//if connect to 1, which child
			if(c[0] != NO){
				dp[node][CORNER] = A + c[0];
			}
		}
	}

	static List<List<Integer>> comps;
	static boolean[] isComp;
	public static void fcc(){
		comps = new ArrayList<List<Integer>>();
		isComp = new boolean[N];
		for(int vi=0;vi<N;vi++){
			if(!isComp[vi]){
				List<Integer> comp = new ArrayList<Integer>();
				dfs(vi, comp);
				comps.add(comp);
			}
		}
	}

	public static void dfs(int vi, List<Integer> comp){
		comp.add(vi);
		isComp[vi] = true;
		for(int vout : edges[vi]){
			if(!isComp[vout]){
				dfs(vout, comp);
			}
		}
	}

	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
