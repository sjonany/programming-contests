package ACMQualifer_2013;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class DBFS {
	static String IN = "/Users/sjonany/Downloads/test_data/shotcube/data/secret/big.in";
	static String OUT = "/Users/sjonany/Downloads/test_data/shotcube/data/secret/my_out.out";
	
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	static int N = 7;
	public static void main(String[] args) throws Exception {
		//in = new BufferedReader(new InputStreamReader(System.in));
		//out = new PrintWriter(System.out);
		
		int TEST_ID = 1;
		in = new BufferedReader(new FileReader(IN));
		out = new PrintWriter(OUT);
		
		int TEST = Integer.parseInt(in.readLine());
		System.out.println("TEST =  " +  TEST);
		for(int test = 0; test < TEST; test++){
			boolean[][] mat = new boolean[N][N];
			int rr = -1;
			int cc = -1;
			for(int r = 0; r <N; r++){
				String line = in.readLine();
				for(int c = 0; c <N; c++){
					mat[r][c] = line.charAt(c) == 'X';
					if(mat[r][c]){
						rr=r;
						cc=c;
					}
				}
			}
			
			System.out.println("doing " + test);
			System.out.println(solve(mat, rr, cc));
			System.out.println("done doing " + test);
			in.readLine();
		}
		
		out.close();
	}

	static boolean[][][] temp = new boolean[2][N][N];
	static boolean[][] swap = new boolean[N][N];
	
	static int solve(boolean[][] mat, int r, int c){
		Set<Board> visit = new HashSet<Board>();
		Queue<Board> q = new LinkedList<Board>();
		Board origin = new Board(mat,0,r,c);
		q.add(origin);
		visit.add(origin);
		int answer = -1;
		while(!q.isEmpty()){
			Board board = q.poll();
			if(check(board.m, board.r, board.c)){
				return board.distance;
			}
			fill(visit, q, board);
		}
		
		return answer;
	}
	
	//addition param: a coordinate of a box
	static void fill(Set<Board> visit, Queue<Board> fringe, Board bb){
		boolean[][] b = bb.m;
		//push right
		for(int r = 0; r < N; r++){
			if(!b[r][0]) continue;
			int endC = 0;
			while(endC+1 < N && b[r][endC+1]){
				endC++;
			}
			
			int nextC = endC+2;
			
			while(nextC < N && !b[r][nextC]){
				nextC++;
			}
			
			if(nextC >= N) continue;
			boolean[][] cop = clone(b);
			
			for(int p = 0; p <= endC; p++){
				cop[r][nextC-p-1] = true;
			}
			
			for(int c = 0; c < nextC - endC -1; c++){
				cop[r][c] = false;
			}
			
			Board newb = new Board(cop, bb.distance + 1, r, nextC);
			if(!visit.contains(newb)){
				visit.add(newb);
				fringe.add(newb);
			}
		}
		
		//push left
		for(int r = 0; r < N; r++){
			if(!b[r][N-1]) continue;
			
			int endC = N-1;
			while(endC-1 >=0 && b[r][endC-1]){
				endC--;
			}
			
			int nextC = endC-2;
			
			while(nextC >= 0 && !b[r][nextC]){
				nextC--;
			}
			
			if(nextC < 0) continue;
			boolean[][] cop = clone(b);

			for(int p = 0; p < N-endC; p++){
				cop[r][nextC+p+1] = true;
			}
			for(int c = 1+nextC+N-endC; c < N; c++){
				cop[r][c] = false;
			}
			
			Board newb = new Board(cop, bb.distance + 1, r, nextC);
			if(!visit.contains(newb)){
				visit.add(newb);
				fringe.add(newb);
			}
		}
		
		//push down
		for(int c = 0; c < N; c++){
			if(!b[0][c]) continue;
			int endR = 0;
			while(endR+1 < N && b[endR+1][c]){
				endR++;
			}
			
			int nextR = endR+2;
			
			while(nextR < N && !b[nextR][c]){
				nextR++;
			}
			
			if(nextR >= N) continue;
			boolean[][] cop = clone(b);
			
			for(int p = 0; p <= endR; p++){
				cop[nextR-p-1][c] = true;
			}
			
			for(int r = 0; r < nextR - endR -1; r++){
				cop[c][r] = false;
			}
			
			Board newb = new Board(cop, bb.distance + 1, nextR, c);
			if(!visit.contains(newb)){
				visit.add(newb);
				fringe.add(newb);
			}
		}
		
		//push up
		for(int c = 0; c < N; c++){
			if(!b[N-1][c]) continue;
			int endR = N-1;
			while(endR-1 >=0 && b[endR-1][c]){
				endR--;
			}
			
			int nextR = endR-2;
			
			while(nextR >= 0 && !b[nextR][c]){
				nextR--;
			}
			
			if(nextR < 0) continue;
			boolean[][] cop = clone(b);
			
			for(int p = 0; p < N-endR; p++){
				cop[nextR+p+1][c] = true;
			}
			
			for(int r = 1+nextR+N-endR; r < N; r++){
				cop[r][c] = false;
			}
			
			Board newb = new Board(cop, bb.distance + 1, nextR, c);
			if(!visit.contains(newb)){
				visit.add(newb);
				fringe.add(newb);
			}
		}
	}
	
	static boolean check(boolean[][] b, int ulr, int ulc){
		int count = 1;
		while(count >= 0 && ulr-1 >= 0 && b[ulr-1][ulc]){
			ulr--;
			count--;
		}
		
		count = 1;
		while(count>=0 && ulc-1 >= 0 && b[ulr][ulc-1]){
			ulc--;
			count--;
		}
		
		for(int r = 0; r < 3; r++){
			for(int c = 0; c < 3; c++){
				if(ulr+r >= N || ulc+c >= N || !b[r+ulr][c+ulc]){
					return false;
				}
			}
		}
		return true;
	}

	static boolean[][] clone(boolean[][] m){
		boolean[][] cop = new boolean[N][N];
		for(int r = 0; r < N; r++){
			for(int c = 0; c < N; c++){
				cop[r][c] = m[r][c];
			}
		}
		return cop;
	}
	
	static class Board implements Comparable<Board>{
		public boolean[][] m;
		public int distance;
		public int r;
		public int c;
		public Board(boolean[][] m, int distance, int r, int c) {
			this.m = m;
			this.distance = distance;
			this.r = r;
			this.c = c;
		}

		@Override 
		public int compareTo(Board b) {
			for(int r = 0; r < m.length; r++){
				for(int c = 0; c < m[0].length; c++){
					if(m[r][c] != b.m[r][c]){
						if(m[r][c]) return 1;
						return -1;
					}
				}
			}
			return 0;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.deepHashCode(m);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Board other = (Board) obj;
			if (!Arrays.deepEquals(m, other.m))
				return false;
			return true;
		}		
	}
	
	static void rot(boolean[][] mat, boolean[][] rotmat){
		for(int r = 0; r < N; r ++){
			for(int c = 0; c < N; c++){
				rotmat[r][c] = mat[N-c-1][r];
			}	
		}
	}
	
	static void flip(boolean[][] mat, boolean[][] flipmat){
		for(int r = 0; r < N; r ++){
			for(int c = 0; c < N; c++){
				flipmat[r][c] = mat[c][r];
			}	
		}
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
