#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>
#include<math.h>

using namespace std;

int getGcd(int a, int b) {
  if (a % b == 0) return b;
  else return getGcd(b, a % b);
}


int main(){
  int test;
  cin >> test;
  
  for (int i = 0; i < test; i++) {
    int ans;
    int r, n, d, m, s;
    cin >> r >> n >> d >> m >> s;
    int t = 3600 * d + 60 * m + s;
    //    cout << t << endl;
    int mod = 360*60*60;
    int gcd = getGcd(mod, t);
    t /= gcd;
    mod /= gcd;
    //    cout << gcd << endl;
    //    cout << mod << endl;
    bool used[mod];
    memset(used, false, sizeof(used));
    if (n >= mod) {
      ans = 1;
    } else {
      int cur = 0;
      for (int i = 0; i < n; i++) {
	used[cur] = true;
	// cout << cur << endl;
	cur = (cur + t) % mod;
      }
      ans = 0;
      int last = 0;
      for (int i = 1; i < mod; i++) {
	if (used[i]) {
	  // cout << ans << endl;
	  ans = max(ans, i - last);
	  last = i;
	}
      }
      ans = max(ans, mod - last);
      
    }
    printf("%.20lf\n", M_PI*(double)ans*r*r / mod);
      
    
  }
}
