package ACMQualifer_2013;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.StringTokenizer;

public class E {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);

		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0 ; test < TEST; test++){
			in.readLine();
			int S = Integer.parseInt(in.readLine());

			Map<Node, List<Node>> outs = new HashMap<Node,List<Node>>();
			int curx =0;
			int cury =0;
			for(int i = 0; i < S; i++){
				char c = in.readLine().charAt(0);
				int nx = 0;
				int ny = 0;
				if(c == 'S'){
					nx = curx;
					ny = cury-1;
				}else if(c == 'E') {
					nx = curx+1;
					ny = cury;
				}else if(c == 'N') {
					nx = curx;
					ny = cury+1;
				}else if(c == 'W') {
					nx = curx-1;
					ny = cury;
				}
				Node from = new Node(curx, cury);
				Node to = new Node(nx, ny);
				add(outs, from, to);
				add(outs, to, from);
				curx = nx;
				cury = ny;
			}
			
			if(S == 0){
				out.println(0);
			}else{
				Node start = new Node(0, 0);
				Node end = new Node(curx, cury);
				Queue<Dist> q = new LinkedList<Dist>();
				Set<Node> visit = new HashSet<Node>();
				q.add(new Dist(start,0));
				while(!q.isEmpty()){
					Dist n = q.poll();
					if(n.node.equals(end)){
						out.println(n.dist);
						break;
					}
					for(Node out : outs.get(n.node)) {
						if(!visit.contains(out)){
							q.add(new Dist(out, n.dist+1));
						}
					}
					visit.add(n.node);
				}
			}
		}
		out.close();
	}

	static void add(Map<Node, List<Node>> outs, Node from, Node to){
		if(!outs.containsKey(from)){
			outs.put(from, new ArrayList<Node>());
		}

		outs.get(from).add(to);
	}

	static class Dist{
		public Node node;
		public Dist(Node node, int dist) {
			this.node = node;
			this.dist= dist;
		}
		@Override
		public String toString() {
			return "Dist [node=" + node + ", dist=" + dist + "]";
		}
		public int dist;
	}

	static class Node{
		public int x;
		public int y;
		public Node(int x, int y) {
			this.x = x;
			this.y = y;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}
		@Override
		public String toString() {
			return "Node [x=" + x + ", y=" + y + "]";
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (x != other.x)
				return false;
			if (y != other.y)
				return false;
			return true;
		}

	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
