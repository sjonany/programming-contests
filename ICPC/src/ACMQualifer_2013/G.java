package ACMQualifer_2013;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.StringTokenizer;


public class G {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	public static void main(String[] args) throws Exception {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out);
		
		while(true){
			int N = Integer.parseInt(in.readLine());
			if(N==0)break;
			
			int[][] minh = new int[1001][1001];
			for(int i = 0; i < N; i++){
				tk = tk(in.readLine());
				int a = Integer.parseInt(tk.nextToken());
				int b = Integer.parseInt(tk.nextToken());
				int h = Integer.parseInt(tk.nextToken());
				if(a==b)continue;
				if(a>b){
					int c = a;
					a = b;
					b = c;
				}
				
				if(minh[a][b] == 0 || minh[a][b] > h){
					minh[a][b] = h;
				}
			}
			
			List<Edge>[] edges = (List<Edge>[]) Array.newInstance(List.class, 1001);
			
			for(int i = 1; i<=1000; i++){
				edges[i] = new ArrayList<Edge>();
			}
			
			for(int i = 1; i <= 1000; i++){
				for(int j = i+1; j<=1000;j++){
					if(minh[i][j] == 0){
						continue;
					}else{
						double cost = (i + j) * minh[i][j];
						edges[i].add(new Edge(i,j,cost));
						edges[j].add(new Edge(j,i,cost));
					}
				}
			}
			
			tk = tk(in.readLine());
			int start = Integer.parseInt(tk.nextToken());
			int end = Integer.parseInt(tk.nextToken());
			
			boolean[] visit = new boolean[1001];
			double[] distance = new double[1001];
			Arrays.fill(distance, -1);
			distance[start] = 0;
			
			PriorityQueue<Node> pq = new PriorityQueue<Node>(1000, new Comparator<Node>(){
				@Override
				public int compare(Node n1, Node n2) {
					return Double.compare(n1.weight, n2.weight);
				}
			});
			pq.add(new Node(start,0));
			while(!pq.isEmpty()){
				Node node = pq.remove();
				if(node.weight > distance[node.id]){
					continue;
				}
				if(node.id == end){
					out.println(String.format("%.2f", node.weight/100.0));
				}
				int closestV = node.id;
				visit[closestV] = true;
				for(Edge out : edges[closestV]){
					if(!visit[out.to] && (distance[out.to] < 0 || distance[closestV] + out.weight < distance[out.to])){
						distance[out.to]= distance[closestV] + out.weight;
						pq.add(new Node(out.to, distance[closestV] + out.weight));
					}
				}
			}
		}
		
		out.close();
	}
	
	static class Node{
		int id;
		double weight;
		public Node(int id, double weight) {
			this.id = id;
			this.weight = weight;
		}
	}
	
	static class Edge{
		public int from;
		public int to;
		public double weight;
		public Edge(int from, int to, double weight) {
			this.from = from;
			this.to = to;
			this.weight = weight;
		}
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
