#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>

using namespace std;

int main(){
  int n;
    cin >> n;
    int a[10];
    int p[10];
    int t[10][10];
    for (int j = n; j>=0; j--) {
      cin >> a[j];
    }
    for (int j = 0; j <= n; j++) {
      p[j] = a[n];
      for (int k = n-1; k >= 0; k--) {
	p[j] = p[j]*j + a[k]; 
      }
    }
    for(int j = 1; j <= n; j++) {
      t[1][j] = p[j] - p[j-1];
    }
    for (int j = 2; j <= n; j++) {
      for (int k = 1; k < n; k++) {
	t[j][k] = t[j-1][k+1] - t[j-1][k];
      }
    }
    cout << p[0];
    for (int j = 1; j <= n; j++) {
      cout << " " << t[j][1];
    }
    cout << endl;
  return 0;
}
