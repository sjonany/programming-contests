#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>

using namespace std;

string board[85];
int R, C;
int group_ind[85][85];
bool seen[85][85];
int n_island;
int n_bridge;
int n_bus;
int cx[] = {0, 0, -1, 1}, cy[] = {1, -1, 0, 0};

void dfs(int ii, int jj){
  group_ind[ii][jj] = n_island;
  for(int i = 0; i < 4; i++ ){
    int iii = cx[i] + ii;
    int jjj = cy[i] + jj;
    if(iii >= 0 && jjj >= 0 && iii < R && jjj < C && (board[iii][jjj] == 'X' || board[iii][jjj] == '#') && group_ind[iii][jjj] == 0){
	dfs(iii, jjj);
    }
  }
}

void dfs2(int ii, int jj, int pre){
  seen[ii][jj] = true;
  for(int i = 0; i < 4; i++ ){
    int iii = cx[i] + ii;
    int jjj = cy[i] + jj;
    if(iii >= 0 && jjj >= 0 && iii < R && jjj < C && board[iii][jjj] == 'B' && (pre == -1 || pre == i)){
	dfs2(iii, jjj, i);
    }
  }
}


void dfs3(int ii, int jj, int pre){
  seen[ii][jj] = true;
  for(int i = 0; i < 4; i++ ){
    int iii = cx[i] + ii;
    int jjj = cy[i] + jj;
    if(iii >= 0 && jjj >= 0 && iii < R && jjj < C && seen[iii][jjj] == false){
      if(board[ii][jj] == '#'){
	if(board[iii][jjj] == 'X' || board[iii][jjj] == '#') dfs3(iii, jjj, -1);
      } else if(board[ii][jj] == 'B'){
	if((board[iii][jjj] == 'X' || board[iii][jjj] == 'B') && (pre == -1 || i == pre)) dfs3(iii, jjj, i);
      } else {
	if(board[iii][jjj] == '#') dfs3(iii, jjj, -1);
	else if(board[iii][jjj] == 'B') dfs3(iii, jjj, i);
	else if(board[iii][jjj] == 'X') dfs3(iii, jjj, -1);
      }
    }
  }
}


int main(){
  int map_ = 0;
  while(getline(cin, board[0])){
    map_++;
    R = 1;
    C = board[0].size();
    while(getline(cin, board[R]) && board[R].size() != 0){
      R++;
    }

    for(int i = 0; i < 85; i++ ){
      for(int j = 0; j < 85; j++ ){
	group_ind[i][j] = 0;
	seen[i][j] = false;
      }
    }

    n_bridge = 0;
    n_island = 0;
    n_bus = 0;
    for(int i = 0; i < R; i++ ){
      for(int j = 0; j < C; j++ ){
	if((board[i][j] == 'X' || board[i][j] == '#') && group_ind[i][j] == 0){
	  n_island++;
	  dfs(i, j);
	}
      }
    }

    for(int i = 0; i < R; i++ ){
      for(int j = 0; j < C; j++ ){
	if(board[i][j] == 'B' && seen[i][j] == false){
	  n_bridge++;
	  dfs2(i, j, -1);
	}
      }
    }

    for(int i = 0; i < 85; i++ ){
      for(int j = 0; j < 85; j++ ){
	seen[i][j] = false;
      }
    }

    for(int i = 0; i < R; i++ ){
      for(int j = 0; j < C; j++ ){
	if((board[i][j] == '#' || board[i][j] == 'X') && seen[i][j] == false){
	  n_bus++;
	  dfs3(i, j, -1);
	}
      }
    }


    
    if(map_ > 1) cout << endl;
    printf("Map %d\n", map_);
    printf("islands: %d\n", n_island);
    printf("bridges: %d\n", n_bridge);
    printf("buses needed: %d\n", n_bus);
  }
  return 0;
}
