package ACMQualifer_2013;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class D {
	static String IN = "/Users/sjonany/Downloads/test_data/shotcube/data/secret/big.in";
	static String OUT = "/Users/sjonany/Downloads/test_data/shotcube/data/secret/my_out.out";
	
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;
	
	static int N = 7;
	static Map<Board, Integer> dp = new TreeMap<Board, Integer>();
	
	static int TOP = 0;
	static int DOWN = 1;
	static int LEFT = 2;
	static int RIGHT = 3;
	static int prevDpSize = 0;
	public static void main(String[] args) throws Exception {
		//in = new BufferedReader(new InputStreamReader(System.in));
		//out = new PrintWriter(System.out);
		
		int TEST_ID = 1;
		in = new BufferedReader(new FileReader(IN));
		out = new PrintWriter(OUT);
		
		int TEST = Integer.parseInt(in.readLine());
		System.out.println("TEST =  " +  TEST);
		for(int test = 0; test < TEST; test++){
			boolean[][] mat = new boolean[N][N];
			int rr = -1;
			int cc = -1;
			for(int r = 0; r <N; r++){
				String line = in.readLine();
				for(int c = 0; c <N; c++){
					mat[r][c] = line.charAt(c) == 'X';
					if(mat[r][c]){
						rr=r;
						cc=c;
					}
				}
			}
			
			System.out.println("doing " + test);
			out.println(get(new Board(mat), rr, cc));
			System.out.println("done doing " + test);
			in.readLine();
		}
		
		out.close();
	}
	
	static boolean[][][] temp = new boolean[2][N][N];
	static boolean[][] swap = new boolean[N][N];
	
	//addition param: a coordinate of a box
	static int get(Board bb, int pr, int pc){
		temp[0] = clone(bb.m);
		for(int i = 0; i < 4; i++){
			Board bbrot = new Board(temp[0]);
			if(dp.containsKey(bbrot)){
				return dp.get(bbrot);
			}
			if(i!=3){
				rot(temp[0], temp[1]);
				swap = temp[1];
				temp[1] = temp[0];
				temp[0] = swap;
			}
		}

		temp[1] = clone(bb.m);
		flip(temp[1], temp[0]);
		for(int i = 0; i < 4; i++){
			Board bbrot = new Board(temp[0]);
			if(dp.containsKey(bbrot)){
				return dp.get(bbrot);
			}
			if(i!=3){
				rot(temp[0], temp[1]);
				swap = temp[1];
				temp[1] = temp[0];
				temp[0] = swap;
			}
		}
		
		if(dp.size() > prevDpSize){
			prevDpSize = dp.size();
			if(prevDpSize%10000 == 0)
				System.out.println(prevDpSize);
		}
		
		if(check(bb, pr, pc)){
			dp.put(bb, 0);
			return 0;
		}
		int ans = Integer.MAX_VALUE;
		boolean[][] b = bb.m;
		//push right
		for(int r = 0; r < N; r++){
			if(!b[r][0]) continue;
			int endC = 0;
			while(endC+1 < N && b[r][endC+1]){
				endC++;
			}
			
			int nextC = endC+2;
			
			while(nextC < N && !b[r][nextC]){
				nextC++;
			}
			
			if(nextC >= N) continue;
			boolean[][] cop = clone(b);
			
			for(int p = 0; p <= endC; p++){
				cop[r][nextC-p-1] = true;
			}
			
			for(int c = 0; c < nextC - endC -1; c++){
				cop[r][c] = false;
			}
			
			Board newb = new Board(cop);
			int childAnswer = get(newb, r, nextC);
			if(childAnswer != -1){
				ans = Math.min(ans, 1+childAnswer);
			}
		}
		
		//push left
		for(int r = 0; r < N; r++){
			if(!b[r][N-1]) continue;
			
			int endC = N-1;
			while(endC-1 >=0 && b[r][endC-1]){
				endC--;
			}
			
			int nextC = endC-2;
			
			while(nextC >= 0 && !b[r][nextC]){
				nextC--;
			}
			
			if(nextC < 0) continue;
			boolean[][] cop = clone(b);

			for(int p = 0; p < N-endC; p++){
				cop[r][nextC+p+1] = true;
			}
			for(int c = 1+nextC+N-endC; c < N; c++){
				cop[r][c] = false;
			}
			
			Board newb = new Board(cop);
			int childAnswer = get(newb, r, nextC);
			if(childAnswer != -1){
				ans = Math.min(ans, 1+childAnswer);
			}
		}
		//push down
		for(int c = 0; c < N; c++){
			if(!b[0][c]) continue;
			int endR = 0;
			while(endR+1 < N && b[endR+1][c]){
				endR++;
			}
			
			int nextR = endR+2;
			
			while(nextR < N && !b[nextR][c]){
				nextR++;
			}
			
			if(nextR >= N) continue;
			boolean[][] cop = clone(b);
			
			for(int p = 0; p <= endR; p++){
				cop[nextR-p-1][c] = true;
			}
			
			for(int r = 0; r < nextR - endR -1; r++){
				cop[c][r] = false;
			}
			
			Board newb = new Board(cop);
			int childAnswer = get(newb, nextR, c);
			if(childAnswer != -1){
				ans = Math.min(ans, 1+childAnswer);
			}
		}
		
		//push up
		for(int c = 0; c < N; c++){
			if(!b[N-1][c]) continue;
			int endR = N-1;
			while(endR-1 >=0 && b[endR-1][c]){
				endR--;
			}
			
			int nextR = endR-2;
			
			while(nextR >= 0 && !b[nextR][c]){
				nextR--;
			}
			
			if(nextR < 0) continue;
			boolean[][] cop = clone(b);
			
			for(int p = 0; p < N-endR; p++){
				cop[nextR+p+1][c] = true;
			}
			
			for(int r = 1+nextR+N-endR; r < N; r++){
				cop[r][c] = false;
			}
			
			Board newb = new Board(cop);
			int childAnswer = get(newb, nextR, c);
			if(childAnswer != -1){
				ans = Math.min(ans, 1+childAnswer);
			}
		}
		
		if(ans == Integer.MAX_VALUE){
			dp.put(bb, -1);
			return -1;
		}

		dp.put(bb, ans);
		return ans;
	}
	
	static boolean check(Board bb, int ulr, int ulc){
		boolean[][] b = bb.m;
		int count = 1;
		while(count >= 0 && ulr-1 >= 0 && b[ulr-1][ulc]){
			ulr--;
			count--;
		}
		
		count = 1;
		while(count>=0 && ulc-1 >= 0 && b[ulr][ulc-1]){
			ulc--;
			count--;
		}
		
		for(int r = 0; r < 3; r++){
			for(int c = 0; c < 3; c++){
				if(ulr+r >= N || ulc+c >= N || !b[r+ulr][c+ulc]){
					return false;
				}
			}
		}
		return true;
	}

	static boolean[][] clone(boolean[][] m){
		boolean[][] cop = new boolean[N][N];
		for(int r = 0; r < N; r++){
			for(int c = 0; c < N; c++){
				cop[r][c] = m[r][c];
			}
		}
		return cop;
	}
	
	static class Board implements Comparable<Board>{
		public boolean[][] m;

		public Board(boolean[][] m) {
			this.m = m;
		}

		@Override 
		public int compareTo(Board b) {
			for(int r = 0; r < m.length; r++){
				for(int c = 0; c < m[0].length; c++){
					if(m[r][c] != b.m[r][c]){
						if(m[r][c]) return 1;
						return -1;
					}
				}
			}
			return 0;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.deepHashCode(m);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Board other = (Board) obj;
			if (!Arrays.deepEquals(m, other.m))
				return false;
			return true;
		}		
	}
	
	static void rot(boolean[][] mat, boolean[][] rotmat){
		for(int r = 0; r < N; r ++){
			for(int c = 0; c < N; c++){
				rotmat[r][c] = mat[N-c-1][r];
			}	
		}
	}
	
	static void flip(boolean[][] mat, boolean[][] flipmat){
		for(int r = 0; r < N; r ++){
			for(int c = 0; c < N; c++){
				flipmat[r][c] = mat[c][r];
			}	
		}
	}
	
	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
