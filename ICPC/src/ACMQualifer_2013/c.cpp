#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>

using namespace std;

#define MAXN 32005

bool notPrime[MAXN];
vector<int> primes;

typedef pair<int, int> PII;

int main(){

  for(int i = 4; i < MAXN; i += 2) notPrime[i] = true;
  for(int i = 3; i * i < MAXN; i+= 2){
    if(!notPrime[i]){
      for(int j = i * 2; j < MAXN; j += i){
	notPrime[j] = true;
      }
    }
  }

  for(int i = 2; i < MAXN; i++ ){
    if(!notPrime[i]){
      primes.push_back(i);
    }
  }


  int test;
  int n;
  cin >> test;
  for(int t = 0; t < test; t++ ){
    cin >> n;
    vector<PII> ans;
    for(int j = 0; j < primes.size(); j++ ){
      for(int k = j; k < primes.size(); k++ ){
	if(primes[j] + primes[k] == n){
	  ans.push_back(PII(primes[j], primes[k]));
	}
      }
    }

    if(t) printf("\n");
    printf("%d has %d representation(s)\n", n, ans.size());
    for(int i = 0; i < ans.size(); i++ ){
      printf("%d+%d\n", ans[i].first, ans[i].second);
    }


  }
  return 0;
}
