#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>
#include<stack>

using namespace std;


int main(){
  int n;
  cin >> n;
  int a;
  stack<int> s;
  for(int i = 0; i < n; i++ ){
    cin >> a;
    if(s.size() >= 1 && (a + s.top()) % 2 == 0){
      s.pop();
    } else {
      s.push(a);
    }
  }
  cout << s.size() << endl;
  return 0;
}
