package ACMQualifer_2013;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class DRev {
	static String IN = "/Users/sjonany/Downloads/test_data/shotcube/data/secret/big.in";
	static String OUT = "/Users/sjonany/Downloads/test_data/shotcube/data/secret/my_out.out";

	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tk;

	static int N = 7;
	static Map<Board, Integer> dist = new TreeMap<Board, Integer>();
	public static void main(String[] args) throws Exception {
		//in = new BufferedReader(new InputStreamReader(System.in));
		//out = new PrintWriter(System.out);
		double time = System.currentTimeMillis();
		int TEST_ID = 1;
		in = new BufferedReader(new FileReader(IN));
		out = new PrintWriter(OUT);

		init();
		int TEST = Integer.parseInt(in.readLine());
		System.out.println("TEST =  " +  TEST);
		for(int test = 0; test < TEST; test++){
			boolean[][] mat = new boolean[N][N];
			for(int r = 0; r <N; r++){
				String line = in.readLine();
				for(int c = 0; c <N; c++){
					mat[r][c] = line.charAt(c) == 'X';
				}
			}
			Board b = new Board(mat);
			Integer ans = getWithSymmetry(b.m);
			if(ans == null) out.println(-1);
			else out.println(ans);
			in.readLine();
		}

		out.close();
		System.out.println(System.currentTimeMillis() - time);
	}

	static void init(){
		Queue<Board> fringe = new LinkedList<Board>();
		for(int ulr = 0; ulr < N; ulr++){
			for(int ulc = 0; ulc < N; ulc++){
				int brr = ulr + 2;
				int brc = ulc + 2;
				if(brr >= N || brc >= N){
					continue;
				}

				boolean[][] b = new boolean[N][N];
				for(int r = ulr; r <= brr; r++){
					for(int c = ulc; c <= brc; c++){
						b[r][c] = true;
					}	
				}

				Integer ans = getWithSymmetry(b);

				if(ans == null){
					Board board = new Board(b);
					fringe.add(board);
					dist.put(board, 0);
				}
			}
		}
		
		while(!fringe.isEmpty()){
			Board board = fringe.poll();
			fill(fringe, board);
			int distance = dist.get(board);

			if(distance > maxDist){
				System.out.println(distance);
				maxDist = distance;
				System.out.println(dist.size());
			}
		}
	}

	static int maxDist = 0;
	
	static Integer getWithSymmetry(boolean[][] b){
		Integer ans = Integer.MAX_VALUE;
		boolean[][] trans = clone(b);
		boolean[][] temp = new boolean[N][N];

		for(int i = 0; i < 4; i++){
			Board bb = new Board(trans);
			Integer transAns = dist.get(bb);
			if(transAns != null) {
				ans = Math.min(ans, transAns);
			}
			
			if(i!=3){
				rot(trans, temp);
				boolean[][] swap = temp;
				temp = trans;
				trans = swap;
			}
		}
		flip(b, trans);
		
		for(int i = 0; i < 4; i++){
			Board bb = new Board(trans);
			Integer transAns = dist.get(bb);
			if(transAns != null) {
				ans = Math.min(ans, transAns);
			}
			
			if(i!=3){
				rot(trans, temp);
				boolean[][] swap = temp;
				temp = trans;
				trans = swap;
			}
		}
		
		if(ans == Integer.MAX_VALUE){
			return null;
		}else{
			return ans;
		}
	}

	static void fill(Queue<Board> fringe, Board bb){
		boolean[][] b = bb.m;

		//push right
		for(int r = 0; r < N; r++){
			if(b[r][0]) continue;
			int firstC = 0;
			while(firstC < N && !b[r][firstC]){
				firstC++;
			}

			if(firstC >= N) continue;

			int endC = firstC;
			while(endC < N && b[r][endC]){
				endC++;
			}
			endC--;
			if(endC == firstC) continue;

			boolean[][] mut = clone(b);
			for(int length = 1; length < (endC - firstC + 1); length++){
				mut[r][firstC + length -1] = false;
				mut[r][length - 1] = true;

				Board newb = new Board(clone(mut));

				if(!dist.containsKey(newb)){
					dist.put(newb, dist.get(bb) + 1);
					fringe.add(newb);
				}
			}
		}

		//push left
		for(int r = 0; r < N; r++){
			if(b[r][N-1]) continue;
			int firstC = N-1;
			while(firstC >= 0 && !b[r][firstC]){
				firstC--;
			}

			if(firstC < 0) continue;

			int endC = firstC;
			while(endC >= 0 && b[r][endC]){
				endC--;
			}
			endC++;
			if(endC == firstC) continue;

			boolean[][] mut = clone(b);
			for(int length = 1; length < (firstC - endC + 1); length++){
				mut[r][firstC - length + 1] = false;
				mut[r][N - length] = true;

				Board newb = new Board(clone(mut));

				if(!dist.containsKey(newb)){
					dist.put(newb, dist.get(bb) + 1);
					fringe.add(newb);
				}
			}
		}

		//push down
		for(int c = 0; c < N; c++){
			if(b[0][c]) continue;
			int firstR = 0;
			while(firstR < N && !b[firstR][c]){
				firstR++;
			}

			if(firstR >= N) continue;

			int endR = firstR;
			while(endR < N && b[endR][c]){
				endR++;
			}
			endR--;
			if(endR == firstR) continue;

			boolean[][] mut = clone(b);
			for(int length = 1; length < (endR - firstR + 1); length++){
				mut[firstR + length -1][c] = false;
				mut[length - 1][c] = true;

				Board newb = new Board(clone(mut));
				if(!dist.containsKey(newb)){
					dist.put(newb, dist.get(bb) + 1);
					fringe.add(newb);
				}
			}
		}

		//push up
		for(int c = 0; c < N; c++){
			if(b[N-1][c]) continue;
			int firstR = N-1;
			while(firstR >= 0 && !b[firstR][c]){
				firstR--;
			}

			if(firstR < 0) continue;

			int endR = firstR;
			while(endR >= 0 && b[endR][c]){
				endR--;
			}
			endR++;
			if(endR == firstR) continue;

			boolean[][] mut = clone(b);
			for(int length = 1; length < (firstR - endR + 1); length++){
				mut[firstR - length + 1][c] = false;
				mut[N - length][c] = true;

				Board newb = new Board(clone(mut));

				if(!dist.containsKey(newb)){
					dist.put(newb, dist.get(bb) + 1);
					fringe.add(newb);
				}
			}
		}
	}

	static void print(boolean[][] b){
		for(int r = 0; r < N ;r++){
			for(int c = 0; c < N ;c++){
				System.out.print(b[r][c] ? 'X' : '.');
			}	
			System.out.println();
		}
		System.out.println();
	}

	static boolean[][] clone(boolean[][] m){
		boolean[][] cop = new boolean[N][N];
		for(int r = 0; r < N; r++){
			for(int c = 0; c < N; c++){
				cop[r][c] = m[r][c];
			}
		}
		return cop;
	}

	static void rot(boolean[][] mat, boolean[][] rotmat){
		for(int r = 0; r < N; r ++){
			for(int c = 0; c < N; c++){
				rotmat[r][c] = mat[N-c-1][r];
			}	
		}
	}

	static void flip(boolean[][] mat, boolean[][] flipmat){
		for(int r = 0; r < N; r ++){
			for(int c = 0; c < N; c++){
				flipmat[r][c] = mat[c][r];
			}	
		}
	}

	static class Board implements Comparable<Board>{
		public boolean[][] m;

		public Board(boolean[][] m) {
			this.m = m;
		}

		@Override 
		public int compareTo(Board b) {
			for(int r = 0; r < m.length; r++){
				for(int c = 0; c < m[0].length; c++){
					if(m[r][c] != b.m[r][c]){
						if(m[r][c]) return 1;
						return -1;
					}
				}
			}
			return 0;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.deepHashCode(m);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Board other = (Board) obj;
			if (!Arrays.deepEquals(m, other.m))
				return false;
			return true;
		}		
	}

	static StringTokenizer tk(String s){
		return new StringTokenizer(s);
	}
}
