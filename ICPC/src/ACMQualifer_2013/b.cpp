#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>

using namespace std;

int main(){
  int n;
  int test;
  cin >> test;
  int pwrs[50];
  pwrs[0] = 1;
  for (int i = 1; i < 50; i++) {
    pwrs[i] = 3*pwrs[i-1];
  }
  for(int i = 0; i < test; i++ ){
    cin >> n;
    int tern[100];
    memset(tern, 0, sizeof(tern));
    for (int j = 0; n > 0; j++) {
      tern[j] += n % 3;
      if (tern[j] >= 2) {
	tern[j+1]++;
	tern[j] -= 3;
      }
      n /= 3;
    }
    if(i >= 1) cout << endl;
    cout << "left pan:";
    for (int j = 51; j >= 0; j--) {
      if (tern[j] == -1)
	cout << " " << pwrs[j];
    }
    cout << endl;
    cout << "right pan:";
    for (int j = 51; j >= 0; j--) {
      if (tern[j] == 1)
	cout << " " << pwrs[j];
    }
    cout << endl;
  }
  
  return 0;
}
