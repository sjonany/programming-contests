#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>

using namespace std;

int n;

char oper[5] = {'+', '-', '/', '*'};
char temp[5];
bool good;
char temp2[5];
char ans[5];
char temp4[5];

int solve(){
  for(int i = 0; i < 5; i++ ){
    temp2[i] = temp4[i];
    temp[i] = temp4[i];
  }
  int in[6];
  for(int i = 0; i < 4; i++ ) in[i] = 4;
  int last = 3;
  //  for(int i = 0; i < 3; i++ ) printf("%c ", temp2[i]);
  //cout << endl;
  //  if(temp2[0] == '+' && temp2[1] == '-' && temp2[2] == '-') printf("A");
  for(int i = 0; i < last; i++ ){
    if(temp[i] == '*'){
      in[i] *= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];  
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    } else if(temp[i] == '/'){
      in[i] /= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    }
  }

  for(int i = 0; i < last; i++ ){
    if(temp[i] == '*'){
      in[i] *= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];  
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    } else if(temp[i] == '/'){
      in[i] /= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    }
  }


  for(int i = 0; i < last; i++ ){
    if(temp[i] == '*'){
      in[i] *= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];  
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    } else if(temp[i] == '/'){
      in[i] /= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    }
  }



  for(int i = 0; i < last; i++ ){
    if(temp[i] == '+'){
      in[i] += in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];  
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    } else if(temp[i] == '-'){
      in[i] -= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    }
  }

  for(int i = 0; i < last; i++ ){
    if(temp[i] == '+'){
      in[i] += in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];  
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    } else if(temp[i] == '-'){
      in[i] -= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    }
  }

  for(int i = 0; i < last; i++ ){
    if(temp[i] == '+'){
      in[i] += in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];  
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    } else if(temp[i] == '-'){
      in[i] -= in[i + 1];
      for(int j = i + 1; j < 3; j++ ) in[j] = in[j + 1];
      for(int j = i; j < 3; j++ ) temp[j] = temp[j + 1];
      last--;
      break;
    }
  }

  if(in[0] == n){
    for(int i = 0; i < 4; i++ ) ans[i] = temp2[i];
    return true;
  }
  return false;
}

void gen(int at, int k){
  if(good) return ;
  if(at == k){
    if(solve()){
      good = true;
      return ;
    }
  } else {
    for(int i = 0; i < 4; i++ ){
      temp4[at] = oper[i];
      gen(at + 1, k);
    }
  }
}

int main(){
  int test;
  cin >> test;
  for(int tt = 0; tt < test; tt++ ){
    good = false;
    cin >> n;
    gen(0, 3);
    if(!good){
      cout << "no solution" << endl;
    } else {
      printf("4");
      for(int i = 0; i < 3; i++ ){
	printf(" %c ", ans[i]);
	printf("4");
      }
      printf(" = %d\n", n);
    }
  }
  return 0;
}
