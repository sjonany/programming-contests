#include<stdio.h>
#include<iostream>
#include<sstream>
#include<string.h>
#include<vector>
#include<string>
#include<algorithm>
#include<queue>
#include<stdlib.h>

using namespace std;

int main(){
  int test;
  int n;
  cin >> test;
  for(int i = 0; i < test; i++ ){
    cin >> n;
    if(abs(n) % 2 == 0) printf("%d is even\n", n);
    else printf("%d is odd\n", n);
  }
  return 0;
}
