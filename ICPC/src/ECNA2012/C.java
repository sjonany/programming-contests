package ECNA2012;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class C {
	static boolean good = false;
	static int[][] input;
	static int[][] index;
	static PrintWriter out;
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		
		int T = in.nextInt();
		for(int t = 0; t < T; t++){
			input = new int[7][6];
			index = new int[7][7];
			good = false;
			for(int i = 0; i < 7; i++){
				for(int j = 0; j < 6; j++){
					input[i][j] = in.nextInt();
					index[i][input[i][j]] = j;
				}
			}

			out.print("Case " + (t+1) + ": ");
			perm(0,new int[7],new boolean[7], new int[6][2]);
			if(!good){
				out.println("No solution");
			}
		}
		out.close();
	}
	static int CCW = 0;
	static int CW = 1;
	static void perm(int toSet, int[] perm, boolean[] chosen, int[][] neighbors){
		if(good) return;
		if(toSet == 0){
			for(int i = 0 ; i < chosen.length; i++){
				chosen[i] = true;
				perm[toSet] = i;
				perm(toSet+1, perm, chosen, neighbors);
				chosen[i] = false;
			}
		}else if(toSet == 7){
			if(toSet >= 2 && neighbors[0][CW] != neighbors[5][CCW])
				return;
			good = true;
			for(int i = 0 ; i < perm.length; i++){
				out.print(perm[i]);
				if(i!=perm.length-1){
					out.print(" ");
				}else{
					out.println();
				}
			}
		}else{
			int centerI = (index[perm[0]][1] + toSet - 1) % 6;
			for(int i = 0 ; i < chosen.length; i++){
				if(!chosen[i]){
					perm[toSet] = i;
					int matchI = index[perm[toSet]][input[perm[0]][centerI]];
					neighbors[toSet-1][CW] = input[perm[toSet]][cw(matchI)];
					neighbors[toSet-1][CCW] = input[perm[toSet]][ccw(matchI)];
					if(toSet >= 2 && neighbors[toSet-1][CW] != neighbors[toSet-2][CCW]){
						continue;
					}
					chosen[i] = true;
					perm(toSet+1, perm, chosen, neighbors);
					chosen[i] = false;
				}
			}
		}
	}	
	
	static int cw(int x){
		return (x+1) % 6;
	}
	
	static int ccw(int x){
		if(x==0) return 5;
		return x-1;
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
