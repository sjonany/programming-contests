package ECNA2012;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class E {
	static int bestV1;
	static int bestV2;
	static char bestOp;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int T = Integer.parseInt(in.readLine());
		for(int t = 1; t<=T; t++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int numOp = Integer.parseInt(tk.nextToken());
			String[] tok = new String[numOp*2+1];
			for(int i = 0; i < tok.length; i++){
				tok[i] = tk.nextToken();
			}
			
			List<Integer> vals = new ArrayList<Integer>();
			List<Character> ops = new ArrayList<Character>();
			for(int i = 0; i < numOp+1; i++){
				vals.add(Integer.parseInt(tok[2*i]));
			}
			for(int i = 0; i < numOp; i++){
				ops.add(tok[2*i+1].charAt(0));
			}
			
			int score1 = max(vals, ops, true);
			String p1Hist = "Player 1 (" + bestV1 + bestOp + bestV2 +") leads to " + score1;
			int score2 = min(vals, ops, true);
			String p2Hist = "Player 2 (" + bestV1 + bestOp + bestV2 +") leads to " + score2;
			
			out.println("Case " + t + ":");
			out.println(p1Hist);
			out.println(p2Hist);
			if(score1 < -score2){
				out.println("Player 2 wins");
			}else if(score1 > -score2){
				out.println("Player 1 wins");
			}else{
				out.println("Tie");
			}
			
		}
		
		out.close();
	}
	
	static int max(List<Integer> vals, List<Character> ops, boolean track){
		if(vals.size() == 1){
			return vals.get(0);
		}else{
			int maxVal = Integer.MIN_VALUE;
			for(int opI = 0; opI < ops.size(); opI++){
				int v1 = vals.get(opI);
				int v2 = vals.get(opI+1);
				Character op = ops.get(opI);
				int newVal = calc(v1, v2, op);
				
				vals.remove(opI);
				vals.remove(opI);
				vals.add(opI, newVal);
				ops.remove(opI);
				int curVal = min(vals, ops, false);
				vals.set(opI, v2);
				vals.add(opI, v1);
				ops.add(opI, op);
				
				if(curVal > maxVal){
					maxVal = curVal;
					if(track){
						bestV1 = vals.get(opI);
						bestV2 = vals.get(opI+1);
						bestOp = ops.get(opI);
					}
				}
			}
			return maxVal;
		}
	}
	
	
	static int calc(int a, int b, char op){
		switch(op){
		case '+':
			return a+b;
		case '-':
			return a-b;
		case '*':
			return a*b;
		}
		return -1;
	}
	
	static int min(List<Integer> vals, List<Character> ops, boolean track){
		if(vals.size() == 1){
			return vals.get(0);
		}else{
			int minVal = Integer.MAX_VALUE;
			for(int opI = 0; opI < ops.size(); opI++){
				int v1 = vals.get(opI);
				int v2 = vals.get(opI+1);
				Character op = ops.get(opI);
				int newVal = calc(v1, v2, op);
				
				vals.remove(opI);
				vals.remove(opI);
				vals.add(opI, newVal);
				ops.remove(opI);
				int curVal = max(vals, ops, false);
				vals.set(opI, v2);
				vals.add(opI, v1);
				ops.add(opI, op);
				
				if(curVal < minVal){
					minVal = curVal;
					if(track){
						bestV1 = vals.get(opI);
						bestV2 = vals.get(opI+1);
						bestOp = ops.get(opI);
					}
				}
			}
			return minVal;
		}
	}
}
