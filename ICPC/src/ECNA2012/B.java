package ECNA2012;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;

public class B {
	static int N;
	static int coord;
	static int cost;
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
	
		int t = 1;
		while(true){
			N = in.nextInt();
			if(N == 0) break;
			int[] xcoords = new int[N];
			int[] ycoords = new int[N];
			for(int i = 0; i < N; i++){
				xcoords[i] = in.nextInt();
				ycoords[i] = in.nextInt();
			}
			Arrays.sort(xcoords);
			Arrays.sort(ycoords);
			
			getMin(xcoords);
			int xCoord = coord;
			int xCost = cost;
			
			getMin(ycoords);
			int yCoord = coord;
			int yCost = cost;
			
			out.println("Case " + t + ": (" + xCoord+ "," + yCoord+ ")" + " " + (xCost + yCost));
			t++;
		}
		out.close();
	}
	
	//cords sorteds
	//get minimum x such that sum |coords[i]-x| is minimized, also give total cost
	static void getMin(int[] coords){
		int bestCost = Integer.MAX_VALUE;
		for(int i: coords){
			int curcost = 0;
			for(int j : coords){
				curcost += Math.abs(i-j);
			}
			if(curcost < bestCost){
				coord = i;
				bestCost = curcost;
			}
		}
		cost = bestCost;
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
