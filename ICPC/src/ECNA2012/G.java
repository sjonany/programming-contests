package ECNA2012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

public class G {
	static Map<String, Integer> index;
	static String[] invIndex;
	static List<Edge>[] edges;
	static int numNode = 0;
	static int target;
	static Fraction closest;
	static int closestInt;
	static int closestVertex;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			String line = in.readLine();
			int n = Integer.parseInt(line);
			if(n == 0) break;
			numNode = 0;
			index = new HashMap<String, Integer>();
			edges = (List<Edge>[]) Array.newInstance(List.class, 8);
			for(int i = 0 ;i < edges.length; i++){
				edges[i] = new ArrayList<Edge>();
			}
			for(int i = 0; i < n; i++){
				StringTokenizer tk = new StringTokenizer(in.readLine());
				int v1 = Integer.parseInt(tk.nextToken());
				String node1 = tk.nextToken();
				tk.nextToken();
				int v2 = Integer.parseInt(tk.nextToken());
				String node2 = tk.nextToken();
				
				if(!index.containsKey(node1)){
					index.put(node1, numNode++);;
				}
				if(!index.containsKey(node2)){
					index.put(node2, numNode++);;
				}
				
				edges[index.get(node1)].add(new Edge(index.get(node2), new Fraction(v2,v1)));
				edges[index.get(node2)].add(new Edge(index.get(node1), new Fraction(v1,v2)));
			}
			
			invIndex = new String[numNode];
			for(Entry<String, Integer> entry : index.entrySet()){
				invIndex[entry.getValue()] = entry.getKey();
			}
			
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int v = Integer.parseInt(tk.nextToken());
			String startNode = tk.nextToken();
			
			target = v;
			closest = null;
			closestInt = -1;
			closestVertex = -1;
			
			dfs(index.get(startNode), new Fraction(1,1), new boolean[numNode], false);
			
			out.printf("Case %d: %d %s\n", t, closestInt, invIndex[closestVertex]);
			t++;
		}
		
		out.close();
	}
	
	static void dfs(int node, Fraction curVal, boolean[] visit, boolean track){
		visit[node] = true;
		if(track){
			int curInt = (int) Math.ceil(1.0 * target * curVal.num / curVal.den);
			Fraction curFrac = new Fraction(curInt*curVal.den, curVal.num);
			if(curInt <= 100000){
				if(closest == null || curFrac.compareTo(closest) < 0){
					closest = curFrac;
					closestInt = curInt;
					closestVertex = node;
				}
			}
		}
		
		for(Edge out : edges[node]){
			if(!visit[out.to]){
				dfs(out.to, curVal.multiply(out.weight), visit, true);
			}
		}
	}
	
	static class Edge{
		public Edge(int to, Fraction weight) {
			this.to = to;
			this.weight = weight;
		}
		public int to;
		public Fraction weight;
		
		
	}
	
	static class Fraction implements Comparable<Fraction>{
		public long num;
		public long den;
		public Fraction(long num, long den){
			this.num = num;
			this.den = den;
			reduce();
		}
		
		public void reduce(){
			long gcd = gcd(num, den);
			num /= gcd;
			den /= gcd;
		}
		
		public Fraction multiply(Fraction f){
			return new Fraction(this.num * f.num, this.den * f.den);
		}
		
		private static long gcd(long a, long b){
			while(b > 0){
				long c = a % b;
				a = b;
				b = c;
			}
			return a;
		}

		@Override
		public int compareTo(Fraction o) {
			return new Long(this.num * o.den).compareTo(o.num * this.den);
		}
		
		@Override
		public String toString(){
			return num +  "/" + den + " = " + 1.0*num/den;
		}
	}
}
