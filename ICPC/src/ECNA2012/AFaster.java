package ECNA2012;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
//2^n instead of 3^n - implementation of jaro's soln
public class AFaster {
	static int N;
	static List<Node> nodes;
	static int ans;

	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		int t = 1;
		while(true){
			N = in.nextInt();
			if(N == 0) break;
			
			ans = 1;
			nodes = new ArrayList<Node>();
			
			for(int i = 0; i < N; i ++){
				int d1 = in.nextInt();
				int d2 = in.nextInt();
				int d3 = in.nextInt();
				nodes.add(new Node(i,d1,d2, nodes.size()));
				nodes.add(new Node(i,d2,d3, nodes.size()));
				nodes.add(new Node(i,d1,d3, nodes.size()));
			}
		
			for(int i = 0; i < nodes.size(); i++){
				Node n = nodes.get(i);
				for(int j = 0; j < nodes.size(); j++){
					Node container = nodes.get(j);
					if(n.w <= container.w && n.h <= container.h && n.color != container.color){
						container.addIn(n);
					}
				}
			}
			
			//true iff there is a path that ends up at vertex i, 
			//and has travelled through all those colors exactly once
			int allCol = 1<<N;
			int[][] dp = new int[nodes.size()][allCol];
			for(int i = 0; i < nodes.size(); i++){
				for(int j = 0; j < allCol; j++)
					if(dp[i][j] == 0)
						calc(dp, i, j, Integer.bitCount(j));
			}

			out.println("Case " + t + ": " +ans);
			t++;
		}

		out.close();
	}

	static int calc(int[][] dp, int v, int colState, int numChosenCol){
		if(numChosenCol == 0){
			dp[v][0] = 0;
			return 0;
		}else if(numChosenCol == 1){
			if((colState & (1<<nodes.get(v).color)) != 0){
				dp[v][colState] = 1;
				return 1;
			}else{
				dp[v][colState] = 0;
				return 0;
			}
		}else{
			if(dp[v][colState] != 0){
				return dp[v][colState];
			}else{
				int mask = 1<<nodes.get(v).color;
				if((colState & mask) == 0){
					dp[v][colState] = -1;
					return -1;
				}
				int prevColState = colState ^ mask;
				for(Node prevNode : nodes.get(v).ingoing){
					if(calc(dp, prevNode.id, prevColState, numChosenCol-1) == 1){
						dp[v][colState] = 1;
						ans = Math.max(ans, numChosenCol);
						return 1;
					}
				}

				dp[v][colState] = -1;
				return -1;
			}
		}
	}

	static class Node{
		public int color;
		public int w;
		public int h;
		public int id;
		public List<Node> ingoing;

		public Node(int color, int w, int h, int id){
			this.id = id;
			this.color = color;
			this.w = Math.min(w, h);
			this.h = Math.max(w, h);
			this.ingoing = new ArrayList<Node>();
		}

		public void addIn(Node in){
			ingoing.add(in);
		}
	}

	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}


		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
