package ECNA2012;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.StringTokenizer;

public class D {
	static int count = 0;
	static Map<Item, Integer> map;
	static Item[] arr;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		map = new HashMap<Item, Integer>();
		arr = new Item[15505];
		gen(0,15,new int[6]);
		
		int t = 1;
		while(true){
			String line = in.readLine();
			StringTokenizer tk = new StringTokenizer(line);
			String command = tk.nextToken();
			if(command.startsWith("e"))break;
			out.print("Case " + t + ": ");
			if(command.startsWith("u")){
				Item it = arr[Integer.parseInt(tk.nextToken())];
				for(int i = 0; i < it.counts.length; i++){
					out.print(it.counts[i]);
					if(i != it.counts.length-1){
						out.print(" ");
					}else{
						out.println();
					}
				}
			}else{
				int[] arr = new int[6];
				for(int i =0; i< arr.length; i++){
					arr[i] = Integer.parseInt(tk.nextToken());
				}
				out.println(map.get(new Item(arr)));
			}
			t++;
		}
		
		out.close();
	}
	
	static void gen(int toSet, int left, int[] hist){
		if(toSet == 5){
			hist[5] = left;
			Item it = new Item(hist);
			arr[count] = it;
			map.put(it, count);
			count++;
		}else{
			for(int i = 0; i <= left; i++){
				hist[toSet] = i;
				gen(toSet+1, left-i, hist);
			}
		}
	}
	
	static class Item{
		public Item(int[] counts) {
			this.counts = counts.clone();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode(counts);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Item other = (Item) obj;
			if (!Arrays.equals(counts, other.counts))
				return false;
			return true;
		}

		public int[] counts;
	}
	
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
