package ECNA2012;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.InputMismatchException;

public class A {
	static int N;
	static int[][] arr;
	static int ans;
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			N = in.nextInt();
			if(N == 0) break;
			arr = new int[N][3];
			for(int i = 0; i < N; i ++){
				for(int j = 0; j < 3; j++){
					arr[i][j] = in.nextInt();
				}
			}
			
			ans = 0;
			choose(0, new Dim[N]);
			out.println("Case " + t + ": " +ans);
			t++;
		}
		
		out.close();
	}
	
	static void choose(int toSet, Dim[] rects){
		if(toSet == N){
			ans = Math.max(ans, solve(rects));
		}else{
			rects[toSet] = new Dim(arr[toSet][0], arr[toSet][1]);
			choose(toSet+1, rects);
			rects[toSet] = new Dim(arr[toSet][0], arr[toSet][2]);
			choose(toSet+1, rects);
			rects[toSet] = new Dim(arr[toSet][1], arr[toSet][2]);
			choose(toSet+1, rects);
		}
	}
	
	//given squares, find the max squares that can fit 
	static int solve(Dim[] rectsOri){
		Dim[] rects = new Dim[N];
		for(int i = 0; i < rects.length; i++){
			rects[i] = new Dim(rectsOri[i].w, rectsOri[i].h);
		}
		
		Arrays.sort(rects,new Comparator<Dim>(){
			@Override
			public int compare(Dim o1, Dim o2) {
				if(o1.h != o2.h)
					return - o1.h + o2.h;
				return -o1.w + o2.w;
			}
		});
		int[] dp = new int[N];
		dp[0] = 1;
		for(int i = 1; i < N; i++){
			dp[i] = Math.max(dp[i-1],1);
			for(int j = i-1; j >= 0; j--){
				if(rects[i].w <= rects[j].w){
					dp[i] = Math.max(dp[i], dp[j]+1);
				}
			}
		}
		return dp[N-1];
	}
	
	static class Dim{
		public int w;
		public int h;
		public Dim(int w, int h){
			this.w = Math.min(w,h);
			this.h = Math.max(h,w);
		}
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
