package ECNA2012;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class F {
	static Set<String> seen;
	static int maxWindowed;
	static int lastConsecutive;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		int T = Integer.parseInt(in.readLine());
		for(int t = 1; t<=T; t++){
			StringTokenizer tk = new StringTokenizer(in.readLine());
			int numSigns = Integer.parseInt(tk.nextToken());
			int windowSize = Integer.parseInt(tk.nextToken());
			
			seen = new HashSet<String>();
			lastConsecutive = 0;
			maxWindowed = 0;
			for(int i = 0; i < numSigns; i++){
				String line = in.readLine();
				boolean prevNumber = false;
				int startIndex = 0;
				for(int j = 0; j < line.length()+1; j++){
					boolean isDigit = j != line.length() &&
							Character.isDigit(line.charAt(j));
					if(prevNumber){
						if(!isDigit){
							expand(line.substring(startIndex, j));
							prevNumber = false;
						}
					}else{
						if(isDigit){
							startIndex = j;
							prevNumber = true;
						}
					}
				}
				
				while(true){
					if(seen.contains("" + (lastConsecutive+1))){
						lastConsecutive++;
					}else{
						break;
					}
				}
				
				maxWindowed = lastConsecutive + windowSize;
				while(maxWindowed > 0){
					if(seen.contains("" + maxWindowed)){
						break;
					}
					maxWindowed--;
				}
				
				Set<String> newSeen = new HashSet<String>();
				for(String s : seen){
					int intt = Integer.parseInt(s);
					if(intt <= maxWindowed && intt >= lastConsecutive){
						newSeen.add(s);
					}
				}
				
				seen = newSeen;
			}
			out.printf("Case %d: %d %d\n", t, lastConsecutive, maxWindowed);
		}
		
		out.close();
	}
	
	static void expand(String s){
		if(s.length() > 6){
			for(int i = 0; i < s.length()-5; i++){
				int end = i + 5;
				String nextStr = s.substring(i, end+1);
				if(!seen.contains(nextStr)){
					expand(nextStr);
				}
			}
		}else{
			if(!seen.contains(s)){
				Integer i = Integer.parseInt(s);
				if(i <= lastConsecutive){
					return;
				}
				
				seen.add(s);
				if(s.length() > 1){
					expand(s.substring(0, s.length()-1));
					expand(s.substring(1, s.length()));
				}
			}
		}
	}
}
