package ECNA2012;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;

public class H {
	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int t = 1;
		while(true){
			int n = in.nextInt();
			if(n == 0) break;
			out.println("Case " + t + ":");
			int[][] costMat = new int[n][n];
			for(int i = 0; i < n; i++){
				for(int j = 0; j < n; j++){
					costMat[i][j] = in.nextInt();
				}
			}
			int[] assignment1 = new Hungarian(costMat).execute();
			int[] invAssignment1 = new int[n];
			for(int i = 0; i < n ;i++){
				invAssignment1[assignment1[i]] = i;
			}
			
			int[][] costMat2 = new int[n][n];
			for(int i = 0; i < n; i++){
				int timeDone = costMat[i][assignment1[i]];
				for(int j = 0; j < n; j++){
					int timeWait = Math.max(0, costMat[invAssignment1[j]][j] - timeDone);
					costMat2[i][j] = in.nextInt() + timeWait;
				}
			}
			
			int[] assignment2 = new Hungarian(costMat2).execute();
			
			int totalIdle = 0;
			int[] timeDone = new int[n];
			for(int i = 0; i < n; i++){
				int timeDone1 = costMat[i][assignment1[i]];
				int timeWait = Math.max(0, costMat[invAssignment1[assignment2[i]]][assignment2[i]] - timeDone1);
				int totalTime = timeDone1 + costMat2[i][assignment2[i]];
				totalIdle += timeWait;
				timeDone[i] = totalTime;
			}
			
			for(int i = 0; i < n; i++){
				out.printf("Worker %d: %d %d %d\n", i+1, (assignment1[i]+1), (assignment2[i]+1), timeDone[i]);
			}
			out.println("Total idle time: " + totalIdle);
			t++;
		}
		
		out.close();
	}
	
	public static class Hungarian{
		private final int[][] costMatrix;
		private final int rows, cols, dim;
		private final int[] labelByWorker, labelByJob;
		private final int[] minSlackWorkerByJob;
		private final int[] minSlackValueByJob;
		private final int[] matchJobByWorker, matchWorkerByJob;
		private final int[] parentWorkerByCommittedJob;
		private final boolean[] committedWorkers;

		public Hungarian(int[][] costMatrix) {
			this.dim = Math.max(costMatrix.length, costMatrix[0].length);
			this.rows = costMatrix.length;
			this.cols = costMatrix[0].length;
			this.costMatrix = new int[this.dim][this.dim];
			for (int w = 0; w < this.dim; w++) {
				if (w < costMatrix.length) {
					if (costMatrix[w].length != this.cols) {
						throw new IllegalArgumentException("Irregular cost matrix");
					}
					this.costMatrix[w] = Arrays.copyOf(costMatrix[w], this.dim);
				} else {
					this.costMatrix[w] = new int[this.dim];
				}
			}
			labelByWorker = new int[this.dim];
			labelByJob = new int[this.dim];
			minSlackWorkerByJob = new int[this.dim];
			minSlackValueByJob = new int[this.dim];
			committedWorkers = new boolean[this.dim];
			parentWorkerByCommittedJob = new int[this.dim];
			matchJobByWorker = new int[this.dim];
			Arrays.fill(matchJobByWorker, -1);
			matchWorkerByJob = new int[this.dim];
			Arrays.fill(matchWorkerByJob, -1);
		}

		protected void computeInitialFeasibleSolution() {
			for (int j = 0; j < dim; j++) {
				labelByJob[j] = Integer.MAX_VALUE;
			}
			for (int w = 0; w < dim; w++) {
				for (int j = 0; j < dim; j++) {
					if (costMatrix[w][j] < labelByJob[j]) {
						labelByJob[j] = costMatrix[w][j];
					}
				}
			}
		}

		/**
		 * Execute the algorithm.
		 * 
		 * @return the minimum cost matching of workers to jobs based upon the
		 *         provided cost matrix. A matching value of -1 indicates that the
		 *         corresponding worker is unassigned.
		 */
		public int[] execute() {
			reduce();
			computeInitialFeasibleSolution();
			greedyMatch();

			int w = fetchUnmatchedWorker();
			while (w < dim) {
				initializePhase(w);
				executePhase();
				w = fetchUnmatchedWorker();
			}
			int[] result = Arrays.copyOf(matchJobByWorker, rows);
			for (w = 0; w < result.length; w++) {
				if (result[w] >= cols) {
					result[w] = -1;
				}
			}
			return result;
		}
		
		protected void executePhase() {
			while (true) {
				int minSlackWorker = -1, minSlackJob = -1;
				int minSlackValue = Integer.MAX_VALUE;
				for (int j = 0; j < dim; j++) {
					if (parentWorkerByCommittedJob[j] == -1) {
						if (minSlackValueByJob[j] < minSlackValue) {
							minSlackValue = minSlackValueByJob[j];
							minSlackWorker = minSlackWorkerByJob[j];
							minSlackJob = j;
						}
					}
				}
				if (minSlackValue > 0) {
					updateLabeling(minSlackValue);
				}
				parentWorkerByCommittedJob[minSlackJob] = minSlackWorker;
				if (matchWorkerByJob[minSlackJob] == -1) {
					/*
					 * An augmenting path has been found.
					 */
					int committedJob = minSlackJob;
					int parentWorker = parentWorkerByCommittedJob[committedJob];
					while (true) {
						int temp = matchJobByWorker[parentWorker];
						match(parentWorker, committedJob);
						committedJob = temp;
						if (committedJob == -1) {
							break;
						}
						parentWorker = parentWorkerByCommittedJob[committedJob];
					}
					return;
				} else {
					int worker = matchWorkerByJob[minSlackJob];
					committedWorkers[worker] = true;
					for (int j = 0; j < dim; j++) {
						if (parentWorkerByCommittedJob[j] == -1) {
							int slack = costMatrix[worker][j]
									- labelByWorker[worker] - labelByJob[j];
							if (minSlackValueByJob[j] > slack) {
								minSlackValueByJob[j] = slack;
								minSlackWorkerByJob[j] = worker;
							}
						}
					}
				}
			}
		}

		protected int fetchUnmatchedWorker() {
			int w;
			for (w = 0; w < dim; w++) {
				if (matchJobByWorker[w] == -1) {
					break;
				}
			}
			return w;
		}

		protected void greedyMatch() {
			for (int w = 0; w < dim; w++) {
				for (int j = 0; j < dim; j++) {
					if (matchJobByWorker[w] == -1
							&& matchWorkerByJob[j] == -1
							&& costMatrix[w][j] - labelByWorker[w] - labelByJob[j] == 0) {
						match(w, j);
					}
				}
			}
		}

		protected void initializePhase(int w) {
			Arrays.fill(committedWorkers, false);
			Arrays.fill(parentWorkerByCommittedJob, -1);
			committedWorkers[w] = true;
			for (int j = 0; j < dim; j++) {
				minSlackValueByJob[j] = costMatrix[w][j] - labelByWorker[w]
						- labelByJob[j];
				minSlackWorkerByJob[j] = w;
			}
		}

		protected void match(int w, int j) {
			matchJobByWorker[w] = j;
			matchWorkerByJob[j] = w;
		}

		protected void reduce() {
			for (int w = 0; w < dim; w++) {
				int min = Integer.MAX_VALUE;
				for (int j = 0; j < dim; j++) {
					if (costMatrix[w][j] < min) {
						min = costMatrix[w][j];
					}
				}
				for (int j = 0; j < dim; j++) {
					costMatrix[w][j] -= min;
				}
			}
			int[] min = new int[dim];
			for (int j = 0; j < dim; j++) {
				min[j] = Integer.MAX_VALUE;
			}
			for (int w = 0; w < dim; w++) {
				for (int j = 0; j < dim; j++) {
					if (costMatrix[w][j] < min[j]) {
						min[j] = costMatrix[w][j];
					}
				}
			}
			for (int w = 0; w < dim; w++) {
				for (int j = 0; j < dim; j++) {
					costMatrix[w][j] -= min[j];
				}
			}
		}

		protected void updateLabeling(int slack) {
			for (int w = 0; w < dim; w++) {
				if (committedWorkers[w]) {
					labelByWorker[w] += slack;
				}
			}
			for (int j = 0; j < dim; j++) {
				if (parentWorkerByCommittedJob[j] != -1) {
					labelByJob[j] -= slack;
				} else {
					minSlackValueByJob[j] -= slack;
				}
			}
		}
	}
	
	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
