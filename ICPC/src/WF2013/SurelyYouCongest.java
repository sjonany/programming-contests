package WF2013;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class SurelyYouCongest {
	static int N;
	static int M;
	static int C;

	//how many commuters at that intersection
	static int[] countCommuters;
	static List<Edge>[] adjMap;
	static List<Integer>[] previous;

	public static void main(String[] args) throws Exception{
		InputReader in = new InputReader(System.in);
		PrintWriter out = new PrintWriter(System.out);

		N = in.readInt();
		M = in.readInt();
		C = in.readInt();

		adjMap = (List<Edge>[]) Array.newInstance(List.class, N+1);
		countCommuters = new int[N];

		for(int i = 0; i < adjMap.length; i++){
			adjMap[i] = new ArrayList<Edge>();
		}

		for(int i = 0; i < M ; i++){
			int from = in.readInt()-1;
			int to = in.readInt()-1;
			int length = in.readInt();
			adjMap[from].add(new Edge(to, length));
			adjMap[to].add(new Edge(from, length));
		}

		for(int i = 0; i < C ; i++){
			countCommuters[in.readInt()-1]++;
		}

		//cost for node i to get to node 0
		long[] costMap = shortestPath(0);

		//get convert previous[] from dijkstra to shortestAdjMap
		//but we reverse dijkstra, because we want direction to the source
		MaximumFlowFordFulkerson flowGraph = new MaximumFlowFordFulkerson(N+1);
		for(int i = 0; i < previous.length; i++){
			for(int from : previous[i]){
				flowGraph.addEdge(i, from, 1);
			}
		}


		//relevant costs
		Set<Long> costs = new HashSet<Long>();
		for(int i = 0; i < N; i++){
			if(countCommuters[i] != 0){
				costs.add(costMap[i]);
			}
		}

		int ans = 0;
		//key = cost. val = starting points whose distance = cost
		Map<Long, List<Integer>> layers = new HashMap<Long, List<Integer>>();

		for(int startPoint = 0; startPoint < N; startPoint++){
			if(countCommuters[startPoint] == 0) continue;
			if(!layers.containsKey(costMap[startPoint])){
				layers.put(costMap[startPoint], new ArrayList<Integer>());
			}
			layers.get(costMap[startPoint]).add(startPoint);
		}

		boolean first = true;
		for(List<Integer> layer : layers.values()){
			//reset caps
			if(!first)
				flowGraph.resetFlow();
			first = false;

			for(int miniSource : layer){
				flowGraph.addEdge(N, miniSource, countCommuters[miniSource]);
			}

			ans += flowGraph.getMaximumFlow(N, 0);
			//delete the forward edges and backedges
			flowGraph.adjMap[N] = new ArrayList<MaximumFlowFordFulkerson.FlowEdge>();
			for(int miniSource : layer){
				flowGraph.adjMap[miniSource].remove(flowGraph.adjMap[miniSource].size()-1);
			}
		}

		out.println(ans);
		out.close();
	}

	//space efficient ford fulkerson
	private static class MaximumFlowFordFulkerson {
		public static class FlowEdge {
			public int dest;
			public int capacity;
			//flow left allowed, NOT currently flowing
			public int flow;
			public FlowEdge reverse;

			public FlowEdge(int v, int capacity, int flow) {
				this.dest = v;
				this.capacity = capacity;
				this.flow = flow;
			}
		}

		private int N; // node from 0 to N - 1
		public List<FlowEdge>[] adjMap;
		int mark;   // global variable for checking if a node is already visited
		int[] seen;  // status of each node

		MaximumFlowFordFulkerson(int numNode){
			N = numNode;
			adjMap = (List<FlowEdge>[]) Array.newInstance(List.class, N);
			for(int i = 0; i < adjMap.length; i++){
				adjMap[i] = new ArrayList<FlowEdge>();
			}

			seen = new int[N];
			mark = 0;
		}

		public void addEdge(int from, int to, int cap){
			FlowEdge forward = new FlowEdge(to, cap, cap);
			FlowEdge back = new FlowEdge(from, 0, 0);
			adjMap[from].add(forward);
			adjMap[to].add(back);
			forward.reverse = back;
			back.reverse = forward;
		}

		public void resetFlow(){
			for(int i = 0; i < N; i++ ){
				int sz = adjMap[i].size();
				for(int j = 0; j < sz; j++ ){
					FlowEdge e = adjMap[i].get(j);
					e.flow = e.capacity;
				}
			}
		}

		public int findAugmentingPath(int at, int sink, int val){
			int sol = 0;
			seen[at] = mark;
			if(at == sink) return val;
			int sz = adjMap[at].size();
			for(int i = 0; i < sz; i++ ){
				FlowEdge flow = adjMap[at].get(i);
				int v = flow.dest;
				int f = flow.flow;
				if(seen[v] != mark && f > 0){
					sol = findAugmentingPath(v, sink, Math.min(f, val));
					if(sol > 0){
						flow.flow -= sol;
						flow.reverse.flow += sol;
						return sol;
					}
				}
			}
			return 0;
		}

		public int getMaximumFlow(int S, int T){
			int res = 0;
			while(true){
				mark++;
				int flow = findAugmentingPath(S, T, 1000000000);
				if(flow == 0) break;
				else res += flow;
			}
			return res;
		}
	};


	public static long[] shortestPath(int source){
		previous = (List<Integer>[]) Array.newInstance(List.class, N+1);
		for(int i = 0; i < previous.length; i++){
			previous[i] = new ArrayList<Integer>();
		}

		long[] distance = new long[N];
		boolean[] visited = new boolean[N];
		for(int i=0;i<N;i++){
			distance[i] = Integer.MAX_VALUE;
		}

		distance[source] = 0;
		//hack. actually node
		PriorityQueue<Node> nextNode = new PriorityQueue<Node>(N, new Comparator<Node>(){
			@Override
			public int compare(Node o1, Node o2){
				return Long.compare(o1.weight, o2.weight);
			}
		});

		nextNode.add(new Node(source, 0));
		while(!nextNode.isEmpty()){
			Node node = nextNode.remove();
			if(node.weight > distance[node.id]){
				//java doesn't have decrease key operator :(
				//so i put multiple same objects with differing keys
				continue;
			}

			int closestV = node.id;
			visited[closestV] = true;

			List<Edge> outEdges = adjMap[closestV];
			if(outEdges == null){
				continue;
			}
			for(Edge outE: outEdges){
				if(!visited[outE.dest]){
					long newDist = distance[closestV] + outE.weight;
					long oldDist = distance[outE.dest];
					if(newDist < oldDist){
						distance[outE.dest] = newDist;
						nextNode.add(new Node(outE.dest, newDist));
						previous[outE.dest] = new ArrayList<Integer>();
					}

					if(newDist <= oldDist){
						previous[outE.dest].add(closestV);
					}
				}
			}
		}

		return distance;
	}

	private static class Node{
		public int id;
		public long weight;
		public Node(int id, long we){
			this.id = id;
			weight= we;
		}
	}

	public static class Edge{
		public int dest;
		public int weight;

		public Edge(int d, int w){
			dest = d;
			weight= w;
		}
	}

	private static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}


		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int readInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public String readString() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
