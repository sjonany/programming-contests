package WF2013;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Factors {
	static BigInteger MAX = BigInteger.valueOf(2).pow(63);
	static List<Integer> primes;
	static Map<BigInteger, BigInteger> ansMap;
	static BigInteger[] fac;
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		primes = getPrimes(62);
		
		fac = new BigInteger[63];
		fac[0] = BigInteger.ONE;
		for(int i = 1; i < fac.length; i++){
			fac[i] = fac[i-1].multiply(BigInteger.valueOf(i));
		}
		
		ansMap = new HashMap<BigInteger, BigInteger>();
		count(62, Integer.MAX_VALUE, BigInteger.ONE, 0, new ArrayList<Integer>());
		String line = in.readLine();
		while(line != null){
			BigInteger n = new BigInteger(line);
			out.println(n + " " + ansMap.get(n));
			line = in.readLine();
		}
		
		out.close();
	}

	//count the number of ways to throw pebbles if I have pebLeft pebbles, and previously I threw prev
	//I don't need to consume all the pebbles. Pebbles have to be in decreasing order
	//count(m,n) = if I throw i pebbles right now.
	//curPrimeIndex = current pebble weight
	static void count (int pebLeft, int prev, BigInteger cur, int curPrimeIndex, List<Integer> history){
		if(pebLeft == 0){
			store(history, cur);
		}else{
			int maxThrow = Math.min(pebLeft, prev);
			//if I throw 0.. ends now
			store(history, cur);
			BigInteger newCur = cur;
			BigInteger curPrime = BigInteger.valueOf(primes.get(curPrimeIndex));
			for(int start = 1; start <= maxThrow; start++){
				newCur = newCur.multiply(curPrime);
				if(newCur.compareTo(MAX) >= 0)
					break;
				history.add(start);
				count(pebLeft - start, start, newCur, curPrimeIndex+1, history);
				history.remove(history.size()-1);
			}
		}
	}
	
	static void store(List<Integer> history, BigInteger smallestK){
		if(history.size() == 0)return;
		int num = 0;
		for(int  i : history) num += i;
		BigInteger ans = fac[num];
		for(int i : history){
			ans = ans.divide(fac[i]);
		}
		if(ansMap.containsKey(ans)){
			BigInteger prevK = ansMap.get(ans);
			if(smallestK.compareTo(prevK) < 0){
				ansMap.put(ans, smallestK);
			}
		}else{
			ansMap.put(ans, smallestK);
		}
	}
	
	//n=max num to be considered
	public static List<Integer> getPrimes(int N){
        boolean[] isPrime = new boolean[500];
        List<Integer> primes = new ArrayList<Integer>();
        for (int i = 2; i < 500; i++) {
            isPrime[i] = true;
        }
        
        int lastI = 0;
        for (int i = 2; i*i < 500; i++) {
            if (isPrime[i]) {
            	primes.add(i);
            	if(primes.size() >= N){
            		return primes;
            	}
                for (int j = i; i*j < 500; j++) {
                    isPrime[i*j] = false;
                }
            }
            lastI = i;
        }
        
        for(int i = lastI+1; i < isPrime.length; i++){
        	if(isPrime[i]){
            	primes.add(i);
            	if(primes.size() >= N){
            		return primes;
            	}
        	}
        }

        return primes;
	}
}
