package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class L {
	static String vow = "aiyeou";
	static String con = "bkxznhdcwgpvjqtsrlmf";
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/Tongues/Tongues.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/Tongues/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		while(true){
			String line = in.readLine();
			if(line == null)break;
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < line.length(); i++){
				boolean isup  = Character.isUpperCase(line.charAt(i));
				char lo = Character.toLowerCase(line.charAt(i));
				if(Character.isAlphabetic(lo)){
					if(vow.contains(""+lo)){
						int ii = vow.indexOf(lo) - 3;
						if(ii<0)ii+=vow.length();
						char newc = vow.charAt(ii);
						if(isup){
							sb.append(Character.toUpperCase(newc));
						}else{
							sb.append(newc);
						}
					}else if(con.contains("" + lo)){
						int ii = con.indexOf(lo) - 10;
						if(ii<0)ii+=con.length();
						char newc = con.charAt(ii);
						if(isup){
							sb.append(Character.toUpperCase(newc));
						}else{
							sb.append(newc);
						}
					}
				}else{
					sb.append(lo);
				}
			}
			out.println(sb.toString());
		}

		out.close();
	}

	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
