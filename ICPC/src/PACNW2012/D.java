package PACNW2012;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;


public class D {
	static int[] touch;
	static double[] angles;
	static Point2D.Double[] points;
	static int N;
	static int W;
	static int H;

	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/Partition/Partition.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/Partition/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		while(true){
			tk = tk(in.readLine());
			N = Integer.parseInt(tk.nextToken());
			W = Integer.parseInt(tk.nextToken());
			H = Integer.parseInt(tk.nextToken());

			if(N==0 && W==0 &&H==0)break;
			points = new Point2D.Double[N];
			for(int i = 0; i < N; i++){
				tk = tk(in.readLine());
				points[i] = new Point2D.Double(Integer.parseInt(tk.nextToken()), Integer.parseInt(tk.nextToken()));
			}

			final Point2D.Double center = new Point2D.Double(W/2.0,H/2.0);
			final Point2D.Double axis = new Point2D.Double(W, H/2.0);
			Arrays.sort(points, new Comparator<Point2D.Double>(){
				@Override
				public int compare(java.awt.geom.Point2D.Double o1,
						java.awt.geom.Point2D.Double o2) {
					return Double.compare(getAngle(center, axis, o1), getAngle(center, axis, o2));
				}
			});

			int Astart = 0;
			for(; Astart < points.length; Astart++){
				int Aend = (Astart + N/2-1)%N;
				int Bstart = (Aend + 1) %N;
				int Bend = (Bstart + N/2-1)%N;
				if(getAngle(center, points[Astart], points[Aend]) <= Math.PI && 
						getAngle(center, points[Bstart], points[Bend]) <= Math.PI){
					break;
				}
			}
			
			for(int i = 0; i < N/2; i++){
				int j = (Astart + i) % N;
				out.println(String.format("%d %d", (int)Math.round(points[j].x), (int)Math.round(points[j].y)));
			}
		}

		out.close();
	}
	
	static double getAngle(Point2D.Double center, Point2D.Double axis, Point2D.Double dest){
		double dx1 = axis.x - center.x;
		double dy1 = axis.y - center.y;
		double dx2 = dest.x - center.x;
		double dy2 = dest.y - center.y;

		double result = Math.atan2(dy2,dx2) - Math.atan2(dy1, dx1);
		if(result < 0) result += 2.0 * Math.PI;
		return result;
	}


	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
