package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class E {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/RingsAndRunes/RingsAndRunes.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/RingsAndRunes/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		int g = Integer.parseInt(in.readLine());
		
		int[] shift = new int[23];
		for(int i = 0; i < shift.length; i++){
			shift[i] = 1<<i;
		}
		for(int test = 0 ; test < g; test++){
			int good = 0;
			tk = tk(in.readLine());
			int numRing = Integer.parseInt(tk.nextToken());
			int numRune = Integer.parseInt(tk.nextToken());

			String status = null;
			int[][] rules = new int[numRune][3];
			for(int i= 0 ; i < rules.length; i++){
				String line = in.readLine();
				tk = tk(line);
				
				rules[i][0] = Integer.parseInt(tk.nextToken());
				rules[i][1] = Integer.parseInt(tk.nextToken());
				rules[i][2] = Integer.parseInt(tk.nextToken());
				
				if(rules[i][0] == 0 || 
						rules[i][1] == 0 || 
						rules[i][2] == 0 ){
					good = 3;
					status = ("INVALID: NULL RING");
				}
				
				if(Math.abs(rules[i][0]) > numRing || 
						Math.abs(rules[i][1]) > numRing || 
						Math.abs(rules[i][2]) > numRing ){
					if(good < 2){
						good = 2;
						status=("INVALID: RING MISSING");
					}
				}
				if(Math.abs(rules[i][0]) == Math.abs(rules[i][1])  || 
						Math.abs(rules[i][1]) == Math.abs(rules[i][2]) || 
								Math.abs(rules[i][0]) == Math.abs(rules[i][2]) ){
					if(good < 1){
						good = 1;
						status= ("INVALID: RUNE CONTAINS A REPEATED RING");
					}
				}
				
			}
			if(good != 0){
				out.println(status);
				continue;
			}
			boolean found = false;
			for(int i = 0; i < shift[numRing]; i++){
				boolean sat = true;
				for(int[] rule : rules){
					boolean rulesat = false;
					for(int j = 0; j < 3; j++){
						boolean setTrue = rule[j] > 0;
						int ind = Math.abs(rule[j]);
						ind--;
						boolean isConfigTrue = (i & shift[ind]) != 0;
						if(isConfigTrue == setTrue){
							rulesat = true;
							break;
						}
					}
					if(!rulesat) {
						sat= false;
						break;
					}
				}
				if(sat){
					found = true;
					break;
				}
			}
			
			if(!found){
				out.println("RUNES UNSATISFIABLE! TRY ANOTHER GATE!");
			}else{
				out.println("RUNES SATISFIED!");
			}
		}

		out.close();
	}

	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
