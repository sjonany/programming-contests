package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class K {
	static char[][] board;
	static int M;
	static int N;
	
	static int[][] dirs = new int[][]{{0,1},{1,0},{-1,0},{0,-1}};
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/TileCut/TileCut.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/TileCut/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		while(true){
			String line = in.readLine();
			if(line == null) break;
			List<String> input = new ArrayList<String>();
			input.add(line);
			boolean end = false;
			while(true){
				line = in.readLine();
				if(line == null){
					end = true;
					break;
				}else if(line.trim().length() == 0){
					break;
				}
				input.add(line.trim());
			}
			M = input.size();
			N = input.get(0).length();
			board = new char[M][N];
			for(int r = 0; r < M; r++){
				for(int c = 0; c < N; c++){
					board[r][c] = input.get(r).charAt(c);
				}
			}
			
			int S = 2*M*N ;
			int T = 2*M*N+1;
			MaxFlow maxflow = new MaxFlow(M*N+2 + M*N);

			for(int r = 0; r < M; r++){
				for(int c = 0; c < N; c++){
					char ch = board[r][c];
					if(ch == 'W'){
						int mypos = conv(r,c);
						maxflow.addEdge(S, mypos, 1);
						for(int[] dir : dirs){
							int nr = r + dir[0];
							int nc = c + dir[1];
							if(valid(nr, nc) && board[nr][nc] == 'I'){
								maxflow.addEdge(mypos, conv(nr,nc), 1);
							}
						}
					}else if(ch == 'I'){
						int mypos = conv(r,c);
						maxflow.addEdge(mypos, M*N + mypos, 1);
						for(int[] dir : dirs){
							int nr = r + dir[0];
							int nc = c + dir[1];
							if(valid(nr, nc) && board[nr][nc] == 'N'){
								maxflow.addEdge(M*N+mypos, conv(nr,nc), 1);
							}
						}
					}else if(ch == 'N'){
						maxflow.addEdge(conv(r,c), T, 1);
					}else{
						System.out.println("WTF?");
					}
				}
			}
			
			out.println(maxflow.getMaxFlow(S, T));
			if(end) break;
		}
		
		out.close();
	}
	
	static int conv(int r, int c){
		return r * N + c;
	}
	
	static boolean valid(int r, int c){
		return r >=0 && r < M && c >= 0 && c < N;
	}
	
	static class MaxFlow{
		public static class FlowEdge {
			public int dest;
			public int cap;
			public int flow;
			public FlowEdge reverse;
			
			public FlowEdge(int v, int cap, int flow){
				this.dest = v;
				this.cap = cap;
				this.flow = flow;
			}
		}
		
		private int N; 
		public List<FlowEdge>[] adjMap;
		int mark;
		int[] seen;
		
		MaxFlow(int numNode){
			this.N = numNode;
			adjMap = (List<FlowEdge>[]) Array.newInstance(List.class, N);
			for(int i = 0; i < adjMap.length; i++){
				adjMap[i] = new ArrayList<FlowEdge>();
			}
			
			seen = new int[N];
			mark = 0;
		}
		
		public void addEdge(int from, int to, int cap){
			FlowEdge forward = new FlowEdge(to, cap, cap);
			FlowEdge backward = new FlowEdge(from, 0, 0);
			adjMap[from].add(forward);
			adjMap[to].add(backward);
			forward.reverse = backward;
			backward.reverse = forward;
		}
		
		private int findAugmentingPath(int at, int sink, int val){
			int sol = 0;
			seen[at] = mark;
			if(at == sink) return val;
			int sz = adjMap[at].size();
			for(int i =0 ;i < sz; i++){
				FlowEdge flow = adjMap[at].get(i);
				int v = flow.dest;
				int f = flow.flow;
				if(seen[v]!=mark && f > 0){
					sol = findAugmentingPath(v, sink, Math.min(f, val));
					if(sol > 0){
						flow.flow -= sol;
						flow.reverse.flow += sol;
						return sol;
					}
				}
			}
			return 0;
		}
		
		public int getMaxFlow(int S, int T){
			int res = 0;
			while(true){
				mark++;
				int flow = findAugmentingPath(S,T,Integer.MAX_VALUE);
				if(flow==0)break;
				else res += flow;
			}
			return res;
		}
		
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
