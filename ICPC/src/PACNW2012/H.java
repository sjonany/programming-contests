package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class H {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/SeatingChart/SeatingChart.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/SeatingChart/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		while(true){
			int N = Integer.parseInt(in.readLine());
			if(N==0)break;
			tk = tk(in.readLine());
			Map<String, Integer> map = new TreeMap<String, Integer>();
			for(int i = 0; i < N; i++){
				map.put(tk.nextToken(), i);
			}
			
			tk = tk(in.readLine());
			int[] arr = new int[N];
			for(int i = 0; i < N ; i++){
				arr[i] = map.get(tk.nextToken());
			}
			out.println(countinv(arr));
		}
		
		out.close();
	}
	
	static long countinv(int[] arr){
		return count(arr, 0, arr.length-1);
	}
	
	static long count(int[] arr, int l, int r){
		if(l>=r){
			return 0;
		}
		long tot = 0;
		int m = (l+r)/2;
		tot += count(arr, l, m);
		tot += count(arr, m+1, r);
		
		int li = l;
		int ri = m+1;
		List<Integer> merged = new ArrayList<Integer>();
		
		while(li <= m && ri <= r){
			if(arr[li] < arr[ri]){
				merged.add(arr[li]);
				li++;
			}else{
				tot += m - li + 1;
				merged.add(arr[ri]);
				ri++;
			}
		}
		
		while(li <= m){
			merged.add(arr[li]);
			li++;
		}
		while(ri <= r){
			merged.add(arr[ri]);
			ri++;
		}
		
		for(int i = 0; i < r-l+1;i++){
			arr[l+i] = merged.get(i);
		}
		return tot;
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
