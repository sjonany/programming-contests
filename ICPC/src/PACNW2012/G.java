package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class G {
	static int MAX_BIT = 55;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/SarumansLevelUp/SarumansLevelUp.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/SarumansLevelUp/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		long[][] comb = new long[MAX_BIT+1][MAX_BIT+1];
		comb[1][1] = 1;
		for(int i = 2; i <= MAX_BIT; i++){
			for(int j = 1; j <= i; j++) {
				if(j==1){
					comb[i][j] = i;
				}else{
					comb[i][j] = comb[i-1][j] + comb[i-1][j-1];
				}
			}
		}
		
		int test = 1;
		while(true){
			String line = in.readLine();
			if(line == null || line.trim().length() ==0){
				break;
			}
			
			long N = Long.parseLong(line);
			//num ways to make j ones, if consider values <= s[0..i]
			long[][] dp = new long[MAX_BIT][MAX_BIT+1];
			
			if(N%2 == 0){
				dp[0][0] = 1;
			}else{
				dp[0][0] = 1;
				dp[0][1] = 1;
			}
			for(int i = 1; i < MAX_BIT; i++){
				dp[i][0] = 1;
				int numbit = i+1;
				for(int j = 1; j <= numbit; j++){
					if((N & (1l<<i)) == 0){
						dp[i][j] = dp[i-1][j];
					}else{
						dp[i][j] = comb[numbit-1][j];
						dp[i][j] += dp[i-1][j-1];
					}
				}	
			}
			/*
			for(int i = 0; i<= 5; i++){
				for(int j = 0; j <= 5; j++){
					System.out.println("consider <= a[..i]" + " " + i + " " +  j + " ones = " + dp[i][j]);
				}
			}*/
			
			long tot = 0;
			for(int i = 3; i <= MAX_BIT;i+=3){
				tot += dp[MAX_BIT-1][i];
			}
			out.println(String.format("Day %d: Level = %d", N , tot));
			test++;
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
