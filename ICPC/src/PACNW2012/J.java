package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class J {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/TempleBuild/TempleBuild.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/TempleBuild/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;
		
		while(true){
			String line = in.readLine();
			if(line == null || line.trim().length() == 0)break;
			tk = tk(line);
			long H = Long.parseLong(tk.nextToken());
			long T = Long.parseLong(tk.nextToken());
			long B = Long.parseLong(tk.nextToken());
			
			if(B < T){
				long temp = B;
				B = T;
				T = temp;
			}
			
			//max volume if toppest is at i
			long[] dp = new long[(int) H+1];
			long[] arr = new long[3];
			long maxSide = 0;
			long minSide = Integer.MAX_VALUE;
			for(int i = 0 ; i < 3 ; i++){
				arr[i] = Long.parseLong(tk.nextToken());
				maxSide = Math.max(arr[i], maxSide);
				minSide = Math.min(arr[i], minSide);
			}
			if(H==0 || maxSide == 0){
				out.println(0);
				continue;
			}
			double rat = 1.0 * (B-T)/H;
			long[] vols = new long[3];
			for(int i = 0; i < 3 ; i++){
				vols[i] = arr[i]*arr[i]*arr[i];
			}
			
			long max = Integer.MIN_VALUE;
			for(int i = 0 ; i <= H; i++){
				for(int sideId = 0; sideId< arr.length; sideId++){
					long side = arr[sideId];
					if(side==0)continue;
					if(i - side >= 0){
						long vol = 0;
						double x = i * rat;
						double w = B - x;
						long maxRep = (long)(w / side);
						long totalRep = maxRep * maxRep;
						vol = vols[sideId] * totalRep;
						dp[i] = Math.max(dp[i], dp[(int)(i-side)] + vol);
					}
				}
				max = Math.max(dp[i], max);
			}
			out.println(max);
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
