package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class A {
	static int[] w1 = new int[]{1, 
		2
		,3
		, 3
		,4
		,10};
	static int[] w2 = new int[]{
		1
		 ,2
		,2
		,2
		,3
		,5
		,10
	};
	
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/GoodVersusEvil/GoodVersusEvil.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/GoodVersusEvil/my.out");
		StringTokenizer tk;
		
		int TEST = Integer.parseInt(in.readLine());
		for(int test = 1; test <= TEST; test++){
			long s1 = 0;
			long s2 = 0;
			
			int[] arr1 = new int[6];
			String line = in.readLine();	
			tk = tk (line);
			for(int i = 0; i < w1.length;i++){
				arr1[i] = Integer.parseInt(tk.nextToken());
				s1 += (long)arr1[i] * w1[i];
			}
			int[] arr2 = new int[w2.length];
			tk = tk (in.readLine());
			for(int i = 0; i < w2.length;i++){
				arr2[i] = Integer.parseInt(tk.nextToken());
				s2 += (long)arr2[i] * w2[i];
			}
			out.printf("Battle %d: ", test);
			if(s1 == s2){
				out.println("No victor on this battle field");
			}else if(s1 > s2){
				out.println("Good triumphs over Evil");
			}else{
				out.println("Evil eradicates all trace of Good");
			}
		}
		
		
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
