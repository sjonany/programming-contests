package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;


public class B {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/MagicMultiple/MagicMultiple.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/MagicMultiple/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		
		while(true){
			String line = in.readLine();
			if(line == null || line.trim().length()==0)break;
			int n = Integer.parseInt(line);
			int k = 1;
			boolean[] touched = new boolean[10];
			long cur = n;
			while(true){
				long temp = cur;
				while(temp > 0){
					touched[(int)(temp % 10)] = true;
					temp /= 10;
				}
				boolean good = true;
				for(int i = 0; i <= 9; i++){
					if(!touched[i]){ 
						good = false;
						break;
					}
				}
				if(good){
					break;
				}
				cur+=n;
				k++;
			}
			out.println(k);
		}
		
		
		StringTokenizer tk;
		
		
		out.close();
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
