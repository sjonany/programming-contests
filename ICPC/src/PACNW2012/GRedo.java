package PACNW2012;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class GRedo {
	static int MAX_BIT = 55;
	static long[][] dp;
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/pacnw2012/SarumansLevelUp/SarumansLevelUp.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/pacnw2012/SarumansLevelUp/my.out");
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
		StringTokenizer tk;

		//num ways to make #ones mod 3 = j, if consider i digits
		dp = new long[MAX_BIT][3];
		
		dp[0][0] = 1;
		dp[0][1] = 1;
		
		for(int i = 1; i < MAX_BIT; i++){
			for(int j = 0; j < 3; j++){
				dp[i][j] = dp[i-1][j] + dp[i-1][(j-1+3) %3];
			}
		}
		
		int test = 1;
		while(true){
			String line = in.readLine();
			if(line == null || line.trim().length() ==0){
				break;
			}
			
			long N = Long.parseLong(line);
			
			String s = Long.toBinaryString(N);
			long tot = q(s,0,0);
			
			out.println(String.format("Day %d: Level = %d", N , tot-1));
			test++;
		}
		
		out.close();
	}
	
	//count <= s[i..n] with wantmod 1's 
	static long q(String s, int i, int wantmod){
		if(i == s.length()-1){
			boolean iszero = s.charAt(i) == '0';
			if(iszero){
				if(wantmod == 0) return 1;
			}else{
				if(wantmod == 0) return 1;
				if(wantmod == 1) return 1;
			}
			return 0;
		}
		
		if(s.charAt(i) == '0'){
			return q(s, i+1, wantmod);
		}else{
			int remlength = s.length() - i - 1;
			long ifzero = dp[remlength-1][wantmod];
			long ifoneupward = q(s, i+1 , (wantmod-1+3)%3);
			return ifzero + ifoneupward;
		}
	}
	
	static StringTokenizer tk(String line){
		return new StringTokenizer(line);
	}
}
