package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class H {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	
	public static void main(String[] args) throws Exception {
		BigInteger[] arr = new BigInteger[82];
		for(int i = 1; i <= 81; i++){
			BigInteger f = BigInteger.ZERO;
			BigInteger m = BigInteger.ONE;
			for(int gen = 1; gen < i; gen++){
				P p = get(f, m);
				f = p.F;
				m = p.M;
			}
			arr[i] = f.add(m);
		}
		
		while(true){
			int i = Integer.parseInt(in.readLine());
			if(i==0)break;
			out.println(arr[i+1]);
		}
		out.close();
	}
	
	static P get(BigInteger F, BigInteger M){
		P p = new P();
		p.F = p.F.add(F);
		p.M = p.M.add(F);
		p.F = p.F.add(M);
		return p;
	}
	
	static class P{
		public BigInteger F;
		public BigInteger M;
		public P() {
			F = BigInteger.ZERO;
			M = BigInteger.ZERO;
		}
		
	}
	
	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
