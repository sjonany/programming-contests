package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class D {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	
	static long MOD = 1000000007;
	static int N = 1000000;
	public static void main(String[] args) throws Exception {
		long[] dp1 = new long[N+1];
		long[] dp2 = new long[N+1];
		
		dp1[0] = 1;
		dp2[0] = 1;
		for(int i = 1; i <= N; i++){
			dp1[i] += 2 * dp1[i-1];
			dp1[i] %= MOD;
			if(i>=2){
				dp1[i] += dp1[i-2];
				dp1[i] %= MOD;
				dp1[i] += (4 * ((dp2[i-1] - dp1[i-1]) % MOD)) % MOD;
			}
			dp1[i] %= MOD;
			while(dp1[i] < 0){
				dp1[i] += MOD;
			}
			
			dp2[i] += dp1[i];
			dp2[i] %= MOD;
			dp2[i] += dp2[i-1];
			dp2[i] %= MOD;
			while(dp2[i] < 0){
				dp2[i] += MOD;
			}
		}
		
		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0; test <TEST; test++){
			out.println(dp1[Integer.parseInt(in.readLine())]);
		}
		
		out.close();
	}
	
	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
