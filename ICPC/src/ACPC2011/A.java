package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

//http://acm.hust.edu.cn/vjudge/contest/view.action?cid=33723#overview
public class A {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	public static void main(String[] args) throws Exception {
		int T = Integer.parseInt(in.readLine());
		for(int test = 0; test < T ; test++){
			tk = tk(in.readLine());
			int C = Integer.parseInt(tk.nextToken());
			int D = Integer.parseInt(tk.nextToken());
			if((C&D) == C){
				String Cs = Integer.toBinaryString(C);
				String Ds = Integer.toBinaryString(D);
				int[] cc = new int[32];
				int[] dd = new int[32];
				for(int i = 0; i < Cs.length(); i++){
					cc[31-i] = Cs.charAt(Cs.length()-i-1)-'0';
				}

				for(int i = 0; i < Ds.length(); i++){
					dd[31-i] = Ds.charAt(Ds.length()-i-1)-'0';
				}
				
				int[] aa = cc.clone();
				int[] bb = cc.clone();
				
				boolean added = false;
				for(int i = 0; i  < 32 ;i++){
					if(dd[i] == 1){
						if(cc[i] == 0){
							if(added){
								aa[i] = 1;
							}else{
								bb[i] = 1;
								added = true;
							}
						}
					}
				}
				int A = 0;
				int B = 0;
				for(int i = 0; i < 32; i++){
					A += (1 << i) * aa[32-i-1];
					B += (1 << i) * bb[32-i-1];
				}
				out.println(A + " " + B);
			}else{
				out.println(-1);
			}
		}
		out.close();
	}

	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
