package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class G {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	
	static int N;
	static int[][] edges;
	static List<Integer>[] sloedges;
	
	public static void main(String[] args) throws Exception {
		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0; test < TEST; test++){
			N = Integer.parseInt(in.readLine());
			sloedges = (List<Integer>[]) Array.newInstance(List.class, N);
			edges = new int[N][];
			for(int i = 0; i < edges.length; i++){
				sloedges[i] = new ArrayList<Integer>();
			}
			for(int i = 0; i < N-1; i++){
				tk = tk (in.readLine());
				int a = Integer.parseInt(tk.nextToken());
				int b = Integer.parseInt(tk.nextToken());
				sloedges[a].add(b);
				sloedges[b].add(a);
			}
			
			for(int i = 0; i < N; i++) {
				edges[i] = new int[sloedges[i].size()];
				int j = 0;
				for(int child : sloedges[i]){
					edges[i][j++] = child;
				}
			}
			
			out.println(String.format("$%d", solve()));
		}
		
		out.close();
	}
	
	//min cost to cover all edges rooted at i, if I also have the ability to cover top edge, or not necessarily
	static int[] dpTop;
	static int[] dp;
	static int[] dpMinTop;
	static int solve() {
		dp = new int[N];
		dpTop = new int[N];
		dpMinTop = new int[N];
		dfs(0, -1);
		return dp[0];
	}
	
	static int ONE = 100;
	static int TWO = 175;
	static int ALL = 500;
	
	static void dfs(int cur, int par) {
		boolean isLeaf = true;
		//preprocess stuff that will make life easier
		int sumDpTop = 0;
		int sumDp = 0;
		int minDpMinTop = Integer.MAX_VALUE/2;
		int min2DpMinTop = Integer.MAX_VALUE/2;
		int childCount = 0;
		for(int child : edges[cur]){
			if(child == par) continue;
			isLeaf = false;
			dfs(child, cur);
			sumDpTop += dpTop[child];
			sumDp += dp[child];
			dpMinTop[child] = dp[child] - dpTop[child];
			if(dpMinTop[child] < minDpMinTop) {
				min2DpMinTop = minDpMinTop;
				minDpMinTop = dpMinTop[child];
			} else if (dpMinTop[child] < min2DpMinTop) {
				min2DpMinTop = dpMinTop[child];
			}
			childCount++;
		}
		
		dp[cur] = Integer.MAX_VALUE/2;
		dpTop[cur] = Integer.MAX_VALUE/2;
		if(isLeaf) {
			dp[cur] = 0;
			dpTop[cur] = ONE;
		} else {
			//case: have to cover parent
			//if single, children have to be dptop
			dpTop[cur] = Math.min(dpTop[cur], ONE + sumDpTop);
			
			//if two, choose one child to be dp, the rest are dp top
			dpTop[cur] = Math.min(dpTop[cur], TWO + sumDpTop + minDpMinTop);
			
			//if star
			dpTop[cur] = Math.min(dpTop[cur], ALL + sumDp);
			
			//case: don't have to cover parent, but you may if you want to
			//---case when you have to not cover the parent
			//vertex does not have plant -> all children must be dpTop
			dp[cur] = Math.min(dp[cur], sumDpTop);
			//vertex has plant
			//if single, ONE + sum dpTop - dpTop[oneChild] + dp[one child] 
			dp[cur] = Math.min(dp[cur], ONE + sumDpTop + minDpMinTop);
			
			//if two, TWO+ sum dpTop - dpTop[twoChild] + dp[two child]
			if(childCount >= 2) {
				dp[cur] = Math.min(dp[cur], TWO + sumDpTop + minDpMinTop + min2DpMinTop);
			}

			//---case when you cover the parent included
			dp[cur] = Math.min(dp[cur], dpTop[cur]);
		}
	}
	
	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
