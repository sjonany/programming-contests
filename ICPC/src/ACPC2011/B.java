package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class B {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	public static void main(String[] args) throws Exception {
		int TEST = Integer.parseInt(in.readLine());
		for(int test =0; test < TEST; test++){
			int M = Integer.parseInt(in.readLine());
			int N = 1<<M;
			Person[] parr = new Person[N];
			int max = 0;
			for(int i = 0; i< N; i++){
				tk = tk(in.readLine());
				Person p = new Person(tk.nextToken(), Integer.parseInt(tk.nextToken()));
				parr[i] = p;
				max = Math.max(max, p.win);
			}
			
			int R = 3*M - max;
			//System.out.println("R = " + R);
			
			Person[] parr2 = new Person[N];
			for(int i = 0; i < N; i++){
				int w = parr[i].win;
				int round = 0;
				if(w < 2*R){
					round = w/2 + 1;
				}else{
					round = R + ((w-2*R)/3) + 1;
				}
				parr2[i] = new Person(parr[i].name, round);
				//System.out.println(parr[i].name + " to round " + round);
			}
			Arrays.sort(parr2);
			for(Person p : parr2){
				out.println(p.name);
			}
		}
		
		out.close();
	}
	
	static class Person implements Comparable<Person>{
		public String name;
		public int win;
		public Person(String name, int win) {
			this.name = name;
			this.win = win;
		}
		@Override
		public int compareTo(Person o) {
			if(this.win != o.win){
				return -Integer.compare(this.win, o.win);
			}
			return name.compareTo(o.name);
		}
	}

	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
