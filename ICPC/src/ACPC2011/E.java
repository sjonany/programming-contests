package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class E {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	
	public static void main(String[] args) throws Exception {
		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0; test < TEST; test++){
			int n = Integer.parseInt(in.readLine());
			List<Integer> ans = new ArrayList<Integer>();
			int[] s = new int[n];
			add(n, s, ans, 0);
			
			List<Integer> realans = new ArrayList<Integer>();
			int mask = (1<<n) - 1;
			int c = 0;
			for(int i : ans){
				if(c % 2 == 0){
					i^=mask;
				}
				realans.add(i);
				c++;
			}
			for(int i : realans){
				out.println(i);
			}
		}
		out.close();
	}

	//pre = s[index...] is where you start
	//post = s[index...] is where you end & you filled in ans
	static void add(int N, int[] s, List<Integer> ans, int index){
		if(N == 1){
			int a = 0;
			for(int i = 0; i < s.length-1; i++){
				if(s[i] == 1){
					a += 1<<(s.length-i-1); 
				}
			}
			if(s[index] == 1){
				ans.add(a+1);
				ans.add(a);
				s[index] = 0;
			}else{
				ans.add(a);
				ans.add(a+1);
				s[index] = 1;
			}
		}else{
			if(s[index] == 0){
				add(N-1, s, ans, index+1);
				s[index] = 1;
				add(N-1, s, ans, index+1);
			}else{
				add(N-1, s, ans, index+1);
				s[index] = 0;
				add(N-1, s, ans, index+1);
			}
		}
	}
	
	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
