package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class C {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	static long P[][][] = new long[11][11][1<<10];
	public static void main(String[] args) throws Exception {
		int T = Integer.parseInt(in.readLine());

		List<Integer>[] bits = (List<Integer>[]) Array.newInstance(List.class, 11);
		for(int i = 0; i < bits.length; i++){
			bits[i] = new ArrayList<Integer>();
		}
		for(int i = 0; i < 1<<10; i++){
			bits[Integer.bitCount(i)].add(i);
		}
		for(int n = 1; n <= 10; n++){
			P[n][0][0] = 1;
			for(int k = 1; k <= 10; k++){
				for(int i = 1; i < 1<<10; i++){
					for(int last = 0; last <10; last++){
						int extrabit = 1<<last;
						if( (i & extrabit) != 0){
							P[n][k][i] += P[n][k-1][i];
							P[n][k][i] += P[n][k-1][i & (~extrabit)];
						}
					}
				}
			}
		}

		//den[N][K] = N^K
		long[][] den = new long[11][11];
		for(int n = 0; n <= 10; n++){
			den[n][0] = 1;
			for(int k = 1; k <= 10; k++){
				den[n][k] = den[n][k-1] * n;
			}
		}		
		for(int test = 0; test < T; test++){
			tk = tk(in.readLine());
			int n = Integer.parseInt(tk.nextToken());
			int m = Integer.parseInt(tk.nextToken());
			int num = Integer.parseInt(tk.nextToken());

			long numerator = 0;
			long denom = den[n][m];
			for(int bit : bits[num]){
				if(Integer.toBinaryString(bit).length() <= n){
					numerator += P[n][m][bit];
				}
			}
			long g = gcd(numerator, denom);
			numerator /= g;
			denom /= g;
			if(numerator == 0){
				out.println(0);
			}else if(denom == 1){
				out.println(1);
			}else{
				out.println(String.format("%d/%d", numerator, denom));
			}
		}

		out.close();
	}

	static long gcd(long a, long b){
		while(b > 0){
			long c = b;
			b = a % b;
			a = c;
		}
		return a;
	}

	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
