package ACPC2011;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public class I {
	static InputReader in = new InputReader(System.in);
	static PrintWriter out = new PrintWriter(System.out);
	
	public static void main(String[] args) throws Exception {
		int TEST = in.nextInt();
		for(int test = 0 ; test <TEST ;test++){
			int H = in.nextInt();
			int W = in.nextInt();
			int N = in.nextInt();
			
			char[][] board = new char[H][W];
			for(int r = 0; r < H; r++){
				String line = in.nextToken();
				for(int c = 0; c < W; c++){
					board[r][c] = line.charAt(c);
				}	
			}
			int ans = 0;
			
			//below is a sidewalk
			for(int r = 1; r < H-1; r++){
				int c = 1;
				while(c < W-1) { 
					if(board[r][c] == '.' && board[r+1][c] == '-' && board[r][c-1] != '|' && board[r][c+1] != '|'){
						int count = 0;
						while(true){
							if(c + 1 >= W) break;
							if(board[r][c] == '.' && board[r+1][c] == '-' && board[r][c-1] != '|' && board[r][c+1] != '|'){
								count++;
								c++;
							}else{
								break;
							}
						}
						ans += solve(count);
					}else{
						c++;
					}
				}
			}
			
			//above is a sidewalk
			for(int r = 1; r < H - 1; r++){
				int c = 1;
				while(c < W-1) { 
					if(board[r][c] == '.' && board[r-1][c] == '-' && board[r][c-1] != '|' && board[r][c+1] != '|'){
						int count = 0;
						while(true){
							if(c + 1 >= W) break;
							if(board[r][c] == '.' && board[r-1][c] == '-' && board[r][c-1] != '|' && board[r][c+1] != '|'){
								count++;
								c++;
							}else{
								break;
							}
						}
						ans += solve(count);
					}else{
						c++;
					}
				}
			}
			
			//left is a sidewalk
			for(int c = 1; c < W - 1; c++){
				int r = 1;
				while(r < H-1) { 
					if(board[r][c] == '.' && board[r][c-1] == '|' && board[r-1][c] != '-' && board[r+1][c] != '-'){
						int count = 0;
						while(true){
							if(r + 1 >= H) break;
							if(board[r][c] == '.' && board[r][c-1] == '|' && board[r-1][c] != '-' && board[r+1][c] != '-'){
								count++;
								r++;
							}else{
								break;
							}
						}
						ans += solve(count);
					}else{
						r++;
					}
				}
			}
			
			//right is a sidewalk
			for(int c = 1; c < W - 1; c++){
				int r = 1;
				while(r < H-1) { 
					if(board[r][c] == '.' && board[r][c+1] == '|' && board[r-1][c] != '-' && board[r+1][c] != '-'){
						int count = 0;
						while(true){
							if(r + 1 >= H) break;
							if(board[r][c] == '.' && board[r][c+1] == '|' && board[r-1][c] != '-' && board[r+1][c] != '-'){
								count++;
								r++;
							}else{
								break;
							}
						}
						ans += solve(count);
					}else{
						r++;
					}
				}
			}
			
			
			if(ans >= N){
				out.println("LOCATION OKAY");
			}else{
				out.println("CHOOSE ANOTHER LOCATION");
			}
		}
		
		out.close();
	}
	
	static int solve(int l) {
		return (l+1)/3;
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}
		
		
		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}

}
