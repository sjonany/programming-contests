package ACPC2011;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class F {

	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static StringTokenizer tk;
	static PrintWriter out = new PrintWriter(System.out);
	
	public static void main(String[] args) throws Exception {
		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0; test < TEST; test++){
			String line = in.readLine();
			//dp[i] = min to express 0..i
			int[] dp = new int[line.length()];
			dp[0] = 1;
			for(int i = 1; i < line.length(); i++){
				//either last char is alone
				dp[i] = 1 + dp[i-1];
				//or last char is a group, and best to greedy extend backward
				for(int groupSize = 1; groupSize <= i; groupSize++) {
					if(groupSize * 2 > i+1) {
						break;
					}
					int count = 1;
					int oriLeft = i - groupSize + 1;
					int left = oriLeft - groupSize;
					//current left is to be checked for validity
					while(true && left >= 0) {
						boolean good = true;
						for(int j = 0; j < groupSize; j++){
							if(line.charAt(left + j) != line.charAt(oriLeft + j)){
								good = false;
								break;
							}
						}
						if(good){
							count++;
							left -= groupSize;
						}else{
							break;
						}
					}
					left += groupSize;
					int prev = left == 0 ? 0 : dp[left-1];
					dp[i] = Math.min(dp[i], prev + 2 + groupSize + cost(count));
				}
			}
			out.println(dp[line.length()-1]);
		}
		out.close();
	}
	
	static int cost(int x){
		int c = 0;
		while(x > 0){
			x /= 10;
			c++;
		}
		return c;
	}
	
	static StringTokenizer tk(String line ){
		return new StringTokenizer(line);
	}
}
