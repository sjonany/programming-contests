package PACNW2013;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Bones {

	static long INF = (long) Math.pow(10, 18);

	static BufferedReader in;
	static PrintWriter out;

	public static void main(String[] args) throws Exception {
		//in = new BufferedReader(new InputStreamReader(System.in));
		//out = new PrintWriter(System.out);

		 in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/_IO/Bones.in"));
		 out = new PrintWriter("/Users/sjonany/Downloads/_IO/myB.out");

		int TEST = Integer.parseInt(in.readLine());
		for (int test = 0; test < TEST; test++) {
			solve();
		}
		out.close();
	}

	static void solve() throws Exception {
		StringTokenizer tk = new StringTokenizer(in.readLine());
		int N = Integer.parseInt(tk.nextToken());
		int K = Integer.parseInt(tk.nextToken());
		int M = Integer.parseInt(tk.nextToken());

		long[][] dist = new long[N][N];

		for(int i = 0; i < N; i++) {
			long[] d = dist[i];
			Arrays.fill(d, INF);
			d[i] = 0;
		}

		for (int i = 0; i < M; i++) {
			tk = new StringTokenizer(in.readLine());
			int a = Integer.parseInt(tk.nextToken());
			int b = Integer.parseInt(tk.nextToken());
			int w = Integer.parseInt(tk.nextToken());
			if(a != b){
				dist[a][b] = Math.min(w, dist[a][b]);
				dist[b][a] = Math.min(w, dist[b][a]);
			}
		}

		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				for(int k=0;k<N;k++){
					if(dist[j][i] + dist[i][k] < dist[j][k]) {
						dist[j][k] = dist[j][i] + dist[i][k];
					}
				}
			}
		}
		//first true
		long lo = 0;
		long hi = Long.MAX_VALUE;
		while(lo < hi) {
			long mid = (lo+hi)/2;
			if(check(mid, K, dist, N)) {
				hi = mid;
			} else {
				lo = mid + 1;
			}
		}
		out.println(lo);
	}

	// can recharge <= R if cap?
	static boolean check(long cap, int R, long[][] apsp, int N) {
		int[][] dist = new int[N][N];
		for(int i = 0; i< N; i++) {
			for(int j = 0; j< N; j++) {
				if(i==j)continue;
				dist[i][j] = apsp[i][j] <= cap ? 1 : Integer.MAX_VALUE / 3;
			}	
		}

		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				for(int k=0;k<N;k++){
					if(dist[j][i] + dist[i][k] < dist[j][k]) {
						dist[j][k] = dist[j][i] + dist[i][k];
					}
				}
			}
		}

		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(dist[i][j] > R) {
					return false;
				}
			}
		}
		
		return true;
	}
}
