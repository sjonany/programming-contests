package PACNW2013;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class KlingonWarfare{
	static int M;
	static int N;

	//eldest to youngest
	static Node[] first;
	static Node[] second;
	static int[] sizeFirst;
	static int[] sizeSecond;
	static List<Integer>[] edgesFirst;
	static List<Integer>[] edgesSecond;
	//cache equal ops, first_id, second_id
	static Map<Pair, Boolean> equalMap;

	public static void main(String[] args) throws Exception {
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);

		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/_IO/Klingons.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/_IO/myK.out");

		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0; test < TEST; test++){
			equalMap = new HashMap();
			StringTokenizer tk = new StringTokenizer(in.readLine());
			M = Integer.parseInt(tk.nextToken());
			N = Integer.parseInt(tk.nextToken());

			first = new Node[M];
			edgesFirst = new List[M];
			for(int i=0; i <M;i++) {
				edgesFirst[i] = new ArrayList();
				first[i] = new Node();
				first[i].first = true;
				first[i].id = i;
				tk = new StringTokenizer(in.readLine());
				first[i].style = tk.nextToken().charAt(0);
				int par = Integer.parseInt(tk.nextToken());
				if(par != -1) {
					first[par].children.add(first[i]);
					edgesFirst[par].add(i);
				}
			}
			sizeFirst = new int[M];
			dfs(sizeFirst, edgesFirst, 0, -1);

			second = new Node[N];
			edgesSecond = new List[N];
			for(int i=0; i <N;i++) {
				edgesSecond[i] = new ArrayList();
				second[i] = new Node();
				second[i].first = false;
				second[i].id = i;
				tk = new StringTokenizer(in.readLine());
				second[i].style = tk.nextToken().charAt(0);
				int par = Integer.parseInt(tk.nextToken());
				if(par != -1) {
					second[par].children.add(second[i]);
					edgesSecond[par].add(i);
				}
			}
			sizeSecond = new int[N];
			dfs(sizeSecond, edgesSecond, 0, -1);

			for(int i = M-1; i>=0; i--){
				first[i].hashCode();
				first[i].totSize = sizeFirst[i];
			}
			for(int i = N-1; i>=0; i--){
				second[i].hashCode();
				second[i].totSize = sizeSecond[i];
			}

			Set<Node> firsts = new HashSet<Node>();
			for(Node n : first) {
				firsts.add(n);
			}

			int max = 0;
			for(int i = 0; i < N; i++) {
				if(firsts.contains(second[i])) {
					max = Math.max(max, sizeSecond[i]);
				}
			}
			out.println(max);
		}
		out.close();
	}

	static boolean equals(Node n1, Node n2){
		if(!n1.first) {
			Node temp = n2;
			n2 = n1;
			n1 = temp;
		}
		Pair key = new Pair(n1.id, n2.id);
		Boolean result = equalMap.get(key);
		if(result == null){
			if (n1.style != n2.style)
				result = false;
			else if(n1.children.size() != n2.children.size())
				result = false;
			else if(n1.totSize != n2.totSize)
				result = false;

			if(result == null) {
				for(int i = 0; i < n1.children.size(); i++) {
					if(!KlingonWarfare.equals(n1.children.get(i), n2.children.get(i))){
						result = false;
						break;
					}
				} 
			}
			if(result == null)result = true;
			equalMap.put(key, result);
		}
		return result;
	}

	static class Pair {
		public int i1;
		public int i2;
		public Pair(int i1, int i2) {
			this.i1 = i1;
			this.i2 = i2;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + i1;
			result = prime * result + i2;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			Pair other = (Pair) obj;
			if (i1 != other.i1)
				return false;
			if (i2 != other.i2)
				return false;
			return true;
		}

	}

	static void dfs(int[] size, List<Integer>[] edges, int cur, int par) {
		size[cur] = 1;
		for(int child : edges[cur]) {
			if(child==par)continue;
			dfs(size, edges, child, cur);
			size[cur] += size[child];
		}
	}

	static class Node {
		public int id;
		public boolean first;
		public List<Node> children;
		public char style;
		public int hashCode;
		public int totSize;
		public boolean hasComputed;

		public Node() {
			children = new ArrayList<Node>();
			hasComputed = false;
		}

		@Override
		public int hashCode() {
			if(hasComputed) return hashCode;
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((children == null) ? 0 : children.hashCode());
			result = prime * result + style;
			hashCode = result;
			hasComputed = true;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			Node other = (Node) obj;
			//don't want first Set to complain
			if(first && other.first) return false;
			return KlingonWarfare.equals(this, other);
		}		
	}
}
