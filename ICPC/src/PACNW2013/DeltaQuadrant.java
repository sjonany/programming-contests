package PACNW2013;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class DeltaQuadrant {

	static List<Edge>[] edges;
	static int N;
	static int K;

	public static void main(String[] args) throws Exception {
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);

		long start = System.currentTimeMillis();
		BufferedReader in = new BufferedReader(new FileReader("/Users/sjonany/Downloads/_IO/DeltaQuadrant.in"));
		PrintWriter out = new PrintWriter("/Users/sjonany/Downloads/_IO/myD.out");

		int TEST = Integer.parseInt(in.readLine());
		for(int test = 0; test < TEST; test++) {
			System.out.println(test);
			StringTokenizer tk = new StringTokenizer(in.readLine());
			N = Integer.parseInt(tk.nextToken());
			K = Integer.parseInt(tk.nextToken());
			edges = new List[N];
			for(int i = 0; i < N; i++) {
				edges[i] = new ArrayList<Edge>();
			}

			for(int i = 0; i < N-1; i++) {
				tk = new StringTokenizer(in.readLine());
				int a = Integer.parseInt(tk.nextToken());
				int b = Integer.parseInt(tk.nextToken());
				int weight = Integer.parseInt(tk.nextToken());
				edges[a].add(new Edge(a,b,weight));
				edges[b].add(new Edge(b,a,weight));
			}
			dpMat = new int[2][N][K+1];
			dp = new int[N][K+1];
			numNode = new int[N];
			weight = new long[N];
			numDirChildren = new int[N];
			int ans = solve();
			out.println(2l*(totWeight-ans));
		}

		System.out.println((System.currentTimeMillis() - start) / 1000.0);
		out.close();
	}

	// dp[i][j] = maximum cut off if consider subtree rooted at i & include i, and cut off exactly j edges
	// actually dp[i][j][k], and also, can only cut from children of i <= k
	static int[][] dp;
	static int[] numNode;
	static long[] weight;
	static int[] numDirChildren;
	static long totWeight;
	static int solve() {
		int ans = Integer.MIN_VALUE;
		totWeight = -1;
		for(int root = 0; root < Math.min(21, N); root++) {
			ans = Math.max(ans, solve(root));
		}

		return ans;
	}

	static int solve(int root) {
		for(int i=0; i <N;i++){
			numDirChildren[i] = edges[i].size()-1;
		}
		numDirChildren[root]++;

		for(int i = 0; i < N; i++){
			for(int j = 0; j <= K; j++){
				dp[i][j] = -1;
				//	dp[i][j] = new int[numDirChildren[i]+1];
			}
		}

		solveDp(root, -1);
		if(totWeight == -1){
			totWeight = weight[root];
		}
		return dp[root][K];
	}

	//optimized

	static int[][][] dpMat;
	static void solveDp(int curn, int par) {
		// dp[i][j][k] = max cut off if can only cut off the first k children
		// m = how many edges to cut from the kth child -> max{dp[i][j-m][k-1] + dp[child_k][m][k]}
		numNode[curn] = 1;
		weight[curn] = 0;
		for(Edge edge : edges[curn]) {
			if(edge.to == par) continue;
			solveDp(edge.to, curn);
			numNode[curn] += numNode[edge.to];
			weight[curn] += weight[edge.to];
			weight[curn] += edge.weight;
		}

		int now = 0;
		int prev = 1;
		for(int numCut = 0; numCut <= K; numCut++) {
			dpMat[now][curn][numCut] = numCut == 0 ? 0 : -1;
		}
		now ^= 1;
		prev ^= 1;

		for(Edge edge : edges[curn]) {
			if(edge.to == par) continue;
			for(int numCut = 0; numCut <= K; numCut++) {
				// no cut at new child
				dpMat[now][curn][numCut] = dpMat[prev][curn][numCut];

				// cut at child, but maintain root connection
				for(int m = 0; m <= numCut; m++) {
					int childCut = dp[edge.to][m];
					int prevCut = dpMat[prev][curn][numCut-m];
					if(childCut == -1 || prevCut == -1) {
						continue;
					}
					dpMat[now][curn][numCut] = Math.max(dpMat[now][curn][numCut], 
							prevCut + childCut);
				}

				// drop entire tree
				int numEdgeIfDropAll = numNode[edge.to];
				if(numEdgeIfDropAll <= numCut) {
					int prevCut = dpMat[prev][curn][numCut-numEdgeIfDropAll];
					if(prevCut != -1)
						dpMat[now][curn][numCut] = Math.max(dpMat[now][curn][numCut], prevCut + (int)weight[edge.to] + edge.weight);
				}
			}
			now ^= 1;
			prev ^= 1;
		}
		
		for(int numCut = 0; numCut <= K; numCut++) {
			dp[curn][numCut] = dpMat[prev][curn][numCut]; 
		}
	}

	/*
	static void solveDp(int cur, int par) {
		// dp[i][j][k] = max cut off if can only cut off the first k children
		// m = how many edges to cut from the kth child -> max{dp[i][j-m][k-1] + dp[child_k][m][k]}
		numNode[cur] = 1;
		weight[cur] = 0;
		for(Edge edge : edges[cur]) {
			if(edge.to == par) continue;
			solveDp(edge.to, cur);
			numNode[cur] += numNode[edge.to];
			weight[cur] += weight[edge.to];
			weight[cur] += edge.weight;
		}

		int childId = 0;
		for(int numCut = 0; numCut <= K; numCut++) {
			dp[cur][numCut][0] = numCut == 0 ? 0 : -1;
		}
		for(Edge edge : edges[cur]) {
			if(edge.to == par) continue;
			for(int numCut = 0; numCut <= K; numCut++) {
				// no cut at new child
				dp[cur][numCut][childId+1] = dp[cur][numCut][childId];

				// cut at child, but maintain root connection
				for(int m = 0; m <= numCut; m++) {
					int childCut = dp[edge.to][m][numDirChildren[edge.to]];
					int prevCut = dp[cur][numCut-m][childId];
					if(childCut == -1 || prevCut == -1) {
						continue;
					}
					dp[cur][numCut][childId+1] = Math.max(dp[cur][numCut][childId+1], 
							prevCut + childCut);
				}

				// drop entire tree
				int numEdgeIfDropAll = numNode[edge.to];
				if(numEdgeIfDropAll <= numCut) {
					int prevCut = dp[cur][numCut-numEdgeIfDropAll][childId];
					if(prevCut != -1)
						dp[cur][numCut][childId+1] = Math.max(dp[cur][numCut][childId+1], prevCut + (int)weight[edge.to] + edge.weight);
				}

			}
			childId++;
		}
	}*/

	static class Edge {
		public int from;
		public int to;
		public int weight;
		public Edge(int from, int to, int weight) {
			this.from = from;
			this.to = to;
			this.weight = weight;
		}
		@Override
		public String toString() {
			return "Edge [from=" + from + ", to=" + to + ", weight=" + weight
					+ "]";
		}


	}
}
