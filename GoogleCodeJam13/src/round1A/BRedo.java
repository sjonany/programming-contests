package round1A;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Stack;
import java.util.StringTokenizer;

public class BRedo {
	public static void main(String[] args) throws Exception{	
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("B-large-practice.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Blarge.out")));
		long T = Long.parseLong(f.readLine());
		for(int i=1; i<=T;i++){
			solve(f, out, i);
		}
		out.close();
	}
	
	public static void solve(BufferedReader f, PrintWriter out, long t) throws Exception{
		out.print("Case #" + t + ": ");
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		long E = Integer.parseInt(tkz.nextToken());
		long R = Long.parseLong(tkz.nextToken());
		int N = Integer.parseInt(tkz.nextToken());
		
		if(R > E){
			R = E;
		}
		
		tkz = new StringTokenizer(f.readLine());
		long[] v = new long[N];
		for(int i=0;i<N;i++){
			v[i] = Integer.parseInt(tkz.nextToken());
		}

		Stack<Integer> days = new Stack<Integer>();
		//for the ith day, which next day is closest and higher than him
		int[] nextHigh = new int[N];
		for(int day=0; day<N; day++){
			while(!days.isEmpty() && v[days.peek()] < v[day]){
				nextHigh[days.peek()] = day;
				days.pop();
			}
			days.push(day);
		}
		
		//these guys don't have next highest
		while(!days.isEmpty()){
			nextHigh[days.peek()] = -1;
			days.pop();
		}
		
		long energy = E;
		long sum = 0;
		for(int day=0;day<N;day++){
			long spending = 0;
			
			if(nextHigh[day] == -1){
				spending = energy;
			}else{
				long minEndEnergy = E - R * (nextHigh[day]-day);
				if (energy > minEndEnergy){
					spending = energy - minEndEnergy;
					if(spending > energy){
						spending = energy;
					}
				}else{
					spending = 0;
				}
			}
			sum += spending * v[day];
			energy -= spending;
			energy = Math.min(energy+R, E);
		}
		out.println(sum);
	}
	
	
public static class FastScanner {
	BufferedReader br;
	StringTokenizer st;

	public FastScanner(String s) {
		try {
			br = new BufferedReader(new FileReader(s));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public FastScanner() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}

	String nextToken() {
		while (st == null || !st.hasMoreElements()) {
			try {
				st = new StringTokenizer(br.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return st.nextToken();
	}

	int nextInt() {
		return Integer.parseInt(nextToken());
	}

	long nextLong() {
		return Long.parseLong(nextToken());
	}

	double nextDouble() {
		return Double.parseDouble(nextToken());
	}
}

}
