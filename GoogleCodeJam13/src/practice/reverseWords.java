package practice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class reverseWords {
	public static void main(String[] args) throws IOException{	
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("B-large-practice.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("B-large.out")));
		int N = Integer.parseInt(f.readLine());
		for(int i=1; i<=N;i++){
			String line = f.readLine();
			String[] toks = line.split(" ");
			StringBuffer sb = new StringBuffer("Case #" + i+": ");
			for(int j=toks.length-1;j>=0;j--){
				if(j!=0)
					sb.append(toks[j] + " ");
				else{
					sb.append(toks[j]);
				}
			}
			out.write(sb + "\n");
		}
		out.close();
	}
	

public static class FastScanner {
	BufferedReader br;
	StringTokenizer st;

	public FastScanner(String s) {
		try {
			br = new BufferedReader(new FileReader(s));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public FastScanner() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}

	String nextToken() {
		while (st == null || !st.hasMoreElements()) {
			try {
				st = new StringTokenizer(br.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return st.nextToken();
	}

	int nextInt() {
		return Integer.parseInt(nextToken());
	}

	long nextLong() {
		return Long.parseLong(nextToken());
	}

	double nextDouble() {
		return Double.parseDouble(nextToken());
	}
}

}
