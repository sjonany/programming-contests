package round2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class A {
	public static void main(String[] args) throws Exception{	
		//BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		//PrintWriter out = new PrintWriter(System.out);
	   
		BufferedReader f = new BufferedReader(new FileReader("A-small-practice.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("A-small.out")));
		long T = Long.parseLong(f.readLine());
		for(int i=1; i<=T;i++){
			solve(f, out, i);
		}
		out.close();
	}
	
	public static void solve(BufferedReader f, PrintWriter out, long t) throws Exception{
		out.print("Case #" + t + ": ");
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		int n = Integer.parseInt(tkz.nextToken());
		int m = Integer.parseInt(tkz.nextToken());
		List<Segment> segments = new ArrayList<Segment>();
		for(int i=0; i<m; i++){
			tkz = new StringTokenizer(f.readLine());
			int s = Integer.parseInt(tkz.nextToken());
			int e = Integer.parseInt(tkz.nextToken());
			int p = Integer.parseInt(tkz.nextToken());
			segments.add(new Segment(s,e,p));
		}
		
		Collections.sort(segments);
		
		List<SegmentGroup> groups = new ArrayList<SegmentGroup>();
		int curStart = -1;
		for(Segment s : segments){
			if(s.start == curStart){
				groups.get(groups.size()-1).add(s);
			}else{
				SegmentGroup g = new SegmentGroup(s.start);
				groups.add(g);
				g.add(s);
				curStart = s.start;
			}
		}
		
		for(SegmentGroup g : groups){
			g.sort();
		}
		
		//TODO: change to big int
		long change = 0;
		while(true){
			boolean foundMerge = false;
			for(int startGroup = 0; startGroup < groups.size(); startGroup++){
				Segment start = groups.get(startGroup).segments.get(0);
				long bestChange = -1;
				Segment bestMerge = null;
				Segment bestEnd = null;
				for(int endGroup = startGroup; endGroup < groups.size(); endGroup++){
					Segment end = groups.get(endGroup).segments.get(0);
					if(end.start > start.end){
						break;
					}
					
					long inter =  Math.min(start.people, end.people);
					Segment merged = new Segment(start.start, end.end, (int)inter);
					long curChange = -inter * (start.cost() + end.cost() - merged.cost()); 
					if(curChange > bestChange){
						bestChange = curChange;
						bestMerge = merged;
						bestEnd = end;
					}
				}
				
				if(bestChange > 0){
					foundMerge = true;
					long inter =  Math.min(start.people, bestEnd.people);
					start.people -= inter;
					bestEnd.people -= inter;
					change += bestChange;
					segments.add(bestMerge);
					
					System.out.println("subtract by " + inter * start.cost() );
					System.out.println("subtract by " + inter * bestEnd.cost() );
					//System.out.println("add by " + inter * merged.cost());
					System.out.println("merged "+ start + " and " + bestEnd + " by " + inter + " people.");
					System.out.println();
				}
			}
			
			if(!foundMerge){
				break;
			}
			
			List<Segment> newSegments = new ArrayList<Segment>();
			for(Segment s : segments){
				if(s.people != 0){
					newSegments.add(s);
				}
			}
			segments = newSegments;
			Collections.sort(segments);
			
			groups = new ArrayList<SegmentGroup>();
			curStart = -1;
			for(Segment s : segments){
				if(s.start == curStart){
					groups.get(groups.size()-1).add(s);
				}else{
					SegmentGroup g = new SegmentGroup(s.start);
					groups.add(g);
					g.add(s);
					curStart = s.start;
				}
			}
			
			for(SegmentGroup g : groups){
				g.sort();
			}
		}
		
		out.println(change);
	}
	
	public static class SegmentGroup implements Comparable<SegmentGroup>{
		public int start;
		public List<Segment> segments;
		
		public SegmentGroup(int s){
			this.start = s;
			this.segments = new ArrayList<Segment>();
		}
		
		public void add(Segment s){
			this.segments.add(s);
		}

		@Override
		public int compareTo(SegmentGroup o) {
			return this.start - o.start;
		}
		
		public void sort(){
			Collections.sort(segments);
		}
	}
	
	public static class Segment implements Comparable<Segment>{
		public int start;
		public int end;
		public int people;
		
		public Segment(int s, int e, int p){
			this.start = s;
			this.end = e;
			this.people = p;
		}

		public int length(){
			return end - start + 1;
		}
		
		public long cost(){
			long l = length()-1;
			return l*(l-1)/2;
		}
		
		@Override
		public int compareTo(Segment o) {
			if(this.start != o.start){
				return this.start - o.start;
			}
			return o.end - this.end;
		}
		
		@Override
		public String toString(){
			return "[" + start + ", " + end + "]";
		}
	}
	
public static class FastScanner {
	BufferedReader br;
	StringTokenizer st;

	public FastScanner(String s) {
		try {
			br = new BufferedReader(new FileReader(s));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public FastScanner() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}

	String nextToken() {
		while (st == null || !st.hasMoreElements()) {
			try {
				st = new StringTokenizer(br.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return st.nextToken();
	}

	int nextInt() {
		return Integer.parseInt(nextToken());
	}

	long nextLong() {
		return Long.parseLong(nextToken());
	}

	double nextDouble() {
		return Double.parseDouble(nextToken());
	}
}

}
