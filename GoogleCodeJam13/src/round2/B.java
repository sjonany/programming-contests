package round2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class B {
	public static void main(String[] args) throws Exception{	
		BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
	   
		//BufferedReader f = new BufferedReader(new FileReader("B-small-practice.in"));
		//PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("B-small.out")));
		long T = Long.parseLong(f.readLine());
		for(int i=1; i<=T;i++){
			solve(f, out, i);
		}
		out.close();
	}
	
	public static void solve(BufferedReader f, PrintWriter out, long i) throws Exception{
		out.print("Case #" + i + ": ");
		StringTokenizer tkz = new StringTokenizer(f.readLine());
		long r = Long.parseLong(tkz.nextToken());
		long t = Long.parseLong(tkz.nextToken());
		
	}
	
public static class FastScanner {
	BufferedReader br;
	StringTokenizer st;

	public FastScanner(String s) {
		try {
			br = new BufferedReader(new FileReader(s));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public FastScanner() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}

	String nextToken() {
		while (st == null || !st.hasMoreElements()) {
			try {
				st = new StringTokenizer(br.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return st.nextToken();
	}

	int nextInt() {
		return Integer.parseInt(nextToken());
	}

	long nextLong() {
		return Long.parseLong(nextToken());
	}

	double nextDouble() {
		return Double.parseDouble(nextToken());
	}
}

}
