import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.PriorityQueue;
import java.util.Set;

public class LOWSUM {
	static InputReader in;
	static PrintWriter out;

	public static void main(String[] args) throws Exception {
		in = new InputReader(System.in);
		out = new PrintWriter(System.out);
		int T = in.nextInt();

		for (int test = 0; test < T; test++) {
			solve();
		}

		out.close();
	}

	static void solve() {
		int N = in.nextInt();
		int Q = in.nextInt();

		Long[] as = new Long[N];
		Long[] bs = new Long[N];
		for (int i = 0; i < N; i++) {
			as[i] = in.nextLong();
		}
		for (int i = 0; i < N; i++) {
			bs[i] = in.nextLong();
		}
		int[] q = new int[Q];
		int maxQ = -1;
		for (int i = 0; i < Q; i++) {
			q[i] = in.nextInt();
			maxQ = Math.max(q[i], maxQ);
		}
		
		Arrays.sort(as);
		Arrays.sort(bs);
		long[] a = new long[N];
		long[] b = new long[N];
		for(int i=0;i<N;i++){a[i] = as[i];}
		for(int i=0;i<N;i++){b[i] = bs[i];}
		
		// expand fringe from a stair shaped thingy
		PriorityQueue<PqNode> pq = new PriorityQueue<>();
		Set<Pair> visit = new HashSet<Pair>();
		Pair initnode = new Pair(0,0);
		visit.add(initnode);
		pq.add(new PqNode(initnode, a[0]+b[0]));
		
		long[] ans = new long[maxQ+1];
		for(int i = 1; i <= maxQ; i++) {
			PqNode pqnode = pq.poll();
			ans[i] = pqnode.weight;
			Pair p = pqnode.pair;
		
			Pair p1 = new Pair(p.first+1, p.second);
			Pair p2 = new Pair(p.first, p.second+1);
			if(p1.first < N && !visit.contains(p1)) {
				visit.add(p1);
				pq.add(new PqNode(p1, a[p1.first] + b[p1.second]));
			}
			if(p2.first < N && !visit.contains(p2)) {
				visit.add(p2);
				pq.add(new PqNode(p2, a[p2.first] + b[p2.second]));
			}
		}
		
		for(int qu : q){
			out.println(ans[qu]);
		}
	}

	static class PqNode implements Comparable<PqNode>{
		public Pair pair;
		public long weight;
		public PqNode(Pair pair, long weight) {
			this.pair = pair;
			this.weight = weight;
		}
		
		@Override
		public int compareTo(PqNode o) {
			return Long.compare(weight, o.weight);
		}
	}
	
	static class Pair {
		public int first;
		public int second;
		public Pair(int first, int second) {
			this.first = first;
			this.second = second;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + first;
			result = prime * result + second;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Pair other = (Pair) obj;
			if (first != other.first)
				return false;
			if (second != other.second)
				return false;
			return true;
		}
	}
	
	static class InputReader {
		private InputStream stream;
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}

		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public String nextToken() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return isWhitespace(c);
		}

		public static boolean isWhitespace(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}
}
